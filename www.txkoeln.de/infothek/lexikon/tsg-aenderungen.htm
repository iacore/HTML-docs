<!DOCTYPE html>

<html lang="de">

<head>

<title>TXpedia - Änderungen des Trans&shy;sexuellen&shy;gesetzes (TSG)</title>

<meta charset="utf-8">

<meta name="description" lang="de" content="Übersicht: Welche Änderungen sind wann am Trans&shy;sexuellen&shy;gesetz (TSG) durch das Parlament und dem Bundesverfassungsgericht seit seiner Vekündung beschlossen worden?">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" type="image/x-icon" href="../../favicon.ico">
<link rel="stylesheet" type="text/css" href="https://www.txkoeln.de/scripte/navigation.css">
<link rel="stylesheet" type="text/css" href="https://www.txkoeln.de/scripte/inhalt.css">

<style>
	dt {float: left; clear: left; width: 2em; text-align: right;}
</style>

</head>

<body>

<div id="seite">

	<header id="kopf">
		<img id="logo" src="https://www.txkoeln.de/grafik/logo-txkoeln.png" alt="TXKöln-Logo">
	</header>

	<nav id="haupt">
		<ul>
			<li><a href="../../index.htm">TXKöln</a></li>
			<li><a href="../../programm/aktuell.htm">Programm</a></li>
			<li class="aktiv">Infothek</li>
			<li><a href="../../bibliothek/bibliothek.htm">Bibliothek</a></li>
			<li><a href="../../treffpunkt/treffpunkt.htm">Treffpunkt</a></li>
			<li><a href="../../kontakt/kontakt.htm">Kontakt</a></li>
		</ul>
	</nav>

	<nav id="unter">
		<ul>
			<li><a href="../txpedia.htm">TXpedia</a></li>
			<li class="aktiv">>&nbsp;&nbsp;Thema</li>
			<li><a href="../tipps.htm">Tipps und Erfahrungen</a></li>
			<li><a href="../links.htm">Hilfreiche Links</a></li>
		</ul>
	</nav>

	<main>
		<article>
			<header>
				<h1>Änderungen des Trans&shy;sexuellen&shy;gesetzes (TSG)</h1>
			</header>
			
			<div class="section">
				<div class="ueberschrift"></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Alle Gesetze, die auf Bundesebene in Deutschland beschlossen werden, erlangen erst Gültigkeit, wenn sie, nachdem der Bundespräsident unterzeichnet hat, im <a href="https://www.bgbl.de/"
	 target="_blank" rel="noopener">&#8599;&nbsp;Bundes&shy;gesetz&shy;blatt</a> (BGBl.) verkündet werden. Dabei können die Gesetze sowohl rückwirkend wirken, als auch erst zu einem späteren Zeitpunkt wirksam werden. Ist für das Inkrafttreten
	 kein Datum angegeben, tritt ein Gesetz automatisch am 14. Tag nach seiner Verkündigung in Kraft (<a href="https://www.gesetze-im-internet.de/gg/art_82.html" target="_blank" rel="noopener">&#8599;&nbsp;<cite>Grundgesetz Artikel 82</cite></a>).
</p>
<p>
	Auch Entscheidungen des Bundes&shy;verfassungs&shy;gerichts (BVerfG), die zwar keine Gesetzes&shy;texte ändern, wohl aber außer Kraft setzen können, werden im BGBl. veröffentlicht.
</p>
<p>
	Das BGBl. besteht aus zwei Teilen, wobei Teil I u.a. Bundesgesetze, als auch Entscheidungen und Urteile des BVerfG, und Teil II völkerrechtliche Entscheidungen verkündet. Alles zum <cite>Trans&shy;sexuellen&shy;gesetz</cite>
	 findet sich also im Teil I. Organisiert ist das BGBl. in Jahren, wobei die Nummerierung der Ausgaben jedes Jahr wieder neu beginnt, die Seitenzahlen aber bis zum Jahres&shy;ende
	 ausgabenübergreifend fortlaufen sind.
</p>
<p>
	Im Folgenden sind die Gesetzesänderungen/<wbr>Entscheidungen des BVerfG in absteigender Reihenfolge aufgeführt. Eine Seitenzahl in Klammern hinter der Ausgaben&shy;nummer bezeichnet die TSG-zugehörige Fund&shy;stelle
	 innerhalb einer Ausgabe, dessen Start&shy;seite ohne Klammern angegeben ist.
</p>
<p>
	<u>Anmerkung</u>: siehe auch <a href="tsg.htm">Transsexuellen&shy;gesetz</a> (Aktuell), <a href="tsg-entstehung.htm">Entstehung des Trans&shy;sexuellen&shy;gesetzes</a> und <a href="antragtsg.htm">Antrag auf
	 Vornamens- und Personen&shy;stands&shy;änderung</a>
</p>

<!-- Textbereich Ende -->

				</div>
			</div>

			<nav class="section">
				<div class="ueberschrift"><h2>Geänderte Paragraphen</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<ul>
	<li><a href="#18">2017</a>&nbsp;&nbsp;(§7 Abs.1 Nr.2 u. 3))</li>
	<li><a href="#17">2017</a>&nbsp;&nbsp;(§3 Abs.2 u. 3)</li>
	<li><a href="#16">2013</a>&nbsp;&nbsp;(§14)</li>
	<li><a href="#15">2011</a>&nbsp;&nbsp;(§8 Abs.1 Nr.3 u. 4) - BVerfG</li>
	<li><a href="#14">2009</a>&nbsp;&nbsp;(§8 Abs.1 Nr.2, §9 Abs.1 Satz 1)</li>
	<li><a href="#13">2008</a>&nbsp;&nbsp;(§3 Abs.1 Satz 1, §4 Abs.1, §13)</li>
	<li><a href="#12">2008</a>&nbsp;&nbsp;(§8 Abs.1 Nr.2) - BVerfG</li>
	<li><a href="#11">2007</a>&nbsp;&nbsp;(§1 Abs.1 Nr.1 bis 3)</li>
	<li><a href="#10">2007</a>&nbsp;&nbsp;(§7 Abs.2 Nr.1 u. 2, §15)</li>
	<li><a href="#9">2006</a>&nbsp;&nbsp;(§1 Abs.1 Nr.1) - BVerfG</li>
	<li><a href="#8">2006</a>&nbsp;&nbsp;(§7 Abs.1 Nr.3) - BVerfG</li>
	<li><a href="#7">1998</a>&nbsp;&nbsp;(§5 Abs.3, §7 Abs.1 Nr.3 u. Abs.2 Nr.1)</li>
	<li><a href="#6">1997</a>&nbsp;&nbsp;(§7 Abs1 Nr.1 u. 2)</li>
	<li><a href="#5">1993</a>&nbsp;&nbsp;(§1 Abs.1 Nr.3) - BVerfG</li>
	<li><a href="#4">1990</a>&nbsp;&nbsp;(§3 Abs. 1 Satz 1)</li>
	<li><a href="#3">1989</a>&nbsp;&nbsp;(§12 Abs.1 Satz 2)</li>
	<li><a href="#2">1982</a>&nbsp;&nbsp;(§8 Abs.1 Nr.1) - BVerfG</li>
	<li><a href="#1">1980</a>&nbsp;&nbsp;(Gesetz verkündet)</li>
</ul>

<!-- Textbereich Ende -->

				</div>
			</nav>

			<section id="18">
				<div class="ueberschrift"><h2>2017</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.52 vom 28.07.2017, S.2787 (S.2787)
</p>
<h3>Gesetz zur Einführung des Rechts auf Eheschließung für Personen gleichen Geschlechts</h3>
<p>
	Vom 20. Juli 2017
</p>
<h4>Artikel 2 Absatz 3</h4>
<p>
	§ 7 Absatz 1 des Trans&shy;sexuellen&shy;gesetzes vom 10. September 1980 (BGBl. I S. 1654), das zuletzt durch Artikel 2a des Gesetzes vom 17. Juli 2017 (BGBl. I S. 2522) geändert worden ist, wird wie folgt geändert:
</p>
<ol>
	<li>In Nummer 2 wird das Wort &quot;oder&quot; gestrichen und das Komma durch einen Punkt ersetzt.</li>
	<li>Nummer 3 wird aufgehoben.</li>
</ol>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="17">
				<div class="ueberschrift"><h2>2017</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.49 vom 24.07.2017, S.2522 (S.2530)
</p>
<h3>Zweites Gesetz zur Änderung personen&shy;stands&shy;recht&shy;licher Vorschriften (2. Personen&shy;stands&shy;rechts-Änderungs&shy;gesetz – 2. PStRÄndG)</h3>
<p>
	Vom 17. Juli 2017
</p>
<h4>Artikel 2a - Änderung des Trans&shy;sexuellen&shy;gesetzes</h4>
<p>
	§ 3 des Trans&shy;sexuellen&shy;gesetzes vom 10. September 1980 (BGBl. I S. 1654), das zuletzt durch Artikel 1 des Gesetzes vom 17. Juli 2009 (BGBl. I S. 1978) geändert worden ist, wird wie folgt geändert:
</p>
<ol>
	<li>
		Absatz 2 wird wie folgt gefasst:
		<p>
			(2) Beteiligter des Verfahrens ist nur der Antragsteller oder die Antragstellerin
		</p>
	</li>
	<li>Absatz 3 wird aufgehoben.</li>
</ol>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="16">
				<div class="ueberschrift"><h2>2013</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.42 vom 29. Juli 2013, S.2586
</p>
<h3>Zweites Gesetz zur Modernisierung des Kostenrechts (2. Kostenrechts&shy;modernisierungs&shy;gesetz – 2. KostRMoG)</h3>
<p>
	Vom 23. Juli 2013
</p>
<p>
	<u>Anmerkung</u>: mit der Schaffung des Gesetzes über Kosten der freiwilligen Gerichtsbarkeit für Gerichte und Notare (GNotKG) im Artikel 1 ist das Gesetz über die Kosten in Angelegenheiten der freiwilligen Gerichtsbarkeit
	 (Kostenordnung) aufgehoben. Der §14 des Trans&shy;sexuellen&shy;gesetzes entfällt.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="15">
				<div class="ueberschrift"><h2>2011</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.5 vom 10.02.2011, S.224
</p>
<h3>Entscheidung des Bundes&shy;verfassungs&shy;gerichts (zu §8 Abs. 1 Nr. 3 und 4 des Trans&shy;sexuellen&shy;gesetzes)</h3>
<h4>Entscheidung des Bundes&shy;verfassungs&shy;gerichts</h4>
<p>
	Aus dem Beschluss des Bundes&shy;verfassungs&shy;gerichts vom 11. Januar 2011 - 1 BvR 3295/07- wird die Entscheidungsfomel veröffentlicht:
</p>
<ol>
	<li>
		§ 8 Absatz 1 Nummern 3 und 4 des Gesetzes über die Änderung der Vornamen und die Feststellung der Geschlechtszugehörigkeit in besonderen Fällen (Transsexuellengesetz – TSG) vom 10. September 1980
		 (Bundesgesetzblatt Teil I Seite 1654) ist mit Artikel 2 Absatz 1 und Absatz 2 in Verbindung mit Artikel 1 Absatz 1 des Grundgesetzes nach Maßgabe der Gründe nicht vereinbar.
	</li>
	<li>
		§ 8 Absatz 1 Nummer 3 und 4 des Trans&shy;sexuellen&shy;gesetzes ist bis zum Inkrafttreten einer gesetzlichen Neuregelung nicht anwendbar.
	</li>
</ol>
<p>
	Die vorstehende Entscheidungsformel hat gemäß § 31 Abs. 2 des Gesetzes über das Bundes&shy;verfassungs&shy;gerichtsgesetzes Gesetzeskraft.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="14">
				<div class="ueberschrift"><h2>2009</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.43 vom 22.07.2009, S.1978
</p>
<h3>Gesetz zur Änderung des Trans&shy;sexuellen&shy;gesetzes (Trans&shy;sexuellen&shy;gesetz-Änderungs&shy;gesetz – TSG-ÄndG)</h3>
<p>
	Vom 17. Juli 2009
</p>
<p>
	Der Bundestag hat das folgende Gesetz beschlossen:
</p>
<h4>Artikel 1 - Änderung des Trans&shy;sexuellen&shy;gesetzes</h4>
<p>
	Das Trans&shy;sexuellen&shy;gesetz vom 10. September 1980 (BGBl. I S. 1654), das zuletzt durch Artikel 11 des Gesetzes vom 17. Dezember 2008 (BGBl. I S. 2586) geändert worden ist, wird wie folgt geändert:
</p>
<ol>
	<li>§ 8 Abs. 1 Nr. 2 wird aufgehoben.</li>
	<li>In § 9 Abs. 1 Satz 1 werden das Komma nach dem Wort &quot;hat&quot; gestrichen und das Wort &quot;oder&quot; eingefügt sowie die Wörter &quot;oder noch verheiratet ist&quot; gestrichen.</li>
</ol>
<h4>Artikel 2 - Inkrafttreten</h4>
<p>
	Dieses Gesetz tritt am Tag nach der Verkündung in Kraft.
</p>
<p>
	Die verfassungsmäßigen Rechte des Bundesrates sind gewahrt. Das vorstehende Gesetz wird hiermit ausgefertigt. Es ist im Bundesgesetzblatt zu verkünden.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="13">
				<div class="ueberschrift"><h2>2008</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.61 vom 22.12.2008, S.2586 (S.2693)
</p>
<h3>Gesetz zur Reform des Verfahrens in Familiensachen und in den Angelegenheiten der freiwilligen Gerichtsbarkeit (FGG-Reformgesetz – FGG-RG)</h3>
<p>
	Vom 17. Dezember 2008
</p>
<h4>Artikel 11 - Änderung des Trans&shy;sexuellen&shy;gesetzes</h4>
<p>
	Das Trans&shy;sexuellen&shy;gesetz vom 10. September 1980 (BGBl. I S. 1654), zuletzt geändert durch Artikel 3a des Gesetzes vom 20. Juli 2007 (BGBl. I S. 1566), wird wie folgt geändert:
</p>
<ol>
	<li>In § 3 Abs. 1 Satz 2 wird das Wort &quot;Vormundschafts&shy;gerichts&quot; durch das Wort &quot;Familien&shy;gerichts&quot; ersetzt.</li>
	<li>
		In § 4 Abs. 1 werden die Wörter &quot;Gesetz über die Angelegenheiten der freiwilligen Gerichtsbarkeit&quot; durch die Wörter &quot;Gesetz über das Verfahren in Familiensachen und in den Angelegenheiten der
		 freiwilligen Gerichtsbarkeit&quot; ersetzt.
	</li>
</ol>
<p>
	<u>Anmerkung</u>: durch die Änderung des Rechtspflegergesetzes im Artikel 23 entfällt §13 des Trans&shy;sexuellen&shy;gesetzes
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="12">
				<div class="ueberschrift"><h2>2008</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.35 vom 08.08.2008, S.1650
</p>
<h3>Entscheidung des Bundes&shy;verfassungs&shy;gerichts (zu §8 Abs. 1 Nr. 2 des Trans&shy;sexuellen&shy;gesetzes)</h3>
<h4>Entscheidung des Bundes&shy;verfassungs&shy;gerichts</h4>
<p>
	Aus dem Beschluss des Bundes&shy;verfassungs&shy;gerichts vom 27. Mai 2008 - 1 BvL 10/05- wird die Entscheidungsfomel veröffentlicht:
</p>
<ol>
	<li>
		§ 8 Absatz 1 Nummer 2 des Gesetzes über die Änderung der Vornamen und die Feststellung der Geschlechts&shy;zugehörigkeit in besonderen Fällen (Trans&shy;sexuellen&shy;gesetz – TSG) vom 10. September 1980 (Bundesgesetzblatt I
		 Seite 1654) in der Fassung des Gesetzes zur Änderung des Passgesetzes und weiterer Vorschriften vom 20. Juli 2007 (Bundesgesetzblatt I Seite 1566) ist mit Artikel 2 Absatz 1 in Verbindung mit Artikel 1 Absatz 1
		 und Artikel 6 Absatz 1 des Grundgesetzes nach Maßgabe der Gründe unvereinbar.
	</li>
	<li>
		§ 8 Absatz 1 Nummer 2 des Trans&shy;sexuellen&shy;gesetzes ist bis zum Inkrafttreten einer gesetzlichen Neuregelung nicht anwendbar.
	</li>
</ol>
<p>
	Die vorstehende Entscheidungsformel hat gemäß § 31 Abs. 2 des Gesetzes über das Bundes&shy;verfassungs&shy;gerichtsgesetzes Gesetzeskraft.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="11">
				<div class="ueberschrift"><h2>2007</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.35 vom 27.07.2007, S.1566 (S.1571)
</p>
<h3>Gesetz zur Änderung des Passgesetzes und weiterer Vorschriften</h3>
<p>
	Vom 20. Juli 2007
</p>
<h4>Artikel 3a - Trans&shy;sexuellen&shy;gesetz</h4>
<p>
	§ 1 Abs. 1 des Trans&shy;sexuellen&shy;gesetzes vom 10. September 1980 (BGBl. I S. 1654), das zuletzt durch Artikel 2 Abs. 5 des Gesetzes vom 19. Februar 2007 (BGBl. I S. 122) geändert worden ist, wird wie folgt gefasst:
</p>
<dl>
	<dt>1.</dt>
	<dd>
		sie sich auf Grund ihrer transsexuellen Prägung nicht mehr dem in ihrem Geburtseintrag angegebenen Geschlecht, sondern dem anderen Geschlecht als zugehörig empfindet und seit mindestens
		 drei Jahren unter dem Zwang steht, ihren Vorstellungen entsprechend zu leben,
	</dd>
	<dt>2.</dt>
	<dd>mit hoher Wahrscheinlichkeit anzunehmen ist, dass sich ihr Zugehörigkeitsempfinden zum anderen Geschlecht nicht mehr ändern wird, und</dd>
	<dt>3.</dt>
	<dd>
		sie
		<dl>
			<dt>a.</dt>
			<dd>Deutscher im Sinne des Grundgesetzes ist,</dd>
			<dt>b.</dt>
			<dd>als Staatenloser oder heimatloser Ausländer ihren gewöhnlichen Aufenthalt im Inland hat,</dd>
			<dt>c.</dt>
			<dd>als Asylberechtigter oder ausländischer Flüchtling ihren Wohnsitz im Inland hat oder</dd>
			<dt>d.</dt>
			<dd>
				als Ausländer, dessen Heimatrecht keine diesem Gesetz vergleichbare Regelung kennt,
				<dl>
					<dt>aa.</dt>
					<dd>ein unbefristetes Aufenthaltsrecht besitzt oder</dd>
					<dt>bb.</dt>
					<dd>eine verlängerbare Aufenthaltserlaubnis besitzt und sich dauerhaft rechtmäßig im Inland aufhält.</dd>
				</dl>
			</dd>
		</dl>
	</dd>
</dl>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="10">
				<div class="ueberschrift"><h2>2007</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.5 vom 23.02.2007, S.122  (S.139)
</p>
<h3>Gesetz zur Reform des Personen&shy;stands&shy;rechts (Personen&shy;stands&shy;rechtsreform&shy;gesetz - PSTRG)</h3>
<p>
	Vom 19. Februar 2007
</p>
<h4>Artikel 2 Absatz 5 - Trans&shy;sexuellen&shy;gesetz</h4>
<p>
	§ 7 Abs. 2 Satz 2 Nr. 1 und 2 des Trans&shy;sexuellen&shy;gesetzes vom 10. September 1980 (BGBl. I S. 1654) das zuletzt durch Artikel 13 des Gesetzes vom 4. Mai 1998 (BGBl. I S. 833) geändert worden ist,
	 wird wie folgt gefasst:
</p>
<ol>
	<li>im Fall des Absatzes 1 Nr. 1 und 2 in das Geburtenregister,</li>
	<li>im Fall des Absatzes 1 Nr. 3 in das Eheregister</li>
</ol>
<p>
	<u>Anmerkung</u>: durch die Neufassung des Personenstands&shy;gesetzes entfällt §15 des Trans&shy;sexuellen&shy;gesetzes
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="9">
				<div class="ueberschrift"><h2>2006</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.55 vom 06.12.2006, S.2737
</p>
<h3>Entscheidung des Bundes&shy;verfassungs&shy;gerichts (zu §1 Abs. 1 Nr. 1 des Trans&shy;sexuellen&shy;gesetzes)</h3>
<h4>Entscheidung des Bundes&shy;verfassungs&shy;gerichts</h4>
<p>
	Aus dem Beschluss des Bundes&shy;verfassungs&shy;gerichts vom 18. Juli 2006 - 1 BvL 1/04, 1 BvL 12/04- wird die Entscheidungsfomel veröffentlicht:
</p>
<ol>
	<li>
		§ 1 Absatz 1 Nummer 1 des Gesetzes über die Änderung der Vornamen und die Feststellung der Geschlechts&shy;zugehörig&shy;keit in besonderen Fällen (Trans&shy;sexuellen&shy;gesetz - TSG)vom 10. September 1980
		  (Bundesgesetzblatt I Seite 1654) ist mit Artikel 3 Absatz 1 in Verbindung mit dem Grundrecht auf Schutz der Persönlichkeit (Artikel 2 Absatz 1 in Verbindung mit Artikel 1 Absatz 1 des Grundgesetzes)
		  nicht vereinbar, soweit ausländische Transsexuelle, die sich rechtmäßig und nicht nur vorübergehend in Deutschland aufhalten, von der Antragsberechtigung zur Änderung des Vornamens und zur Feststellung
		  der Geschlechts&shy;zugehörig&shy;keit nach § 8 Absatz 1 Nummer 1 des Trans&shy;sexuellen&shy;gesetzes ausnimmt, sofern deren Heimatrecht vergleichbare Regelungen nicht kennt.
	</li>
	<li>
		§ 1 Absatz 1 Nummer 1 des Trans&shy;sexuellen&shy;gesetzes bleibt bis zum Inkrafttreten einer gesetzlichen Neuregelung anwendbar.
	</li>
	<li>
		Dem Gesetzgeber wird aufgegeben, bis zum 30. Juni 2007 eine verfassungsgemäße Neuregelung zu treffen.
	</li>
</ol>
<p>
	Die vorstehende Entscheidungsformel hat gemäß § 31 Abs. 2 des Gesetzes über das Bundes&shy;verfassungs&shy;gerichtsgesetzes Gesetzeskraft.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="8">
				<div class="ueberschrift"><h2>2006</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.5 vom 30.01.2006, S.276
</p>
<h3>Entscheidung des Bundes&shy;verfassungs&shy;gerichts (zu §7 Abs. 1 Nr. 3 des Trans&shy;sexuellen&shy;gesetzes)</h3>
<h4>Entscheidung des Bundes&shy;verfassungs&shy;gerichts</h4>
<p>
	Aus dem Beschluss des Bundes&shy;verfassungs&shy;gerichts vom 6. Dezember 2005 - 1 BvL 3/03- wird die Entscheidungsfomel veröffentlicht:
</p>
<ol>
	<li>
		§ 7 Absatz 1 Nummer 3 des Gesetzes über die Änderung der Vornamen und die Feststellung der Geschlechts&shy;zugehörig&shy;keit in besonderen Fällen (Trans&shy;sexuellen&shy;gesetz - TSG)vom 10. September 1980
		  (Bundesgesetzblatt I Seite 1654) ist mit Artikel 2 Absatz 1 in Verbindung mit Artikel 1 Absatz 1 des Grundgesetzes nicht vereinbar, solange homosexuell orientierten
		  Transsexuellen ohne Geschlechtsumwandlung eine rechtlich gesicherte Partnerschaft nicht ohne Verlust des nach § 1 des Trans&shy;sexuellen&shy;gesetzes geänderten Vornamens eröffnet ist.
	</li>
	<li>
		§ 7 Absatz 1 Nummer 3 des Trans&shy;sexuellen&shy;gesetzes ist bis zum In-Kraft-Treten einer gesetzlichen Regelung, die homosexuell orientierten Transsexuellen ohne
		 Geschlechtsumwandlung das Eingehen einer rechtlich gesicherten Partnerschaft ohne Vornamensverlust ermöglicht, nicht anwendbar.
	</li>
</ol>
<p>
	Die vorstehende Entscheidungsformel hat gemäß § 31 Abs. 2 des Gesetzes über das Bundes&shy;verfassungs&shy;gerichtsgesetzes Gesetzeskraft.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="7">
				<div class="ueberschrift"><h2>1998</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.25 vom 08.05.1998, S.833 (S.841)
</p>
<h3>Gesetz zur Neuordnung des Eheschließungsrechts (Eheschließungsrechtsgesetz - EheschlRG)</h3>
<p>
	Vom 4. Mai 1998
</p>
<h4>Artikel 13 - Änderung des Trans&shy;sexuellen&shy;gesetzes</h4>
<p>
	Das Trans&shy;sexuellen&shy;gesetz vom 10. September 1980 (BGBl. I S. 1654), zuletzt geändert durch Artikel 14 § 2 des Gesetzes vom 16. Dezember 1997 (BGBl. I S. 2942), wird wie folgt geändert:
</p>
<ol>
	<li>In § 5 Abs. 3 werden der Strichpunkt und die Worte &quot;gleiches gilt für den Eintrag einer Todgeburt&quot; gestrichen.</li>
	<li>
		§ 7 wird wie folgt geändert:
		<ol style="list-style-type: lower-alpha;">
			<li>In Absatz 1 Nr. 3 wird die Angabe &quot;§ 13 des Ehegesetzes&quot; durch die Angabe &quot;§ 1310 Abs. 1 des Bürgerlichen Gesetzbuchs&quot; ersetzt.</li>
			<li>In Absatz 2 Nr. 1 werden die Worte &quot;bei einer Todgeburt in das Sterbebuch&quot; und das nachfolgende Komma gestrichen.</li>
		</ol>
	</li>
</ol>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="6">
				<div class="ueberschrift"><h2>1997</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.84 vom 19.12.1997, S.2942 (S.2964)
</p>
<h3>Gesetz zur Reform des Kindschaftsrechts (Kindschaftsrechtsreformgesetz - KindRG)</h3>
<p>
	Vom 16. Dezember 1997
</p>
<h4>Artikel 14 § 2 - Änderung des Trans&shy;sexuellen&shy;gesetzes</h4>
<p>
	In § 7 Abs. 1 Nr. 1 und 2 des Trans&shy;sexuellen&shy;gesetzes vom 10. September 1980 (BGBl- I S. 1654), das zuletzt durch Artikel 7 § 8 des Gesetzes vom 12. September 1990 (BGBl. I S. 2002) geändert worden ist,
	 wird jeweils das Wort &quot;dreihundertzwei&quot; durch das Wort &quot;dreihundert&quot; ersetzt.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="5">
				<div class="ueberschrift"><h2>1993</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.9 vom 19.03.1993, S.326
</p>
<h3>Entscheidung des Bundes&shy;verfassungs&shy;gerichts (zu §3 Abs. 1 Nr. 3 des Trans&shy;sexuellen&shy;gesetzes)</h3>
<h4>Entscheidung des Bundes&shy;verfassungs&shy;gerichts</h4>
<p>
	Aus dem Beschluß des Bundes&shy;verfassungs&shy;gerichts vom 26. Januar 1993 -1 BvL 38/92 u.a.- wird die Entscheidungsformel veröffentlicht:
</p>
<p>
	§ 1 Absatz 1 Nummer 3 des Gesetzes über die Änderung der Vornamen und Feststellung der Geschlechts&shy;zugehörig&shy;keit in besonderen Fällen (Trans&shy;sexuellen&shy;gesetz - TSG) vom 10. September 1980 (Bundesgesetzbl. I S. 1654) ist
	 mit Artikel 3 Absatz 1 des Grundgesetzes unvereinbar und nichtig.
</p>
<p>
	Die vorstehende Entscheidungsformel hat gemäß § 31 Abs. 2 des Gesetzes über das Bundesverfassungsgericht Gesetzeskraft.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="4">
				<div class="ueberschrift"><h2>1990</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.48 vom 21.09.1990, S.2002 (S.2018)
</p>
<h3>Gesetz zur Reform des Rechts der Vormundschaft und Pflegschaft für Volljährige (Betreuungsgesetz - BtG)</h3>
<p>
	Vom 12. September 1990
</p>
<h4>Artikel 7 § 8 - Änderung des Trans&shy;sexuellen&shy;gesetzes</h4>
<p>
	§ 3 Abs. 1 Satz 1 des Trans&shy;sexuellen&shy;gesetzes vom 10. September 1980 (BGBl. I S. 1654), das durch Artikel 49 des Gesetzes vom 18. Dezember 1989 (BGBl. I S. 2261) geändert worden ist, wird aufgehoben.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="3">
				<div class="ueberschrift"><h2>1989</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.60 vom 28.12.1989, S.2261 (S.2386)
</p>
<h3>Gesetz zur Reform der gesetzlichen Rentenversicherung (Rentenreformgesetz 1992 - RRG 1992)</h3>
<p>
	Vom 18. Dezember 1989
</p>
<h4>Artikel 49 - Trans&shy;sexuellen&shy;gesetz</h4>
<p>
	In § 12 Abs. 1 Satz 2 des Trans&shy;sexuellen&shy;gesetzes vom 10. September 1980 (BGBl. I S. 1654) werden die Worte &quot;der Umwandlung solcher Leistungen wegen eines neuen Versicherungs&shy;falles oder geänderter
	 Verhältnisse&quot; durch die Worte &quot; einer sich unmittelbar anschließender Leistung aus demselben Rechtsverhältnis&quot; ersetzt.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="2">
				<div class="ueberschrift"><h2>1982</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.17 vom 19.05.1982, S.619
</p>
<h3>Entscheidung des Bundes&shy;verfassungs&shy;gerichts (zu §8 Abs. 1 Nr. 1 des Trans&shy;sexuellen&shy;gesetzes)</h3>
<h4>Entscheidung des Bundes&shy;verfassungs&shy;gerichts</h4>
<p>
	Aus dem Beschluß des Bundes&shy;verfassungs&shy;gerichts vom 16. März 1982 -1 BvR 938/81-, ergangen auf Verfassungsbeschwerde, wird die Entscheidungsformel veröffentlicht:
</p>
<p>
	§8 Absatz 1 Nummer 1 des Gesetzes über die Änderung der Vornamen und die Feststellung der Geschlechts&shy;zugehörig&shy;keit in besonderen Fällen (Transsexuellengestz - TSG) vom 10. September 1980 (Bundesgesetzbl. I S. 1654)
	 ist mit Artikel 3 Absatz 1 des Grundgesetzes insoweit unvereinbar und daher nichtig, als auch bei Erfüllung der übrigen gesetzlichen Voraussetzungen die gerichtliche Feststellung über die Änderung der ursprünglichen
	 Geschlechts&shy;zugehörig&shy;keit vor Vollendung des 25. Lebensjahres ausgeschlossen ist.
</p>
<p>
	Die vorstehende Entscheidungsformel hat gemäß § 31 Abs. 2 des Gesetzes über das Bundesverfassungsgericht Gesetzeskraft.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
			
			<section id="1">
				<div class="ueberschrift"><h2>1980</h2></div>
				<div class="inhalt">

<!-- Textbereich -->

<p>
	Ausgabe Nr.56 vom 16.09.1980, S.1654
</p>
<h3>Gesetz über die Änderung der Vornamen und Feststellung der Geschlechts&shy;zugehörig&shy;keit in besonderen Fällen (Trans&shy;sexuellen&shy;gesetz - TSG)</h3>
<p>
	Vom 10. September 1980
</p>
<p>
	Volltext siehe <a href="tsg-entstehung.htm">Entstehung des Trans&shy;sexuellen&shy;gesetzes</a>.
</p>

<!-- Textbereich Ende -->

				</div>
			</section>
		</article>
	</main>

	<aside>
		<nav id="seitenende">
			<a href="#seite">Seitenanfang</a>
		</nav>
	</aside>

</div>

</body>

</html>
