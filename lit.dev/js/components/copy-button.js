import{_ as t}from"../tslib.js";import{$ as i,r as e,e as o,s as c}from"../lit.js";import"../litdev-ripple-icon-button.js";
/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const r=i`<svg width="24" height="24" viewBox="0 0 24 24"><path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z"/></svg>`
/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */;class l extends c{render(){return i`<litdev-ripple-icon-button @click="${this._click}" label="Copy to clipboard">${r}</litdev-ripple-icon-button>`}_click(){void 0!==this.text&&navigator.clipboard.writeText(this.text)}}l.styles=e`:host{--mdc-theme-primary:currentcolor}`,t([o()],l.prototype,"text",void 0),customElements.define("copy-button",l);export{l as CopyButton};
