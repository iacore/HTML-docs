import{_ as e}from"../tslib.js";import{r as t,n as s,s as i,$ as l}from"../lit.js";import"../litdev-code-language-switch.js";import"../code-language-preference.js";
/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */let o=class extends i{render(){return l`<litdev-code-language-switch></litdev-code-language-switch><slot></slot>`}};o.styles=t`:host{display:block;position:relative}litdev-code-language-switch{position:absolute;right:6px;top:6px}`,o=e([s("litdev-switchable-sample")],o);export{o as LitDevSwitchableSample};
