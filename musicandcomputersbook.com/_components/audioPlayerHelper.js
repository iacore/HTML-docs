const audioPlayer = document.querySelector('#audio-player');
const setSrc = (src) => {
  audioPlayer.setAttribute('audiofile', src);
};
audioPlayer.addEventListener('ended', () => audioPlayer.removeAttribute('audiofile'));
