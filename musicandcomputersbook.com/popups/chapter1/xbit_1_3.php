<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>More on Decibels</h1>
					<p>When we talk about decibels we are really talking about a ratio of two quantities. These quanitites could describe sound pressure, voltage, or wattage&#151;all of which are interesting subjects for electronic music composers.</p>
					<p>You are probably most familiar with decibels being discussed in terms of SPL (sound pressure level) or acoustic levels. With SPLs, we discuss the ratio of an audible sound to the <i>threshold</i> of sound (the point where we can begin to just hear a sound). So, a loud concert hovers around 115 dB above the threshold of hearing (which we would call 0 dB). If I were speaking to you, my voice would probably sound to you to be about 80 dB above the threshold of hearing.</p>
					<p>But really, the definition of a decibel stems from the unit of measurement called a <i>bel</i> which is the logarithm of an electrical, acoustic, or other power ratio. Again, it is important to note that a decibel describes the ratio of two powers, not a specific power value. So a decibel itself has no absolute value at all.</p>
					<p>How do we perceive decibels in terms of sound pressure levels? A sound that is 3 dB higher in level in comparison to another sound may seem just a tiny bit louder. A sound that is 10 dB higher in level sounds twice as loud as the softer sound we are comparing it to. If you have ever seen a mixer used for sound reinforcement, you may have noticed that the sliders might have decibel markings. Try pushing the slider up 10 dB while monitoring a sound to see if the sound appears to get twice as loud.</p>
				</td>
			</tr>
		</table>

		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
