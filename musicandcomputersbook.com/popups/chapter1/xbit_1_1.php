<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>


		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1><b>Sonification</b></h1>
					<table width="180" border="0" cellpadding="12" align="right" bgcolor="white">
						<tr>
							<td>
								<p class="figure_caption_flush"><img height="173" width="134" src="../../images/chapter1/dodge.gif"></p>
								<p class="figure_caption_flush"><b>Figure 1&nbsp;&nbsp;</b>Charles Dodge is an important computer music composer and pioneer whose early work <i>Earth&#146;s Magnetic Field</i> is an interesting example of sonification of a natural function. Dodge (with his collaborator, scientist Bruce Boller) used a graph of what are called the <i>K<sub>p</sub></i> indices, which chart the &quot;average magnetic activity&quot; for the Earth:</p>
								<p class="figure_caption_flush">&quot;The succession of notes in the music corresponds to the natural succession of the <i>K<sub>p</sub></i> indices for the year 1961... The musical interpretration consists of setting up a correlation between the level of the <i>K<sub>p</sub></i> reading and the pitch of the note (in a diatonic collection over four octaves), and compressing the 2,920 readings for the year into just over eight minutes of musical time&quot; (from the liner notes to Charles Dodge, <i>Earth&#146;s Magnetic Field,</i> Nonesuch LP H-71250).</p>
							</td>
						</tr>
					</table>
					<p>There are a great many functions that occur in nature, and one thing that composers have often experimented with is the sonification of those functions. (Sonification is the process of turning non-aural information into sound.) What do the stars in the sky sound like? How about the skyline of New York City? Or the fluctuations of the stock market? What would it mean to &quot;listen&quot; to such things? How can you take a natural process, a set of data, any sort of non-sound signal, and convert it perceptually to sound? For example, is it possible that by listening to a fast playback of the digits of <img src="../../images/shared/pi.gif" height="10" width="12"> or <i>e</i>, you could perceive a pattern, as yet undiscovered, that gives a unique personality to these numbers?</p>
					<p>Engineers and academics who study this idea often call it <i>auditory display</i>, and besides being artistically interesting, it can have important functional applications. For example, imagine when the air in your car&#146;s tires started to get low: your car starts to hum, and that hum gets higher and higher as the tires get flatter and flatter (sort of &quot;reverse&quot; sonification). </p>
					<p>The academic discipline of auditory display tends to divide this field into three categories (thanks to composer/researcher Ed Childs for the following and for some good ideas in this section):</p>
					<ul>
						<li><i>audification:</i> the direct rendering of digital data in (usually) sub-audio frequencies to the audible range, using resampling. Example: speeding up an hour of seismological data to play in a second.
						<li><i>sonification:</i> mapping data with some other meaning into sound<br>
						<li><i>auditory icons/earcons:</i> using sound in computer GUIs (graphical user interfaces) and other technological interfaces to orient users to menu depth, error conditions, and so on. For example, a cell phone company could design an <i>AUI</i> (auditory user interface) for its cell phones so that users do not have to look at the little LCD display while driving to select the desired function.

					</ul>
					<p>The question of exactly how to sonify a function is often referred to as the <i>mapping problem,</i> since the first thing that needs to be done is to figure out how to map, or relate, the original information to sonic parameters such as pitch, amplitude, and timbre. </p>
					<table width="100%" border="0" cellspacing="0" cellpadding="4">
						<tr>
							<td colspan="2" valign="top">
								<hr>
							</td>
						</tr>
						<tr>
							<td align="center" valign="top" width="1%">
								<p class="icon_caption"><a onclick="setSrc('../../sounds/chapter1/earth.mp3')"><img src="../../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
									<b><span class="sample_link"><nobr>Soundfile 1</nobr></span></b></p>
							</td>
							<td valign="top">
								<p class="applet_caption">Excerpt from Dodge&rsquo;s <i>Earth&#146;s Magnetic Field</i>, an example of classic computer music sonification</p>
							</td>
						</tr>
						<tr>
							<td colspan="2" valign="top">
								<hr>
							</td>
						</tr>
					</table>
					<p>Let&#146;s take a simple example: what if we want to &quot;listen&quot; to the digits of the number <b><img src="../../images/shared/pi.gif" height="10" width="12"></b>? How could we do it? Perhaps the simplest way would be to assign each of the numbers 0 through 9 to a specific frequency and to play that frequency every time its number comes up. We&#146;ll let you try this as an exercise, because we&#146;ve heard about ten-gazillion undergraduate pieces that use this brilliant idea. Our sophisticated pedagogical response?</p>
					<p>Pretty boring. Well, maybe the number <img src="../../images/shared/pi.gif" height="10" width="12"> just isn&#146;t very exciting (don&#146;t tell your geometry teacher!). What if we used Euler&#146;s constant, the number <i>e</i>? The two examples (if we made them) would be clearly different. However, in a larger sense, they wouldn&#146;t sound all that much different, because they&#146;re both lists of numbers that have very little in common with our musical or even perceptual expectations and experience.</p>
					<p>It would be difficult, if not impossible, for most people to know what they were listening to without being told. But artistically, this may not even matter! One of the primary issues that composers working with sonification need to address is: does it matter if the listeners &quot;know&quot; what they are listening to, and if so, how? How we map data can often be more significant than the data itself. Is there something inherently &quot;musical&quot; about these digits, and if so, are we just not ready to hear it? What do you think?</p>
					<p>The motivations behind sonification are similar to those of graphical representations of data&#151;the idea is that by mapping information from one domain onto another, we will perceive the data in a new way and hopefully gain some insight into what the data means. Instead of visualizing something, we <i>auralize</i> it. It&#146;s interesting that in computer music, we often go the other way as well&#151;for instance, a sonogram is a graphical representation of a set of aural data that gives us insights into the sound that we might otherwise not have had.</p>
					<p>Sonification is not only a musically interesting technique&#151;it&#146;s an idea being studied and used in many areas, particularly as an alternative to visually presented data. For example, airline pilots, who already have enough things to look at, might use sonification to &quot;listen&quot; to the status of certain key gauges in the cockpit. Or a scientist studying population growth might listen to population data from a number of countries, to try to hear any similarities or trends that aren&#146;t obvious from the raw or graphed data. Can you think of other areas where sonification might be useful?</p>
					<table width="100%" border="0" cellspacing="0" cellpadding="4">
						<tr>
							<td colspan="2" valign="top">
								<hr>
							</td>
						</tr>
						<tr>
							<td align="center" valign="top" width="1%">
								<p class="icon_caption"><a onclick="setSrc('../../sounds/chapter1/nerd.aiff.mp3')"><img src="../../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
									<b><span class="sample_link"><nobr>Soundfile 2</nobr></span></b><br>
									</p>
							</td>
							<td valign="top">
								<p class="applet_caption">Sonification of the raw data on a hard drive. &quot;I am a Nerd,&quot; by computer music composer Phil Stone.</p>
								
            <p class="applet_caption">In this wonderful, silly parody of computer 
              musicians, Stone used an early single board computer called a <i>SYM</i> (which had 1k of memory) to make the sounds. The &quot;solo&quot; in the middle is the computer simply reading through its program memory and translating that as a set of frequencies to it&rsquo;s tiny little speaker.</p>
							</td>
						</tr>
						<tr>
							<td colspan="2" valign="top">
								<hr>
							</td>
						</tr>
					</table>
					<p class="figure"><img height="457" width="560" src="../../images/chapter1/Sonif2.jpg"><br>
						<br>
					</p>
					<p class="figure_caption"><b>Figure 2</b>&nbsp;&nbsp;Ed Childs' fluid dynamics sonification (original data)</p>
					<p>Composer/researcher Ed Childs has been doing some interesting work in the sonification of complex mathematical problems involving fluid flow, an area which has long been of interest visually.</p>
					
      <p>Figure 2 is a typical graphical representation of a discretization of space, used by scientists to solve the complex sets of equations that describe the movement of fluid in some prescribed location (in this case, a horizontal duct into which flow enters across the dashed line at the left and leaves across the dashed line at the right). The horizontal and vertical arrows and the dots, respectively, are the storage locations of the <i>x</i> and <i>y</i> component velocities and the pressures. By 
        mapping each discrete velocity and pressure to sound as the solver runs, 
        Childs has been trying to present an aural description of the fluid flow 
        solver.</p>
					<p>There are two sound files below. The first sound file is a direct sonification. The second sound file is a piece built up from ten sonifications with different parameters.</p>
					<table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr> 
          <td colspan="2" valign="top"> <hr> </td>
        </tr>
						<tr>
							<td align="center" valign="top" nowrap width="1%">
								<p class="icon_caption"><a onclick="setSrc('../../sounds/chapter1/CFDSound5.mp3')"><img src="../../images/global/sound.gif" width="80" height="68" name="button34" border="0" alt=""></a><br>
									<b>Soundfile 3<br>
										<br>
									</b></p>
							</td>
							<td valign="top">
								<p class="applet_caption">A direct sonification.</p>
							</td>
						</tr>
						<tr> 
          <td align="center" valign="top" nowrap width="1%"> <p class="icon_caption"><a onclick="setSrc('../../sounds/chapter1/CFDSound5.mp3')"><img src="../../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a><br>
									<b>Soundfile 4</b></p></td>
          <td valign="top"> <p class="applet_caption">A composition built from ten fluid dynamics sonifications, each using different parameters.</p></td>
        </tr>
        <tr> 
          <td colspan="2" align="center" valign="top" nowrap> <hr> </td>
        </tr>
      </table>

		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
