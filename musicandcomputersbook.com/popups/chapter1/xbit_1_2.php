<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>Another Sonic Universe</h1>
					<p>Can you imagine a world in which pitch and amplitude are not independent? Turning up the radio would radically alter the pitches of the song you&#146;re listening to... it would be impossible to speak in a high, quiet voice... birds would sing different songs depending on how close you are to them... it would be a strange sounding world indeed! Imagine writing a piece that sounds like something you might hear in such a world, where the listeners have different perceptual rules.</p>
					<p>This is not as bizarre as it sounds. For example, a flute is a good example of an instrument in which pitch and loudness are closely correlated. Try, for instance, playing a soft, really high note on it. It turns out that the means of production of sound on flutes is such that high notes are loud and low notes are soft. A good flutist can, to a certain extent, get around this, but not in extreme circumstances (ask a flutist to play an E above the treble stave pianissimo and you&#146;ll get a dirty look!).</p>
					<p>When we talk about the Fletcher-Munson curves in Section 1.3, you&#146;ll also see that our perception of loudness is very much dependent on the perceived pitch of a sound as well.</p>
					<p>What&#146;s the moral of this all?</p>
					<center>
						<p><i>Nothing, grasshopper, is ever as simple as it seems.</i></p>
					</center>
				</td>
			</tr>
		</table>

		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
