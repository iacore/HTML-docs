<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>The Mathematics of Pure Tones</h1>
					<h3>Tuning Forks and the Basics of Sound</h3>
					<p>We&#146;ve now seen a few audiograms and sound sure can look messy. As scientists we want to look for the simplest kinds of sounds and then build any sound out of our fundamental building blocks.</p>
					<p>Just as chemistry tries to understand the properties of a substance by looking at the molecular structure, which in turn is studied by considering the individual atoms, we want to understand sound by finding &quot;atoms of sound,&quot; understanding these, and then combining the atoms into molecules of sound, and finally the molecules combine to form general sounds, like a symphony, a voice, or the wind rustling through the leaves.</p>
					<p>What should these pure sounds be? Let&#146;s think a bit about music. Most of us have seen a musical score. At its most basic level, the score lays out a sequence of notes to be played and the duration that the notes need to be held. So, running left to right is time, and up and down is pitch (middle C, A sharp, etc.). The &quot;pure&quot; tones are the notes. Notes are realized differently on each instrument, but in fact, they are characterized as particular kinds of sound waves that occur at specific frequencies. We&#146;ll explain this idea now.</p>
					<p>Different instruments realize these notes in different ways&#151;an A on a guitar sounds different from an A on a trombone or a flute. That&#146;s because the physics of a plucked string is different from the physics of a open-ended tube. As a way of understanding pure tones, or our atoms of sound, the simplest model is the tuning fork.</p>
					<p>We ring a tuning fork by whacking it against something and then standing it up and allowing the vibrations of the tines to disturb the surrounding air, sending the resulting sound to the waiting ears of everyone in the vicinity.</p>
					<p>The tines move back and forth, sending the air molecules to your ear, where they go crashing against your eardrum. So, what might the audiogram look like?</p>
					
      <p>Well, let&#146;s concentrate our attention on the tip of one of the tines. 
        In fact, imagine that we put a drop of glow-in-the-dark paint on the tip, 
        turn the lights off, and follow the glowing dot. We see it move back and 
        forth regularly (does a stiffer fork vibrate faster or slower than a &quot;flabby&quot; 
        fork?). The air molecules then get sent away, and our ear drums more or 
        less follow the same pattern. So, one possibility is something like this:</p>
					<p class="figure"><img height="156" width="287" src="../../images/chapter1/sawtooth.jpg"></p>
					<p class="figure_caption"><b>Figure 1</b></p>
					<p>Notice the regular up and down. It is a <i>periodic</i> wave, in the sense that there is a simple pattern that is repeated over and over again. The number of full copies of the pattern that fit in a single second is the frequency of the periodic function, and as we&#146;ve already remarked, is measured in <i>Hertz,</i> or cycles per second. The duration of a single copy of the basic pattern is called the <i>period</i> of the periodic function. The period and frequency are related by</p>
					<p><b><i>period = 1/frequency</i></b></p>
					<p>Now one possible drawback of the &quot;triangle&quot; wave (we call it that because the wave forms a triangle above and below the <i>x</i>-axis) is that it isn&#146;t smooth. The graph makes it look like as the tine reaches its maximal displacement, that it immediately whips back in the other direction. Our experience is that the change of direction is a bit more subtle than that&#151;that there is a gradual slowing down as the apex is reached, and then just for a moment, it&#146;s as if the tine stops, and then a gradual speeding up, and so on and so on... So, maybe instead, the graph is more like a smoothed out version of the <i>triangle,</i> maybe something like this:</p>
					<p class="figure"><img height="91" width="175" src="../../images/chapter1/sine.jpg"></p>
					<p class="figure_caption"><b>Figure 2</b></p>
					<p>Some of you may recognize this as the graph of the sine function! In fact, this is the right graph, and before we convince you of that mathematically, it&#146;s worth pointing out a few things about this graph.</p>
					<p>We&rsquo;ve already talked about a sine&rsquo;s frequency, or equivalently, its period. Figure 3 shows a few different kinds of sine functions of differing frequencies (but the same amplitude) all superimposed over one another. Higher frequency means the peaks and troughs are squeezed more closely together.</p>
					<p>The following picture shows a lot of sine waves, all at different frequencies, but at only one amplitude.</p>
					<p class="figure"><img height="401" width="484" src="../../images/chapter1/lotsofsine.jpg"></p>
					
      <p class="figure_caption"><b>Figure 3</b></p>
					<p>Perceptually, higher frequency (usually) means a higher pitch, although in fact this is sort of complicated and we&#146;ll discuss that later in Section 1.4.</p>
					<p>A few other things about these graphs: There are other things that could have been varied (besides the frequency) in making the pictures. We could have also varied the height (or equivalently, the depth) of the wave. This parameter is called the <i>amplitude</i> of the wave. Also, there is an arbitrary nature to where in time we started to measure the displacement. If we had started a moment later or earlier, we would have obtained a shifted version of this picture, though we&#146;d still have a sine wave.</p>
					<p>Figure 5 shows what happens when we have a lot of sine waves, all at the same frequency and all at the same amplitude, but at different phases.</p>
					<p class="figure"><img height="401" width="482" src="../../images/chapter1/lotsofphases.jpg"></p>
					
      <p class="figure_caption"><b>Figure 4</b></p>
					<p>Strangely enough, when we add up all those amplitude/frequency-similar-but-phase-different sine waves, we get&#151;you guessed it&#151;one pretty ordinary sine wave!</p>
					<p class="figure"><img height="404" width="484" src="../../images/chapter1/addingupphasedsines.jpg"></p>
					
      <p class="figure_caption"><b>Figure 5</b></p>
					<p>This phase parameter is called a <i>phase shift.</i> To conclude, any sinusoidal wave is determined by three parameters: frequency, amplitude, and phase shift. We&#146;ll be studying how the values of these parameters affect what we hear.</p>
					<h2>Getting to the Math</h2>
					<p><i>Newton's</i> second law states that the force felt by an object in motion is in proportion to the acceleration experienced by the object. For example, think about driving in your car&#151;you and the contents of your fine automobile are the objects in motion. You are speeding down the interstate, brimming coffee cup in the cup holder and compact discs on the seat next to you. You&#146;re cruising at 80 mph, so no acceleration, and life is good. You&#146;re slouched in your seat, coffee isn&#146;t spilling, CDs stay in place. Suddenly, you pass a state trooper hidden in the median. You hit the brakes, causing a deceleration (a negative acceleration), and now the calm of your car is disturbed. CDs go sliding to the floor, coffee sloshes against the side of the cup, and you find yourself thrown forward a bit in your seat. Everything is experiencing a force! The harder you brake, the stronger the force.</p>
					<p>In shorthand we write</p>
					<p><b><i>force = mass x acceleration</i></b></p>
				
					<p><b><i>F = ma</i></b></p>
					<p>So, greater acceleration means greater force.</p>
					<p>Since Newton&#146;s second law is a general law, it also applies to the force felt by the tip of the tine of our tuning fork.</p>
					<p>Think about how the <i>force</i> on the tine depends on how far from rest the tine is displaced. If you move it a little to the left, then a little restorative force, bringing the tine back to rest, is felt moving the tine to the right. Similarly, if the tine is displaced a lot, then there is a big force felt trying to move it back the other way. It&#146;s not too hard to believe that in general, the force felt is proportional to the displacement, but in the opposite direction. We also need to account for the fact that different tuning forks may have different stiffness. So, we write</p>

					<p><b><i>Force = &ndash;tuning fork constant x displacement</i></b></p>

					<p><b><i>F = &ndash;kx</i></b></p>
					<p>The minus sign indicates that the force is in the direction opposite to the displacement. The symbol <i>k</i> will be a number that depends on the tuning fork. The higher the value, the stiffer the fork and the faster it vibrates.</p>
					<p>So, since force equals force, we end up with the following equation.</p>
					<p><b><i>ma = &ndash;kx</i></b></p>
					<p>This is in fact a famous equation, a type of differential equation that is the basic harmonic oscillator. What it says is that for something like the tine on a tuning fork the acceleration experienced by the tine is related to its displacement. Remember that both the displacement of the tine and its acceleration are functions of time. So, a solution to this differential equation is a function that describes the motion of the tine and relates the position and acceleration as in the equation. It turns out that if you have an object whose displacement is a sine function (meaning that at time <i>t</i>, the displacement is sin(<i>t</i>)), then you can solve for its acceleration at time <i>t</i> (we&#146;ll see that it is described by &ndash;sin(<i>t</i>)) so that it satisfies the harmonic oscillator for the values <i>k</i>=1 and <i>m</i>=1. The general solution is still a sine function, but with a different frequency. In fact, it is</p>
					<p><img height="31" width="129" src="../../images/chapter1/1_4_xtra_bit.gif"></p>
				</td>
			</tr>
		</table>


		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>

