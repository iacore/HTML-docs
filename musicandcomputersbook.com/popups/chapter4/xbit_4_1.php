<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>Computer Code for Generating an Array of a Sine Wave</h1>
					<p>Here&#146;s some simple computer code (in the C language) for generating an array with a 512 entry sine wave in it:</p>
					<pre>
#define TABLE_SIZE 512

#define TWO_PI (3.14159 * 2)

float samples [TABLE_SIZE];

float phaseIncrement = TWO_PI/TABLE_SIZE;

float currentPhase = 0.0;

int i;

for (i = 0; i &lt; TABLE_SIZE; i ++){

samples[i] = sin(currentPhase);

currentPhase += phaseIncrement;

}
					</pre>
      <p>What can you say about the frequency of the sine wave computed above? 
        (Hint: You need to know the <i>sampling rate,</i> or how many samples 
        we&#146;re computing per second; typically, that might be 44.1 kHz.) How 
        about the amplitude?&#8212;how would you take the code above and change 
        the amplitude? Change the frequency? Maybe distort the sine wave?</p>
					<p>How much do you remember from geometry class? Or better yet, how much do you remember from Chapter 3, where we talked about <i>phasors</i>?</p>
					<p>What&#146;s TWO_PI doing in there? Try to figure out what&#146;s going on in this code! What would happen, for example, if you changed sin(currentPhase) to sin(currentPhase)/2, or 2*sin(currentPhase)? If you know a programming language, try writing a program that does a graphical printout of the wave.</p>
				</td>
			</tr>
		</table>
		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
