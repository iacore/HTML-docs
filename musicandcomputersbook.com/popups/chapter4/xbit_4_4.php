<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>Spectral Formula of a Waveform</h1>
					<p>For the curious: the spectral formula of a waveform gives a simple relationship between the partial number and its amplitude (remember, we know its frequency from the Fourier theorem).</p>
					<p>For example, the formula for a square wave is an amplitude of 1/<i>n</i>, 
        when <i>n</i> is odd, or 0, when <i>n</i> is even.</p>
					<p>Using the formula, we can compute the following partial strengths for a square wave:</p>
					<blockquote>
						<p>1st partial = 1/1</p>
						<p>2nd partial = 0</p>
						<p>3rd partial = 1/3</p>
					</blockquote>
					<p>and so on.</p>
					<p>There are some other simple waveforms whose formulae are well known:</p>
<blockquote>
						<p>Sawtooth wave: 1/<i>n</i> when <i>n</i> is even or odd.</p>
						
      <p>Triangle wave: 1/<i>n</i><sup>2</sup> when <i>n</i> is odd, 0 when <I>n</I> 
        is even. (Note how &quot;fast&quot; the amplitudes of a triangle wave 
        drop off&#151;it&#146;s said to be a &quot;not very bright&quot; timbre.)</p>
</blockquote>

					
      <p> Surprisingly, as we mentioned in Section 1.4, <i>symmetrical</i> waveforms 
        (like the square and triangle) have no even partials.</p>
				</td>
			</tr>
		</table>
		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>

