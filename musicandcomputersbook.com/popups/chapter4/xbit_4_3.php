<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>Early Computer Network Ensembles</h1>
					<p>In the 1970s and 1980s the San Francisco Bay Area and &quot;Silicon Alley&quot; were home to thriving communities of inventors, engineers, designers, and hackers who created much of the computing technology that we now take for granted. Companies like Apple Computer and Microsoft got their start as small, independent projects driven primarily by the enthusiasm and curiousity of their founders. That same spirit lead many composers in the area to investigate the use of rather primitive early home computers (&quot;microcomputers&quot;) as a means of making new kinds of music.</p>
					<p>Improvisational performing ensembles of various sorts (free jazz, gamelan, experimental rock) were very popular at the time. So it's not surprising that some intrepid computer musicians not only designed, built, and programmed their own computers, but they also figured out how to make them talk to one another in order to form computer music bands! The first such ensemble was The League of Automatic Music Composers (David Behrman, John Bischoff, Rich Gold, Jim Horton, Tim Perkis), which was followed later by The Hub (John Bischoff, Chris Brown, Scot Gresham-Lancaster, Tim Perkis, Phil Stone, Mark Trayle)</p>
					<p>According to Chris Brown, The Hub was &quot;an ensemble that explored a new genre called &quot;Computer Network Music&quot;. The six composer/performers of The Hub electronically coordinated the activity of their individual systems through a central microcomputer, &quot;the hub&quot; itself, as well as manually through ears, eyes, and hands. The Hub sought surprise through the lively and unpredictable responses&nbsp; of their systems, and instead of trying to eliminate the imperfect human performer, used the electronic tools available to enhance the social aspect of music making.&quot; (Source: <a href="http://www.cbmuse.com/collaborations/">http://www.cbmuse.com/collaborations/</a>)</p>
					<p>Individual members of The Hub would compose works for the group, often by writing up a set of informal instructions or guidelines for responding to one another's actions (or having their computers respond to the other computers' actions). Several wonderful examples can be found at:</p>
					<p><a href="http://www.o-art.org/history/08_Computer/Hub">http://www.o-art.org/history/08_Computer/Hub</a></p>
					<p>including a description of the piece &quot;Hertz Donut&quot; by Phil Stone that features the immortal lines: &quot;We will have to make sure we are in tune. I am deeply sorry about this.&quot;&nbsp;</p>
					<p>The advent of the Internet, standardized data protocols, and &quot;plug and play&quot; interfaces have all made it much easier to exchange data between computers, not only in the same room, but around the world. Internet-based performances with musicians in different cities are now common, and even live video collaborations are not too hard to set up. But in the early days of computer music, getting&nbsp; home-made computers and electronic circuitry to communicate was quite a challenge, which makes the accomplishments of these early computer network ensembles even more impressive. Not only did they overcome tremendous technical hurdles, but they also managed to create some truly sophisticated, enduring, and fun music while they were at it!&nbsp;</p>
					<p>For a terrific introduction to computer network bands and the experimental music scene in the Bay Area in the 1970s and 1980s, see Chris Brown and John Bischoff's article &quot;Indigenous to the Net: Early Network Music Bands in the San Francisco Bay Area&quot; at: <a href="http://crossfade.walkerart.org/brownbischoff">http://crossfade.walkerart.org/brownbischoff</a></p>
				</td>
			</tr>
		</table>
		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
