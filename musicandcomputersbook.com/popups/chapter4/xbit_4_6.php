<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1><nobr>Human Formant Manipulation&#151;</nobr></h1>
      <h2>Throat Singing, Trumping</h2>
					<p>Formant manipulation isn&#146;t limited to just computer speech geeks&#151;the famous Tuvan throat singers have been doing it for generations! By carefully controlling the shape of their vocal tracts, these singers are able to precisely pinpoint and emphasize certain formants in their voices.</p>
					
      <p>Another example is the instrument known as the &quot;jaw harp&quot; or 
        &quot;trump&quot; (as they call it in Scotland). By twanging a flexible 
        steel (or in some cases bamboo) tine held in place between the teeth and 
        moving the vocal cavity, folk musicians (actually, in Europe it&#146;s 
        a classical instrument as well) all over the world have been doing formant 
        modulation for centuries. American musicians, like Mike Seeger, have been 
        able to play complicated fiddle tunes on this tiny but amazing instrument 
        that operates in more or less the same way (acoustically) as throat singing.</p>
				</td>
			</tr>
			<tr>
				<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td colspan="2" valign="top">
								<hr>
							</td>
						</tr>
						<tr>
							<td align="center" valign="top" nowrap width="1%">
								<p class="icon_caption"><a onclick="setSrc('../../sounds/chapter1/CFDSound2.mp3')"><img src="../../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
									<b>Soundfile 1</b></p>
							</td>
							<td valign="top">
								<p class="applet_caption">
									Tuvan throat singing</p>
							</td>
						</tr>
						<tr>
							<td colspan="2" valign="top">
								<hr>
							</td>
						</tr>
						<tr>
							<td align="center" valign="top" nowrap width="1%">
								<p class="icon_caption"><a onclick="setSrc('../../sounds/chapter4/kurTwang.mp3')"><img src="../../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
									<b>Soundfile 2</b></p>
							</td>
							<td valign="top">
								<p class="applet_caption">
									Jaw harp</p>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center" valign="top" nowrap>
								<hr>
							</td>
						</tr>
					</table>
				</td>
			</tr>

		</table>

		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
