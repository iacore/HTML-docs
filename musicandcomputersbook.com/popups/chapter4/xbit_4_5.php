<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1><b>Resonances</b></h1>
					<p>Noted computer music researcher Perry Cook points out that there are almost no examples of actual musical instruments that change their resonance during the course of a note, one of the fundamental ways in which voices differ from instruments.</p>
					
      <p>In other words, the <i>shifting formant regions</i> of a voice are quite 
        unusual in the musical world. Some of the few examples we can think of 
        are the electric guitar wah-wah pedal, the trumpet with a plunger mute, 
        the famous Leslie organ speaker (which rotates mechanically), and a certain 
        kind of North Sumatran open-backed lute called a hasapi, which the player 
        rotates against her stomach to produce different timbres. Naturally, the 
        only picture we have is of the obscure North Sumatran lute. This is the 
        back of the instrument, with the hole for changing the resonance against 
        the belly.</p>
					<p>Can you think of some other instruments where the pitch is constant but the resonance changes in time?</p>
					<p class="figure"><img height="240" width="320" src="../../images/chapter4/hasapi.back.small.JPG"></p>
					
      <p class="figure_caption"><b>Figure 1</b>&nbsp;&nbsp;Picture of the back 
        of a hasapi, a two-stringed open-backed lute from the Batak of North Sumatra, 
        Indonesia, that is often used for trance music.</p>
				</td>
			</tr>
		</table>
		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
