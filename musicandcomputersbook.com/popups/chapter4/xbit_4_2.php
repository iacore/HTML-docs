<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>Composer Robert Marsanyi</h1>
					<p>Composer Robert Marsanyi took an entirely different approach to sound synthesis in his pieces &quot;Lurch&quot; and &quot;Study for Lurch.&quot; He designed the pieces to run on, and exploit the properties of, a specific piece of hardware, the Motorola 56001 DSP (digital signal processing) chip.</p>
					
      <p>One very interesting aspect of Marsanyi&#146;s pieces is that the hardware 
        generates its own instructions, which generate numbers, which are then 
        interpreted as sound data. There is nothing intrinsically musical or sound-related 
        about the numbers&#151;they are simply the result of a process influenced 
        by the nature of the hardware. Marsanyi explains that &quot;the piece 
        is based on the <i>instrument</i>.&quot;</p>
					<p>According to Marsanyi:</p>
					<blockquote>
						<p>Rather than invoke levels of abstraction between what the hardware is doing and what the compositional environment is (for example, using unit generators to create complex &quot;instruments&quot; to be played by &quot;scores&quot;), I choose to explore the model that the hardware invokes. In this case, the model is below the level of sound, in a way; a behavior built around audio interrupts: there is a finite time in which the processor can work, after which it must produce a single number representing the total audio experience at that point in micro-time. It&#146;s like writing an orchestral piece, with the instructions from the conductor &quot;talk amongst yourselves for a couple of minutes, figure out what you&#146;re going to play, and when I wave my baton, I want a single sound from all of you.&quot;</p>
						
        <p>Another aspect of the hardware is the direct correspondence of computer 
          code and sound: with this instrument, you&#146;re not manipulating parameters 
          of some sound-making device anymore&#151;you&#146;re dealing directly 
          with the sound itself. Parameters like pitch, amplitude, or location 
          become emergent properties. Instead of interacting with a piece by controlling 
          a set of parameters, the idea here is to narrow the communication channel 
          between the performer and the piece to a single time-varying degree-of-freedom, 
          that of like or dislike. &quot;Study for Lurch&quot; is a piece for 
          performance, not for listening to from a recording (that is, the composition 
          is structured not so much in terms of how the piece sounds, which is 
          what a recording captures, but in the logic used to interact with a 
          performer). The performer responds to the soundfield being heard by 
          moving a MIDI continuous controller, with large numbers meaning &quot;good&quot; 
          and small numbers meaning &quot;bad.&quot;</p>
					</blockquote>
				</td>
			</tr>
		</table>
		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
