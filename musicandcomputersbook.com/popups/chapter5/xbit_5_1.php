<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
					<td>
						<h1>&quot;Slow Motion Sound&quot;</h1>
						<div align="center">
						<img height="470" width="402" src="../../images/chapter5/reich.jpg">
						</div>
					<p class="figure_caption"><b>Figure W.1&nbsp;&nbsp;</b>Steve Reich&#146;s &quot;Slow Motion Sound,&quot; from his book <i>Writings About Music.</i> In 1967, Reich considered this to be a &quot;conceptual&quot; score, since the technology wasn&#146;t commonly available to do it (except for a very few tape recorders with a &quot;rotating playback head&quot;).</p>
						<p>Here are Steve Reich&#146;s notes for &quot;Slow Motion Sound&quot; (1967).</p>
					<blockquote>
						
        <p><i>Slow Motion Sound</i> (1967) has remained a concept on paper because 
          it was technologically impossible to realize. The basic idea was to 
          take a tape loop, probably of speech, and ever so gradually slow it 
          down to enormous length without lowering its pitch. In effect it would 
          have been like the true synchronous sound track to a film loop gradually 
          presented in slower and slower motion.<br>
							<br>
							The roots of this idea date from 1963 when I first became interested in experimental films, and began looking at film as an analog to tape. Extreme slow motion seemed particularly interesting since it allowed one to see minute details that were normally impossible to observe. The real moving image was left intact with only its tempo slowed down.<br>
							<br>
							Experiments with rotating head tape recorders, digital analysis and synthesis of speech, and vocoders all proved unable to produce the gradual yet enormous elongation, to factors of 64 or more times original length, together with high fidelity speech reproduction, which were both necessary for musical results.<br>
							<br>
							The possibility of a live performer trying to speak incredibly slowly did not interest me since it would be impossible, in that way, to produce the same results as normal speech, recorded, and then slowed down.<br>
							<br>
							I was able to experiment with a tape loop of a little African girl learning English by rote from an African lady schoolteacher in Ghana. The teacher said, &quot;My shoes are new&quot;, and the little girl repeated, &quot;My shoes are new.&quot; The musical interest lay in the speech melody which was very clearly [the pitches] e', c#', a, b [' means the higher octave of the pitch] both when the teacher spoke and when the little girl responded. Since African languages are generally tonal, and learning the correct speech melody is as necessary for understanding as is the correct word, this was simply carried over into the teaching of English. The loop was slowed down on a vocoder at the Massachusetts Institute of Technology to approximately 10 times its original length, in gradual steps, without lowering its pitch. Though the quality of speech reproduction on the vocoder was extremely poor it was still possible to hear how &quot;My&quot;, instead of merely being a simple pitch, is in reality a complex glissando slowly rising from about c# up to e', then dissolving into the noise band of &quot;sh&quot;, to emerge gradually into the c#' of &quot;oe&quot;, back into the noise of &quot;s&quot;, and so on.<br>
							<br>
							Slowing down the motion in musical terms is augmentation; the lengthening of duration of notes previously played in shorter note values. Though <i>Slow Motion Sound</i> was never completed as a tape piece, the idea of augmentation finally realized itself in my music as <i>Four Organs</i> of 1970, and then again in <i>Music for Mallet Instruments, Voices and Organ</i> of 1973.<br>
							<br>
          Though by now I have lost my taste for working with complex technology 
          I believe a genuinely interesting tape piece could still be made from 
          a loop of speech, gradually slowed down further and further, while its 
          pitch and timbre remain constant.</p>
					</blockquote>
			</td>
				</tr>
		</table>
		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>

