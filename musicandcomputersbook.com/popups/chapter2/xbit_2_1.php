<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>A Free Sample</h1>
					<p>A moving tonsorial tale by our Dan</p>
					<p>(Used with the kind permission of Vermont Public Radio)</p>
					<p>I went to get my hair cut the other day. As always, I entrusted my curls to the woman who has been cutting my hair almost from the time I first moved to here, to the Upper Connecticut River Valley. As I settled back into the chair, we started chatting, getting caught up on the summer&#146;s events and plans for the fall.</p>
					<p>As I left the salon I thought about the ongoing 7-year conversation that she and I have been having, made up of 20 minute samples, taken every 8 weeks or so, and the way in which she knows me through these <i>snippets</i> of information.</p>
					<p>We usually share the big picture with one another. She knows of things like loves won and lost, family illnesses and happiness, my travels, and how I feel about these things. In these exchanges the little things are clipped away&#151;there is no mention of what I had for dinner last Thursday or my frustration at having no clean socks yesterday. But nevertheless, even without the day-to-day minutiae, from these many snippets, or <i>periodic samples,</i> I&#146;d say that she has a pretty good idea of who I am.</p>
					<p>If I were a piece of music, I&#146;d say that what she knows are my fundamentals, the big themes around which my life seems to vibrate and move. She knows the rhythm and the beat. What remains harder to predict or know is the variation from these themes, the trilled notes that get played over the bass line. I think, I hope, she knows I&#146;m generally a good guy, but how that plays into every little event in my life would still be hard for her to predict.</p>
					<p>This is the sort of problem that we all deal with every day. Anyone&#146;s life is a continuous process, but when we explain things, we can really only relate a sequence of discrete events or <i>samples.</i> The problem is relating enough samples, recalled at a high enough frequency, so that we allow the listener to hear as truthful a representation as is necessary. In the world of mathematics, this is part of something called sampling theory, and it plays a big role in the world of engineering, especially when reconstructing sound or music on your compact disc player.</p>
					<p>Sound is a great example of a continuous process. It is a type of wave motion, much like the waves that you see when a pebble is tossed into a pond. Similarly, sound waves are initiated by things like banging on a drum. Once the drumhead is hit, the molecules nearby start bouncing around, banging into more nearby molecules and so on and so on, and soon you&#146;ve got a bunch of molecules bouncing against your own eardrum, and it&#146;s the constant up and down of your eardrum that is the continuous process of sound.</p>
					<p>Snare drums, bass drums, eardrums, it doesn&#146;t matter: the waves that move on any of these are actually combinations of many simple kinds of waves called sine waves or, more generally, sinusoids. The sinuous sinusoids are characterized by their frequencies, which are basically the number of times the drumhead bounces up and down each second. Low-frequency sinusoids are characteristic of the bass tones, while high-frequency ones are the sharper treble tones. By emphasizing certain frequencies and de-emphasizing others, you can get an infinite number of different kinds of sounds.</p>
					<p>Now, making noise is easy, but recording it on a CD is a much more complicated matter. You see, CDs are really just long lists of numbers representing how far your eardrum has moved at each instant, and the interesting problem is to figure out how often you need to take samples of a sound so you get an accurate representation. Some time ago, a mathematician named Nyquist figured out that you need to take samples at a rate that is at least two times as fast as the highest frequency found in the original sound. For example, since humans really only hear frequencies that are less than 20,000 waves per second, most sound is sampled at a rate of well over 40,000 times per second (actually, for reasons a bit difficult to explain, at 44,100 times per second).</p>
					<p>Reproducing sound is one thing, but what is the right sampling rate for hearing the true me? I don&#146;t know, but it&#146;s probably more than eight or nine haircuts a year. After all, a new <i>wave</i> guy like me does march to the beat of a different drummer, so you may need a few more of my clippings to be on the cutting edge... .</p>
				</td>
			</tr>
		</table>


		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
