<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>The Peanut Butter Conundrum</h1>
					<p>One of the authors (Polansky) has proposed, for pedagogical reasons, a kind of metaphorical example to illustrate the ethical dilemma presented by virtually unlimited and unrestricted sonic information (not to mention software). He calls it the &quot;peanut butter conundrum.&quot; </p>
					<p>Let&#146;s suppose we have an unlimited supply of peanut butter. It&#146;s sold in the usual way in grocery stores, but it turns out to be cheaper for store owners to place it out in front of the store, unguarded. Stealing it is illegal and immoral, but because it&#146;s plentiful, there is no penalty for doing so. Nor is there danger of getting caught (it&#146;s left out on the street every night in plain sight). You can have it, there&#146;s plenty more, and there are no consequences. But it&#146;s still wrong! People are requested not to steal it&#151;if everyone stole it, peanut butter companies would go out of business and grocery stores would suffer.</p>
					<p>We are in a similar situation with respect to cyber-piracy, in terms of both sound and software. We can take as much as we please, and for all practical purposes we can&#146;t be prosecuted. Software protection is broken as fast as it is created. In at least one electronic music graduate school we know of (not our own!), students, with full knowledge of their professors, crack software (mostly programs they would like to use) as a kind of unofficial extracurricular activity in their music technology education. </p>
					<p>There&#146;s no question that piracy is wrong, but it&#146;s also useless to explain this to hackers and artists who bristle at the very notion of software protection. In many cases they themselves are churning out innovative, high-quality public-domain software. They see themselves as cyber&#150;Robin Hoods and have little sympathy for complicated protection schemes that make their own work (educational and artistically oriented) difficult for the sake of being difficult (since they could not afford to buy the stuff to begin with!).</p>
				</td>
			</tr>
		</table>
		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
