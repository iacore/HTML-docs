<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>MatLab Code to Plot Amplitude Envelopes</h1>
					<p>This Xtra bit shows the computer code, written in the widely used language MatLab, for making the two examples of amplitude envelopes in the text. A lot of the examples in this book were made using MatLab, but we&#146;ve used a number of other software programs as well. We&#146;re including this to give you a look &quot;under the hood&quot; at how one goes about making these kinds of pictures. Try to read through it to get some idea of how one codes in MatLab.</p>
					<p>Comments are preceded by the percent sign (%). The code itself is in <b>bold</b>.</p>
					<hr>
<pre>
% MatLab Code:
% Take the average and peak envelopes of an input signal, and plot all three
% together.
% Expects a .wav soundfile as input.
% usage: ampenv('myfile.wav')

% This beginning part just defines the function to be used in MatLab: takes a 
% &quot;wav&quot; sound file as an input, and spits out a graph.

<b>function [ampenv] = ampenv( file );</b>
<b>clf</b> % Clears the graphic screen.

% Reads in the sound file, into a big array called y.

<b>y = wavread( file );</b>

% Normalize y; that is, scale all values to its maximum. Note how simple it is 
% to do this in MatLab.

<b>y = y/max(abs(y));</b>

% If you want to hear the sound after you read it in, uncomment this next line. 
% <i>Sound</i> just plays a file to the speaker.

%<i>sound(y, 44100);</i>

% Initialize peak and average arrays to be the same as the signal. We&#146;re 
% keeping three big arrays, all of the same length.

<b>averageenv = y;						
maxenv = y;</b>

% Set the window lengths directly, in number of samples. Generally, the average 
% should be smaller than the peak, since it will tend to flatten out if it gets 
% too big. These two numbers can be played with to vary what the pictures will 
% look like. Note that we keep two different windowsizes so that we can have 
% different kinds of resolution for peak and average charts.

<b>averagewindowsize = 512;						
peakwindowsize = 1000;</b>

% Go through the input signal, taking an average of the previous, 
% averagewindowsize number of samples, and store that in the current place in
% the average array. We do this by having a loop that starts at the end of the
% first window and goes to the end of the sound file, indicated by 
% <i>length(averageenv).</i> The MatLab command <i>sum</i> takes some range of a 
% vector, and gives you back the sum.

<b>for k = averagewindowsize:length(averageenv)							
runningsum = sum(abs(y(k-averagewindowsize-1:k)));							
averageenv(k) = runningsum /averagewindowsize ;							
end</b>

% Go through the input signal, taking the maximum value of the previous 
% peakwindowsize number of samples. We do this in the same way as we did the 
% average, but now we use <i>max</i> instead of <i>sum.</i>

<b>for k = peakwindowsize:length(maxenv)							
maxenv(k) = max(y(k-peakwindowsize:k));							
end</b>

% Plot the three &quot;signals&quot;: the original in the color cyan, the 
% peak in magenta, and the average in blue. The command <i>hold on</i> invokes the 
% ghost of Sam and Dave to allow us to overprint on the current graph, so that 
% we can show all three lines together. The rest of this is just MatLab 
% graphics instructions (titles, labels, axes, etc.). The colors are the 
% letters in single quotes.

<b>plot(y, 'c')						
hold on
plot (maxenv, 'm')						
hold on
plot(averageenv, 'b')
title('Monochord: Signal, Average Signal Envelope, Peak Signal Envelope')
xlabel('sample number')
ylabel('amplitude (-1 to 1)')
legend('original signal', 'peak envelope', 'running average envelope', 0)</b>
</pre>
				</td>
			</tr>
		</table>
		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>

