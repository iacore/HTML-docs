<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>The FFT: Real, Imaginary, Magnitude, Phase</h1>
					<p>We&#146;ve mentioned that most FFTs return their information in the form of a complex pair: a <i>real</i> part and an <i>imaginary</i> part. There&#146;s obviously a lot more to say about that, but it gets pretty mathematical, and we&#146;ll let you explore that on your own.</p>
					<p>But we&#146;ve been talking about the data that an FFT returns more in terms of the <i>amplitude</i> (or magnitude) and <i>phase</i> of a given frequency bin. What is important in this discussion is that you understand more or less how to get from the complex number pair to the magnitude/phase pair, which is generally, for computer music, more useful. You&#146;ll probably want to manipulate amplitudes and phases, not complex numbers.</p>
					<p>It turns out not to be so hard. Remember that an imaginary number, written in the form: <b>a + bi</b>, can be represented in a two-dimensional space as follows:</p>
					<p class="figure"><img height="317" width="339" src="../../images/chapter3/magphase.jpeg"></p>
					<p class="figure_caption"><b>Figure 1</b></p>
					<p>The length of the line from the origin to the point (a, b) is the magnitude specific to the spectral bin; that is, it&#146;s the amount of energy at that frequency. It&#146;s just the hypotenuse of the right triangle drawn to the <i>x</i>-axis. The equation for it is very simple:</p>
					<p><img height="26" width="97"  src="../../images/chapter3/3_4_xtra_bit.gif"></p>
					
      <p>You might recognize that as the equation for the length of the hypotenuse 
        of a right triangle (called the <i>Pythagorean theorem</i>)<i>.</i> </p>
					<p>That was pretty easy, right? Deriving the <i>phase</i> from the complex pair is a little trickier. What we want to find is the angle made by that point to the <i>x</i>-axis. Remember that phases are expressed in radians, 0 to <nobr>2<img src="../../images/shared/pi.gif" height="10" width="12"></nobr>, and that a complete circle is <nobr>2<img src="../../images/shared/pi.gif" height="10" width="12"></nobr>. We call that angle <img src="../../images/shared/theta.gif" height="14" width="10">. So, if we can find <img src="../../images/shared/theta.gif" height="14" width="10"> in radians, we&#146;re done: we have the phase of that bin.</p>
					<p>It turns out that we&#146;re looking for an angle that is the ratio of the opposite side to the adjacent side (remember: SOH CAH TOA: <i><span style="text-decoration: underline">s</span>in,</i> <span style="text-decoration: underline">o</span>pposite over <span style="text-decoration: underline">h</span>ypotenuse; <i><span style="text-decoration: underline">c</span>osine,</i> <span style="text-decoration: underline">a</span>djacent over <span style="text-decoration: underline">h</span>ypotenuse; <i><span style="text-decoration: underline">t</span>angent,</i> <span style="text-decoration: underline">o</span>pposite over <span style="text-decoration: underline">a</span>djacent). We want the <i>angle</i> whose tangent is the <i>y</i>-value (opposite) over the <i>x</i>-value (adjacent), or <b>b/a</b>. That&#146;s called the <i>arctangent,</i> and is written as <b>tan<sup>-1</sup></b>. So the equation for calculating phase is:</p>
					<p><img src="../../images/shared/theta.gif" height="14" width="10"> = tan<sup>- 1</sup> (a/b)</p>
					<p>There, that wasn&#146;t so painful, was it?</p>
				</td>
			</tr>
		</table>

		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
