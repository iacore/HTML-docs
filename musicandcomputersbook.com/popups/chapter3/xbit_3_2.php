<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../../index.html"><img src="../../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h2>History of the FFT</h2>
					<p>(Note: This is a somewhat technical history, an excerpt from a scholarly article written by one of our authors.)</p>
					<p>The first appearance of the FFT, like so much of mathematics, can be traced back to <i>Gauss</i> (see M. T. Heideman, D. H. Johnson, and C. S. Burrus, &quot;Gauss and the history of the fast Fourier transform,&quot; <i>Archive for History of Exact Sciences</i> 34 (1985), no. 3, 265-277). Gauss&#146;s interests were in certain astronomical calculations (a recurrent area of application of the FFT) having to do with the interpolation of asteroidal orbits from a finite set of equally spaced observations. Surely the prospect of a huge laborious hand calculation was good motivation for the development of a fast algorithm. Fewer calculations also imply less opportunity for error, and hence are also more numerically stable! Gauss observes that a Fourier series of bandwidth N = N1 N2 can be broken up into a computation of the N2 subsampled DFTs of length N1 which are combined as N1 DFTs of length N2. Gauss&#146;s algorithm was never published outside of his collected works.</p>
					<p>A less general but still important version of the FFT, used for the efficient computation of the Hadamard and Walsh transforms, was first published in 1932 by the statistician Yates (F. Yates, &quot;The design and analysis of factorial experiments,&quot; <i>Imperial Bureau of Soil Science Technology Committee</i> 35 (1937)). Yates&#146;s &quot;interaction algorithm&quot; is a fast technique for computing the analysis of variance for a 2n-factorial design, and is described in almost any text on statistical design and analysis of experiments.</p>
					<p>Another important predecessor is the work of Danielson and Lanczos (G. C. Danielson and C. Lanczos, &quot;Some improvements in practical Fourier analysis and their application to X-ray scattering from liquids,&quot; <i>Journal of the Franklin Institute</i> 233 (1942), no. 4 and 5, 365-380 and 432-452), performed in the service of X-ray crystallography, another frequent user of FFT technology. Their &quot;doubling trick&quot; showed how to reduce a DFT in 2N points to two DFTs on N points using on N extra operations. Today it brings a smile to our faces as we note their problem sizes and timings: &quot;Adopting these improvements the approximate times for Fourier analysis are 10 minutes for 8 coefficients, 25 minutes for 16 coefficients, 60 minutes for 32 coefficients, and 140 minutes for 64 coefficients.&quot; This indicates a running time of about .37N logN minutes for an N point DFT!</p>
					<p>Despite these early discoveries of an FFT, it wasn&#146;t until Cooley and Tukey&#146;s article (J. W. Cooley and J. W. Tukey, &quot;An algorithm for machine calculation of complex Fourier series,&quot; <i>Mathematical Computation</i> 19 (1965), 297-301) that the algorithm gained any notice. The story of their collaboration is an interesting one. Tukey arrived at the basic reduction while in a meeting of President Kennedy&#146;s Science Advisory Committee where among the topics of discussion were techniques for off-shore detection of nuclear tests in the Soviet Union. Ratification of a proposed United States/Soviet Union nuclear test ban depended upon the development of a method for detecting the tests without actually visiting the Soviet nuclear facilities. One idea was to analyze seismological time series obtained from off-shore seismometers, the length and number of which would require fast algorithms for computing the DFT. Other possible applications to national security included the long-range acoustic detection of nuclear submarines.</p>
					<p>Richard Garwin of IBM was another of the participants at this meeting, and when Tukey showed him this idea he immediately saw a wide range of potential applicability and quickly set to getting this algorithm implemented. He was directed to Cooley and, needing to hide the national security issues, instead told Cooley that he wanted the code for another problem of interest: the determination of the periodicities of the spin orientations in a 3-D crystal of He3. Cooley had other projects going on, and only after quite a lot of prodding did he sit down to program the &quot;Cooley-Tukey&quot; FFT. In short order, Cooley was published almost instantaneously (in 6 months!). This publication, as well as Garwin&#146;s fervent proselytizing, did a lot to help publicize the existence of this (apparently) new fast algorithm.</p>
					<p>The timing of the announcement was such that usage spread quickly. The roughly simultaneous development of analog to digital converters capable of producing digitized samples of a time-varying voltage at rates of 300,000 samples per second had already initiated something of a digital revolution and was also providing scientists with heretofore unimagined quantities of digital data to analyze and manipulate (just as is the case today!). Even the &quot;standard&quot; applications of Fourier analysis as an analysis tool for waveforms or solving PDEs (partial differential equations) meant that a priori there would be a tremendous interest in the algorithm. But even more, the ability to do this analysis quickly allowed scientists from new areas to try the DFT without having to invest too much time and energy in the exercise. I can do no better than to quote the introduction to the FFT from <i>Numerical Recipes</i> (a standard work on computer algorithms): &quot;If you speed up any nontrivial algorithm by a factor of a million or so the world will beat a path towards finding useful applications for it.&quot; (See W. H. Press, B.P. Falnnery, S. A. Teukolsky, and W. T. Vettering, <i>Numerical recipes in C: The art of scientific computing,</i> Cambridge University Press, New York, 1988.)</p>
				</td>
			</tr>
		</table>



		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form> 
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../../_components/audioPlayerHelper.js'></script>
	</body>

</html>
