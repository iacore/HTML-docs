<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="01_02.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="8" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 1: The Digital Representation of Sound,<br>
						Part One: Sound and Timbre</h1>
					<h1>Section 1.1: What Is Sound?<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
						<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2426A14'));return CSClickReturn()" href="../applets/1_1_hear_sine.php" target="_blank" csclick="BCE2426A14"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 1.1</b><br>
						
						Hearing a sine wave oscillator</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>Sound is a complex phenomenon involving physics and perception. Perhaps the simplest way to explain it is to say that sound involves at least three things:</p>
					<ul>
						<li>something moves
						<li>something transmits the results of that movement
						<li>something (or someone) hears the results of that movement (though this is philosophically debatable)
					</ul>
					<p>All things that make sound move, and in some very metaphysical sense, all things that move (if they don&#146;t move too slowly or too quickly) make <i>sound. </i></p>
					<p>As things move, they &quot;push&quot; and &quot;pull&quot; at the surrounding air (or water or whatever medium they occupy), causing pressure variations (compressions and rarefactions). Those pressure variations, or <i>sound waves</i>, are what we hear as sound.</p>
					<p>Sound is often represented visually by figures, as in Figures 1.1 and 1.2.</p>
					<p class="figure_flush"><img height="223" width="580" src="../images/chapter1/elmowave.jpg"></p>
					<p class="figure_caption"><b>Figure 1.1&nbsp;&nbsp;</b>Most sound waveforms end up looking pretty much alike. It&#146;s hard to tell much about the nature of this sound from this sort of <i>time-domain</i> plot of a sound wave.<br>
						<br>
						This illustration is a plot of the immortal line &quot;I love you, Elmo!&quot; Try to figure out which part of the image corresponds to each word in the spoken phrase.</p>
					<p class="figure_flush"><img height="178" width="570" src="../images/chapter1/zoomin.jpg"></p>
					<p class="figure_caption"><b>Figure 1.2&nbsp;&nbsp;</b>Minute portion of the sound wave file from Figure 1.1, zoomed in many times.<br>
						<br>
						In this image we can see the almost sample-by-sample movement of the waveform (we&#146;ll learn later what samples are). You can see that sound is pretty much a symmetrical type of affair&mdash;compression and rarefaction, or what goes up usually comes down. This is more or less a direct result of Newton&#146;s third law of motion, which states that for every action there is an equal and opposite reaction.</p>
					<p>Figures 1.1 and 1.2 are often called <i>functions.</i> The concept of function is the simplest glue between mathematical and musical ideas.</p>
					<h2>Sound as a Function</h2>
					<p>Most of you probably have a favorite song, something that reminds you of a favorite place or person. But how about a favorite <i>function</i>? No, not something like a black-tie affair or a tailgate party; we mean a favorite <i>mathematical</i> function.
					<p>In fact, songs and functions aren&#146;t so different. Music, or more generally sound, can be described as a function.</p>
					<p>Mathematical functions are like machines that take in numbers as raw material and, from this input, produce another number, which is the output.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2427915'));return CSClickReturn()" href="../popups/chapter1/xbit_1_1.php" target="_blank" csclick="BCE2427915"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 1.1</b><br>
						
						Sonification</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>There are lots of different kinds of functions. Sometimes functions operate by some easily specified rule, like squaring. When a number is input into a squaring function, the output is that number squared, so the input 2 produces an output of 4, the input 3 produces an output of 9, and so on. For shorthand, we&#146;ll call this function <i>s</i>.</p>

					<p><i>s</i>(2) = 2<sup>2</sup> = 4<br>
					<i>s</i>(3) = 3<sup>2</sup> = 9<br>
					<i>s</i>(<i>x</i>) = <i>x</i><sup>2</sup></p>
					<p>The last expression is really just an abbreviation that says for any number given as input to <i>s</i>,  the number squared is the output. If the input is <i>x,</i> then the output is <i>x</i><sup>2</sup>.</p>
					<p>Sometimes the input/output relation may be easy to describe, but often the actual cause and effect may be more complicated. For example, review the following function.</p>
					<blockquote>
						<p>Assume there&#146;s a thermometer on the wall. Starting at 8&nbsp;<span class="smallcaps">A.M.</span>, for any number <i>t</i> of minutes that have elapsed since 8&nbsp;<span class="smallcaps">A.M.</span>, our function gives an output of the temperature at that time. So, for an input of 5, the output of our temperature function is the room temperature at 5 minutes after 8&nbsp;<span class="smallcaps">A.M.</span> The input 10 gives as output the room temperature at 10 minutes after 8&nbsp;<span class="smallcaps">A.M.</span>, and so on.</p>
					</blockquote>
					<p>Once again, for shorthand we can abbreviate this and call the function <i>f</i>.</p>
					
					<p><i>f</i>(5) = room temperature at 5 minutes after 8&nbsp;<span class="smallcaps">A.M.</span>						
					<br><i>f</i>(10) = room temperature at 10 minutes after 8&nbsp;<span class="smallcaps">A.M.</span>						
					<br><i>f</i>(<i>t</i>) = room temperature at <i>t</i> minutes after 8&nbsp;<span class="smallcaps">A.M.</span></p>
					
					<p>You can see how this temperature function is a little like our previous sound amplitude graphs. The easiest way to understand the temperature function is according to its <i>graph</i>, the picture that helps us visualize the function. The two axes are the input and output. If an input is some number <i>x</i> units from 0 and the output is <i>f</i>(<i>x</i>) units (which could be a positive or negative number), then we place a mark at <i>f</i>(<i>x</i>) units above <i>x.</i></p>
					<p>Assume the following:</p>
					<p>
					<i>f</i>(0) = 30						
					<br><i>f</i>(5) = 35						
					<br><i>f</i>(10) = 38</p>
					
					<p>Figure 1.3 shows what happens when we graph these three temperatures. (Note that we&#146;ll leave the <i>x</i>-axis in real time, but to be more precise we probably should have written 0, 5, and 10 there!) We&#146;ll join these marks by a straight line.</p>
					<p class="figure"><img height="260" width="266" src="../images/chapter1/temp.time.jpg"></p>
					<p class="figure_caption"><b>Figure 1.3</b></p>
					<p>So how do we get a <i>function</i> out of sound or music?</p>

					<h2>A Kindergarten Example</h2>
<!--					<table border="0" cellspacing="0" cellpadding="2" align="right">
						<tr>
							<td align="center">
								<p class="figure_caption_flush"><img height="100" width="106" src="../images/chapter1/bounce.gif" hspace="10" vspace="10"><br>
									<b>Animation 1.x</b></p>
							</td>
						</tr>
					</table>
-->
					
					<p>Imagine an entire kindergarten class piled on top of a trampoline in your neighbor&#146;s backyard (yes, we know this would be dangerous!). The kids are jumping up and down like maniacs, and the surface of the trampoline is moving up and down in a way that is seemingly impossible to analyze.</p>
					<p>Suppose that before the kids jump on the trampoline, we paint a fluorescent yellow dot on the trampoline and then ask the kids not to jump on that dot so that we can watch how it moves up and down. The surface of the trampoline is initially at rest. The class climbs on. We take a stopwatch out of our pocket and yell &quot;Go!&quot; while simultaneously pressing the start button. As the kids go crazy, our job is to measure at each possible instant how far the yellow dot has moved from its rest position. If the dot is <i>above</i> the initial position, we measure it as positive (so a displacement of 3 cm up is recorded as +3). If the displacement is <i>below</i> the rest position, we measure it as negative (so a displacement of 3 cm down is recorded as -3).</p>
					<p>So follow the bouncing dot! It rises, then falls, sometimes a lot, sometimes a little, again and again. If we chart this bouncing dot on a moving piece of paper, we get  the kind of function (of pressure, or <i>deformation</i> or <i>perturbation</i>) that we&#146;ve been talking about.</p>
					<p>Let&#146;s return to the idea of writing down a list of numbers corresponding to a set of times. Now we&#146;re going to turn that list into the graph of a mathematical function! We&#146;ll call that function <i>F</i>.</p>
					<p>On the horizontal line (the <i>x</i>-axis), we mark off the equally spaced numbers 1, 2, 3, and so on. Then we  mark off on the vertical axis (the <i>y</i>-axis) the numbers 1, 2, 3, and so on, going up, and -1, -2, -3, and so on, going down. The numbers on the <i>x</i>-axis stand for time, and on the <i>y</i>-axis the numbers represent displacement. If at time <i>N</i> we recorded a displacement of 4, we put a dot at 4 units above <i>N</i> and we say that <i>F</i>(<i>N</i>) = 4. If we recorded a displacement of -2, we put a dot at the position 2 units below <i>N</i> and we say <i>F</i>(<i>N</i>) = -2. Each of the values <i>F</i>(<i>N</i>) is called a <i>sample</i> of the function <i>F</i>.</p>
					<p>We&#146;ll learn later (in Section 2.1, when we talk about sampling a waveform) that this process of &quot;every now and then&quot; recording the value of a displacement in time is referred to as <i>sampling</i>, and it&#146;s fundamental to computer music and the storage of digital data. Sampling is actually pretty simple. We regularly inspect some continuous movement and record its position. It&#146;s like watching a marathon on television: you don&#146;t really need to see the whole thing from start to finish&#151;checking in every minute or so gives you a good sense of how the race was run.</p>
					<p>But suppose you could take a measurement at absolutely every instant in time&#151;that is, take these measurements <i>continuously.</i> That would give you a lot of numbers (infinitely many, in fact, because who&#146;s to say how <i>small</i> a moment in time can be?). Then you would have numbers above and below every point and get a picture something like Figures 1.1 and 1.2, which appear to be continuous.</p>
					<p>Actually, calling these axes <i>x</i> and <i>y</i> is not so instructive. It is better to call the <i>y</i>-axis &quot;amplitude&quot; and the <i>x</i>-axis &quot;time.&quot; The following examples let you play with the notion of pressures in time.</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<br>
						<a onclick="setSrc('../sounds/chapter1/words.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button26" border="0" alt=""></a><br>
						<b>Soundfile 1.1<br>
						</b>Speaking</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush"><a href="../sounds/chapter1/words.aiff.mp3"><img height="158" width="422" src="../images/chapter1/words.aiff.jpg" border="0"></a></p>
					<p class="figure_caption_flush"><b>Figure 1.4</b></p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<br>
						<a onclick="setSrc('../sounds/chapter1/gull.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button22" border="0" alt=""></a><br>
						<b>Soundfile 1.2<br>
						</b>Bird song</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush"><a href="../sounds/chapter1/gull.aiff.mp3"><img height="158" width="422" src="../images/chapter1/gull.aiff.jpg" border="0"></a></p>
					<p class="figure_caption_flush"><b>Figure 1.5</b></p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<br>
						<a onclick="setSrc('../sounds/chapter1/gam.clip.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button28" border="0" alt=""></a><br>
						<b>Soundfile 1.3<br>
						</b>Gamelan</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush"><a href="../sounds/chapter1/gam.clip.aiff.mp3"><img height="157" width="422" src="../images/chapter1/gamclip.jpg" border="0"></a></p>
					<p class="figure_caption_flush"><b>Figure 1.6</b></p>
					<p>When you <i>hear</i> something, this is in fact the end result of a very complicated sequence of events in your brain that was initiated by vibrations of your eardrum. The vibrations are caused by air molecules hitting the eardrum. Together they act a bit like waves crashing against a big rubber seawall (or those kids on the trampoline).</p>
					<p>These waves are the result of things like speaking, plucking a guitar string, hitting a key of the piano, the wind rustling leaves, or blowing into a saxophone. Each of these actions causes the air molecules near the sound source to be disturbed, like dropping many pebbles into a pond all at once. The resulting waves are sent merrily on their way toward you, the listener, and your eagerly awaiting eardrum. The corresponding function takes as input the number representing the time elapsed since the sound was initiated and returns a number that measures how far and in what direction your eardrum has moved at that instant. But what is your eardrum actually <i>measuring</i>? That&#146;s what we&#146;ll talk about next.</p>
					<h2>Amplitude and Pressure</h2>
					<p>In the graphs of sound waves shown in Figures 1.1 and 1.2, time was represented on the <i>x</i>-axis and amplitude on the <i>y</i>-axis. So as a <i>function,</i> time is the input and amplitude (or pressure) is the output, just like in the temperature example.</p>
					<p>As we&#146;ll point out again and again in this chapter, one way to think about sound is as a sequence of time-varying amplitudes or pressures, or, more succintly, as a <i>function of time.</i> The amplitude (<i>y</i>-)axis of the graphs of sound represents the amount of air <i>compression</i> (above zero) or <i>rarefaction</i> (below zero) caused by a moving object, like vocal chords. Note that zero is the &quot;rest&quot; position, or pressure equilibrium (<i>silence</i>). Looking at the changes in amplitude over time gives a good idea of the amplitude shape or <i>envelope</i> of the sound wave.</p>
					<p>Actually, this amplitude shape might correspond closely to a number of things, including:</p>
					<ul>
						<li>the actual <i>vibration</i> of the object
						<li>the changes in <i>pressure</i> of the air, or water, or other medium
						<li>the <i>deformation</i> (in or out) of the eardrum
					</ul>
					<p>This picture of a sound wave, as with amplitudes in time, provides a nice visual metaphor for the idea of sound as a continuous sequence of pressure variations. When we talk about computers, this graph of pressure versus time becomes a picture of a list of numbers plotted against some variable (again, time). We&#146;ll see in Chapter 2 how these numbers are stored and manipulated.</p>
					<h2>Frequency: A Preview</h2>
					<p>Amplitude is just one mathematical, or acoustical, characteristic of sound, just as loudness is only one of the perceptual characteristics of sounds. But, as we know, sounds aren&#146;t only loud and soft.</p>
					<p>People often describe musical sounds as being &quot;high&quot; or &quot;low.&quot; A bird tweeting may sound &quot;high&quot; to us, or a tuba may sound &quot;low.&quot;</p>
					<p>But what are we really saying when we classify a sound as &quot;high&quot; or &quot;low&quot;? There&#146;s a fundamental characteristic of these graphs of pressure in time that is less obvious to the eye but very obvious to the ear: namely, whether there is (or is not) a repeating pattern, and if so how quickly it repeats. That&#146;s <i>frequency</i>!<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/tuba.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button29" border="0" alt=""></a><br>
						<b>Soundfile 1.4<br>
						</b>Tuba sounds</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/thrush.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button27" border="0" alt=""></a><br>
						<b>Soundfile 1.5<br>
						</b>More bird sounds</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>When we say that the tuba sounds are low and the bird sounds are high, what we are really talking about is the result of the frequency of these particular sounds&#151;how fast a pattern in the sound&#146;s graph repeats. In terms of waveforms, like what you saw and heard in the previous sound files, we can, for the moment, somewhat concisely state that the rate at which the air pressure fluctuates (moves in and out) is the frequency of the sound wave. We&#146;ll learn a lot more about frequency and its related cognitive phenomenon, <i>pitch,</i> in Section 1.3.
</p>
					<h2>How Our Ears Work</h2>
					<p>Mathematical functions and kids jumping on a trampoline are one thing, but what&#146;s the connection to sound and music? Just moving an eardrum in and out can&#146;t be the whole story! Well, it isn't. The ear is a complex mechanism that tries to make sense out of these arbitrary functions of pressure in time and sends the information to the brain.</p>
					<p>We&#146;ve already used the physical analogy of the trampoline as our eardrum and the kids as the air molecules set in motion by a sound source. But to cover the topic more completely, we need to discuss how sounds interact, via the eardrum, with the rest of our <i>auditory system</i> (including the brain).</p>
					<p>Our eardrums, like microphones and speakers, are in a sense <i>transducers</i>&#151;they turn one form of information or energy into another.</p>
					<p>When sound waves reach our ears, they vibrate our eardrums, transferring the sound energy through the middle ear to the inner ear, where the real magic of human hearing takes place in a snail-shaped organ called the <i>cochlea.</i> The cochlea is filled with fluid and is bisected by an elastic partition called the <i>basilar membrane</i>, which is covered with hair cells. When sound energy reaches the cochlea, it produces fluid waves that form a series of peaks in the basilar membrane, the position and size of which depend on the frequency content of the sound.</p>
					<p>Different sections of the basilar membrane <i>resonate</i> (form peaks) at different frequencies: high frequencies cause peaks toward the front of the cochlea, while low frequencies cause peaks toward the back. These peaks match up with and excite certain <i>hair cells,</i> which send nerve impulses to the brain via the auditory nerve. The brain interprets these signals as sound, but as an interesting thought experiment, imagine extraterrestrials who might &quot;see&quot; sound waves (and maybe &quot;hear&quot; light). In short, the cochlea transforms sounds from their physical, time domain (amplitude versus time) form to the frequency domain (amplitude versus frequency) form that our brains understand. Pretty impressive stuff for a bunch of goo and some hairs!</p>
					<p class="figure"><img height="175" width="437" src="../images/chapter1/basilar.jpg"><br>
						<br>
					</p>
					<p class="figure_caption"><b>Figure 1.7</b>&nbsp;&nbsp;Diagram of the inner ear showing how sound waves that enter through the auditory canal are transformed into peaks, according to their frequencies, on the basilar membrane. In other words, the <i>basilar membrane</i> serves as a <i>time-to-frequency converter,</i> in order to prepare sonic information for its eventual cognition by the higher functions of the brain.</p>
					
      <p>Who&#146;d have thought sound was this complicated! But keep in mind 
        that the sound wave pressure picture is just raw data; it contains no 
        frequency, timbral, or any other kind of information. It needs a lot of 
        processing, organization, and consideration to provide any sort of meaning 
        to us higher species.</p>
					<p>We&#146;ve made the hearing process seem pretty simple, but actually there&#146;s a lot of controversy in current auditory cognition research about the specifics of this remarkable organ and how it works. As we understand more and more about the ear, musicians and scientists gain an increasing sense of understanding how we perceive sound, and even, some believe, how we perceive music. It&#146;s an exciting field of research, and an active one!<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<a onclick="setSrc('../sounds/chapter1/trp.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button23" border="0" alt=""></a><br>
						<b>Soundfile 1.6</b><br>
						
						Trumpet sound</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/airplane.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button30" border="0" alt=""></a><br>
						<b>Soundfile 1.7</b><br>
						Loud sound that gets softer</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/whoosh.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 1.8</b><br>
						Whooshing</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/nuthatch1.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 1.9</b><br>
						
						Chirping</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/funky.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button24" border="0" alt=""></a><br>
						<b>Soundfile 1.10</b><br>
						
						Phat groove</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<h2>How Do We Describe Sound?</h2>
					<p>Sound can be described in many ways. We have a lot of different words for sounds, and different ways of speaking about them. For example, we can call a sound &quot;groovy,&quot; &quot;dark,&quot;  &quot;bright,&quot; &quot;intense,&quot; &quot;low and rumbly,&quot; and so on. In fact, our colloquial language for talking about sound, from a scientific viewpoint, is pretty imprecise. Part of what we&#146;re trying to do in computer music is to try to formulate more formal ways of describing sonic phenomena. That doesn&#146;t mean that there&#146;s anything wrong with our usual ways of talking about sounds: our current vocabulary actually works pretty well.</p>
					<p>But to manipulate digital signals with a computer, it is useful to have access to a different sort of description. We need to ask (and answer!) the following kinds of questions about the sound in question:</p>
					<ul>
						<li>How loud is it?
						<li>What is its pitch?
						<li>What is its spectrum?
						<li>What frequencies are present?
						<li>How loud are the frequencies?
						<li>How does the sound change over time?
						<li>Where is the sound coming from?
						<li>What&#146;s a good guess as to the characteristics of the physical object that made the sound?
					</ul>
					<p>Even some of these questions can be broken down into lots of smaller questions. For example, what specifically is meant by &quot;pitch&quot;? Taken together, the answers to these questions and others help describe the various characteristics and features that for many years have been referred to collectively as the <i>timbre</i> (or &quot;color&quot;) of a sound. But before we talk about timbre, let&#146;s start with more basic concepts: <i>amplitude</i> and <i>loudness</i> (Section 1.2).</p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="01_02.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
