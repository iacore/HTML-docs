<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>


		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="../chapter2/02_01.php">Next Chapter --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="14" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td rowspan="2" align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 1: The Digital Representation of Sound,<br>
						Part One: Sound and Timbre</h1>
					<h1>Section 1.4: Timbre<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="580">
					<p>What&#146;s the difference between a tuba and a flute (or, more accurately, the sounds of each)? How do we tell the difference between two people singing the same song, when they&#146;re singing exactly the same notes? Why do some guitars &quot;sound&quot; better than others (besides the fact that they&#146;re older or cost more or have Eric Clapton&#146;s autograph on them)? What is it that makes things &quot;sound&quot; like themselves?</p>
					<p>It&#146;s not necessarily the pitch of the sound (how high or low it is)&#151;if everyone in your family sang the same note, you could almost surely tell who was who, even with your eyes closed. It&#146;s also not just the loudness&#151;your voice is still your voice whether you talk softly or scream at the top of your lungs. So what&#146;s left? The answer is found in a somewhat mysterious and elusive thing we call, for lack of a better word, &quot;timbre,&quot; and that&#146;s what this section is all about.</p>
					<p>&quot;Timbre&quot; (pronounced &quot;tam-ber&quot;) is a kind of sloppy word, inherited from previous eras, that lumps together lots of things that we don&#146;t fully understand. Some think we should abandon the word and concept entirely! But it&#146;s one of those words that gets used a lot, even if it doesn&#146;t make much sense, so we&#146;ll use it here too&#151;we&#146;re sort of stuck with it for the time being. <br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2444F35'));return CSClickReturn()" href="../applets/1_4_draw_wave.php" target="_blank" csclick="BCE2444F35"><img src="../images/global/applet.gif" width="80" height="65" name="5" border="0"></a><br>
						<b>Applet 1.9</b><br>
						<span class="icon_caption">Draw a waveform</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This applet lets you draw a periodic waveform. How much does the shape of the waveform (which is a result of its steady-state spectra) influence what you hear? Some shapes seem to sound &quot;brighter&quot; than others, some &quot;duller.&quot;</p>
					<p class="applet_caption">We&#146;ll see, as we learn more about timbre, that the periodic waveform shape may not be all that important when it comes to our recognizing, distinguishing, and identifying sounds. This is somewhat surprising, and contrary to 100 years of popular assumption. But you may notice, after all, that in redrawing the waveform you might not feel that you&#146;re creating a whole new sound event, just a kind of different &quot;buzz.&quot;</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h2>
						What Makes Up Timbre?</h2>
					<p><i>Timbre</i> can be roughly defined as those qualities of a sound that aren&#146;t just frequency or amplitude. These qualities might include:</p>
					<ul>
						<li><i>spectra:</i> the aggregate of simpler waveforms (usually sine waves) that make up what we recognize as a particular sound. This is what Fourier analysis gives us (we&#146;ll discuss that in Chapter 3).

						<li><i>envelope:</i> the attack, sustain, and decay portions of a sound (often referred to as <i>transients</i>). 
						
					</ul>
					<p>Envelope and spectra are very complicated concepts, encompassing a lot of subcategories. For example, <i>spectral features</i> are very important, different ways that the spectral aggregates are organized statistically in terms of shape and form (e.g., the relative &quot;noisiness&quot; of a sound is a result, in large part, of its spectral relationships). Many facets of envelope (onset time, harmonic decay, spectral evolution, steady-state modulations, etc.) are not easily explained by just looking at the envelope of a sound. Researchers spend a great deal of time on very specific aspects of these ideas, and it&#146;s an exciting and interesting area for computer musicians to research.</p>
					<p>Figure 1.18 shows a simplified picture of the envelope of a trumpet tone.</p>
					<p class="figure"><img height="159" width="262" src="../images/chapter1/adsr2.jpg"><br>
						<img height="258" width="381" src="../images/chapter1/adsr.jpg" border="1" vspace="5"></p>
					<p class="figure_caption"><b>Figure 1.18&nbsp;&nbsp;</b>This image illustrates the attack, sustain, and decay portions of a standard amplitude envelope. This is a very simple, idealized picture, called a trapezoidal envelope. We are not aware of any actual, natural occurrence of this kind of straight-lined sound!</p>
					<p>It&#146;s helpful here to bring another descriptive term into our vocabulary: spectrum. <i>Spectrum</i> is defined by a waveform&#146;s distribution of energy at certain frequencies. The combination of spectra (plural of spectrum) and envelope helps us to define the &quot;color&quot; of a sound. Timbre is difficult to talk about, because it&#146;s hard to measure something subjective like the &quot;quality&quot; of a sound. This concept gives music theorists, computer musicians, and psychoacousticians a lot of trouble. However, computers have helped us make great progress in the exploration and understanding of the various components of what&#146;s traditionally been called &quot;timbre.&quot;</p>
					<h2>Basic Elements of Sound</h2>
					<p>As we&#146;ve shown, the average piece of music can be a pretty complicated function. Nevertheless, it&#146;s possible to think of it as a combination of much simpler sounds (and hence simpler functions)&#151;even simpler than individual instruments. The basic atoms of sound, the sinusoids (sine waves) we talked about in the previous sections, are sometimes called <i>pure tones,</i> like those produced when a tuning fork vibrates. We use the tuning fork to talk about these tones because it is one of the simplest physical vibrating systems.</p>
					<p>Although you might think that a discussion of tuning forks belongs more in a discussion of frequency, we&#146;re going to use them to introduce the notion of sinusoids: Fourier components of a sound.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/tuningfork.aiff..mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button35" border="0" alt=""></a><br>
						<b>Soundfile 1.16</b><br>
						
						<span class="icon_caption">Tuning fork<br>
						
							
						at 256 Hz</span><br>
						<br>
					</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/sine.256.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button22" border="0" alt=""></a><br>
						<b>Soundfile 1.17</b><br>
						
						<span class="icon_caption">Sine wave<br>
						
							
							at 256 Hz</span><br>
						<br>
					</p>
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2475238'));return CSClickReturn()" href="../popups/chapter1/xbit_1_6.php" target="_blank" csclick="BCE2475238"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 1.6</b><br>
						<span class="icon_caption">Tuning forks</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush_top"><img height="238" width="200" src="../images/chapter1/fork.jpg"></p>
					<p class="figure_caption"><b>Figure 1.19</b>&nbsp;&nbsp;This tuning fork rings at 256 Hz. You can hear the sound it makes by clicking on Soundfile 1.16.<br>
						<br>
						Compare the tuning fork sound to that of a pure sine wave at the same frequency of 256 Hz.</p>
					<p>When you hit the tines of a tuning fork, it vibrates and emits a very pure note or tone. Tuning forks are able to vibrate at very precise frequencies. The frequency of a tuning fork is the number of times the tip goes back and forth in a second. And this number won&#146;t change, no matter how hard you hit that fork. As we mentioned, the human ear is capable of hearing sounds that vibrate all the way from 20 times a second to 20,000 times a second. Low-frequency sounds are like bass notes, and high-frequency sounds are like treble notes. (Low frequency means that the tines vibrate slowly, and high frequency means that they vibrate quickly.)</p>
					<p class="figure">
					<a href="../images/chapter1/forklo.jpg" csclick="BAD89388108,BAD89388110" onclick="CSAction(new Array(/*CMP*/'BAD89388108',/*CMP*/'BAD89388110'));return CSClickReturn()"><img height="100" width="60" src="../images/chapter1/tunefork.gif" border="0"></a>
					<a href="../images/chapter1/forkmed.jpg" csclick="BAD8938B116,BADD8AF01" onclick="CSAction(new Array(/*CMP*/'BAD8938B116',/*CMP*/'BADD8AF01'));return CSClickReturn()"><img height="100" width="60" src="../images/chapter1/tunefork.gif" border="0" hspace="60"></a>
					<a href="../images/chapter1/forkhi.jpg" csclick="BAD8938A112,BADD8AE00" onclick="CSAction(new Array(/*CMP*/'BAD8938A112',/*CMP*/'BADD8AE00'));return CSClickReturn()"><img height="100" width="60" src="../images/chapter1/tunefork.gif" border="0"></a></p>
					
      <p class="figure_caption"><b>Figure 1.20&nbsp;&nbsp;</b>Click on the different 
        tuning forks to see the different audiograms. Notice what they have in<b> 
        </b>common! They are all roughly the same shape&#151;simple waves that 
        differ only in the width of the<b> </b>regularly repeating peaks. The 
        higher tones give more peaks over the same interval. In other<b> </b>words, 
        the peaks occur more frequently. (Get it? Higher frequency!)</p>
					<p>When you whack the tines of a tuning fork, the fork <i>vibrates.</i> The number of times the tines go back and forth in one second determines the frequency of a particular tuning fork.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/burt.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button37" border="0" alt=""></a><br>
						<b>Soundfile 1.18</b><br>
						<span class="icon_caption">Composer Warren Burt&#146;s piece for tuning forks</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>Click Soundfile 1.18. You&#146;ll hear composer Warren Burt&#146;s piece for tuning forks, &quot;Improvisation in Two Ancient Greek Modes.&quot;</p>
					<p>Now, why do tuning fork functions have their simple, sinusoidal shape? Think about how the tip of the tuning fork<b> </b>is moving over time. We see that it is moving back and forth, from its greatest displacement in one direction all the way back to just about the same displacement in the opposite direction.</p>
					<p>Imagine<b> </b>that you are sitting on the end of the tine (hold on tight!). When you move to the left, that will be a<b> </b><i>negative</i> displacement; and when you move to the right, that will be a <i>positive</i> displacement. Once again, as time progresses we can graph the function that at each moment in time outputs your position. Your back-and-forth motion yields the functions many of you remember from trigonometry: sines and cosines.</p>
					<p class="figure"><img height="288" width="388" src="../images/chapter1/sine-cosine-animation2.gif"></p>
					
      <p class="figure_caption"><b>Figure 1.21&nbsp;&nbsp;</b>Sine and cosine 
        waves. Thanks to Wayne Mathews for this image.</p>
					<p>Any sound can be represented as a combination of different amounts of these sines and cosines of varying frequencies. The mathematical topic that explains sounds and other wave phenomena is called <i>Fourier analysis</i>, named after its discoverer, the great 18th century mathematician <i>Jean Baptiste Joseph Fourier.</i></p>
					<p class="figure"><img height="219" width="530" src="../images/chapter1/spec.jpg"></p>
					
      <p class="figure_caption"><b>Figure 1.22&nbsp;&nbsp;</b>Spectra of (a) sawtooth 
        wave and (b) square wave.</p>
					<p>Figure 1.22 shows the relative amplitudes of sinusoidal components of simple waveforms.</p>
					<p>For example, Figure 1.22(a) indicates that a sawtooth wave can be made by addition in the following way: one part of a sine wave at the fundamental frequency (say, 1 Hz), then half as much of a sine wave at 2 Hz, and a third as much at 3 Hz, and so on, infinitely.</p>
					<p>In Section 4.2, we&#146;ll talk about using the Fourier technique in synthesizing sound, called <i>additive synthesis</i>. If you want to jump ahead a bit, try the applet in Section 4.2, that lets you build simple waveforms from sinusoidal components. Notice that when you try to build a square wave, there are little ripples on the edges of the square. This is called <i>Gibbs ringing,</i> and it has to do with the fact that the sum of any finite number of these decreasing amounts of sine waves of increasing frequency is never <i>exactly</i> a square wave.</p>
					<p>What the charts in Figure 1.22 mean is that if you add up all those sinusoids whose frequencies are integer multiples of the fundamental frequency of the sound and whose amplitudes are described in the charts by the heights of the bars, you&rsquo;ll get the sawtooth and square waves.</p>
					<p>This is what Fourier analysis is all about: every periodic waveform (which is the same, more or less, as saying every pitched sound) can be expressed as a sum of sines whose frequencies are integer multiples of the fundamental and whose amplitudes are unknown. The sawtooth and square wave charts in Figure 1.22 are called <i>spectral histograms</i> (they don&#146;t show any evolution over time, since these waveforms are periodic).</p>
					<p>These sine waves are sometimes referred to as the spectral components, partials, overtones, or harmonics of a sound, and they are what was thought to be primarily responsible for our sense of timbre. So when we refer to the tenth partial of a timbre, we mean a sinusoid at 10 times the frequency of the sound&#146;s fundamental frequency (but we don&#146;t know its amplitude).</p>
					<p>The sounds in some of the following soundfiles are conventional instruments with their attacks lopped off, so that we can hear each instrument as a different periodic waveform and listen to each instrument&#146;s different spectral configurations. Notice that, strangely enough, the clarinet (whose sound wave is a lot like a sawtooth wave) and the flute, without their attacks, are not all that different (in the grand scheme of things).<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<a onclick="setSrc('../sounds/chapter1/clar.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button36" border="0" alt=""></a><br>
						<b>Soundfile 1.19</b><br>
						
						Clarinet sound</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/clar.clip.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button23" border="0" alt=""></a><br>
						<b>Soundfile 1.20</b><br>
						
						Clarinet with attack lopped off</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush"><img height="219" width="314" src="../images/chapter1/clarinet.gif"></p>
					
      <p class="figure_caption_flush"><b>Figure 1.23</b>&nbsp;&nbsp; Clarinet.<br>
							<br>
						</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<a onclick="setSrc('../sounds/chapter1/fl.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button24" border="0" alt=""></a><br>
						<b>Soundfile 1.21</b><br>
						
						Flute sound</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/fl.clip.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button25" border="0" alt=""></a><br>
						<b>Soundfile 1.22</b><br>
						
						Flute with attack lopped off</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush"><img height="215" width="313" src="../images/chapter1/flute.gif"></p>
					
      <p class="figure_caption_flush"><b>Figure 1.24</b>&nbsp;&nbsp;Flute.<br>
							<br>
						</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<a onclick="setSrc('../sounds/chapter1/pno.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button26" border="0" alt=""></a><br>
						<b>Soundfile 1.23</b><br>
						
						Piano sound</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/pno.clip.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button38" border="0" alt=""></a><br>
						<b>Soundfile 1.24</b><br>
						
						Piano with attack lopped off<br>
						<br>
					</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush"><img height="201" width="299" src="../images/chapter1/piano.gif"></p>
					
      <p class="figure_caption_flush"><b>Figure 1.25</b>&nbsp;&nbsp;Piano.<br>
							<br>
						</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<a onclick="setSrc('../sounds/chapter1/trb.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button27" border="0" alt=""></a><br>
						<b>Soundfile 1.25</b><br>
						
						Trombone<br>
						(not played underwater!)</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/trb.clip.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button28" border="0" alt=""></a><br>
						<b>Soundfile 1.26</b><br>
						
						Trombone with attack lopped off<br>
						<br>
					</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush"><img height="120" width="200" src="../images/chapter1/trombone.jpg"></p>
					
      <p class="figure_caption_flush"><b>Figure 1.26&nbsp;&nbsp;</b><span class="applet_caption">Picture 
        courtesy of Bob Hovey www.TROMBONISTICALISMS.bigstep.com.</span></p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<a onclick="setSrc('../sounds/chapter1/vln.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button29" border="0" alt=""></a><br>
						<b>Soundfile 1.27</b><br>
						
						Violin</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/vln.clip.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button30" border="0" alt=""></a><br>
						<b>Soundfile 1.28</b><br>
						
						Violin with attack lopped off</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush"><img height="274" width="274" src="../images/chapter1/violin.gif"></p>
					
      <p class="figure_caption_flush"><b>Figure 1.27</b>&nbsp;&nbsp;Violin.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<a onclick="setSrc('../sounds/chapter1/vox.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 1.29</b><br>
						
						Voice</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/vox.clip.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 1.30</b><br>
						
						Voice with attack lopped off</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush"><img height="258" width="250" src="../images/chapter1/singer.gif"></p>
					
      <p class="figure_caption_flush"><b>Figure 1.28&nbsp;&nbsp;</b><span class="applet_caption">Photo 
        courtesy of www.bobhoose.com.</span></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="../chapter2/02_01.php">Next Chapter --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0">
			</tr>
		</table>


<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
