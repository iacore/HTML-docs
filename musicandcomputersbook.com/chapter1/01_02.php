<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>


		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="01_03.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="14" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 1: The Digital Representation of Sound,<br>
						Part One: Sound and Timbre</h1>
					<h1>Section 1.2: <b>Amplitude and Loudness</b><br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE243C428'));return CSClickReturn()" href="../popups/chapter1/xbit_1_2.php" target="_blank" csclick="BCE243C428"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 1.2</b><br>
						<span class="icon_caption">Another sonic universe</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>In the previous section we talked briefly about how a function of amplitude in time could be thought of as a kind of sampling of a sound. (Remember, a <i>sample</i> is essentially a measurement of the amplitude of a sound at a point in time.)</p>
					<p>But knowing that at 200.056 milliseconds the amplitude of a sound is 0.2 doesn&#146;t really help us understand much about the sound in most cases. What we need is some way of measuring some form of <i>average</i> amplitude of a number of samples (we sometimes call this a <i>frame</i>). We need a way of understanding how these amplitudes, which are a <i>physical</i> measurement (like frequency), correspond to our perception of loudness, which is a <i>psychophysical</i> (anything that we perceive about the physical world is called &quot;psychophysical&quot;) or, more precisely, <i>psychoacoustic</i> or <i>cognitive</i> measure (like pitch).</p>
					
      <p>We&#146;ll learn in Section 1.3 that amplitude and frequency are not 
        independent&#151;they both contribute to our perception of loudness; that 
        is, we use them together (in a way described by something called the <i>Fletcher-Munson 
        curves</i>, Section 1.3). But to describe that complex psychoacoustic 
        or cognitive aggregate called loudness, we first need to understand something 
        about amplitude and another related quantity called <i>intensity.</i> 
        Then, at the end of our discussion on frequency (Section 1.3), we&#146;ll 
        return to an important way that frequency affects loudness (we&#146;ll 
        give you a little preview of this in this section as well).</p>
					<p>In fact, it&rsquo;s very important to realize that certain terms refer to physical or acoustic measures and others refer to cognitive ones. The cognitive measures are much higher level and often incorporate related effects from several acoustic phenomena. See Figure 1.8 to sort out this little terminological jungle!</p>
					<table width="100%" border="0" cellspacing="1" cellpadding="6" bgcolor="black">
						<tr>
							<td class="figure_caption" colspan="5" valign="top" bgcolor="white">
								<table width="100%" border="0" cellspacing="1" cellpadding="6">
									<tr>
										<td colspan="5" align="center"><b>Acoustic and Cognitive (Psycho-acoustic Correlates)</b></td>
									</tr>
									<tr>
										<td colspan="2" valign="top">Frequency</td>
										<td colspan="2" valign="top">--&gt;</td>
										<td valign="top">Pitch</td>
									</tr>
									<tr>
										<td class="figure_caption" colspan="2" valign="top">How fast something vibrates.</td>
										<td colspan="2" valign="top">&nbsp;</td>
										<td class="figure_caption" valign="top">How high or low we perceive it.</td>
									</tr>
									<tr>
										<td valign="top">Amplitude</td>
										<td valign="top" nowrap>--&gt;</td>
										<td valign="top">Intensity</td>
										<td valign="top" nowrap>--&gt;</td>
										<td valign="top">Loudness</td>
									</tr>
									<tr>
										<td class="figure_caption" valign="top">How much something vibrates</td>
										<td valign="top">&nbsp;</td>
										<td class="figure_caption" valign="top">How much the medium is displaced.</td>
										<td valign="top">&nbsp;</td>
										<td class="figure_caption" valign="top">How loud we perceive it, affected by pitch and timbre.</td>
									</tr>
									<tr>
										<td class="figure_caption" valign="top">Waveshape</td>
										<td valign="top">&nbsp;</td>
										<td valign="top">--&gt;</td>
										<td valign="top">&nbsp;</td>
										<td valign="top">Timbre (???)</td>
									</tr>
									<tr>
										<td class="figure_caption" colspan="5" valign="top">Attack/Decay (traansients)</td>
									</tr>
									<tr>
										<td class="figure_caption" colspan="5" valign="top">Modulation</td>
									</tr>
									<tr>
										<td class="figure_caption" colspan="5" valign="top">Spectral characteristics</td>
									</tr>
									<tr>
										<td class="figure_caption" colspan="5" valign="top">Spectral, pitch, and loudness trajectory</td>
									</tr>
									<tr>
										<td class="figure_caption" colspan="5" valign="top">Everything else... .</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<p class="figure_caption_flush_bottom"><b>Figure 1.8&nbsp;&nbsp;</b>Acoustic phenomena resulting from pressure variations and their psychophysical, or psychoacoustic (perceptual), counterparts.<br>
						<br>
						This chart gives some sense of how the terminology for sound varies depending on whether we talk about direct physical measures (frequency, amplitude) or cognitive ones (pitch, loudness).<br>
						<br>
						It&#146;s true that pitch is largely a result of frequency, but be careful: they&#146;re not the same thing.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE243CE29'));return CSClickReturn()" href="../applets/1_2_hear_just_amp.php" target="_blank" csclick="BCE243CE29"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 1.2</b><br>
						<span class="icon_caption">Changing amplitudes</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This applet lets you experiment with changing amplitudes of a signal so that you can experiment with different sound levels.
</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<h2>Amplitude and Pitch Independence</h2>
					<p>If you gently pluck a string on a guitar and then pluck it again, this time harder, what is the difference in the sounds you hear? You&#146;ll hear the same pitch, only louder. That illustrates something interesting about the relationship between pitch and loudness&#151;they are generally independent of one another.</p>
					<p>You can have two of the same frequencies at one loudness, or two different loudnesses at one frequency. You can visualize this by drawing a series of sine waves, each with the same period but different amplitude. <i>Pure tones</i> with the same period will generally be heard to have the same pitch&#151;so all of the sine waves must be at the same frequency. We&#146;ll see that pure tones correspond to variations in that old favorite function, the sine function. Remember that amplitude is <i>not</i> loudness (one is physical, the other is psychophysical), but for the moment let&#146;s not make that distinction too rigid.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/SameFDifferentA.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button23" border="0" alt=""></a><br>
						<b>Soundfile 1.11</b><br>
						<span class="icon_caption">Two sine waves</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush_top"><img height="200" width="497" src="../images/chapter1/samepitchdiffamp.jpg"></p>
					<p class="figure_caption_flush_bottom"><b>Figure 1.9&nbsp;&nbsp;</b>Two sine waves with the same frequency, different amplitude.<br>
						<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/SameFDifferentTimbre.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button22" border="0" alt=""></a><br>
						<b>Soundfile 1.12</b><br>
        A sine wave and a triangular wave</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush_top"><img height="205" width="500" src="../images/chapter1/sinetri.jpg"></p>
					<p class="figure_caption"><b>Figure 1.10&nbsp;&nbsp;</b>Two sounds may have the same frequency but different waveforms (resulting in a different sense of timbre).</p>
					<p class="figure"><img height="215" width="532" src="../images/chapter1/envelope.jpg" vspace="5"></p>
					<p class="figure_caption"><b>Figure 1.11</b>&nbsp;&nbsp;We can draw an <i>envelope</i> over a sound file, which is the average, or smoothed, amplitude of the sound wave.<br>
						<br>
						This is roughly equivalent to what we perceive as the changes in loudness of the sound. (If we just take one average, we might call that the loudness of the whole sound.)</p>
					<p class="figure"><img height="192" width="532" src="../images/chapter1/outofphase.jpg" vspace="5"></p>
					<p class="figure_caption"><b>Figure 1.12&nbsp;&nbsp;</b>Two sine waves starting at different <i>phase points.</i> These sine waves have different starting points&#151;like the trampoline we talked about in the previous section. It can start flexed, either up or down. Remember that these starting points are very close together in time (tiny fractions of a second). We&#146;ll talk more about phase and frequency later.</p>
					<p>This simple picture directly above shows us something interesting about amplitude. The point where the colors overlap is where the waveforms will combine, either summing to a combined signal at a higher level or summing to a combined signal at a lower level. That is, when one waveform goes negative (compression, perhaps), it will counteract the other&#146;s positive (rarefaction, perhaps). This is called <i>phase cancellation</i>, but the complexity with which this phenomenon occurs in the real sound world is obviously very great&#151;lots and lots of sound waves going positive and negative all over the place.</p>
					
      <h2>Intensity</h2>

					</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/sine.sw.0-20K.snd.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button25" border="0" alt=""></a><br>
						<b>Soundfile 1.13</b><br>
						<span class="icon_caption">Chirp</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580"> <p>You can think of Soundfile 
        1.13 as a function that begins life as a sine function, <I>f</I>(<I>x</I>) 
        = sin(<I>x</I>), and then over time morphs into a really fast oscillating 
        sine function, like <I>f</I>(<I>x</I>) = sin(20,000<I>x</I>). As it morphs, 
        we&#146;ll continually listen to it.</p>
      <p>Soundfile 1.3, sometimes called a <i>chirp</i> in acoustic discussions, 
        sweeps a sine wave over the frequency range<b> </b>from 0 Hz to 20,000 
        Hz. (Just so you know, this sonic phenomenon is not named for a bird chirp, 
        but in fact for a radar chirp, which is a special function used in some 
        radar work.)</p>
					<p>The amplitude of the sine wave in Soundfile 1.13 does not change, but the perceived loudness changes as it moves through areas of greater sensitivity in the Fletcher-Munson curve (Section 1.3). In other words, how loud we perceive something to be is mostly a result of amplitude, but it is also a result of frequency. This simple example shows how complicated our <i>perception</i> of even these simple phenomena is.</p>
					<p>We measure amplitude in volts, pressure, or even just sample numbers: it doesn&#146;t really matter. We just graph a function of something moving, and then if we want, we can say what its average displacement was. If it was generally a big displacement (for example, sixth graders on the trampoline), we say it has a big amplitude. If, on the other hand, things didn&#146;t move very much (say we drop a bunch of mice on the trampoline), we say that the sound function had a small amplitude.</p>
					<p>In the real world, things vibrate and send their vibrations through some medium (usually gas) to our eardrums. When we become interested in how amplitude actually affects a medium, we speak of the <i>intensity</i> of a sound in the medium. This is a little more specific than discussing amplitude, which is more a purely relative term.</p>
					<p>The medium we&#146;re usually interested in is air (at sea level, 72&deg;F), and we measure intensity as the amount of energy in a given air unit, the cubic meter. In this case, <i>energy</i> (or work done) is measured in watts, and we can then measure intensity in watts per meter<sup>2</sup> (or wm<sup>2</sup>).</p>
					<p>As is the case with the perception of frequency as pitch, our perception of intensity as loudness is logarithmic. But what the heck does that mean? <i>Logarithmic perception</i> means that it takes more of a change in the amplitude to produce the same perceived change in loudness.</p>
					<h2>Bubba and You</h2>
					<p>Think of it this way. Let&#146;s say you work at a burger joint flipping burgers, and you make $8.00 an hour. Let&#146;s say your supervisor, Bubba, makes $9.00 an hour. Now let&#146;s say the burger corporation hits it big with their new broccoli sandwich, and they decide to put every employee on a monthly raise schedule.</p>
					<p>They decide to give Bubba a dollar a month raise, and you a 7% raise each month. Bubba thinks this is great!</p>
					<p>That means the first month you only get $8.56, and Bubba gets $10.00. The next month, Bubba gets $11.00, and you get $9.15. This means that you now got a 59&cent; raise, or that your raise went up while his remained the same. The equation for your salary for any given month is:</p>
					<p>new salary = old salary + (0.07 X old salary)</p>
					<p>Bubba&#146;s is:</p>
					<p>new salary = old salary + 1</p>
					
      <p>You&rsquo;re getting an increase by a fixed ratio of your salary, which itself is increasing, while Bubba&rsquo;s raise/salary ratio is actually decreasing. (The first month he got 1/9, the next month 1/10&#151;at this rate he&rsquo;ll approach a zero percent raise if he works at the burger place long enough.) Figure 1.13 shows what the salary raises look like as functions.</p>
					<p class="figure"><img height="301" width="489" src="../images/chapter1/bubba.gif"></p>
					<p class="figure_caption"><b>Figure 1.13</b>&nbsp;&nbsp;Bubba & U.</p>
					<p>This fundamental difference between ratiometric change and fixed arithmetic change is very important. We tend to perceive most changes not in terms of <i>absolute</i> quantities (in this case, $1.00), but in terms of <i>relative</i> quantities (percentage of a quantity).</p>
					
      <p>Changes in both amplitude and frequency are also perceived in terms of 
        ratios. In the case of amplitude, we have a standard measure, called the 
        <i>decibel</i> (dB), which can describe how <i>loud</i> a sound is perceived. 
        As a convention, <i>silence,</i> which is 0 dB, is set to 10<sup>-12</sup> 
        wm<sup>2</sup>. This is not really silence in the absolute sense, because 
        things are still vibrating, but it is more or less what you would hear 
        in a very quiet recording studio with nobody making any sound. There is 
        still air movement and other sound-producing activity. (There are rooms 
        called <i>anechoic chambers,</i> used to study sound, that try to get 
        things much quieter than 0 dB&#151;but they&#146;re very unusual places 
        to be.) Any change of 10 dB corresponds roughly to a doubling of perceived 
        loudness. So, for example, going from 10 dB to 20 dB or from 12 dB to 
        22 dB is perceived as a doubling of perceived sound pressure level.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2442333'));return CSClickReturn()" href="../popups/chapter1/xbit_1_3.php" target="_blank" csclick="BCE2442333"><img src="../images/global/popup.gif" width="80" height="65" name="button6" border="0"></a><br>
						<b>Xtra bit 1.3</b><br>
						
						
						<span class="icon_caption">More on decibels</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<div align="center">
						<table border="0" cellpadding="5" cellspacing="1" width="523" bgcolor="black">
							<tr>
								<td bgcolor="white"><b>Source</b></td>
								<td nowrap bgcolor="white"><b>Average dB</b></td>
							</tr>
							<tr>
								<td bgcolor="white">silence</td>
								<td bgcolor="white" align="right">0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td bgcolor="white">whisper, rustling of leaves on a romantic summer evening</td>
								<td bgcolor="white" align="right">30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td bgcolor="white">phone ringing, normal conversation</td>
								<td bgcolor="white" align="right">60&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td bgcolor="white">car engine</td>
								<td bgcolor="white" align="right">70&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td bgcolor="white">diesel truck, heavy city traffic, vacuum cleaner, factory noise</td>
								<td bgcolor="white" align="right">80&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td bgcolor="white">power lawn mower, subway train</td>
								<td bgcolor="white" align="right">90&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td bgcolor="white">chain saw, rock concert</td>
								<td bgcolor="white" align="right">110&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td bgcolor="white">jet takeoff, gunfire, Metallica (your head near the guitar player&#146;s amplifier)</td>
								<td bgcolor="white" align="right">120+&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								
							</tr>
						</table>
					</div>
					<p class="figure_caption"><b>Table 1.1</b>&nbsp;&nbsp;Average decibel levels for environmental sounds. 120 dB is often called the threshold of pain. Good name for a rock band, huh?<br>
						<br>
						Note that even brief exposure to very loud (90 dB or greater) sounds or constant exposure to medium-level (60 dB to 80 dB) sounds can cause hearing loss. Be careful with your ears&#151;invest in some earplugs!</p>
					<blockquote>
						<p><b>&quot;Audience participation: During tests in the Royal Festival Hall, a note played mezzo-forte on the horn measured approximately 65 decibels of sound. A single uncovered cough gave the same reading. A handkerchief placed over the mouth when coughing assists in obtaining a pianissimo.&quot;</b></p>
						<p>&#151;Concert program from the Royal Festival Hall, South Bank Centre, London</p>
					</blockquote>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2442934'));return CSClickReturn()" href="../applets/1_2_hear_amp_versus_db.php" target="_blank" csclick="BCE2442934"><img src="../images/global/applet.gif" width="80" height="65" name="3" border="0"></a><br>
						<b>Applet 1.3</b><br>
						<span class="icon_caption">Amplitude versus decibel level</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Two faders, one that changes amplitude (linear, acoustical), one that changes decibel level (exponential, psychoacoustical). Which one would you rather use?</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580"><p>Let&rsquo;s move on to a discussion of frequency in the next section.</p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="01_03.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
