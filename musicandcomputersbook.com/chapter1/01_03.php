<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>


		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="01_04.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="21" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 1: The Digital Representation of Sound,<br>
						Part One: Sound and Timbre</h1>
					<h1>Section 1.3: Frequency, Pitch, and Intervals<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE235721'));return CSClickReturn()" href="../popups/chapter1/xbit_1_4.php" target="_blank" csclick="BCE235721"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 1.4</b><br>
						
						<span class="icon_caption">Tapping a<br>frequency</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>What is frequency? Essentially, it&#146;s a measurement of how often a given event repeats in time. If you subscribe to a daily paper, then the frequency of paper delivery could be described as once per day, seven times per week. When we talk about the frequency of a sound, we&#146;re referring to how many times a particular pattern of amplitudes repeats during one second.</p>
					<p>Not all waveforms or physical vibrations repeat exactly (in fact almost none do!). But many vibratory phenomena, especially those in which we perceive some sort of pitch, repeat approximately regularly. If we assume that in fact they are repeating, we can measure the rate of repetition, and we call that the waveform&#146;s <i>frequency.</i><br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE235540'));return CSClickReturn()" href="../applets/1_3_hear_freq.php" target="_blank" csclick="BCE235540"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 1.4</b><br>
							<span class="icon_caption">Hearing frequency and amplitude</span><br>
						</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This applet lets you hear and see what different frequencies and amplitudes look/sound like. Try to see if you can predict what a tone will sound like before you change it.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/2sines.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button20" border="0" alt=""></a><br>
						<b>Soundfile 1.14</b><br>
						<span class="icon_caption">Two sine waves, different frequencies</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush_top"><img height="203" width="492" src="../images/chapter1/octaves.jpg"></p>
					<p class="figure_caption_flush_bottom"><b>Figure 1.14&nbsp;&nbsp;</b>Two sine waves. The frequency of the red wave is twice that of the blue one, but their amplitudes are the same. It would be difficult or impossible to actually hear this as two <i>distinct</i> tones, since the octaves fuse into one sound.<br>
						<br>
						As we&#146;ll discuss later, we measure frequencies in cycles per second, or hertz (Hz). Click on the soundfile icon to hear two sine tones: one at 400&nbsp;Hz and one at 800&nbsp;Hz.</p>
					<h2>Sine Waves</h2>
					<p>A sine wave is a good example of a repeating pattern of amplitudes, and in some ways it is the simplest. That&#146;s why sine waves are sometimes referred to as simple harmonic motions. Let&#146;s arbitrarily fix an amplitude scale to be from &ndash;1 to 1, so the sine wave goes from 0 to 1 to 0 to &ndash;1 to 0. If the complete cycle of the sine wave&#146;s curve takes one second to occur, then we say that it has a frequency of one cycle per second (cps), or one <i>hertz</i> (Hz, or kHz for 1,000&nbsp;Hz).</p>
					<p>The frequency range of sound (or, more accurately, of human hearing) is usually given as <i>0&nbsp;Hz to 20 kHz,</i> but our ears don&#146;t fuse very low frequency oscillations (0&nbsp;Hz to 20&nbsp;Hz, called the <i>infrasonic</i> range) into a pitch. Low frequencies just sound like beats. These numbers are just averages: some people hear pitches as low as 15&nbsp;Hz; others can hear frequencies significantly higher than 20 kHz. A lot depends on the amplitude, the timbre, and other factors. The older you get (and the more rock n&#146; roll you listened to!), the more your ears become insensitive to high frequencies (a natural biological phenomenon called <i>presbycusis</i>).<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE236504'));return CSClickReturn()" href="../applets/1_3_hear_ticks_to_tone.php" target="_blank" csclick="BCE236504"><img src="../images/global/applet.gif" width="80" height="65" name="3" border="0"></a><br>
						<b>Applet 1.5</b><br>
						<span class="icon_caption">When ticks become tones</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Pulses change into fused sounds with a discernible pitch at a certain rate, which is somewhere between 15&nbsp;Hz to 25&nbsp;Hz. This applet lets you move a pulse&#146;s frequency in and out of that range. Where do you hear it fuse into a pitch?</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<div align="center">
						<br>
						<table border="0" cellpadding="5" cellspacing="1" bgcolor="black">
							<tr>
								<td nowrap bgcolor="white"><b>Source</b></td>
								<td nowrap bgcolor="white"><b>Lowest freqency (Hz)</b></td>
								<td nowrap bgcolor="white"><b>Highest frequency (Hz)</b></td>
							</tr>
							<tr>
								<td nowrap bgcolor="white">piano</td>
								<td nowrap bgcolor="white" align="right">27.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td nowrap bgcolor="white" align="right">4,186&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td nowrap bgcolor="white">female speech</td>
								<td nowrap bgcolor="white" align="right">140&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td nowrap bgcolor="white" align="right">500&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td nowrap bgcolor="white">male speech</td>
								<td nowrap bgcolor="white" align="right">80&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td nowrap bgcolor="white" align="right">240&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td nowrap bgcolor="white">compact disc</td>
								<td nowrap bgcolor="white" align="right">0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td nowrap bgcolor="white" align="right">22,050&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td nowrap bgcolor="white">human hearing</td>
								<td nowrap bgcolor="white" align="right">20&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td nowrap bgcolor="white" align="right">20,000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
						</table>
					</div>
					<p class="figure_caption"><b>Table 1.2</b>&nbsp;&nbsp;Some frequency ranges.</p>
					<h2>Period</h2>
					<p>When we talk about <i>frequency</i> in music, we are referring to how often a sonic event happens in its entirety over the course of a specified time segment. For a sound to have a perceived frequency, it must be <i>periodic</i> (repeat in time). Since the period of an event is the length of time it takes the event to occur, it&#146;s clear that the two concepts (periodicity and frequency) are related, if not pretty much equivalent.</p>
					<p>The <i>period</i> of a repeating waveform is the length of time it takes to go through one cycle. The frequency is sort of the inverse: how many times the waveform repeats that cycle per unit time. We can understand the periodicity of sonic events just like we understand that the period of a daily newspaper delivery is one day. </p>
					<p>Since a 20&nbsp;Hz tone by definition is a cycle that repeats 20 times a second, then in 1/20th of a second one cycle goes by, so a 20&nbsp;Hz tone has a period of 1/20 or 0.05 second. Now the &quot;thing&quot; that repeats is one basic unit of this regularly repeating wave&#151;such as a sine wave (at the beginning of this section there&#146;s a picture of two sine waves together). It&#146;s not hard to see that the time it takes for one copy of the basic wave to recur (or move through whatever medium it is in) is proportional to the distance from crest to crest (or any two successive corresponding points, for that matter) of the sine wave. </p>
					<p>This distance is called the <i>wavelength</i> of the wave (or of the periodic function). In fact, if you know how fast the wave is moving, then it is easy to figure out the wavelength from the period. </p>
					<p>Physically, the period is inversely proportional to the wavelength. Wavelength is a spatial measure that says how far the wave travels in space in one period. We measure it in distance, not time. The speed of sound (<i>s</i>) is about 345 meters/second. To find the wavelength (<i>w</i>) for a sound of a given frequency, first we invert the frequency (<i>1/f</i>) to get its period (<i>p</i>), and then we use the following simple formula:</p>
					<p><img height="38" width="128" src="../images/formulas/1.3.1.form.jpg"></p>
					<p><img height="61" width="131" src="../images/formulas/1.3.2.form.jpg"><br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter1/gong.clip.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button22" border="0" alt=""></a><br>
						<b>Soundfile 1.15</b><br>
						<span class="icon_caption">Low gong</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush_top"><img height="244" width="270" src="../images/chapter1/gong.jpg"></p>
					<p class="figure_caption"><b>Figure 1.15&nbsp;&nbsp;</b>Very low musical sounds can have very long wavelengths: some central Javanese (from Indonesia) gongs vibrate at around 8&nbsp;Hz to 10&nbsp;Hz, and as such their wavelengths are on the order of 30 to 35 m. Look at the size of the gong in this photo! It makes some very low sounds.<br>
						<br>
						Click on the sound icon above to hear a gong.<br>
						<br>
						(The musicians above are from STSI Bandung, a music conservatory in West Java, Indonesia, participating in a recording session for a piece called &quot;mbuh,&quot; by the contemporary composer Suhendi. This recording can be heard on the CD <i>Asmat Dream: New Music Indonesia</i>, Lyrichord Compact Disc #7415.)</p>
					<p>Using the formula, we find that the wavelength of a 1&nbsp;Hz tone is 345 meters, which makes sense, since a 1&nbsp;Hz tone has a period of 1 second, and sound travels 345 meters in one second! That&#146;s pretty far, until you realize that, since these waveforms are usually symmetrical, if you were standing, say, at 172.5 meters from a vibrating object making a 1&nbsp;Hz tone and right behind you was a perfectly reflective surface, it&#146;s entirely possible that the negative portion of the waveform might cancel out the positive and you&#146;d hear nothing! While this is a rather extreme and completely hypothetical example, it is true that <i>wave cancellation</i> is a common physical occurrence, though it depends on a great many parameters.</p><br>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">

				</td>
				<td valign="top" bgcolor="white" width="580">
					<h2>Pitch</h2>
					<p>Musicians usually talk to each other about the frequency content of their music in terms of <i>pitch,</i> or sets of pitches called <i>scales</i>. You&#146;ve probably heard someone mention a G-minor chord, a blues scale, or a symphony in C, but has anyone ever told you about the new song they wrote with lots of 440 cycles per seconds in it? We hope not!</p>
					<p>Humans tend to recognize relative relationships, not absolute physical values. And when we do, those relationships (especially in the aural domain) tend to be <i>logarithmic.</i> That is, we don&#146;t perceive the difference (subtraction) of two frequencies, but rather the ratio (division). </p>
					<p>This means that it is much easier for most humans to hear or describe the relationship or ratio between two frequencies than it is to name the exact frequencies they are hearing. And in fact, for most of us, the exact frequencies aren&#146;t even very important&#151;we recognize &quot;Row, Row, Row Your Boat&quot; regardless of what frequency it is sung at, as long as the relationships between the notes are more or less correct. The common musical term for this is <i>transposition</i>: we hear the tune correctly no matter what <i>key</i> it&#146;s sung in.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2399311'));return CSClickReturn()" href="../applets/1_3_hear_linear_transpose.php" target="_blank" csclick="BCE2399311"><img src="../images/global/applet.gif" width="80" height="65" name="4" border="0"></a><br>
						<b>Applet 1.6</b><br>
						<span class="icon_caption">Transpose using multiply and add</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This applet shows what would happen if a simple melody were transposed <i>linearly,</i> as opposed to <i>logarithmically</i>, in other words, if we perceived frequency differences rather than frequency ratios. Note that the <i>contour</i> (the ups and downs of the pitches) remains, but intuitively we tend not to recognize the operation of addition in frequency, but multiplication.</p>
					<p class="applet_caption">That&#146;s what logarithmic perception is all about.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<p><br>
						Although pitch is directly related to frequency, they&#146;re not the same. As we pointed out earlier, and similar to what we saw in Section 1.2 when we discussed amplitude and loudness, frequency is a <i>physical</i> or acoustical phenomenon. Pitch is <i>perceptual</i> (or psychoacoustic, cognitive, or psychophysical). The way we organize frequencies into pitches will be no surprise if you understood what we talked about in the previous section: we require more and more change in frequency to produce an equal perceptual change in pitch. </p>
					<p>Once again, this is all part of that logarithmic perception &quot;thing&quot; we&#146;ve been yammering on about, because the way we describe that increase is by logarithms and exponentials. Here&#146;s a simple example: the difference to our ears between 101&nbsp;Hz and 100&nbsp;Hz is much greater than the difference between 1,001&nbsp;Hz and 1,000&nbsp;Hz. We don&#146;t hear a change of 1&nbsp;Hz for each; instead we hear a change of 1,001/1,000 (= 1.001) as compared to a much bigger change of 101/100 (= 1.01).</p>

					<h2>Intervals and Octaves</h2>
					<p>So we don&#146;t really care about the <i>linear,</i> or arithmetic, differences between frequencies; we are almost solely interested in the <i>ratio</i> of two frequencies. We call those ratios <i>intervals,</i> and almost every musical culture in the world has some term for this concept. In Western music, the 2:1 ratio is given a special importance, and it&#146;s called an <i>octave.</i> </p>
					<p>It seems clear (though not totally unarguable) that most humans tend to organize the frequency spectrum between 20&nbsp;Hz and 20 kHz roughly into <i>octaves,</i> which means powers of 2. That is, we perceive the same pitch difference between 100&nbsp;Hz and 200&nbsp;Hz as we do between 200&nbsp;Hz and 400&nbsp;Hz, 400&nbsp;Hz and 800&nbsp;Hz, and so on. In each case, the ratio of the two frequencies is 2:1. We sometimes call this <i>base-2 logarithmic perception.</i> Many theorists believe that the octave is somehow fundamental to, or innate and hard-wired in, our perception, but this is difficult to prove. It&#146;s certainly common throughout the world, though a great deal of approximation is tolerated, and often preferred!<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2399F12'));return CSClickReturn()" href="../applets/1_3_octave_stretch.php" target="_blank" csclick="BCE2399F12"><img src="../images/global/applet.gif" width="80" height="65" name="5" border="0"></a><br>
						<b>Applet 1.7</b><br>
						<span class="icon_caption">Octave quiz</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Do you prefer your octaves pure, or just a little bit &quot;off&quot;? Most people seem to &quot;prefer&quot; octaves (2:1 frequency ratios) that are just a little bit wide, or sharp.</p>
					<p class="applet_caption">An octave is a very special frequency relationship that seems to have near universal importance, but nobody is exactly sure why (though a lot of people think they know!).</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<p><br>
						In almost all musical cultures, pitches are named not by their actual frequencies, but as general categories of frequencies in relationship to other frequencies, all a power of 2 apart. For example, A is the name given to the pitch on the piano or clarinet with a frequency of 440&nbsp;Hz as well as 55&nbsp;Hz, 110&nbsp;Hz, 220&nbsp;Hz, 880&nbsp;Hz, 1760&nbsp;Hz, and so on. The important thing is the <i>ratio</i> between the frequencies, not the distance; for example, 55&nbsp;Hz to 110&nbsp;Hz is an octave that happens to span 55&nbsp;Hz, yet 50&nbsp;Hz to 100&nbsp;Hz is also an octave, even though it only covers 50&nbsp;Hz. But if an orchestra tunes to a different A (as most do nowadays, for example, to middle A = 441&nbsp;Hz or 442&nbsp;Hz to sound higher and brighter), those frequencies will all change to be multiples/divisors of the new absolute A.</p>
					<p class="figure"><img height="323" width="145" src="../images/chapter1/linlog.jpg" hspace="10" vspace="10"></p>
					<p class="figure_caption"><b>Figure 1.16&nbsp;&nbsp;</b>The red graph shows a series of octaves starting at 110&nbsp;Hz (an A); each new octave is twice as high as the last one. The blue graph shows <i>linearly</i> increasing frequencies, also starting at 110 Hz.<br>
						<br>
						We say that the frequencies in the blue graph are rising linearly, because to get each new frequency we simply add 110&nbsp;Hz to the last frequency: the change in frequency is always the same. However, to get octaves we must double the frequency, meaning that the difference (subtractively) in frequency between two adjacent octaves is always increasing.</p>
					<p>One thing is clear, however: to have pitch, we need frequency, and thus periodic waveforms. Each of these three concepts implies the other two. This relationship is very important when we discuss how frequency is not used just for pitch, but also in determining timbre. To get some sense of this, consider that the highest note of a piano is around 4 kHz. What about the rest of the range, the almost 18 kHz of available sound? It turns out that this larger frequency range is used by the ear to determine a sound&#146;s timbre. We will discuss timbre in Section 1.4.</p>
					<p>Before we move on to timbre, though, we should mention that pitch and amplitude are also related. When we hear sounds, we tend to compare them, and think of their amplitudes, in terms of loudness. The perceived loudness of a sound depends on a combination of factors, including the sound&#146;s amplitude and frequency content. For example, given two sounds of very different frequencies but at exactly the same amplitude, the lower-frequency sound will often seem softer. Our ear tends to amplify certain frequencies and attenuate others.</p><br>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<a onclick="CSAction(new Array(/*CMP*/'BCE239AA13'));return CSClickReturn()" href="../applets/1_3_fletcher_munson.php" target="_blank" csclick="BCE239AA13"><img src="../images/global/applet.gif" width="80" height="65" name="6" border="0"></a><br>
						<b>Applet 1.8</b><br>
						
						Fletcher-Munson example</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<h2>Fletcher-Munson Curves</h2>
					<p class="figure"><img height="236" width="290" src="../images/chapter1/fletchmuns.jpg" hspace="10" vspace="10"></p>
					<p class="figure_caption"><b>Figure 1.17&nbsp;&nbsp;</b>Equal-loudness contours (often referred to as Fletcher-Munson curves) are curves that tell us how much intensity is needed at a certain frequency in order to produce the same perceived loudness as a tone at a different frequency. They&#146;re sort of &quot;loudness isobars.&quot; If you follow one line (which meanders across intensity levels and frequencies), you&#146;re following an equal loudness contour.<br>
						<br>
						For example, for a 50&nbsp;Hz tone to sound as loud as a 1000&nbsp;Hz tone at 20 dB, it needs to sound at about 55 dB. These curves are surprising, and they tell us some important things about how our ears evolved.<br>
						<br>
						While these curves are the result of rather gross data generalization, and while they will of course vary depending on the different sonic environments to which listeners are accustomed, they also seem to be rather surprisingly accurate across cultures. Perhaps they represent something that is hardwired rather than learned. These curves are widely used by audio manufacturers to make equipment more efficient and sound more realistic.</p>
					<p>When looking at Figure 1.17 for the Fletcher-Munson curves, note how the curves start high in the low frequencies, dip down in the mid-frequencies, and swing back up again. What does this mean? Well, humans need to be very sensitive to the mid-frequency range. That&rsquo;s how, for instance, you can tell immediately if your mom&rsquo;s upset when she calls you on the phone. (Phones, by the way, cut off everything above around 7 kHz.)</p>
					<p>Most of the sounds we need to recognize for survival purposes occur in the mid-frequency range. Low frequencies are not too important for survival. The nuances and tiny inflections in speech and most of sonic sound tend to happen in the 500 Hz to 2 kHz range, and we have evolved to be extremely sensitive in that range (though it&#146;s hard to say which came first, the evolution of speech or the evolution of our sensitivity to speech sounds.</p>
					<p>This mid-frequency range sensitivity is probably a universal human trait and not all that culturally dependent. So, if you&#146;re traveling far from home, you may not be able to understand the person at the table in the caf&eacute; next to you, but if you whistle the Fletcher-Munson curves, you&#146;ll have a great time together.</p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="01_04.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>


<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
