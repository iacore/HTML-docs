<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
				<tr>
					<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
				</tr>
			</table>
			<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
				<tr>
					<form name="FormName" method="get">
						<td align="right"><input type="button" name="CloseWindow" value="Close" onClick="window.close();"></td>
						<td align="center" width="20">&nbsp;</td>
					</form>
				</tr>
			</table>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
				<tr>
					<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
				</tr>
			</table>
		</csobj>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>Make a Noisy Copy</h1>
					When you make a copy of an audiocassette tape or a VHS tape, the copy has more noise than the original. If you make a copy of the copy, then you will get even more noise. Noise can be thought of as a random modification of the original. This principle is illustrated in this applet, which makes a copy of a melody. Since we are working in the digital domain, we could make a perfect copy each time. But instead, when we make a copy of the melody we will alter it randomly in a subtle way.
					<p>To do:</p>
					<ul>
						<li>Click on the drawing of the melody to hear it. It is the box with the dots.
						<li>Make a copy by hitting the Make a Noisy Copy button. It may be exactly the same or slightly different.
						<li>Play the copy by clicking on it.
						<li>Keep making copies and notice how they get further and further from the original.
						<li>Hit Reset and try it again with a new song.
					</ul>
					<div align="center">
						<iframe src="../jsApps/2.6_Add_Noise_To_Song.html" width=100% height=500 ></iframe>
					</div>
				</td>
			</tr>
		</table>
		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form>
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
