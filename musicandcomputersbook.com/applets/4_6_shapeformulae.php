<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
				<tr>
					<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
				</tr>
			</table>
			<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
				<tr>
					<form name="FormName" method="get">
						<td align="right"><input type="button" name="CloseWindow" value="Close" onClick="window.close();"></td>
						<td align="center" width="20">&nbsp;</td>
					</form>
				</tr>
			</table>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
				<tr>
					<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
				</tr>
			</table>
		</csobj>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>Changing the Shape of a Waveform</h1>
					<p>How it works:</p>
					A sine wave can be played by pressing keys on the computer
      keyboard. This sine wave passes through a chain of WaveShapers that distorts
      the shape of the sine wave and adds harmonics. Don&#146;t worry if it sounds
      distorted&#151;it&#146;s supposed to. Distortion in an audio system is often
      just unintentional WaveShaping that occurs when the signal exceeds the limits
      of the system. Whether that is good or bad is your decision.
      <p>To do:<p>
					<ul>
						<li>Scroll down until you can see the black oscilloscope display.
						<li>Play sine waves by touching keys on the keyboard. Not all keys make sound. Each row is an octave higher. The tuning is a seven-tone scale using just intonation.

        <li>If the keyboard ever stops playing notes, click on the Play Keys button.
        <li>Enable one WaveShaper at a time by checking the box next to the formula.
						<li>Increase the amplitude while holding down a key and notice how the shape changes. Also notice what happens when you reach the top and bottom. You can get clipping even without a WaveShaper.
						<li>Play a chord and notice that the sound changes dramatically when WaveShaping is turned on. This is because you are now shaping a signal that is more complex than a single sine wave. This results in enharmonic partials. This could be avoided by putting an individual WaveShaper on each sine wave. If the function is linear, &quot;y=ax+b&quot;, then it doesn&#146;t matter whether we do the WaveShaping before or after we mix (add) them together. But if the WaveShaping function is nonlinear, &quot;y=x<sup>3</sup>&quot; or &quot;y=sin(x)&quot;, then we get different results depending on whether we apply the WaveShaper before or after mixing.

        <li>Play a low note. Enable the first WaveShaper &quot;y=x<sup>2</sup>&quot;.
          Play the same note. Notice that the note sounds an octave higher. This
          is because the shape of a <I>x</I><sup>2</sup> is a parabola that is
          symmetric about the <I>y-</I>axis.
        <li>Turn on several WaveShapers at once. Each one operates on the output of the one above, so &quot;y=g(f(x))&quot;.
						<li>The last WaveShaper is a &quot;soft clipper&quot;:  &quot;y=x/(1+|x|)&quot; will never exceed the range of &ndash;1.0 or +1.0. So it prevents the signal from going beyond the limits that the audio card can handle.
					</ul>
					<div align="center">
						<iframe src="../jsApps/4.6_WaveShaper_Formulae.html" width=100% height=600 ></iframe>

					</div>
				</td>
			</tr>
		</table>
		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form>
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
