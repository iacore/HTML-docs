<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
				<tr>
					<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
				</tr>
			</table>
			<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
				<tr>
					<form name="Cmp00301F4A57FormName" method="get">
						<td align="right"><input type="button" name="Cmp00301F4A57CloseWindow" value="Close" onClick="window.close();"></td>
						<td align="center" width="20">&nbsp;</td>
					</form>
				</tr>
			</table>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
				<tr>
					<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
				</tr>
			</table>
		</csobj>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">
			<tr>
				<td>
					<h1>Building a Sawtooth using Sine Waves</h1>

<p><b>How it works:</b>
<br>
A sawtooth wave can be constructed from sine waves.
In this Applet we start with a sine wave and add more and more harmonics until we get to something like a sawtooth.
Click the &quot;Add Harmonic&quot; button to start building the sawtooth wave.
<p>If you add lots of harmonics, and then move the frequency fader to a high value then you may hear aliasing. This is because the higher harmonics are above the Nyquist Rate.
					<div align="center">
						<iframe src="../jsApps/3.3_Build_Saw_Using_Sines.html" width=100% height=400 ></iframe>
          </div>
				</td>
			</tr>
		</table>

		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form>
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
