// This used to embed a native code plugin. Now it just provides
// a link back to JSyn for tech support.
	
if( !navigator.javaEnabled() )
{
	document.writeln("<p><b>Java is NOT enabled for your browser.");
	document.writeln("You cannot run JSyn without Java!</b></p>");
}
else
{
	document.writeln('<p>Applet uses <a HREF="http://www.softsynth.com/jsyn/">JSyn</a> for interactive audio.</p>');
}
