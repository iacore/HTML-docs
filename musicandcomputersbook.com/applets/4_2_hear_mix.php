<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
				<tr>
					<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
				</tr>
			</table>
			<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
				<tr>
					<form name="FormName" method="get">
						<td align="right"><input type="button" name="CloseWindow" value="Close" onClick="window.close();"></td>
						<td align="center" width="20">&nbsp;</td>
					</form>
				</tr>
			</table>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
				<tr>
					<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
				</tr>
			</table>
		</csobj>
		<table width="100%" border="0" cellspacing="2" cellpadding="16">

  <tr>
    <td> <h1>Hearing Mixed Sounds and Clipping</h1>
      Sounds can be mixed together by adding together the numbers that represent
      the sounds. In order to control their relative loudness, we can change the
      amplitude of the individual sounds. In this applet, we mix two SineOscillators
      using an AddUnit.
      <p><b>Important: the sound card in your computer can only handle a limited
        range of numbers. </b>That range typically corresponds to a range of &ndash;1.0
        to +1.0 in JSyn. If you add together oscillators and their sum goes beyond
        that range, then the result will be &quot;clipped&quot; to fit within
        this range. This can introduce severe distortion and can sound very bad
        if you are trying to produce a smooth tone. If you are trying to produce
        a nasty sound, then this can be a useful technique. Guitar distortion
        boxes use a similar technique, but they do not clip abruptly at &ndash;1.0
        and +1.0. They have a gradual distortion that imitates the saturation
        of a tube amplifier. To prevent clipping, set the amplitudes of your sounds
        so that they cannot sum to over 1.0. For example, if you have <I>N</I>
        oscillators, set the amplitude to (1.0/<I>N</I>).</p>
      <p>To do:</p>
      <ul>
        <li>Set the amplitude of each oscillator to about 0.4. </li>
        <li>Change the frequencies of each oscillator so that you can hear each
          of them distinctly. </li>
        <li>For safety, turn down the volume of your stereo speakers until it
          is at a soft level. </li>
        <li>Set the amplitudes of each oscillator to about 0.6. See and hear the
          clipping that results. </li>
        <li>The scope shows the color-coded output of both oscillators and the
          mixer. Uncheck the Auto box to stop the scope. Convince yourself that
          the mixer output is the sum of the two oscillators. (It may appear to
          be very slightly delayed.) </li>
        <li>Set both the amplitudes to 0.4. Set the frequencies almost the same
          but not quite. The wavering effect is called &quot;beating.&quot; Notice
          that the mixed signal is large when the oscillators rise together, but
          small when they move in opposite directions. The beat frequency is the
          difference in frequencies of the two oscillators. </li>
      </ul>
      <div align="center">
				<iframe src="../jsApps/4.2_Hear_Mixed_Sines.html" width=100% height=600 ></iframe>

      </div></td>
			</tr>
		</table>
		<center>
		<form method="post">
		<input type="button" value="Close" onclick="window.close()">
		</form>
		</center>
			<br>
			<hr>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
