<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_07.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 5: The Transformation of Sound by Computer</h1>
					<h1>Section 5.6: <b>Morphing</b><br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				
    <td valign="top" bgcolor="white" width="580"><p>In recent years the idea of 
        <i>morphing</i>, or turning one sound (or image) into another, has become 
        quite popular. What is especially interesting, besides the idea of having 
        a lion roar change gradually and imperceptibly into a meow, is the broader 
        idea that there are sounds &quot;in between&quot; other sounds. </p>
      <p class="figure"><img height="211" width="139" src="../images/chapter5/tom.gif"></p>
					<p class="figure_caption"><b>Figure 5.15&nbsp;&nbsp;</b>Image morphing: several stages of a morph.</p>
					
      <p>What does it mean to change one sound into another? Well, how would you 
        graphically change a picture into another? Would you replace, over time, 
        little bits of one picture with those of another? Would you gradually 
        change the most important shapes of one into those of the other? Would 
        you look for important features (background, foreground, color, brightness, 
        saturation, etc.), isolate them, and cross-fade them independently? You 
        can see that there are lots of ways to morph a picture, and each way produces 
        a different set of effects. The same is true for sound.<br>
        <br>
      </p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/51melodies_edit.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 5.22<br>
							 &quot;morphing piece&quot;</b></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Larry Polansky's &quot;51 Melodies&quot; is a terrific example of a computer-assisted composition that uses morphing to generate novel (and kind of insane!) melodies. Polansky specified the <i>source</i> and <i>target</i> melodies, and then wrote a computer program to generate the melodies in between. From the liner notes to the CD &quot;Change&quot;:</p>
					<p class="applet_caption">&quot;51 Melodies is based on two melodies, a <i>source</i> and a <i>target</i>, and is in three sections. The piece begins with the <i>source,</i> a kind of pseudo-anonymous rock lick. The target melody, an octave higher and more chromatic, appears at the beginning of the third section, played in unison by the guitars and bass. The piece ends with the source. In between, the two guitars morph, in a variety of different, independent ways, from the source to the target (over the course of Sections 1 and 2) and back again (Section 3). A number of different morphing functions, both durational and melodic, are used to distinguish the three sections.&quot;</p>
					<p class="applet_caption">(The Soundfile 5.22 is an three minute edited version of the complete 12 minute piece, composed of one minute sections from the beginning, middle, and end of the recording.)</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<h2>Simple Morphing</h2>
					
      <p>The simplest <i>sonic morph</i> is essentially an amplitude cross-fade. Clearly, this doesn&rsquo;t do much (you could do it on a little audio mixer).&nbsp;</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<a onclick="setSrc('../sounds/chapter5/crossfade.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 5.23</b><br>
        Cross-fade</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure"><img height="185" width="314" src="../images/chapter5/crossfade.jpg"></p>
					
      <p class="figure_caption"><b>Figure 5.16&nbsp;&nbsp;</b>An amplitude cross-fade 
        of a number of different data points.</p>
					
      <p>What would constitute a more interesting morph, even limiting us to the 
        time domain? How about this: let&#146;s take a sound and gradually replace 
        little bits of it with another sound. If we overlap the segments that 
        we&#146;re &quot;replacing,&quot; we will avoid horrible clicks that will 
        result from samples jumping drastically at the points of insertion. </p>
					<h2>Interpolation and Replacement Morphing</h2>
					
      <p>The two ways of morphing described above might be called replacement 
        and interpolation morphing, respectively. In a <i>replacement morph,</i> 
        intact values are gradually substituted from one sound into another. In 
        an <i>interpolation morph,</i> we compare the values between two sounds 
        and select values somewhere between them for the new sound. In the former, 
        we are morphing completely some part of the time; in the latter, we are 
        morphing somewhat all of the time.</p>
					<p>In general, we can specify a degree of morphing, by convention called &Omega;, that tells how far one sound is from the other. A general formula for (linear) interpolation is:</p>
					
      <p><i>I</i> = <i>A</i> + (&Omega;*(<i>B</i> &ndash;<I> A</i>))</p>
					
      <p>In this equation, <i>A</i> is the starting value, <i>B</i> is the ending 
        value, and &Omega; is the interpolation index, or &quot;how far&quot; 
        you want to go. Thus, when &Omega; = 0, <i>I</i> = <i>A</i>; when &Omega; 
        = 1, <i>I</i> = <i>B</i>, and when &Omega; = 0.5, <i>I</i> = the average 
        of <i>A</i> and <i>B</i>. </p>
					<p>This equation is a complicated way of saying: take some sound (<i>SourceSound</i>) and add to it some percentage of the difference between it and another sound (TargetSound &ndash; SourceSound), to get the new sound.</p>
					<p>Sonic morphing can be more interesting in the frequency domain, in the creation of sounds whose spectral content is some kind of hybrid of two other sounds. (Convolution, by the way, could be thought of as a kind of morph!) </p>
					
      <p>An interesting approach to morphing is to take some feature of a sound 
        and morph that feature onto another sound, trying to leave everything 
        else the same. This is called <i>feature morphing</i>. Theoretically, 
        one could take any mathematical or statistical feature of the sound, even 
        perceptually meaningless ones&#151;like the standard deviation of every 
        13th bin&#151;and come up with a simple way to morph that feature. This 
        can produce interesting effects. But most researchers have concentrated 
        their efforts on features, or some organized representation of the data, 
        that are perceptually, cognitively, or even musically salient, such as 
        attack time, brightness, roughness, harmonicity, and so on, finding that 
        feature morphing is most effective on such perceptually meaningful features.</p>
					<h2>Feature Morphing Example: Morphing the Centroid</h2>
					<p>Music cognition researchers and computer musicians commonly use a measure of sounds called the <i>spectral centroid.</i> The spectral centroid is a measure of the &quot;brightness&quot; of a sound, and it turns out to be extremely important in the way we compare different sounds. If two sounds have a radically different centroid, they are generally perceived to be timbrally distant (sometimes this is called a <i>spectral metric</i>).</p>
					<p>Basically, the <i>centroid</i> can be considered the average frequency component (taking into consideration the amplitude of all the frequency components). The formula for the spectral centroid of one FFT frame of a sound is:</p>
					
      <p class="figure"><img src="../images/chapter5/centroid.form.jpg" width="130" height="99"></p>
					<p><i>C<sub>i</sub></i> is the centroid for one spectral frame, and <i>i</i> is the number of frames for the sound. A spectral frame is some number of samples that is equal to the size of the FFT.</p>
					<p>The (individual) centroid of a spectral frame is defined as the average frequency weighted by amplitudes, divided by the sum of the amplitudes, as follows:</p>
					
      <p class="figure"><img src="../images/chapter5/centroid2.form.jpg" width="217" height="140"></p>

					<p>We add up all the frequencies multiplied by their amplitudes (the numerator) and add up all the amplitudes (the denominator), and then divide. The &quot;strongest&quot; frequency wins! In other words, it&#146;s the average frequency weighted by amplitude: where the frequency concentration of a sound is.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<br><br>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/chris.choir.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a><br>
						<b>Soundfile 5.24</b><br>
						Chris Mann
						</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/SingleViolin.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button34" border="0" alt=""></a><br>
						<b>Soundfile 5.25</b><br>
						Single violin</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure"><img height="388" width="550" src="../images/chapter5/mann.centroid.jpg"></p>
					
      <p class="figure_caption"><b>Figure 5.17&nbsp;&nbsp;</b>The centroid curve 
        of a sound over time. Note that centroids tend to be suprisingly high 
        and never the &quot;fundamental&quot; (unless our sound is a pure sine 
        wave). One of these curves is of a violin tone; the other is of a rapidly 
        changing voice (Australian sound poet Chris Mann). The soundfile for Chris 
        Mann is included as well.<br>
						<br>
					</p>
					<p>Now let&#146;s take things one step further, and try to morph the centroid of one sound onto that of another. Our goal is to take the time-variant centroid from one sound and graft that onto a second sound, preserving as much of the second sound&#146;s amplitude/spectra relationship as possible. In other words, we&#146;re trying to morph one feature while leaving others constant.</p>
					<p>To do this, we can think of the centroid in an unusual way: as the frequency 
        that divides the total sound file energy into two parts (above and below). 
        That&#146;s what an <i>average </i>is. For some time-variant centroid 
        (<i>c<sub>i</sub></i>) extracted from one sound and some total amplitude 
        from another (<i>ampsum</i>), we simply &quot;plop&quot; the new centroid 
        onto the sound and scale the amplitude of the frequency bins above and 
        below the new centroid frequency to (<i>0.5 * ampsum</i>). This will produce 
        a sort of &quot;brightness morph.&quot; Notice that on either side of 
        the centroid in the new sound, the spectral amplitude relationships remain 
        the same. We&#146;ve just forced a new centroid.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_07.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>

