<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="5" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 5: The Transformation of Sound by Computer</h1>
					<h1>Section 5.7: <b>Graphical Manipulation of Sound</b><br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>

    <td valign="top" bgcolor="white" width="580"><p>We&#146;ve seen how it&#146;s
        possible to take sounds and turn them into pictures by displaying their
        spectral data in sonograms, waterfall plots, and so on. But how about
        going the other way? What about creating a <i>picture</i> of a sound,
        and then synthesizing it into an actual sound? Or how about starting with
        a picture of something else&#151;Dan Rockmore&#146;s dog Digger, for instance?
        What would he sound like? Or how about editing a sound as an image&#151;what
        if you could draw a box around some region of a sound and simply drag
        it to some other place, or erase part of it, or apply a &quot;blur&quot;
        filter to it? </p>
      <p>Graphical manipulation of sound is still a relatively underdeveloped
        and emerging field, and the last few years have seen some exciting developments
        in the theory and tools needed to do such work. One of the interesting
        issues about it is that the graphical manipulations may not have any obvious
        relationship to the sonic effects. This could be looked upon as either
        a drawback or an advantage. Although graphical techniques are used for
        both the synthesis and transformation of sounds, much of the current work
        in this area seems geared more toward sonic manipulation than synthesis.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<!-- <p class="icon_caption"><br>
						<br>
						<br>
						<br>
						<a onclick="CSAction(new Array(/*CMP*/'BD47939D0'));return CSClickReturn()" href="../applets/5_7_color_organ.php" target="_blank" csclick="BD47939D0"><img src="../images/global/applet.gif" width="80" height="65" name="3" border="0"></a><br>
						<b>Applet 5.6</b><br>
							Color organ, drawing sound</p> -->
				</td>

    <td valign="top" bgcolor="white" width="580"> <h2>Pre&#8211;Digital Era Graphic
        Manipulation</h2>

      <p>Composers have always been interested in exploring the relationships
        between color, shape, and sound. In fact, people in general are fascinated
        with this. Throughout history, certain people have been <i>synaesthetic</i>&#151;they
        see sound or hear color. Color organs, sound pictures, even just visual
        descriptions of sound have been an important part of the way people try
        to understand sound and music, and most importantly their own experience
        of it. Timbre is often called &quot;sound color,&quot; even though sound
        color should more appropriately be analogized to frequency/pitch. </p>

      <p>Computer musicians have often been interested in working with sounds
        from a purely graphical perspective&#151;a &quot;let&#146;s see what would
        happen if&quot; kind of approach. Creating and editing sounds graphically
        is not a new idea, although it&#146;s only recently that we&#146;ve had
        tools flexible enough to do it well. Even before digital computers, there
        were a number of graphics-to-sound systems in use. In fact, some of the
        earliest film sound technology was optical&#151;a waveform was printed,
        or even drawn by hand (as in the wonderfully imaginative work of Canadian
        filmmaker Norman McLaren), on a thin stripe of film running along the
        socket holes. A light shining through the waveform allowed electronic
        circuitry to sense and play back the sound.</p>
					<p>Canadian inventor and composer Hugh LeCaine took a different approach. In the late 1950s, he created an instrument called the <i>Spectrogram,</i> consisting of a bank of 108 analog sine wave oscillators controlled by curves drawn on long rolls of paper. The paper was fed into the instrument, which sensed the curves and used them to determine the frequency and volume of each oscillator. Sound familiar? It should&#151;LeCaine&#146;s Spectrogram was essentially an analog additive synthesis instrument!</p>
					<p class="figure"><img height="224" width="276" src="../images/chapter5/lecaine.jpg"><img height="403" width="272" src="../images/chapter5/spectro.gif"></p>
					<p class="figure_caption"><b>Figure 5.18</b>&nbsp;&nbsp;Canadian composer and instrument builder, and one of the great pioneers of electronic and computer music, Hugh LeCaine. LeCaine was especially interested in physical and visual descriptions of electronic music.<br>
						<br>
						On the right is one of LeCaine&#146;s inventions, an electronic musical instrument called the <i>Spectrogram</i>.</p>
					<h2>UPIC System</h2>

      <p>One of the first digital graphics-to-sound schemes, Iannis Xenakis&#146;s
        UPIC (Unit&eacute; Polyagogique Informatique du CEMAMu) system, was similar
        to LeCaine&#146;s invention in that it allowed composers to draw lines
        and curves that represent control information for a bank of oscillators
        (in this case, digital oscillators). In addition, it allowed the user
        to perform graphical manipulations (cut and paste, copy, rearrange, etc.)
        on what had been drawn. Another benefit of the digital nature of the UPIC
        system was that any waveform (including sampled ones) could be used in
        the synthesis of the sound. By the early 1990s, UPIC was able to do all
        of its synthesis and processing live, enabling it to be used as a real-time
        performance instrument. Newer versions of the UPIC system are still being
        developed and are currently in use at CEMAMu (Centre des Etudes Math&eacute;matiques
        Automatiques Musicales) in Paris, an important center for research in
        computer music.</p>
					<h2>AudioSculpt and SoundHack</h2>
					<p>More recently, a number of FFT/IFFT-based graphical sound manipulation techniques have been developed. One of the most advanced is AudioSculpt from IRCAM in France. AudioSculpt allows you to operate on spectral data as you would an image in a painting program&#151;you can paint, erase, filter, move around, and perform any number of other operations on the sonograms that AudioSculpt presents.</p>
					<p class="figure"><img height="265" width="398" src="../images/chapter5/audiosculpt.jpg"></p>
					<p class="figure_caption"><b>Figure 5.19</b>&nbsp;&nbsp;FFT data displayed as a sonogram in the computer music program AudioSculpt. Partials detected in the sound are indicated by the red lines. On the right are a number of tools for selecting and manipulating the sound/image data.</p>

      <p>Another similar, and in some ways more sophisticated, approach is Tom
        Erbe&#146;s QT-coder, a part of his SoundHack program. The QT-coder allows
        you to save the results of an FFT of a sound as a color image that contains
        all of the data (magnitude and phase) associated with the sound you&#146;ve
        analyzed (as opposed to AudioSculpt, which only presents you with the
        magnitude information). It saves the images as successive frames of a
        QuickTime movie, which can then be opened by most image/video editing
        software. The result is that you can process and manipulate your sound
        using not only specialized audio tools, but also a large number of programs
        meant primarily for traditional image/video processing. The movie can
        then be brought back into SoundHack for resynthesis. It is also possible
        to go the other way, that is, to use SoundHack to synthesize an actual
        movie into sound, manipulate that sound, and then transform it back into
        a movie. As you may imagine, using this technique can cause some pretty
        strange effects!<br>
						&nbsp; </p>
				</td>
			</tr>

  <tr>
    <td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/gtr.original.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 5.26</b><br>
						Original image
							<br>
						</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/gtr.altered.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 5.27</b><br>
						Altered image</p>

				</td>
				<td valign="top" bgcolor="white" width="580">


					<p class="figure"><img height="240" width="250" src="../images/chapter5/gtr.orig.jpg">&nbsp;&nbsp;&nbsp;<img height="240" width="250" src="../images/chapter5/gtr.altered.jpg"></p>

      <p class="figure_caption_flush_bottom"><b>Figure 5.20&nbsp;&nbsp;</b>An
        original image created by the QT-coder in SoundHack (left), and the image
        after alterations (right). Listen to the original sound (Soundfile 5.26)
        and examine the original image. Now examine the altered image. Can you
        guess what the alterations (Soundfile 5.27) will sound like?<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<br>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/jingo-morph-excerpt.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a>&nbsp;<br>
						<b>Soundfile 5.28</b><br>
        Chris Penrose&#146;s composition &quot;American Jingo&quot;</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush_top"><br>
						<img height="419" width="550" src="../images/chapter5/Hyperupic.jpg"></p>
					<p class="figure_caption"><b>Figure 5.21</b>&nbsp;&nbsp;Chris Penrose&#146;s Hyperupic. This computer software allows for a wide variety of ways to transform images into sound. See Soundfile 5.28 for Penrose&#146;s Hyperupic composition.</p>

					<h2>squiggy</h2>

					<p><i>squiggy,</i> a project developed by one of the authors (repetto), combines some of the benefits of both the UPIC system and the FFT-based techniques. It allows for the real-time creation, manipulation, and playback of sonograms. <i>squiggy</i> can record, store, and play back a number of sonograms at once, each of which can be drawn on, filtered, shifted, flipped, erased, looped, combined in various ways, scrubbed, mixed, panned, and so on&#151;all live. The goal of <i>squiggy</i> is to create an instrument for live performance that combines some of the functionality of a traditional time-domain sampler with the intuitiveness and timbral flexibility of frequency-domain processing.
					</p>
					<br>
					<p class="figure_flush_top"><img height="265" width="305" src="../images/chapter5/squiggy.jpg"></p>
					<p class="figure_caption"><b>Figure 5.22&nbsp;&nbsp;</b>Screenshot from
        <I>squiggy</I>, a real-time spectral manipulation tool by douglas repetto.<br>
						<br>
        On the left is the spectral data display window, and on the right are
        controls for independent volume, pan, loop speed, and loop length settings
        for each sound. In addition there are a number of drawing tools and processes
        (not shown) that allow direct graphical manipulation of the spectral data.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
