<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_03.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="7" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 5: The Transformation of Sound by Computer</h1>
					<h1>Section 5.2: Reverb<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580"><p>One of the most
        important and widely used techniques in computer music is reverberation
        and the addition of various types and speeds of delays to dry (unreverberated)
        sounds. Reverberation and delays can be used to simulate room and other
        environmental acoustics or even to create new sounds of their own that
        are not necessarily related to existing physical spaces. </p>
      <p>There are a number of commonly used techniques for simulating and modeling different reverberations and physical environments. One interesting technique is to actually record the ambience of a room and then superimpose that onto a sound recorded elsewhere. This technique is called <i>convolution</i>.</p>
					<h2>A Mathematical Excursion: Convolution in the Time Domain</h2>

      <p>Convolution can be accomplished in two equivalent ways: either in the
        time domain or in the frequency domain. We&#146;ll talk about the time-domain
        version first. Convolution is like a &quot;running average&quot;&#151;that
        is, we take the original function representing the music (we&#146;ll call
        that one <i>m</i>(<i>n</i>), where <i>m</i>(<i>n</i>) is the amplitude
        of the music function at time <i>n</i>) and then we make a new, smoothed
        function by making the amplitude <i>a</i> at time <i>n</i> of our new
        function equal to:</p>

      <p><i>a</i> = (1/3)[<i>m</i>(<i>n</i> &ndash; 2) + <i>m</i>(<i>n </i>&ndash;
        1) + <i>m</i>(<i>n</i>)]</p>
					<p>Let&#146;s call this new function <i>r</i>(<i>n</i>)&#151;for running average&#151;and let&#146;s look at it a little more closely. </p>

      <p>Just to make things work out, we&#146;ll start off by making <i>r</i>(0)
        = 0 and <i>r</i>(1) = 0. Then:
<p><i>r</i>(2) = 1/3[<i>m</i>(0) + <i>m</i>(1) + <i>m</i>(2)]</p>
					<p><i>r</i>(3) = 1/3[<i>m</i>(1) + <i>m</i>(2) + <i>m</i>(3)]</p>

      <p>We continue on, running along and taking the average of every three values
        of <i>m</i>&#151; hence the name &quot;running average.&quot; Another
        way to look at this is as taking the music <i>m</i>(<i>n</i>) and &quot;convolving
        it against the filter <i>a</i>(<i>n</i>)&quot; where <i>a</i>(<i>n</i>)
        is another function, defined by:</p>
					<p><i>a</i>(0) = 1/3, <i>a</i>(1) = 1/3, <i>a</i>(2) = 1/3</p>

      <p>All the rest of the <i>a</i>(<i>n</i>) are 0. </p>
					<p>Now, what is this mysterious &quot;convolving against&quot; thing? Well, first off we write it like:</p>

      <p><I>m</I>*<i>a</i>(<i>n</i>)</p>

      <p>then:</p>

      <p><I>m</I>*<i>a</i>(<i>n</i>) = <i>m</i>(<i>n</i>)<i>a</i>(0) + <i>m</i>(<i>n
        </i>&ndash; 1)<i>a</i>(1) + <i>m</i>(<i>n </i>&ndash; 2)<i>a</i>(2) +....+
        <i>m</i>(0)<i>a</i>(<i>n</i>)</p>

      <p>next:</p>

      <I>m</I>*<i>a</i>(<i>n</i>) = <i>m</i>(<i>n</i>)(1/3) + <i>m</i>(<i>n </i>&ndash;
      1)(1/3) + <i>m</i>(<i>n</i> &ndash; 2)(1/3)
      <p>Now, there is nothing special about the filter <i>a</i>(<i>n</i>)&#151;in
        fact it could be any kind of function. If it were a function that reflects
        the acoustics of your room, then what we are doing&#151;look back at the
        formula&#151;is shaping the input function <i>m</i>(<i>n</i>) according
        to the filter function <i>a(n).</i> A little more terminology: the number
        of nonzero values that <i>a</i>(<i>n</i>) takes on is called the number
        of <I>taps</I> for <i>a</i>(<i>n</i>). So, our running average is a three-tap
        filter. </p>
					<h2>Reverb in the Time Domain</h2>

      <p>Convolution is sure fun, and we&#146;ll see more about it in the following
        section, but a far easier way to create reverb in the time domain is to
        simply delay the signal some number of times (by some very small time
        value) and feed it back onto itself, simulating the way a sound bounces
        around a room. There are a large variety of commercially available and
        inexpensive devices that allow for many different reverb effects. One
        interesting possibility is to change reverb over the course of a sound,
        in effect making a room grow and shrink over time (see Soundfile 5.7).</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="745">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/hammerverb.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 5.7</b><br>
						Reverberant characteristics</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Changing the reverberant characteristics of a sound over time.</p>
					<p class="applet_caption">Here is a nail being hammered. The room size goes from very small to very large over the course of about 15 seconds. This can&#146;t happen in the real, physical world, and it&#146;s a good example of the way the computer musician can create imaginary soundscapes.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="745">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">


					<p>Before there were digital systems, engineers created various
        ways to get reverb-type effects. Any sort of situation that could cause
        delay was used. Engineers used reverberant chambers&#151;sending sound
        via a loudspeaker into a tiled room and rerecording the reverb-ed sound.
        When one of us was a kid, we ran a microphone into our parents' shower
        to record a trumpet solo! </p>
					<p>Reverb units made with springs and metal plates make interesting effects, and many digital signal processing units have algorithms that can be dialed up to create similar effects. Of course we like to think that with a combination of delayed copies of signals we can simulate any type of enclosure, whether it be the most desired recording studio room or a parking garage. </p>

      <p>As you&#146;ll see in Figure 5.5, there are some essential elements for
        creating a reverberant sound. Much of the time a good reverb &quot;patch,&quot;
        or set of digital algorithms, will make use of a number of copies of the
        original signal&#151;just as there are many versions of a sound bouncing
        around a room. There might be one copy of the signal dedicated to making
        the <i>first reflection</i> (which is the very first reflected sound we
        hear when a sound is introduced into a reverberant space). Others might
        be algorithms dedicated to making the <i>early reflections</i>&#151;the
        first sounds we hear after the initial reflection. The rest of the software
        for a good digital reverb is likely designed to blend the reverberant
        sound. Filters are usually used to make the reverb tails sound as if they
        are far away. Any filter that attenuates the higher frequencies (like
        5 kHz or up) makes a signal sound farther away from us, since high frequencies
        have very little energy and don&#146;t travel very far. </p>
					<p>When we are working to simulate rooms with digital systems, we have to take a number of things into consideration:</p>
					<ul>
						<li type="disc">How large is the room? How long will it take for the first reflection to get back to our ears?
						<li type="disc">What are the surfaces like in the room? Not just the walls, ceiling, and floor, but also any objects in the room that can reflect sound waves; for example, are there any huge ice sculptures in the middle of the room?

        <li type="disc">Are there surfaces in the room that can absorb sound?
          These are called <i>absorptive</i> surfaces. For example: movie theaters
          usually have curtains hanging on the walls to suck up sound waves. Maybe
          there are people in the room: most bodies are soft, and soft surfaces
          remove energy from reflected sounds.
      </ul>
					<p class="figure"><img height="201" width="360" src="../images/chapter5/reverb.gif"></p>
					<p class="figure_caption"><b>Figure 5.5</b>&nbsp;&nbsp;Time-domain reverb is conceptually similar to the idea of physical modeling&#151;we take a signal (say a hand clap) and feed it into a model of a real-world environment (like a large room). The signal is modified according to our model room and then output as a new signal.<br>
						<br>
						In this illustration you see the graphic representation of a direct sound being introduced into a reverberant space. The RT 60 refers to reverb time, or how long it takes the sound in the space to reduce by &ndash;60 dB.<br>
						<br>
        Photo courtesy of Garry Clennell &lt;gcat@clara.co.uk&gt;.</p>
					<p class="figure"><img height="162" width="363" src="../images/chapter5/room.gif"></p>
					<p class="figure_caption"><b>Figure 5.6</b>&nbsp;&nbsp;In this illustration you can see how sound from a sound source (like a loudspeaker) can bounce around a reverberant space to create what we call <i>reverb</i>.<br>
						<br>
        A typical reverb algorithm can have lots of control variables such as
        room size (gymnasium, small club, closet, bathroom), brightness (hard
        walls, soft walls or curtains, jello), and feedback or absorption coefficient
        (are there people, sand, rugs, llamas in the room&#151;how quickly does
        the sound die out?).<br>
						<br>
						Photo courtesy of Garry Clennell &lt;gcat@clara.co.uk&gt;.</p>
					<p class="figure"><img height="165" width="394" src="../images/chapter5/comb.delay.jpg"></p>

      <p class="figure_caption"><b>Figure 5.7</b>&nbsp;&nbsp;Here&#146;s a basic
        diagram showing how a signal is <i>delayed</i> (in the delay box), then
        fed back and added to the original signal with some <i>attenuated</i>
        (that is, not as loud) version of the original signal added. This would
        create an effect known as <I>comb filtering</I> (a short delay with feedback
        that emphasizes specific harmonics) as well as a delay.<br>
						<br>
						What does a <i>delay</i> do? It takes a function (a signal), shifts it &quot;backward&quot; in time, then combines it with another &quot;copy&quot; of the same signal. This is, once again, a kind of averaging.</p>
					<p class="figure"><img height="216" width="397" src="../images/chapter5/allpass.delay.jpg"></p>

      <p class="figure_caption"><b>Figure 5.8</b>&nbsp;&nbsp;When we both feed-<i>back</i>
        and feed-<i>forward</i> the same signal at the same time, phase inverted,
        we get what is called an <i>all-pass filter.</i> (Phase inverting a signal
        means changing its phase by 180&deg;.) With this type of filter/delay,
        we don&#146;t get the comb filtering effect because of the phase inversion
        of the feed-forward signal, which fills in the missing frequencies created
        by the comb filter of the feedbacked signal.<br>
        <br>
        This type of unit is used to create blended reverb. A set of delay algorithms
        would probably have some combination of a number of comb-type delays as
        in this figure, as well as some all-pass filters to create a blend.</p>
					<h2>Making Reverb in the Frequency Domain: A Return to Convolution</h2>

      <p>As we mentioned, convolution is a great way to get reverberant effects.
        But we mainly talked about the mathematics of it and why convolving a
        signal is basically putting it through a type of filter. </p>
					<p>But now we&#146;re going to talk about how to actually convolve a sound. The process is fairly straightforward: </p>
					<ol>
						<li type="1">Locate a space whose reverberant characteristics we like.
						<li type="1">Set up a microphone and some digital media for recording.

        <li type="1">Make a short, loud sound in this room, like hitting two drumsticks
          together (popping a balloon and firing a starter&#146;s pistol are other
          standard ways of doing this&#151;sometimes it&#146;s called &quot;shooting
          the room&quot;).
        <li type="1">Record this sound, called the <i>impulse.</i>

        <li type="1">Analyze the impulse in the room; this gives us the <i>impulse
          response</i> of the room, which is the characteristics of the room&#146;s
          response to sound.
        <li type="1">Use the technique of convolution to combine this impulse response with other sound sources, more or less bringing the room to the sound.
					</ol>

      <p>Convolution can be powerful, and it&#146;s a fairly complicated software
        process. It takes each sample in the impulse response file (the one we
        recorded in the room, which should be short) and multiplies that sample
        by each sample in the sound file that we want to &quot;put in&quot; that
        room. So, each sample input sound file, like a vocal sound file to which
        we want to add reverb, is multiplied by each sample in the impulse response
        file. </p>

      <p>That&#146;s a lot of multiplies! Let&#146;s pretend, for the moment,
        that we have a 3-second impulse file and a 1-minute sound file to which
        we want to add the reverberant characteristics of some space. At 44.1
        kHz, that&#8217;s:</p>
					<ul>
						<p><code>3 * 44,100 * 60 * 44,100</code></p>
						<p><code>= 180 * 680,683,500,000 </code></p>

        <p><code>= 122,523,403,030,000 multiplies</code></p>
					</ul>

      <p>If you&#146;ve been paying close attention, you might raise an interesting
        question here: isn&#146;t this the time domain? We&#146;re just multiplying
        signals together (well, actually, we&#146;re multiplying each point in
        each function by every other point in the other function&#151;called a
        cross multiply). But this multiply in the time domain actually produces
        what we refer to as the <i>convolution in the frequency domain</i>. </p>

      <p>Now, we&#146;re not math geniuses, but even we know that this is a whole
        mess of multiplies! That&#146;s why convolution, which is <i>very</i>
        computationally expensive, had not been a popular technique until your
        average computer got fast enough to do it. It was also a completely unknown
        sound manipulation idea until digital recording technology made it feasible.</p>

      <p>But nowadays we can do it, and the reason we can do it is our old friend,
        the FFT! You see, it turns out that for filters with lots of taps (remember
        that this means one with lots of nonzero values), it is easier to compute
        the convolution in the spectral domain. </p>

      <p>Suppose we want to convolve our music function <i>m</i>(<i>n</i>) against
        our filter function <i>a</i>(<i>n</i>). We can tell you immediately that
        the convolution of these functions (which, remember, is another sound,
        a function that we call <i>c</i>(<i>n</i>)) has a spectrum equal to the
        pointwise product of the spectrum of the music function and the filter
        function. The <i>pointwise product</i> is the frequency content at any
        point in the convolution and is calculated by multiplying the spectrums
        of the music function and the filter function at that particular point.</p>

      <p>Another way of saying this is to say that the Fourier coefficients of
        the convolution can be computed by simply multiplying together each of
        the Fourier coefficients of <i>m</i>(<i>n</i>) and <i>a</i>(<i>n</i>).
        The <i>zero coefficient</i> (the DC term) is the product of the DC terms
        of <i>a</i>(<i>n</i>) and <i>m</i>(<i>n</i>); the first coefficient is
        the product of the first Fourier coefficient of <i>a</i>(<i>n</i>) and
        <i>m</i>(<i>n</i>); and so on. </p>
					<p>So, here&#146;s a sneaky algorithm for making the convolution:</p>
					<ul>
						<p>Step 1: Compute the Fourier coefficients of both <i>m</i>(<i>n</i>)  and <i>a</i>(<i>n</i>). </p>
						<p>Step 2: Compute the pointwise products of the Fourier coefficients of <i>a</i>(<i>n</i>) and <i>m</i>(<i>n</i>). </p>
						<p>Step 3: Compute the inverse Fourier transform (IFFT) of the result of Step 1.</p>
					</ul>
					<p>And we&#146;re done! This was one of the great properties of the FFT: it made convolution just about as easy as multiplication!<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<!-- <p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2603F180'));return CSClickReturn()" href="../applets/5_3_comb_filter_input.php" target="_blank" csclick="BCE2603F180"><img src="../images/global/applet.gif" width="80" height="65" name="3" border="0"></a><br>
						<b>Applet 5.3</b><br>
							Flanges, delays, reverb: effect box fun</p>
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE26046181'));return CSClickReturn()" href="../applets/5_2_multitap.php" target="_blank" csclick="BCE26046181"><img src="../images/global/applet.gif" width="80" height="65" name="4" border="0"></a><br>
						<b>Applet 5.4</b><br>
						Multitap delay<br>
						</p> -->
				</td>

    <td valign="top" bgcolor="white" width="580"><p>So, once again, if we have
        a big library of great impulse responses&#151; the best-sounding cathedrals,
        the best recording studios, concert halls, the Grand Canyon, Grand Central
        Station, your shower&#151;we can simulate any space for any sound. And
        indeed this is how many digital effects processors and reverb plugins
        for computer programs work. This is all very cool when you&#146;re working
        hard to get that &quot;just right&quot; sound. </p>
      <p class="figure"><img height="114" width="394" src="../images/chapter5/impulseResponse.gif"></p>
					<p class="figure_caption"><b>Figure 5.9</b>&nbsp;&nbsp;The impulse on the left, and the room&#146;s response on the right.<br>
						<br>
						Thanks to Nigel Redmon of earlevel.com for this graphic.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_03.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
