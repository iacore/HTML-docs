<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_02.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="21" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 5: The Transformation of Sound by Computer</h1>
					<h1>Section 5.1: <b>Introduction to the Transformation of Sound by Computer</b><br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p></p>
				</td>

    <td valign="top" bgcolor="white" width="580"><p>Although direct synthesis
        of sound is an important area of computer music, it can be equally interesting
        (or even more so!) to take existing sounds (recorded or synthesized) and
        transform, mutate, deconstruct&#151;in general, mess around with them.
        There are as many basic sound transformation techniques as there are synthesis
        techniques, and in this chapter we&#146;ll describe a few important ones.
        For the sake of convenience, we will separate them into time-domain and
        frequency-domain techniques. </p>
      <h2>Tape Music/Cut and Paste</h2>
					<p>The most obvious way to transform a sound is to chop it up, edit it, turn it around, and collage it. These kinds of procedures, which have been used in electronic music since the early days of tape recorders, are done in the time domain. There are a number of sophisticated software tools for manipulating sounds in this way, called <i>digital sound editors</i>. These editors allow for the most minute, sample-by-sample changes of a sound. These techniques are used in almost all fields of audio, from avant-garde music to Hollywood soundtracks, from radio station advertising spots to rap.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/chef.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 5.1</b><br>
						Jon Appleton:<br>
        &quot;Chef d'Oeuvre&quot;</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This classic of analog electronic
        music was composed using only tape recorders, filters, and other precomputer
        devices and was based on a recording of an old television commercial.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/FOR_DIR_FROM_JMR.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 5.2</b><br>
        &quot;BS Variation 061801&quot;</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">&quot;BS Variation 061801&quot;
        by Huk Don Phun was created using Phun&#146;s experimental &quot;bleeding
        eyes&quot; filtering technique. He used only free software and sounds
        he downloaded from the web.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h2>Time-Domain Restructuring</h2>
					<p>Composers have experimented a lot with unusual time-domain restructuring of sound. By chopping up waveforms into very small segments and radically reordering them, some noisy and unusual effects can be created. As in collage visual art, the ironic and interesting juxtaposition of very familiar materials can be used to create new works that are perhaps greater than the sum of their constituent parts.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/puzzles.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a><br>
						<b>Soundfile 5.3</b><br>
        Jane Dowe&#146;s &quot;Beck Deconstruction&quot; piece</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Playing with the perception of the listener, &quot;Puzzels &amp; Pagans&quot; takes the first 2 minutes and 26 seconds of &quot;Jackass&quot; (from <i>Odelay</i>) and cuts it up into 2,500 pieces. These pieces are then reshuffled, taking into account probability functions (that change over the length of the track) that determine if pieces remain in their original position or if they don&#146;t sound at all.</p>
					<p class="applet_caption">This was realized using James McCartney&#146;s SuperCollider computer music programming language.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<p><br>
        A unique, experimental, and rather strange program for deconstructing
        and reconstructing sounds in the time domain is Arge&iuml;phontes Lyre,
        written by the enigmatic Akira Rabelais. It provides a number of techniques
        for radical decomposition/recomposition of <nobr>sounds&#151;</nobr>techniques that
        often preclude the user from making specific decisions in favor of larger,
        more probabilistic decisions.</p>
					<p class="figure"><img height="311" width="400" src="../images/chapter5/akira.jpg"></p>

      <p class="figure_caption"><b>Figure 5.1</b>&nbsp;&nbsp;Sample GUI from Arge&iuml;phontes
        Lyre, for sound deconstruction. This is time-domain mutation.<br>
							<br>
						</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/aposiopesis.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button34" border="0" alt=""></a><br>
						<b>Soundfile 5.4</b><br>
						Arge&iuml;phontes Lyre</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Sound example using Arge&iuml;phontes Lyre (random cut-up).</p>
					<p class="applet_caption">The original piano piece &quot;Aposiopesi&quot; was written by Vincent Cart&eacute; in 1999. This recording was made by Akira Rabelais at Wave Equation Studios in Hollywood, California. The piece was filtered with Arge&iuml;phontes Lyre 2.0b1.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h2>Sampling</h2>
					<p><i>Sampling</i> refers to taking small bits of sound, often recognizable ones, and recontextualizing them via digital techniques. By digitally sampling, we can easily manipulate the pitch and time characteristics of ordinary sounds and use them in any number of ways.</p>

      <p>We&#146;ve talked a lot about samples and sampling in the preceding chapters.
        In popular music (especially electronic dance and beat-oriented music),
        the term &quot;sampling&quot; has acquired a specialized meaning. In this
        context, a sample refers to a (usually) short excerpt from some previously
        recorded source, such as a drum loop from a song or some dialog from a
        film soundtrack, that is used as an element in a new work. A <i>sampler</i>
        is the hardware used to record, store, manipulate, and play back samples.
        Originally, most samplers were stand-alone pieces of gear. Today sampling
        tends to be integrated into a studio&#146;s computer-based digital audio
        system.</p>
					<p>Sampling was pioneered by rap artists in the mid-1980s, and by the early 1990s it had become a standard studio technique used in virtually all types of music. Issues of copyright violation have plagued many artists working with sample-based music, notably John Oswald of &quot;Plunderphonics&quot; fame and the band Negativland, although the motives of the &quot;offended&quot; parties (generally large record companies) have tended to be more financial than artistic. One result of this is that the phrase &quot;Contains a sample from xxx, used by permission&quot; has become ubiquitous on CD cases and in liner notes.</p>

      <p>Although the idea of using excerpts from various sources in a new work
        is not new (many composers, from B&eacute;la Bart&oacute;k, who used Balkan
        folk songs, to Charles Ives, who used American popular music folk songs,
        have done so), digital technology has radically changed the possibilities.</p>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top" bgcolor="white" width="125">
					<br>
					<br>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/dustiny.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button35" border="0" alt=""></a><br>
						<b>Soundfile 5.5</b><br>
        &quot;Dustiny&quot;</p>
			</td>
			<td valign="top" bgcolor="white" width="580">

					<p class="figure"><img height="247" width="321" src="../images/chapter5/herbert.jpg"><img height="200" width="200" src="../images/chapter5/sawdustb.jpg"></p>

      <p class="figure_caption"><b>Figure 5.2</b>&nbsp;&nbsp;Herbert Br&uuml;n
        said of his program SAWDUST: &quot;The computer program which I called
        SAWDUST allows me to work with the smallest parts of waveforms, to link
        them and to mingle or merge them with one another. Once composed, the
        links and mixtures are treated, by repetition, as periods, or by various
        degrees of continuous change, as passing moments of orientation in a process
        of transformations.&quot; Also listen to Soundfile 5.5, Br&uuml;n&#146;s
        1978 SAWDUST composition &quot;Dustiny.&quot;</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<br>
						<br>
						<a onclick="setSrc('../sounds/chapter5/loopsound2.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button36" border="0" alt=""></a><br>
						<b>Soundfile 5.6<br>
						</b>Drum machine sounds</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<h2>Drum Machines</h2>
					<p><i>Drum machines</i> and samplers are close cousins. Many drum machines are just specialized samplers&#151;their samples just happen to be all percussion/drum-oriented. Other drum machines feature electronic or digitally synthesized drum sounds. As with sampling, drum machines started out as stand-alone pieces of hardware but now have largely been integrated into computer-based systems.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<!-- <p class="icon_caption"><br>
						<br>
						<br>
						<br>
						<a onclick="CSAction(new Array(/*CMP*/'BCE25FFB178'));return CSClickReturn()" href="../applets/5_1_drumbox.php" target="_blank" csclick="BCE25FFB178"><img src="../images/global/applet.gif" width="80" height="65" name="3" border="0"></a><br>
						<b>Applet 5.2</b><br>
        Drum machine </p> -->
				</td>
				<td valign="top" bgcolor="white" width="580">
					<h2>DAW Systems</h2>

      <p><i>Digital-audio workstations</i> (DAWs) in the 1990s and 2000s have
        had the same effect on digital sound creation as desktop publishing software
        had on the publishing industry in the 1980s: they&#146;ve brought digital
        sound creation out of the highly specialized and expensive environments
        in which it grew up and into people&#146;s homes. A DAW usually consists
        of a computer with some sort of sound card or other hardware for analog
        and digital input/output; sound recording/editing/playback/multi-track
        software; and a mixer, amplifier, and other sound equipment traditionally
        found in a home studio. Even the most modest of DAW systems can provide
        from eight to sixteen tracks of CD-quality sound, making it possible for
        many artists to self-produce and release their work for much less than
        it would traditionally cost. This ability, in conjunction with similar
        marketing and publicizing possibilities opened up by the spread of the
        Internet, has contributed to the explosion of tiny record labels and independently
        released CDs we&#146;ve seen recently.</p>

      <p>In 1989, Digidesign came out with Sound Tools, the first professional
        tape&ndash;less recording system. With the popularization of personal
        computers, numerous software and hardware manufacturers have cornered
        the market for computer-based digital audio. Starting in the mid-1980s,
        personal computer&ndash;based production systems have allowed individuals
        and institutions to make the highest-quality recordings and DAW systems,
        and have also revolutionized the professional music, broadcast, multimedia,
        and film industries with audio systems that are more flexible, more accessible,
        and more creatively oriented than ever before. </p>

      <p>Today DAWs come in all shapes and sizes and interface with most computer
        operating systems, from Mac to Windows to LINUX. In addition, many DAW
        systems involve a &quot;breakout box&quot;&#151;a piece of hardware that
        usually provides four to eight channels of digital audio I/O (inputs and
        outputs); and as we write this a new technology called FireWire looks
        like a revolutionary way to hook up the breakout boxes to personal computers.
        Nowadays many DAW systems also have &quot;control surfaces&quot; &#151;pieces
        of hardware that look like a standard mixer but are really controllers
        for parameters in the digital audio system. </p>
					<p class="figure"><img height="168" width="200" src="../images/chapter5/MOTU.gif"></p>
					<p class="figure_caption"><b>Figure 5.3</b>&nbsp;&nbsp;The Mark of the Unicorn (MOTU) 828 breakout box, part of a DAW system with FireWire interface.<br>
						<br>
						Thanks to MOTU.com for their permission to use this graphic.</p>
					<p class="figure"><img height="159" width="200" src="../images/chapter5/mm.jpg"></p>

      <p class="figure_caption"><b>Figure 5.4</b>&nbsp;&nbsp;Motor Mix&#153; stands
        alone as the only DAW mixer control surface to offer eight-bank, switchable
        motorized faders. It also features eight rotary pots, a rotary encoder,
        68 switches, and a 40-by-2 LCD.<br>
						<br>
        Thanks to Sentech Electronics, Inc. for this graphic.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_02.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
