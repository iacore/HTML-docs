<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_05.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="2" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 5: The Transformation of Sound by Computer</h1>
					<h1>Section 5.4: <b>Introduction to Spectral Manipulation</b><br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				
    <td valign="top" bgcolor="white" width="580"><p>There are two different approaches 
        to manipulating the frequency content of sounds: filtering, and a combination 
        of spectral analysis and resynthesis. Filtering techniques, at least classically 
        (before the FFT became commonly used by most computer musicians), attempted 
        to describe spectral change by designing time-domain operations. More 
        recently, a great deal of work in filter design has taken place directly 
        in the spectral domain. </p>
      <p>Spectral techniques allow us to represent and manipulate signals directly in the frequency domain, often providing a much more intuitive and user-friendly way to work with sound. Fourier analysis (especially the FFT) is the key to many current spectral manipulation techniques.</p>
					
      <h2>Phase Vocoder</h2>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/pvoc.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 5.9</b><br>
							Unmodified speech<br>
						</p>
					
      <p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/pvoc.short.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 5.10</b><br>
							Speech made half as long with a phase vocoder<br>
						</p>

					
      <p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/pvoc.long.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a><br>
						<b>Soundfile 5.11</b><br>
							Speech made twice as long with a phase vocoder<br>
						</p>

					
      <p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/pvoc.up.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button34" border="0" alt=""></a><br>
						<b>Soundfile 5.12</b><br>
        Speech transposed up an octave<br>
						</p>

					
      <p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/pvoc.down.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button35" border="0" alt=""></a><br>
						<b>Soundfile 5.13</b><br>
							Speech transposed down an octave<br>
						</p>
				</td>
				<td valign="top" bgcolor="white" width="580"><p>Perhaps the most 
        commonly used implementation of Fourier analysis in computer music is 
        a technique called the <i>phase vocoder.</i> What is called the phase 
        vocoder actually comprises a number of techniques for taking a time-domain 
        signal, representing it as a series of amplitudes, phases, and frequencies, 
        and manipulating this information and returning it to the time domain. 
        (Remember, Fourier analysis is the process of turning the list of samples 
        of our music function into a list of Fourier coefficients, which are complex 
        numbers that have phase and amplitude, and each corresponds to a frequency.) 
      </p>
      <p>Two of the most important ways that musicians have used the phase vocoder 
        technique are to use a sound&#146;s Fourier representation to manipulate 
        its length without changing its pitch and, conversely, to change its pitch 
        without affecting its length. This is called <i>time stretching</i> and 
        <i>pitch shifting</i>.</p>
					<p>Why should this even be difficult? Well, consider trying it in the time domain: play back, say, a 33 1/3 RPM record at 45 RPMs. What happens? You play the record faster, the needle moves through the grooves at a higher rate, and the sound is higher pitched (often called the &quot;chipmunk&quot; effect, possibly after the famous 1960s novelty records featuring Alvin and his friends). The sound is also much shorter: in this case, pitch is directly related to frequency&#151;they&#146;re both controlled by the same mechanism. A creative and virtuosic use of this technique is scratching as practiced by hip-hop, rap, and dance DJs.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="744">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/pitch.up.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button36" border="0" alt=""></a><br>
						<b>Soundfile 5.14</b><br>
						Speeding up a sound</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/pitch.down.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button37" border="0" alt=""></a><br>
						<b>Soundfile 5.15</b><br>
						Slowing down a sound</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">An example of time-domain pitch shifting/speed changing. In this case, pitch and time transformations are related. The faster the sound is played, the higher the pitch becomes, as heard in Soundfile 5.14. In Soundfile 5.15, the opposite effect is heard: the slower file sounds lower in pitch.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="744">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h2>The Pitch/Speed Relationship in the Digital World</h2>
					
      <p>Now think of altering the speed of a digital signal. To play it back 
        faster, you might raise the sampling rate, reading through the samples 
        for playback more quickly. Remember that sometimes we refer to the sampling 
        rate as the rate at which we stored (sampled) the sounds, but it also 
        can refer to the kind of internal clock that the computer uses with reference 
        to a sound (for playback and other calculations). We can vary that rate, 
        for example playing back a sound sampled at 22.05 kHz at 44.1 kHz. With 
        more samples (read) per second, the sound gets shorter. Since frequency 
        is closely related to sampling rate, the sound also changes pitch.<br>
						<br>
						<br>
					</p>
				</td>
			</tr>

			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="744">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE260FE192'));return CSClickReturn()" href="../popups/chapter5/xbit_5_1.php" target="_blank" csclick="BCE260FE192"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 5.1<br>
        </b>&quot;Slow Motion Sound&quot;</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption"> Even with the basic pitch/speed 
        problem, manipulating the speed of sound has always attracted creative 
        experiment. Consider an idea proposed by composer Steve Reich in 1967, 
        thought to be a kind of impossible dream for electronic music: to slow 
        down a sound without changing its pitch (and vice versa). </p>
				</td>
			</tr>

			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="744">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/pvoc.varispeed.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button38" border="0" alt=""></a><br>
						<b>Soundfile 5.16<br>
        </b>Varispeed</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">A SoundHack varispeed of some standard 
        speech. Note how the speech&#146;s speed is changed over time.</p>
					
      <p class="applet_caption"><I>Varispeed</I> is a general term for &quot;fooling 
        around&quot; with the sampling rate of a sound file.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="744">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h2>Using the Phase Vocoder</h2>
					<p>Using the phase vocoder, we can realize Steve Reich&#146;s piece (see Xtra bit 5.1), and a great many others. The phase vocoder allows us independent control over the time and the pitch of a sound.</p>
					<p>How does this work? Actually, in two different ways: by changing the speed and changing the pitch.</p>
					
      <p>To change the speed, or length, of a sound without changing its pitch, 
        we need to know something about what is called <i>windowing.</i> Remember 
        that when doing an FFT on a sound, we use what are called <i>frames</i>&#151;time-delimited 
        segments of sound. Over each frame we impose a window: an amplitude envelope 
        that allows us to cross-fade one frame into another, avoiding problems 
        that occur at the boundaries of the two frames. </p>
					
      <p>What are these problems? Well, remember that when we take an FFT of some 
        portion of the sound, that FFT, by definition, assumes that we&#146;re 
        analyzing a periodic, infinitely repeating signal. Otherwise, it wouldn&#146;t 
        be Fourier analyzable. But if we just chop up the sound into FFT-frames, 
        the points at which we do the chopping will be hard-edged, and we&#146;ll 
        in effect be assuming that our periodic signal has nasty edges on both 
        ends (which will typically show up as strong high frequencies). So to 
        get around this, we attenuate the beginning and ending of our frame with 
        a window, smoothing out the assumed periodical signal. Typically, these 
        windows overlap at a certain rate (1/8, 1/4, 1/2 overlap), creating even 
        smoother transitions between one FFT frame and another.</p>
					<p class="figure"><img height="370" width="133" src="../images/chapter5/wave.jpg">&nbsp;&nbsp;&nbsp;<img height="363" width="127" src="../images/chapter5/window.jpg">&nbsp;&nbsp;&nbsp;<img height="366" width="131" src="../images/chapter5/windowed.jpg"></p>
					
      <p class="figure_caption"><b>Figure 5.12</b>&nbsp;&nbsp;Why do we window 
        FFT frames? The image on the left shows the waveform that our FFT would 
        analyze without windowing&#151;notice the sharp edges where the frame 
        begins and ends. The image in the middle is our window. The image on the 
        right shows the windowed waveform. By imposing a smoothing window on the 
        time domain signal and doing an FFT of the windowed signal, we de-emphasize 
        the high-frequency artifacts created by these sharp vertical drops at 
        the beginning and end of the frame.<br>
						<br>
        Thanks to Jarno Sepp&auml;nen &lt;Jarno.Seppanen@nokia.com&gt; Nokia Research 
        Center, Tampere, Finland, for these images.</p>
					<p class="figure"><img height="341" width="500" src="../images/chapter5/overlap.jpg"></p>
					<p class="figure_caption"><b>Figure 5.13 </b>&nbsp;&nbsp;After we window a signal for the FFT, we overlap those windowed signals so that the original signal is reconstructed without the sharp edges.</p>
					<p>By changing the length of the overlap when we resynthesize the signal, we can change the speed of the sound without affecting its frequency content (that is, the FFT information will remain the same, it&#146;ll just be resynthesized at a &quot;larger&quot; frame size). That&#146;s how the phase vocoder typically changes the length of a sound.</p>
					
      <p>What about changing the pitch? Well, it&#146;s easy to see that with 
        an FFT we get a set of amplitudes that correspond to a given set of frequencies. 
        But it&#146;s clear that if, for example, we have very strong amplitudes 
        at 100 Hz, 200 Hz, 300 Hz, 400 Hz, and so on, we will perceive a strong 
        pitch at 100 Hz. What if we just take the amplitudes at all frequencies 
        and move them &quot;up&quot; (or down) to frequencies twice as high (or 
        as low)? What we&#146;ve done then is re-create the frequency/amplitude 
        relationships starting at a higher frequency&#8212;changing the perceived 
        pitch without changing the frequency.</p>
					<p class="figure"><img height="354" width="321" src="../images/chapter5/bins.jpg"></p>
					<p class="figure_caption"><b>Figure 5.14&nbsp;&nbsp;</b>Two columns of FFT bins. These bins divide the Nyquist frequency evenly. In other words, if we were sampling at 10 kHz and we had 100 FFT bins (both these numbers are rather silly, but they&#146;re arithmetically simple), our Nyquist frequency would be 5 kHz, and the bin width would be 50 Hz.<br>
					<br>
        Each of these frequency bins has its own <i>amplitude</i>, which is the 
        strength or energy of the spectra at that frequency (or more precisely, 
        the average energy in that frequency range). To implement a pitch shift 
        in a phase vocoder, the amplitudes in the left column are shifted up to 
        higher frequencies in the right column. This operation shifts 1 spectra 
        in the left bin to a higher frequency in the right bin.</p>
					
      <p>The phase vocoder technique actually works just fine, though for radical 
        pitch/time deformations we get some problems (usually called &quot;phasiness&quot;). 
        These techniques work better for slowly changing harmonic sounds and for 
        simpler pitch/time relationships (integer multiples). Still, the phase 
        vocoder works well enough, in general, for it to be a widely used technique 
        in both the commercial and the artistic sound worlds.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="762">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/anna.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button39" border="0" alt=""></a><br>
						<b>Soundfile 5.17</b><br>
						&quot;Study: Anna&quot;</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Larry Polansky&#146;s 1-minute piece, 
        &quot;Study: Anna, the long and the short of it.&quot; </p>
					<p class="applet_caption">All the sounds are created using phase vocoder 
        pitch and time shifts of a recording of a very short cry (with introductory 
        inhale) of the composer&#146;s daughter when she was 6 months old.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_05.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>

