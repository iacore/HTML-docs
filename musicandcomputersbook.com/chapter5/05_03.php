<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_04.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="3" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 5: The Transformation of Sound by Computer</h1>
					<h1>Section 5.3: <b>Localization/Spatialization</b><br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<!-- <td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td> -->
			</tr>
			<tr>
				<!-- <td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2605F182'));return CSClickReturn()" href="../applets/5_3_itd_location.php" target="_blank" csclick="BCE2605F182"><img src="../images/global/applet.gif" width="80" height="65" name="3" border="0"></a><br>
						<b>Applet 5.5</b><br>
						Filter-based localization<br>
						</p>
					<p class="icon_caption"></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This applet lets you pan, fade, and simulate binaural listening (best if listened to with headphones).</p>
					<p class="applet_caption"></p>
				</td> -->
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125"><a onclick="setSrc('../sounds/chapter5/funkyaiffBNMV.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><span class="icon_caption"><br>
						<b>Soundfile 5.8</b><br>
						Binaural location
						</span></td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Using the program SoundHack by Tom Erbe, we can place a sound anywhere in the <i>binaural</i> (two ears) space. SoundHack does this by convolving the sound with known filter functions that simulate the ITD (interaural time delay) between the two ears.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<p><br>
						Close your eyes and listen to the sounds around you. How well can you tell where they&#146;re coming from? Pretty well, hopefully! How do we do that? And how could we use a computer to simulate moving sound so that, for example, we can make a car go screaming across a movie screen or a bass player seem to walk over our heads? </p>

      <p>Humans have a pretty complicated system for perceptually locating sounds,
        involving, among other factors, the relative loudness of the sound in
        each ear, the time difference between the sound&#146;s arrival in each
        ear, and the difference in frequency content of the sound as heard by
        each ear. How would a &quot;cyclaural&quot; (the equivalent of a &quot;cyclops&quot;)
        hear? Most attempts at spatializing, or localizing, recorded sounds make
        use of some combination of factors involving the two ears on either side
        of the head.</p>
					<h2>Simulating Sound Placement</h2>

      <p>Simulating a loudness difference is pretty simple&#151;if someone standing
        to your right says your name, their voice is going to sound louder in
        your right ear than in your left. The simplest way to simulate this volume
        difference is to increase the volume of the signal in one channel while
        lowering it in the other&#151;you&#146;ve probably used the <I>pan</I>
        or balance knob on a car stereo or boombox, which does exactly this. Panning
        is a fast, cheap, and fairly effective means of localizing a signal, although
        it can often sound artificial.</p>
					<h2><b>Interaural Time Delay (ITD)</b></h2>
					<p>Simulating a time difference is a little trickier, but it adds a lot to the realism of the localization. Why would a sound reach your ears at different times? After all, aren&#146;t our ears pretty close together? We&#146;re generally not even aware that this is true: snap your finger on one side of your head, and you&#146;ll think that you hear the sound in both ears at exactly the same time. </p>

      <p>But you don&#146;t. Sound moves at a specific speed, and it&#146;s not
        all that fast (compared to light, anyway): about 345 meters/second. Since
        your fingers are closer to one ear than the other, the sound waves will
        arrive at your ears at different times, if only by a small fraction of
        a second. Since most of us have ears that are quite close together, the
        time difference is very slight&#151;too small for us to consciously &quot;perceive.&quot;</p>

      <p>Let&#146;s say your head is a bit wide: roughly 250 cm, or a quarter
        of a meter. It takes sound around 1/345 of a second to go 1 meter, which
        is approximately 0.003 second (3 thousandths of a second). It takes about
        a quarter of that time to get from one ear of your wide head to the other,
        which is about 0.0007 second (0.7 thousandths of a second). That&#146;s
        a pretty small amount of time! Do you believe that our brains perceive
        that tiny interval and use the difference to help us localize the sound?
        We hope so, because if there&#146;s a frisbee coming at you, it would
        be nice to know which direction it&#146;s coming from! In fact, though,
        the delay is even smaller because your head&#146;s smaller than 0.25 meter
        (we just rounded it off for simplicity). The technical name for this delay
        is <i>interaural time delay</i> (ITD).</p>
					<p>To simulate ITD by computer, we simply need to add a delay to one channel of the sound. The longer the delay, the more the sound will seem to be panned to one side or the other (depending on which channel is delayed). The delays must be kept <i>very</i> short so that, as in nature, we don&#146;t consciously perceive them as delays, just as location cues. Our brains take over and use them to calculate the position of the sound. Wow!</p>

      <h2>Modeling Our Ears and Our Heads</h2>

      <p>That the ears perceive and respond to a difference in volume and arrival
        time of a sound seems pretty straightforward, albeit amazing. But what&#146;s
        this about a difference in the frequency content of the sound? How could
        the position of a bird change the spectral makeup of its song? The answer:
        your head!</p>

      <p>Imagine someone speaking to you from another room. What does the voice
        sound like? It&#146;s probably a bit muffled or hard to understand. That&#146;s
        because the wall through which the sound is traveling&#151;besides simply
        cutting down the loudness of the sound&#151;acts like a low-pass filter.
        It lets the low frequencies in the voice pass through while attenuating
        or muffling the higher ones. </p>
					<p>Your head does the same thing. When a sound comes from your right, it must first pass through, or go around, your head in order to reach your left ear. In the process, your head absorbs, or blocks, some of the high-frequency energy in the sound.  Since the sound didn&#146;t have to pass through your head to get to your right ear, there is a difference in the spectral makeup of the sound that each ear hears. As with ITD, this is a subtle effect, although if you&#146;re in a quiet room and you turn your head from side to side while listening to a steady sound, you may start to perceive it.</p>

      <p>Modeling this by computer is easy, provided you know something about
        how the head filters sounds (what frequencies are attenuated and by how
        much). If you&#146;re interested in the frequency response of the human
        head, there are a number of published sources available for the data,
        since they are used by, among others, the government for all sorts of
        things (like flight simulators, for example). Researcher and author Durand
        Begault has been a leading pioneer in the design and implementation of
        what are called <i>head transfer functions</i>&#151;frequency response
        curves for different locations of sound.</p>

      <h2>What Are Head-Related Transfer Functions (HRTFs)?</h2>
					<p class="figure"><img height="268" width="371" src="../images/chapter5/hrtf.gif"></p>

      <p class="figure_caption"><b>Figure 5.10&nbsp;&nbsp;</b>This illustration
        shows how the spectral contents of a sound change depending on which direction
        the sound is coming from. The body (head and shoulders) and the time-of-arrival
        difference that occurs between the left and right ears create a filtering
        effect.</p>
					<p class="figure"><img height="330" width="233" src="../images/chapter5/dummyhead.gif"></p>
					<p class="figure_caption"><b>Figure 5.11</b>&nbsp;&nbsp;The binaural dummy head recording system includes an acoustic baffle with the approximate size, shape, and weight of a human head. Small microphones are mounted where our ears are located.<br>
						<br>
						This recording system is designed to emulate the acoustic effects of the human head (just as our ears might hear sounds) and then capture the information on recording media.<br>
						<br>
						A number of recording equipment manufacturers make these &quot;heads,&quot; and they often have funny names (Sven, etc.).<br>
						<br>
						Thanks to Sonic Studios for this photo.</p>

      <p>Not surprisingly, humans are extremely adept at locating sounds in two
        dimensions, or the plane. We&#146;re great at figuring out the source
        direction of a sound, but not the height. When a lion is coming at us,
        it&#146;s nice of evolution to have provided us with the ability to know,
        quickly and without much thought, which way to run. It&#146;s perhaps
        more of a surprise that we&#146;re less adept at locating sounds in the
        third dimension, or more accurately, in the &quot;up/down&quot; axis.
        But we don&#146;t really need this ability. We can&#146;t jump high enough
        for that perception to do us much good. Barn owls, on the other hand,
        have little filters on their cheeks, making them extraordinarily good
        at sensing their sonic altitude distances. You would be good at sensing
        your sonic altitude distance, too, if you had to catch and eat, from the
        air, rapidly running field mice. So if it&#146;s not a frisbee heading
        at you more or less in the two-dimensional plane, but a softball headed
        straight down toward your head, we&#146;d suggest a helmet!</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_04.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
