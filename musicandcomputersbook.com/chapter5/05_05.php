<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_06.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="4" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 5: The Transformation of Sound by Computer</h1>
					<h1>Section 5.5: More on Convolution</h1>
					<p>Another interesting use of the phase vocoder is to perform <i>convolution.</i> We talked about convolution a lot in our section on reverb (both in the time domain and in the frequency domain).</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="723">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/power.conv.voice.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 5.18</b><br>
							Convolution: Schwarzhund</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">A simple convolution: a guitar power chord (the <i>impulse response</i>), a vocal excerpt, and the convolution of one with the other.</p>
					<p class="applet_caption">We have sonically turned the singer&#146;s head into the guitar chord: he&#146;s singing &quot;through&quot; the chord.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="723">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<p>Remember that a convolution multiplies every frequency 
        content in one sound by every frequency content in another (sometimes 
        called a <i>cross-multiply</i>). This is different from simply multiplying 
        each value in one sound by its one corresponding value in another. In 
        fact, as we mentioned, there is a well-known and surprisingly simple relationship 
        between these two concepts: multiplication in the time domain is the same 
        as convolution in the frequency domain (and vice versa). As you probably 
        saw in Section 5.2, the mathematics of convolution can get a little hairy. 
        But the uses of convolution for transformations of sound are pretty straightforward, 
        so we&#146;ll explain them in this section.</p>
					<h2>Cross-Synthesis</h2>
					<p>Convolution is a type of <i>cross-synthesis</i>: a general, less technical 
        term which means that some aspect of one sound is imposed onto another. 
        A simple example of cross-synthesis returns us to the subject of reverb. 
        We described the way that, by recording what is called the <i>impulse 
        response</i> of a room (its resonant characteristics) using a very short, 
        loud sound, we can place another sound in that room (at whatever position 
        we &quot;shot&quot;) by convolving the impulse response with that sound. 
        Surprisingly, by convolving any sound with white noise, we can simulate 
        simple reverb.</p>
					<h2>Using Convolution</h2>
					<p>Although reverberation is a common application of the convolution technique, convolution can be used creatively to produce unusual sounds as well. Simple-to-use convolution tools (like that in the program SoundHack) have only recently become available to a large community of musicians because up until recently, they only ran on quite large computers and in rather arcane environments. So we are likely to hear some amazing things in the near future using these techniques!<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="723">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/noise.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 5.19</b><br>
							Noise</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/inhale.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a><br>
						<b>Soundfile 5.20</b><br>
							Inhale</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter5/noise.inhale.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button34" border="0" alt=""></a><br>
						<b>Soundfile 5.21</b><br>
							Noise and inhale convolved</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">In this convolution example, Soundfile 
        5.19 and Soundfile 5.20 are convolved into Soundfile 5.21. The two sound 
        sources are noise and an inhale.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td width="580">
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="05_06.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>

