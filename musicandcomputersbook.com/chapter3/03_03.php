<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>


		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="03_04.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="13" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 3: The Frequency Domain</h1>
					<h1>Section 3.3: <b>Fourier and the Sum of Sines</b><br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="bottom" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter3/add.all.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button25" border="0" alt=""></a><br>
						<b>Soundfile 3.5<br>
						</b>Adding sine waves<br>
						<br>
					</p>
				</td>
				<td valign="top" bgcolor="white" width="580"><p>In this section, 
        we&#146;ll try to really explain the notion of a Fourier expansion by 
        building on the ideas of phasors, partials, and sinusoidal components 
        that we introduced in the previous section. </p>
      <p>A long time ago, French scientist and mathematician Jean Baptiste Fourier (1768&ndash;1830) proved the mathematical fact that any periodic waveform can be expressed as the sum of an infinite set of sine waves. The frequencies of these sine waves must be integer multiples of some fundamental frequency. </p>
					<p>In other words, if we have a trumpet sound at middle A (440 Hz), we know by Fourier&#146;s theorem that we can express this sound as a summation of sine waves: 440 Hz, 880Hz, 1,320Hz, 1,760 Hz..., or 1, 2, 3, 4... times the fundamental, each at various amplitudes. This is rather amazing, since it says that for every periodic waveform (one, by the way, that has pitch), we basically know everything about its partials except their amplitudes.</p>
					<p class="figure"><img height="632" width="484" src="../images/chapter3/simple.waves.jpeg"></p>
					<p class="figure_caption_flush_bottom"><b>Figure 3.20</b>&nbsp;&nbsp;The spectrum of the sine wave has energy only at one frequency. The triangle wave has energy at odd-numbered harmonics (meaning odd multiples of the fundamental), with the energy of each harmonic decreasing as 1 over the square of the harmonic number (1/<i>N</i><sup>2</sup>). In other words, at the frequency that is <i>N</i> times the fundamental, we have 1/<i>N</i><sup>2</sup> as much energy as in the fundamental.<br>
						<br>
        The partials in the sawtooth wave decrease in energy in proportion to 
        the inverse of the harmonic number (1/<i>N</i>). Pulse (or rectangle or 
        square) waveforms have energy over a broad area of the spectrum, but only 
        for a brief period of time.</p>
					<h2>Fourier Series</h2>
					<p>
							What exactly is a Fourier series, and how does it relate to phasors? We use phasors to represent our basic tones. The amazing fact is that any sound can be represented as a combination of phase-shifted, amplitude-modulated tones of differing frequencies. Remember that we got a hint of this concept when we discussed adding phasors in Section 3.2.</p>
					<p>A phasor is essentially a way of representing a sinusoidal function. What this means, mathematically, is that <i>any</i> sound can be represented as a sum of sinusoids. This sum is called a <i>Fourier series.</i> </p>
					<p>Note that we haven&#146;t limited these sounds to periodic sounds (if we did, we&#146;d have to add that last qualifier about integer multiples of a fundamental frequency). Nonperiodic, or <i>aperiodic</i>, sounds are just as interesting&#151;maybe even more interesting&#151;than periodic ones, but we have to do some special computer tricks to get a nice &quot;harmonic&quot; series out of them for the purposes of analysis and synthesis.</p>
					<p>But let&#146;s get down to the nitty-gritty. First, let&#146;s take a look at what happens when we add two sinusoids of the same frequency. Adding a sine and cosine of the same frequency gives a phase-shifted sine of the same frequency:</p>
					<p><img height="27" width="316" src="../images/chapter3/3.equations/3-3-1.gif"></p>
					<p>In fact, the amplitude of the sum, <i>C</i>, is given by:</p>
					<p><img height="35" width="129" src="../images/chapter3/3.equations/3-3-2.gif"></p>
					<p>The phase shift <img height="13" width="20" src="../images/chapter3/phi.jpg"> is given by the angle whose tangent is equal to <i>A/B.</i> The shorthand for this is:</p>
					<p><img height="25" width="130" src="../images/chapter3/3.equations/3-3-3.gif"></p>
					<p>We can visualize this with a phasor. Remember that the cosine is just a phase-shifted sine. Since the sine and cosine are moving at the same frequency, they are always &quot;out of sync&quot; by <img src="../images/shared/pi.gif" height="10" width="12">/2, so when we add them it looks like this:</p>
					<p class="figure"><img src="../images/chapter3/phasor_addition.gif" alt="" width="336" height="292" border="0"></p>
					<p>And we get another sinusoid of that frequency.</p>
					<p>
							Any periodic function of period 1 can be written as follows:</p>
					<p><img height="68" width="464" src="../images/chapter3/3.equations/3-3-4.gif"></p>
					
							Notice that these sums can be infinite! 
					<p>We have a nice shorthand for those possibly infinite sums (also called an <i>infinite series</i>):</p>
					<img height="47" width="437" src="../images/chapter3/3.equations/3-3-5.gif">
					<p>The two expressions after the &Sigma; signs are called the Fourier coefficients of the function <i>f</i>(<i>t</i>). The Fourier coefficient <i>A<sub>0</sub></i> has a special name: it is called the <i>DC term,</i> or the <i>DC offset.</i> It tells you the average value of the function. The Fourier coefficients make up a set of numbers called the <i>spectrum</i> of the sound. Now, when you think of the word &quot;spectrum,&quot; you might think of colors, like the spectrum of colors of the rainbow. In a way it&#146;s the same: the spectrum tells you how much of each frequency (color) is in the sound.</p>
					<p>The values of <i>A<sub>n</sub></i> and <i>B<sub>n</sub></i> for &quot;small&quot; values of <i>n</i> make up the low-frequency information, and we call these the <i>low-order Fourier coefficients.</i> Similarly, the big values of <i>n</i> index the high-frequency information. Since most sounds are made up of a lot of low-frequency information, the low-frequency Fourier coefficients have larger absolute value than the high-frequency Fourier coefficients.</p>
					<p>What this means is that it is theoretically possible to take a complex sound, like a person&#146;s voice, and decompose it into a bunch of sine waves, each at a different frequency, amplitude, and phase. These are called the <i>sinusoidal</i> or <i>spectral</i> components of a sound. To find them, we do a Fourier analysis. Fourier synthesis is the inverse process, where we take varying amounts of a bunch of sine waves and add them together (play them at the same time) to reconstruct a sound. Sounds a bit fantastic, doesn&#146;t it? But it works. This process of analyzing or synthesizing a sound based on its component sine waves is called performing a Fourier transform on the sound. When the computer does it, it uses a very efficient technique called the <i>fast Fourier transform</i> (or FFT) for analysis and the <i>inverse FFT</i> (IFFT) for synthesis.</p>
					<p class="figure"><img height="418" width="500" src="../images/chapter3/sumofsines.jpg"></p>
					<p class="figure_caption_flush_bottom"><b>Figure 3.21&nbsp;&nbsp;</b>What happens if we add a number of sine waves together? We end up with a complicated waveform that is the summation of the individual waves. This picture is a simple example: we just added up two sine waves. For a complex sound, hundreds or even thousands of sine waves are needed to accurately build up the complex waveform. By looking at the illustration from the bottom up, you can see that the inverse is also true&#151;the complex waveform can be broken down into a collection of independent sine waves.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption">&nbsp;</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure"><img height="355" width="493" src="../images/chapter3/trp.fft.jpeg"><br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<br>
						<a onclick="setSrc('../sounds/chapter3/trp.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button26" border="0" alt=""></a><br>
						<b>Soundfile 3.6</b><br>
						
						Trumpet</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure"><img height="370" width="502" src="../images/chapter3/trp.fft2.jpeg"></p>
					<p class="figure_caption"><b>Figure 3.22 </b>A trumpet note in an FFT (fast Fourier transform) analysis&#151;two views. The trumpet sound can be heard by clicking on Soundfile 3.6. Both of these pictures show the evolution of the amplitude of spectral components in time.</p>
					<p>The advantage of representing a sound in terms of its Fourier series is that it allows us to manipulate the frequency content directly. If we want to accentuate the high-frequency effects in a sound (make a sound brighter), we could just make all the high-frequency Fourier coefficients bigger in amplitude. If we wanted to turn a sawtooth wave into a square wave, we could just set to zero the Fourier coefficients of the even partials.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE256C2103'));return CSClickReturn()" href="../applets/3_3_see_fft.php" target="_blank" csclick="BCE256C2103"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 3.3</b><br>
						FFT demo</p>
				</td>
				<td valign="top" bgcolor="white" width="580"><p>In fact, we often 
        modify sounds by removing certain frequencies. This corresponds to making 
        a new function where certain Fourier coefficients are set equal to zero 
        while all others are left alone. When we do this we say that we <i>filter</i> 
        the function or sound. These sorts of filters are called <i>bandpass filters</i>, 
        and the frequencies that we leave unaltered in this sort of situation 
        are said to be in the <i>passband.</i> A low-pass filter puts all the 
        low frequencies (up to some bandwidth) in the passband, while a high-pass 
        filter puts all high frequencies (down to some cutoff) in the passband. 
        When we do this, we talk about high-passing and low-passing the sound. 
        In the following soundfiles, we listen to a sound and its high-passed 
        and low-passed versions. We&#146;ll talk a lot more about filters in Chapter 
        4.<br>
        <br>
      </p></td>
			</tr>
			<tr>
				<td colspan="2" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter3/dunn.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button22" border="0" alt=""></a><br>
						<b>Soundfile 3.7</b><br>
						<span class="icon_caption">Filter examples</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">We start with a sampled file of bugs. This recording was made by composer and sound artist David Dunn using very powerful <i>hydrophones</i> (microphones that work underwater) to amplify the &quot;microsound&quot; of bugs in a pond.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter3/dunn.hp.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button24" border="0" alt=""></a><br>
						<b>Soundfile 3.8</b><br>
						<span class="icon_caption">High-pass filtered</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This is David Dunn&#146;s bugs sound file filtered so that we hear only the frequencies above 1,000 Hz. In other words, we have high-pass filtered the sound.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter3/dunn.lp.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button27" border="0" alt=""></a><br>
						<b>Soundfile 3.9</b><br>
						<span class="icon_caption">Low-pass filtered</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This is the bugs sound file filtered so that we hear only the frequencies below 1,000 Hz. Here we have low-pass filtered the sound.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580"><p><br>
						
        Once you <i>have</i> the spectral content of a sound, there is a lot you 
        can do with it, but how do you get it?! That&#146;s what the FFT does, 
        and we&#146;ll talk about it in the next section.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="03_04.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
