<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="03_02.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="7" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 3: The Frequency Domain</h1>
					<h1>Section 3.1: Frequency Domain<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580"><p>Time-domain representations 
        show us a lot about the amplitude of a signal at different points in time. 
        <i>Amplitude</i> is a word that means, more or less, &quot;how much of 
        something,&quot; and in this case it might represent pressure, voltage, 
        some number that measures those things, or even the in-out deformation 
        of the eardrum. </p>
      <p>For example, the time-domain picture of the waveform in Figure 3.1 starts with the attack of the note, continues on to the steady-state portion (sustain) of the note, and ends with the cutoff and decay (release). We sometimes call the attack and decay <i>transients</i> because they only happen once and they don&#146;t stay around! We also use the word <i>transient</i>, perhaps more typically, to describe timbral fluctuations during the sound that are irregular or singular and to distinguish between those kinds of sounds and the steady state.</p>
					<p>From the typical sound event shown in Figure 3.1, we can tell something about how the sound&rsquo;s amplitude develops over time (what we call its <i>amplitude envelope</i>). But from this picture we can&#146;t really tell much of anything about what is usually referred to as the timbre or &quot;sound&quot; of the sound: What instrument is it? What note is it? Is it bright or dark? Is it someone singing or a dog barking, or maybe a Back Street Boys bootleg? Who knows! These time domain pictures all look pretty much alike.</p>
					<p class="figure_flush"><img height="197" width="580" src="../images/chapter3/mystery.time.jpg"></p>
					<p class="figure_caption"><b>Figure 3.1 </b>A time-domain waveform. It&#146;s easy to see the <i>attack, steady-state,</i> and <i>decay</i> portions of the &quot;note&quot; or sound event, because these are all pretty much variations of amplitude, which time-domain representations show us quite well. The amplitude envelope is a kind of average of this picture.<br>
						<br>
						We can even be a little more precise and mathematical. If the amplitude at the <i>n</i><sup>th</sup> sample in the above is <i>A</i>[<i>n</i>] and we make a new signal with amplitude, say, <i>S</i>[<i>n</i>], then the <i>n</i><sup>th</sup> sample (of <i>S</i>[<i>n</i>], our envelope) would be:<br>
						<br>
						<i>S</i>[<i>n</i>] = (<i>A</i>[<i>n</i>&ndash;1] + <i>A</i>[<i>n</i>] + <i>A</i>[<i>n</i>+1])/3<br>
							<br>
						This would look like the envelope. This averaging operation is sometimes called <i>smoothing</i> or <i>low-pass filtering.</i> We&#146;ll talk more about it later.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<a onclick="setSrc('../sounds/chapter3/monochord.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button24" border="0" alt=""></a><br>
						<b>Soundfile 3.1<br>
						</b>Monochord sound</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p><img height="432" width="576" src="../images/chapter3/monochordenv.jpep"></p>
                    <p class="figure_caption"><b>Figure 3.2</b>&nbsp;&nbsp;Monochord sound: signal, average signal envelope, peak signal envelope.<br>
					<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<a onclick="setSrc('../sounds/chapter3/trpt.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button25" border="0" alt=""></a><br>
						<b>Soundfile 3.2<br>
						</b>Trumpet sound</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p><img height="432" width="576" src="../images/chapter3/trptenv.jpeg"></p>
                    <p class="figure_caption"><b>Figure 3.3</b>&nbsp;&nbsp;Trumpet sound: signal, average signal envelope, peak signal envelope.<br>
					<br>
					</p>
					<h2><b>Two Sounds, and Two Different Kinds of Amplitude Envelopes</b></h2>
					<p>The two figures and sounds in Soundfiles 3.1 and 3.2 (one of a trumpet, one of a one-stringed instrument called a monochord, made for us by Sam Torrisi) illustrate different ways of looking at amplitude and the time domain. In each, the time-domain signal itself is given by the light blue area. This is exactly the same as what we showed you at the beginning of this section in Figure 3.1. But we&#146;ve added two more envelopes to these figures, to illustrate two useful ways to think of a sound event.</p>
					<p>The magenta line more or less follows the <i>peaks</i> of the signal, or its highest amplitudes. Note that it doesn&#146;t matter whether these amplitudes are very positive or very negative; all we really care about is their absolute value, which is more or less like saying how much energy or displacement (in either direction). Sometimes, we even simplify this further by measuring the peak-to-peak amplitude of a signal, just looking at the maximum range of amplitudes (this will tell us, for example, if our speakers/ears will be able to withstand the maximum of the signal). In Figure 3.2, we look at some number of samples and more or less remain on the highest value in that window (that&#146;s why it has a kind of staircase look to it).</p>
					<p>The dark blue line is a running average of the absolute value of the signal, which in effect smooths the sound out tremendously, and also attenuates it. There&#146;s a similar measure, called RMS (root-mean-squared) amplitude, that tries to give an overall average of energy. Once again, we used a running window technique to average the last <i>n</i> number of samples (where <i>n</i> is the length of the window). Different values for <i>n</i> would give very different pictures.</p>
					<p>Just to give you some idea how we generate these kinds of graphs and measurements, in Xtra bit 3.1 we&#146;ve included the computer code, written in a popular mathematical modeling program called MatLab, that made these pictures. By studying this code and the accompanying comments, you can get some idea of what computer music software often looks like and how you might go about making similar kinds of measurements.</p>
					
      <h2>The Frequency/Amplitude/Time Plot<br>
      </h2></td>
</tr>
<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
				<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2555D96'));return CSClickReturn()" href="../popups/chapter3/xbit_3_1.php" target="_blank" csclick="BCE2555D96"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 3.1<br>
						</b>MatLab code to plot amplitude envelopes</p>
				</td>
				<td valign="top" bgcolor="white" width="580"><p>Distinguishing 
        between sounds is one place where the frequency domain comes in. Figure 
        3.4 is a frequency/amplitude/time plot of the same sound as the time-domain 
        picture in Figure 3.1. This new kind of sound-image is called a <i>sonogram.</i> 
        Time still goes from left to right along the <i>x</i>-axis, but now the 
        <i>y</i>-axis is frequency, not amplitude. Amplitude is encoded in the 
        intensity of a point on the image: the darker the point, the more energy 
        present at that frequency at around that time.</p>
					<p>For example, the semi-dark line around 7,400 Hz shows that from about 0.05 second to 0.125 second, there is some contribution to the sound at that frequency. This is occurring in the attack portion. It pretty much dies after a short period of time.</p>
					<p class="figure_flush"><img height="338" width="580" src="../images/chapter3/mystery.freq.jpg"></p>
					<p class="figure_caption"><b>Figure 3.4 </b>This picture shows the same sound as that of the time domain in Figure 3.1, but now in the <i>frequency domain</i>, as a sonogram. Here, the <i>y</i>-axis is frequency (or, more accurately, frequency components). The darkness of the line indicates how much energy is present in that frequency component. The <i>x</i>-axis is, as usual, time.<br>
						<br>
						What sorts of information do the two pictures give us about the sound? Can you make some guesses about what sort of sound this might be?</p>
					
					
      <p>What does this sonogram tell us about the sound? Remember that we said 
        before that we use the entire frequency range to determine timbre as well 
        as pitch. As it turns out, any sound contains many smaller component sounds 
        at a wide variety of frequencies (we&#146;ll learn more about this later; 
        it&#146;s really important!). What you&#146;re seeing in this sonogram 
        is a representation of how all those component sounds change in frequency 
        and amplitude over time.<br>
        <br>
      </p></td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter3/mystery.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button22" border="0" alt=""></a><br>
						<b>Soundfile 3.3<br>
						</b>Mystery sound</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>Now listen to Soundfile 3.3 at left.<br>
					</p>
					
      <p>The sonogram shows that the mystery sound starts with a burst of energy 
        that is spread out across the frequency spectrum&#151;notice the spikes 
        that reach all the way up to the top frequencies in the image. Then it 
        settles down into a fairly constant, more concentrated, and lower energy 
        state, where it remains until the end, when it quickly fades out. This 
        is a pretty common description of a vibrating system: start it vibrating 
        out of its rest state (chaotic, loud), listen to it settle into some sort 
        of regular vibratory behavior, and then, if the energy source is removed 
        (for example, you stop blowing the horn or take your e-bow off your electric 
        guitar string), listen to it decay (again, chaotic). </p>
      <p>The presence of a band of high-amplitude, low-frequency energy coupled with some lower-amplitude, high-frequency energy implies that we&#146;re looking at some sort of pitched sound with a number of strong harmonics. The darkest low band is probably the fundamental note of the sound. By studying the sonogram, can you get a mental idea of what sort of sound it might be?</p>
					<p>Listen to the sound a few times while watching the waveform and sonogram images. Can you follow along? Is there a clear correlation between what you see and what you hear? Does the sound look the way it sounds? Do you agree that the sonogram gives you a more informative visual representation of the sound? Isn&#146;t the frequency domain cool?<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter3/warbler.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button26" border="0" alt=""></a><br>
						<b>Soundfile 3.4<br>
						</b>The song of the hooded warbler. Can you follow it with Figure 3.5?</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure"><img height="187" width="320" src="../images/chapter3/warbler.jpg"></p>
					<p class="figure_caption"><b>Figure 3.5</b>&nbsp;&nbsp;Song of the hooded warbler.<br>
								<br>
							This is another kind of sonogram, kind of like a negative image of the sound moving in pitch (the <i>y</i>-axis) over time. The thickness of the line shows a lot about the pitch range. What this old-style sonogram did was try to find the maximum energy concentration and give a picture of the moving pitch of a sound, natural or otherwise.<br>
							<br>
						Sometimes pictures like this, which were very common a long time ago, are called <i>melograms,</i> or <i>melographs,</i> because they graph pitch in time. We got this wonderful picture out of an old book about recording natural sounds!<br>
							<br>
						</p>
					<p class="figure"><b><img height="650" width="357" src="../images/chapter3/phono.photo.jpeg"></b></p>
					<p class="figure_caption_flush_bottom"><b>Figure 3.6</b>&nbsp;&nbsp;Just for historical interest, the picture above is an example of an old process called <i>phonophotography,</i> an early (1920s) method for capturing a graphic image of a sound. It&#146;s essentially a melographic technique.<br>
						<br>
						What we are looking at is a picture of a &quot;recording&quot; of a performance of the gospel song &quot;Swing Low, Sweet Chariot.&quot; This color image came from the work of a brilliant researcher named Metfessel.<br>
							<br>
							This kind of highly descriptive analysis greatly influenced music theorists in the first part of the 20th century. Many people saw it as a kind of revolutionary mechanism for describing sound and music, potentially removing music analysis from the realm of the aesthetic, the emotional, and the transcendental into a more modernist, scientific, and objective domain.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="03_02.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
