<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>


		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="03_06.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="15" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 3: The Frequency Domain</h1>
					<h1>Section 3.5: <b>Problems with the FFT/IFFT</b><br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure"><img height="497" width="251" src="../images/chapter3/instr.range.jpeg">&nbsp;&nbsp;&nbsp;<img height="233" width="244" src="../images/chapter3/vox.range.jpeg" align="top"></p>
					<p class="figure_caption"><b>Figure 3.31&nbsp;&nbsp;</b>There is a lot of &quot;wasted&quot; space in an FFT analysis&#151;most of the frequencies that are of concern to us tend to be below 5 kHz. Shown are the approximate ranges of some musical instruments and the human voice. It&#146;s also a big, big problem that the FFT divides the frequency range into linear segments (each frequency bin is the same &quot;width&quot;) while, as we well know, our perception of frequency is logarithmic.<br>
						<br>
							Charts courtesy of Geoff Husband and tnt-audio.com. Used with permission.</p>
					<p>The FFT often sounds like the perfect tool for exploring the frequency domain and timbre, right? Well, it does work very well for many things, but it&#146;s not without its problems. One of the main drawbacks is that the frequency bins are <i>linear.</i> For example, if we have a bin width of 43 Hz (which will be a result of dividing Nyquist frequency by the FFT frame size), then we have bins from 0 Hz to 43 Hz, 43 Hz to 86 Hz, 86 Hz to 129 Hz, and so on. </p>
					<p>The problem with this, as we learned earlier, is that the human ear responds to frequency logarithmically, not linearly. At low frequencies, 43 Hz is quite a wide interval&#151;the jump from 43 Hz to 86 Hz is a whole octave! But at higher frequencies, 43 Hz is a tiny interval (perceptually)&#151;less than a minor second. So the FFT has very fine high-frequency pitch resolution, but very poor low-frequency resolution.</p>
					<p>The effect of the FFT&#146;s linearity is that, for us, much of the FFT data is &quot;wasted&quot; on recording high-frequency information very accurately, at the expense of the low-frequency information that is generally more useful in a musical context. Wavelets, which we&#146;ll look at in Section 3.6, are one approach to solving this problem.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="736">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter3/sine.sweep.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button25" border="0" alt=""></a><br>
						<b>Soundfile 3.10</b><br>
						<span class="icon_caption">Sine wave lobes</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Soundfile 3.10 is an example of a sine wave swept from 50 Hz to 10 kHz, processed through an FFT. Figure 3.32 illustrates the sine wave sweep in an FFT analysis.</p>
					<p class="applet_caption">The <i>lobes</i> that you see are the result of the energy of the sine wave &quot;centering&quot; in the successive FFT bands and then fading slightly as the width of the band forces the FFT into less accurate representations of the moving frequency (until it centers in the next band). In other words, one of the hardest things for an FFT to represent is a simply moving sine wave!</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="736">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure"><img height="401" width="514" src="../images/chapter3/sine.fft.jpeg"></p>
					<p class="figure_caption_flush_bottom"><b>Figure 3.32</b>&nbsp;&nbsp;Sweeping a sine wave through an FFT.</p>
					<h2>Frequency and Time Resolution Trade-Off</h2>
					<p>A related drawback of the FFT is the trade-off that must be made between frequency and time resolution. The more accurately we want to measure the frequency content of a signal, the more samples we have to analyze in each frame of the FFT. Yet there is a cost to expanding the frame size&#151;the larger the frame, the less we know about the temporal events that take place within that frame. </p>
					<p>In other words, more samples require more time; but the longer the time, the less the sound over that interval looks like a sine wave, or something periodic&#151;so the less well it is represented by the FFT. We simply can&#146;t have it both ways!</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<a onclick="setSrc('../sounds/chapter3/mann.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button22" border="0" alt=""></a><br>
						<b>Soundfile 3.11<br>
						</b></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure_flush"><img height="190" width="580" src="../images/chapter3/mann.512.jpg"></p>
					<p class="figure_flush"><img height="190" width="580" src="../images/chapter3/mann.2048.jpg"><br>
					<p class="figure_caption"><b>Figure 3.33&nbsp;&nbsp;</b>Selecting an FFT size involves making trade-offs in terms of time and frequency accuracy. Basically it boils down to this: The more accurate the analysis is in one domain, the less accurate it will be in the other. This figure illustrates what happens when we choose different frame sizes.<br>
						<br>
						In the first illustration, we used an FFT size of 512 samples, giving us pretty good time resolution. In the second, we used 2,048 samples, giving us pretty good frequency resolution. As a result, frequencies are smeared vertically in the first analysis, while time is smeared horizontally in the second. What&#146;s the solution to the time/frequency uncertainty dilemma? Compromise.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" width="736">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter3/tapeaiff.normal.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button24" border="0" alt=""></a><br>
						<b>Soundfile 3.12</b><br>
						<span class="icon_caption">Beat sound</span></p>
				</td>
				<td valign="top" width="580">
					<p class="applet_caption">A fairly normal-sounding beat soundfile.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" width="736">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter3/tapeaiff.lotsbins.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button23" border="0" alt=""></a><br>
						<b>Soundfile 3.13</b><br>
						<span class="icon_caption">Time smeared</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">In this example the beat soundfile has been processed to provide accurate frequency resolution but inaccurate time resolution, or rhythmic smearing.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" width="736">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter3/tapeaiff.fewbins.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button26" border="0" alt=""></a><br>
						<b>Soundfile 3.14</b><br>
						<span class="icon_caption">Frequency smeared</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">In this example the beat soundfile has been processed to provide accurate rhythmic resolution but inaccurate frequency resolution, or spectral smearing.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" width="736">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<h3>Time Smearing</h3>
					<p>We mentioned that 1,024 samples (1k) is a pretty common frame size for 
        an audio FFT. At a sample rate of 44.1 kHz, 1,024 samples is about 0.022 
        second of sound. What that means is that all the sonic events that take 
        place within that 0.022 second will be lumped together and analyzed as 
        one event. Because of the nature of the FFT, this &quot;event&quot; is 
        actually treated as if it were an infinitely repeating periodic waveform. 
        The amplitudes of the frequency components of all the sonic events in 
        that time frame will be averaged, and these averages will end up in the 
        frequency bins. </p>
					<p>This is known as <i>time smearing.</i> Now let&#146;s say that we need 
        more than the 43 Hz frequency resolution that a 1k FFT gives us. To get 
        better frequency resolution, we need to use a bigger frame size. But a 
        bigger frame size means that even more samples will be lumped together, 
        giving us even worse time resolution. At a frame size of 2k we get a frequency 
        resolution of about 21.5 Hz, but our time resolution goes down to about 
        0.05 (1/20) of a second. And, believe it or not, a great deal can happen 
        in 1/20 of a second!</p>
					<h3>Good Time Resolution</h3>
					<p>Conversely, if we need good time resolution (say we&#146;re analyzing some percussive sounds and we want to know exactly when they happen), we need to shrink the frame size. The ideal frame size for the time domain would of course be one sample&#151;that way we would know at exactly which sample something happened.</p>
					<p>Unfortunately, with only one sample to analyze, we would get no useful frequency information out of the FFT at all. A more reasonable frame size and one that is considered small for audio, such as 256 samples (a 0.006-second chunk of time), gives us 128 analysis bands, for a bin width of about 172 Hz. While a 0.006-second time resolution is reasonable, 172 Hz is a pretty dreadful frequency resolution. That would put several bottom octaves of the piano into one averaged bin.</p>
					<h3>A Compromise</h3>
					<p>So what&#146;s the answer to this time/frequency dilemma? There really isn&#146;t one. If we use the FFT to do our analysis, we&#146;re stuck with the fact that higher resolution in one domain results in lower resolution in the other. The trick is to find a useful balance, based on the types of sounds we are analyzing. No single frame size will work well for all sounds.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="03_06.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
