<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>


		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="03_05.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="4" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 3: The Frequency Domain</h1>
					<h1>Section 3.4: <b>The DFT, FFT, and IFFT</b><br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580"><p>The most common 
        tools used to perform Fourier analysis and synthesis are called the fast 
        Fourier transform (FFT) and the inverse fast Fourier transform (IFFT). 
        The FFT and IFFT are optimized (very fast) computer-based algorithms that 
        perform a generalized mathematical process called the <i>discrete Fourier 
        transform</i> (DFT). The DFT is the actual mathematical transformation 
        that the data go through when converted from one domain to another (time 
        to frequency). Basically, the DFT is just a slow version of the FFT&#151;too 
        slow for our impatient ears and brains! </p>
      <p>FFTs, IFFTs, and DFTs became really important to a lot of disciplines when engineers figured out how to take samples quickly enough to generate enough data to re-create sound and other analog phenomena digitally. Remember, they don&#146;t just work on sounds; they work on any continuous signal (images, radio waves, seismographic data, etc.).<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25727107'));return CSClickReturn()" href="../popups/chapter3/xbit_3_2.php" target="_blank" csclick="BCE25727107"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 3.2<br>
						</b>Dan&#146;s history<br>
						of the FFT</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>An FFT of a time domain signal takes the samples and gives us a new set of numbers representing the frequencies, amplitudes, and phases of the sine waves that make up the sound we&#146;ve analyzed. It is these data that are displayed in the sonograms we looked at in Section 1.2.</p>
					<p class="figure"><img height="297" width="508" src="../images/chapter3/fftbin3.4.jpeg"></p>
					<p class="figure_flush_top"><img height="326" width="453" src="../images/chapter3/bins.jpeg"></p>
					<p class="figure_caption"><b>Figure 3.23</b>&nbsp;&nbsp;Graph and table of spectral components.</p>
					<p>Figure 3.23 shows the first 16 bins of a typical FFT analysis after the conversion is made from real and imaginary numbers to amplitude/phase pairs. We left out the phases, because, well, it was too much trouble to just make up a bunch of arbitrary phases between 0 and 2<img src="../images/shared/pi.gif" height="10" width="12">. In a lot of cases, you might not need them (and in a lot of cases, you would!). In this case, the sample rate is 44.1 kHz and the FFT size is 1,024, so the bin width (in frequency) is the Nyquist frequency (44,100/2 = 22,050) divided by the FFT size, or about 22 Hz.</p>
					<p>Amplitude values are assumed to be between 0 and 1, and notice that they&#146;re quite small because they all must sum to 1 (and there are a lot of bins!).</p>
					<p>We confess that we just sort of made up the numbers; but notice that we made them up to represent a sound that has a simple, more or less harmonic structure with a fundamental somewhere in the 66 Hz to 88 Hz range (you can see its harmonics at around 2, 3, 4, 5, and 6 times its frequency, and note that the harmonics decrease in amplitude more or less like they would in a sawtooth wave).</p>
					<h2><b>How the FFT Works</b></h2>
					<p>The way the FFT works is fairly straightforward. It takes a chunk of time called a <i>frame</i> (a certain number of samples) and considers that chunk to be a single period of a repeating waveform. The reason that this works is that most sounds are &quot;locally stationary&quot;; meaning that over any short period of time, the sound really does look like a regularly repeating function. </p>
					<p>The following is a way to consider this mathematically&#151;taking a window over some portion of some signal that we want to consider as a periodic function.</p>
					<table width="100%" border="0" cellspacing="0" cellpadding="16" bgcolor="#cccccc">
						<tr>
							<td>
								<h3>The Fast Fourier Transform in a Nutshell: <br>Computing Fourier Coefficients</h3>
								<p>Here&#146;s a little three-step procedure for digital sound processing.</p>
								<ol>
									<li><i>Window</i>
									<li><i>Periodicize</i>
									<li><i>Fourier transform</i> (this also requires sampling, at a rate equal to 2 times the highest frequency required). We do this with the FFT. Following is an illustration of steps 1 and 2.
								</ol>
								<p>Here&#146;s the graph of a (periodic) function, <i>f</i>(<i>t</i>). (Note that <i>f</i>(<i>t</i>) need not be a <i>periodic</i> function.)</p>
								<p class="figure"><img height="180" width="327" src="../images/chapter3/win.per.1.jpeg"></p>
								<p class="figure_caption"><b>Figure 3.24</b></p>
								<p>Suppose we&rsquo;re only interested in the portion of the graph between 0 &#x2264; <i>t</i> &#x2264; 1. Following is a graph of the window function we need to use. We&rsquo;ll call the function <i>w</i>(<i>t</i>). Note that <i>w</i>(<i>t</i>) equals 1 only in the interval 0 &#x2264; t &#x2264; 1 and it&rsquo;s 0 everywhere else.</p>
								<p class="figure"><img height="186" width="330" src="../images/chapter3/win.per.2.jpeg"></p>
								<p class="figure_caption"><b>Figure 3.25</b></p>
								
								
            <p>In step 1, we need to <i>window</i> the function. In Figure 3.25 we&rsquo;ve plotted both the window function, <i>w</i>(<i>t</i>) 
              (which is nonzero in the region we&#146;re interested in) and function 
              <i>f</i>(<i>t</i>) in the same picture. </p>
            <p class="figure"><img height="182" width="347" src="../images/chapter3/win.per.3.jpeg"></p>
								<p class="figure_caption"><b>Figure 3.26</b></p>
								
								
            <p>In Figure 3.26 we&rsquo;ve plotted <i>f</i>(<i>t</i>)*<i>w</i>(<i>t</i>), 
              which is the periodic function multiplied by the windowing function. 
              From this figure, it&#146;s obvious what part of <i>f</i>(<i>t</i>) 
              we&#146;re interested in. </p>
            <p class="figure"><img height="189" width="326" src="../images/chapter3/win.per.4.jpeg"></p>
								<p class="figure_caption"><b>Figure 3.27</b></p>
								
								
            <p>In step 2, we need to periodically extend the windowed function, 
              <i>f</i>(<i>t</i>)*<i>w</i>(<i>t</i>), all along the <i>t</i>-axis. 
            </p>
            <p class="figure"><img height="191" width="328" src="../images/chapter3/win.per.5.jpeg"></p>
								<p class="figure_caption"><b>Figure 3.28</b></p>
								
								
            <p>Great! We now have a periodic function, and the Fourier theorem 
              says we can represent this function as a sum of sines and cosines. 
              This is step 3. </p>
            <p>Remember, we can also use other, nonsquare windows. This is done to ameliorate the effect of the square windows on the frequency content of the original signal.</p>
							</td>
						</tr>
					</table>
					<p>Now, once we&#146;ve got a periodic function, all we need to do is figure out, using the FFT, what the component sine waves of that waveform are.</p>
					<p>As we&#146;ve seen, it is possible to represent any periodic waveform as a sum of phase-shifted sine waves. In theory, the number of component sine waves is infinite&#151;there is no limit to how many frequency components a sound might have. In practice, we need to limit ourselves to some predetermined number. This limit has a serious effect on the accuracy of our analysis.</p>
					<p>Here&#146;s how that works: rather than looking for the frequency content of the sound at all possible frequencies (an infinitely large number&#151;100.000000001 Hz, 100.000000002 Hz, 100.000000003 Hz, etc.), we divide up the frequency spectrum into a number of frequency bands and call them <i>bins.</i> The size of these bins is determined by the number of samples in our analysis frame (the chunk of time mentioned above). The number of bins is given by the formula:</p>
					<p>number of bins = frame size/2</p>
					<h2>Frame Size</h2>
					<p>So let&#146;s say that we decide on a frame size of 1,024 samples. This is a common choice because most FFT algorithms in use for sound processing require a number of samples that is a <i>power of two</i>, and it&#146;s important not to get too much or too little of the sound.</p>
					<p>A frame size of 1,024 samples gives us 512 frequency bands. If we assume that we&#146;re using a sample rate of 44.1 kHz, we know that we have a frequency range (remember the <i>Nyquist theorem</i>) of 0 kHz to 22.05 kHz. To find out how wide each of our frequency bins is, we use the following formula:</p>
					<p>bin width = frequency/number of bins</p>
					<p>This formula gives us a bin width of about 43 Hz. Remember that frequency perception is logarithmic, so 43 Hz gives us worse resolution at the low frequencies and better resolution at higher frequencies.</p>
					<p>By selecting a certain frame size and its corresponding bandwidth, we avoid the problem of having to compute an infinite number of frequency components in a sound. Instead, we just compute one component for each frequency band.</p>
					<h2>Software That Uses the FFT</h2>
					<p class="figure"><img height="256" width="243" src="../images/chapter3/pvc.jpg"></p>
					<p class="figure_caption"><b>Figure 3.29&nbsp;&nbsp;</b>Example of a commonly used FFT-based program: the phase vocoder menu from Tom Erbe&#146;s SoundHack.<br>
							<br>
						Note that the user is allowed to select (among several other parameters) the number of bands in the analysis. This means that the user can customize what is called the <i>time/frequency resolution trade-off</i> of the FFT. Don&#146;t ask us what the other options on this screen are&#151;download the program and try it yourself!</p>
					<p>There are many software packages available that will do FFTs and IFFTs of your data for you and then let you mess around with the frequency content of a sound. In Chapter 4 we&#146;ll talk about some of the many strange and wonderful things that can be done to a sound in the frequency domain.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25735108'));return CSClickReturn()" href="../popups/chapter3/xbit_3_3.php" target="_blank" csclick="BCE25735108"><img src="../images/global/popup.gif" width="80" height="65" name="button6" border="0"></a><br>
						<b>Xtra bit 3.3</b><br>
						
						The mathematics<br>
						of magnitude and phase in the FFT</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>The details of how the FFT works are well beyond the scope of this book. What is important for our purposes is that you understand the general idea of analyzing a sound by breaking it into its frequency components and, conversely, by using a bunch of frequency components to synthesize a new sound. The FFT has been understood for a long time now, and most computer music platforms have tools for Fourier analysis and synthesis.</p>
					<p class="figure"><img height="148" width="254" src="../images/chapter3/histogram.jpg"></p>
					<p class="figure_caption"><b>Figure 3.30&nbsp;&nbsp;</b>Another way to look at the frequency spectrum is to remove time as an axis and just consider a sound as a histogram of frequencies. Think of this as averaging the frequencies over a long time interval. This kind of picture (where there&#146;s no time axis) is useful for looking at a short-term snapshot of a sound (often just one frame), or perhaps even for trying to examine the spectral features of a sound that doesn&#146;t change much over time (because all we see are the &quot;averages&quot;).</p>
					<p>The <i>y</i>-axis tells us the amplitude of each component frequency. Since we&rsquo;re looking at just one frame of an FFT, we usually assume a periodic, unchanging signal. A histogram is generally most useful for investigating the steady-state portion of a sound. (Figure 3.30 is a screen grab from SoundHack.)</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="03_05.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
