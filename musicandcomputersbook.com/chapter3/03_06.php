<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>


		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="../chapter4/04_01.php">Next Chapter --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="2" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 3: The Frequency Domain</h1>
					<h1>Section 3.6: Some Alternatives to the FFT<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580"><p>We know that in 
        previous sections we&#146;ve made it sound like the FFT is the only game 
        in town, but that&#146;s not entirely true. Historically,the FFT was the 
        first, most important, and most widely used algorithm for frequency domain 
        analysis. Musicians, especially, have taken a liking to it because there 
        are some well-known references on its implementation, and computer musicians 
        especially have gained a lot of experience in its nuances. It&#146;s fairly 
        simple to program, and there are a lot of existing software programs to 
        use if you get lazy. Also, it works pretty well (emphasis on the word 
        &quot;pretty&quot;). </p>
      <p>The FFT breaks up a sound into sinusoids, but there may be reasons why you&#146;d like to break a sound into <i>other</i> sorts of basic sounds. These alternate transforms <i>can</i> work better, for some purposes, than the FFT. (A <i>transform</i> is just something that takes a list of the sample values and turns the values into a new list of numbers that describes a possible new way to add up different basic sounds.) </p>
					<p>For example, <i>wavelet transforms</i> (sometimes called <i>dynamic base transforms</i>) can modify the resolution for different frequency ranges, unlike the FFT, which has a constant bandwidth (and thus is, by definition, less sensitive to lower frequencies&#151;the important ones&#151; than to higher ones!). Wavelets also use a variety of different analysis waveforms (as opposed to the FFT, which only uses sinusoids) to get a better representation of a signal. Unfortunately, wavelet transforms are still a bit uncommon in computer software, since they are, in general, harder to implement than FFTs. One of the big problems is deciding which wavelet to use, and in what frequency bandwidth. Often, that decision-making process can be more important (and time-consuming) than the actual transform!</p>
					<p>Wavelets are a pretty hot topic though, so we&#146;ll probably be seeing some wavelet-based techniques emerge in the near future. </p>
					<p class="figure"><img height="279" width="370" src="../images/chapter3/wavelet.jpg"></p>
					<p class="figure_caption"><b>Figure 3.34</b> Some common wavelet analysis waveforms. Each of these is a prototype, or &quot;mother wavelet,&quot; that provides a basic template for all the related analysis waveforms.<br>
						<br>
						A wavelet analysis will start with one of these waveforms and break up the sound into a sum of translated and stretched (dilated) versions of these.</p>
					<h2>McAulay-Quatieri (MQ) Analysis</h2>
					<p>Another interesting approach to a more organized, information-rich set of transforms is the extended McAulay-Quatieri (MQ) analysis algorithm. This is used in the popular Macintosh program Lemur Pro, written by the brilliant computer music researcher Kelly Fitz. </p>
					<p>MQ analysis works a lot like normal FFTs&#151;it tries to figure out how a sound can be re-created using a number of sine waves. However, Lemur Pro is an enhancement of more traditional FFT-based software in that it uses the resulting sine waves to extract frequency tracks from a sound. These tracks attempt to follow particular components of the sound as they change over time. That is, MQ not only represents the amplitude trajectories of partials, but also tries to describe the ways that spectral components change in frequency over time. This is a fundamentally different way of describing spectra, and a powerful one.</p>
					<p>MQ is an excellent idea, as it allows the analyst a much higher level of data representation and provides a more perceptually significant view of the sound. A number of more advanced FFT-based programs also implement some form of frequency tracking in order to improve the results of the analysis, similar to that of MQ analysis.</p>
					<p>For example, even though a sine wave might sweep from 1 kHz to 2 kHz, it would still be contained in one MQ track (rather than fading in and out of FFT bins, forming what are called <i>lobes</i>). </p>
					<p>An MQ-type analysis is often more useful for sound work than regular FFT analysis, since it has sonic cognition ideas built into it. For example, the ear tends to follow frequencies in time, much like an MQ analysis, and in our perception loud frequencies tend to mask neighboring quieter frequencies (as do MQ analyses). These kinds of sophisticated analysis techniques, where frequency and amplitude are isolated, suggest purely graphical manipulation of sounds, and we&#146;ll show some examples of that in Section 5.7.</p>
					<p class="figure"><img height="182" width="348" src="../images/chapter3/lemurscreenshot.jpg"></p>
					<p class="figure_caption"><b>Figure 3.35</b>&nbsp;&nbsp;Frequency tracks in Lemur Pro. The information looks much like one of our old sonograms, but in fact it is the result of a great deal of high-level processing of FFT-type data.</p>
					<p class="figure"><img height="204" width="309" src="../images/chapter3/MQ.jpeg"></p>
					<p class="figure_caption_flush_bottom"><b>Figure 3.36</b>&nbsp;&nbsp;MQ plot of 0'00&quot; to 5'49&quot; of Movement II: from the electro-acoustic work &quot;Sud&quot; by the great French composer and computer music pioneer Jean-Claude Risset. Photo by David Hirst, an innovative composer and computer music researcher from Australia.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="../chapter4/04_01.php">Next Chapter --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
