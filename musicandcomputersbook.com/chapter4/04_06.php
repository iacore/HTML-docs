<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>


		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_07.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td rowspan="12" valign="top" bgcolor="white" width="20">&nbsp;</td>
    <td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
    <td valign="top" bgcolor="white" width="580"> <h1>Chapter 4: The Synthesis of Sound by Computer</h1>
					<h1>Section 4.6: Waveshaping<br>
						<br>
					</h1>
				</td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125"></td>
    <td valign="top" bgcolor="white" width="580"><p><i>Waveshaping</i> is a popular
        synthesis-and-transformation technique that turns simple sounds into complex
        sounds. You can take a pure tone, like a sine wave, and transform it into
        a harmonically rich sound by changing its shape. A guitar fuzz box is
        an example of a waveshaper. The unamplified electric guitar sound is fairly
        close to a sine wave. But the fuzz box amplifies it and gives it sharp
        corners. We have seen in earlier chapters that a signal with sharp corners
        has lots of high harmonics. Sounds that have passed through a waveshaper
        generally have a lot more energy in their higher-frequency harmonics,
        which gives them a &quot;richer&quot; sound. </p>
      <h2>Simple Waveshaping Formulae</h2>
      <p>A waveshaper can be described as a function that takes the original signal
        <i>x</i> as input and produces a new output signal <i>y.</i> This function
        is called the <i>transfer function</i>.</p>
      <p><i>y</i> = <i>f</i>(<i>x</i>)</p>
      <p>This is simple, right? In fact, it&#146;s much simpler than any other
        function we&#146;ve seen so far. That&#146;s because waveshaping, in its
        most general form, is just any old function. But there&#146;s a lot more
        to it than that. In order to change the shape of the function (and not
        just make it bigger or smaller), the function must be nonlinear, which
        means it has exponents greater than 1, or transcendental (like sines,
        cosines, exponentials, logarithms, etc.). You can use almost any function
        you want as a waveshaper. But the most useful ones output zero when the
        input is zero (that&#146;s because you usually don&#146;t want any output
        when there is no input).</p>
      <p>0 = <i>f</i>(0)</p>
      <p>Let&#146;s look at a very simple waveshaping function:</p>
      <p><i>y</i> = <i>f</i>(<i>x</i>) = <i>x</i> * <i>x</i> * <i>x</i> = <i>x</i><sup>3</sup></p>
      <p>What would it look like to pass a simple sine wave that varied from <nobr>-1.0</nobr>
        to 1.0 through this waveshaper? If our input <i>x</i> is sin(<i>wt</i>),
        then:</p>
      <p><i>y</i> = <i>x</i><sup>3</sup> = sin<sup>3</sup>(<i>wt</i>)</p>
      <p>If we plot both functions (sin(<i>x</i>) and the output signal), we can
        see that the original input signal is very round, but the output signal
        has a narrower peak. This will give the output a richer sound.</p>
      <p class="figure"><img src="../images/chapter4/cubic_waveshaper.jpg" nosave height="279" width="337"></p>
      <p class="figure_caption"><b>Figure 4.15</b>&nbsp;&nbsp;Waveshaping by <i>x</i><sup>3</sup>.</p>
      <p>This example gives some idea of the power of this technique. A simple
        function (sine wave) gets immediately transformed, using simple math and
        even simpler computation, into something new. </p>
      <p>One problem with the <i>y</i> = <i>x</i><sup>3</sup> waveshaper is that
        for <i>x-</i>values outside the range &ndash;1.0 to +1.0, <i>y</i> can
        get very large. Because computer music systems (especially sound cards)
        generally only output sounds between &ndash;1.0 and +1.0, it is handy
        to have a waveshaper that takes any input signal and outputs a signal
        in the range &ndash;1.0 to +1.0. Consider this function:</p>
      <p><i>y</i> = <i>x</i> / (1 + |<i>x</i>|)</p>
      <p>When <i>x</i> is zero, <i>y</i> is zero. Plug in a few&nbsp; numbers
        for <i>x</i>, like 0.5, 7.0, 1,000.0, &ndash;7.0, and see what you get.
        As <i>x</i> gets larger (approaches positive infinity), <i>y</i> approaches
        +1.0 but never reaches it. As <i>x</i> approaches negative infinity, <i>y</i>
        approaches &ndash;1.0 but never reaches it. This kind of curve is sometimes
        called <i>soft clipping</i> because it does not have any hard edges. It
        can give a nice &quot;tubelike&quot; distortion sound to a guitar. So
        this function has some nice properties, but unfortunately it requires
        a divide, which takes a lot more CPU power than a multiply. On older or
        smaller computers, this can eat up a lot of CPU time (though it&#146;s
        not much of a problem nowadays).</p>
      <p>Here is another function that is a little easier to calculate. It is
        designed for input signals between &ndash;1.0 and +1.0.</p>
      <p><i>y</i> = 1.5<I>x</I> - 0.5<i>x</i><sup>3<br>
        <br>
        </sup></p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="750"> <hr>
    </td>
			</tr>
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25DE8154'));return CSClickReturn()" href="../applets/4_6_shapeformulae.php" target="_blank" csclick="BCE25DE8154"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 4.9</b><br>
        Changing the shape of a waveform </p></td>
				<td valign="top" bgcolor="white" width="580"> <p class="applet_caption">This
        applet plays a sine wave through various waveshaping formulae.</p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="750"> <hr>
    </td>
			</tr>
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580"> <h2> Chebyshev Polynomials</h2>
      <p>A transfer function is often expressed as a polynomial, which looks like:</p>
      <p><i>y</i> = <i>f</i>(<i>x</i>) = <i>d</i><sub>0</sub> + <i>d</i><sub>1</sub><i>x</i>
        + <i>d</i><sub>2</sub><i>x</i><sup>2</sup> + <i>d</i><sub>3</sub><i>x</i><sup>3</sup>
        + ... + <i>d</i><sub><i>N</i></sub><i>x</i><sup><i>N</i></sup></p>
      <p>The highest exponent <i>N</i> of this polynomial is called the &quot;order&quot; of the polynomial. In Applet 4.9, we saw that the <i>x</i><sup>2</sup>
        formula resulted in a doubling of the pitch. So a polynomial of order
        2 produced strong second harmonics in the output. It turns out that a
        polynomial of order <i>N</i> will only generate frequencies up to the
        <i>N</i>th harmonic of the input sine wave. This is a useful property
        when we want to limit the high harmonics to a value below the Nyquist
        rate to avoid aliasing.</p>
      <p>Back in the 19th century, Pafnuty Chebyshev discovered a set of polynomials
        known as the Chebyshev polynomials. Mathematicians like them for lots
        of different reasons, but computer musicians like them because they can
        be used to make weird noises, er, we mean music.&nbsp;These Chebyshev
        polynomials have the property that if you input a sine wave of amplitude
        1.0, you get out a sine wave whose frequency is <i>N</i> times the frequency
        of the input wave. So, they are like frequency multipliers. If the amplitude
        of the input sine wave is less than 1.0, then you get a complex mix of
        harmonics. Generally, the lower the amplitude of the input, the lower
        the harmonic content. This gives musicians a single number, sometimes
        called the <i>distortion index,</i> that they can tweak to change the
        harmonic content of a sound. If you want a sound with a particular mixture
        of harmonics, then you can add together several Chebyshev polynomials
        multiplied by the amount of the harmonic that you desire. Is this cool,
        or what? </p>
      <p>Here are the first few Chebyshev polynomials:</p>
      <p><i>T</i><sub>0</sub>(<i>x</i>) = 1<br>
        <i>T</i><sub>1</sub>(<i>x</i>) = <i>x</i><br>
        <i>T</i><sub>2</sub>(<i>x</i>) = 2<i>x</i><sup>2</sup> &ndash; 1<br>
        <i>T</i><sub>3</sub>(<i>x</i>) = 4<i>x</i><sup>3</sup> &ndash; 3<i>x</i><br>
        <i>T</i><sub>4</sub>(<i>x</i>) = 8<i>x</i><sup>4</sup> &ndash; 8<i>x</i><sup>2</sup>
        + 1</p>
      <p>You can generate more Chebyshev polynomials using this <I>recursive formula</I>
        (a recursive formula is one that takes in its own output as the next input):</p>
      <p><i>T</i><sub><i>k</i>+1</sub>(<i>x</i>) = 2<i>xT</i><sub><i>k</i></sub>(<i>x</i>)
        &ndash; <i>T</i><sub><i>k</i>&ndash;1</sub>(<i>x</i>)</p>
      <h2><b>Table-Based Waveshapers</b></h2>
      <p>Doing all these calculations in realtime at audio rates can be a lot
        of work, even for a computer. So we generally precalculate these polynomials
        and put the results in a table. Then when we are synthesizing sound, we
        just take the value of the input sine wave and use it to look up the answer
        in the table. If you did this during an exam it would be called cheating,
        but in the world of computer programming it is called <i>optimization.</i>
      </p>
      <p>One big advantage of using a table is that regardless of how complex
        the original equations were, it always takes the same amount of time to
        look up the answer. You can even draw a function by hand without using
        an equation and use that hand-drawn function as your transfer function.<br>
        <br>
      </p></td>
  </tr>
  <!-- <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr>
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25DF0155'));return CSClickReturn()" href="../applets/4_6_shapetable.php" target="_blank" csclick="BCE25DF0155"><img src="../images/global/applet.gif" width="80" height="65" name="3" border="0"></a><br>
						<b>Applet 4.10</b><br>
        Waveshaping </p></td>
				<td valign="top" bgcolor="white" width="580"> <p class="applet_caption">This
        applet plays sine waves through polynomials and hand-drawn waves.</p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr> -->
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580"> <h2>Don Buchla&#146;s Synthesizers</h2>
      <p>Don Buchla, a pioneering designer of synthesizers, created a series of
        instruments based on digital waveshaping. One such instrument, known as
        the <i>Touche</i>, was released in 1978. It had 16 digital oscillators
        that could be combined into eight voices. The Touche had extensive programming
        capabilities and had the ability to morph one sound into another. Composer
        David Rosenboom worked with Buchla and developed much of the software
        for the Touche. Rosenboom produced an album in 1981 called <i>Future Travel</i>
        using primarily the Touche and the Buchla 300 Series Music System.<br>
        <br>
      </p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/toyoji_patch_w_flute_exc.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 4.22a</b><br>
        Experimental waveshaping: &quot;Toyoji Patch&quot; </p>
      <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/toyoji_patch_w_flute_exc.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 4.22b</b><br>
        Experimental waveshaping: &quot;Toyoji Patch&quot; </p></td>
				<td width="580" valign="top" bgcolor="white"> <p class="applet_caption">Now
        that you know a lot about waveshaping, Chebyshev polynomials, and transfer
        functions, we&#146;ll show you what happens when the information gets
        into the wrong hands!</p>
      <p class="applet_caption">These soundfiles are two recordings done in the
        mid-1980s, at the Mills College Center for Contemporary Music, by one
        of our authors (Larry Polansky). They use an highly unusual live, interactive
        computer music waveshaping system.</p> <p class="applet_caption">&quot;Toyoji
        Patch&quot; was a piece/software/installation written originally for trombonist
        and composer Toyoji Tomita to play with. It was a real-time feedback system,
        which fed live audio into the transfer function of a set of six digital
        waveshaping oscillators. The hardware was an old S-100 68000-based computer
        system running the original prototype of a computer music language called
        HMSL, including a GUI and set of instrument drivers and utilities for
        controlling synthesizer designer Don Buchla&#146;s 400 series digital
        waveshaping oscillators.</p> <p class="applet_caption">The system could
        be used with or without a live input, since it made use of an external
        microphone to feed back its output to itself. The audio time domain signal
        used a transfer function to modify itself, but you could also alter Chebyshev
        coefficients in realtime, redraw the waveform, and control a number of
        other parameters.</p> <p class="applet_caption">Both of these sound excerpts
        feature the amazing contemporary flutist Anne LaBerge. In the first version,
        LaBerge is playing but is not recorded. She is in another room, and the
        output of the system is fed back into itself through a microphone. By
        playing, she could drastically affect the sound (since her flute went
        immediately into the transfer function). However, although she&#146;s
        causing the changes to occur, we don&#146;t actually hear her flute. In
        the second version, LaBerge is in front of the same microphone that&#146;s
        used for feedback and recording.</p>
      <p class="applet_caption">In both versions, Polansky was controlling the
        mix and the feedback gain as well as playing with the computer.</p>
					<p></p>
				</td>
  </tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_07.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
