<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_06.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="13" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 4: The Synthesis of Sound by Computer</h1>
					<h1>Section 4.5: <b>Amplitude Modulation</b></h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">

				</td>
				<td valign="top" bgcolor="white" width="580">
					<h2><br>
						Introduction to Modulation</h2>
					<p>We might be more familiar with the term <i>modulation</i> in relationship to radio. Radio transmission utilizes <i>amplitude modulation</i> (AM) and <i>frequency modulation</i> (FM), but we too can create complex waveforms by using these techniques. </p>
					<p>Modulated signals are those that are changed regularly in time, usually by other signals. They can get pretty complicated. For example, modulated signals can modulate other signals! To create a modulated signal, we begin with two or more oscillators (or anything that produces a signal) and combine the output signals of the oscillators in such a way as to modulate the amplitude, frequency, and/or phase of one of the oscillators.</p>
				<p>In the amplitude modulation equation, the DC offset (a signal that is essentially a straight line) is added to the signal <i>m</i>(<i>t</i>) and multiplied by a sinusoid with frequency <i>f<sub>c</sub></i> . <i>A<sub>c</sub></i> is the carrier amplitude, and <i>k<sub>a</sub></i> is the modulation index.</p>
				<p class="figure"><img height="51" width="467" src="../images/chapter4/am.equation.jpg"></p>


				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
<!-- <p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25D30149'));return CSClickReturn()" href="../applets/4_5_lfo_mod.php" target="_blank" csclick="BCE25D30149"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 4.8</b><br>
						LFO modulation</p> -->
					</td>
				<td valign="top" bgcolor="white" width="580">
					<p>Applet 4.9 shows what happens, in the case of frequency
        modulation, if the modulating signal is low frequency. In that case, we&#146;ll
        hear something like <i>vibrato</i> (a regular change in frequency, or
        perceived pitch). We can also modulate amplitude in this way (<i>tremolo</i>),
        or even formant frequencies if we want. Low-frequency modulations (that
        is, modulators that themselves are low-frequency signals) can produce
        interesting sonic effects.</p>
					<p>But for making really complex sounds, we are generally interested in high-frequency modulation. We take two audio frequency signals and multiply them together. More precisely, we start with a <i>carrier oscillator</i> and attach a <i>modulating</i> <i>oscillator</i> to modify and distort the signal that the carrier oscillator puts out. The output of the carrier oscillator can include its original signal and the <i>sidebands</i> or added spectra that are generated by the modulation process.</p>
					<h2>Amplitude Modulation</h2>

      <p>Figure 4.13 shows how we might construct a computer music instrument to do amplitude modulation. The two half-ovals are often called <i>unit
        generators,</i> and they refer to some software device like an oscillator,
        a mixer, a filter, or an envelope generator that has inputs and outputs
        and makes and transforms digital signals.</p>
					<p class="figure"><img height="553" width="468" src="../images/chapter4/am.jpg"></p>

      <p class="figure_caption_flush_bottom"><b>Figure 4.13 </b>Amplitude modulation,
        two operator case.<b><br>
        </b></p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="711">
					<hr>
				</td>
			</tr>
			<tr>
				<td class="icon_caption" align="center" valign="top" bgcolor="white" width="125"><a onclick="setSrc('../sounds/chapter4/lowpass.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
					<b>Soundfile 4.18</b><br>
					Low-pass moving filter (modulated by sine)</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">A low-pass moving filter that uses
        a sine wave to control a sweep between 0 Hz and 500 Hz.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/hipassmove.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 4.19</b><br>
						High-pass moving filter (modulated by sine)</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">A high-pass moving filter that uses
        a sine wave to control a sweep between 5,000 Hz and 15,000 Hz.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/lopasssaw.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a><br>
						<b>Soundfile 4.20</b><br>
						Low-pass moving filter (modulated by sawtooth)</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">A low-pass moving filter that uses a sawtooth wave to control a sweep between 0 Hz and 500 Hz.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/hipasssaw.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button34" border="0" alt=""></a><br>
						<b>Soundfile 4.21</b><br>
						High-pass moving filter (modulated by sawtooth)</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">A high-pass moving filter that uses a sawtooth wave to control a sweep between 5,000 Hz and 15,000 Hz.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="711">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure"><img height="357" width="545" src="../images/chapter4/tenney.jpg"></p>

      <p class="figure_caption_flush_bottom"><b>Figure 4.14&nbsp;&nbsp;</b>James
        Tenney&#146;s &quot;Phases,&quot; one of the earliest and still most interesting
        pieces of computer-assisted composition. The pictures above are his &quot;notes&quot;
        for the piece, which constitute a kind of score.<br>
						<br>
						Tenney made use of some simple modulation trajectories to control timbral parameters over time (like amplitude modulation rate, spectral upper limit, note-duration, and so on). By simply coding these functions in the computer and linking the output to the synthesis engine, Tenney was able to realize a number of highly original works in which he controlled the overall, large-scale process, but the micro-structure was largely determined by the computer making use of his curves.<br>
						<br>
						&quot;Phases&quot; was released on an Artifact CD of James Tenney&#146;s computer music.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_06.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
