<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_03.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td rowspan="22" valign="top" bgcolor="white" width="20">&nbsp;</td>
    <td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
    <td valign="top" bgcolor="white" width="580"> <h1>Chapter 4: The Synthesis of Sound by Computer</h1>
					<h1>Section 4.2: Additive Synthesis<br>
						<br>
					</h1>
				</td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption">&nbsp;</p></td>
    <td valign="top" bgcolor="white" width="580">
      <p><i>Additive synthesis</i> refers to a number of related synthesis techniques,
        all based on the idea that complex tones can be created by the <i>summation,</i>
        or addition, of simpler tones. As we saw in Chapter 3, it is theoretically
        possible to break up any complex sound into a number of simpler ones,
        usually in the form of sine waves. In additive synthesis, we use this
        theory in reverse.</p>
      <p class="figure"><img src="../images/chapter4/adding_sines_figure4.1.jpg" alt="" width="500" height="171" border="0"></p>
					<p class="figure_caption"><b>Figure 4.1</b>&nbsp;&nbsp;Two waves joined by a plus sign.</p>
					<p class="figure"><img height="348" width="381" src="../images/chapter4/organ.jpg"></p>
					<p class="figure_caption"><b>Figure 4.2</b>&nbsp;&nbsp;This organ has a
        great many pipes, and together they function exactly like an <i>additive
        synthesis algorithm.<br>
        <br>
        </i>Each pipe essentially produces a sine wave (or something like it),
        and by selecting different combinations of harmonically related pipes
        (as partials), we can create different combinations of sounds, called
        (on the organ) <i>stops.</i> This is how organs get all those different
        sounds: organists are experts on Fourier series and additive synthesis
        (though they may not know that!).</p>
      <p>The technique of mixing simple sounds together to get more complex sounds
        dates back a very long time. In the Middle Ages, huge pipe organs had
        a great many stops that could be &quot;pulled out&quot; to combine and
        recombine the sounds from several pipes. In this way, different &quot;patches&quot;
        could be created for the organ. More recently, the <i>telharmonium,</i>
        a giant electrical synthesizer from the early 1900s, added together the
        sounds from dozens of electro-mechanical tone generators to form complex
        tones. This wasn&#146;t very practical, but it has an important place
        in the history of electronic and computer music.<br>
        <br>
      </p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr>
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25A46118'));return CSClickReturn()" href="../applets/4_2_hear_mix.php" target="_blank" csclick="BCE25A46118"><img src="../images/global/applet.gif" width="80" height="65" name="5" border="0"></a><br>
						<b>Applet 4.2</b><br>
        <span class="icon_caption">Mixed sounds</span></p></td>
				<td valign="top" bgcolor="white" width="580"> <p class="applet_caption">This
        applet demonstrates how sounds are mixed together. </p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr>
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580"> <h2> The Computer and Additive
        Synthesis</h2>
      <p>While instruments like the pipe organ were quite effective for some sounds,
        they were limited by the need for a separate pipe or oscillator for each
        tone that is being added. Since complex sounds can require anywhere from
        a couple dozen to several thousand component tones, each needing its own
        pipe or oscillator, the physical size and complexity of a device capable
        of producing these sounds would quickly become prohibitive. Enter the
        computer!<br>
        <br>
      </p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr>
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125"><a onclick="setSrc('../sounds/chapter4/lemon_drops.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button27" border="0" alt=""></a><span class="icon_caption"><br>
						<b>Soundfile 4.1</b><br>
      Excerpt from Kenneth Gaburo&#146;s composition &quot;Lemon Drops&quot;</span></td>
				<td valign="top" bgcolor="white" width="580"> <p class="applet_caption">A
        short excerpt from Kenneth Gaburo&#146;s composition &quot;Lemon Drops,&quot;
        a classic of electronic music made in the early 1960s.</p>
      <p class="applet_caption">This piece and another extraordinary Gaburo work,
        &quot;For Harry,&quot; were made at the University of Illinois at Urbana-Champaign
        on an early electronic music instrument called the harmonic tone generator,
        which allowed the composer to set the frequencies and amplitudes of a
        number of sine wave oscillators to make their own timbres. It was extremely
        cumbersome to use, but it was essentially a giant Fourier synthesizer,
        and, theoretically, any periodic waveform was possible on it!</p>
      <p class="applet_caption">It&#146;s a tribute to Gaburo&#146;s genius and
        that of other early electronic music pioneers that they were able to produce
        such interesting music on such primitive instruments. Kind of makes it
        seem like we&#146;re almost cheating, with all our fancy software!</p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr>
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580"> <p><br>
        If there is one thing computers are good at, it&#146;s adding things together.
        By using <i>digital oscillators</i> instead of actual physical devices,
        a computer can add up any number of simple sounds to create extremely
        complex waveforms. Only the speed and power of the computer limit the
        number and complexity of the waveforms. Modern systems can easily generate
        and mix thousands of sine waves in real time. This makes additive synthesis
        a powerful and versatile performance and synthesis tool. Additive synthesis
        is not used so much anymore (there are a great many other, more efficient
        techniques for getting complex sounds), but it&#146;s definitely a good
        thing to know about.</p>
      <h2>A Simple Additive Synthesis Sound</h2>
      <table border="0" cellspacing="2" cellpadding="4" align="right">
        <tr>
          <td><img height="478" width="324" src="../images/chapter4/squ_3_comp.gif"></td>
        </tr>
        <tr>
          <td><img height="478" width="340" src="../images/chapter4/squ_5_comp.gif"></td>
        </tr>
        <tr>
          <td><img height="478" width="324" src="../images/chapter4/squ_13_comp.gif">
								<p class="figure_caption_flush"><b>Figure 4.3</b></p>
							</td>
        </tr>
      </table>
      <p>Let&#146;s design a simple sound with additive synthesis. A nice example
        is the generation of a <i>square wave.</i></p>
      <p>You can probably imagine what a square wave would look like. We start
        with just one sine wave, called the <i>fundamental.</i> Then we start
        adding <i>odd partials</i> to the fundamental, the amplitudes of which are inversely proportional to their partial number. That means that the third partial is 1/3 as strong as the first, the fifth partial is 1/5 as strong, and so on. (Remember that the fundamental is the first partial; we could also call it the first harmonic.) Figure 4.3 shows what we get after adding seven harmonics. Looks pretty square, doesn&rsquo;t it?</p>
      <p>Now, we should admit that there&#146;s an easier way to synthesize square
        waves: just flip from a high sample value to a low sample value every
        <i>n</i> samples. The lower the value of <i>n</i>, the higher the frequency
        of the square wave that&#146;s being generated. Although this technique
        is clearer and easier to understand, it has its problems too; directly
        generating waveforms in this way can cause unwanted frequency aliasing.</p>
      <p><img height="217" width="245" src="../images/chapter4/synclavier.jpg"></p>
      <p class="figure_caption_flush"><b>Figure 4.4 </b>The <i>Synclavier</i>
        was an early digital electronic music instrument that used a large oscillator
        bank for additive synthesis. You can see this on the front panel of the
        instrument&#151;many of the LEDs indicate specific partials! On the Synclavier
        (as was the case with a number of other analog and digital instruments),
        the user can tune the partials, make them louder, even put envelopes on
        each one.</p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr>
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25A66120'));return CSClickReturn()" href="../applets/4_2_wavemaker.php" target="_blank" csclick="BCE25A66120"><img src="../images/global/applet.gif" width="80" height="65" name="3" border="0"></a><br>
						<b>Applet 4.3</b><br>
        <span class="icon_caption">Additive synthesis</span> </p></td>
				<td valign="top" bgcolor="white" width="580"> <p class="applet_caption">This
        applet lets you add sine waves together at various amplitudes, to see
        how additive synthesis works.</p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr>
  <!-- <tr>
				<td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"><br>
						<b>Applet 4.4<br>
        </b><span class="icon_caption">Spectral envelopes</span><b><br>
							<font color="red">[Under Development<br>
									Available Oct. 2004]</font></b></p></td>
				<td valign="top" bgcolor="white" width="580"> <p class="applet_caption">This
        applet<b> </b>lets you add<b> </b><i>spectral envelopes</i> to a number
        of partials. This means that you can impose a different amplitude trajectory
        for each partial, independently making each louder and softer over time.</p>
      <p class="applet_caption">This is really more like the way things work in
        the real world: partial amplitudes evolve over time&#151;sometimes independently,
        sometimes in conjunction with other partials (in a phenomenon called <i>common
        fate</i>). This is called <i>spectral evolution,</i> and it&#146;s what 
        makes sounds live.</p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr> -->
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><br>
        <br>
        <br>
						<br>
						<a onclick="CSAction(new Array(/*CMP*/'BCE25A70122'));return CSClickReturn()" href="../popups/chapter4/xbit_4_4.php" target="_blank" csclick="BCE25A70122"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 4.4</b><br>
        Spectral formula of a waveform</p></td>
				<td valign="top" bgcolor="white" width="580"> <h2> A More Interesting Example</h2>
      <p>OK, now how about a more interesting example of additive synthesis? The
        quality of a synthesized sound can often be improved by varying its parameters
        (partial frequencies, amplitudes, and envelope) over time. In fact, time-variant
        parameters are essential for any kind of &quot;lifelike&quot; sound, since
        all naturally occurring sounds vary to some extent.</p>
      <table border="0" cellspacing="0" cellpadding="4">
        <tr>
          <td align="center" width="6">&nbsp;</td>
          <td align="center" width="120"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/sine_dog.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button22" border="0" alt=""></a><b><br>
              Soundfile 4.2<br>
              </b>Sine wave<br>
              speech</p></td>
          <td align="center" width="120"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/dog.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><b><br>
              Soundfile 4.3<br>
              </b>Regular<br>
              speech</p></td>
          <td width="22">&nbsp;</td>
          <td align="center" width="120"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/where.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><b><br>
              Soundfile 4.4<br>
              </b>Sine wave<br>
              speech</p></td>
          <td align="center" width="120"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/wherepcm.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a><b><br>
              Soundfile 4.5<br>
              </b>Regular<br>
              speech</p></td>
        </tr>
      </table>
      <p class="figure_caption_flush_bottom">These soundfiles are examples of
        sentences reconstructed with sine waves. Soundfile 4.2 is the sine wave
        version of the sentence spoken in Soundfile 4.3, and Soundfile 4.4 is
        the sine wave version of the sentence spoken in Soundfile 4.5.<br>
						<br>

        Sine wave speech is an experimental technique that tries to simulate speech
        with just a few sine waves, in a kind of primitive additive synthesis.
        The idea is to pick the sine waves (frequencies and amplitudes) carefully.
        It&#146;s an interesting notion, because sine waves are pretty easy to
        generate, so if we can get close to &quot;natural&quot; speech with just
        a few of them, it follows that we don&#146;t require <i>that</i> much
        information when we listen to speech.<br>
        <br>
        Sine wave speech has long been a popular idea for experimentation by psychologists
        and researchers. It teaches us a lot about speech&#151;what&#146;s important
        in it, both perceptually and acoustically.<br>
        <br>
        These files are used with the permission of Philip Rubin, Robert Remez,<br>
        and Haskins Laboratories.</p>
      <h2>Attacks, Decays, and Time Evolution in Sounds</h2>
      <p>As we&#146;ve said, additive synthesis is an important tool, and we can
        do a lot with it. It does, however, have its drawbacks. One serious problem
        is that while it&#146;s good for periodic sounds, it doesn&#146;t do as
        well with noisy or chaotic ones.</p>
      <p>For instance, creating the <i>steady-state</i> part (the sustain) of
        a flute note is simple with additive synthesis (just a couple of sine
        waves), but creating the <i>attack</i> portion of the note, where there
        is a lot of breath noise, is nearly impossible. For that, we have to synthesize
        a lot of different kinds of information: noise, attack transients, and
        so on.</p>
      <p>And there&#146;s a worse problem that we&#146;d love to sweep under the
        old psychoacoustical rug, too, but we can&#146;t: it&#146;s great that
        we know so much about steady-state, periodic, Fourier-analyzable sounds,
        but from a cognitive and perceptual point of view, we really couldn&#146;t
        care less about them! The ear and brain are much more interested in things
        like attacks, decays, and changes over time in a sound (<i>modulation</i>).
        That&#146;s bad news for all that additive synthesis software, which doesn&#146;t
        handle such things very well.</p>
      <p>That&#146;s not to say that if we play a triangle wave and a sawtooth
        wave, we couldn&#146;t tell them apart; we certainly could. But that really
        doesn&#146;t do us much good in most circumstances. If angry lions roared
        in square waves, and cute cuddly puppy dogs barked in triangle waves,
        maybe this would be useful, but we have evolved&#151;or learned to hear
        attacks, decays, and other transients as being more crucial. What we need
        to be able to synthesize are transients, spectral evolutions, and modulations.
        Additive synthesis is not really the best technique for those.</p>
      <p>Another problem is that additive synthesis is very computationally expensive.
        It&#146;s a lot of work to add all those sine waves together for each
        output sample of sound! Compared to some other synthesis methods, such
        as <i>frequency modulation</i> (FM) synthesis, additive synthesis needs
        lots of computing power to generate relatively simple sounds.</p>
      <p>But despite its drawbacks, additive synthesis is conceptually simple,
        and it corresponds very closely to what we know about how sounds are constructed
        mathematically. For this reason it&#146;s been historically important
        in computer sound synthesis.</p>
      <p class="figure"><img height="156" width="285" src="../images/chapter4/adsr.jpg"></p>
      <p class="figure_caption_flush_bottom"><b>Figure 4.5&nbsp;&nbsp;</b>A typical
        <i>ADSR</i> (attack, decay, sustain, release) steady-state modulation.
        This is a standard amplitude envelope shape used in sound synthesis.<br>
        <br>
        The ability to change a sound&#146;s amplitude envelope over time plays
        an important part in the perceived &quot;naturalness&quot; of the sound.</p>
      <h2>Shepard Tones</h2>
      <p>One cool use of additive synthesis is in the generation of a very interesting
        phenomenon called <i>Shepard tones.</i> Sometimes called &quot;endless
        glissandi,&quot; Shepard tones are created by specially configured sets
        of oscillators that add their tones together to create what we might call
        a constantly rising tone. Certainly the Shepard tone phenomenon is one
        of the more interesting topics in additive synthesis.</p>
      <p>In the 1960s, experimental psychologist Roger Shepard, along with composers
        James Tenney and Jean-Claude Risset, began working with a phenomenon that
        scientifically demonstrates an independent dimension in pitch perception
        called <i>chroma,</i> confirming the circularity of relative pitch judgments.</p>
      <p>What <i>circularity</i> means is that pitch is perceived in kind of a
        circular way: it keeps going up until it hits an octave, and then it sort
        of starts over again. You might say pitch <i>wraps around</i> (think of
        a piano, where the C notes are evenly spaced all the way up and down).
        By <i>chroma,</i> we mean an aspect of pitch perception in which we group
        together the same pitches that are related as frequencies by multiples
        of 2. These are an <i>octave</i> apart. In other words, 55 Hz is the same
        chroma as 110 Hz as 220 Hz as 440 Hz as 880 Hz. It&#146;s not exactly
        clear whether this is &quot;hard-wired&quot; or learned, or ultimately
        how important it is, but it&#146;s an extraordinary idea and an interesting
        aural illusion.</p>
      <p>We can construct such a circular series of pitches in a laboratory setting
        using synthesized <i>Shepard tones.</i> These complex tones are comprised
        of partials separated by octaves<i>.</i> They are complex tones where
        all the non-power-of-two numbered partials are omitted.</p>
      <p>These tones slide gradually from the bottom of the frequency range to the top. The amplitudes of the component frequencies follow a bell-shaped spectral envelope (see Figure 4.6) with a maximum near the middle of the standard musical range. In other words, they fade in and out as they get into the most common frequency range. This creates an interesting illusion: a circular Shepard tone scale can be created that varies only in tone chroma and collapses the second dimension of tone height by combining all octaves. In other words, what you hear is a continuous pitch change through one octave, but not bigger than one octave (that&rsquo;s a result of the special spectra and the amplitude curve). It&rsquo;s kind of like a barber pole: the pitches sound as if they just go around for a while, and then they&rsquo;re back to where they started (even though, actually, they&rsquo;re continuing to rise!).</p>
      <p class="figure"><img height="300" width="500" src="../images/chapter4/shep.tone.gif"></p>
      <p class="figure_caption_flush_bottom"><b>Figure 4.10</b>&nbsp;&nbsp;Bell-shaped
        spectral envelope for making Shepard tones.</p>
      <p>Shepard wrote a famous paper in 1964 in which he explains, to some extent,
        our notion of <i>octave equivalence</i> using this auditory illusion:
        a sequence of these Shepard tones that shifts only in chroma as it is
        played. The apparent fundamental frequency increases step by step, through
        repeated cycles. Listeners hear the pitch steps as climbing continuously
        upward, even though the pitches are actually moving only around the chroma
        circle. Absolute pitch height (that is, how &quot;high&quot; or &quot;low&quot;
        it sounds) is removed from our perception of the sequence.<br>
        <br>
      </p></td>
  </tr>
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/shepard.wav.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button34" border="0" alt=""></a><br>
						<b>Soundfile 4.6</b><br>
        <span class="icon_caption">Shepard tone</span></p></td>
				<td valign="top" bgcolor="white" width="580"> <p class="figure_flush_top"><img height="142" width="440" src="../images/chapter4/SHEP.gif"><br>
        <br>
      </p>
      <p class="figure_flush_top"><img height="80" width="523" src="../images/chapter4/klavier-animation.gif"></p>
      <p class="figure_caption_flush_bottom"><b>Figure 4.7 </b>Try clicking on
        Soundfile 4.6. After you listen to the soundfile once, click again to
        listen to the frequencies continue on their upward spiral.<br>
        <br>
        Used with permission from Susan R. Perry, M.A., Dept. of Psychology, University
        of Tennessee.</p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr>
  <tr>
				<td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/shep.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button35" border="0" alt=""></a><br>
						<b>Soundfile 4.7</b><br>
        <span class="icon_caption">Shepard tone</span></p></td>
				<td valign="top" bgcolor="white" width="580"> <p class="applet_caption">The
        Shepard tone contains a large amount of octave-related harmonics across
        the frequency spectrum, all of which rise (or fall) together. The harmonics
        toward the low and high ends of the spectrum are attenuated gradually,
        while those in the middle have maximum amplification. This creates a spiraling
        or barber pole effect. (Information from Doepfer Musikelektronik GmbH.)</p>
      <p class="applet_caption">Soundfile 4.7 is an example of the spiraling Shepard
        tone effect.</p></td>
  </tr>
  <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
			</tr>
			<tr>
				<td width="125" align="center" valign="top" bgcolor="white"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/forann.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button36" border="0" alt=""></a><br>
						<b>Soundfile 4.8</b><br><span class="icon_caption">
        &quot;For Ann&quot; (rising), by James Tenney.</p></td>
				<td valign="top" bgcolor="white" width="580"> <p class="applet_caption">James
        Tenney is an important computer music composer and pioneer who worked
        at Bell Laboratories with Roger Shepard in the early 1960s.</p>
      <p class="applet_caption">This piece was composed in 1969. This composition
        is based on a set of continuously rising tones, similar to the effect
        created by Shepard tones. The compositional process is simple: each glissando,
        separated by some fixed time interval, fades in from its lowest note and
        fades out as it nears the top of its audible range. It is nearly impossible
        to follow, aurally, the path of any given glissando, so the effect is
        that the individual tones never reach their highest pitch.<br>
        <br>
      </p></td>
  </tr>
			<tr>
				<td valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_03.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
