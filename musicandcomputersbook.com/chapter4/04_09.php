<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="../chapter5/05_01.php">Next Chapter --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="6" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 4: The Synthesis of Sound by Computer</h1>
					<h1>Section 4.9: Physical Modeling<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>

    <td valign="top" bgcolor="white" width="580"><p>We&#146;ve already covered
        a bit of material on physical modeling without even telling you&#151;the
        ideas behind formant synthesis are directly derived from our knowledge
        of the physical construction and behavior of certain instruments. Like
        all of the synthesis methods we&#146;ve covered, physical modeling is
        not one specific technique, but rather a variety of related techniques.
        Behind them all, however, is the basic idea that by understanding how
        sound/vibration/air/string behaves in some physical system (like an instrument),
        we can model that system in a computer and thus synthetically generate
        realistic sounds. </p>
      <h2>Karplus-Strong Algorithm</h2>

      <p>Let&#146;s take a look at a really simple but very effective physical
        model of a plucked string, called the <i>Karplus-Strong algorithm</i>
        (so named for its principal inventors, Kevin Karplus and Alex Strong).
        One of the first musically useful physical models (dating from the early
        1980s), the Karplus-Strong algorithm has proven quite effective at generating
        a variety of plucked-string sounds (acoustic and electric guitars, banjos,
        and kotos) and even drumlike timbres.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<!-- <td colspan="2" align="center" valign="top" bgcolor="white" width="756">
					<hr>
				</td> -->
			</tr>
			<tr>
				<!-- <td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25F0D168'));return CSClickReturn()" href="../applets/4_9_tj_pluck.php" target="_blank" csclick="BCE25F0D168"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 4.13</b><br>
						Karplus-Strong plucked string algorithm<br>
						</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Fun with the Karplus-Strong plucked string algorithm.</p>
				</td> -->
			</tr>
			<tr>
				<!-- <td colspan="2" align="center" valign="top" bgcolor="white" width="756">
					<hr>
				</td> -->
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<p><br>
						Here&#146;s a simplified view of what happens when we pluck a string: at first the string is highly energized and it vibrates like mad, creating a fairly <i>complex</i> (meaning rich in harmonics) sound wave whose fundamental frequency is determined by the mass and tension of the string. Gradually, thanks to friction between the air and the string, the string&#146;s energy is depleted and the wave becomes less complex, resulting in a &quot;purer&quot; tone with fewer harmonics. After some amount of time all of the energy from the pluck is gone, and the string stops vibrating. </p>
					<p>If you have access to a stringed instrument, particularly one with some very low notes, give one of the strings a good pluck and see if you can see and hear what&#146;s happening per the description above.</p>
					<h2>How a Computer Models a Plucked String with the Karplus-Strong Algorithm</h2>
					<p>Now that we have a physical idea of what&#146;s happening in a plucked string, how can we model it with a computer? The Karplus-Strong algorithm does it like this: first we start with a buffer full of random values&#151;noise. (A buffer is just some computer memory (RAM) where we can store a bunch of numbers.) The numbers in this buffer represent the initial energy that is transferred to the string by the pluck. The Karplus-Strong algorithm looks like this:</p>

 					<p class="figure"><img height="56" width="217" src="../images/chapter4/kp.algo.jpeg"></p>

					<p>To generate a waveform, we start reading through the buffer and using the values in it as sample values. If we were to just keep reading through the buffer over and over again, what we&#146;d get would be a complex, pitched waveform. It would be complex because we started out with noise, but pitched because we would be repeating the same set of random numbers. (Remember that any time we repeat a set of values, we end up with a pitched (periodic) sound. The pitch we get is directly related to the size of the buffer (the number of numbers it contains) we&#146;re using, since each time through the buffer represents one complete cycle (or period) of the signal.)</p>

      <p>Now here&#146;s the trick to the Karplus-Strong algorithm: each time
        we read a value from the buffer, we average it with the last value we
        read. It is this averaged value that we use as our output sample. We then
        take that averaged sample and feed it back into the buffer. That way,
        over time, the buffer gets more and more averaged (this is a simple filter,
        like the averaging filter described in Section 3.1). Let&#146;s look at
        the effect of these two actions separately.</p>


      <h2>Averaging and Feedback</h2>
      <p>First, what happens when we <i>average</i> two values? Averaging acts
        as a low-pass filter on the signal. Because we&#146;re averaging the signal,
        the signal changes less with each sample, and by limiting how quickly
        it can change we&#146;re limiting the number of high frequencies it can
        contain (since high frequencies have a high rate of change). So, averaging
        a signal effectively gets rid of high frequencies, which according to
        our string description we need to do&#151;once the string is plucked,
        it should start losing harmonics over time.</p>
      <p>The &quot;over time&quot; part is where feeding the averaged samples
        back into the buffer comes in. If we were to just keep averaging the values
        from the buffer but never actually changing them (that is, sticking the
        average back into the buffer), then we would still be stuck with a static
        waveform. We would keep averaging the same set of random numbers, so we
        would keep getting the same results.</p>
      <p>Instead, each time we generate a new sample, we stick it back into the
        buffer. That way our waveform evolves as we move through it. The effect
        of this low-pass filtering accumulates over time, so that as the string
        &quot;rings,&quot; more and more of the high frequencies are filtered
        out of it. The filtered waveform is then fed back into the buffer, where
        it is filtered again the next time through, and so on. After enough times
        through the process, the signal has been averaged so many times that it
        reaches equilibrium&#151;the waveform is a flat line the string has died
        out.</p>
      <p class="figure_flush"><img height="595" width="580" src="../images/chapter4/KSalgy.jpg"></p>
      <p class="figure_caption"><b>Figure 4.22</b>&nbsp;&nbsp;Applying the Karplus-Strong
        algorithm to a random waveform. After 60 passes through the filter/feedback
        cycle, all that&#146;s left of the wild random noise is a gently curving
        wave.<br>
        <br>
        The result is much like what we described in a plucked string: an initially
        complex, periodic waveform that gradually becomes less complex over time
        and ultimately fades away.</p>
      <p class="figure"><img height="103" width="450" src="../images/chapter4/kp.jpeg"></p>
      <p class="figure_caption"><b>Figure 4.23</b>&nbsp;&nbsp;Schematic view of
        a computer software implementation of the basic Karplus-Strong algorithm.<br>
        <br>
        For each note, the switch is flipped and the computer memory buffer is
        filled with random values (noise). To generate a sample, values are read
        from the buffer and averaged. The newly calculated sample is both sent
        to the output stream and fed back into the buffer. When the end of the
        buffer is reached, we simply wrap around and continue reading at the beginning.
        This sort of setup is often called a <i>circular buffer.</i> After many
        iterations of this process, the buffer&#146;s contents will have been
        transformed from noise into a simple waveform.<br>
        <br>
        If you think of the random noise as a lot of energy and the averaging
        of the buffer as a way of lessening that energy, this digital explanation
        is not all that dissimilar from what happens in the real, physical case.<br>
        <br>
        Thanks to Matti Karjalainen for this graphic.</p>
      <p>Physical models generally offer clear, &quot;real world&quot; controls
        that can be used to play an instrument in different ways, and the Karplus-Strong
        algorithm is no exception: we can relate the buffer size to pitch, the
        initial random numbers in the buffer to the energy given to the string
        by plucking it, and the low-pass buffer feedback technique to the effect
        of air friction on the vibrating string.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/star.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 4.30</b><br>
						Super guitar
					</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Many researchers and composers have worked on the plucked string sound as a kind of basic mode of physical modeling.</p>

      <p class="applet_caption">One researcher, engineer Charlie Sullivan (who
        we're proud to say is one of our Dartmouth colleagues!) built a &quot;super&quot;
        guitar in software. Here&#146;s the heavy metal version of &quot;The Star
        Spangled Banner.&quot;</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<h2>
						Understand the Building Blocks of Sound</h2>

      <p>Physical modeling has become one of the most powerful and important current
        techniques in computer music sound synthesis. One of its most attractive
        features is that it uses a very small number of easy-to-understand building
        blocks&#151;delays, filters, feedback loops, and commonsense notions of
        how instruments work&#151;to model sounds. By offering the user just a
        few intuitive knobs (with names like &quot;brightness,&quot; &quot;breathiness,&quot;
        &quot;pick hardness,&quot; and so on), we can use existing sound-producing
        mechanisms to create new, often fantastic, virtual instruments.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<a onclick="setSrc('../sounds/chapter4/vocaliz.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 4.31</b><br>
						An example of<br>
						Perry Cook&#146;s SPASM</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="figure"><img height="360" width="153" src="../images/chapter4/perrycook.jpg"></p>
					<p class="figure_caption"><b>Figure 4.24</b>&nbsp;&nbsp;Part of the interface from Perry R. Cook&#146;s SPASM singing voice software. Users of SPASM can make Sheila, a computerized singer, sing. Perry Cook has been one of the primary investigators of musically useful physical models. He&#146;s released lots of great physical modeling software and source code.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="../chapter5/05_01.php">Next Chapter --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
