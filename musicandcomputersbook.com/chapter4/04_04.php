<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_05.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td rowspan="18" valign="top" bgcolor="white" width="20">&nbsp;</td>
    <td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
    <td valign="top" bgcolor="white" width="580"> <h1>Chapter 4: The Synthesis of Sound by Computer</h1>
					<h1>Section 4.4: <b>Formant
        Synthesis</b><br>
						<br>
					</h1>
				</td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
    <td valign="top" bgcolor="white" width="580"><p><i>Formant synthesis</i> is
        a special but important case of subtractive synthesis. Part of what makes
        the timbre of a voice or instrument consistent over a wide range of frequencies
        is the presence of fixed frequency peaks, called <i>formants</i>. </p>
      <p>These peaks stay in the same frequency range, independent of the actual
        (fundamental) pitch being produced by the voice or instrument. While there
        are many other factors that go into synthesizing a realistic timbre, the
        use of formants is one way to get reasonably accurate results.</p>
      <div align="right">
        <p class="figure_flush"><img height="345" width="575" src="../images/chapter4/trumpet.formants.jpg"></p>
      </div> <p class="figure_caption_flush_bottom"><b>Figure 4.10&nbsp;&nbsp;</b>A
        trumpet plays two different notes, a perfect fourth apart, but the formants
        (fixed resonances) stay in the same places.<br>
        <br>
      </p></td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125">
			<!-- <p class="icon_caption"><br>
        <br>
        <br>
						<br>
						<a onclick="CSAction(new Array(/*CMP*/'BCE25C72139'));return CSClickReturn()" href="../applets/4_4_voice_formants.php" target="_blank" csclick="BCE25C72139"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 4.7</b><br>
        Formants<br></p> -->
			</td>
    <td valign="top" bgcolor="white" width="580"> <h2>Resonant Structure</h2>
      <p>The location of formants is based on the <i>resonant physical structure</i>
        of the sound-producing medium. For example, the body of a certain violin
        exhibits a particular set of formants, depending upon how it is constructed.
        Since most violins share a similar shape and internal construction, they
        share a similar set of formants and thus sound alike. In the human voice,
        the vocal tract and nasal cavity act as the resonating body. By manipulating
        the shape and size of that resonant space (i.e., by changing the shape
        of the mouth and throat), we change the location of the formants in our
        voice. We recognize different vowel sounds mainly by their formant placement.
        Knowing that, we can generate some fairly convincing synthetic vowels
        by manipulating formants in a synthesized set of tones. A number of books
        list actual formant frequency values for various voices and vowels (including
        Charles Dodge&#146;s highly recommended standard text, <i>Computer Music</i>&#151;Dodge
        is a great pioneer in computer music voice synthesis).<br>
        <br>
      </p></td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><br>
        <br>
        <br>
						<br>
						<a onclick="CSAction(new Array(/*CMP*/'BCE25C78140'));return CSClickReturn()" href="../popups/chapter4/xbit_4_5.php" target="_blank" csclick="BCE25C78140"><img src="../images/global/popup.gif" width="80" height="65" name="button6" border="0"></a><br>
						<b>Xtra bit 4.5</b><br>
        Change of resonance</p></td>
    <td valign="top" bgcolor="white" width="580"> <h2>Composing with Synthetic
        Speech</h2>
      <p>Generating really good and convincing synthetic speech and singing voices
        is more complex than simply moving around a set of formants&#151;we haven&#146;t
        mentioned anything about generating consonants, for example. And no speech
        synthesis system relies purely on formant synthesis. But, as these examples
        illustrate, even very basic formant manipulation can generate sounds that
        are undoubtedly &quot;vocal&quot; in nature.</p>
      <div align="right">
        <p class="figure_flush"><img height="423" width="602" src="../images/chapter4/bow_boy_bold_board.gif"></p>
      </div>
      <p class="figure_caption_flush_bottom"><b>Figure 4.11</b>&nbsp;&nbsp;A spectral
        picture of the voice, showing formants. Graphic courtesy of the alt.usage.english
        newsgroup.</p></td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><br>
        <br>
						<br>
						<a onclick="CSAction(new Array(/*CMP*/'BCE25C80141'));return CSClickReturn()" href="../popups/chapter4/xbit_4_6.php" target="_blank" csclick="BCE25C80141"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 4.6</b><br>
        Formant manipulations</p></td>
    <td valign="top" bgcolor="white" width="580"> <p class="figure"><img height="313" width="260" src="../images/chapter4/paulLansky.gif"></p>
      <p class="figure_caption_flush_bottom"><b>Figure 4.12</b>&nbsp;&nbsp;Composer
        Paul Lansky.<br>
        <br>
      </p></td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="top" bgcolor="white" width="731"> <hr>
    </td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/njmic.ex.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 4.12</b><br>
						&quot;Notjustmoreidlechatter&quot; of Paul Lansky<br>
					</p>
				</td>
    <td valign="top" bgcolor="white" width="580"> <p class="applet_caption">&quot;Notjustmoreidlechatter&quot;
        was made on a DEC MicroVaxII computer in 1988. All the &quot;chatter&quot;
        pieces (there are three in the set) use techniques known as <i>linear
        predictive coding</i>, <i>granular synthesis</i>, and a variety of stochastic
        mixing techniques.</p>
      <p class="applet_caption">Paul Lansky is a well-known composer and researcher
        of computer music who teaches at Princeton University. He has been a leading
        pioneer in software design, voice synthesis, and compositional techniques.</p>
      <p class="applet_caption">Used with permission from Paul Lansky.</p></td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="top" bgcolor="white" width="731"> <hr>
    </td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/ichatjr.ex.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 4.13</b><br>
        &quot;idlechatterjunior&quot; of Paul Lansky from 1999<br>
      </p></td>
    <td valign="top" bgcolor="white" width="580"> <p class="applet_caption">Paul
        Lansky writes:</p> <p class="applet_caption">&quot;Over ten years ago I wrote three 'chatter' pieces, and then decided to quit while I was ahead. The urge to strike again recently overtook me, however, and after my lawyer assured me that the statute of limitations had run out on this particular offense, I once again leapt into the fray. My hope is that the seasoning provided by my labors in the intervening years results in something new and different. If not, then look out for 'Idle Chatter III'... .&quot;</p>
      <p class="applet_caption">Used with permission from Paul Lansky.</p></td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="top" bgcolor="white" width="731"> <hr>
    </td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/myers.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a><br>
						<b>Soundfile 4.14</b><br>
        Composition by composer Sarah Myers entitled &quot;Trajectory of Her Voice&quot;
        <br>
      </p></td>
    <td valign="top" bgcolor="white" width="580"> <p class="applet_caption">Composer
        Sarah Myers used an interview with her friend Gili Rei as the source material
        for her composition. &quot;Trajectory of Her Voice&quot; is a ten-part
        canon that explores the musical qualities of speech. As the verbal content
        becomes progressively less comprehensible as language, the focus turns
        instead to the sonorities inherent in her voice.</p>
      <p class="applet_caption">This piece was composed using the Cmix computer
        music language in 1998 (Cmix was written by Paul Lansky).</p></td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="top" bgcolor="white" width="731"> <hr>
    </td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/FredVoice.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button34" border="0" alt=""></a><br>
						<b>Soundfile 4.15</b><br>
        Synthetic speech example, &quot;Fred&quot; voice from the Macintosh computer
        <br>
      </p></td>
    <td valign="top" bgcolor="white" width="580"> <p class="applet_caption">Over
        the years, computer voice simulations have become better and better. They
        still sound a bit robotic, but advances in voice synthesis and acoustic
        technology make voices more and more realistic. Bell Telephone Laboratories
        has been one of the leading research facilities for this work, which is
        expected to become extremely important in the near future.</p></td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="top" bgcolor="white" width="731"> <hr>
    </td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/scholz.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button35" border="0" alt=""></a><br>
						<b>Soundfile 4.16</b><br>
        Carter Sholz&#146;s 1-minute piece &quot;Mannagram&quot; <br>
      </p></td>
    <td valign="top" bgcolor="white" width="580"> <p class="applet_caption">In
        this piece, based on a reading by Australian sound-poet Chris Mann, the
        composer tries to separate vowels and consonants, moving them each to
        a different speaker. This was inspired by an idea of Mann's, who always
        wanted to do a &quot;headphone piece&quot; in which he spoke and the consonants
        appeared in one ear, the vowels in another.</p></td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="top" bgcolor="white" width="731"> <hr>
    </td>
  </tr>
			<tr>
    <td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/turkey.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button36" border="0" alt=""></a><br>
						<b>Soundfile 4.17</b><br>
        The trump <br>
      </p></td>
    <td width="580"  valign="top" bgcolor="white"> <p class="applet_caption">One
        of the most interesting examples of formant usage is in playing the <i>trump,</i>
        sometimes called the jaw-harp. Here, a metal tine is plucked and the shape
        of the vocal cavity is used to create different pitches.</p></td>
  </tr>
			<tr>
				<td bgcolor="white">&nbsp;</td>
				<td bgcolor="white">
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_05.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
