<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_04.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="10" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 4: The Synthesis of Sound by Computer</h1>
					<h1>Section 4.3: <b>Filters</b><br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">

				</td>
				<td valign="top" bgcolor="white" width="580"><p>The most common
        way to think about filters is as functions that take in a signal and give
        back some sort of transformed signal. Usually, what comes out is &quot;less&quot;
        than what goes in. That&#146;s why the use of filters is sometimes referred
        to as <i>subtractive synthesis.</i> </p>
      <p>It probably won&#146;t surprise you to learn that <i>subtractive synthesis</i> is in many ways the opposite of additive synthesis. In additive synthesis, we start with simple sounds and add them together to form more complex ones. In subtractive synthesis, we start with a complex sound (like noise) and subtract, or filter out, parts of it. Subtractive synthesis can be thought of as sound sculpting&#151;you start out with a thick chunk of sound containing many possibilities (frequencies), and then you carve out (filter) parts of it. Filters are one of the sound sculptor&#146;s most versatile and valued tools.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/telephone.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 4.9</b><br>
						<span class="icon_caption">Telephone simulations</span>
						</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Older telephones had around an 8k
        low-pass filter imposed on their audio signal, mostly for noise reduction
        and to keep the equipment a bit cheaper.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/hipass.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 4.10</b><br>
						<span class="icon_caption">High-pass filtered noise</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">White noise (every frequency below
        the Nyquist rate at equal level) is filtered so we hear only frequencies
        above 5 kHz.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/lowpass.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a><br>
						<b>Soundfile 4.11</b><br>
						<span class="icon_caption">Low-pass filtered noise</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Here we hear only frequencies up to 500 Hz.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<h2> Four Basic Types of Filters</h2>
					<p class="figure"><img height="446" width="450" src="../images/chapter4/filtertypes.jpg"></p>
					<p class="figure_caption"><b>Figure 4.8</b>&nbsp;&nbsp;Four common filter types (clockwise from upper left): low-pass, high-pass, band-reject, band-pass.
							<br>

					<p>Figure 4.8 illustrates four basic types of filters: low-pass, high-pass, band-pass, and band-reject. Low-pass and high-pass filters should already be familiar to you&#151;they are exactly like the &quot;tone&quot; knobs on a car stereo or boombox. A low-pass (also known as high-stop) filter stops, or attenuates, high frequencies while letting through low ones, while a high-pass (low-stop) filter does just the opposite.<br>
						<br>
					</p>
				</td>
			</tr>
			<!-- <tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="721">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25C8C142'));return CSClickReturn()" href="../applets/4_3_filterfun.php" target="_blank" csclick="BCE25C8C142"><img src="../images/global/applet.gif" width="80" height="65" name="5" border="0"></a><br>
						<b>Applet 4.5</b><br>
						Using filters</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This applet is a good example of how filters, combined with something like noise, can produce some common and useful musical effects with very few operations.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="721">
					<hr>
				</td> -->
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h3>
						Band-Pass and Band-Reject Filters</h3>
					<p>Band-pass and band-reject filters are basically combinations of low-pass and high-pass filters. A <i>band-pass filter</i> lets through only frequencies above a certain point and below another, so there is a <i>band</i> of frequencies that get through. A <i>band-reject filter</i> is the opposite: it stops a band of frequencies. Band-reject filters are sometimes called <i>notch filters</i>, because they can notch out a particular part of a sound.<br>
						<br>
					</p>
				</td>
			</tr>
			<!-- <tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="721">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BD48D60A1'));return CSClickReturn()" href="../applets/4_3_comb_filter_sample.php" target="_blank" csclick="BD48D60A1"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 4.6</b><br>
						 Comb filters</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Comb filters are a very specific type of digital process in which a short delay (where some number of samples are actually delayed in time) and simple feedback algorithm (where outputs are sent back to be reprocessed and recombined) are used to create a rather extraordinary effect. Sounds can be &quot;tuned&quot; to specific harmonics (based on the length of the delay and the sample rate).</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="721">
					<hr>
				</td>
			</tr> -->
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h3>
						Low-Pass and High-Pass Filters</h3>
					<p>Low-pass and high-pass filters have a value associated with them called the <i>cutoff frequency,</i> which is the frequency where they begin &quot;doing their thing.&quot; So far we have been talking about <i>ideal,</i> or perfect, filters, which cut off instantly at their cutoff frequency. However, real filters are not perfect, and they can&rsquo;t just stop all frequencies at a certain point. Instead, frequencies die out according to a sort of curve around the corner of their cutoff frequency. Thus, the filters in Figure 4.8 don&rsquo;t have right angles at the cutoff frequencies&#151;instead they show general, more or less realistic response curves for low-pass and high-pass filters.</p>
					<h2>Cutoff Frequency</h2>
					<p>The cutoff frequency of a filter is defined as the point at which the signal is attenuated to 0.707 of its maximum value (which is 1.0). No, the number 0.707 was not just picked out of a hat! It turns out that the power of a signal is determined by squaring the amplitude: 0.7072 = 0.5. So when the amplitude of a signal is at 0.707 of its maximum value, it is at half-power. The cutoff frequency of a filter is sometimes called its <i>half-power point.</i></p>
					<h2>Transition Band</h2>

      <p>The area between where a filter &quot;turns the corner&quot; and where
        it &quot;hits the bottom&quot; is called the <i>transition band.</i> The
        steepness of the slope in the transition band is important in defining
        the sound of a particular filter. If the slope is very steep, the filter
        is said to be &quot;sharp&quot;; conversely, if the slope is more gradual,
        the filter is &quot;soft&quot; or &quot;gentle.&quot;</p>

      <p>Things really get interesting when you start combining low-pass and high-pass
        filters to form band-pass and band-reject filters. Band-pass and band-reject
        filters also have transition bands and slopes, but they have two of them:
        one on each side. The area in the middle, where frequencies are either
        passed or stopped, is called the <i>passband</i> or the <i>stopband.</i>
        The frequency in the middle of the band is called the <i>center frequency,</i>
        and the width of the band is called the filter&#146;s <i>bandwidth.</i></p>
					<p>You can plainly see that filters can get pretty complicated, even these
        simple ones. By varying all these parameters (cutoff frequencies, slopes,
        bandwidths, etc.), we can create an enormous variety of subtractive synthetic
        timbres.</p>
					<h2>A Little More Technical: IIR and FIR Filters</h2>
					<p>Filters are often talked about as being one of two types: finite impulse
        response (FIR) and infinite impulse response (IIR). This sounds complicated
        (and can be!), so we&#146;ll just try to give a simple explanation as
        to the general idea of these kinds of filters.</p>
					<p><i>Finite impulse response</i> filters are those in which delays are used along with some sort of averaging. <i>Delays</i> mean that the sound that comes out at a given time uses some of the previous samples. They&#146;ve been delayed before they get used.</p>

      <p>We&#146;ve talked about these filters in earlier chapters. What goes
        into an FIR is always less than what comes out (in terms of amplitude).
        Sounds reasonable, right? FIRs tend to be simpler, easier to use, and
        easier to design than IIRs, and they are very handy for a lot of simple
        situations. An averaging low-pass filter, in which some number of samples
        are averaged and output, is a good example of an FIR.</p>
					<p><i>Infinite impulse response</i> filters are a little more complicated, because they have an added feature: feedback. You&#146;ve all probably seen how a microphone and speaker can have feedback: by placing the microphone in front of a speaker, you amplify what comes out and then stick it back into the system, which is amplifying what comes in, creating a sort of infinite amplification loop. Ouch! (If you&#146;re Jimi Hendrix, you can control this and make great music out of it.)</p>

      <p>Well, IIRs are similar. Because the feedback path of these filters consists
        of some number of delays and averages, they are not always what are called
        <i>unity gain</i> transforms. They can actually output a higher signal
        than that which is fed to them. But at the same time, they can be many
        times more complex and subtler than FIRs. Again, think of electric guitar
        feedback&#151;IIRs are harder to control but are also very interesting.</p>
      <p class="figure"><img height="288" width="504" src="../images/chapter4/fir.gif"></p>
					<p class="figure"><img height="342" width="504" src="../images/chapter4/iir.gif"></p>

      <p class="figure_caption"><b>Figure 4.9</b>&nbsp;&nbsp;<i>FIR</i> and <i>IIR
        </i>filters.<br>
							<br>
        Filters are usually designed in the time domain, by delaying a signal
        and then averaging (in a wide variety of ways) the delayed signal and
        the nondelayed one. These are called <i>finite impulse response</i> (FIR)
        filters, because what comes out uses a finite number of samples, and a
        sample only has a finite effect.<br>
						<br>
        If we delay, average, and then feed the output of that process back into
        the signal, we create what are called <i>infinite impulse response</i>
        (IIR) filters. The feedback process actually allows the output to be much
        greater than the input. These filters can, as we like to say, &quot;blow
        up.&quot;<br>
						<br>
        These diagrams are technical lingo for typical filter diagrams for FIR
        and IIR filters. Note how in the IIR diagram the output of the filter&#146;s
        delay is summed back into the input, causing the infinite response characteristic.
        That&#146;s the main difference between the two filters.<br>
						<br>
						Thanks to Fernando Pablo Lopez-Lezcano for these graphics.</p>
					<p>Designing filters is a difficult but key activity in the field of <i>digital signal processing,</i> a rich area of study that is well beyond the range of this book. It is interesting to point out that, surprisingly, even though filters change the frequency content of a signal, a lot of the mathematical work done in filter design is done in the time domain, not in the frequency domain. By using things like sample averaging, delays, and feedback, one can create an extraordinarily rich variety of digital filters. </p>

      <p>For example, the following is a simple equation for a low-pass filter.
        This equation just averages the last two samples of a signal (where <i>x(n</i>)
        is the current sample) to produce a new sample. This equation is said
        to have a <i>one-sample delay.</i> You can see easily that quickly changing
        (that is, high-frequency) time domain values will be &quot;smoothed&quot;
        (removed) by this equation.</p>

      <p><i>x</i>(<i>n</i>) = (<i>x</i>(<i>n</i>) + <i>x</i>(<i>n </i>- 1))/2</p>

      <p>In fact, although it may look simple, this kind of filter design can
        be quite difficult (although extremely important). How do you know which
        frequencies you&#146;re removing? It&#146;s not intuitive, unless you&#146;re
        well schooled in digital signal processing and filter theory, have some
        background in mathematics, and know how to move from the time domain (what
        you have) to the frequency domain (what you want) by averaging, delaying,
        and so on.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_04.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
