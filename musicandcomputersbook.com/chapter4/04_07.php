<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>


		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_08.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="2" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 4: The Synthesis of Sound by Computer</h1>
					<h1>Section 4.7: FM Synthesis<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<!-- <p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25E33159'));return CSClickReturn()" href="../applets/4_7_fm_edit.php" target="_blank" csclick="BCE25E33159"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 4.11</b><br>

					Frequency modulation</p> -->
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>One goal of synthesis design is to find <i>efficient</i>
        algorithms, which don&#146;t take a lot of computation to generate rich
        sound palettes. Frequency modulation synthesis (FM) has traditionally
        been one of the most popular techniques in this regard, and it provides
        a good way to communicate some basic concepts about sound synthesis. You&#146;ve
        probably heard of frequency modulation&#151;it is the technique used in
        FM radio broadcasting. We took a look at amplitude modulation (AM) in
        Section 4.5.</p>
					<h2>History of FM Synthesis</h2>

      <p>FM techniques have been around since the early 20th century, and by the
        1930s FM theory for radio broadcasting was welldocumented and understood.
        It was not until the 1970s, though, that a certain type of FM was thoroughly
        researched as a musical synthesis tool. In the early 1970s, John Chowning,
        a composer and researcher at Stanford University, developed some important
        new techniques for music synthesis using FM.</p>

      <p>Chowning&#146;s research paid off. In the early 1980s, the Yamaha Corporation
        introduced their extremely popular DX line of FM synthesizers, based on
        Chowning&#146;s work. The DX-7 keyboard synthesizer was the top of their
        line, and it quickly became <i>the</i> digital synthesizer for the 1980s,
        making its mark on both computer music and synthesizer-based pop and rock.
        It&#146;s the most popular synthesizer in history. </p>

      <p>FM turned out to be good for creating a wide variety of sounds, although
        it is not as flexible as some other types of synthesis. Why has FM been
        so useful? Well, it&#146;s simple, easy to understand, and allows users
        to tweak just a few &quot;knobs&quot; to get a wide range of sonic variation.
        Let&#146;s take a look at how FM works and listen to some examples.</p>
					<h2 class="figure"><img height="169" width="561" src="../images/chapter4/dx7.jpg"></h2>

      <p class="figure_caption"><b>Figure 4.16&nbsp;&nbsp;</b>The Yamaha DX-7
        synthesizer was an extraordinarily popular instrument in the 1980s and
        was partly responsible for making electronic and computer music a major
        industry.
        <br><br>Thanks to Joseph Rivers and The Audio Playground Synthesizer Museum for this photo.</p>

					<h2>Simple FM</h2>
					<p>In its simplest form, FM involves two sine waves. One is called the <i>modulating wave</i>, the other the <i>carrier wave</i>. The modulating wave changes the frequency of the carrier wave. It can be easiest to visualize, understand, and hear when the modulator is low frequency.</p>
					<p class="figure"><img height="625" width="429" src="../images/chapter4/fm.jpeg" border="1"></p>

      <p class="figure_caption_flush_bottom"><b>Figure 4.17&nbsp;</b>&nbsp;Frequency
        modulation, two operator case.<b><br>
        <br>
        </b></p>

      <h2> Vibrato</h2>
					</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="783">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/vib.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 4.23<br>
        </b>Vibrato sound </p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption"> Carrier: 500 Hz; modulator frequency:
        1 Hz; modulator index: 100.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="783">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"></p>
				</td>

    <td valign="top" bgcolor="white" width="580"> <p>FM can create vibrato when
        the modulating frequency is less than 30 Hz. Okay, so it&#146;s still
        not that exciting&#151;that&#146;s just because everything is moving <I>slowly.</I>
        We&#146;ve created a very slow, weird vibrato! That&#146;s because we
        were doing low-frequency modulation. In Soundfile 4.23, the frequency
        (<I>f<sub>c</sub></I>) of the carrier wave is 500 Hz and the modulating
        frequency (<I>f<sub>m</sub></I>) is 1 Hz. 1 Hz means one complete cycle
        each second, so you should hear the frequency of the carrier rise, fall,
        and return to its original pitch once each second.</p>
      <p><img height="94" width="409" src="../images/chapter4/fm.eq.jpg"></p>
      <p><I>f<sub>c</sub></I> = carrier frequency, <I>m</I>(<I>t</I>) = modulating
        signal, and <I>A<sub>c</sub></I>= carrier amplitude.</p>
      <p>Note that the frequency of the modulating wave is the <i>rate of change</i>
        in the carrier&#146;s frequency. Although you can&#146;t tell from the
        above equation, it also turns out that the amplitude of the modulator
        is the degree of change of the carrier&#146;s frequency, and the waveform
        of the modulator is the shape of change of the carrier&#146;s frequency.</p>

      <p>In Figure 4.17 showing the unit generator diagram for frequency modulation (remember, we showed you one of these in Section 4.5), note that each of the sine wave oscillators has two inputs: one for frequency and one for amplitude. For our modulating oscillator we are using 1 Hz as the frequency, which becomes <i>f<sub>m</sub></i> to the carrier (that is,
        the frequency of the carrier is changed 1 time per second). The modulator&#146;s
        amplitude is 100, which will determine <i>how much</i> the frequency of
        the carrier gets changed (at a rate of 1 time per second). </p>

      <p>The amplitude of the modulator is often called the <i>modulation depth,</i>
        since this value determines how high and low the frequency of the carrier
        wave will go. In the sound example, the <i>f<sub>c</sub></i> ranges from
        400 Hz to 600 Hz (500 Hz &ndash; 100 Hz to 500 Hz + 100 Hz). If we change
        the depth to 500 Hz, then our <i>f<sub>c</sub></i> would range from 0
        Hz to 1,000 Hz. Humans can only hear sounds down to about 30 Hz, so there
        should be a moment of &quot;silence&quot; each time the frequency dips
        below that point.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="717">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/vib2.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a><br>
						<b>Soundfile 4.24</b><br>
						Vibrato sound</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Carrier: 500 Hz, modulator frequency: 1 Hz, modulator index: 500.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="717">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<h2>

							Generating Spectra with FM </h2>

      <p>If we raise the frequency of the modulating oscillator above 30 Hz, we
        can start to hear more complex sounds. We can make an analogy to being
        able to see the spokes of a bike wheel if it rotates slowly, but once
        the wheel starts to rotate faster a visual blur starts to occur. </p>
      <p>So it is with FM: when the modulating frequency starts to speed up, the
        sound becomes more complex. The tones you heard in Soundfile 4.24 sliding
        around are called <i>sidebands</i> and are extra frequencies located on
        either side of the carrier frequency. Sidebands are the secret to FM synthesis.
        The frequencies of the sidebands (called, as a group, the <i>spectra</i>)
        depend on the ratio of <i>f<sub>c</sub></i> to <i>f<sub>m</sub></i>. John
        Chowning, in a famous article, showed how to predict where those sidebands
        would be using a simple mathematical idea called <i>Bessel functions.</i>
        By controlling that ratio (called the <i>FM index</i>) and using Bessel
        functions to determine the spectra, you can create a wide variety of sounds,
        from noisy jet engines to a sweet-sounding Fender Rhodes.</p>
					<p class="figure_flush"><img height="429" width="580" src="../images/chapter4/bessel.jpg"></p>

      <p class="figure_caption_flush_bottom"><b>Figure 4.18&nbsp;&nbsp;</b>FM
        sidebands.<br>
        <br>
        </p>
					<p>Soundfiles 4.25 through 4.28 show some simple two-carrier FM sounds with modulating frequencies above 30 Hz.<br><br></p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="777">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/bell.fm.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a><br>
						<b>Soundfile 4.25</b><br>
						Bell-like sound<br>&nbsp;
					</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Carrier: 100 Hz; modulator frequency:
        280 Hz; FM index: 6.0 -&gt; 0.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/bass.cl.fm.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button34" border="0" alt=""></a><br>
						<b>Soundfile 4.26</b><br>
						Bass clarinet-type sound<br>&nbsp;
					</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Carrier: 250 Hz; modulator frequency:
        175 Hz; FM index: 1.5 -&gt; 0.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/trp.fm.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button35" border="0" alt=""></a><br>
						<b>Soundfile 4.27</b><br>
						Trumpet-like sound
							<br>&nbsp;
						</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Carrier: 700 Hz; modulator frequency:
        700 Hz; FM index: 5.0 -&gt; 0.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/fm.snd.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button36" border="0" alt=""></a><br>
						<b>Soundfile 4.28</b><br>
						FM sound</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Carrier: 500 Hz; modulator frequency:
        500 -&gt; 5,000 Hz; FM index: 10.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="777">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<h2><br>
						An Honest-to-Goodness Computer Music Example of FM (Using Csound)</h2>

      <p>One of the most common computer languages for synthesis and sound processing
        is called <i>Csound,</i> developed by Barry Vercoe at MIT. Csound is popular
        because it is powerful, easy to use, public domain, and runs on a wide
        variety of platforms. It has become a kind of <i>lingua franca</i> for
        computer music. Csound divides the world of sound into orchestras, consisting
        of instruments that are essentially unit-generator designs for sounds,
        and scores (or note lists) that tell how long, loud, and so on a sound
        should be played from your orchestra. </p>
					<p>The code in Figure 4.19 is an example of a simple FM instrument in Csound. You don&rsquo;t need to know what most of it means (though by now a lot of it is probably self-explanatory, like <i>sr,</i> which is just the
        sampling rate). Take a close look at the following line:</p>
					<pre>asig foscil p4*env, cpspch(p5), 1,3,2,1 ;simple FM</pre>
					<p>Commas separate the various parts of this command. </p>
					<ul>
        <p><i>Asig</i> is simply a name for this line of code, so we can use it
          later (<i>out asig</i>). </p>
        <p><i>foscil</i> is a predefined Csound unit generator, which is just
          a pair of oscillators that implement simple frequency modulation (in
          a compact way).</p>
        <p><i>p4*env</i> states that the amplitude of the oscillator will be multiplied by an envelope (which, as seen in Figure 4.19, is defined with the Csound <i>linseg</i> function). </p>
        <p><i>cpspch(p5)</i> looks to the fifth value in the score (<i>8.00,</i>
          which will be a middle C) to find what the fundamental of the sound
          will be. The next three values determine the carrier and modulating
          frequencies of the two oscillators, as well as the modulation index
          (we won&#146;t go into what they mean, but these values will give us
          a nice, sweet sound). The final value (1) points to a <i>sine wave table</i>
          (look at the first line of the score).</p>
      </ul>
					<p>Yes, we know, you might be completely confused, but we thought you&#146;d like to see a common language that actually uses some of the concepts we&#146;ve been discussing!</p>

      <h2>Computer Code for an FM Instrument and a Score to Play the Instrument
        in Csound</h2>
					<p class="figure"><img height="377" width="431" src="../images/chapter4/FM.csound.jpg" border="1"></p>
					<p class="figure_caption"><b>Figure 4.19&nbsp;&nbsp;</b>This is a <i>Csound Orchestra and Score</i> blueprint for a simple FM synthesis instrument. Any number of basic unit generators, such as oscillators, adders, multipliers, filters, and so on, can be combined to create complex instruments.<br>
						<br>
						Some music languages, like CSound, make extensive use of the <i>unit generator model.</i> Generally, unit generators are used to create instruments (the orchestra), and then a set of instructions (a score) is created that tells the instruments what to do.</p>

      <p>Now that you understand the basics of FM synthesis, go back to the beginning
        of this section and play with Applet 4.12. FM is kind of interesting theoretically,
        but it&#146;s far more fun and educational to just try it out.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_08.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
