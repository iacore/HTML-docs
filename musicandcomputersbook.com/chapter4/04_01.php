<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_02.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="2" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 4: The Synthesis of Sound by Computer</h1>
					<h1>Section 4.1: <b>Introduction to Sound Synthesis<br>
							<br>
						</b></h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<h2>What Is Sound Synthesis?</h2>

      <p>We&#146;ve learned that a digitally recorded sound is stored as a sequence
        of numbers. What would happen if, instead of using recorded sounds to
        generate those numbers, we simply generated the numbers ourselves, using
        the computer? For instance, what if we just randomly select 44,100 numbers
        (1 second&#146;s worth) and call them <i>samples</i>? What might the result
        sound like?</p>

      <p>The answer is <i>noise.</i> No surprise here&#151;it makes sense that
        a signal generated randomly would sound like noise. Philosophically (and
        mathematically), that&#146;s more or less the definition of noise: random
        or, more precisely, <i>unpredictable</i> information. While noise can
        be sonically interesting (at least at first!) and useful for lots of things,
        it&#146;s clear that we&#146;ll want to be able to generate other types
        of sounds too. We&#146;ve seen how fundamentally important <i>sine waves</i>
        are; how about generating one of those? The formula for generating a sine
        wave is very straightforward:</p>

      <p><i>y</i> = sin(<i>x</i>)</p>

      <p>That&#146;s simple, but controlling frequency, amplitude, phase, or anything
        more complicated than a sine wave can get a bit trickier.<br>
						<br>
					</p>
				</td>
			</tr>
			<!-- <tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25A09114'));return CSClickReturn()" href="../applets/4_1_hear_noise.php" target="_blank" csclick="BCE25A09114"><img src="../images/global/applet.gif" width="80" height="65" name="3" border="0"></a><br>
						<b>Applet 4.1</b><br>
        <span class="icon_caption">Bring in da funk,<br>
							bring in da noise</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This applet allows you to hear some
        basic types of noise: pink, white, and red. </p>

      <p class="applet_caption">We use different colors to describe different
        types of noise, based on the average frequency content of the noiseband.
        <i>White noise</i> contains all frequencies, at random amplitudes (some
        of which may be zero). <i>Pink noise</i> has a heavier dose of lower frequencies, 
        and <i>red noise</i> even more.</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr> -->
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<h2>Sine Wave Example</h2>

      <p>If we take a sequence of numbers (the usual range is 0 to 2<img src="../images/shared/pi.gif" height="10" width="12">, perhaps
        with a <i>phase increment</i> of 0.1&#8212;meaning that we&#146;re adding
        0.1 for each value) and plug them into the sine formula, what results
        is another sequence of numbers that describe a sine wave. By carefully
        selecting the numbers, we can control the frequency of the generated sine
        wave.</p>
					<p>Most basic computer synthesis methods follow this same general scheme: a formula or <i>function</i> is defined that accepts a sequence of values as input. Since waveforms are defined in terms of amplitudes in time, the input sequence of numbers to a synthesis function is usually an ongoing list of time values. A synthesis function usually outputs a sequence of sample values describing a waveform. These sample values are, then, a function of time&#151;the result of the specific synthesis method. </p>
					<p>For example, assume the following sequence of time values.</p>
					<p>(<i>t</i><sub>1</sub>, <i>t</i><sub>2</sub>, <i>t</i><sub>3</sub>,....., <i>t</i><sub><i>n</i></sub>)</p>
					<p>Then we have some function of those values (we can call it <i>f</i>(<i>t</i>)):</p>
					<p>(<i>f</i>(<i>t</i><sub>1</sub>), <i>f</i>(<i>t</i><sub>2</sub>), <i>f</i>(<i>t</i><sub>3</sub>),....., <i>f</i>(<i>t</i><sub><i>n</i></sub>)) = (<i>s</i><sub>1</sub>, <i>s</i><sub>2</sub>, <i>s</i><sub>3</sub>,....., <i>s</i><sub><i>n</i></sub>)</p>

      <p>(<i>s</i><sub>1</sub>, <i>s</i><sub>2</sub>, <i>s</i><sub>3</sub>,.....,
        <i>s</i><sub><i>n</i></sub>) are the sample values.</p>
					<p>This sine wave function is a very simple example. For some sequence of time values (<i>t</i><sub>1</sub>, <i>t</i><sub>2</sub>, <i>t</i><sub>3</sub>,....., <i>t</i><sub><i>n</i></sub>), sin(<i>t</i>) gives us a new set of numbers {sin(<i>t</i><sub>1</sub>), sin(<i>t</i><sub>2</sub>), sin(<i>t</i><sub>3</sub>),....., sin(<i>t</i><sub><i>n</i></sub>)}, which we call the signal&#146;s <i>sample values</i>.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25A11115'));return CSClickReturn()" href="../popups/chapter4/xbit_4_1.php" target="_blank" csclick="BCE25A11115"><img src="../images/global/popup.gif" width="80" height="65" name="button8" border="0"></a><br>
						<b>Xtra bit 4.1<br>
						</b>Computer code<br>
						for generating<br>
						an array of<br>
						a sine wave</p>
				</td>

    <td valign="top" bgcolor="white" width="580"><p>As we&#146;ll see later, more
        sophisticated functions can be used to generate increasingly complex waveforms.
        Often, instead of generating samples from a raw function in time, we take
        in the results of applying a previous function (a list of numbers, not
        a list of time values) and do something to that list with our new function.
        In fact, we have a more general name for sound functions that take one
        sequence of numbers (samples) as an input and give you another list back:
        they&#146;re called <i>filters.</i><br>
        <br>
      </p></td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="20"></td>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25A28116'));return CSClickReturn()" href="../popups/chapter4/xbit_4_2.php" target="_blank" csclick="BCE25A28116"><img src="../images/global/popup.gif" width="80" height="65" name="button6" border="0"></a><br>
						<b>Xtra bit 4.2<br>
						</b>Composer Robert Marsanyi</p>
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25A2D117'));return CSClickReturn()" href="../popups/chapter4/xbit_4_3.php" target="_blank" csclick="BCE25A2D117"><img src="../images/global/popup.gif" width="80" height="65" name="button7" border="0"></a><br>
						<b>Xtra bit 4.3<br>
						</b>Early computer music network ensembles<br>
						<br>
					</p>
				</td>
				<td valign="top" bgcolor="white" width="580"><p>You may never
        have to get quite this &quot;down and dirty&quot; when dealing with sound
        synthesis&#151;most computer music software takes care of things like
        generating noise or sine waves for you. But it&#146;s important to understand
        that there&#146;s no reason why the samples we use need to come from the
        real world. By using the many available synthesis techniques, we can produce
        a virtually unlimited variety of purely synthetic sounds, ranging from
        clearly artificial, machine-made sounds to ones that sound almost exactly
        like their &quot;real&quot; counterparts!</p>
      <p>There are many different approaches to sound synthesis, each with its
        own strengths, weaknesses, and suitability for creating certain types
        of sound. In the sections that follow, we&#146;ll take a look at some
        simple and powerful synthesis techniques.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_02.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
