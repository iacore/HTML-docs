<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_09.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td rowspan="6" valign="top" bgcolor="white" width="20">&nbsp;</td>
    <td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
    <td valign="top" bgcolor="white" width="580"> <h1>Chapter 4: The Synthesis of Sound by Computer</h1>
					<h1>Section 4.8: Granular Synthesis<br>
						<br>
					</h1>
				</td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
    <td valign="top" bgcolor="white" width="580"> <p>When we discussed additive
        synthesis, we learned that complex sounds can be created by adding together
        a number of simpler ones, usually sets of sine waves. <i>Granular synthesis</i>
        uses a similar idea, except that instead of a set of sine waves whose
        frequencies and amplitudes change over time, we use many thousands of
        very <i>short</i> (usually less than 100 milliseconds) overlapping sound
        <i>bursts</i> or <i>grains.</i> The waveforms of these grains are often
        sinusoidal, although any waveform can be used. (One alternative to sinusoidal
        waveforms is to use grains of sampled sounds, either pre-recorded or captured
        live.) By manipulating the temporal placement of large numbers of grains
        and their frequencies, amplitude envelopes, and waveshapes, very complex
        and time-variant sounds can be created.<br>
        <br>
      </p></td>
  </tr>
  <tr>
    <!-- <td colspan="2" align="center" valign="top" bgcolor="white" width="738"> <hr>
    </td> -->
  </tr>
  <tr>
    <!-- <td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE25EF3167'));return CSClickReturn()" href="../applets/4_8_tj_grains.php" target="_blank" csclick="BCE25EF3167"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 4.12</b><br>
        Granular synthesis</p></td>
    <td valign="top" bgcolor="white" width="580"> <p class="applet_caption">This
        applet lets you granulate a signal and alter some of the typical parameters
        of granular synthesis, a popular synthesis technique.</p></td> -->
  </tr>
  <tr>
    <!-- <td colspan="2" align="center" valign="top" bgcolor="white" width="738"> <hr>
    </td> -->
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="white" width="125"></td>
    <td valign="top" bgcolor="white" width="580"> <p class="figure_flush"><img height="437" width="580" src="../images/chapter4/grain.jpg"></p>
      <p class="figure_caption"><b>Figure 4.20&nbsp;&nbsp;</b>A <i>grain</i> is
        created by taking a waveform, in this case a sine wave, and multiplying
        it by an amplitude envelope.<br>
        <br>
        How would a different amplitude envelope, say a square one, affect the
        shape of the grain? What would it do to the sound of the grain?</p>
      <h2>Clouds of Sound</h2>
      <p>Granular synthesis is often used to create what can be thought of as
        &quot;sound clouds&quot;&#151;shifting regions of sound energy that seem
        to move around a sonic space. A number of composers, like Iannis Xenakis
        and Barry Truax, thought of granular synthesis as a way of shaping large
        masses of sound by using granulation techniques. These two composers are
        both considered pioneers of this technique (Truax wrote some of the first
        special-purpose software for granular synthesis). Sometimes, cloud terminology
        is even used to talk about ways of arranging grains into different sorts
        of configurations.</p>
      <p class="figure"><img height="307" width="550" src="../images/chapter4/granularScore.jpg"></p>
      <p class="figure_caption_flush_bottom"><b>Figure 4.21&nbsp;&nbsp;</b>Visualization
        of a granular synthesis &quot;score.&quot; Each dot represents a grain
        at a particular frequency and moment in time. An image such as this one
        can give us a good idea of how this score might sound, even though there
        is some important information left out (such as the grain amplitudes,
        waveforms, amplitude envelopes, and so on).<br>
        <br>
        What sorts of sounds does this image imply? If you had three vocal performers,
        one for each &quot;cloud,&quot; how would you go about performing this
        piece? Try it!<br>
        <br>
      </p></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="white" width="20"></td>
    <td colspan="2" align="center" valign="top" bgcolor="white" width="709"> <hr>
    </td>
  </tr>

			<tr>
    <td width="20" valign="top" bgcolor="white"></td>
    <td align="center" valign="top" bgcolor="white" width="125"> <p class="icon_caption"><a onclick="setSrc('../sounds/chapter4/implements.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a><br>
						<b>Soundfile 4.29</b><br>
        &quot;Implements of Actuation&quot; </p></td>
    <td width="580" valign="top" bgcolor="white"> <p class="applet_caption">This
        is an excerpt of a composition by computer music composer and researcher
        Mara Helmuth titled &quot;Implements of Actuation.&quot; The sounds of
        an electric mbira and bicycle wheels are transformed via granular synthesis.</p>
      <p class="applet_caption">There are a great many commercial and public domain
        applications to do granular synthesis because it is relatively easy to
        implement and the sounds can be very interesting and attractive.</p>
					<p></p>
				</td>
  </tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="04_09.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
