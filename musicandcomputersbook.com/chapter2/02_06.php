<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_07.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="13" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 2: The Digital Representation of Sound,<br>
						Part Two: Playing by the Numbers</h1>
					<h1>Section 2.6: Digital Copying<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580"><p>One of the most 
        important things about digital audio is the ability, theoretically, to 
        make perfect copies. Since all we&#146;re storing are lists of numbers, 
        it shouldn&#146;t be too hard to rewrite them somewhere else without error. 
        This means that unlike, for example, photocopying a photocopy, there is 
        no information loss in the transfer of data from one copy to another, 
        nor is there any noise added. Noise, in this context, is any information 
        that is added by the imperfections of the recording or playback technology. 
      </p>
      <p class="figure"><img height="190" width="290" src="../images/chapter2/digital.analog.jpg"></p>
					<p class="figure_caption_flush_bottom"><b>Figure 2.15</b>&nbsp;&nbsp;Each generation of analog copying is a little <i>noisier</i> than the last. Digital copies are (generally!) perfect, or noiseless.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<br>
						<a onclick="CSAction(new Array(/*CMP*/'BCE24B4678'));return CSClickReturn()" href="../popups/chapter2/xbit_2_2.php" target="_blank" csclick="BCE24B4678"><img src="../images/global/popup.gif" width="80" height="65" name="button10" border="0"></a><br>
						<b>Xtra bit 2.2<br>
						</b>The peanut butter conundrum</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<h2><b>Not-So-Perfect Copying</b></h2>
					<p>What does perfect copying really mean? In the good old days, if you copied your favorite LP vinyl (remember those?) onto cassette and gave it to your buddy, you knew that your original sounded better than your buddy&#146;s copy. He knew the same: if he made a copy for his buddy, it would be even worse (these copies are called <i>generations</i>). It was like the children&#146;s game of telephone:</p>
					<blockquote>
						<p><i>&quot;My cousin once saw a Madonna concert!&quot;</i></p>
						<p><i>&quot;My cousin Vince saw Madonna&#146;s monster!&quot;</i></p>
						<p><i>&quot;My mother Vince sat onna mobster!&quot;</i></p>
						<p><i>&quot;My mother winces when she eats lobster!&quot;</i></p>
					</blockquote>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE24B4C79'));return CSClickReturn()" href="../applets/2_6_noisy_copy.php" target="_blank" csclick="BCE24B4C79"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 2.5</b><br>
						
						<span class="icon_caption">Melody copier</span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">Start with a simple tune and set a <i>noise</i> parameter that determines how bad the next copy of the file will be. The melody <i>degrades</i> over time.</p>
					<p class="applet_caption">Note that when we use the term <i>degrade,</i> we are giving a purely technical description, not an aesthetic one. The melodies don&#146;t necessarily get worse, they just get further from the original.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<a onclick="CSAction(new Array(/*CMP*/'BCE24B5180'));return CSClickReturn()" href="../popups/chapter2/xbit_2_3.php" target="_blank" csclick="BCE24B5180"><img src="../images/global/popup.gif" width="80" height="65" name="button8" border="0"></a><br>
						<b>Xtra bit 2.3<br>
						</b>Errors in digital copying: <i>parity</i></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p><br>
						The noise added to successive generations blurs the detail of the original. But we still try to reinterpret the new, blurred message in a way that makes sense to us. In audio, this blurring of detail usually manifests itself as a loss of high-frequency information (in some sense, <i>sonic detail</i>) as well as an addition of the noise of the transmission mechanism itself (hiss, hum, rumble, etc.).<br>
						<br>
					</p>
				</td>
			</tr>
			
			
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			
			
			
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter2/lucier.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button25" border="0" alt=""></a><br>
						<b>Soundfile 2.4</b><br>
						<span class="icon_caption">Our rendition of Alvin Lucier&#146;s "I am Sitting in a Room"</span>
						</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
				<p class="applet_caption">Alvin Lucier&#146;s classic electronic work, &quot;I Am Sitting in a Room,&quot; &quot;redone&quot; with our own text.</p>
					<blockquote class="applet_caption">
						&quot;This is a cheap imitation of a great and classic work of electronic music by the composer Alvin Lucier. You can easily hear the degradations of the copied sound; this was part of the composer&#146;s idea for this composition.&quot;
					</blockquote>
					<p class="applet_caption">We strongly encourage all of you to listen to the original, which is a beautiful, innovative work.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<p>				
				


					<h2><b>Is It Real, or Is It...?</b></h2>
					<p>Digital sound technology changes this copying situation entirely and raises some new and interesting questions. What&#146;s the original, and what&#146;s the copy? </p>
					<p>When we copy a CD to some other digital medium (like our hard drive), all we&#146;re doing is copying a list of numbers, and there&#146;s no reason to expect that any, or significantly many, errors will be incurred. That means the copy of the president&#146;s speech that we sample for our web site contains the same data&#151;is the same signal&#151;as the original. There&#146;s no way to trace where the original came from, and in a sense no way to know who owns it (if anybody does). </p>
					<p>This makes questions of copyright and royalties, and the more general issue of intellectual property, complicated to say the least, and has become, through the musical technique of sampling (as in rap, techno, and other musical styles), an important new area of technological, aesthetic, and legal research.</p>
					
      <ul>
        <p><i>&quot;If creativity is a field, copyright is the fence.&quot; <br>
          &#151;John Oswald</i>
       </ul>
					<p>In the mid-1980s, composer John Oswald made a famous CD in which 
          every track was an electronic transformation, in some unique way, of 
          existing, copyrighted material. This controversial CD was eventually 
          litigated out of existence, largely on the basis of its Michael Jackson 
          track (called &quot;Dab&quot;). <!--.AGL.wrap(Kirk ;2004.08.17 11:42;;)<p><font color="red">Digital copying has produced a crisis in the commercial music world, as anyone who has ever used something like Napster knows. The government, the commercial music industry, and even organizations like BMI (Broadcast Music Inc.) and ASCAP (American Society of Composers, Authors, and Publishers), which distribute royalties to composers and artists, are struggling with the law and the technology, and the two are forever out of synch (guess who&rsquo;s ahead!). The following graphic was prepared (this is an excerpt) by BMI to try to illustrate the problem (at least as it appeared about a year ago). Things change every day, but one thing never changes&#151;technology moves faster than our society&rsquo;s ability to deal with it legally and ethically. Of course, that makes things interesting. (<b>Slated for removal - KM)</b></font></p>--></p>
					<p>Digital copying has produced a crisis in the commercial music world, as anyone who has downloaded music from the internet knows. The government, the commercial music industry, and even organizations like BMI (Broadcast Music Inc.) and ASCAP (American Society of Composers, Authors, and Publishers), which distribute royalties to composers and artists, are struggling with the law and the technology, and the two are forever out of synch (guess who's ahead!). Things change every day, but one thing never changes-technology moves faster than our society's ability to deal with it legally and ethically. Of course, that makes things interesting.</p>
					<p class="figure"><img height="228" width="247" src="../images/chapter2/BMI1.jpg" border="1"></p>
					<p class="figure_caption"><b>Figure 2.16</b></p>
					<p class="figure"><img height="215" width="341" src="../images/chapter2/BMI2.jpg" border="1"></p>
					<p class="figure_caption"><b>Figure 2.17</b></p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter2/plunderparton.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button22" border="0" alt=""></a><br>
						<b>Soundfile 2.5</b><br>
						<span class="icon_caption"><i>Pretender,</i> from John Oswald&#146;s <i>Plunderphonics</i></span></p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>In <i>Pretender,</i> 
        John Oswald gradually changes the pitch and speed of Dolly Parton&#146;s 
        version of the song &quot;The Great Pretender&quot; in a wry and musically 
        beautiful commentary on gender, intellectual property, and sound itself.</p>
					<p class="figure"><img height="280" width="358" src="../images/chapter2/BMI3.jpg" border="1"></p>
					<p class="figure_caption"><b>Figure 2.18</b></p>
					<p class="figure"><img height="248" width="345" src="../images/chapter2/BMI10.jpg" border="1"></p>
					<p class="figure_caption"><b>Figure 2.19</b></p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter2/tape.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button27" border="0" alt=""></a><br>
						<b>Soundfile 2.6(a)</b><br>
						Original</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter2/tape.degrade.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button26" border="0" alt=""></a><br>
						<b>Soundfile 2.6(b)</b><br>
						Degraded</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter2/dmahler.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button24" border="0" alt=""></a><br>
						<b>Soundfile 2.7<br>
						</b>David Mahler</p>
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE24BCB87'));return CSClickReturn()" href="../popups/chapter2/xbit_2_4.php" target="_blank" csclick="BCE24BCB87"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 2.4<br>
						</b>Digital watermarking</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>Soundfiles 2.6(a) and 2.6(b) show an analog copy that was 
        made over and over again, exemplifying how the signal degrades to noise 
        after numerous copies. Soundfile 2.6(a) is the original digital file; 
        Soundfile 2.6(b) is the copied analog file. Note that unlike the example 
        in Soundfile 2.4, where we tried to re-create a famous piece by Alvin 
        Lucier, this example doesn&#146;t involve the acoustics of space, just 
        the noise of electronics, tape to tape.</p>
					<table border="0" cellspacing="0" cellpadding="6" align="right">
						<tr>
							<td>
								<p class="figure_caption_flush"><img height="191" width="150" src="../images/chapter2/DMahler.jpg" vspace="4"><br>
									<b>Figure 2.20</b> David Mahler</p>
							</td>
						</tr>
					</table>
					<p>Sampling, and using pre-existing materials, can be a lot of fun and artistically interesting. Soundfile 2.7 is an excerpt from David Mahler&#146;s composition &quot;Singing in the Style of The Voice of the Poet.&quot; This is a playful work that in some ways parodies text-sound composition, radio interviewing, and electronic music by using its own techniques. In this example, Mahler makes a joke of the fact that when speech is played backward, it sounds like &quot;Swedish,&quot; and he combines that effect (backward talking) with composer Ingram Marshall&#146;s talking about his interest in Swedish text-sound composers.</p>
					<p>The entire composition can be heard on David Mahler&#146;s CD <i>The Voice of the Poet; Works on Tape 1972&ndash;1986</i>, on Artifact Recordings.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_07.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
