<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_04.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="10" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 2: The Digital Representation of Sound,<br>
						Part Two: Playing by the Numbers</h1>
					<h1>Section 2.3: Sampling Theory<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580"><p>So now we know
        that we need to sample a continuous waveform to represent it digitally.
        We also know that the faster we sample it, the better. But this is still
        a little vague. How <i>often</i> do we need to sample a waveform in order
        to achieve a good representation of it? </p>
      <p>The answer to this question is given by the <i>Nyquist sampling theorem,</i> which states that to well represent a signal, the sampling rate (or sampling frequency&#151;not to be confused with the frequency content of the sound) needs to be at least twice the highest frequency contained in the sound of the signal.</p>
					<p>For example, look back at our time-frequency picture in Figure 2.3 from Section 2.1. It looks like it only contains frequencies up to 8,000 Hz. If this were the case, we would need to sample the sound at a rate of 16,000 Hz (16 kHz) in order to accurately reproduce the sound. That is, we would need to take sound bites (bytes?!) 16,000 times a second.</p>
					<p>In the next chapter, when we talk about representing sounds in the frequency domain (as a combination of various amplitude levels of frequency components, which change over time) rather than in the time domain (as a numerical list of sample values of amplitudes), we&#146;ll learn a lot more about the ramifications of the Nyquist theorem for digital sound. But for our current purposes, just remember that since the human ear only responds to sounds up to about 20,000 Hz, we need to sample sounds at least 40,000 times a second, or at a rate of 40,000 Hz, to represent these sounds for human consumption. You may be wondering why we even need to represent sonic frequencies that high (when the piano, for instance, only goes up to the high 4,000 Hz range). The answer is timbral, particularly spectral. Remember that we saw in Section 1.4 that those higher frequencies fill out the descriptive sonic information.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2495956'));return CSClickReturn()" href="../popups/chapter2/xbit_2_1.php" target="_blank" csclick="BCE2495956"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 2.1</b><br>
						Free sample:<br>

						a tonsorial tale</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>Just to review: we measure frequency in cycles per second (cps) or Hertz (Hz). The frequency range of human hearing is usually given as 20 Hz to 20,000 Hz, meaning that we can hear sounds in that range. Knowing that, if we decide that the highest frequency we&rsquo;re interested in is 20 kHz, then according to the Nyquist theorem, we need a sampling rate of at least twice that frequency, or 40 kHz.</p>
					<p class="figure"><img height="216" width="493" src="../images/chapter2/aliasing.jpg"></p>
					<p class="figure_caption_flush_bottom"><b>Figure 2.7&nbsp;&nbsp;</b>Undersampling: What happens if we sample too slowly for the frequencies we&rsquo;re trying to represent?<br>
						<br>
						We take samples (black dots) of a sine wave (in blue) at a certain interval (the sample rate). If the sine wave is changing too quickly (its frequency is too high), then we can&rsquo;t grab enough information to reconstruct the waveform from our samples. The result is that the high-frequency waveform masquerades as a lower-frequency waveform (how sneaky!), or that the higher frequency is <i>aliased</i> to a lower frequency.<br>
						<br>
					</p>
				</td>
			</tr>
			<!-- <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="729">
					<hr>
				</td>
			</tr> -->
			<!-- <tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2496057'));return CSClickReturn()" href="../applets/2_3_osc_bl.php" target="_blank" csclick="BCE2496057"><img src="../images/global/applet.gif" width="80" height="65" name="4" border="0"></a><br>
						<b>Applet 2.2</b><br>
						Oscillators</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This applet demonstrates <i>band-limited</i> and <i>non-band-limited</i> waveforms.</p>
					<p class="applet_caption">Band-limited waveforms are those in which the synthesis method itself does not allow higher harmonics, or frequencies, than the sampling rate allows. It&#146;s kind of like putting a governor on your car that doesn&#146;t allow you to go past the speed limit. This technique can be useful in a lot of applications where one has absolutely no interest in the wonderful joy of listening to things like aliasing, foldover, and unwanted distortion.</p>
				</td>
			</tr> -->
			<!-- <tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="729">
					<hr>
				</td>
			</tr> -->
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><br>
						<a onclick="setSrc('../sounds/chapter2/bragg.1024.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button22" border="0" alt=""></a><br>
						<b>Soundfile 2.1</b><br>
						Undersampling</p>
				</td>
				<td valign="top" bgcolor="white" width="580"><p><br>
        Soundfile 2.1 demonstrates undersampling of the same sound source as Soundfile
        2.2. In this example, the file was sampled at 1,024 samples per second.
        Note that the sound sounds &quot;muddy&quot; at a 1,024 sampling rate&#151;that
        rate does not allow us any frequencies above about 500 Hz, which is sort
        of like sticking a large canvas bag over your head, and putting your fingers
        in your ears, while listening.<br>
        <br>
      </p></td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter2/bragg.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button23" border="0" alt=""></a><br>
						<b>Soundfile 2.2</b><br>
						Standard sampling at 44,100 samples per second</p>
				</td>
				<td valign="top" bgcolor="white" width="580"><p>Soundfile 2.2
        was sampled at the standard 44,100 samples per second. This allows frequencies
        as high as around 22 kHz, which is well above our ear&#146;s high-frequency
        range. In other words, it&#146;s &quot;good enough.&quot; </p>
      <div align="center">
						<p class="figure_flush"><img height="181" width="580" src="../images/chapter2/512.mp3.jpg"></p>
					</div>
					<p class="figure_caption"><b>Figure 2.8&nbsp;&nbsp;</b>Picture of an undersampled waveform. This sound was sampled 512 times per second. This was <i>way</i> too slow.</p>
					<div align="center">
						<p class="figure_flush"><img height="180" width="580" src="../images/chapter2/44100.jpg"></p>
					</div>
					<p class="figure_caption_flush_bottom"><b>Figure 2.9&nbsp;&nbsp;</b>This is the same sound file as above, but now sampled 44,100 (44.1 kHz) times per second. Much better.</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<!-- <p class="icon_caption"><br>
						<br>
						<a onclick="CSAction(new Array(/*CMP*/'BCE2499360'));return CSClickReturn()" href="../applets/2_3_scrubber.php" target="_blank" csclick="BCE2499360"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 2.3</b><br>
						Scrubber applet</p> -->
				</td>
				<td valign="top" bgcolor="white" width="580">
					<h2>Aliasing</h2>
					<p>The most common standard sampling rate for digital audio (the one used for CDs) is 44.1 kHz, giving us a Nyquist frequency (defined as half the sampling rate) of 22.05 kHz. If we use lower sampling rates, for example, 20 kHz, we can&#146;t represent a sound whose frequency is above 10 kHz. In fact, if we try, we&#146;ll get usually undesirable artifacts, called <i>foldover</i> or <i>aliasing,</i> in the signal. </p>
					<p>In other words, if a sine wave is changing quickly, we would get the same set of samples that we would have obtained had we been taking samples from a sine wave of lower frequency! The effect of this is that the higher-frequency contributions now act as <i>impostors</i> of lower-frequency information. The effect of this is that there are extra, unanticipated, and new low-frequency contributions to the sound. Sometimes we can use this in cool, interesting ways, and other times it just messes up the original sound.</p>
					<p>So in a sense, these impostors are <i>aliases</i> for the low frequencies, and we say that the result of our <i>undersampling</i> is an aliased waveform at a lower frequency.</p>
					<div align="center">
						<p class="figure_flush"><img height="233" width="580" src="../images/chapter2/alias.grey.jpg"></p>
					</div>
					<p class="figure_caption_flush_bottom"><b>Figure 2.10&nbsp;&nbsp;</b>Foldover aliasing. This picture shows what happens when we sweep a sine wave up past the Nyquist rate. It&#146;s a picture in the frequency domain (which we haven&#146;t talked about much yet), so what you&#146;re seeing is the amplitude of specific component frequencies over time. The <i>x</i>-axis is frequency, the <i>z</i>-axis is amplitude, and the <i>y</i>-axis is time (read from back to front).<br>
						<br>
						As the sine wave sweeps up into frequencies above the Nyquist frequency, an aliased wave (starting at 0 Hz and ending at 44,100 Hz over 10 seconds) is reflected below the Nyquist frequency of 22,050 Hz. The sound can be heard in Soundfile 2.3.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter2/sine.sweep.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button24" border="0" alt=""></a><br>
						<b>Soundfile 2.3</b><br>
						Chirping</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>Soundfile 2.3 is a 10-second soundfile sweeping a sine wave from 0 Hz to 44,100 Hz. Notice that the sound seems to disappear after it reaches the Nyquist rate of 22,050 Hz, but then it wraps around as aliased sound back into the audible domain.</p>
					<h2>Anti-Aliasing Filters</h2>
					<p>Fortunately it&#146;s fairly easy to avoid aliasing&#151;we simply make sure that the signal we&#146;re recording doesn&#146;t contain any frequencies above the Nyquist frequency. To accomplish this task, we use an anti-aliasing filter on the signal. <i>Audio filtering</i> is a technique that allows us to selectively keep or throw out certain frequencies in a sound&#151;just as light filters (like ones you might use on a camera) only allow certain frequencies of light (colors) to pass. For now, just remember that a filter lets us color a sound by changing its frequency content.</p>
					<p>An anti-aliasing filter is called a <i>low-pass filter</i> because it only allows frequencies below a certain cutoff frequency to pass. Anything above the cutoff frequency gets removed. By setting the cutoff frequency of the low-pass filter to the Nyquist frequency, we can throw out the offending frequencies (those high enough to cause aliasing) while retaining all of the lower frequencies that we want to record.</p>
					<p class="figure"><img height="342" width="302" src="../images/chapter2/aa.filter.jpg"></p>
					<p class="figure_caption"><b>Figure 2.11&nbsp;&nbsp;</b>An anti-aliasing low-pass filter. Only the frequencies within the <i>passband,</i> which stops at the Nyquist frequency (and &quot;rolls off&quot; after that), are allowed to pass. This diagram is typical of the way we draw what is called the <i>frequency response</i> of a filter. It shows the amplitude that will come out of the filter in response to different frequencies (of the same amplitude).<br>
						<br>
        Anti-aliasing filters can be analogized to coffee filters. The desired
        components (frequencies or liquid coffee) are preserved, while the filter
        (coffee filter or anti-aliasing filter) catches all the undesirable components
        (the coffee grounds or the frequencies that the system cannot handle).<br>
						<br>
						Perfect anti-aliasing filters cannot be constructed, so we almost always get some aliasing error in an ADC&rarr;DAC conversion.</p>
					<p>Anti-aliasing filters are a standard component in digital sound recording, so aliasing is not usually of serious concern to the average user or computer musician. But because many of the sounds in computer music are not recorded (and are instead created digitally inside the computer itself), it&#146;s important to fully understand aliasing and the Nyquist theorem. There&#146;s nothing to stop us from using a computer to create sounds with frequencies well above the Nyquist frequency. And while the computer has no problem dealing with such sounds as data, as soon as we mere humans want to actually hear those sounds (as opposed to just conceptualizing or imagining them), we need to deal with the physical realities of aliasing, the Nyquist theorem, and the analog-to-digital conversion process.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_04.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
