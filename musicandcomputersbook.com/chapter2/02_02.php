<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_03.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="3" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 2: The Digital Representation of Sound,<br>
						Part Two: Playing by the Numbers</h1>
					<h1>Section 2.2: Analog Versus Digital<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<p>The distinction between analog and digital information is not an easy one to understand, but it&#146;s fundamental to the realm of computer music (and in fact computer anything!). In this section, we&#146;ll offer some analogies, explanations, and other ways to understand the difference more intuitively.</p>
					<p>Imagine watching someone walking along, bouncing a ball. Now imagine that the ball leaves a trail of smoke in its path. What does the trail look like? Probably some sort of continuous zigzag pattern, right?</p>
					<p class="figure"><img height="43" width="181" src="../images/chapter2/bouncing%20ball.image.jpg"><img height="43" width="20" src="../images/chapter2/ball3.gif"></p>
					<p class="figure_caption"><b>Figure 2.4&nbsp;&nbsp;</b>The path of a bouncing ball.</p>
					<p>OK, keep watching, but now blink repeatedly. What does the trail look like now? Because we are blinking our eyes, we&#146;re only able to see the ball at discrete moments in time. It&#146;s on the way up, we blink, now it&#146;s moved up a bit more, we blink again, now it may be on the way down, and so on. We&#146;ll call these snapshots <i>samples</i> because we&#146;ve been taking visual samples of the complete trajectory that the ball is following. The <i>rate</i> at which we obtain these samples (blink our eyes) is called the <i>sampling rate.</i></p>
					<p>It&#146;s pretty clear, though, that the faster we sample, the better chance we have of getting an accurate picture of the entire continuous path along which the ball has been traveling. </p>
					<p class="figure"><img height="43" width="189" src="../images/chapter2/bouncing%20ball2.image.jpg"><img height="43" width="20" src="../images/chapter2/ball3.gif"></p>
					<p class="figure_caption"><b>Figure 2.5&nbsp;&nbsp;</b>The same path, but <i>sampled</i> by blinking.</p>
					<p>What&#146;s the difference between the two views of the bouncing ball: the blinking and the nonblinking? Each view pretty much gives the same picture of the ball&#146;s path. We can tell how fast the ball is moving and how high it&#146;s bouncing. The only real difference seems to be that in the first case the trail is <i>continuous,</i> and in the second it is broken, or <i>discrete.</i> That&#146;s the main distinction between analog and digital representations of information: analog information is continuous, while digital information is not.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE2493F55'));return CSClickReturn()" href="../applets/2_2_sampled_fader.php" target="_blank" csclick="BCE2493F55"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 2.1</b><br>
						Sampled fader</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<h2><b>Analog and Digital Waveform Representations</b></h2>
					<p>Now let&#146;s take a look at two time domain representations of a sound wave, one analog and one digital, in Figure 2.6.</p>
					<p class="figure"><img height="216" width="498" src="../images/chapter2/sinestair.jpg"></p>
					<p class="figure_caption"><b>Figure 2.6&nbsp;&nbsp;</b>An analog waveform and its digital cousin: the analog waveform has smooth and continuous changes, and the digital version of the same waveform has a stairstep look. The black squares are the actual samples taken by the computer. The grey lines suggest the &quot;staircasing&quot; that is an inevitable result of converting an analog signal to digital form. Note that the grey lines are only for show&#151;all that the computer knows about are the discrete points marked by the black squares. There is nothing in between those points.</p>
					<p>The analog waveform is nice and smooth, while the digital version is kind of chunky. This &quot;chunkiness&quot; is called <i>quantization</i> or <i>staircasing</i>&#151;for obvious reasons! Where do the &quot;stairsteps&quot; come from? Go back and look at the digital bouncing ball figure again, and see what would happen if you connected each of the samples with two lines at a right angle. Voila, a staircase!</p>
					<p>Staircasing is an artifact of the digital recording process, and it illustrates how digitally recorded waveforms are only <i>approximations</i> of analog sources. They will <i>always</i> be approximations, in some sense, since it is theoretically impossible to store truly continuous data digitally. However, by increasing the number of samples taken each second (the <i>sample rate</i>), as well as increasing the accuracy of those samples (the <i>resolution</i>), an extremely accurate recording can be made. In fact, we can prove mathematically that we can get so accurate that, theoretically, there is <i>no</i> difference between the analog waveform and its digital representation, at least to our ears.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_03.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
