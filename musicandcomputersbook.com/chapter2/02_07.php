<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_08.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="2" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 2: The Digital Representation of Sound,<br>
						Part Two: Playing by the Numbers</h1>
					<h1>Section 2.7: Storage Concerns: The Size of Sound<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="bottom" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE24C1388'));return CSClickReturn()" href="../popups/chapter2/xbit_2_5.php" target="_blank" csclick="BCE24C1388"><img src="../images/global/popup.gif" width="80" height="65" name="button5" border="0"></a><br>
						<b>Xtra bit 2.5</b><br>
						
						Hard drives<br>
						<br>
						<br>
						<br>
					</p>
				</td>
				<td valign="top" bgcolor="white" width="580"><p>OK, now we know 
        about bits and how to copy them from one place to another. But where exactly 
        do we <i>put</i> all these bits in the first place? </p>
      <p>One familiar digital storage medium is the <i>compact disc</i> (CD). 
        Bits are encoded on a CD as a series of <i>pits</i> in a metallic surface. 
        The length of a pit corresponds to the state of the bit (on or off). As 
        we&#146;ve seen, it takes a lot of bits to store high-quality digital 
        audio&#151;a standard 74-minute CD can have more than 6 billion pits on 
        it!</p>
					<p class="figure"><img height="183" width="229" src="../images/chapter2/CompactDisc.gif"></p>
					<p class="figure_caption"><b>Figure 2.21&nbsp;&nbsp;</b>This photo shows what standard CD <i>pits</i> look like under high magnification. A CD stores data digitally, using long and short pits to encode a binary representation of the sound for reading by the laser mechanism of a CD player. Thanks to Evolution Audio and Video in Agoura Hills California for this photo from their A/V Newsletter (Jan 1996). The magnification is 20k.</p>
					<div align="right">
						<p class="figure_flush"><img height="149" width="565" src="../images/chapter2/cross.cd.gif"></p>
					</div>
					<p class="figure_caption"><b>Figure 2.22&nbsp;&nbsp;</b>Physically, a CD is composed of a thin film of <i>aluminum</i> embedded between two discs of polycarbonate plastic. Information is recorded on the CD as a series of microscopic pits in the aluminum film arranged along a continuous spiral track. If expanded linearly, the track would span over 3 miles.<br>
						<br>
						Using a low-power infrared laser (with a wavelength of 780 nm), the data are retrieved from the CD using photosensitive sensors that measure the intensity of the reflected light as the laser traverses the track. Since the recovered bit stream is simply a bit pattern, any digitally encoded information can be stored on a CD.</p>


					<h2><b>Putting Everything Together</b></h2>

					<p>Now that we know about sampling rates, bit width, number systems, and a lot of other stuff, how about a nice practical example that ties it all together?</p>
					<p>Assume we&#146;re composers working in a digital medium. We&#146;ve got some cool sounds, and we want to store them. We need to figure out how much storage we need.</p>
					<p>Let&#146;s assume we&#146;re working with a <i>stereo</i> (two independent channels of sound) signal and 16-bit samples. We&#146;ll use a sampling rate of 44,100 times/second. </p>
					<p>One 16-bit sample takes 2 bytes of storage space (remember that 8 bits equal 1 byte). Since we&#146;re in stereo, we need to <i>double</i> that number (there&#146;s one sample for each channel) to 4 bytes per sample. For each second of sound, we will record 44,100 four-byte stereo samples, giving us a data rate of 176.4 kilobytes (176,400 bytes) per second.</p>
					<p>Let&#146;s review this, because we know it can get a bit complicated. There are 60 seconds in a minute, so 1 minute of high-quality stereo digital sound takes 176.4 * 60 KB or 10.584 megabytes (10,584 KB) of storage space. In order to store 1 hour of stereo sound at this sampling rate and resolution, we need 60 * 10.584 MB, or about 600 MB. This is more or less the amount of sound information on a standard commercial audio CD (actually, it can store closer to 80 minutes comfortably). One <i>gigabyte</i> is equal to 1,000 megabytes, so a standard CD is around two-thirds of a gigabyte.</p>
					<p class="figure_flush"><img height="176" width="570" src="../images/chapter2/bits_to_cd.jpg"></p>
					<p class="figure_caption"><b>Figure 2.23</b></p>
					<p>One good rule of thumb is that CD-quality sound currently requires about 10 megabytes per minute.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_08.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
