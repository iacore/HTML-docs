<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_05.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="6" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 2: The Digital Representation of Sound,<br>
						Part Two: Playing by the Numbers</h1>
					<h1>Section 2.4: Binary Numbers<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<p>Now we know more than we ever wanted to know about how often to store numbers in order to digitally represent a signal. But surely speed isn&#146;t the only thing that matters. What about size? In this section, we&#146;ll tell you something about how big those numbers are and what they &quot;look&quot; like.</p>
					<p>All digital information is stored as <i>binary numbers.</i> A binary number is simply a way of representing our regular old numbers as a list, or sequence, of zeros and ones. This is also called a <i>base-2</i> representation.</p>
					<p>In order to explain things in base 2, let&#146;s think for a minute about those ordinary numbers that we use. You know that we use a decimal representation of numbers. This means that we write numbers as finite sequences whose symbols are taken from a collection of ten symbols: our digits 0, 1, 2, 3, 4, 5, 6, 7, 8, and 9.</p>
					<p>A number is really a shorthand for an arithmetic expression. For example:</p>
					<div align="center">
						<p><br>
							<img height="34" width="374" src="../images/chapter2/e2.4.1.gif"><br>
							<br>
						</p>
					</div>
					<p>Now we bet you can see a pattern here. For every place in the number, we just multiply by a higher power of ten. Representing a number in <i>base-8</i>, say (which is called an <i>octal</i> representation), would mean that we would only use eight distinct symbols, and multiply those by powers of 8 (instead of powers of 10). For example (in base 8), the number 2,051 means:</p>
					<div align="center">
						<p><br>
							<img height="85" width="353" src="../images/chapter2/e2.4.2.gif"><br>
							<br>
						</p>
					</div>
					<p>There is even <i>hexadecimal</i> notation in which we work in base 16, so we need 16 symbols. These are our usual 0 through 9, augmented by A, B, C, D, E, and F (counting for 10 through 15). So, in base 16 we might write:</p>
					<div align="center">
						<p><br>
							<img height="85" width="409" src="../images/chapter2/e2.4.3.gif"><br>
							<br>
						</p>
					</div>
					<p>But back to binary: this is what we use for digital (that is, computer) representations. With only two symbols, 0 and 1, numbers look like this:</p>
					<div align="center">
						<p><br>
							<img height="57" width="419" src="../images/chapter2/e2.4.4.gif"><br>
							<br>
						</p>
					</div>
					<p>Each of the places is called a <i>bit</i> for (<i>B</i>inary dig<i>IT</i>). The leftmost bit is called the <i>most significant bit</i> (MSB), and the rightmost is the <i>least significant bit</i> (because the digit in the leftmost position, the highest power of 2, makes the most significant contribution to the total value represented by the number, while the rightmost makes the least significant contribution). If the digit at a given bit is equal to 1, we say it is <i>set.</i> We also label the bits by what power of 2 they represent.</p>
					<p>How many different numbers can we represent with 4 bits? There are sixteen such combinations, and here they are, along with their octal, decimal, and hexadecimal counterparts:</p>
					<div align="center">
					<table border="0" cellpadding="6" cellspacing="1" width="264" bgcolor="#333333">
						<tr>
							<td bgcolor="white"><b>Binary</b></td>
							<td bgcolor="white"><b>Octal</b></td>
							<td bgcolor="white"><b>Decimal</b></td>
							<td bgcolor="white"><b>Hexadecimal</b></td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">0000</td>
							<td align="center" bgcolor="white">00</td>
							<td align="center" bgcolor="white">00</td>
							<td align="center" bgcolor="white">00</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">0001</td>
							<td align="center" bgcolor="white">01</td>
							<td align="center" bgcolor="white">01</td>
							<td align="center" bgcolor="white">01</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">0010</td>
							<td align="center" bgcolor="white">02</td>
							<td align="center" bgcolor="white">02</td>
							<td align="center" bgcolor="white">02</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">0011</td>
							<td align="center" bgcolor="white">03</td>
							<td align="center" bgcolor="white">03</td>
							<td align="center" bgcolor="white">03</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">0100</td>
							<td align="center" bgcolor="white">04</td>
							<td align="center" bgcolor="white">04</td>
							<td align="center" bgcolor="white">04</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">0101</td>
							<td align="center" bgcolor="white">05</td>
							<td align="center" bgcolor="white">05</td>
							<td align="center" bgcolor="white">05</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">0110</td>
							<td align="center" bgcolor="white">06</td>
							<td align="center" bgcolor="white">06</td>
							<td align="center" bgcolor="white">06</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">0111</td>
							<td align="center" bgcolor="white">07</td>
							<td align="center" bgcolor="white">07</td>
							<td align="center" bgcolor="white">07</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">1000</td>
							<td align="center" bgcolor="white">10</td>
							<td align="center" bgcolor="white">08</td>
							<td align="center" bgcolor="white">08</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">1001</td>
							<td align="center" bgcolor="white">11</td>
							<td align="center" bgcolor="white">09</td>
							<td align="center" bgcolor="white">09</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">1010</td>
							<td align="center" bgcolor="white">12</td>
							<td align="center" bgcolor="white">10</td>
							<td align="center" bgcolor="white">0A</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">1011</td>
							<td align="center" bgcolor="white">13</td>
							<td align="center" bgcolor="white">11</td>
							<td align="center" bgcolor="white">0B</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">1100</td>
							<td align="center" bgcolor="white">14</td>
							<td align="center" bgcolor="white">12</td>
							<td align="center" bgcolor="white">0C</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">1101</td>
							<td align="center" bgcolor="white">15</td>
							<td align="center" bgcolor="white">13</td>
							<td align="center" bgcolor="white">0D</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">1110</td>
							<td align="center" bgcolor="white">16</td>
							<td align="center" bgcolor="white">14</td>
							<td align="center" bgcolor="white">0E</td>
						</tr>
						<tr>
							<td align="center" bgcolor="white">1111</td>
							<td align="center" bgcolor="white">17</td>
							<td align="center" bgcolor="white">15</td>
							<td align="center" bgcolor="white">0F</td>
						</tr>
					</table>
					</div>
					<p class="figure_caption"><b>Table 2.1</b>&nbsp;&nbsp;Number base chart.</p>

					<p>Some mathematicians and philosophers argue that the reason we use base 10 is that we have ten fingers (digits)&#151;those extraterrestrials we met in Chapter 1 who hear light might also have an extra finger or toe (or sqlurmphragk or something), and to them the millennial year might be the year 1559 (in base 11)! Boy, that will just shock them right out of their fxrmmp!qts!</p>
					<p>What&#146;s important to keep in mind is that it doesn&#146;t much matter which system we use; they&#146;re all pretty much equivalent (1010 base 2 = 10 base 10 = 12 base 8 = A base 16, and so on). We pick numeric bases for convenience: binary systems are useful for switches and logical systems, and computers are essentially composed of lots of switches.</p>
					<p class="figure"><img height="96" width="216" src="../images/chapter2/4bitnumber.jpg"></p>
					
      <p class="figure_caption"><b>Figure 2.12</b>&nbsp;&nbsp;A four-bit binary 
        number (called a <i>nibble</i>). Sixteen is the biggest decimal number 
        we can represent in 4 bits.</p>
					<h2>Numbering Those Bits</h2>
					
      <p>Consider the binary number 0101 from Figure 2.12. It has 4 bits, numbered 
        0 to 3 (computers generally count from 0 instead of 1). The rightmost 
        bit (bit zero, the LSB) is the &quot;ones&quot; bit. If it is <i>set</i> 
        (equal to 1), we add 1 to our number. The next bit is the &quot;twos&quot; 
        bit, and if set adds 2 to our number. Next comes the &quot;fours&quot; 
        bit, and finally the &quot;eights&quot; bit, which, when set, add 4 and 
        8 to our number, respectively. So another way of thinking of the binary 
        number 0101 would be to say that we have zero eights, one four, zero twos, 
        and one 1.</p>
					<div align="center">
						<table width="524" border="0" cellpadding="6" cellspacing="1" bgcolor="#333333">
							<tr>
								<td align="center" bgcolor="white"><b>Bit number</b></td>
								<td align="center" bgcolor="white"><b>2<sup>Bit</sup> number</b></td>
								<td align="center" bgcolor="white"><b>Bit values</b></td>
							</tr>
							<tr>
								<td align="center" bgcolor="white">0</td>
								<td align="center" bgcolor="white">2<sup>0</sup></td>
								<td align="center" bgcolor="white">1</td>
							</tr>
							<tr>
								<td align="center" bgcolor="white">1</td>
								<td align="center" bgcolor="white">2<sup>1</sup></td>
								<td align="center" bgcolor="white">2</td>
							</tr>
							<tr>
								<td align="center" bgcolor="white">2</td>
								<td align="center" bgcolor="white">2<sup>2</sup></td>
								<td align="center" bgcolor="white">4</td>
							</tr>
							<tr>
								<td align="center" bgcolor="white">3</td>
								<td align="center" bgcolor="white">2<sup>3</sup></td>
								<td align="center" bgcolor="white">8</td>
							</tr>
						</table>
					</div>
					<p class="figure_caption"><b>Table 2.2</b>&nbsp;&nbsp;Bits and their values. Note that every bit in <i>base 2</i> is the next power of 2. More generally, every place in a base-<i>n</i> system is <i>n</i> raised to that place number.</p>
					
      <p>Don&#146;t worry about remembering the value of each bit. There&#146;s 
        a simple trick: to find the value of a bit, just raise it to its bit number 
        (and remember to start counting at 0!). What would be the value of bit 
        4 in a five-bit number? If you get confused about this concept, just remember 
        that this is simply what you learned in grade school with base 10 (the 
        1s column, the 10s column, the 100s column, and so on).<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE249D962'));return CSClickReturn()" href="../applets/2_4_binary_beeper.php" target="_blank" csclick="BCE249D962"><img src="../images/global/applet.gif" width="80" height="65" name="2" border="0"></a><br>
						<b>Applet 2.4</b><br>
						<span class="icon_caption">Binary counter</span>
					</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p class="applet_caption">This applet <i>sonifies,</i> or 
        lets you hear a binary counter. Each of the 8 bits is assigned to a different 
        note in the <i>harmonic series</i> (pitches that are integer multiples 
        of the fundamental pitch, corresponding to the spectral components of 
        a periodic waveform). As the binary counter counts, it turns the notes 
        on and off depending on whether or not the bit is set.</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="top" bgcolor="white" width="709">
					<hr>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580">
					<p><br>
        The crucial question is, though, how many different numbers can we represent 
        with four bits? Or more important, how many different numbers can we represent 
        with <i>n</i> bits? That will determine the resolution of our data (and 
        for sound, how accurately we can record minute amplitude changes). Look 
        back at Table 2.1: using all possible combinations of the 4 bits, we were 
        able to represent <i>sixteen</i> numbers (but notice that, starting at 
        0, they only go to 15). What if we only used 2 bits? How about 16? </p>
					<p>Again, thinking of the analogy with base 10, how many numbers can we represent in three &quot;places&quot; (0 through 999 is the answer, or 1,000 different numbers). A more general question is this: for a given base, how many values can be represented with <i>n</i> places (can&#146;t call them bits unless it&#146;s binary)? </p>
					<p>The answer is the base to the <i>n</i><sup>th</sup> power.</p>
					<p>The largest number we can represent (since we need 0) is the base to the <i>n</i><sup>th</sup> power minus 1. For example, the largest number we can represent in binary with <i>n</i> bits is <nobr>2<i><sup>n</sup></i> &#150; 1</nobr>.</p>
					<div align="center">
						<table width="526" border="0" cellpadding="6" cellspacing="1" bgcolor="#333333">
							<tr>
								<td bgcolor="white"><b>Number of bits</b></td>
								<td bgcolor="white"><b>2</b><sup><b>Number of bits</b></sup></td>
								<td bgcolor="white"><b>Number of numbers</b></td>
							</tr>
							<tr>
								<td bgcolor="white">&nbsp;8</td>
								<td bgcolor="white">2<sup>8</sup></td>
								<td bgcolor="white">256</td>
							</tr>
							<tr>
								<td bgcolor="white">16</td>
								<td bgcolor="white">2<sup>16</sup></td>
								<td bgcolor="white">65,536</td>
							</tr>
							<tr>
								<td bgcolor="white">24</td>
								<td bgcolor="white">2<sup>24</sup></td>
								<td bgcolor="white">16,777,216</td>
							</tr>
							<tr>
								<td bgcolor="white">32</td>
								<td bgcolor="white">2<sup>32</sup></td>
								<td bgcolor="white">4,294,967,296</td>
							</tr>
						</table>
					</div>
					<p class="figure_caption"><b>Table 2.3</b> How many numbers can an <i>n</i>-bit number represent? They get big pretty fast, don&#146;t they? In fact, by definition, these numbers get big exponentially as the number of bits increases linearly (like we discussed when we talked about pitch and loudness perception in Section 1.2). <br>
						<br>
						Modern computers use 64-bit numbers! If 2<sup>32</sup> is over 4 billion, try to imagine what 2<sup>64 </sup>is (2<sup>32 </sup>x<sup> </sup>2<sup>32</sup>). It&#146;s a number almost unimaginably large. As an example, if you counted very fast, say five times a second, from 0 to this number, it would take you over 18 quintillion years to get there (you&#146;d need the lifetimes of several solar systems to accomplish this).</p>
					<p>OK, we&#146;ve got that figured out, right? Now back to sound. Remember that a <i>sample</i> is essentially a &quot;snapshot&quot; of the instantaneous amplitude of a sound, and that snapshot is stored as a number. What sort of number are we talking about? 3? 0.00000000017? 16,000,000,126? The answer depends on how accurately we capture the sound and how much space we have to store our data.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_05.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
