<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_06.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="2" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 2: The Digital Representation of Sound,<br>
						Part Two: Playing by the Numbers</h1>
					<h1>Section 2.5: <b>Bit Width</b><br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125"></td>
				<td valign="top" bgcolor="white" width="580"><p>When we talked 
        about sampling, we made the point that the faster you sample, the better 
        the quality. Fast is good: it gives us higher resolution (in time), just 
        like in the old days when the faster your tape recorder moved, the more 
        (horizontal) space it had to put the sound on the tape. But when you had 
        fast tape recorders, moving at 30 or 60 inches per second, you used up 
        a lot of tape. The faster our moving storage, the more media it&#146;s 
        going to consume. While this may be bad ecologically, it&#146;s good sonically. 
        Accuracy in recording demands space. </p>
      <p>However, if we only have a limited amount of storage space, we need to do something about that space issue. One thing that eats up space digitally (in the form of memory or disk space) is <i>bits.</i> The more bits you use, the more hard disk space or memory size you need. That&#146;s also true with sampling rates. If we sample at high rates, we&#146;ll use up more space.</p>
					<p>We could, in principle, use 64-bit numbers (capable of extraordinary detail) and sample at 100 kHz&#151;big numbers, fast speeds. But our sounds, as digitally stored numbers, will be huge. Somehow we have to make some decisions balancing our need for accuracy and sonic quality with our space and storage limitations.</p>
					<p>For example, suppose we only use the values 0, 1, 2, and 3 as sample values. This would mean that every sample measurement would be &quot;rounded off&quot; to one of these four values. On one hand, this would probably be pretty inaccurate, but on the other hand, each sample would then be encoded using only a 2-bit number. Not too consumptive, and pretty simple, technologically! Unfortunately, using only these four numbers would probably mean that sample values won&#146;t be distinguished all that much! That is, most of our functions in the digital world would look pretty much alike. This would create very low resolution data, and the audio ramification is that they would sound terrible. Think of the difference between, for example, 8 mm and 16 mm film: now pretend you are using 1 mm film! That&#146;s what a 4-bit sample size would be like.</p>
					<p>So, while speed is important&#151;the more snapshots we take of a continuous function, the more accurately we can represent it in discrete form&#151;there&#146;s another factor that seriously affects resolution: the resolution of the actual number system we use to store the data. For example, with only three numbers available (say, 0, 1, 2), every value we store has to be one of those three numbers. That&#146;s bad. We&#146;ll basically be storing a bunch of simple square waves. We&#146;ll be turning highly differentiated, continuous data into nondifferentiated, overly discrete data.</p>
					<table width="580" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<p class="figure_flush"><img height="192" width="243" src="../images/chapter2/bits.jpg" vspace="4"></p>
							</td>
							<td>
								<p class="figure_flush"><img height="197" width="256" src="../images/chapter2/more.bits.jpg" vspace="4"></p>
							</td>
						</tr>
						<tr>
							<td valign="top" style="padding-right: 6px">
								<p class="figure_caption_flush"><b>Figure 2.13</b>&nbsp;&nbsp;An example of what a 3-bit sound file might look like (8 possible values).</p>
							</td>
							<td valign="top">
								<p class="figure_caption_flush"><b>Figure 2.14</b>&nbsp;&nbsp;An example of what a 6-bit sound file might look like (64 possible values).</p>
							</td>
						</tr>
					</table>
					<p>In computers, the way we describe numerical resolution is by the size, or number of bits, used for number storage and manipulation. The number of bits used to represent a number is referred to as its <i>bit width</i> or <i>bit depth.</i> Bit width (or depth) and sample speed more or less completely describe the resolution and accuracy of our digital recording and synthesis systems. Another way to think of it is as the word-length, in bits, of the binary data.</p>
					<p>Common bit widths used for digital sound representation are 8, 16, 24, and 32 bits. As we said, more is better: 16 bits gives you much more accuracy than 8 bits, but at a cost of twice the storage space. (Note that, except for 24, they&#146;re all powers of 2. Of course you could think of 24 as halfway between 2<sup>4</sup> and 2<sup>5</sup>, but we like to think of it as 2<sup>4.584962500721156</sup>.) </p>
					<p>We&#146;ll take a closer look at storage in Section 2.7, but for now let&#146;s consider some standard number sizes.</p>
					<div align="center">
						<table border="0" cellpadding="6" cellspacing="1" bgcolor="#333333">
							<tr>
								<td bgcolor="white">4 bits</td>
								<td bgcolor="white">1 nibble</td>
							</tr>
							<tr>
								<td bgcolor="white">8 bits</td>
								<td bgcolor="white">1 byte (2 nibbles)</td>
							</tr>
							<tr>
								<td bgcolor="white">16 bits</td>
								<td bgcolor="white">1 word (2 bytes)</td>
							</tr>
							<tr>
								<td bgcolor="white">1,024 bytes</td>
								<td bgcolor="white">1 kilobyte (K)</td>
							</tr>
							<tr>
								<td bgcolor="white">1,000 K</td>
								<td bgcolor="white">1 megabyte (MB)</td>
							</tr>
							<tr>
								<td bgcolor="white">1,000 MB</td>
								<td bgcolor="white">1 gigabyte (GB)</td>
							</tr>
							<tr>
								<td bgcolor="white">1,000 GB</td>
								<td bgcolor="white">1 terabyte (TB)</td>
							</tr>
						</table>
					</div>
					<p class="figure_caption"><b>Table 2.4</b>&nbsp;&nbsp;Some convenient units for dealing with bits. If you just remember that 8 bits is the same as a byte, you can pretty much figure out the rest.</p>
					<div align="center">
						<table width="523" border="0" cellpadding="6" cellspacing="1" bgcolor="#333333">
							<tr>
								<td bgcolor="white"><b>Sample rate (in Hz)</b></td>
								<td align="center" bgcolor="white"><b>16 bits</b></td>
								<td align="center" bgcolor="white"><b>8 bits</b></td>
							</tr>
							<tr>
								<td bgcolor="white">44,100</td>
								<td bgcolor="white"><a onclick="setSrc('../sounds/chapter2/deb.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button28" border="0" alt=""></a></td>
								<td bgcolor="white"><a onclick="setSrc('../sounds/chapter2/deb.8bit.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button29" border="0" alt=""></a></td>
							</tr>
							<tr>
								<td bgcolor="white">22,050</td>
								<td bgcolor="white"><a onclick="setSrc('../sounds/chapter2/deb.22050.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button30" border="0" alt=""></a></td>
								<td bgcolor="white"><a onclick="setSrc('../sounds/chapter2/deb22050.8bit.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button31" border="0" alt=""></a></td>
							</tr>
							<tr>
								<td bgcolor="white">11,025</td>
								<td bgcolor="white"><a onclick="setSrc('../sounds/chapter2/deb.11025.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button32" border="0" alt=""></a></td>
								<td bgcolor="white"><a onclick="setSrc('../sounds/chapter2/deb11025.8bit.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button33" border="0" alt=""></a></td>
							</tr>
							<tr>
								<td bgcolor="white">5,512.5</td>
								<td bgcolor="white"><a onclick="setSrc('../sounds/chapter2/deb.5512.5.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button34" border="0" alt=""></a></td>
								<td bgcolor="white"><a onclick="setSrc('../sounds/chapter2/deb55125.8bit.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button35" border="0" alt=""></a></td>
							</tr>
						</table>
					</div>
					<p class="figure_caption"><b>Table 2.5</b>&nbsp;&nbsp;Some sound files at various bit widths and sampling rates. Fast and wide are better (and more expensive in terms of technology, computer time, and computer storage). So the ideal format in this table is 44,100 Hz at 16 bits (which is standard audio CD quality). Listen to the sound files and see if you can hear what effect sampling rate and bit width have on the sound.</p>
					<p>You may notice that lower sampling rates make the sound &quot;duller,&quot; or lacking in high frequencies. Lower bit widths make the sound, to use an imprecise word, &quot;flatter&quot;&#151;small amplitude nuances (at the waveform level) cannot be heard as well, so the sound&#146;s timbre tends to become more similar, and less defined.</p>
					<p>Let&#146;s sum it up: what effect does bit width have on the digital storage of sounds? Remember that the more bits we use, the more accurate our recording. But each time our accuracy increases, so do our storage requirements. Sometimes we don&#146;t need to be that accurate&#151;there are lots of options open to us when playing with digital sounds.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_06.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
