<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="../chapter3/03_01.php">Next Chapter --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="4" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 2: The Digital Representation of Sound,<br>
						Part Two: Playing by the Numbers</h1>
					<h1>Section 2.8: Compression<br>
						<br>
					</h1>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<p>When we start 
        talking about taking 44.1 kHz samples per second, each one of those samples 
        has a 16-bit value, so we&#146;re building up a whole heck of a lot of 
        bits. In fact, it&#146;s too many bits for most purposes. While it&#146;s 
        not too wasteful if you want an hour of high-quality sound on a CD, it&#146;s 
        kind of unwieldy if we need to download or send it over the Internet, 
        or store a bunch of it on our home hard drive. Even though high-quality 
        sound data aren&#146;t anywhere near as large as image or video data, 
        they&#146;re still too big to be practical. What can we do to reduce the 
        data explosion?</p>
					<p>If we keep in mind that we&#146;re representing sound as a kind of list of symbols, we just need to find ways to express the same information in a shorter string of symbols. That&#146;s called <i>data compression</i>, and it&#146;s a rapidly growing field dealing with the problems involved in moving around large quantities of bits quickly and accurately. </p>
					<p>The goal is to store the most information in the smallest amount of space, without compromising the quality of the signal (or at least, compromising it as little as possible). Compression techniques and research are not limited to digital sound&#151;data compression plays an essential part in the storage and transmission of all types of digital information, from word-processing documents to digital photographs to full-screen, full-motion videos. As the amount of information in a medium increases, so does the importance of data compression.</p>
					<p>
							What is compression exactly, and how does it work? There is no one thing that is &quot;data compression.&quot; Instead, there are many different approaches, each addressing a different aspect of the problem. We&#146;ll take a look at just a couple of ways to compress digital audio information. What&#146;s important about these different ways of compressing data is that they tend to illustrate some basic ideas in the representation of information, particularly sound, in the digital world.</p>
					<h2>Eliminating Redundancy</h2>
					<p>There are a number of classic approaches to data compression. The first, and most straightforward, is to try to figure out what&#146;s redundant in a signal, leave it out, and put it back in when needed later. Something that is redundant could be as simple as something we already know. For example, examine the following messages:</p>
					<blockquote>
						<p><i>YNK DDL WNT T TWN, RDNG N PNY</i></p>
						<p>or</p>
						<p><i>DNT CNT YR CHCKNS BFR THY HTCH</i></p>
					</blockquote>
					<p>It&#146;s pretty clear that leaving out the vowels makes the phrases shorter, unambiguous, and fairly easy to reconstruct. Other phrases may not be as clear and may need a vowel or two. However, clarity of the intended message occurs only because, in these particular messages, we already know what it says, and we&#146;re simply storing something to jog our memory. That&#146;s not too common.</p>
					<p>Now say we need to store an arbitrary series of colors:</p>
					<table border="0" cellpadding="6" cellspacing="1" bgcolor="#333333">
						<tr>
							<td bgcolor="white"><font size="-1">blue</font></td>
							<td bgcolor="white"><font size="-1">blue</font></td>
							<td bgcolor="white"><font size="-1">blue</font></td>
							<td bgcolor="white"><font size="-1">blue</font></td>
							<td bgcolor="white"><font size="-1">green</font></td>
							<td bgcolor="white"><font size="-1">green</font></td>
							<td bgcolor="white"><font size="-1">green</font></td>
							<td bgcolor="white"><font size="-1">red</font></td>
							<td bgcolor="white"><font size="-1">blue</font></td>
							<td bgcolor="white"><font size="-1">red</font></td>
							<td bgcolor="white"><font size="-1">blue</font></td>
							<td bgcolor="white"><font size="-1">yellow</font></td>
						</tr>
					</table>
					<p>This is easy to shorten to: </p>
					<table border="0" cellpadding="6" cellspacing="1" bgcolor="#333333">
						<tr>
							<td bgcolor="white"><font size="-1">4 blue </font></td>
							<td bgcolor="white"><font size="-1">3 green</font></td>
							<td bgcolor="white"><font size="-1">red</font></td>
							<td bgcolor="white"><font size="-1">blue</font></td>
							<td bgcolor="white"><font size="-1">red</font></td>
							<td bgcolor="white"><font size="-1">blue</font></td>
							<td bgcolor="white"><font size="-1">yellow</font></td>
						</tr>
					</table>
					<p>In fact, we can shorten that even more by saying:</p>
					<table border="0" cellpadding="6" cellspacing="1" bgcolor="#333333">
						<tr>
							<td bgcolor="white"><font size="-1">4 blue </font></td>
							<td bgcolor="white"><font size="-1">3 green</font></td>
							<td bgcolor="white"><font size="-1">2 (red blue)</font></td>
							<td bgcolor="white"><font size="-1">yellow</font></td>
						</tr>
					</table>
					<p>We could shorten it even more, if we know we&#146;re only talking about colors, by:</p>
					<table border="0" cellpadding="6" cellspacing="1" bgcolor="#333333">
						<tr>
							<td bgcolor="white"><font size="-1">4b3g2(rb)y</font></td>
						</tr>
					</table>
					<p>
					We can reasonably guess that &quot;y&quot; means yellow. The &quot;b&quot; is more problematic, since it might mean &quot;brown&quot; or &quot;black,&quot; so we might have to use more letters to resolve its ambiguity. This simple example shows that a reduced set of symbols will suffice in many cases, especially if we know roughly what the message is &quot;supposed&quot; to be. Many complex compression and encoding schemes work in this way.</p>
					<h2><b>Perceptual Encoding</b></h2>
					<p>A second approach to data compression is similar. It also tries to get rid of data that do not &quot;buy us much,&quot; but this time we measure the value of a piece of data in terms of how much it contributes to our overall perception of the sound. </p>
					<p>Here&#146;s a visual analogy: if we want to compress a picture for people or creatures who are color-blind, then instead of having to represent all colors, we could just send black-and-white pictures, which as you can well imagine would require less information than a full-color picture. However, now we are attempting to represent data based on our perception of it. Notice here that we&#146;re not using numbers at all: we&#146;re simply trying to compress all the relevant data into a kind of summary of what&#146;s most important (to the receiver). The tricky part of this is that in order to understand what&#146;s important, we need to <i>analyze</i> the sound into its component features, something that we didn&#146;t have to worry about when simply shortening lists of numbers.</p>
					<div align="center">
						<table border="0" cellspacing="1" cellpadding="0" bgcolor="#333333">
							<tr>
								<td bgcolor="white">
									<table width="522" border="0" cellpadding="6" cellspacing="1">
										<tr>
											<td><b>temperature: 76<sup>o</sup>F</b>
												<p><b>humidity: 35%</b></p>
												<p><b>wind: north-east at 5 MPH</b></p>
												<p><b>barometer: falling</b></p>
												<p><b>clouds: none</b></p>
											</td>
											<td><b>&nbsp;=</b></td>
											<td align="right"><b>&nbsp;It&#146;s a nice day out!</b></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					<p class="figure_caption_flush_bottom"><b>Figure 2.24</b>&nbsp;&nbsp;We humans use perception-based encoding all the time. If we didn't, we&#146;d have very tedious conversations.<br>
						<br>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE24C3E89'));return CSClickReturn()" href="../popups/chapter2/xbit_2_6.php" target="_blank" csclick="BCE24C3E89"><img src="../images/global/popup.gif" width="80" height="65" name="button8" border="0"></a><br>
						<b>Xtra bit 2.6</b><br>
						
						MP3</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter2/128kbps.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button23" border="0" alt=""></a><br>
						<b>Soundfile 2.8</b><br>
						
						128 Kbps</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter2/64kbps.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button22" border="0" alt=""></a><br>
						<b>Soundfile 2.9</b><br>
						
						64 Kbps</p>
					<p class="icon_caption"><a onclick="setSrc('../sounds/chapter2/32kbps.aiff.mp3')"><img src="../images/global/sound.gif" width="80" height="68" name="button25" border="0" alt=""></a><br>
						<b>Soundfile 2.10</b><br>
						
						32 Kbps</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<p>MP3 is the current standard for data compression of sound on the web. But keep in mind that these compression standards change frequently as people invent newer and better methods.</p>

					<p>Soundfiles 2.8, 2.9, and 2.10 were all compressed into the MP3 format but at different bit rates. The lower the bit rate, the more degradation. (<i>Kbps</i> means <i>kilobits per second.</i>)</p>
					
					<p>Perceptually based sound compression algorithms usually work by eliminating numerical information that is not perceptually significant and just keeping what&#146;s important.</p>
					<p><i>&micro;-law</i> (&quot;<i>mu-law</i>&quot;) encoding is a simple, common, and important perception-based compression technique for sound data. It&#146;s an older technique, but it&#146;s far easier to explain here than a more sophisticated algorithm like MP3, so we&#146;ll go into it in a bit of detail. Understanding it is a useful step toward understanding compression in general.</p>
					<p>&micro;-law is based on the principle that our ears are far more sensitive to low amplitude changes than to high ones. That is, if sounds are soft, we tend to notice the change in amplitude more easily than between very loud and other nearly equally loud sounds. &micro;-law compression takes advantage of this phenomenon by mapping 16-bit values onto an 8-bit <i>&micro;-law table</i> like Table 2.6.</p>
					<div align="center">
						<table width="524" border="0" cellpadding="6" cellspacing="1" bgcolor="#333333">
							<tr>
								<td bgcolor="white"><b>0</b></td>
								<td bgcolor="white"><b>8</b></td>
								<td bgcolor="white"><b>16</b></td>
								<td bgcolor="white"><b>24</b></td>
								<td bgcolor="white"><b>32</b></td>
								<td bgcolor="white"><b>40</b></td>
								<td bgcolor="white"><b>48</b></td>
								<td bgcolor="white"><b>56</b></td>
							</tr>
							<tr>
								<td bgcolor="white"><b>64</b></td>
								<td bgcolor="white"><b>72</b></td>
								<td bgcolor="white"><b>80</b></td>
								<td bgcolor="white"><b>88</b></td>
								<td bgcolor="white"><b>96</b></td>
								<td bgcolor="white"><b>104</b></td>
								<td bgcolor="white"><b>112</b></td>
								<td bgcolor="white"><b>120</b></td>
							</tr>
							<tr>
								<td bgcolor="white"><b>132</b></td>
								<td bgcolor="white"><b>148</b></td>
								<td bgcolor="white"><b>164</b></td>
								<td bgcolor="white"><b>180</b></td>
								<td bgcolor="white"><b>196</b></td>
								<td bgcolor="white"><b>212</b></td>
								<td bgcolor="white"><b>228</b></td>
								<td bgcolor="white"><b>244</b></td>
							</tr>
							<tr>
								<td bgcolor="white"><b>260</b></td>
								<td bgcolor="white"><b>276</b></td>
								<td bgcolor="white"><b>292</b></td>
								<td bgcolor="white"><b>308</b></td>
								<td bgcolor="white"><b>324</b></td>
								<td bgcolor="white"><b>340</b></td>
								<td bgcolor="white"><b>356</b></td>
								<td bgcolor="white"><b>372</b></td>
							</tr>
						</table>
					</div>
					<p class="figure_caption"><b>Table 2.6</b>&nbsp;&nbsp;Entries from a typical &micro;-law table. The complete table consists of 256 entries spanning the 16-bit numerical range from &ndash;32,124 to 32,124. Half the range is positive, and half is negative. This is often the way sound values are stored.</p>
					<p>Notice how the range of numbers is divided <i>logarithmically</i> rather than <i>linearly,</i> giving more precision at lower amplitudes. In other words, loud sounds are just loud sounds.</p>
					<p>To encode a &micro;-law sample, we start with a 16-bit sample value, say 330. We then find the entry in the table that is closest to our sample value. In this case, it would be 324, which is the 28th entry (starting with entry 0), so we store 28 as our &micro;-law sample value. Later, when we want to decode the &micro;-law sample, we simply read 28 as an index into the table, and output the value stored there: 324. </p>
					<p>You might be thinking, &quot;Wait a minute, Our original sample value was 330, but now we have a value of 324. What good is that?&quot; While it&#146;s true that we lose some accuracy when we encode &micro;-law samples, we still get much better sound quality than if we had just used regular 8-bit samples. </p>
					<p>Here&#146;s why: in the low-amplitude range of the &micro;-law table, our encoded values are only going to be off by a small margin, since the entries are close together. For example, if our sample value is 3 and it&#146;s mapped to 0, we&#146;re only off by 3. But since we&#146;re dealing with 16-bit samples, which have a total range of 65,536, being off by 3 isn&#146;t so bad. As amplitude increases we can miss the mark by much greater amounts (since the entries get farther and farther apart), but that&#146;s OK too&#151;the whole point of &micro;-law encoding is to exploit the fact that at higher amplitudes our ears are not very sensitive to amplitude changes. Using that fact, &micro;-law compression offers near-16-bit sound quality in an 8-bit storage format.</p>
					<h2><b>Prediction Algorithms</b></h2>
					<p>A third type of compression technique involves attempting to predict what a signal is going to do (usually in the frequency domain, not in the time domain) and only storing the difference between the prediction and the actual value. When a prediction algorithm is well tuned for the data on which it&#146;s used, it&#146;s usually possible to stay pretty close to the actual values. That means that the difference between your prediction and the real value is very small and can be stored with just a few bits.</p>
					<p>Let&#146;s say you have a sample value range of 0 to 65,536 (a 16-bit range, in all positive integers) and you invent a magical prediction algorithm that is never more than 256 units above or below the actual value. You now only need 8 bits (with a range of 0 to 255) to store the difference between your predicted value and the actual value. You might even keep a running average of the actual differences between sample values, and use that adaptively as the range of numbers you need to represent at any given time. Pretty neat stuff! In actual practice, coming up with such a good prediction algorithm is tricky, and what we&#146;ve presented here is an extremely simplified presentation of how prediction-based compression techniques really work.</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="bottom" bgcolor="white" width="125">
					<p class="icon_caption"><a onclick="CSAction(new Array(/*CMP*/'BCE24C8393'));return CSClickReturn()" href="../popups/chapter2/xbit_2_7.php" target="_blank" csclick="BCE24C8393"><img src="../images/global/popup.gif" width="80" height="65" name="button6" border="0"></a><br>
						<b>Xtra bit 2.7</b><br>
						
						<span class="icon_caption">Delta modulation</span><br>
						<br>
						<br>
						<br>
						<br>
					</p>
				</td>
				<td valign="top" bgcolor="white" width="580">
					<h2><b>The Pros and Cons of Compression Techniques</b></h2>
					<p>Each of the techniques we&#146;ve talked about has advantages and disadvantages. Some are time-consuming to compute but accurate; others are simple to compute (and understand) but less powerful. Each tends to be most effective on certain kinds of data. Because of this, many of the actual compression implementations are adaptive&#151;they employ some variable combination of all three techniques, based on qualities of the data to be encoded. </p>
					<p>A good example of a currently widespread adaptive compression technique is the <i>MPEG</i> (Moving Picture Expert Group) standard now used on the Internet for the transmission of both sound and video data. MPEG (which in audio is currently referred to as MP3) is now the standard for high-quality sound on the Internet and is rapidly becoming an audio standard for general use. A description of how MPEG audio really works is well beyond the scope of this book, but it might be an interesting exercise for the reader to investigate further.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="../chapter3/03_01.php">Next Chapter --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
