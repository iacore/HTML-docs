<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>

		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_02.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td rowspan="2" valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td rowspan="2" align="center" valign="top" bgcolor="white" width="125">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
					<h1>Chapter 2: The Digital Representation of Sound,<br>
						Part Two: Playing by the Numbers</h1>
					<h1>Section 2.1: <b>Digital Representation of Sound<br>
							<br>
						</b></h1>
				</td>
			</tr>
			<tr>
				<td valign="top" bgcolor="white" width="580">
					<p>The world is continuous. Time marches on and on, and there are plenty of things that we can measure at any instant. For example, weather forecasters might keep an ongoing record of the temperature or  barometric pressure. If you are in the hospital, the nurses might be keeping a record of your temperature, your heart rate, or your brain waves. Any of these records gives you a function <i>f</i>(<i>t</i>) where, at a given time <i>t</i>, <i>f</i>(<i>t</i>) is the value of the particular statistic that interests you. These sorts of functions are called <i>time series.</i></p>
					<p class="figure"><img height="304" width="500" src="../images/chapter2/time.series.gif"></p>
					<p class="figure_caption"><b>Figure 2.1</b><i>&nbsp;&nbsp;Genetic algorithms</i> (<i>GA</i>) are evolutionary computing systems, which have been applied to things like optimization and machine learning. These are examples of continuous functions. What we mean by &quot;continuous&quot; is that at any instant of time the functions take on a well-defined value, so that they make squiggly line graphs that could be traced without the pencil ever leaving the paper. This might also be called an <i>analog</i> function.<br>
						<br>
						Copyright Juha Haataja/Center for Scientific Computing, Finland</p>
					<p>Of course, the time series that interest us are those that represent sound. In particular, we want to take these time series, stick them on the computer, and play with them!</p>
					<p>Now, if you&#146;ve been paying attention, you may realize that at this moment we&#146;re in a bit of a bind: the type of time series that we&#146;ve been describing is a <i>continuous</i> function. That is, at every instant in time, we could write down a number that is the value of the function at that instant&#151;whether it be how much your eardrum has been displaced, what your temperature is, what your heart rate is, and so on. But such a continuous function would provide an infinite list of numbers (any one of which may have an infinite expansion, like <img src="../images/shared/pi.gif" height="10" width="12"> = 3.1417...), and no matter how big your computer is, you&#146;re going to have a pretty tough time fitting an infinite collection of numbers on your hard drive.</p>
					<p>So how do we do it? How can we represent sound as a finite collection of numbers that can be stored efficiently, in a finite amount of space, on a computer, and played back and manipulated at will? In short, how do we represent sound <i>digitally</i>? That&#146;s the problem that we&#146;ll start to investigate in this chapter. </p>
					
					<p>Somehow we have to come up with a finite list of numbers that does a good job of representing our continuous function. We do it by sampling the original function, at every few instants (at some predetermined rate, called the <i>sampling rate</i>), recording the value of the function at that moment. For example, maybe we only record the temperature every 5 minutes. For sound we need to go a lot faster, and we often use a special device that grabs instantaneous amplitudes at rapid, audio rates (called an <i>analog to digital converter</i>, or ADC).</p>
					<p>A continuous function is also called an <i>analog function</i>, and, to restate the problem, we have to convert analog functions to lists of samples, or <i>digital functions</i>, which is the fundamental way that computers store information. In computers, think of a digital function as a function of location in computer memory. That is, these functions are stored as lists of numbers, and as we read through them we are basically creating a discrete function of time of individual amplitude values.</p>
					<p class="figure"><img height="150" width="279" src="../images/chapter2/ADC.DAC.jpg"></p>
					<p class="figure_caption"><b>Figure 2.2&nbsp;&nbsp;</b>A pictorial description of the recording and playback of sounds through an ADC/DAC.</p>
					<p>In analog to digital conversions, continuous functions (for example, air pressures, sound waves, or voltages) are <i>sampled</i> and stored as numerical values. In digital to analog conversions, numerical values  are <i>interpolated</i> by the converter to force some continuous system (such as amplifiers, speakers, and subsequently the air and our ears) into a continuous vibration. Interpolation just means smoothly transitioning across the discrete numerical values.</p>
					<p>When we digitally record a sound, an image, or even a temperature reading, we numerically represent that phenomenon.</p>
					<p>To convert sounds between our analog world and the digital world of the computer, we use a device called an <i>analog to digital converter</i> (ADC). A <i>digital to analog converter</i> (DAC) is used to convert these numbers back to sound (or to make the numbers usable by an analog device, like a loudspeaker). An ADC takes <i>smooth</i> functions (of the kind found in the physical world) and returns a list of <i>discrete</i> values. A DAC takes a list of <i>discrete</i> values (like the kind found in the computer world) and returns a <i>smooth,</i> continuous function, or more accurately the ability to create such a function from the computer memory or storage medium.</p>
					<p class="figure"><img height="224" width="500" src="../images/chapter2/whistle.time.jpg"></p>
					<div align="center">
						<p class="figure_flush"><img height="414" width="580" src="../images/chapter2/whistle.freq.jpg"></p>
					</div>
					<p class="figure_caption"><b>Figure 2.3&nbsp;&nbsp;</b>Two graphical representations of sound. The top is our usual time domain graph, or <i>audiogram</i>, of the waveform created by a five-note whistled melody. Time is on the <i>x</i>-axis, and amplitude is on the <i>y</i>-axis.<br>
						<br>
						The bottom graph is the same melody, but this time we are looking at a time-frequency representation. The idea here is that if we think of the whistle as made up of contiguous small chunks of sound, then over each small time period the sound is composed of differing amounts of various pieces of frequency. The amount of frequency <i>y</i> at time <i>t</i> is encoded by the brightness of the pixel at the coordinate (<i>t, y</i>). The darker the pixel, the more of that frequency at that time. For example, if you look at time 0.4 you see a band of white, except near 2,500, showing that around that time we mainly hear a pure tone of about 2,500 Hz, while at 0.8 second, there are contributions all around from about 0 Hz to 3,000 Hz, but stronger ones at about 2,500 Hz and 200 Hz.</p>
					<p>We&#146;ll be giving a much more precise description of the <i>frequency domain</i> in Chapter 3, but for now we can simply think of sounds as combinations of more basic sounds that are distinguished according to their &quot;brightness.&quot; We then assign numbers to these basic sounds according to their brightness. The brighter a sound, the higher the number.</p>
					<p>As we learned in Chapter 1, this number is called the <i>frequency,</i> and the basic sound is called a <i>sinusoid</i>, the general term for sinelike waveforms. So, high frequency means high brightness, and low frequency means low brightness (like a deep bass rumble), and in between is, well, simply in between.</p>
					<p>All sound is a combination of these sinusoids, of varying amplitudes. It&#146;s sort of like making soup and putting in a bunch of basic spices: the flavor will depend on how much of each spice you include, and you can change things dramatically when you alter the proportions. The sinusoids are our basic sound spices! The complete description of how much of each of the frequencies is used is called the <i>spectrum</i> of the sound.</p>
					<p>Since sounds change over time, the <i>proportions</i> of each of the sinusoids changes too. Just like when you spice up that soup, as you let it boil the spice proportion may be changing as things evaporate.</p>
					<p></p>
				</td>
			</tr>
		</table>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="02_02.php">Next Section --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
