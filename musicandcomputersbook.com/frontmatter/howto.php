<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="vision.php">The Vision of Mathematics Across the Curriculum --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table width="725" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
                    <h1>How to Use the Web Site</h1>
                    <p>The Web-based text is organized around numerous examples, and the narrative serves as a connecting thread. Students and instructors should use the individual topics as starting points for discussions; possible themes of departure could be musical, technological, and philosophical. You are encouraged to construct your own digital musical instruments using any available computer music system. Because creating and constructing are the most powerful learning strategies, you are expected to apply what you learn by working through the short compositional/technological exercises (in the form of the applets) included with each chapter.</p>
                    <p>While working through the online material, you will encounter three icons:</p>
                    <table width="100%" border="0" cellspacing="2" cellpadding="6">
                        <tr>
                            <td><img src="../images/global/applet.gif" alt="" height="65" width="80" align="absmiddle" border="0"></td>
                            <td>Applets for exploring interactive sound-making modules and images.</td>
                        </tr>
                        <tr>
                            <td><img src="../images/global/popup.gif" alt="" width="80" height="65" border="0" align="absmiddle"></td>
                            <td>Xtra bits for learning more about a particular topic.</td>
                        </tr>
                        <tr>
                            <td><img src="../images/global/sound.gif" alt="" width="80" height="68" border="0" align="absmiddle"></td>
                            <td>Sound files for hearing musical examples of the material described.</td>
                        </tr>
                    </table>
                    <p>Although the Web-based material is best used sequentially, the first two chapters, the third chapter, and the last two chapters could function as three independent units, which might be used in parallel. That is, teaching acoustics and basic computer ideas might (and ought to) be taught while teaching basic synthesis techniques, which are closely derived from computational and psychoacoustic precepts. Similarly, the third chapter, covering the basics of DSP and Fourier analysis, is not necessarily a prerequisite to making computer music, or even to understanding most of the material in the later chapters. However, taught in parallel, this important mathematically- oriented material will greatly enhance the student&rsquo;s understanding of computer music and its tools.</p>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
