<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="intro.php">Introduction --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table width="725" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
                    <h1>The Vision of Mathematics Across the Curriculum</h1>
                    <p>Dear Reader,</p>
                    <p>In 1994, Dartmouth College received a generous grant from the National Science Foundation to integrate mathematics throughout the undergraduate college curriculum in a five-year project, Mathematics Across the Curriculum (MATC). The project has involved over 40 faculty members from Dartmouth and various other colleges and universities representing departments of biology, chemistry, music, drama, English, art history, computer science, physics, earth science, economics, engineering, medicine, mathematics, and Spanish, producing lesson plans, short books, videotapes, and a Web site with images and texts. The series of volumes published by Key College Publishing represents some of the best of the MATC collection.</p>
                    <p>These materials will make it easier for students to become more quantitatively literate as they tackle complex, real-world problems that must be approached through the door of mathematics. We hope that you, the reader, will appreciate our efforts to place the mathematics in this book completely in the context of your field of interest. Our goal is to help you see that applied mathematics is a powerful form of inquiry, and ever so much richer than mere &ldquo;word problems.&rdquo; We trust that you will like this approach and want to explore some of the other volumes in the series.</p>
                    <p>Sincerely,</p>
                    <p>Dorothy Wallace<br>
                        Professor of Mathematics<br>
                        Principal Investigator: Mathematics Across the Curriculum Project<br>
                        Dartmouth College</p>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
