<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="preface.php">Preface and Acknowledgments --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table width="725" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">


                    <h1>Preface to the Archival Version (Spring, 2011)</h1>

                    <p>
			Growing out of classes in computer music at Dartmouth College in the early 1990s, the text <i>Music and
			Computers</i> was begun by Larry Polansky and Douglas Repetto in 1997 as part of an NSF Math Across the
			Curriculum (MATC) grant at Dartmouth College.  Dan Rockmore, Mary Roberts, and Phil Burk joined as
			co-authors in 1998, and together we self-published the book online as a freely available resource,
			and for use in classes co-taught by Polansky and Rockmore. In 2002 Key College Publishing purchased
			the rights to this web-book, and <i>Music and Computers</i> was only available as a commercial website for
			several years. In 2008 Key College Publishing returned the rights to the book to the authors, and we
			have decided to once again make it a freely available resource.
		    </p>
		    <p>
			We have reorganized and updated some minor aspects of the book for this current version. We hope it is
			useful and interesting.
		    </p>
		    <p>
			<br>
				+++
				<br><br>
			The commercial version of the online book came with a printed Teaching Guide. We have uploaded a
			scanned copy of the guide:
			<a href="../MusicAndComputers_TeachingGuide.pdf">
				MusicAndComputers_TeachingGuide.pdf (8.4MB pdf)</a>
		    </p>

<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
