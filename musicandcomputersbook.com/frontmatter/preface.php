<html>
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-750748-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-750748-8');
        </script>
		
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Music and Computers</title>
		<link href="../css/styles.css" rel="stylesheet" media="screen">
		<link rel="alternate stylesheet" type="text/css" href="../css/styles_MS.css" title="Manuscript">
    </head>

	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="white" text="black" link="blue" alink="#000066" vlink="blue">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#99cc99">
			<tr>
			<td><a href="../index.html"><img src="../images/global/headFM.jpg" alt="" width="780" height="80" border="0"></a></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ccff33">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
            </tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="#99cc33">
			<tr>
			<td width="299"></td>
			<td align="right" width="387"></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#669933">
			<tr>
			<td><img border="0" height="2" width="150" src="../images/global/spacer.gif"></td>
			</tr>
		</table>
		<script src='../_components/audio-player.min.js'></script>
			<audio-player id='audio-player'></audio-player>
		<table width="745" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="small_nav" valign="bottom" nowrap><a href="javascript:window.history.go(-1);">&lt;-- Back to Previous Page</a></td>
				<td class="small_nav" aligh="center" valign="bottom" nowrap>
				<a href="../index.html">TOC</a></td>
				<td class="small_nav" align="right" valign="bottom" nowrap><a href="howto.php">How to Use the Web Site --&gt;</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="../images/global/grey_dot.gif" alt="" width="100%" height="2" border="0"></td>
			</tr>
		</table>
		<br>
		<table width="725" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td valign="top" bgcolor="white" width="20">&nbsp;</td>
				<td valign="top" bgcolor="white" width="580">
	  	

                    <h1>Preface</h1>

                    <p>This Web-based course is intended as a &ldquo;user-friendly&rdquo; introduction to music and computers for college and upper-division secondary students. By organizing a presentation around interactive visual and sonic examples, we hope to provide a resource and guide for those just beginning to look at the field of computer music, as well as for more advanced computer composers who might benefit from a fresh insight. Our material can be used as a text for a one term or semester course, or as a module inside of a larger curriculum.</p>
                    <p>We have designed a flexible music synthesis learning environment that can accommodate a broad scope of curricular settings; including courses in contemporary music, composition, music technology, mathematics, computer science, and interdisciplinary studies. The many platform-independent interactive examples and audio aids stimulate the learner&rsquo;s creative spirit while the multi-level approach (which contains plenty of optional tangents for in-depth mathematical studies) should cover all the bases for a comprehensive study.</p>
                    <p>Our personalized approach to generating most of the audio examples has been a homegrown one; we have worked to provide examples that can be easily replicated by laypersons who may have a minimal, yet working knowledge of a computer music language. We encourage all users of the Web-based text to take advantage of our sound-making modules and use them and tweak them in any way they might imagine.</p>
                    <p>Our applets are written in Phil Burk&rsquo;s JSyn and Java in another attempt to again provide a platform-independent environment for designing interactive computer music examples. As instructors, we use a number of computer and music programs and platforms (SuperCollider, Csound, Max/MSP, Soundhack, JSyn, and others) but we were reluctant to make any one of these a prerequisite for using this program. We of course encourage the instructors using this material to motivate their students to actually compose computer music, either by programming it themselves or by using higher-level software tools. This Web-based text is meant to be used in conjunction with that kind of hands-on activity. There is no substitute for doing it yourself!</p>
                    <p>One of the most difficult aspects of studying computer music is the rapid evolution and eventual extinction of software and hardware platforms&#151;a familiar, yet frustrating aspect of the digital work environment. These developments are implicit for advancements in compositional tools, but difficulties crop up for keeping documentation current, especially with textbooks. The best standard texts (particularly those mentioned below by Roads and Dodge and Jerse) have elegantly avoided this problem by correctly focusing on ideas rather than software implementation, and we have followed suit.</p>
                    <p>Although we have created a great many of our examples in some of the most current, popular, and useful software (particularly those previously mentioned), we have no doubt that within a few years there will be a completely new set of standard tools. We also have no doubt that most of the fundamental ideas presented here will still be applicable, as they have been for some time now. We encourage students and instructors to find appropriate technology for their own compositions, and to avoid the kind of techno-consumer partisanship that so often inhibits real artistic and intellectual growth.</p>
                    <p>The important and advanced texts in the field of computer music, notably <i>Computer Music</i> by Charles Dodge and Thomas Jerse&rsquo;s and <i>Computer Music Tutorial</i> Curtis Roads, present comprehensive and in-depth studies of computer-generated sounds and related disciplines. We hope that our shorter, less technical work will lead the interested student and instructor to these essential works. We want our book will serve as the doorway, rather than the room itself.</p>
<p></p>
                    <h1><a name="Anchor-Acknowledgments-47857" id="Anchor-Acknowledgments-47857"></a>Acknowledgments</h1>
                    <p>Work on this text was made possible by a National Science Foundation Grant for Mathematics Across the Curriculum (MATC) at Dartmouth College. Many people at Dartmouth College have made invaluable contributions to the development of this book, including: Marcia Groszek and Dorothy Wallace of the Mathematics Department; Jon Appleton, Director of the Graduate Program in Electroacoustic Music and Co-Director of the Bregman Electronic Music Studio; and Claude Poux and Kim V. Rheinlander of the MATC Program. We would also like to thank our colleagues and the graduate students at the Bregman Studio for contributing in innumerable ways to this work: Charles Dodge, Dee Copley, Colby Leider, Lesley Stone, and Matthew Smith.</p>
                    <p>We would like to thank the staff at Key College Publishing for their work on the now-defunct commercial version of this web book. Particularly, we would like to thank the reviewers solicited by Key to read the manuscript: James Bohn, University of Massachusetts-Dartmouth; Tim Koozin, University of Houston; Gary Lee Nelson, TIMARA Department, Oberlin College; and Eleanor Trawick, Ball State University.</p>
                    <p>Over the past several years author Larry Polansky has been teaching an interdisciplinary computer music course at Dartmouth College. We owe a great deal of appreciation to the folks who have helped to co-teach this interdisciplinary class: Dennis Healy and Charles Owen, both of the Dartmouth Mathematics and Computer Science Department. These indispensable and valued colleagues have made deep contributions to the collection of ideas presented here.</p>


<p></p>
<p class="copyright">&copy;Burk/Polansky/Repetto/Roberts/Rockmore. All rights reserved.</p>
                </td>
				<td align="right" valign="top" bgcolor="white" width="125">&nbsp;</td>
			</tr>
		</table>
		<br>
    <script src='../_components/audioPlayerHelper.js'></script>
	</body>

</html>
