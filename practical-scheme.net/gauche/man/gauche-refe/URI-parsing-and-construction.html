<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>URI parsing and construction (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="URI parsing and construction (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="URI parsing and construction (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Library-modules-_002d-Utilities.html" rel="up" title="Library modules - Utilities">
<link href="UUID.html" rel="next" title="UUID">
<link href="Transport-layer-security.html" rel="prev" title="Transport layer security">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="URI-parsing-and-construction">
<div class="header">
<p>
Next: <a href="UUID.html" accesskey="n" rel="next"><code>rfc.uuid</code> - UUID</a>, Previous: <a href="Transport-layer-security.html" accesskey="p" rel="prev"><code>rfc.tls</code> - Transport layer security</a>, Up: <a href="Library-modules-_002d-Utilities.html" accesskey="u" rel="up">Library modules - Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="rfc_002euri-_002d-URI-parsing-and-construction"></span><h3 class="section">12.55 <code>rfc.uri</code> - URI parsing and construction</h3>

<dl class="def">
<dt id="index-rfc_002euri-1"><span class="category">Module: </span><span><strong>rfc.uri</strong><a href='#index-rfc_002euri-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-rfc_002euri"></span>
<p>Provides a set of procedures to parse and construct Uniform Resource Identifiers
defined in RFC 2396 (<a href="https://www.ietf.org/rfc/rfc2396.txt">https://www.ietf.org/rfc/rfc2396.txt</a>),
as well as Data URI scheme defined in RFC2397.
</p></dd></dl>

<p>First, lets review the structure of URI briefly.
The following graph shows how the URI is constructed:
</p>
<div class="example">
<pre class="example">URI-+-scheme
    |
    +-specific--+--authority-+--userinfo
                |            +--host
                |            +--port
                +--path
                +--query
                +--fragment
</pre></div>

<p>Not all URIs have this full hierarchy.  For example,
<code>mailto:admin@example.com</code> has only <em>scheme</em> (<code>mailto</code>)
and <em>specific</em> (<code>admin@example.com</code>) parts.
</p>
<p>Most popular URI schemes, however, organize resources
in a tree, so they adopt <em>authority</em> (which usually identifies
the server) and the hierarchical <em>path</em>.  In the URI
<code>http://example.com:8080/search?q=key#results</code>, the authority
part is <code>example.com:8080</code>, the path is <code>/search</code>,
the query is <code>key</code> and the fragment is <code>results</code>.
The userinfo can be provided before hostname, such as <code>anonymous</code>
in <code>ftp://anonymous@example.com/pub/</code>.
</p>
<p>We have procedures that decompose a URI into those parts,
and that compose a URI from those parts.
</p>
<span id="Parsing-URI"></span><h4 class="subheading">Parsing URI</h4>

<dl class="def">
<dt id="index-uri_002dref"><span class="category">Function: </span><span><strong>uri-ref</strong> <em>uri parts</em><a href='#index-uri_002dref' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>rfc.uri</tt>}
Extract specific part(s) from the given URI.  You can fully
decompose URI by the procedures described below, but in actual
applications, you often need only some of the parts.  This procedure
comes handy for it.
</p>
<p>The <var>parts</var> argument may be a symbol, or a list of symbols,
to name the desired parts.  The recognized symbos are as follows.
</p>
<dl compact="compact">
<dt><span><code>scheme</code></span></dt>
<dd><p>The scheme part, as string.
</p></dd>
<dt><span><code>authority</code></span></dt>
<dd><p>The authority part, as string.
If URI doesn&rsquo;t have the part, <code>#f</code>.
</p></dd>
<dt><span><code>userinfo</code></span></dt>
<dd><p>The userinfo part, as string.  If URI doesn&rsquo;t have the part, <code>#f</code>.
</p></dd>
<dt><span><code>host</code></span></dt>
<dd><p>The host part, as string.  If URI doesn&rsquo;t have the part, <code>#f</code>.
</p></dd>
<dt><span><code>port</code></span></dt>
<dd><p>The port part, as integer.  If URI doesn&rsquo;t have the part, <code>#f</code>.
</p></dd>
<dt><span><code>path</code></span></dt>
<dd><p>The path part, as string.  If URI isn&rsquo;t hierarchical, this returns
the specific part.
</p></dd>
<dt><span><code>query</code></span></dt>
<dd><p>The query part, as string. If URI doesn&rsquo;t have the part, <code>#f</code>.
</p></dd>
<dt><span><code>fragment</code></span></dt>
<dd><p>The fragment part, as string.  If URI doesn&rsquo;t have the part, <code>#f</code>.
</p></dd>
<dt><span><code>scheme+authority</code></span></dt>
<dd><p>The scheme and authority part.
</p></dd>
<dt><span><code>host+port</code></span></dt>
<dd><p>The host and port part.
</p></dd>
<dt><span><code>userinfo+host+port</code></span></dt>
<dd><p>The userinfo, host and port part.
</p></dd>
<dt><span><code>path+query</code></span></dt>
<dd><p>The path and query part.
</p></dd>
<dt><span><code>path+query+fragment</code></span></dt>
<dd><p>The path, query and fragment part.
</p></dd>
</dl>

<div class="example">
<pre class="example">(define uri &quot;http://foo:bar@example.com:8080/search?q=word#results&quot;)

(uri-ref uri 'scheme)             &rArr; &quot;http&quot;
(uri-ref uri 'authority)          &rArr; &quot;//foo:bar@example.com:8080/&quot;
(uri-ref uri 'userinfo)           &rArr; &quot;foo:bar&quot;
(uri-ref uri 'host)               &rArr; &quot;example.com&quot;
(uri-ref uri 'port)               &rArr; 8080
(uri-ref uri 'path)               &rArr; &quot;/search&quot;
(uri-ref uri 'query)              &rArr; &quot;q=word&quot;
(uri-ref uri 'fragment)           &rArr; &quot;results&quot;
(uri-ref uri 'scheme+authority)   &rArr; &quot;http://foo:bar@example.com:8080/&quot;
(uri-ref uri 'host+port)          &rArr; &quot;example.com:8080&quot;
(uri-ref uri 'userinfo+host+port) &rArr; &quot;foo:bar@example.com:8080&quot;
(uri-ref uri 'path+query)         &rArr; &quot;/search?q=word&quot;
(uri-ref uri 'path+query+fragment)&rArr; &quot;/search?q=word#results&quot;
</pre></div>

<p>You can extract multiple parts at once by specifying a list of parts.
A list of parts is returned.
</p>
<div class="example">
<pre class="example">(uri-ref uri '(host+port path+query))
  &rArr; (&quot;example.com:8080&quot; &quot;/search?q=word&quot;)
</pre></div>
</dd></dl>


<dl class="def">
<dt id="index-uri_002dparse"><span class="category">Function: </span><span><strong>uri-parse</strong> <em>uri</em><a href='#index-uri_002dparse' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-uri_002dscheme_0026specific"><span class="category">Function: </span><span><strong>uri-scheme&amp;specific</strong> <em>uri</em><a href='#index-uri_002dscheme_0026specific' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-uri_002ddecompose_002dhierarchical"><span class="category">Function: </span><span><strong>uri-decompose-hierarchical</strong> <em>specific</em><a href='#index-uri_002ddecompose_002dhierarchical' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-uri_002ddecompose_002dauthority"><span class="category">Function: </span><span><strong>uri-decompose-authority</strong> <em>authority</em><a href='#index-uri_002ddecompose_002dauthority' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>rfc.uri</tt>}
General parser of URI.  These functions does not decode
URI encoding, since the parts to be decoded differ among
the uri schemes.   After parsing uri, use <code>uri-decode</code> below
to decode them.
</p>
<p><code>uri-parse</code> is the most handy procedure.  It breaks the uri
into the following parts and returns them as multiple values.
If the uri doesn&rsquo;t have the corresponding
parts, <code>#f</code> are returned for the parts.
</p>
<ul>
<li> URI scheme as a string
(e.g. <code>&quot;mailto&quot;</code> in <code>&quot;mailto:foo@example.com&quot;</code>).
</li><li> User-info in the authority part (e.g. <code>&quot;anonymous&quot;</code>
in <code>ftp://anonymous@ftp.example.com/pub/foo</code>).
</li><li> Hostname in the authority part (e.g. <code>&quot;ftp.example.com&quot;</code>
in <code>ftp://anonymous@ftp.example.com/pub/foo</code>).
</li><li> Port number in the authority part, as an integer (e.g. <code>8080</code>
in <code>http://www.example.com:8080/</code>).
</li><li> Path part (e.g. <code>&quot;/index.html&quot;</code> in
<code>http://www.example.com/index.html</code>).
</li><li> Query part (e.g. <code>&quot;key=xyz&amp;lang=en&quot;</code> in
<code>http://www.example.com/search?key=xyz&amp;lang=en</code>).
</li><li> Fragment part (e.g. <code>&quot;section4&quot;</code> in
<code>http://www.example.com/document.html#section4</code>).
</li></ul>

<p>The following procedures are finer grained and break up
uris with different stages.
</p>
<p><code>uri-scheme&amp;specific</code> takes a URI <var>uri</var>, and
returns two values, its scheme part and its scheme-specific part.
If <var>uri</var> doesn&rsquo;t have a scheme part, <code>#f</code> is returned for it.
</p><div class="example">
<pre class="example">(uri-scheme&amp;specific &quot;mailto:sclaus@north.pole&quot;)
  &rArr; &quot;mailto&quot; <span class="roman">and</span> &quot;sclaus@north.pole&quot;
(uri-scheme&amp;specific &quot;/icons/new.gif&quot;)
  &rArr; #f <span class="roman">and</span> &quot;/icons/new.gif&quot;
</pre></div>

<p>If the URI scheme uses hierarchical notation, i.e.
&ldquo;<code>//<var>authority</var>/<var>path</var>?<var>query</var>#<var>fragment</var></code>&rdquo;,
you can pass
the scheme-specific part to <code>uri-decompose-hierarchical</code>
and it returns four values, <var>authority</var>, <var>path</var>, <var>query</var>
and <var>fragment</var>.
</p><div class="example">
<pre class="example">(uri-decompose-hierarchical &quot;//www.foo.com/about/company.html&quot;)
  &rArr; &quot;www.foo.com&quot;<span class="roman">,</span> &quot;/about/company.html&quot;<span class="roman">,</span> #f <span class="roman">and</span> #f
(uri-decompose-hierarchical &quot;//zzz.org/search?key=%3fhelp&quot;)
  &rArr; &quot;zzz.org&quot;<span class="roman">,</span> &quot;/search&quot;<span class="roman">,</span> &quot;key=%3fhelp&quot; <span class="roman">and</span> #f
(uri-decompose-hierarchical &quot;//jjj.jp/index.html#whatsnew&quot;)
  &rArr; &quot;jjj.jp&quot;<span class="roman">,</span> &quot;/index.html&quot;<span class="roman">,</span> #f <span class="roman">and</span> &quot;whatsnew&quot;
(uri-decompose-hierarchical &quot;my@address&quot;)
  &rArr; #f<span class="roman">,</span> #f<span class="roman">,</span> #f <span class="roman">and</span> #f
</pre></div>

<p>Furthermore, you can parse <var>authority</var> part of the
hierarchical URI by <code>uri-decompose-authority</code>.
It returns <var>userinfo</var>, <var>host</var> and <var>port</var>.
</p><div class="example">
<pre class="example">(uri-decompose-authority &quot;yyy.jp:8080&quot;)
  &rArr; #f<span class="roman">,</span> &quot;yyy.jp&quot; <span class="roman">and</span> &quot;8080&quot;
(uri-decompose-authority &quot;[::1]:8080&quot;)  ;<span class="roman">(IPv6 host address)</span>
  &rArr; #f<span class="roman">,</span> &quot;::1&quot; <span class="roman">and</span> &quot;8080&quot;
(uri-decompose-authority &quot;mylogin@yyy.jp&quot;)
  &rArr; &quot;mylogin&quot;<span class="roman">,</span> &quot;yyy.jp&quot; <span class="roman">and</span> #f
</pre></div>
</dd></dl>


<dl class="def">
<dt id="index-uri_002ddecompose_002ddata"><span class="category">Function: </span><span><strong>uri-decompose-data</strong> <em>uri</em><a href='#index-uri_002ddecompose_002ddata' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>rfc.uri</tt>}
Parse a Data URI string <var>uri</var>.  You can either pass the entire
uri including <code>data:</code> scheme part, or just the specific part.
If the passed uri is invalid as a data uri, an error is signalled.
</p>
<p>Returns two values: parsed content type and the decoded data.
The data is a string if the content type is <code>text/*</code>, and
a u8vector otherwise.
</p>
<p>The content-type is parsed by <code>mime-parse-content-type</code>
(see <a href="MIME-message-handling.html"><code>rfc.mime</code> - MIME message handling</a>).  The result format is a list as follows:
</p>
<div class="example">
<pre class="example"><code>(<i>type</i> <i>subtype</i> (<i>attribute</i> . <i>value</i>) &hellip;)</code>.
</pre></div>

<p>Here are a couple of examples:
</p>
<div class="example">
<pre class="example">(uri-decompose-data
 &quot;data:text/plain;charset=utf-8;base64,KGhlbGxvIHdvcmxkKQ==&quot;)
  &rArr; (&quot;text&quot; &quot;plain&quot; (&quot;charset&quot; . &quot;utf-8&quot;)) <span class="roman">and</span> &quot;(hello world)&quot;

(uri-decompose-data
 &quot;data:application/octet-stream;base64,AAECAw==&quot;)
  &rArr; (&quot;application&quot; &quot;octet-stream&quot;) <span class="roman">and</span> #u8(0 1 2 3)
</pre></div>

</dd></dl>


<span id="Constructing-URI"></span><h4 class="subheading">Constructing URI</h4>

<dl class="def">
<dt id="index-uri_002dcompose"><span class="category">Function: </span><span><strong>uri-compose</strong> <em>:key scheme userinfo host port authority path path* query fragment specific</em><a href='#index-uri_002dcompose' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>rfc.uri</tt>}
Compose a URI from given components.
There can be various combinations of components to create a valid
URI&mdash;the following diagram shows the possible &rsquo;paths&rsquo; of
combinations:
</p>
<div class="example">
<pre class="example">        /-----------------specific-------------------\
        |                                            |
 scheme-+------authority-----+-+-------path*---------+-
        |                    | |                     |
        \-userinfo-host-port-/ \-path-query-fragment-/
</pre></div>

<p>If <code>#f</code> is given to a keyword argument, it is
equivalent to the absence of that keyword argument.
It is particularly useful to pass the results of
parsed uri.
</p>
<p>If a component contains a character that is not appropriate
for that component, it must be properly escaped before
being passed to <code>url-compose</code>.
</p>
<p>Some examples:
</p><div class="example">
<pre class="example">(uri-compose :scheme &quot;http&quot; :host &quot;foo.com&quot; :port 80
             :path &quot;/index.html&quot; :fragment &quot;top&quot;)
  &rArr; &quot;http://foo.com:80/index.html#top&quot;

(uri-compose :scheme &quot;http&quot; :host &quot;foo.net&quot;
             :path* &quot;/cgi-bin/query.cgi?keyword=foo&quot;)
  &rArr; &quot;http://foo.net/cgi-bin/query.cgi?keyword=foo&quot;

(uri-compose :scheme &quot;mailto&quot; :specific &quot;a@foo.org&quot;)
  &rArr; &quot;mailto:a@foo.org&quot;

(receive (authority path query fragment)
   (uri-decompose-hierarchical &quot;//foo.jp/index.html#whatsnew&quot;)
 (uri-compose :authority authority :path path
              :query query :fragment fragment))
  &rArr; &quot;//foo.jp/index.html#whatsnew&quot;
</pre></div>
</dd></dl>


<dl class="def">
<dt id="index-uri_002dmerge"><span class="category">Function: </span><span><strong>uri-merge</strong> <em>base-uri relative-uri relative-uri2 &hellip;</em><a href='#index-uri_002dmerge' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>rfc.uri</tt>}
Arguments are strings representing
full or part of URIs.  This procedure resolves <var>relative-uri</var>
in relative to <var>base-uri</var>, as defined in RFC3986 Section 5.2.
&ldquo;Relative Resolution&rdquo;.
</p>
<p>If more <var>relative-uri2</var>s are given, first <var>relative-uri</var>
is merged to <var>base-uri</var>, then the next argument is merged
to the resulting uri, and so on.
</p>
<div class="example">
<pre class="example">(uri-merge &quot;http://example.com/foo/index.html&quot; &quot;a/b/c&quot;)
 &rArr; &quot;http://example.com/foo/a/b/c&quot;

(uri-merge &quot;http://example.com/foo/search?q=abc&quot; &quot;../about#me&quot;)
 &rArr; &quot;http://example.com/about#me&quot;

(uri-merge &quot;http://example.com/foo&quot; &quot;http://example.net/bar&quot;)
 &rArr; &quot;http://example.net/bar&quot;

(uri-merge &quot;http://example.com/foo/&quot; &quot;q&quot; &quot;?xyz&quot;)
 &rArr; &quot;http://example.com/foo/q?xyz&quot;
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-uri_002dcompose_002ddata"><span class="category">Function: </span><span><strong>uri-compose-data</strong> <em>data :key content-type encoding</em><a href='#index-uri_002dcompose_002ddata' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>rfc.uri</tt>}
Creates a Data URI of the given <var>data</var>, with specified content-type
and transfer encoding.  Returns a string.
</p>
<p>The <var>data</var> argument must be a string or a u8vector.
</p>
<p>The <var>content-type</var> argument can be <code>#f</code> (default),
a string that represents a content type (e.g. <code>&quot;text/plain;charset=utf-8&quot;</code>),
or a list form of parsed content type
(e.g. <code>(&quot;application&quot; &quot;octet-stream&quot;)</code>.  If it is <code>#f</code>,
<code>text/plain</code> with the gauche&rsquo;s native character encoding is
used when <var>data</var> is a complete string, and <code>application/octet-stream</code>
is used otherwise.
</p>
<p>The <var>encoding</var> argument can be either <code>#f</code> (default),
or a symbol <code>uri</code> or <code>base64</code>.  This is for transfer encoding,
not character encoding.  If it is <code>#f</code>, URI encoding is used
for text data and base64 encoding is used for binary data.
</p>
<div class="example">
<pre class="example">(uri-compose-data &quot;(hello world)&quot;)
 &rArr; &quot;data:text/plain;charset=utf-8,%28hello%20world%29&quot;

(uri-compose-data &quot;(hello world)&quot; :encoding 'base64)
 &rArr; &quot;data:text/plain;charset=utf-8;base64,KGhlbGxvIHdvcmxkKQ==&quot;

(uri-compose-data '#u8(0 1 2 3))
 &rArr; &quot;data:application/octet-stream;base64,AAECAw==&quot;
</pre></div>
</dd></dl>


<span id="URI-Encoding-and-decoding"></span><h4 class="subheading">URI Encoding and decoding</h4>

<dl class="def">
<dt id="index-uri_002ddecode"><span class="category">Function: </span><span><strong>uri-decode</strong> <em>:key :cgi-decode</em><a href='#index-uri_002ddecode' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-uri_002ddecode_002dstring"><span class="category">Function: </span><span><strong>uri-decode-string</strong> <em>string :key :cgi-decode :encoding</em><a href='#index-uri_002ddecode_002dstring' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>rfc.uri</tt>}
Decodes &ldquo;URI encoding&rdquo;, i.e. <code>%</code>-escapes.
<code>uri-decode</code> takes input from the current input port,
and writes decoded result to the current output port.
<code>uri-decode-string</code> takes input from <var>string</var> and
returns decoded string.
</p>
<p>If <var>cgi-decode</var> is true, also replaces <code>+</code> to a space character.
</p>
<p>To <code>uri-decode-string</code> you can provide the external character
encoding by the <var>encoding</var> keyword argument.  When it is given,
the decoded octet sequence is assumed to be in the specified encoding
and converted to the Gauche&rsquo;s internal character encoding.
</p></dd></dl>

<dl class="def">
<dt id="index-uri_002dencode"><span class="category">Function: </span><span><strong>uri-encode</strong> <em>:key :noescape</em><a href='#index-uri_002dencode' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-uri_002dencode_002dstring"><span class="category">Function: </span><span><strong>uri-encode-string</strong> <em>string :key :noescape :encoding</em><a href='#index-uri_002dencode_002dstring' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>rfc.uri</tt>}
Encodes unsafe characters by <code>%</code>-escape.  <code>uri-encode</code>
takes input from the current input port and writes the result to
the current output port.  <code>uri-encode-string</code> takes input
from <var>string</var> and returns the encoded string.
</p>
<p>By default, characters that are not specified &ldquo;unreserved&rdquo; in
RFC3986 are escaped.  You can pass different character
set to <var>noescape</var> argument to keep from being encoded.
For example, the older RFC2396 has several more &ldquo;unreserved&rdquo;
characters, and passing <code>*rfc2396-unreserved-char-set*</code> (see below)
prevents those characters from being escaped.
</p>
<p>The multibyte characters are encoded as the octet stream of Gauche&rsquo;s
native multibyte representation by default.  However, you can pass
the <code>encoding</code> keyword argument to <code>uri-encode-string</code>,
to convert <var>string</var> to the specified character encoding.
</p></dd></dl>

<dl class="def">
<dt id="index-_002arfc2396_002dunreserved_002dchar_002dset_002a"><span class="category">Constant: </span><span><strong>*rfc2396-unreserved-char-set*</strong><a href='#index-_002arfc2396_002dunreserved_002dchar_002dset_002a' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_002arfc3986_002dunreserved_002dchar_002dset_002a"><span class="category">Constant: </span><span><strong>*rfc3986-unreserved-char-set*</strong><a href='#index-_002arfc3986_002dunreserved_002dchar_002dset_002a' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>rfc.uri</tt>}
These constants are bound to character sets that represents
&ldquo;unreserved&rdquo; characters defined in RFC2396 and RFC3986, respectively.
(See <a href="Character-sets.html#Character-sets">Character Sets</a>, and <a href="R7RS-large.html#R7RS-character-sets"><code>scheme.charset</code> - R7RS character sets</a>, for
operations on character sets).
</p></dd></dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="UUID.html" accesskey="n" rel="next"><code>rfc.uuid</code> - UUID</a>, Previous: <a href="Transport-layer-security.html" accesskey="p" rel="prev"><code>rfc.tls</code> - Transport layer security</a>, Up: <a href="Library-modules-_002d-Utilities.html" accesskey="u" rel="up">Library modules - Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
