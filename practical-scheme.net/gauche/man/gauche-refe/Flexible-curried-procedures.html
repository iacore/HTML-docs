<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Flexible curried procedures (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Flexible curried procedures (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Flexible curried procedures (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Library-modules-_002d-SRFIs.html" rel="up" title="Library modules - SRFIs">
<link href="Combinators-_0028SRFI_0029.html" rel="next" title="Combinators (SRFI)">
<link href="Tagged-procedures.html" rel="prev" title="Tagged procedures">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Flexible-curried-procedures">
<div class="header">
<p>
Next: <a href="Combinators-_0028SRFI_0029.html" accesskey="n" rel="next"><code>srfi.235</code> - Combinators (SRFI)</a>, Previous: <a href="Tagged-procedures.html" accesskey="p" rel="prev"><code>srfi.229</code> - Tagged procedures</a>, Up: <a href="Library-modules-_002d-SRFIs.html" accesskey="u" rel="up">Library modules - SRFIs</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="srfi_002e232-_002d-Flexible-curried-procedures"></span><h3 class="section">11.60 <code>srfi.232</code> - Flexible curried procedures</h3>

<dl class="def">
<dt id="index-srfi_002e232-1"><span class="category">Module: </span><span><strong>srfi.232</strong><a href='#index-srfi_002e232-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-srfi_002e232"></span>
<p>This srfi provides macros to define a procedure that can accept
less than or more than the given formal parameters.  If the number of given
parameters is less than the required parameters, it becomes partial
application; a procedure that will take the remaining parameters is returned.
If the number of given parameters is more than the maximum number of
parameters, then the excessive parameters are passed to the result
of the original procedure, assuming the original procedure returns
a procedure.
</p>
<p>Currying in the strict sense converts n-ary procedures to nested 1-ary
procedures:
</p>
<div class="example">
<pre class="example">(lambda (a b c) body)
  &rArr; (lambda (a) (lambda (b) (lambda (c) body)))
</pre></div>

<p>However, with Scheme, you need nested parentheses to give all
the parameters to it:
</p>
<div class="example">
<pre class="example">(((f 1) 2) 3)
</pre></div>

<p>The procedure created with this srfi allows more than one parameters
to be passed at once.  The following calls to <code>f</code> all yield the
same result:
</p>
<div class="example">
<pre class="example">(define-curried (f a b c) body)

(f 1 2 3)
(((f 1) 2) 3)
((f 1 2) 3)
((f 1) 2 3)
</pre></div>

<p>Moreover, the procedure can take more parameters than the formals,
assuming that the procedure with full parameters return a procedure
that takes extra parameters.  In other words, the following
equivalance holds:
</p>
<div class="example">
<pre class="example">(f 1 2 3 4 5 6) &equiv; ((f 1 2 3) 4 5 6)
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-curried"><span class="category">Macro: </span><span><strong>curried</strong> <em>formals body &hellip;</em><a href='#index-curried' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[SRFI-232]{<tt>srfi.232</tt>}
Returns a procedure just like <var>(lambda formals body &hellip;)</var>,
except that the returned procedure may take less or more parameters
than the specified in <var>formals</var>.
</p>
<p>If it is given with less parameters than the required ones, it works
as partial application; the result is a procedure that will accept
the remaining parameters.
</p>
<div class="example">
<pre class="example">(define f (curried (a b c d) (+ a b c d)))

(f 1 2 3 4) &rArr; 10        ; ordinary application
(f 1 2) &rArr; #&lt;procedure&gt;  ; partial application, like (pa$ f 1 2)
((f 1 2) 3 4) &rArr; 10      ; fulfilling the remaining argument
(((f 1 2) 3) 4) &rArr; 10    ; partial application can be nested
(((f 1) 2 3) 4) &rArr; 10    ; ... and can be grouped in any way
</pre></div>

<p>The procedure can accept more parameters than the ones given to
<var>formals</var>, even <var>formals</var> does not specify the &ldquo;rest&rdquo; parameter.
If <var>formals</var> takes the rest parameter, the excess arguments are passed
to it.  If not, the result of the procedure with required parameters
are applied over the excess arguments.
</p>
<div class="example">
<pre class="example">(define f (curried (a b c) (^[x] (* x (+ a b c)))))

(f 1 2 3 4) &rArr; 24  ; &equiv; ((f 1 2 3) 4)
</pre></div>

<p>If no arguments are given to the procedure,
it returls the procedure itself:
</p>
<div class="example">
<pre class="example">(((f)) 1 2 3) &equiv; (f 1 2 3)
</pre></div>

<p>As a special case, <code>(curried () body)</code> and <code>(curried identifier body)</code>
are the same as <code>(lambda () body)</code> and <code>(lambda identifier body)</code>,
respectively.
</p></dd></dl>

<dl class="def">
<dt id="index-define_002dcurried"><span class="category">Macro: </span><span><strong>define-curried</strong> <em>(name . formals) body &hellip;</em><a href='#index-define_002dcurried' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[SRFI-232]{<tt>srfi.232</tt>}
A shorthand of <code>(define name (curried formals body &hellip;))</code>.
</p></dd></dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="Combinators-_0028SRFI_0029.html" accesskey="n" rel="next"><code>srfi.235</code> - Combinators (SRFI)</a>, Previous: <a href="Tagged-procedures.html" accesskey="p" rel="prev"><code>srfi.229</code> - Tagged procedures</a>, Up: <a href="Library-modules-_002d-SRFIs.html" accesskey="u" rel="up">Library modules - SRFIs</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
