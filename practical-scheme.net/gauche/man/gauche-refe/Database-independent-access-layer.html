<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Database independent access layer (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Database independent access layer (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Database independent access layer (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Library-modules-_002d-Utilities.html" rel="up" title="Library modules - Utilities">
<link href="Generic-DBM-interface.html#Generic-DBM-interface" rel="next" title="Generic DBM interface">
<link href="Universally-unique-lexicographically-sortable-identifier.html" rel="prev" title="Universally unique lexicographically sortable identifier">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Database-independent-access-layer">
<div class="header">
<p>
Next: <a href="Generic-DBM-interface.html#Generic-DBM-interface" accesskey="n" rel="next"><code>dbm</code> - Generic DBM interface</a>, Previous: <a href="Universally-unique-lexicographically-sortable-identifier.html" accesskey="p" rel="prev"><code>data.ulid</code> - Universally unique lexicographically sortable identifier</a>, Up: <a href="Library-modules-_002d-Utilities.html" accesskey="u" rel="up">Library modules - Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="dbi-_002d-Database-independent-access-layer"></span><h3 class="section">12.27 <code>dbi</code> - Database independent access layer</h3>

<dl class="def">
<dt id="index-dbi-1"><span class="category">Module: </span><span><strong>dbi</strong><a href='#index-dbi-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-dbi"></span>
<p>This module provides the unified interface to access various
relational database systems (RDBMS).  The operations specific
to individual database systems are packaged
in database driver (DBD) modules, which is usually loaded
implicitly by DBI layer.
</p>
<p>The module is strongly influenced by Perl&rsquo;s DBI/DBD architecture.
If you have used Perl DBI, it would be easy to use this module.
</p></dd></dl>

<p>It&rsquo;s better to look at the example.  This is a simple outline
of accessing a database by <code>dbi</code> module:
</p>
<div class="example">
<pre class="example">(use dbi)
(use gauche.collection) ; to make 'map' work on the query result

(guard (e ((&lt;dbi-error&gt; e)
           ;; handle error
           ))
  (let* ((conn   (dbi-connect &quot;dbi:mysql:test;host=dbhost&quot;))
         (query  (dbi-prepare conn
                   &quot;SELECT id, name FROM users WHERE department = ?&quot;))
         (result (dbi-execute query &quot;R&amp;D&quot;))
         (getter (relation-accessor result)))
    (map (lambda (row)
           (list (getter row &quot;id&quot;)
                 (getter row &quot;name&quot;)))
         result)))
</pre></div>

<p>There&rsquo;s nothing specific to the underlying database system
except the argument <code>&quot;dbi:mysql:test;host=dbhost&quot;</code>
passed to <code>dbi-connect</code>, from which <code>dbi</code> module
figures out that it is an access to <code>mysql</code> database,
loads <code>dbd.mysql</code> module, and let it handle the mysql-specific
stuff.  If you want to use whatever database system, you can just
pass <code>&quot;dbi:<var>whatever</var>:<var>parameter</var>&quot;</code> to <code>dbi-connect</code>
instead, and everything stays the same as far as you have
<code>dbd.whatever</code> installed in your system.
</p>
<p>A query to the database can be created by <code>dbi-prepare</code>.
You can issue the query by <code>dbi-execute</code>.  This two-phase
approach allows you to create a prepared query, which is a kind of
parameterized SQL statement.  In the above example the query
takes one parameter, denoted as <code>'?'</code> in the SQL.
The actual value is given in <code>dbi-execute</code>.  When you
issue similar queries a lot, creating a prepared query and
execute it with different parameters may give you performance gain.
Also the parameter is automatically quoted.
</p>
<p>When the query is a <code>SELECT</code> statement,
its result is returned as a collection that implements
the relation protocol.  See <a href="Collection-framework.html#Collection-framework"><code>gauche.collection</code> - Collection framework</a>
and <a href="Relation-framework.html"><code>util.relation</code> - Relation framework</a> for the details.
</p>
<p>The outermost <code>guard</code> is to catch errors.  The <code>dbi</code> related
errors are supposed to inherit <code>&lt;dbi-error&gt;</code> condition.
There are a few specific errors defined in <code>dbi</code> module.
A specific <code>dbd</code> layer may define more specific errors.
</p>
<p>In the next section we describe user-level API, that is,
the procedures you need to concern when you&rsquo;re using <code>dbi</code>.
The following section is for the driver API, which you need to use
to write a specific <code>dbd</code> driver to make it work with <code>dbi</code>
framework.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="#DBI-user-API" accesskey="1">DBI user API</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="#Writing-drivers-for-DBI" accesskey="2">Writing drivers for DBI</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="subsection" id="DBI-user-API">
<div class="header">
<p>
Next: <a href="#Writing-drivers-for-DBI" accesskey="n" rel="next">Writing drivers for DBI</a>, Previous: <a href="#Database-independent-access-layer" accesskey="p" rel="prev"><code>dbi</code> - Database independent access layer</a>, Up: <a href="#Database-independent-access-layer" accesskey="u" rel="up"><code>dbi</code> - Database independent access layer</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="DBI-user-API-1"></span><h4 class="subsection">12.27.1 DBI user API</h4>

<span id="DBI-Conditions"></span><h4 class="subsubheading">DBI Conditions</h4>

<p>There are several predefined conditions <code>dbi</code> API may throw.
See <a href="Exceptions.html#Exceptions">Exceptions</a> for the details of conditions.
</p>
<dl class="def">
<dt id="index-_003cdbi_002derror_003e"><span class="category">Condition Type: </span><span><strong>&lt;dbi-error&gt;</strong><a href='#index-_003cdbi_002derror_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
The base class of <code>dbi</code>-related conditions.  Inherits <code>&lt;error&gt;</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-_003cdbi_002dnonexistent_002ddriver_002derror_003e"><span class="category">Condition Type: </span><span><strong>&lt;dbi-nonexistent-driver-error&gt;</strong><a href='#index-_003cdbi_002dnonexistent_002ddriver_002derror_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
This condition is thrown by <code>dbi-connect</code> when it cannot
find the specified driver.  Inherits <code>&lt;dbi-error&gt;</code>.
</p>
<dl class="def">
<dt id="index-driver_002dname-of-_003cdbi_002dnonexistent_002ddriver_002derror_003e"><span>Instance Variable of &lt;dbi-nonexistent-driver-error&gt;: <strong>driver-name</strong><a href='#index-driver_002dname-of-_003cdbi_002dnonexistent_002ddriver_002derror_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Holds the requested driver name as a string.
</p></dd></dl>
</dd></dl>

<dl class="def">
<dt id="index-_003cdbi_002dunsupported_002derror_003e"><span class="category">Condition Type: </span><span><strong>&lt;dbi-unsupported-error&gt;</strong><a href='#index-_003cdbi_002dunsupported_002derror_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
This condition is thrown when the called method isn&rsquo;t supported
by the underlying driver.  Inherits <code>&lt;dbi-error&gt;</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-_003cdbi_002dparameter_002derror_003e"><span class="category">Condition Type: </span><span><strong>&lt;dbi-parameter-error&gt;</strong><a href='#index-_003cdbi_002dparameter_002derror_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
This condition is thrown when the number of parameters given to
the prepared query doesn&rsquo;t match the ones in the prepared statement.
</p></dd></dl>

<p>Besides these errors, if a driver relies on <code>dbi</code> to
parse the prepared SQL statement, <code>&lt;sql-parse-error&gt;</code> may
be thrown if an invalid SQL statement is passed to <code>dbi-prepare</code>.
(see <a href="SQL-parsing-and-construction.html"><code>text.sql</code> - SQL parsing and construction</a>).
</p>
<span id="Connecting-to-the-database"></span><h4 class="subsubheading">Connecting to the database</h4>

<dl class="def">
<dt id="index-dbi_002dconnect"><span class="category">Function: </span><span><strong>dbi-connect</strong> <em>dsn :key username password</em><a href='#index-dbi_002dconnect' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Connect to a database using a data source specified by <var>dsn</var>
(data source name).  <var>Dsn</var> is a string with the following syntax:
</p><div class="example">
<pre class="example">dbi:<var>driver</var>:<var>options</var>
</pre></div>

<p><var>Driver</var> part names a specific driver.  You need to have the
corresponding driver module, <code>dbd.<var>driver</var></code>, installed in
your system.  For example, if <var>dsn</var> begins with <code>&quot;dbi:mysql:&quot;</code>,
<code>dbi-connect</code> tries to load <code>dbd.mysql</code>.
</p>
<p>Interpretation of the <var>options</var> part is up to the driver.
Usually it is in the form of <code>key1=value1;key2=value2;...</code>,
but some driver may interpret it differently.  For example,
<code>mysql</code> driver allows you to specify a database name
at the beginning of <var>options</var>.   You have to check out
the document of each driver for the exact specification of
<var>options</var>.
</p>
<p>The keyword arguments gives extra information required for
connection.  The <var>username</var> and <var>password</var> are commonly
supported arguments.  The driver may recognize more keyword arguments.
</p>
<p>If a connection to the database is successfully established,
a connection object (an instance of a subclass of <code>&lt;dbi-connection&gt;</code>)
is returned.  Otherwise, an error is signaled.
</p></dd></dl>

<dl class="def">
<dt id="index-_003cdbi_002dconnection_003e"><span class="category">Class: </span><span><strong>&lt;dbi-connection&gt;</strong><a href='#index-_003cdbi_002dconnection_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-dbi_002dconnection"></span>
<p>{<tt>dbi</tt>}
The base class of a connection to a database system.
Each driver defines a subclass of this to keep information about
database-specific connections.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002dopen_003f"><span class="category">Method: </span><span><strong>dbi-open?</strong> <em>(c &lt;dbi-connection&gt;)</em><a href='#index-dbi_002dopen_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Queries whether a connection to the database is still open (active).
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002dclose"><span class="category">Method: </span><span><strong>dbi-close</strong> <em>(c &lt;dbi-connection&gt;)</em><a href='#index-dbi_002dclose' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Closes a connection to the database.  This causes releasing resources
related to this connection.   Once closed, <var>c</var> cannot
be used for any dbi operations (except passing to <code>dbi-open?</code>).
Calling <code>dbi-close</code> on an already closed connection has no effect.
</p>
<p>Although a driver usually closes a connection when <code>&lt;dbi-connection&gt;</code>
object is garbage-collected, it is not a good idea to rely on that,
since the timing of GC is unpredictable.  The user program must make
sure that it calls <code>dbi-close</code> at a proper moment.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002dlist_002ddrivers"><span class="category">Function: </span><span><strong>dbi-list-drivers</strong><a href='#index-dbi_002dlist_002ddrivers' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Returns a list of module names of known drivers.
</p></dd></dl>

<dl class="def">
<dt id="index-_003cdbi_002ddriver_003e"><span class="category">Class: </span><span><strong>&lt;dbi-driver&gt;</strong><a href='#index-_003cdbi_002ddriver_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-dbi_002ddriver"></span>
<p>{<tt>dbi</tt>}
The base class of a driver.  You usually don&rsquo;t need to see this
as far as you&rsquo;re using the high-level <code>dbi</code> API.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002dmake_002ddriver"><span class="category">Function: </span><span><strong>dbi-make-driver</strong> <em>driver-name</em><a href='#index-dbi_002dmake_002ddriver' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
This is a low-level function called from <code>dbi-connect</code> method,
and usually a user doesn&rsquo;t need to call it.
</p>
<p>Loads a driver module specified by <var>driver-name</var>, and
instantiate the driver class and returns it.
</p></dd></dl>

<span id="Preparing-and-issuing-queries"></span><h4 class="subsubheading">Preparing and issuing queries</h4>

<dl class="def">
<dt id="index-dbi_002dprepare"><span class="category">Method: </span><span><strong>dbi-prepare</strong> <em>conn sql :key pass-through &hellip;</em><a href='#index-dbi_002dprepare' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
From a string representation of SQL statement <var>sql</var>,
creates and returns a query object (an instance of <code>&lt;dbi-query&gt;</code>
or its subclass) for the database connection <code>conn</code>
</p>
<p><var>Sql</var> may contain parameter slots, denoted by <code>?</code>.
</p><div class="example">
<pre class="example">(dbi-prepare conn &quot;insert into tab (col1, col2) values (?, ?)&quot;)

(dbi-prepare conn &quot;select * from tab where col1 = ?&quot;)
</pre></div>

<p>They will be filled when you actually issue the query by
<code>dbi-execute</code>.
There are some advantages of using parameter
slots: (1) The necessary quoting is done automatically.
You don&rsquo;t need to concern about security holes caused by
improper quoting, for example.
(2) Some drivers support a feature to send the template SQL
statement to the server at the preparation stage, and send
only the parameter values at the execution stage.  It would be
more efficient if you issue similar queries lots of time.
</p>
<p>If the backend doesn&rsquo;t support prepared statements (SQL templates
having <code>?</code> parameters), the driver may use <code>text.sql</code>
module to parse <var>sql</var>.  It may raise <code>&lt;sql-parse-error&gt;</code>
condition if the given SQL is not well formed.
</p>
<p>You may pass a true value to the keyword argument <var>pass-through</var>
to suppress interpretation of SQL and pass <var>sql</var> as-is to the
back end database system.  It is useful if the back-end supports
extension of SQL which <code>text.sql</code> doesn&rsquo;t understand.
</p>
<p>If the driver lets prepared statement handled in back-end,
without using <code>text.sql</code>, the <code>pass-through</code> argument
may be ignored.
The driver may also take other keyword arguments.  Check out
the documentation of individual drivers.
</p>
<p><em>Note:</em> Case folding of SQL statement
is implementation dependent.  Some DBMS may treat table
names and column names in case insensitive way, while
others do in case sensitive way.  To write a portable
SQL statement, make them quoted identifiers, that is,
always surround names by double quotes.
</p></dd></dl>

<dl class="def">
<dt id="index-_003cdbi_002dquery_003e"><span class="category">Class: </span><span><strong>&lt;dbi-query&gt;</strong><a href='#index-_003cdbi_002dquery_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-dbi_002dquery"></span>
<p>{<tt>dbi</tt>}
Holds information about prepared query, created by <code>dbi-prepare</code>.
The following slots are defined.
</p>
<dl class="def">
<dt id="index-connection-of-_003cdbi_002dquery_003e"><span>Instance Variable of &lt;dbi-query&gt;: <strong>connection</strong><a href='#index-connection-of-_003cdbi_002dquery_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Contains the <code>&lt;dbi-connection&gt;</code> object.
</p></dd></dl>

<dl class="def">
<dt id="index-prepared-of-_003cdbi_002dquery_003e"><span>Instance Variable of &lt;dbi-query&gt;: <strong>prepared</strong><a href='#index-prepared-of-_003cdbi_002dquery_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>If the driver prepares query by itself, this slot may contain
a prepared statement.  It is up to each driver how to use
this slot, so the client shouldn&rsquo;t rely on its value.
</p></dd></dl>

</dd></dl>

<dl class="def">
<dt id="index-dbi_002dopen_003f-1"><span class="category">Method: </span><span><strong>dbi-open?</strong> <em>(q &lt;dbi-query&gt;)</em><a href='#index-dbi_002dopen_003f-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Returns <code>#t</code> iff the query can still be passed to
<code>dbi-execute</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002dclose-1"><span class="category">Method: </span><span><strong>dbi-close</strong> <em>(q &lt;dbi-query&gt;)</em><a href='#index-dbi_002dclose-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Destroy the query and free resources associated to the query.
After this operation, <code>dbi-open?</code> returns <code>#f</code> for <var>q</var>,
and the query can&rsquo;t be used in any other way.  Although the resource
may be freed when <var>q</var> is garbage-collected, it is strongly recommended
that the application closes queries explicitly.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002dexecute"><span class="category">Method: </span><span><strong>dbi-execute</strong> <em>(q &lt;dbi-query&gt;) parameter &hellip;</em><a href='#index-dbi_002dexecute' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Executes a query created by <code>dbi-prepare</code>.  You should pass
the same number of <var>parameter</var>s as the query expects.
</p>
<p>If the issued query is <code>select</code> statement, <code>dbi-execute</code>
returns an object represents a <em>relation</em>.
A relation encapsulates the values in
rows and columns, as well as meta information like column names.
See &quot;Retrieving query results&quot; below for how to access the result.
</p>
<p>If the query is other types, such as <code>create</code>, <code>insert</code>
or <code>delete</code>, the return value of the query closure is unspecified.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002ddo"><span class="category">Method: </span><span><strong>dbi-do</strong> <em>conn sql :optional options parameter-value &hellip;</em><a href='#index-dbi_002ddo' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
This is a convenience procedure when you create a query
and immediately execute it.   It is equivalent to the following
expression, although the driver may overload this method to avoid
creating intermediate query object to avoid the overhead.
</p><div class="example">
<pre class="example">(dbi-execute (apply dbi-prepare conn sql options)
             parameter-value &hellip;)
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-dbi_002descape_002dsql"><span class="category">Method: </span><span><strong>dbi-escape-sql</strong> <em>conn str</em><a href='#index-dbi_002descape_002dsql' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Returns a string where special characters in <var>str</var> are escaped.
</p>
<p>The official SQL standard only specify a single quote (<code>'</code>) as
such character.  However, it doesn&rsquo;t specify non-printable characters,
and the database system may use other escaping characters.  So
it is necessary to use this method rather than doing escaping
by your own.
</p>
<div class="example">
<pre class="example">;; assumes c is a valid DBI connection
(dbi-escape-sql c &quot;don't know&quot;)
  &rArr; &quot;don''t know&quot;
</pre></div>
</dd></dl>

<span id="Retrieving-query-results"></span><h4 class="subsubheading">Retrieving query results</h4>

<p>If the query is a <code>select</code> statement, it returns an object
of both <code>&lt;collection&gt;</code> and <code>&lt;relation&gt;</code>.
It is a collection of rows (that is, it implements <code>&lt;collection&gt;</code> API),
so you can use <code>map</code>, <code>for-each</code> or other generic functions
to access rows.  You can also use the relation API to retrieve column
names and accessors from it.  See <a href="Relation-framework.html"><code>util.relation</code> - Relation framework</a>, for the relation
API, and <a href="Collection-framework.html#Collection-framework"><code>gauche.collection</code> - Collection framework</a>, for the collection API.
</p>
<p>The actual class of the object returned from a query depends on
the driver, but you may use the following method on it.
</p>
<dl class="def">
<dt id="index-dbi_002dopen_003f-2"><span class="category">Method: </span><span><strong>dbi-open?</strong> <em>result</em><a href='#index-dbi_002dopen_003f-2' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Check whether the result of a query is still active.
The result may become inactive when it is explicitly closed
by <code>dbi-close</code> and/or the connection to the database is
closed.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002dclose-2"><span class="category">Method: </span><span><strong>dbi-close</strong> <em>result</em><a href='#index-dbi_002dclose-2' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Close the result of the query.  This may cause releasing resources
related to the result.   You can no longer use <var>result</var> once it is
closed, except passing it to <code>dbi-open?</code>.
</p>
<p>Although a driver usually releases resources when the result is
garbage-collected, the application shouldn&rsquo;t rely on that and
is recommended call <code>dbi-close</code> explicitly when it is done
with the result.
</p></dd></dl>

<hr>
</div>
<div class="subsection" id="Writing-drivers-for-DBI">
<div class="header">
<p>
Previous: <a href="#DBI-user-API" accesskey="p" rel="prev">DBI user API</a>, Up: <a href="#Database-independent-access-layer" accesskey="u" rel="up"><code>dbi</code> - Database independent access layer</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Writing-drivers-for-DBI-1"></span><h4 class="subsection">12.27.2 Writing drivers for DBI</h4>

<p>Writing a driver for a specific database system means implementing
a module <code>dbd.<var>foo</var></code>, where <var>foo</var> is the name of the driver.
</p>
<p>The module have to implement several classes and methods, as explained below.
</p>
<span id="DBI-classes-to-implement"></span><h4 class="subsubheading">DBI classes to implement</h4>

<p>You have to define the following classes.
</p>
<ul>
<li> Subclass <code>&lt;dbi-driver&gt;</code>.
The class name <em>must</em> be <code>&lt;<var>foo</var>-driver&gt;</code>, where
<var>foo</var> is the name of the driver.
Usually this class produces a singleton instance,
and is only used to dispatch <code>dbi-make-connection</code>
method below.
</li><li> Subclass <code>&lt;dbi-connection&gt;</code>.  An instance of this class is created
by <code>dbi-make-connection</code>.  It needs to keep the information about
the actual connections.
</li><li> Subclass <code>&lt;relation&gt;</code> and <code>&lt;collection&gt;</code> to represent
query results suitable for the driver.  (In most cases, the order of
the result of SELECT statement is significant, since it may be
sorted by ORDER BY clause.  Thus it is more appropriate to
inherit <code>&lt;sequence&gt;</code>, rather than <code>&lt;collection&gt;</code>).
</li><li> Optionally, subclass <code>&lt;dbi-query&gt;</code> to keep driver-specific
information of prepared queries.
</li></ul>

<span id="DBI-methods-to-implement"></span><h4 class="subsubheading">DBI methods to implement</h4>

<p>The driver need to implement the following methods.
</p>
<dl class="def">
<dt id="index-dbi_002dmake_002dconnection"><span class="category">Method: </span><span><strong>dbi-make-connection</strong> <em>(d &lt;foo-driver&gt;) (options &lt;string&gt;) (options-alist &lt;list&gt;) :key username password &hellip;</em><a href='#index-dbi_002dmake_002dconnection' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
This method is called from <code>dbi-connect</code>, and responsible to
connect to the database and to create a connection object.
It must return a connection object, or raise an <code>&lt;dbi-error&gt;</code> if
it cannot establish a connection.
</p>
<p><var>Options</var> is the option part of the data source name (DSN) given to
<code>dbi-connect</code>.  <var>options-alist</var> is an assoc list of
the result of parsing <var>options</var>.  Both are provided so that
the driver may interpret <var>options</var> string in nontrivial way.
</p>
<p>For example, given <code>&quot;dbi:foo:myaddressbook;host=dbhost;port=8998&quot;</code>
as DSN, foo&rsquo;s <code>dbi-make-connection</code> will receive
<code>&quot;myaddressbook;host=dbhost;port=8998&quot;</code> as <var>options</var>,
and <code>((&quot;myaddressbook&quot; . #t) (&quot;host&quot; . &quot;dbhost&quot;) (&quot;port&quot; . &quot;8998&quot;))</code>
as <var>options-alist</var>.
</p>
<p>After <var>options-alist</var>, whatever keyword arguments given to
<code>dbi-connect</code> are passed.  DBI protocol currently
specifies only <var>username</var> and <var>password</var>.
The driver may define other keyword arguments.
It is recommended to name the driver-specific keyword arguments
prefixed by the driver name, e.g. for <code>dbd.foo</code>, it may take
a <code>:foo-whatever</code> keyword argument.
</p>
<p>It is up to the driver writer to define what options are available and
the syntax of the options.  The basic idea is that the DSN
identifies the source of the data; it&rsquo;s role is like URL in WWW.
So, it may include the hostname and port number of the database,
and/or the name of the database, etc.  However, it shouldn&rsquo;t include
information related to authentication, such as username and password.
That&rsquo;s why those are passed via keyword arguments.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002dprepare-1"><span class="category">Method: </span><span><strong>dbi-prepare</strong> <em>(c &lt;foo-connection&gt;) (sql &lt;string&gt;) :key pass-through &hellip;</em><a href='#index-dbi_002dprepare-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
This method should create and return a prepared query object,
which is an instance of <code>&lt;dbi-query&gt;</code> or its subclass.
The query specified by <var>sql</var> is issued to the database system
when the prepared query object is passed to <code>dbi-execute</code>.
</p>
<p>The method must set <var>c</var> to the <code>connection</code> slot of
the returned query object.
</p>
<p><var>Sql</var> is an SQL statement.  It may contain placeholders represented
by <code>'?'</code>.  The query closure should take the same number of arguments
as of the placeholders.   It is up to the driver whether it parses
<var>sql</var> internally and construct a complete SQL statement when
the query closure is called, or it passes <var>sql</var> to the back-end
server to prepare the statement and let the query closure just send
parameters.
</p>
<p>If the driver parses SQL statement internally, it should recognize
a keyword argument <code>pass-through</code>.  If a true value is given,
the driver must treat <code>sql</code> opaque and pass it as is when
the query closure is called.
</p>
<p>The driver may define other keyword arguments.
It is recommended to name the driver-specific keyword arguments
prefixed by the driver name, e.g. for <code>dbd.foo</code>, it may take
a <code>:foo-whatever</code> keyword argument.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002dexecute_002dusing_002dconnection"><span class="category">Method: </span><span><strong>dbi-execute-using-connection</strong> <em>(c &lt;foo-connection&gt;) (q &lt;dbi-query&gt;) (params &lt;list&gt;)</em><a href='#index-dbi_002dexecute_002dusing_002dconnection' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
This method is called from <code>dbi-execute</code>.
It must issue the query kept in <var>q</var>.  If the query is parameterized,
the actual parameters given to <var>dbi-execute</var> are passed to
<var>params</var> argument.
</p>
<p>If <var>q</var> is a <code>select</code>-type query, this method must return
an appropriate relation object.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002descape_002dsql-1"><span class="category">Method: </span><span><strong>dbi-escape-sql</strong> <em>(c &lt;foo-connection&gt;) str</em><a href='#index-dbi_002descape_002dsql-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
If the default escape method isn&rsquo;t enough, the driver may
overload this method to implement a specific escaping.
For example, MySQL treats backslash characters specially
as well as single quotes, so it has its <code>dbi-escape-sql</code>
method.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002dopen_003f-3"><span class="category">Method: </span><span><strong>dbi-open?</strong> <em>(c &lt;foo-connection&gt;)</em><a href='#index-dbi_002dopen_003f-3' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-dbi_002dopen_003f-4"><span class="category">Method: </span><span><strong>dbi-open?</strong> <em>(q &lt;foo-query&gt;)</em><a href='#index-dbi_002dopen_003f-4' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-dbi_002dopen_003f-5"><span class="category">Method: </span><span><strong>dbi-open?</strong> <em>(r &lt;foo-result&gt;)</em><a href='#index-dbi_002dopen_003f-5' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-dbi_002dclose-3"><span class="category">Method: </span><span><strong>dbi-close</strong> <em>(c &lt;foo-connection&gt;)</em><a href='#index-dbi_002dclose-3' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-dbi_002dclose-4"><span class="category">Method: </span><span><strong>dbi-close</strong> <em>(q &lt;foo-query&gt;)</em><a href='#index-dbi_002dclose-4' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-dbi_002dclose-5"><span class="category">Method: </span><span><strong>dbi-close</strong> <em>(r &lt;foo-result&gt;)</em><a href='#index-dbi_002dclose-5' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Queries open/close status of a connection and a result, and
closes a connection and a result.  The close methods should cause
releasing resources used by connection/result.  The driver
has to allow <code>dbi-close</code> to be called on a connection or a
result which has already been closed.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002ddo-1"><span class="category">Method: </span><span><strong>dbi-do</strong> <em>(c &lt;foo-connection&gt;) (sql &lt;string&gt;) :optional options parameter-value &hellip;</em><a href='#index-dbi_002ddo-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
The default method uses <code>dbi-prepare</code> and <code>dbi-execute</code>
to implement the function.  It just works,
but the driver may overload this method in order to skip
creating intermediate query object for efficiency.
</p></dd></dl>

<span id="DBI-utility-functions"></span><h4 class="subsubheading">DBI utility functions</h4>

<p>The following functions are low-level utilities which you may
use to implement the above methods.
</p>
<dl class="def">
<dt id="index-dbi_002dparse_002ddsn"><span class="category">Function: </span><span><strong>dbi-parse-dsn</strong> <em>data-source-name</em><a href='#index-dbi_002dparse_002ddsn' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Parse the data source name (DSN) string given to <code>dbi-connect</code>,
and returns tree values: (1) The driver name in a string. (2)
&rsquo;options&rsquo; part of DSN as a string.  (3) parsed options in an assoc
list.  This may raise <code>&lt;dbi-error&gt;</code> if the given string doesn&rsquo;t
conform DSN syntax.
</p>
<p>You don&rsquo;t need to use this to write a typical driver, for the
parsing is done before <code>dbi-make-connection</code> is called.
This method may be useful if you&rsquo;re writing a kind of meta-driver,
such as a proxy.
</p></dd></dl>

<dl class="def">
<dt id="index-dbi_002dprepare_002dsql"><span class="category">Function: </span><span><strong>dbi-prepare-sql</strong> <em>connection sql</em><a href='#index-dbi_002dprepare_002dsql' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>dbi</tt>}
Parses an SQL statement <var>sql</var> which may contain placeholders,
and returns a closure, which generates a complete SQL statement when
called with actual values for the parameters.  If the back-end
doesn&rsquo;t support prepared statements, you may use this function
to prepare queries in the driver.
</p>
<p><var>Connection</var> is a DBI connection to the database.  It is required
to escape values within SQL properly (see <code>dbi-escape-sql</code> above).
</p>
<div class="example">
<pre class="example">;; assume c contains a valid dbi connection
((dbi-prepare-sql c &quot;select * from table where id=?&quot;) &quot;foo'bar&quot;)
 =&gt; &quot;select * from table where id='foo''bar'&quot;
</pre></div>
</dd></dl>


</div>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Generic-DBM-interface.html#Generic-DBM-interface" accesskey="n" rel="next"><code>dbm</code> - Generic DBM interface</a>, Previous: <a href="Universally-unique-lexicographically-sortable-identifier.html" accesskey="p" rel="prev"><code>data.ulid</code> - Universally unique lexicographically sortable identifier</a>, Up: <a href="Library-modules-_002d-Utilities.html" accesskey="u" rel="up">Library modules - Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
