<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Partial continuations (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Partial continuations (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Partial continuations (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Library-modules-_002d-Gauche-extensions.html" rel="up" title="Library modules - Gauche extensions">
<link href="High_002dlevel-process-interface.html#High_002dlevel-process-interface" rel="next" title="High-level process interface">
<link href="Parsing-command_002dline-options.html" rel="prev" title="Parsing command-line options">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Partial-continuations">
<div class="header">
<p>
Next: <a href="High_002dlevel-process-interface.html#High_002dlevel-process-interface" accesskey="n" rel="next"><code>gauche.process</code> - High-level process interface</a>, Previous: <a href="Parsing-command_002dline-options.html" accesskey="p" rel="prev"><code>gauche.parseopt</code> - Parsing command-line options</a>, Up: <a href="Library-modules-_002d-Gauche-extensions.html" accesskey="u" rel="up">Library modules - Gauche extensions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="gauche_002epartcont-_002d-Partial-continuations"></span><h3 class="section">9.26 <code>gauche.partcont</code> - Partial continuations</h3>

<dl class="def">
<dt id="index-gauche_002epartcont-1"><span class="category">Module: </span><span><strong>gauche.partcont</strong><a href='#index-gauche_002epartcont-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-gauche_002epartcont"></span>
<p>Gauche internally supports partial continuations
(a.k.a. delimited continuations) natively.
This module exposes the feature for general use.
</p></dd></dl>

<p>Note:
Partial continuations use two operators, <code>reset</code> and <code>shift</code>.
Those names are introduced in the original papers, and stuck in the
programming world.  Unfortunately those names are too generic as
library function names.  We thought giving them more descriptive names,
but decided to keep them after all; when you talk about partial
continuations you can&rsquo;t get away from those names.  If these names
conflict to other names in your program, you can use <code>:prefix</code>
import specifier (see <a href="Modules.html#Using-modules">Using modules</a>), for example as follows:
</p>
<div class="example">
<pre class="example">;; Add prefix pc: to the 'reset' and 'shift' operators.
(use gauche.partcont :prefix pc:)

(pc:reset ... (pc:shift k ....) )
</pre></div>

<dl class="def">
<dt id="index-reset"><span class="category">Macro: </span><span><strong>reset</strong> <em>expr &hellip;</em><a href='#index-reset' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>gauche.partcont</tt>}
Saves the current continuation, and executes <var>expr</var> &hellip; with
a <em>null continuation</em> or <em>empty continuation</em>.
The <code>shift</code> operator captures the continuation from the <code>shift</code>
expression to this null continuation.
</p>
<p>Note on <em>implicit delimited continuations</em>:
There&rsquo;s an occasion Gauche effectively calls <code>reset</code> internally:
When C routine calls back to Scheme in non-CPS manner.
(If you know C API, it is <code>Scm_EvalRec()</code>, <code>Scm_ApplyRec*()</code>,
<code>Scm_Eval()</code> and <code>Scm_Apply()</code> family of functions.)  The callers
of such routines expect the result is returned at most once, which won&rsquo;t
work well with Scheme&rsquo;s continuations that have unlimited extent.
Such calls create delimited continuations implicitly.
</p>
<p>For example, the <code>main</code> routine of <code>gosh</code> calls Scheme REPL by
<code>Scm_Eval()</code>, which means the entire REPL is effectively surrounded
by a <code>reset</code>.  So, if you call <code>shift</code> without corresponding
<code>reset</code>, the continuation of <code>shift</code> becomes the continuation
of the entire REPL&mdash;which is to exit from <code>gosh</code>.
This may be surprising if you don&rsquo;t know about the implicit delimited
continuation.
</p>
<p>Other places the implicit delimited continuations are created
are the handlers virtual ports (see <a href="Virtual-ports.html"><code>gauche.vport</code> - Virtual ports</a>),
<code>object-apply</code> methods called from <code>write</code> and <code>display</code>,
and GUI callbacks such as the one registered by <code>glut-display-func</code>
(See the document of Gauche-gl for the details), to name a few.
</p>
<p>In general you don&rsquo;t need to worry about it too much, since most
built-in and extension routines written in C calls back Scheme in CPS manner,
and works with both full and delimited continuations.
</p></dd></dl>

<dl class="def">
<dt id="index-shift"><span class="category">Macro: </span><span><strong>shift</strong> <em>var expr &hellip;</em><a href='#index-shift' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>gauche.partcont</tt>}
Packages the continuation of this expression until the current null
continuation marked by the most recent <code>reset</code> into a procedure,
binds the procedure to <var>var</var>, then executes <var>expr</var> &hellip;
with the continuation saved by the most recent <code>reset</code>.
</p>
<p>That is, after executing <var>expr</var> &hellip;, the value is passed
to the expression waiting for the value of the most recent <code>reset</code>.
When a partial continuation bound to <var>var</var> is executed, its
argument is passed to the continuation waiting for the value
of this <code>shift</code>.   When the execution of the partial continuation
reaches its end, it returns from the expression waiting
for the value of invocation of <var>var</var>.
</p></dd></dl>

<dl class="def">
<dt id="index-call_002fpc"><span class="category">Function: </span><span><strong>call/pc</strong> <em>proc</em><a href='#index-call_002fpc' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>gauche.partcont</tt>}
This is a wrapper of <code>shift</code>.  <code>(shift k expr &hellip;)</code>
is equivalent to <code>(call/pc (lambda (k) expr &hellip;))</code>.
Sometimes this similarity of <code>call/cc</code> comes handy.
</p></dd></dl>

<p>Well, &hellip; I bet you feel like your brain is twisted hard
unless you are one of those rare breed from the land of continuation.
Let me break down what&rsquo;s happening here informally and intuitively.
</p>
<p>Suppose a procedure A calls an expression B.   If A expects a return
value from B and continue processing, we split the part after returning
from B into a separate chunk A&rsquo;, then we can think of the whole
control flow as this straight chain:
</p>
<div class="example">
<pre class="example">A -&gt; B -&gt; A'
</pre></div>

<p><code>A -&gt; B</code> is a procedure call and <code>B -&gt; A'</code> is a return,
but we all know procedure call and return is intrinsically the
same thing, right?
</p>
<p>Procedure B may call another procedure C, and so on.  So when
you look at an execution of particular piece of code, you can
think of a chain of control flow like this:
</p>
<div class="example">
<pre class="example">... -&gt; A -&gt; B -&gt; C -&gt; .... -&gt; C' -&gt; B' -&gt; A' -&gt; ...
</pre></div>

<p>The magic procedure <code>call/cc</code> picks the head of the chain
following its execution (marked as <code>*</code> in the figure below),
and passes it to the given procedure (denoted <code>k</code> in the figure
below).
So, whenever <code>k</code> is invoked, the control goes through the
chain from <code>*</code>.
</p>
<div class="example">
<pre class="example">... -&gt; A -&gt; B -&gt; (call/cc -&gt; (lambda (k) ... ) ) -&gt; B' -&gt; A' -&gt; ...
                                      |             ^
                                      \-----------&gt; *
</pre></div>

<p>One difficulty with <code>call/cc</code> is that the extracted chain
is only one-ended&mdash;we don&rsquo;t know what is chained to the
right.  In fact, what will come after that depends
on the whole program; it&rsquo;s outside of local control.  This global
attribute of <code>call/cc</code> makes it difficult to deal with.
</p>
<p>The <code>reset</code> primitive <em>cuts</em> this chain of
continuation.  The original chain of continuation (the <code>x</code>-end
in the following figure) is saved somewhere, and the continuation
of <code>reset</code> itself becomes open-ended (the <code>o</code>-end
in the following figure).
</p>
<div class="example">
<pre class="example">... -&gt; A -&gt; B -&gt; (reset ... ) -&gt; o

                                 x -&gt; B ' -&gt; A' -&gt; ...
</pre></div>

<p>A rule: If control reaches to the <code>o</code>-end, we pick the
<code>x</code>-end <em>most recently saved</em>.
Because of this, <code>reset</code> alone doesn&rsquo;t show any
difference in the program behavior.
</p>
<p>Now what happens if we insert <code>shift</code> inside <code>reset</code>?
This is a superficial view of inserting <code>shift</code> into
somewhere down the chain of reset:
</p>
<div class="example">
<pre class="example">... -&gt; (reset -&gt; X -&gt; Y -&gt; (shift k ... ) -&gt; Y' -&gt; X' ) -&gt; o
</pre></div>

<p>What actually happens is as follows.
</p>
<ol>
<li> <code>shift</code> packages <em>the rest of the chain of work</em> until the
end of <code>reset</code>, and bind it to the variable <var>k</var>.
</li><li> The continuation of <code>shift</code> becomes a null continuation as well,
so after <code>shift</code> returns, the control skips the rest of
operations until the corresponding <code>reset</code>.
</li></ol>

<div class="example">
<pre class="example">... -&gt; (reset -&gt; X -&gt; Y -&gt; (shift k ... ) ---------&gt; ) -&gt; o
                                  |
                                  \-------&gt; Y' -&gt; X' ) -&gt; o
</pre></div>

<p>In other words, when you consider the <code>reset</code> form as
one chunk of task, then <code>shift</code> in it
<em>stashes away</em> the rest of the task and immediately
returns from the task.
</p>
<p>Let&rsquo;s see an example.  The <var>walker</var> argument in
the following example is a procedure that takes a procedure
and some kind of collection,
and applies the procedure to the each element in the collection.
We ignore the return value of <var>walker</var>.
</p>
<div class="example">
<pre class="example">(define (inv walker)
  (lambda (coll)
    (define (continue)
      (reset (walker (lambda (e) (shift k (set! continue k) e)) coll)
             (eof-object)))
    (lambda () (continue))))
</pre></div>

<p>A typical example of <var>walker</var> is <code>for-each</code>, which takes
a list and applies the procedure to each element of the list.
If we pass <code>for-each</code> to <code>inv</code>, we get a procedure
that is inverted <em>inside-out</em>.  What does that mean?
See the following session:
</p>
<div class="example">
<pre class="example">gosh&gt; (define inv-for-each (inv for-each))
inv-for-each
gosh&gt; (define iter (inv-for-each '(1 2 3)))
iter
gosh&gt; (iter)
1
gosh&gt; (iter)
2
gosh&gt; (iter)
3
gosh&gt; (iter)
#&lt;eof&gt;
</pre></div>

<p>When you pass a list to <code>inv-for-each</code>, you get an iterator
that returns each element in the list for each call.  That&rsquo;s because every
time <code>iter</code> is called, <code>shift</code> in <code>inv</code>
stashes away the task of <em>walking the rest of the collection</em>
and set it to <code>continue</code>, then returns the current element <var>e</var>.
</p>
<p><var>walker</var> doesn&rsquo;t need to work just on list.  The following
function <code>for-each-leaf</code> traverses a tree and
apply <var>f</var> on each non-pair element.
</p>
<div class="example">
<pre class="example">(define (for-each-leaf f tree)
  (match tree
   [(x . y) (for-each-leaf f x) (for-each-leaf f y)]
   [x (f x)]))
</pre></div>

<p>And you can inverse it just like <code>for-each</code>.
</p>
<div class="example">
<pre class="example">gosh&gt; (define iter2 ((inv for-each-leaf) '((1 . 2) . (3 . 4))))
iter2
gosh&gt; (iter2)
1
gosh&gt; (iter2)
2
gosh&gt; (iter2)
3
gosh&gt; (iter2)
4
gosh&gt; (iter2)
#&lt;eof&gt;
</pre></div>

<p>The <code>util.combinations</code> module (see <a href="Combination-library.html"><code>util.combinations</code> - Combination library</a>)
provides a procedure that <em>calls</em> a given procedure with
every permutation of the given collection.  If you pass
it to <code>inv</code>, you get a procedure that <em>returns</em>
every permutation each time.
</p>
<div class="example">
<pre class="example">gosh&gt; (define next ((inv permutations-for-each) '(a b c)))
next
gosh&gt; (next)
(a b c)
gosh&gt; (next)
(a c b)
gosh&gt; (next)
(b a c)
gosh&gt; (next)
(b c a)
gosh&gt; (next)
(c a b)
gosh&gt; (next)
(c b a)
gosh&gt; (next)
#&lt;eof&gt;
</pre></div>



</div>
<hr>
<div class="header">
<p>
Next: <a href="High_002dlevel-process-interface.html#High_002dlevel-process-interface" accesskey="n" rel="next"><code>gauche.process</code> - High-level process interface</a>, Previous: <a href="Parsing-command_002dline-options.html" accesskey="p" rel="prev"><code>gauche.parseopt</code> - Parsing command-line options</a>, Up: <a href="Library-modules-_002d-Gauche-extensions.html" accesskey="u" rel="up">Library modules - Gauche extensions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
