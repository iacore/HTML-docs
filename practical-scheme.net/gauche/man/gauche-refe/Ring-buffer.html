<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Ring buffer (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Ring buffer (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Ring buffer (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Library-modules-_002d-Utilities.html" rel="up" title="Library modules - Utilities">
<link href="Skew-binary-random_002daccess-lists.html" rel="next" title="Skew binary random-access lists">
<link href="Range.html" rel="prev" title="Range">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Ring-buffer">
<div class="header">
<p>
Next: <a href="Skew-binary-random_002daccess-lists.html" accesskey="n" rel="next"><code>data.skew-list</code> - Skew binary random-access lists</a>, Previous: <a href="Range.html" accesskey="p" rel="prev"><code>data.range</code> - Range</a>, Up: <a href="Library-modules-_002d-Utilities.html" accesskey="u" rel="up">Library modules - Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="data_002ering_002dbuffer-_002d-Ring-buffer"></span><h3 class="section">12.22 <code>data.ring-buffer</code> - Ring buffer</h3>

<dl class="def">
<dt id="index-data_002ering_002dbuffer-1"><span class="category">Module: </span><span><strong>data.ring-buffer</strong><a href='#index-data_002ering_002dbuffer-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-data_002ering_002dbuffer"></span>
<p>A ring buffer is an array with two fill pointers; in a typical usage,
a producer adds new data to one end while a consumer removes data
from the other end; if fill pointer reaches at the end of the array,
it wraps around to the beginning, hence the name.
</p>
<p>The ring buffer of this module allows adding and removing elements
from both ends, hence functionally it is a double-ended queue,
or deque.  It also allows O(1) indexed access to the contents,
and customized handling for the case when the buffer gets full.
</p>
<p>You can use an ordinary vector or a uniform vector as the backing
storage of a ring buffer.
</p></dd></dl>

<dl class="def">
<dt id="index-make_002dring_002dbuffer"><span class="category">Function: </span><span><strong>make-ring-buffer</strong> <em>:optional initial-storage :key overflow-handler initial-head-index initial-tail-index</em><a href='#index-make_002dring_002dbuffer' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Creates a ring buffer.  By default, a fresh vector is allocated
for the backing storage.  You can pass a vector or a uvector to
<var>initial-storage</var> to be used instead.  The passed storage must
be mutable, and will
be modified by the ring buffer; the caller shouldn&rsquo;t modify it,
nor make assumption about its content.
</p>
<p>By default, the storage you pass as <var>initial-storage</var> is assumed to
be an empty buffer.  You can also pass a pre-filled storage, by
specifing valid range of existing data with
<var>initial-head-index</var> and <var>initial-tail-index</var> arguments
(both are defaulted to 0).  For example, the following code returns
a ring buffer of initial capacity 8, with the first 4 items are
already filled.
</p>
<div class="example">
<pre class="example">(make-ring-buffer (vector 'a 'b 'c 'd #f #f #f #f)
                  :initial-tail-index 4)
</pre></div>


<p>The <var>overflow-handler</var> keyword argument specifies what to do when
a new element is about to be added to the full buffer.
It must be a procedure, or a symbol <code>error</code>
or <code>overwrite</code>.
</p>
<p>If it is a procedure, it will be called with a ring buffer and
a backing storage (vector or uvector) when it is filled.  The procedure
must either (1) allocate and return a larger vector/uvector of the same
type of the passed backing storage, (2) return a symbol <code>error</code>,
or (3) return a symbol <code>overwrite</code>.  If it returns a vector/uvector,
it will be used as the new backing storage.  The returned vector doesn&rsquo;t need
to be initialized; the ring buffer routine takes care of copying the
necessary data.
If it returns <code>error</code>,
an error (&ldquo;buffer is full&rdquo;) is thrown.  If it returns <code>overwrite</code>,
the new element overwrites the existing element (as if one element
from the other end is popped and discarded.)
</p>
<p>Passing a symbol <code>error</code> or <code>overwrite</code> to <var>overflow-handler</var>
is a shorthand of passing a procedure that unconditionally returns
<code>error</code> or <code>overwrite</code>, respectively.
</p>
<p>The default behavior on overflow is to double the size of backing
storage.  You can use <code>make-overflow-doubler</code> below to create
the customized overflow handler easily.
</p></dd></dl>

<dl class="def">
<dt id="index-make_002doverflow_002ddoubler"><span class="category">Function: </span><span><strong>make-overflow-doubler</strong> <em>:key max-increase max-capacity</em><a href='#index-make_002doverflow_002ddoubler' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Returns a procedure suitable to be passed to the <var>overflow-handler</var>
keyword argument of <code>make-ring-buffer</code>.
</p>
<p>The returned procedure takes a ring buffer and its backing storage,
and behaves as follows.
</p>
<ul>
<li> If the size of current backing storage is equal to or greater than
<var>max-capacity</var>, returns <code>error</code>.
</li><li> Otherwise, if the size of current backing storage is equal to or
greater than <var>max-increase</var>, allocates a vector/uvector of
the same type of the current backing storage, with the size
<code>(+ max-increase size-of-current-storage)</code>.
</li><li> Otherwise, allocates a vector/uvector of
the same type of the current backing storage with the size
<code>(* 2 size-of-current-storage)</code>.
</li></ul>

<p>The default value of <var>max-increase</var> and <var>max-capacity</var> is
<code>+inf.0</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-ring_002dbuffer_002dempty_003f"><span class="category">Function: </span><span><strong>ring-buffer-empty?</strong> <em>rb</em><a href='#index-ring_002dbuffer_002dempty_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Returns <code>#t</code> if the ring buffer <var>rb</var> is empty,
<code>#f</code> if not.
</p></dd></dl>

<dl class="def">
<dt id="index-ring_002dbuffer_002dfull_003f"><span class="category">Function: </span><span><strong>ring-buffer-full?</strong> <em>rb</em><a href='#index-ring_002dbuffer_002dfull_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Returns <code>#t</code> if the ring buffer <var>rb</var> is full,
<code>#f</code> if not.
</p></dd></dl>

<dl class="def">
<dt id="index-ring_002dbuffer_002dnum_002dentries"><span class="category">Function: </span><span><strong>ring-buffer-num-entries</strong> <em>rb</em><a href='#index-ring_002dbuffer_002dnum_002dentries' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Returns the number of current elements in the ring buffer <var>rb</var>.
</p></dd></dl>

<dl class="def">
<dt id="index-ring_002dbuffer_002dcapacity"><span class="category">Function: </span><span><strong>ring-buffer-capacity</strong> <em>rb</em><a href='#index-ring_002dbuffer_002dcapacity' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Returns the size of the current backing storage of the ring buffer <var>rb</var>.
</p></dd></dl>

<dl class="def">
<dt id="index-ring_002dbuffer_002dfront"><span class="category">Function: </span><span><strong>ring-buffer-front</strong> <em>rb</em><a href='#index-ring_002dbuffer_002dfront' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-ring_002dbuffer_002dback"><span class="category">Function: </span><span><strong>ring-buffer-back</strong> <em>rb</em><a href='#index-ring_002dbuffer_002dback' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Returns the element in the front or back of the ring buffer <var>rb</var>,
respectively.
If the buffer is empty, an error is signaled.
</p></dd></dl>

<dl class="def">
<dt id="index-ring_002dbuffer_002dadd_002dfront_0021"><span class="category">Function: </span><span><strong>ring-buffer-add-front!</strong> <em>rb elt</em><a href='#index-ring_002dbuffer_002dadd_002dfront_0021' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-ring_002dbuffer_002dadd_002dback_0021"><span class="category">Function: </span><span><strong>ring-buffer-add-back!</strong> <em>rb elt</em><a href='#index-ring_002dbuffer_002dadd_002dback_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Add an element to the front or back of the ring buffer <var>rb</var>,
respectively.  If <var>rb</var> is full, the behavior is determined by
the buffer&rsquo;s overflow handler, as described in <code>make-ring-buffer</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-ring_002dbuffer_002dremove_002dfront_0021"><span class="category">Function: </span><span><strong>ring-buffer-remove-front!</strong> <em>rb</em><a href='#index-ring_002dbuffer_002dremove_002dfront_0021' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-ring_002dbuffer_002dremove_002dback_0021"><span class="category">Function: </span><span><strong>ring-buffer-remove-back!</strong> <em>rb</em><a href='#index-ring_002dbuffer_002dremove_002dback_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Remove an element from the front or back of the ring buffer <var>rb</var>,
and returns the removed element, respectively.
If the buffer is empty, an error is signaled.
</p></dd></dl>

<dl class="def">
<dt id="index-ring_002dbuffer_002dref"><span class="category">Function: </span><span><strong>ring-buffer-ref</strong> <em>rb index :optional fallback</em><a href='#index-ring_002dbuffer_002dref' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Returns <var>index</var>-th element in the ring buffer <var>rb</var>.
The elements are counted from the front; thus, if a new element
is added to the front, the indexes of existing elements will shift.
</p>
<p>If the index out of bounds of the existing content,
<var>fallback</var> will be returned; if <var>fallback</var> is not
provided, an error is signaled.
</p></dd></dl>

<dl class="def">
<dt id="index-ring_002dbuffer_002dset_0021"><span class="category">Function: </span><span><strong>ring-buffer-set!</strong> <em>rb index value</em><a href='#index-ring_002dbuffer_002dset_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Sets <var>index</var>-th element of the ring buffer <var>rb</var> to <var>value</var>.
The elements are counted from the front; thus, if a new element
is added to the front, the indexes of existing elements will shift.
</p>
<p>An error is signaled if the index is out of bounds.
</p></dd></dl>


<dl class="def">
<dt id="index-ring_002dbuffer_002d_003eflat_002dvector"><span class="category">Function: </span><span><strong>ring-buffer-&gt;flat-vector</strong> <em>rb :optional start end</em><a href='#index-ring_002dbuffer_002d_003eflat_002dvector' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.ring-buffer</tt>}
Returns the current valid content of the ring buffer <var>rb</var>
as a fresh flat vector
of the same type as <var>rb</var>&rsquo;s storage.  If the optional <var>start</var>/<var>end</var>
indexes are given, the content between those indexes are taken.
</p>
<div class="example">
<pre class="example">(define rb (make-ring-buffer (make-vector 4)))

(ring-buffer-&gt;flat-vector rb) &rArr; #()

(ring-buffer-add-back! rb 'a)
(ring-buffer-add-back! rb 'b)
(ring-buffer-add-front! rb 'z)
(ring-buffer-add-front! rb 'y)

(ring-buffer-&gt;flat-vector rb) &rArr; #(y z a b)
(ring-buffer-&gt;flat-vector rb 1 3) &rArr; #(z a)
</pre></div>
</dd></dl>


</div>
<hr>
<div class="header">
<p>
Next: <a href="Skew-binary-random_002daccess-lists.html" accesskey="n" rel="next"><code>data.skew-list</code> - Skew binary random-access lists</a>, Previous: <a href="Range.html" accesskey="p" rel="prev"><code>data.range</code> - Range</a>, Up: <a href="Library-modules-_002d-Utilities.html" accesskey="u" rel="up">Library modules - Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
