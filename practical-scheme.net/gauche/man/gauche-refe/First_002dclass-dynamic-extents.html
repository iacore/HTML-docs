<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>First-class dynamic extents (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="First-class dynamic extents (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="First-class dynamic extents (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Library-modules-_002d-SRFIs.html" rel="up" title="Library modules - SRFIs">
<link href="Homogeneous-numeric-vector-libraries.html" rel="next" title="Homogeneous numeric vector libraries">
<link href="String-library-_0028reduced_0029.html" rel="prev" title="String library (reduced)">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="First_002dclass-dynamic-extents">
<div class="header">
<p>
Next: <a href="Homogeneous-numeric-vector-libraries.html" accesskey="n" rel="next"><code>srfi.160</code> - Homogeneous numeric vector libraries</a>, Previous: <a href="String-library-_0028reduced_0029.html" accesskey="p" rel="prev"><code>srfi.152</code> - String library (reduced)</a>, Up: <a href="Library-modules-_002d-SRFIs.html" accesskey="u" rel="up">Library modules - SRFIs</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="srfi_002e154-_002d-First_002dclass-dynamic-extents"></span><h3 class="section">11.30 <code>srfi.154</code> - First-class dynamic extents</h3>

<dl class="def">
<dt id="index-srfi_002e154-1"><span class="category">Module: </span><span><strong>srfi.154</strong><a href='#index-srfi_002e154-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-srfi_002e154"></span>
<p>This module provides a convenient way to reify the dynamic environment.
A continuation captured by <code>call/cc</code> includes the dynamic environment,
as well as the control flow.  Sometimes you only want the dynamic environment
part.  A dynamic extent is a reified dynamic environment.
</p>
<p>Let&rsquo;s see an example.  You want a procedure that prints out
a message to the error port, so you wrote this <code>print-error</code>:
</p>
<div class="example">
<pre class="example">(define print-error
  (lambda (msg) (display msg (current-error-port))))
</pre></div>

<p>However, if <code>print-error</code> is called while the current error port
is altered, it is affected.  It is supposed to be so, that&rsquo;s
the point of <code>current-error-port</code>.
</p>
<div class="example">
<pre class="example">(call-with-output-string
  (^p (with-error-to-port p (^[] (print-error &quot;abc\n&quot;)))))
 &rArr; &quot;abc\n&quot;
</pre></div>

<p>If you do want <code>print-error</code> to use the error port
at the time it is defined, you have to extract the dynamic value
at the moment.
</p>
<div class="example">
<pre class="example">(define print-error
  (let1 eport (current-error-port)
    (lambda (msg) (display msg eport))))
</pre></div>

<p>This would be quickly cumbersome when you need to capture multiple
dynamic values, or the original <code>print-error</code> is called indirectly
and you can&rsquo;t modify it the way shown above.
</p>
<p>Using <code>dynamic-lambda</code> addresses this issue.  It not only captures
the lexical environment, but also the dynamic environment when it
is evaluated.  So the <code>current-error-port</code> in its body
returns the current error port at the time of <code>print-error</code>
being defined, not when it is called:
</p>
<div class="example">
<pre class="example">(define print-error
  (dynamic-lambda (msg) (display msg (current-error-port))))
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-current_002ddynamic_002dextent"><span class="category">Function: </span><span><strong>current-dynamic-extent</strong><a href='#index-current_002ddynamic_002dextent' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[SRFI-154]{<tt>srfi.154</tt>}
Returns a dynamic-extent object that reifies the current dynamic
environment.
</p></dd></dl>

<dl class="def">
<dt id="index-dynamic_002dextent_003f"><span class="category">Function: </span><span><strong>dynamic-extent?</strong> <em>obj</em><a href='#index-dynamic_002dextent_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[SRFI-154]{<tt>srfi.154</tt>}
Returns <code>#t</code> iff <var>obj</var> is a dynamic-extent object.
</p></dd></dl>

<dl class="def">
<dt id="index-with_002ddynamic_002dextent"><span class="category">Function: </span><span><strong>with-dynamic-extent</strong> <em>dynext thunk</em><a href='#index-with_002ddynamic_002dextent' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[SRFI-154]{<tt>srfi.154</tt>}
Calls the <var>thunk</var> in the dynamic extent <var>dynext</var>, and
returns the values yielded by <var>thunk</var>.
</p></dd></dl>

<dl class="def">
<dt id="index-dynamic_002dlambda"><span class="category">Macro: </span><span><strong>dynamic-lambda</strong> <em>formals body &hellip;</em><a href='#index-dynamic_002dlambda' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[SRFI-154]{<tt>srfi.154</tt>}
Like <code>lambda</code>, but this not only captures the lexical environment,
but also the dynamic environment.  Yields a procedure.
</p>
<p>Note: Since <code>dynamic-lambda</code> needs to swap the dynamic environment
after executing <var>body</var>, the last expression of <var>body</var> isn&rsquo;t
called in the tail context even if the procedure created by
<code>dynamic-lambda</code> is called in tail context.
</p></dd></dl>



</div>
<hr>
<div class="header">
<p>
Next: <a href="Homogeneous-numeric-vector-libraries.html" accesskey="n" rel="next"><code>srfi.160</code> - Homogeneous numeric vector libraries</a>, Previous: <a href="String-library-_0028reduced_0029.html" accesskey="p" rel="prev"><code>srfi.152</code> - String library (reduced)</a>, Up: <a href="Library-modules-_002d-SRFIs.html" accesskey="u" rel="up">Library modules - SRFIs</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
