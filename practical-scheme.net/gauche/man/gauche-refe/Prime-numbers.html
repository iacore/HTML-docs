<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Prime numbers (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Prime numbers (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Prime numbers (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Library-modules-_002d-Utilities.html" rel="up" title="Library modules - Utilities">
<link href="Simplex-solver.html" rel="next" title="Simplex solver">
<link href="Mersenne_002dTwister-random-number-generator.html" rel="prev" title="Mersenne-Twister random number generator">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Prime-numbers">
<div class="header">
<p>
Next: <a href="Simplex-solver.html" accesskey="n" rel="next"><code>math.simplex</code> - Simplex solver</a>, Previous: <a href="Mersenne_002dTwister-random-number-generator.html" accesskey="p" rel="prev"><code>math.mt-random</code> - Mersenne Twister Random number generator</a>, Up: <a href="Library-modules-_002d-Utilities.html" accesskey="u" rel="up">Library modules - Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="math_002eprime-_002d-Prime-numbers"></span><h3 class="section">12.37 <code>math.prime</code> - Prime numbers</h3>

<dl class="def">
<dt id="index-math_002eprime-1"><span class="category">Module: </span><span><strong>math.prime</strong><a href='#index-math_002eprime-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-math_002eprime"></span>
<p>This module provides utilities related to prime numbers.
</p></dd></dl>

<span id="Sequence-of-prime-numbers"></span><h4 class="subheading">Sequence of prime numbers</h4>

<dl class="def">
<dt id="index-_002aprimes_002a"><span class="category">Variable: </span><span><strong>*primes*</strong><a href='#index-_002aprimes_002a' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>math.prime</tt>}
An infinite lazy sequence of primes.
</p>
<div class="example">
<pre class="example">;; show 10 prime numbers from 100-th one.
(take (drop *primes* 100) 10)
 &rArr; (547 557 563 569 571 577 587 593 599 601)
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-reset_002dprimes"><span class="category">Function: </span><span><strong>reset-primes</strong><a href='#index-reset_002dprimes' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>math.prime</tt>}
Once you take a very large prime out of <code>*primes*</code>, all primes
before that has been calculated remains in memory, since the
head of sequence is held in <code>*primes*</code>.  Sometimes you know
you need no more prime numbers and you wish those calculated ones
to be garbage-collected.  Calling <code>reset-primes</code> rebinds
<code>*primes*</code> to unrealized lazy sequence, allowing the previously
realized primes to be GCed.
</p></dd></dl>

<dl class="def">
<dt id="index-primes"><span class="category">Function: </span><span><strong>primes</strong><a href='#index-primes' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>math.prime</tt>}
Returns a fresh lazy sequence of primes.  It is useful when
you need certain primes in a short period of time&mdash;if you don&rsquo;t keep
a reference to the head of the returned sequence, it will be garbage
collected after you&rsquo;ve done with the primes.
(Note that calculation of a prime number needs the
sequence of primes from the beginning,
so even if your code only keep a reference
in the middle of the sequence, the entire sequence will be kept
in the thunk within the lazy sequence&mdash;you have to release all
references in order to make the sequence GCed.)
</p>
<p>On the other hand,
each sequence returned by <code>primes</code> are realized individually,
duplicating calculation.
</p>
<p>The rule of thumb is&mdash;if you use primes repeatedly throughout
the program, just use <code>*primes*</code> and you&rsquo;ll save calculation.
If you need primes one-shot, call <code>primes</code> and abandon it
and you&rsquo;ll save space.
</p></dd></dl>

<span id="Testing-primality"></span><h4 class="subheading">Testing primality</h4>

<dl class="def">
<dt id="index-small_002dprime_003f"><span class="category">Function: </span><span><strong>small-prime?</strong> <em>n</em><a href='#index-small_002dprime_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>math.prime</tt>}
For relatively small positive integers
(below <code>*small-prime-bound*</code>, to be specific), this procedure
determines if the input is prime or not, quickly and deterministically.
If <var>n</var> is on or above the bound, this procedure returns <code>#f</code>.
</p>
<p>This can be used to quickly filter out known primes; it never returns
<code>#t</code> on composite numbers (while it may return <code>#f</code> on
large prime numbers).
Miller-Rabin test below can tell if the input is composite for sure,
but it may return <code>#t</code> on some composite numbers.
</p></dd></dl>

<dl class="def">
<dt id="index-_002asmall_002dprime_002dbound_002a"><span class="category">Variable: </span><span><strong>*small-prime-bound*</strong><a href='#index-_002asmall_002dprime_002dbound_002a' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>math.prime</tt>}
For all positive integers below this value
(slightly above 3.4e14 in the current implementation),
<code>small-prime?</code> can determines whether it is a prime or not.
</p></dd></dl>


<dl class="def">
<dt id="index-miller_002drabin_002dprime_003f"><span class="category">Function: </span><span><strong>miller-rabin-prime?</strong> <em>n :key num-tests random-integer</em><a href='#index-miller_002drabin_002dprime_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>math.prime</tt>}
Check if an exact integer <var>n</var> is a prime number, using
probabilistic Miller-Rabin algorithm (<var>n</var> must be greater than 1).
If this procedure returns <code>#f</code>,
<var>n</var> is a composite number.  If this procedure returns <code>#t</code>,
<var>n</var> is <em>likely</em> a prime, but there&rsquo;s a small probability
that it is a false positive.
</p>
<p>Note that if <var>n</var> is smaller than a certain number
(<code>*small-prime-bound*</code>), the algorithm is
deterministic; if it returns <code>#t</code>, <var>n</var> is certainly a prime.
</p>
<p>If <var>n</var> is greater than or equal to
<code>*small-prime-bound*</code>,
we use a probabilistic test.  We choosing random base integer
to perform Miller-Rabin test up to 7 times by default.
You can change the number of tests by the keyword argument
<var>num-tests</var>.  The error probability
(to return <code>#t</code> for a composite number)
is at most <code>(expt 4 (- num-tests))</code>.
</p>
<p>For a probabilistic test, <code>miller-rabin-prime?</code> uses
its own fixed random seed by default.  We chose fixed seed
so that the behavior can be reproducible.  To change the random
sequence, you can provide your own random integer generator
to the <var>random-integer</var> keyword argument.   It must be
a procedure that takes a positive integer <var>k</var> and returns
a random integer from 0 to <var>k-1</var>, including.
</p></dd></dl>

<dl class="def">
<dt id="index-bpsw_002dprime_003f"><span class="category">Function: </span><span><strong>bpsw-prime?</strong> <em>n</em><a href='#index-bpsw_002dprime_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>math.prime</tt>}
Check if an exact integer <var>n</var> is a prime number, using
Baillie-PSW primality test
(<a href="http://www.trnicely.net/misc/bpsw.html">http://www.trnicely.net/misc/bpsw.html</a>).   It is deterministic,
and returns the definitive answer below 2^64 (around 1.8e19).
For larger integers this can return <code>#t</code> on a composite number,
although such number hasn&rsquo;t been found yet.  This never returns <code>#f</code>
on a prime number.
</p>
<p>This is slower than Miller-Rabin but fast enough for casual use,
so it is handy when you want a definitive answer below the above range.
</p></dd></dl>

<span id="Factorization"></span><h4 class="subheading">Factorization</h4>

<dl class="def">
<dt id="index-naive_002dfactorize"><span class="category">Function: </span><span><strong>naive-factorize</strong> <em>n :optional divisor-limit</em><a href='#index-naive_002dfactorize' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>math.prime</tt>}
Factorize a positive exact integer <var>n</var> by trying to divide it with
all primes up to <code>(sqrt n)</code>.  Returns a list of prime factors
(each of which is equal to or greater than 2),
smaller ones first.
</p>
<div class="example">
<pre class="example">(naive-factorize 142857)
  &rArr; (3 3 3 11 13 37)
</pre></div>

<p>Note that <code>(naive-factorize 1)</code> is <code>()</code>.
</p>
<p>Although this is pretty naive method, this works well as far as
any of <var>n</var>&rsquo;s factors are up to the order of around <code>1e7</code>.
For example, the following example runs in about 0.4sec on 2.4GHz Core2
machine.
(The first time will take about 1.3sec to realize lazy prime sequences.)
</p>
<div class="example">
<pre class="example">(naive-factorize 3644357367494986671013))
  &rArr; (10670053 10670053 32010157)
</pre></div>

<p>Of course, if <var>n</var> includes any factors above that order,
the performance becomes abysmal.   So it is better to use this
procedure below 1e14 or so.
</p>
<p>Alternatively, you can give <var>divisor-limit</var> argument that specifies
the upper bound of the prime number to be tried.  If it is given,
<code>naive-factorize</code> leaves a factor <var>f</var> as is if it can&rsquo;t be
divided by any primes less than or equal to <var>divisor-limit</var>.
So, the last element of the returned list may be composite number.
This is handy to exclude trivial factors before applying more sophisticated
factorizing algorithms.
</p>
<div class="example">
<pre class="example">(naive-factorize 825877877739 1000)
  &rArr; (3 43 6402154091)

;; whereas
(naive-factorize 825877877739)
  &rArr; (3 43 4591 1394501)
</pre></div>

<p>The procedure also memoizes the results on smaller <var>n</var> to make
things faster.
</p></dd></dl>

<dl class="def">
<dt id="index-mc_002dfactorize"><span class="category">Function: </span><span><strong>mc-factorize</strong> <em>n</em><a href='#index-mc_002dfactorize' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>math.prime</tt>}
Factorize a positive exact integer <var>n</var> using the algorithm
described in
R. P. Brent, An improved Monte Carlo factorization algorithm, BIT 20 (1980), 176-184. <a href="http://maths-people.anu.edu.au/~brent/pub/pub051.html">http://maths-people.anu.edu.au/~brent/pub/pub051.html</a>.
</p>
<p>This one is capable to handle much larger range than
<code>naive-factorize</code>, somewhere around 1e20 or so.
</p>
<p>Since this method is probabilistic, the execution time may vary
on the same <var>n</var>.  But it will always return the definitive
results as far as every prime factor of <var>n</var> is smaller than 2^64.
</p>
<p>At this moment, if <var>n</var> contains a prime factor greater than
2^64, this routine would keep trying factorizing it forever.
Practical applications should have some means to interrupt the
function and give it up after some time bounds.
This will be addressed once we have deterministic primality test.
</p></dd></dl>


<span id="Miscellaneous-1"></span><h4 class="subheading">Miscellaneous</h4>

<dl class="def">
<dt id="index-jacobi"><span class="category">Function: </span><span><strong>jacobi</strong> <em>a n</em><a href='#index-jacobi' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>math.prime</tt>}
Calculates Jacobi symbol <code>(<var>a</var>/<var>n</var>)</code>
(<a href="http://en.wikipedia.org/wiki/Jacobi_symbol">http://en.wikipedia.org/wiki/Jacobi_symbol</a>).
</p></dd></dl>

<dl class="def">
<dt id="index-totient"><span class="category">Function: </span><span><strong>totient</strong> <em>n</em><a href='#index-totient' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>math.prime</tt>}
Euler&rsquo;s totient function of nonnegative integer <var>n</var>.
</p>
<p>The current implementation relies on <code>mc-factorize</code> above,
so it may take very long if <var>n</var> contains large prime factors.
</p></dd></dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="Simplex-solver.html" accesskey="n" rel="next"><code>math.simplex</code> - Simplex solver</a>, Previous: <a href="Mersenne_002dTwister-random-number-generator.html" accesskey="p" rel="prev"><code>math.mt-random</code> - Mersenne Twister Random number generator</a>, Up: <a href="Library-modules-_002d-Utilities.html" accesskey="u" rel="up">Library modules - Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
