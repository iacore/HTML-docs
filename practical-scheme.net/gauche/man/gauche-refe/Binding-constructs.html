<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Binding constructs (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Binding constructs (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Binding constructs (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Core-syntax.html" rel="up" title="Core syntax">
<link href="Grouping.html" rel="next" title="Grouping">
<link href="Conditionals.html" rel="prev" title="Conditionals">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Binding-constructs">
<div class="header">
<p>
Next: <a href="Grouping.html" accesskey="n" rel="next">Grouping</a>, Previous: <a href="Conditionals.html" accesskey="p" rel="prev">Conditionals</a>, Up: <a href="Core-syntax.html" accesskey="u" rel="up">Core syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Binding-constructs-1"></span><h3 class="section">4.6 Binding constructs</h3>

<dl class="def">
<dt id="index-let"><span class="category">Special Form: </span><span><strong>let</strong> <em>((var expr) &hellip;) body &hellip;</em><a href='#index-let' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-let_002a"><span class="category">Special Form: </span><span><strong>let*</strong> <em>((var expr) &hellip;) body &hellip;</em><a href='#index-let_002a' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-letrec"><span class="category">Special Form: </span><span><strong>letrec</strong> <em>((var expr) &hellip;) body &hellip;</em><a href='#index-letrec' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-letrec_002a"><span class="category">Special Form: </span><span><strong>letrec*</strong> <em>((var expr) &hellip;) body &hellip;</em><a href='#index-letrec_002a' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS base]
Creates a local scope where <var>var</var> &hellip; are bound to the
value of <var>expr</var> &hellip;, then evaluates <var>body</var> &hellip;.
<var>Var</var>s must be symbols, and there shouldn&rsquo;t be duplicates.
The value(s) of the last expression of <var>body</var> &hellip; becomes
the value(s) of this form.
</p>
<p>The four forms differ in terms of the scope and the order
<var>expr</var>s are evaluated.
<code>Let</code> evaluates <var>expr</var>s before (outside of) <code>let</code> form.
The order of evaluation of <var>expr</var>s is undefined, and the compiler
may reorder those <var>expr</var>s freely for optimization.
<code>Let*</code> evaluates <var>expr</var>s, in the order they appears,
and each <var>expr</var> is evaluated in the scope
where <var>var</var>s before it are bound.
</p>
<p><code>Letrec</code> evaluates <var>expr</var>s, in an undefined order,
in the environment where <var>var</var>s are already bound
(to an undefined value, initially).
<code>letrec</code> is necessary to define mutually recursive local procedures.
Finally, <code>letrec*</code> uses the same scope rule as <code>letrec</code>,
and it evaluates <var>expr</var> in the order of appearance.
</p>
<div class="example">
<pre class="example">(define x 'top-x)

(let  ((x 3) (y x)) (cons x y)) &rArr; (3 . top-x)
(let* ((x 3) (y x)) (cons x y)) &rArr; (3 . 3)

(let ((cons (lambda (a b) (+ a b)))
      (list (lambda (a b) (cons a (cons b 0)))))
  (list 1 2))  &rArr; (1 2 . 0)

(letrec ((cons (lambda (a b) (+ a b)))
         (list (lambda (a b) (cons a (cons b 0)))))
  (list 1 2))  &rArr; 3
</pre></div>

<p>You need to use <code>letrec*</code> if evaluation of one <var>expr</var> requires
the value of <var>var</var> that appears before the <var>expr</var>.  In the following
example, calculating the value of <var>a</var> and <var>b</var> requires the value
of <var>cube</var>, so you need <code>letrec*</code>.   (Note the difference from
the above example, where <em>calculating</em> the value of <var>list</var>
doesn&rsquo;t need to take the value of <var>cons</var> bound in the same <code>letrec</code>.
The value of <var>cons</var> isn&rsquo;t required until <var>list</var> is actually applied.)
</p>
<div class="example">
<pre class="example">(letrec* ((cube (lambda (x) (* x x x)))
          (a (+ (cube 1) (cube 12)))
          (b (+ (cube 9) (cube 10))))
  (= a b)) &rArr; #t
</pre></div>

<p>This example happens to work with <code>letrec</code> in the current Gauche, but
it is not guaranteed to keep working in future.  You just should not
rely on evaluation order when you use <code>letrec</code>.
In retrospect, it would be a lot simpler if we only had <code>letrec*</code>.
Unfortunately <code>letrec</code> preceded for long time in Scheme history
and it&rsquo;s hard to remove that.  Besides, <code>letrec</code> does have more
opportunities to optimize than <code>letrec*</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-let1"><span class="category">Macro: </span><span><strong>let1</strong> <em>var expr body &hellip;</em><a href='#index-let1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>A convenient macro when you have only one variable.
Expanded as follows.
</p>
<div class="example">
<pre class="example">(let ((<var>var</var> <var>expr</var>)) <var>body</var> &hellip;)
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-if_002dlet1"><span class="category">Macro: </span><span><strong>if-let1</strong> <em>var expr then</em><a href='#index-if_002dlet1' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-if_002dlet1-1"><span class="category">Macro: </span><span><strong>if-let1</strong> <em>var expr then else</em><a href='#index-if_002dlet1-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>This macro simplifies the following idiom:
</p><div class="example">
<pre class="example">(let1 <var>var</var> <var>expr</var>
  (if <var>var</var> <var>then</var> <var>else</var>))
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-rlet1"><span class="category">Macro: </span><span><strong>rlet1</strong> <em>var expr body &hellip;</em><a href='#index-rlet1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>This macro simplifies the following idiom:
</p><div class="example">
<pre class="example">(let1 <var>var</var> <var>expr</var>
  <var>body</var> &hellip;
  <var>var</var>)
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-and_002dlet_002a"><span class="category">Macro: </span><span><strong>and-let*</strong> <em>(binding &hellip;) body &hellip;</em><a href='#index-and_002dlet_002a' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[SRFI-2]
In short, it works like <code>let*</code>, but returns <code>#f</code> immediately
whenever the expression in <var>binding</var>s evaluates to <code>#f</code>.
</p>
<p>Each <var>binding</var> should be one of the following form:
</p><dl compact="compact">
<dt><span><code>(<var>variable</var> <var>expression</var>)</code></span></dt>
<dd><p>The <var>expression</var> is evaluated; if it yields true value, the value
is bound to <var>variable</var>, then proceed to the next binding.  If
no more bindings, evaluates <var>body</var> &hellip;.   If <var>expression</var>
yields <code>#f</code>, stops evaluation and returns <code>#f</code> from <code>and-let*</code>.
</p></dd>
<dt><span><code>(<var>expression</var>x)</code></span></dt>
<dd><p>In this form, <var>variable</var> is omitted.  <var>Expression</var> is evaluated
and the result is used just to determine whether we continue or
stop further evaluation.
</p></dd>
<dt><span><code><var>bound-variable</var></code></span></dt>
<dd><p>In this form, <var>bound-variable</var> should be an identifier denoting
a bound variable.  If its value is not <code>#f</code>, we continue
the evaluation of the clauses.
</p></dd>
</dl>

<p>Let&rsquo;s see some examples.  The following code searches <var>key</var>
from an assoc-list <var>alist</var> and returns its value if found.
</p><div class="example">
<pre class="example">(and-let* ((entry (assoc key alist))) (cdr entry))
</pre></div>

<p>If <var>arg</var> is a string representation of an exact integer, returns its value;
otherwise, returns 0:
</p><div class="example">
<pre class="example">(or (and-let* ((num (string-&gt;number arg))
               ( (exact? num) )
               ( (integer? num) ))
      num)
    0)
</pre></div>

<p>The following is a hypothetical code that searches a certain server port
number from a few possibilities (environment variable, configuration file,
...)
</p><div class="example">
<pre class="example">(or (and-let* ((val (sys-getenv &quot;SERVER_PORT&quot;)))
      (string-&gt;number val))
    (and-let* ((portfile (expand-path &quot;~/.server_port&quot;))
               ( (file-exists? portfile) )
               (val (call-with-input-string portfile port-&gt;string)))
      (string-&gt;number val))
    8080) ; default
</pre></div>

</dd></dl>

<dl class="def">
<dt id="index-and_002dlet1"><span class="category">Macro: </span><span><strong>and-let1</strong> <em>var test exp1 exp2 &hellip;</em><a href='#index-and_002dlet1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Evaluates <var>test</var>, and if it isn&rsquo;t <code>#f</code>, binds <var>var</var> to it
and evaluates <var>exp1</var> <var>exp2</var> &hellip;.  Returns the result(s) of
the last expression.  If <var>test</var> evaluates to <code>#f</code>, returns <code>#f</code>.
</p>
<p>This can be easily written by <code>and-let*</code> or <code>if-let1</code> as follows.
However, we&rsquo;ve written this idiom so many times that it deserves
another macro.
</p>
<div class="example">
<pre class="example">(and-let1 var test
  exp1
  exp2 &hellip;)

&equiv;

(and-let* ([var test])
  exp1
  exp2 &hellip;)

&equiv;

(if-let1 var test
  (begin exp1 exp2 &hellip;)
  #f)
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-fluid_002dlet"><span class="category">Macro: </span><span><strong>fluid-let</strong> <em>((var val) &hellip;) body &hellip;</em><a href='#index-fluid_002dlet' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>A macro that emulates dynamic scoped variables.
<var>Var</var>s must be variables bound in the scope including
<code>fluid-let</code> form.  <var>Val</var>s are expressions.
<code>Fluid-let</code> first evaluates <var>val</var>s, then
evaluates <var>body</var> &hellip;, with binding
<var>var</var>s to the corresponding values during the dynamic
scope of <var>body</var> &hellip;.
</p>
<p>Note that, in multithreaded environment,
the change of the value of <var>var</var>s are visible from
all the threads.   This form is provided mainly for
the porting convenience.   Use parameter objects instead
(see <a href="Parameters-and-dynamic-states.html#Parameters">Parameters</a>) for thread-local dynamic state.
</p><div class="example">
<pre class="example">(define x 0)

(define (print-x) (print x))

(fluid-let ((x 1))
  (print-x))  &rArr; ;; prints 1
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-receive"><span class="category">Special Form: </span><span><strong>receive</strong> <em>formals expression body &hellip;</em><a href='#index-receive' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[SRFI-8]
This is the way to receive multiple values.
<var>Formals</var> can be a (maybe-improper) list of symbols.
<var>Expression</var> is evaluated, and the returned value(s)
are bound to <var>formals</var> like the binding of lambda formals,
then <var>body</var> &hellip; are evaluated.
</p>
<div class="example">
<pre class="example">(define (divrem n m)
  (values (quotient n m) (remainder n m)))

(receive (q r) (divrem 13 4) (list q r))
  &rArr; (3 1)

(receive all (divrem 13 4) all)
  &rArr; (3 1)

(receive (q . rest) (divrem 13 4) (list q rest))
  &rArr; (3 (1))
</pre></div>

<p>See also <code>call-with-values</code> in <a href="Procedures-and-continuations.html#Multiple-values">Multiple values</a>
which is the procedural equivalent of <code>receive</code>.
You can use <code>define-values</code> (see <a href="Definitions.html#Definitions">Definitions</a>) to
bind multiple values to variables simultaneously.
Also <code>let-values</code> and <code>let*-values</code> below
provides <code>let</code>-like syntax with multiple values.
</p></dd></dl>

<dl class="def">
<dt id="index-let_002dvalues"><span class="category">Macro: </span><span><strong>let-values</strong> <em>((vars expr) &hellip;) body &hellip;</em><a href='#index-let_002dvalues' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS base]
<var>vars</var> are a list of variables. <var>expr</var> is evaluated, and
its first return value is bound to the first variable in <var>vars</var>,
its second return value to the second variable, and so on, then
<var>body</var> is evaluated.
The scope of <var>expr</var>s are the outside of <code>let-values</code> form,
like <code>let</code>.
</p>
<div class="example">
<pre class="example">(let-values (((a b) (values 1 2))
             ((c d) (values 3 4)))
  (list a b c d)) &rArr; (1 2 3 4)

(let ((a 1) (b 2) (c 3) (d 4))
  (let-values (((a b) (values c d))
               ((c d) (values a b)))
    (list a b c d))) &rArr; (3 4 1 2)
</pre></div>

<p><var>vars</var> can be a dotted list or a single symbol, like the
lambda parameters.
</p>
<div class="example">
<pre class="example">(let-values (((x . y) (values 1 2 3 4)))
  y) &rArr; (2 3 4)

(let-values ((x (values 1 2 3 4)))
  x) &rArr; (1 2 3 4)
</pre></div>

<p>If the number of values returned by <var>expr</var> doesn&rsquo;t match
what <var>vars</var> expects, an error is signaled.
</p></dd></dl>

<dl class="def">
<dt id="index-let_002a_002dvalues"><span class="category">Macro: </span><span><strong>let*-values</strong> <em>((vars expr) &hellip;) body &hellip;</em><a href='#index-let_002a_002dvalues' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS base]
Same as <code>let-values</code>, but each <var>expr</var>&rsquo;s scope includes
the preceding <var>vars</var>.
</p>
<div class="example">
<pre class="example">(let ((a 1) (b 2) (c 3) (d 4))
  (let*-values (((a b) (values c d))
                ((c d) (values a b)))
    (list a b c d))) &rArr; (3 4 3 4)
</pre></div>
</dd></dl>


<dl class="def">
<dt id="index-rec"><span class="category">Macro: </span><span><strong>rec</strong> <em>var expr</em><a href='#index-rec' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-rec-1"><span class="category">Macro: </span><span><strong>rec</strong> <em>(name . vars) expr &hellip;</em><a href='#index-rec-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[SRFI-31]
A macro to evaluate an expression with recursive reference.
</p>
<p>In the first form, evaluates expr while <var>var</var> in <var>expr</var> is
bound to the result of <var>expr</var>.
The second form is equivalent to the followings.
</p><div class="example">
<pre class="example">(rec <var>name</var> (lambda <var>vars</var> <var>expr</var> &hellip;))
</pre></div>

<p>Some examples:
</p>
<div class="example">
<pre class="example">;; constant infinite stream
(rec s (cons 1 (delay s)))

;; factorial function
(rec (f n)
  (if (zero? n)
      1
      (* n (f (- n 1)))))
</pre></div>
</dd></dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="Grouping.html" accesskey="n" rel="next">Grouping</a>, Previous: <a href="Conditionals.html" accesskey="p" rel="prev">Conditionals</a>, Up: <a href="Core-syntax.html" accesskey="u" rel="up">Core syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
