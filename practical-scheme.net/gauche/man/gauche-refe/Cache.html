<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Cache (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Cache (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Cache (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Library-modules-_002d-Utilities.html" rel="up" title="Library modules - Utilities">
<link href="Heap.html" rel="next" title="Heap">
<link href="Password-hashing.html" rel="prev" title="Password hashing">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Cache">
<div class="header">
<p>
Next: <a href="Heap.html" accesskey="n" rel="next"><code>data.heap</code> - Heap</a>, Previous: <a href="Password-hashing.html" accesskey="p" rel="prev"><code>crypt.bcrypt</code> - Password hashing</a>, Up: <a href="Library-modules-_002d-Utilities.html" accesskey="u" rel="up">Library modules - Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="data_002ecache-_002d-Cache"></span><h3 class="section">12.14 <code>data.cache</code> - Cache</h3>

<dl class="def">
<dt id="index-data_002ecache-1"><span class="category">Module: </span><span><strong>data.cache</strong><a href='#index-data_002ecache-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-data_002ecache"></span>
<p>A cache works similarly as a dictionary, associating keys to values,
but its entries may disappear according to the policy of the cache
algorithm.  This module defines
a common protocol for cache datatypes, and also provides several
typical cache implementations.
</p></dd></dl>

<span id="Examples-2"></span><h4 class="subheading">Examples</h4>

<p>Let&rsquo;s start from simple examples to get the idea.
</p>
<p>Suppose you want to read given files and you want to
cache the frequently read ones.  The following code defines
a cached version of <code>file-&gt;string</code>:
</p>
<div class="example">
<pre class="example">(use data.cache)
(use file.util)

(define file-&gt;string/cached
  (let1 file-cache (make-lru-cache 10 :comparator string-comparator)
    (^[path] (cache-through! file-cache path file-&gt;string))))
</pre></div>

<p>The procedure closes a variable <code>file-cache</code>,
which is an LRU (least recently used) cache that associates
string pathnames to the file contents.
The actual logic is in <code>cache-through!</code>, which first consults the
cache if it has an entry for the <code>path</code>.  If the cache has
the entry, its value (the file content) is returned.  If not,
it calls <code>file-&gt;string</code> with the <var>path</var> to fetch the file content,
register it to the cache, and return it.  The capacity of the cache is
set to 10 (the first argument of <code>make-lru-cache</code>), so when the
11th file is read, the least recently used file will be evicted from the
cache.
</p>
<p>The effect of the cache isn&rsquo;t very visible in the above example.  You
can insert some print stubs to see the cache is actually in action,
as the following example.
Try read various files using <code>file-&gt;string/cached</code>.
</p>
<div class="example">
<pre class="example">(define file-&gt;string/cached
  (let1 file-cache (make-lru-cache 10 :comparator string-comparator)
    (^[path]
      (print #&quot;file-&gt;string/cached called on ~path&quot;)
      (cache-through! file-cache path
                      (^[path]
                        (print #&quot;cache miss.  fetching ~path&quot;)
                        (file-&gt;string path))))))
</pre></div>

<p>Caveat: A cache itself isn&rsquo;t MT-safe.  If you are using it in
multithreaded programs, you have to wrap it with an atom
(see <a href="Threads.html#Synchronization-primitives">Synchronization primitives</a>):
</p>
<div class="example">
<pre class="example">(use data.cache)
(use file.util)
(use gauche.threads)

(define file-&gt;string/cached
  (let1 file-cache (atom (make-lru-cache 10 :comparator string-comparator))
    (^[path]
      (atomic file-cache (cut cache-through! &lt;&gt; path file-&gt;string)))))
</pre></div>

<span id="Common-properties-of-caches"></span><h4 class="subheading">Common properties of caches</h4>

<p>A cache of any kind has a comparator and a storage.
The comparator is used to compare keys; in the example above,
we use <code>string-comparator</code> to compare string pathnames
(see <a href="Equality-and-comparison.html#Basic-comparators">Basic comparators</a>, for more about comparators).
</p>
<p>The storage is a dictionary that maps keys to internal
structures of the cache.  By default, a hashtable is
created automatically using the given comparator (or,
if a comparator is omitted, using <code>default-comparator</code>).
The comparator must have hash function.
</p>
<p>Alternatively, you can give a pre-filled dictionary
(copied from another instance of the same kind of cache)
to start cache with some data already in it.  Note that
what the cache keeps in the dictionary totally depends on the
cache algorithm, so you can&rsquo;t just pass a random dictionary;
it has to be created by the same kind of cache.
If you pass in the storage, the comparator is taken from it.
</p>
<p>Thus, the cache constructors uniformly take keyword arguments
<var>comparator</var> and <var>storage</var>; you can specify either one,
or omit both to use the defaults.
</p>
<span id="Predefined-caches"></span><h4 class="subheading">Predefined caches</h4>

<p>For the <var>storage</var> and <var>comparator</var> keyword arguments, see above.
</p>
<dl class="def">
<dt id="index-make_002dfifo_002dcache"><span class="category">Function: </span><span><strong>make-fifo-cache</strong> <em>capacity :key storage comparator</em><a href='#index-make_002dfifo_002dcache' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
Creates and returns a FIFO (first-in, first-out) cache that can
hold up to <var>capacity</var> entries.
If the number of entries exceeds <var>capacity</var>, the oldest entry
is removed.
</p></dd></dl>

<dl class="def">
<dt id="index-make_002dlru_002dcache"><span class="category">Function: </span><span><strong>make-lru-cache</strong> <em>capacity :key storage comparator</em><a href='#index-make_002dlru_002dcache' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
Creates and returns an LRU (least recently used) cache that can
hold up to <var>capacity</var> entries.
If the number of entries exceeds <var>capacity</var>, the least recently used
entry is removed.
</p></dd></dl>

<dl class="def">
<dt id="index-make_002dttl_002dcache"><span class="category">Function: </span><span><strong>make-ttl-cache</strong> <em>timeout :key storage comparator timestamper</em><a href='#index-make_002dttl_002dcache' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
Creates and returns a TTL (time to live) cache with the timeout value
<var>timeout</var>.
Each entry is timestamped when it&rsquo;s inserted, and it is removed
when the current time passes <var>timeout</var> unit from the timestamp.
The actual entry removal is done when the cache is accessed.
</p>
<p>By default, the Unix system time (seconds from Epoch) is used
as a timestamp, and <var>timeout</var> is in seconds.  It may not be
fine-grained enough if you add multiple entries in shorter intervals
than seconds.   You can customize
it by giving a thunk to <var>timestamper</var>; the thunk is called to
obtain a timestamp, which can be any monotonically increasing real number
(it doesn&rsquo;t need to be associated with physical time).
If you give <var>timestamper</var>, the unit of <var>timeout</var> value should
be the same as whatever <var>timestamper</var> returns.
</p></dd></dl>

<dl class="def">
<dt id="index-make_002dttlr_002dcache"><span class="category">Function: </span><span><strong>make-ttlr-cache</strong> <em>timeout :key storage comparator timestamper</em><a href='#index-make_002dttlr_002dcache' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
A variation of TTL cache, but the entry&rsquo;s timestamp is updated
(refreshed) whenever the entry is read.  Hence we call it TTL with refresh
(TTLR).  But you can also think it as a variation of LRU cache with
timeout.
</p>
<p>The unit of timeout, and the role of <var>timestamper</var> argument, are
the same as <code>make-ttl-cache</code>.
</p></dd></dl>

<span id="Common-operations-of-caches"></span><h4 class="subheading">Common operations of caches</h4>

<p>The following APIs are for the users of a cache.
</p>
<dl class="def">
<dt id="index-cache_002dlookup_0021"><span class="category">Function: </span><span><strong>cache-lookup!</strong> <em>cache key :optional default</em><a href='#index-cache_002dlookup_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
Look for an entry with <var>key</var> in <var>cache</var>, and returns its
value if it exists.  If there&rsquo;s no entry, the procedure returns <var>default</var>
if it is provided, or throws an error otherwise.
</p>
<p>Some types of cache algorithms update <var>cache</var> by this operation, hence
the bang is in the name.
</p></dd></dl>

<dl class="def">
<dt id="index-cache_002dthrough_0021"><span class="category">Function: </span><span><strong>cache-through!</strong> <em>cache key value-fn</em><a href='#index-cache_002dthrough_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
Look for an entry with <var>key</var> in <var>cache</var>, and returns its
value if it exists.  If there&rsquo;s no entry, a procedure <var>value-fn</var>
is called with <var>key</var> as the argument, and its return value
is inserted into <var>cache</var> and also returned.
</p></dd></dl>

<dl class="def">
<dt id="index-cache_002dwrite_0021"><span class="category">Generic function: </span><span><strong>cache-write!</strong> <em>cache key value</em><a href='#index-cache_002dwrite_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
This inserts association of <var>key</var> and <var>value</var> into <var>cache</var>.
If there&rsquo;s already an entry with <var>key</var>, it is overwritten.
Otherwise a new entry is created.
</p>
<p>The same effect can be achieved by calling <code>cache-evict!</code> then
<code>cache-through!</code>, but cache algorithms may provide efficient
way through this method.
</p></dd></dl>

<dl class="def">
<dt id="index-cache_002devict_0021"><span class="category">Generic function: </span><span><strong>cache-evict!</strong> <em>cache key</em><a href='#index-cache_002devict_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
Removes an entry with <var>key</var> from <var>cache</var>, if it exists.
</p></dd></dl>

<dl class="def">
<dt id="index-cache_002dclear_0021"><span class="category">Generic function: </span><span><strong>cache-clear!</strong> <em>cache</em><a href='#index-cache_002dclear_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
Removes all entries from <var>cache</var>.
</p></dd></dl>

<span id="Implementing-a-cache-algorithm"></span><h4 class="subheading">Implementing a cache algorithm</h4>

<p>Each cache algorithm must define a class inheriting <code>&lt;cache&gt;</code>,
and implement the following two essential methods.
The higher-level API calls them.
</p>
<dl class="def">
<dt id="index-cache_002dcheck_0021"><span class="category">Generic function: </span><span><strong>cache-check!</strong> <em>cache key</em><a href='#index-cache_002dcheck_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
Looks for an entry with <var>key</var> in <var>cache</var>.
If it exists, returns a pair of <var>key</var> and the associated value.
Otherwise, returns <code>#f</code>.  It may update the cache,
for example, the timestamp of the entry for being read.
</p></dd></dl>

<dl class="def">
<dt id="index-cache_002dregister_0021"><span class="category">Generic function: </span><span><strong>cache-register!</strong> <em>cache key value</em><a href='#index-cache_002dregister_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
Add an entry with <var>key</var> and associated <var>value</var> into <var>cache</var>.
This is called after <var>key</var> is confirmed not being in <var>cache</var>.
</p></dd></dl>

<p>Additionally, the implementation should consider the following points.
</p>
<ul>
<li> The <code>initialize</code> method must call <code>next-method</code> first, which sets up
the <code>comparator</code> and <code>storage</code> slots.  You should check
if <code>storage</code> has pre-filled entries, and if so, set up other
internal structures appropriately.
</li><li> The default methods of <code>cache-evict!</code> and <code>cache-clear!</code> only
takes care of the storage of the cache.  You should implement them
if your auxiliary structure needs to be taken care of.
</li><li> The default method of <code>cache-write!</code> is just <code>cache-evict!</code>
followed by <code>cache-register!</code>.  You may provide alternative method
if you can do it more efficiently, which is often the case.
</li></ul>

<p>There are several procedures that help implementing cache subclasses:
</p>
<dl class="def">
<dt id="index-cache_002dcomparator"><span class="category">Function: </span><span><strong>cache-comparator</strong> <em>cache</em><a href='#index-cache_002dcomparator' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-cache_002dstorage"><span class="category">Function: </span><span><strong>cache-storage</strong> <em>cache</em><a href='#index-cache_002dstorage' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
Returns the comparator and the storage of the cache, respectively.
</p></dd></dl>

<p>Typical caches may be constructed with a storage (dictionary) and
a queue, where the storage maps keys to <code>(&lt;n&gt; . &lt;value&gt;)</code>,
and queues holds <code>(&lt;key&gt; . &lt;n&gt;)</code>, <code>&lt;n&gt;</code> being a number
(timestamp, counter, etc.)  Here are some common operations work
on this queue-and-dictionary scheme:
</p>
<dl class="def">
<dt id="index-cache_002dpopulate_002dqueue_0021"><span class="category">Function: </span><span><strong>cache-populate-queue!</strong> <em>queue storage</em><a href='#index-cache_002dpopulate_002dqueue_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
You can call this in the <code>initialize</code> method to set up the
queue.  This procedure walks <var>storage</var> (a dictionary that maps
keys to <code>(&lt;n&gt; . &lt;value&gt;)</code>) to construct <code>(&lt;key&gt; . &lt;n&gt;)</code>
pairs, sorts it in increasing order of <code>&lt;n&gt;</code>, and pushes them
into the <code>queue</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-cache_002dcompact_002dqueue_0021"><span class="category">Function: </span><span><strong>cache-compact-queue!</strong> <em>queue storage</em><a href='#index-cache_002dcompact_002dqueue_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
The queue may contain multiple pairs with the same key.  Sometimes the
queue gets to have too many duplicated entries (e.g. the same entry
is read repeatedly, and you push the read timestamp to the queue
for every read).  This procedure scans the queue and removes duplicated entries
but the up-to-date one.  After this operation, the length of the queue
and the number of entries in the storage should match.
</p></dd></dl>

<dl class="def">
<dt id="index-cache_002drenumber_002dentries_0021"><span class="category">Function: </span><span><strong>cache-renumber-entries!</strong> <em>queue storage</em><a href='#index-cache_002drenumber_002dentries_0021' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>{<tt>data.cache</tt>}
This procedure renumbers <code>&lt;n&gt;</code>s in the queue and the storage
starting from 0, without changing their order, and returns the
maximum <code>&lt;n&gt;</code>.  The duplicated entries in the queue is removed
as in <code>cache-compact-queue!</code>.
</p>
<p>When you&rsquo;re using monotonically increasing counter for <code>&lt;n&gt;</code> and
you don&rsquo;t want <code>&lt;n&gt;</code> to get too big (i.e. bignums), you can
call this procedure occasionally to keep <code>&lt;n&gt;</code>&rsquo;s in reasonable range.
</p></dd></dl>


</div>
<hr>
<div class="header">
<p>
Next: <a href="Heap.html" accesskey="n" rel="next"><code>data.heap</code> - Heap</a>, Previous: <a href="Password-hashing.html" accesskey="p" rel="prev"><code>crypt.bcrypt</code> - Password hashing</a>, Up: <a href="Library-modules-_002d-Utilities.html" accesskey="u" rel="up">Library modules - Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
