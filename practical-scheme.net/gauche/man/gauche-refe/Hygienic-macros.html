<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Hygienic macros (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Hygienic macros (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Hygienic macros (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Macros.html" rel="up" title="Macros">
<link href="Traditional-macros.html" rel="next" title="Traditional macros">
<link href="Why-hygienic_003f.html" rel="prev" title="Why hygienic?">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Hygienic-macros">
<div class="header">
<p>
Next: <a href="Traditional-macros.html" accesskey="n" rel="next">Traditional macros</a>, Previous: <a href="Why-hygienic_003f.html" accesskey="p" rel="prev">Why hygienic?</a>, Up: <a href="Macros.html" accesskey="u" rel="up">Macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Hygienic-macros-1"></span><h3 class="section">5.2 Hygienic macros</h3>

<span id="Macro-bindings"></span><h4 class="subheading">Macro bindings</h4>

<p>The following forms establish bindings of <var>name</var> and
a macro transformer created by <var>transformer-spec</var>.  The
binding introduced by these forms shadows
a binding of <var>name</var> established in outer scope, if there&rsquo;s any.
</p>
<p>For toplevel bindings, it will shadow bindings of <var>name</var> imported
or inherited from other modules (see <a href="Modules.html#Modules">Modules</a>).
(Note: This toplevel shadowing behavior is Gauche&rsquo;s extension;
in R7RS, you shouldn&rsquo;t redefine imported bindings, so the portable
code should avoid it.)
</p>
<p>The effect is undefined if you bind the same name more than once
in the same scope.
</p>
<p>The <var>transformer-spec</var> can be either one of <code>syntax-rules</code>
form, <code>er-macro-transformer</code> form, or another macro keyword
or syntactic keyword.  We&rsquo;ll explain them later.
</p>
<dl class="def">
<dt id="index-define_002dsyntax"><span class="category">Special Form: </span><span><strong>define-syntax</strong> <em>name transformer-spec</em><a href='#index-define_002dsyntax' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS base]
If this form appears in toplevel, it binds toplevel <var>name</var> to
a macro transformer defined by <var>transformer-spec</var>.
</p>
<p>If this form appears in the <em>declaration</em> part of
body of <code>lambda</code> (internal define-syntax), <code>let</code> and
other similar forms, it binds <var>name</var> locally within that body.
Conceptually, internal <code>define-syntax</code>es on the same level
are treated like <code>letrec-syntax</code>.  However, mere appearance of
<code>define-syntax</code> does not create another scope; for example,
you can interleave internal <code>define</code> and internal <code>define-syntax</code>
within the same scope.  It is important, though, that the local macros
defined by internal <code>define-syntax</code> should not be required
to expand macro uses before the definition.
</p></dd></dl>

<dl class="def">
<dt id="index-let_002dsyntax"><span class="category">Special Form: </span><span><strong>let-syntax</strong> <em>((name transformer-spec) &hellip;) body</em><a href='#index-let_002dsyntax' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-letrec_002dsyntax"><span class="category">Special Form: </span><span><strong>letrec-syntax</strong> <em>((name transformer-spec) &hellip;) body</em><a href='#index-letrec_002dsyntax' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS base]
Defines local macros.  Each <var>name</var> is bound to a macro
transformer as specified by the corresponding <var>transformer-spec</var>,
then <code>body</code> is expanded.  With <code>let-syntax</code>,
<var>transformer-spec</var> is evaluated with the scope
surrounding <code>let-syntax</code>, while with <code>letrec-syntax</code>
the bindings of <var>name</var>s are included in the scope where
<var>transformer-spec</var> is evaluated.  Thus <code>letrec-syntax</code>
allows mutually recursive macros.
</p></dd></dl>

<span id="Transformer-specs"></span><h4 class="subheading">Transformer specs</h4>

<p>The <var>transformer-spec</var> is a special expression that evaluates
to a macro transformer.  It is evaluated in a different phase
than the other expressions, since macro transformers must be
executed during compiling.  So there are some restrictions.
</p>
<p>At this moment, only one of the following expressions are allowed:
</p>
<ol>
<li> A <code>syntax-rules</code> form.   This is called &ldquo;high-level&rdquo; macro,
for it uses pattern matching entirely, which is basically a
different declarative language from Scheme, thus putting the
complication of the phasing and hygiene issues completely under the hood.
Some kind of macros are easier to write in <code>syntax-rules</code>.
See <a href="#Syntax_002drules-macro-transformer">Syntax-rules macro transformer</a>, for further description.

</li><li> An <code>er-macro-transformer</code> form.  This employs <em>explicit-renaming</em>
(ER) macro, where you can use arbitrary Scheme code to transform
the program, with required renaming to keep hygienity.  The legacy
Lisp macro can also be written with ER macro if you don&rsquo;t use
renaming.  See <a href="#Explicit_002drenaming-macro-transformer">Explicit-renaming macro transformer</a>, for the details.

</li><li> Macro or syntax keyword.  This is Gauche&rsquo;s extension, and can be
used to define alias of existing macro or syntax keyword.
<div class="example">
<pre class="example">(define-syntax si if)
(define écrivez write)

(si (&lt; 2 3) (écrivez &quot;oui&quot;))
</pre></div>
</li></ol>



<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="#Syntax_002drules-macro-transformer" accesskey="1">Syntax-rules macro transformer</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="#Explicit_002drenaming-macro-transformer" accesskey="2">Explicit-renaming macro transformer</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="subsection" id="Syntax_002drules-macro-transformer">
<div class="header">
<p>
Next: <a href="#Explicit_002drenaming-macro-transformer" accesskey="n" rel="next">Explicit-renaming macro transformer</a>, Previous: <a href="#Hygienic-macros" accesskey="p" rel="prev">Hygienic macros</a>, Up: <a href="#Hygienic-macros" accesskey="u" rel="up">Hygienic macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Syntax_002drules-macro-transformer-1"></span><h4 class="subsection">5.2.1 Syntax-rules macro transformer</h4>

<dl class="def">
<dt id="index-syntax_002drules"><span class="category">Special Form: </span><span><strong>syntax-rules</strong> <em>(literal &hellip;) clause clause2 &hellip;</em><a href='#index-syntax_002drules' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-syntax_002drules-1"><span class="category">Special Form: </span><span><strong>syntax-rules</strong> <em>ellipsis (literal &hellip;) clause clause2 &hellip;</em><a href='#index-syntax_002drules-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS base]
This form creates a macro transformer by pattern matching.
</p>
<p>Each <var>clause</var> has the following form:
</p>
<div class="example">
<pre class="example">(<var>pattern</var> <var>template</var>)
</pre></div>

<p>A <var>pattern</var> denotes a pattern to be matched to the macro call.
It is an S-expression that matches if the macro call has the same
structure, except that symbols in <var>pattern</var> can match a whole subtree
of the input; the matched symbol is called a <em>pattern variable</em>,
and can be referenced in the <var>template</var>.
</p>
<p>For example, if a pattern is <code>(_ &quot;foo&quot; (a b))</code>, it can match the
macro call <code>(x &quot;foo&quot; (1 2))</code>, or <code>(x &quot;foo&quot; (1 (2 3)))</code>, but does
not match <code>(x &quot;bar&quot; (1 2))</code>, <code>(x &quot;foo&quot; (1))</code> or
<code>(x &quot;foo&quot; (1 2) 3)</code>.  You can also match repeating structure or
literal symbols; we&rsquo;ll discuss it fully later.
</p>
<p>Clauses are examined in order to see if the macro call form matches
its pattern.  If matching pattern is found, the corresponding <var>template</var>
replaces the macro call form.  A pattern variable in the template is
replaced with the subtree of input that is bound to the pattern variable.
</p>
<p>Here&rsquo;s a definition of <code>when</code> macro in <a href="Why-hygienic_003f.html">Why hygienic?</a>,
using <code>syntax-rules</code>:
</p>
<div class="example">
<pre class="example">(define-syntax when
  (syntax-rules ()
    [(_ test body ...) (if test (begin body ...))]))
</pre></div>

<p>The pattern is <code>(_ test body ...)</code>, and the template is
<code>(if test (begin body ...))</code>.
The ellipsis <code>...</code> is a symbol; we&rsquo;re not omitting code here.
It denotes that the previous pattern (<code>body</code>) may
repeat zero or more times.
</p>
<p>So, if the <code>when</code> macro is called as
<code>(when (zero? x) (print &quot;huh?&quot;) (print &quot;we got zero!&quot;))</code>,
the macro expander first check if the input matches the pattern.
</p>
<ul>
<li> The <var>test</var> in pattern matches the input <code>(zero? x)</code>.
</li><li> The <var>body</var> in pattern matches the input <code>(print &quot;huh?&quot;)</code>
<em>and</em> <code>(print &quot;we got zero!&quot;)</code>.
</li></ul>

<p>The matching of <var>body</var> is a bit tricky; as a pattern variable,
you may think that <var>body</var> works like an array variable, each element
holds each match&mdash;and you can use them in similarly
repeating substructures in template.
Let&rsquo;s see the template, now that the input fully matched the pattern.
</p>
<ul>
<li> In the template, <code>if</code> and <code>begin</code> are not pattern variable,
since they are not appeared in the pattern.
So they are inserted as identifiers&mdash;that is, hygienic symbols effectively
renamed to make sure to refer to the global <code>if</code> and <code>begin</code>,
and will be unaffected by the macro use environment.
</li><li> The <var>test</var> in the template is a pattern variable, so it is replaced
for the matched value, <code>(zero? x)</code>.
</li><li> The <var>body</var> is also a pattern variable.  The important point is
that it is also followed by ellipsis.  So we repeat <var>body</var> as many
times as the number of matched values.
The first value, <code>(print &quot;huh?&quot;)</code>, and the second value,
<code>(print &quot;we got zero!&quot;)</code>, are expanded here.
</li><li> Hence, we get
<code>(if (zero? x) (begin (print &quot;huh?&quot;) (print &quot;we got zero!&quot;)))</code>
as the result of expansion.  (With the note that <code>if</code> and <code>begin</code>
refers to the identifiers visible from the macro definition environment.)
</li></ul>

<p>The expansion of ellipses is quite powerful.  In the template,
the ellipses don&rsquo;t need to follow the sequence-valued pattern variable
immediately; the variable can be in a substructure, as long as the
substructure itself is followed by an ellipsis.
See the following example:
</p>
<div class="example">
<pre class="example">(define-syntax show
  (syntax-rules ()
    [(_ expr ...)
     (begin
       (begin (write 'expr) (display &quot;=&quot;) (write expr) (newline))
       ...)]))
</pre></div>

<p>If you call this macro as follows:
</p>
<div class="example">
<pre class="example">(show (+ 1 2) (/ 3 4))
</pre></div>

<p>It is expanded to the following form, modulo hygienity:
</p>
<div class="example">
<pre class="example">(begin
  (begin (write '(+ 1 2)) (display &quot;=&quot;) (write (+ 1 2)) (newline))
  (begin (write '(/ 3 4)) (display &quot;=&quot;) (write (/ 3 4)) (newline)))
</pre></div>

<p>So you&rsquo;ll get this output.
</p>
<div class="example">
<pre class="example">(+ 1 2)=3
(/ 3 4)=3/4
</pre></div>

<p>You can also match with a repetition of substructures in the pattern.
The following example is a simplified <code>let</code> that expands to
<code>lambda</code>:
</p>
<div class="example">
<pre class="example">(define-syntax my-let
  (syntax-rules ()
    [(_ ((var init) ...) body ...)
     ((lambda (var ...) body ...) init ...)]))
</pre></div>

<p>If you call it as <code>(my-let ((a expr1) (b expr2)) foo)</code>,
then <var>var</var> is matched to <code>a</code> and <code>b</code>,
while <var>init</var> is matched to <code>expr1</code> and <code>expr2</code>, respectively.
They can be used separately in the template.
</p>
<p>Suppose &ldquo;level&rdquo; of a pattern variable
means the number of nested ellipses that designate repetition of the pattern
variable.
A subtemplate can be followed as many ellipses as the maximum level of
pattern variables in the subtemplate.
In the following example, the level of pattern variable <code>a</code> is 1
(it is repeated by the last ellipsis in the pattern),
while the level of <code>b</code> is 2 (repeated by the last two ellipses),
and the level of <code>c</code> is 3 (repeated by all the ellipses).
</p>
<div class="example">
<pre class="example">(define-syntax ellipsis-test
  (syntax-rules ()
    [(_ (a (b c ...) ...) ...)
     '((a ...)
       (((a b) ...) ...)
       ((((a b c) ...) ...) ...))]))
</pre></div>

<p>In this case, the subtemplate <code>a</code> must be repeated by one level
of ellipsis, <code>(a b)</code> must be repeated by two,
and <code>(a b c)</code> must be repeated by three.
</p>
<div class="example">
<pre class="example">(ellipsis-test (1 (2 3 4) (5 6)) (7 (8 9 10 11)))
 &rArr; ((1 7)
    (((1 2) (1 5)) ((7 8)))
    ((((1 2 3) (1 2 4)) ((1 5 6))) (((7 8 9) (7 8 10) (7 8 11)))))
</pre></div>

<p>In the template, more than one ellipsis directly follow a subtemplate,
splicing the leaves into the surrounding list:
</p>
<div class="example">
<pre class="example">(define-syntax my-append
  (syntax-rules ()
    [(_ (a ...) ...)
     '(a ... ...)]))

(my-append (1 2 3) (4) (5 6))
  &rArr; (1 2 3 4 5 6)

(define-syntax my-append2
  (syntax-rules ()
    [(_ ((a ...) ...) ...)
     '(a ... ... ...)]))

(my-append2 ((1 2) (3 4)) ((5) (6 7 8)))
  &rArr; (1 2 3 4 5 6 7 8)
</pre></div>

<p>Note: Allowing multiple ellipses to directly follow a subtemplate,
and a pattern variable in a subtemplate to be enclosed within more
than the variable&rsquo;s level of nesting of ellipses, are extension to
R7RS, and defined in SRFI-149.  In the above examples,
<code>ellipsis-test</code>, <code>my-append</code> and <code>my-append2</code> are
outside of R7RS.
</p>
<p>Identifiers in a pattern is treated as pattern variables.  But sometimes
you want to match a specific identifier in the input.  For example,
the built-in <code>cond</code> and <code>case</code> detects an identifier <code>else</code>
as a special identifier.  You can use <var>literal</var> &hellip; for that.
See the following example.
</p>
<div class="example">
<pre class="example">(define-syntax if+
  (syntax-rules (then else)
    [(_ test then expr1 else expr2) (if test expr1 expr2)]))
</pre></div>

<p>The identifiers listed as the literals don&rsquo;t become pattern variables,
but literally match the input.  If the input doesn&rsquo;t have the same identifier
in the position, match fails.
</p>
<div class="example">
<pre class="example">(if+ (even? x) then (/ x 2) else (/ (+ x 1) 2))
 <span class="roman">expands into</span> (if (even? x) (/ x 2) (/ (+ x 1) 2))

(if+ (even? x) foo (/ x 2) bar (/ (+ x 1) 2))
 &rArr; ERROR: malformed if+
</pre></div>

<p>We&rsquo;ve been saying identifiers instead of symbols.  Roughly speaking,
an identifier is a symbol with the surrounding syntactic environment,
so that they can keep identity under renaming of hygiene macro.
</p>
<p>The following example fails, because the <code>else</code> passed to the
<code>if+</code> macro is the one locally bound by <code>let</code>, which is
different from the global <code>else</code> when <code>if+</code> was defined,
hence they don&rsquo;t match.
</p>
<div class="example">
<pre class="example">(let ((else #f))
  (if+ (even? x) then (/ x 2) else (/ (+ x 1) 2))
  &rArr; ERROR: malformed if+
</pre></div>
</dd></dl>

<hr>
</div>
<div class="subsection" id="Explicit_002drenaming-macro-transformer">
<div class="header">
<p>
Previous: <a href="#Syntax_002drules-macro-transformer" accesskey="p" rel="prev">Syntax-rules macro transformer</a>, Up: <a href="#Hygienic-macros" accesskey="u" rel="up">Hygienic macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Explicit_002drenaming-macro-transformer-1"></span><h4 class="subsection">5.2.2 Explicit-renaming macro transformer</h4>

<dl class="def">
<dt id="index-er_002dmacro_002dtransformer"><span class="category">Special Form: </span><span><strong>er-macro-transformer</strong> <em>procedure-expr</em><a href='#index-er_002dmacro_002dtransformer' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Creates a macro transformer from the given <var>procedure-expr</var>.
The created macro transformer has to be bound to the syntactic keyword
by <code>define-syntax</code>, <code>let-syntax</code> or <code>letrec-syntax</code>.
Other use of macro transformers is undefined.
</p>
<p>The <var>procedure-expr</var> must evaluate to a procedure that takes
three arguments; <var>form</var>, <var>rename</var> and <var>id=?</var>.
</p>
<p>The <var>form</var> argument receives the S-expression of
the macro call.  The <var>procedure-expr</var> must return an
S-expression as the result of macro expansion.  This part is pretty much
like the traditional lisp macro.  In fact, if you ignore <var>rename</var>
and <var>id=?</var>, the semantics is the same as the traditional
(unhygienic) macro.  See the following example
(Note the use of <code>match</code>; it is a good
tool to decompose macro input):
</p>
<div class="example">
<pre class="example">(use util.match)

;; Unhygienic 'when-not' macro
(define-syntax when-not
  (er-macro-transformer
    (^[form rename id=?]
      (match form
        [(_ test expr1 expr ...)
         `(if (not ,test) (begin ,expr1 ,@expr))]
        [_ (error &quot;malformed when-not:&quot; form)]))))

(macroexpand '(when-not (foo) (print &quot;a&quot;) 'boo))
  &rArr; (if (not (foo)) (begin (print &quot;a&quot;) 'boo))
</pre></div>

<p>This is ok as long as you know you don&rsquo;t need hygiene&mdash;e.g. when
you only use this macro locally in your code, knowing all the
macro call site won&rsquo;t contain name conflicts.  However, if you
provide your <code>when-not</code> macro for general use,
you have to protect namespace pollution around the macro use.
For example, you want to make sure your macro work even if it is
used as follows:
</p>
<div class="example">
<pre class="example">(let ((not values))
  (when-not #t (print &quot;This shouldn't be printed&quot;)))
</pre></div>

<p>The <var>rename</var> argument passed to <var>procedure-expr</var> is
a procedure that takes a symbol (or, to be precise, a symbol or
an identifier) and <em>effectively renames</em> it to a unique
identifier that keeps identity within the macro definition environment and
won&rsquo;t be affected in the macro use environment.
</p>
<p>As a rule of thumb, you have to pass
<em>all new identifiers you insert into macro output</em> to the
<var>rename</var> procedure to keep hygiene.  In our <code>when-not</code> macro,
we insert <code>if</code>, <code>not</code> and <code>begin</code> into the macro output,
so our hygienic macro would look like this:
</p>
<div class="example">
<pre class="example">(define-syntax when-not
  (er-macro-transformer
    (^[form rename id=?]
      (match form
        [(_ test expr1 expr ...)
         `(,(rename 'if) (,(rename 'not) ,test)
            (,(rename 'begin) ,expr1 ,@expr))]
        [_ (error &quot;malformed when-not:&quot; form)]))))
</pre></div>

<p>This is cumbersome and makes it hard to read the macro, so Gauche
provides an auxiliary macro <code>quasirename</code>, which works like
<code>quasiquote</code> but renaming identifiers in the form.  See the
entry of <code>quasirename</code> below for the details.  You can write
the hygienic <code>when-not</code> as follows:
</p>
<div class="example">
<pre class="example">(define-syntax when-not
  (er-macro-transformer
    (^[form rename id=?]
      (match form
        [(_ test expr1 expr ...)
         (quasirename rename
           `(if (not ,test) (begin ,expr1 ,@expr)))]
        [_ (error &quot;malformed when-not:&quot; form)]))))
</pre></div>

<p>You can intentionally break hygiene by inserting a symbol
without renaming.  The following code implements
<em>anaphoric</em> <code>when</code>, meaning the result of the
test expression is available in the <var>expr1</var> <var>exprs</var> &hellip;
with the name <code>it</code>.  Since the binding of the identifier <code>it</code>
does not exist in the macro use site, but rather injected into
the macro use site by the macro expander, it is unhygienic.
</p>
<div class="example">
<pre class="example">(define-syntax awhen
  (er-macro-transformer
    (^[form rename id=?]
      (match form
        [(_ test expr1 expr ...)
         `(,(rename 'let1) it ,test     ; 'it' is not renamed
             (,(rename 'begin) ,expr1 ,@expr))]))))
</pre></div>

<p>If you use <code>quasirename</code>, you can write <code>,'it</code> to prevent
<code>it</code> from being renamed:
</p>
<div class="example">
<pre class="example">(define-syntax awhen
  (er-macro-transformer
    (^[form rename id=?]
      (match form
        [(_ test expr1 expr ...)
         (quasirename rename
           `(let1 ,'it ,test
              (begin ,expr1 ,@expr)))]))))
</pre></div>

<p>Here&rsquo;s an example:
</p>
<div class="example">
<pre class="example">(awhen (find odd? '(0 2 8 7 4))
  (print &quot;Found odd number:&quot; it))
 &rArr; <span class="roman">prints</span> Found odd number:7
</pre></div>

<p>Finally, the <var>id=?</var> argument to the <var>procedure-expr</var> is
a procedure that takes two arguments, and returns <code>#t</code> iff
both are identifiers and either both are referring to the same binding
or both are free.  It can be used to compare literal syntactic keyword
(e.g. <code>else</code> in <code>cond</code> and <code>case</code> forms) hygienically.
</p>
<p>The following <code>if=&gt;</code> macro behaves like <code>if</code>, except that
it accepts <code>(if=&gt; test =&gt; procedure)</code> syntax,
in which <code>procedure</code> is called with the value of <code>test</code>
if it is not false (similar to <code>(cond [test =&gt; procedure])</code> syntax).
The symbol <code>=&gt;</code> must match hygienically,
that is, it must refer to the same binding as in the macro definition.
</p>
<div class="example">
<pre class="example">(define-syntax if=&gt;
  (er-macro-transformer
    (^[form rename id=?]
      (match form
        [(_ test a b)
         (if (id=? (rename '=&gt;) a)
           (quasirename rename
             `(let ((t ,test))
                (if t (,b t))))
           (quasirename rename
             `(if ,test ,a ,b)))]))))
</pre></div>

<p>The call <code>(rename '=&gt;)</code> returns an identifier that captures
the binding of <code>=&gt;</code> in the macro definition, and using
<code>id=?</code> with the thing passed to the macro argument
checks if both refer to the same binding.
</p>
<div class="example">
<pre class="example">(if=&gt; 3 =&gt; list)  &rArr; (3)
(if=&gt; #f =&gt; list) &rArr; #&lt;undef&gt;

;; If the second argument isn't =&gt;, if=&gt; behaves like ordinary if:
(if=&gt; #t 1 2)     &rArr; 1

;; The binding of =&gt; in macro use environment differs from
;; the macro definition environment, so this if=&gt; behaves like
;; ordinary if, instead of recognizing literal =&gt;.
(let ((=&gt; 'oof)) (if=&gt; 3 =&gt; list)) &rArr; oof
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-quasirename"><span class="category">Macro: </span><span><strong>quasirename</strong> <em>renamer quasiquoted-form</em><a href='#index-quasirename' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>It works like quasiquote, except that the symbols and identifiers
that appear in the &ldquo;literal&rdquo; portion of <var>form</var> (i.e. outside
of <code>unquote</code> and <code>unquote-splicing</code>) are replaced
by the result of applying <var>rename</var> on themselves.
</p>
<p>The <var>quasiquote-form</var> argument must be a quasiquoted form.
The outermost quasiquote <code>`</code> is consumed by <code>quasirename</code> and
won&rsquo;t appear in the output.
The reason we require it is to make nested quasiquotes/quasirenames work.
</p>
<p>For example, a form:
</p><div class="example">
<pre class="example">(quasirename r `(a ,b c &quot;d&quot;))
</pre></div>
<p>would be equivalent to write:
</p><div class="example">
<pre class="example">(list (r 'a) b (r 'c) &quot;d&quot;)
</pre></div>

<p>This is not specifically tied to macros; the <var>renamer</var> can
be any procedure that takes one symbol or identifier argument:
</p>
<div class="example">
<pre class="example">(quasirename (^[x] (symbol-append 'x: x)) `(+ a ,(+ 1 2) 5))
  &rArr; (x:+ x:a 3 5)
</pre></div>

<p>However, it comes pretty handy to construct the result form
in ER macros.  Compare the following two:
</p>
<div class="example">
<pre class="example">(use util.match)

;; using quasirename
(define-syntax swap
  (er-macro-transformer
    (^[f r c]
      (match f
        [(_ a b) (quasirename r
                   `(let ((tmp ,a))
                      (set! ,a ,b)
                      (set! ,b tmp)))]))))

;; not using quasirename
(define-syntax swap
  (er-macro-transformer
    (^[f r c]
      (match f
        [(_ a b) `((r'let) (((r'tmp) ,a))
                     ((r'set!) ,a ,b)
                     ((r'set!) ,b (r'tmp)))]))))
</pre></div>

<p>Note: In Gauche 0.9.7 and before, <code>quasirename</code> didn&rsquo;t use
quasiquoted form as the second argument; you can write
<code>(quasirename r form)</code> instead of
<code>(quasirename r `form)</code>.
</p>
<p>For the backward compatibility,
we support the form without quasiquote by default for a while.
</p>
<p>If you already have a quasirename form that does intend to produce
a quasiquoted form, you have to rewrite it with double quasiquote:
<code>(quasirename r ``form)</code>.
</p>
<p>To help transition, the handling of quasiquote in of <code>quasirename</code>
can be customized with the environment variable
<code>GAUCHE_QUASIRENAME_MODE</code>.  It can have one of the following
values:
</p>
<dl compact="compact">
<dt><span><code>legacy</code></span></dt>
<dd><p><code>Quasirename</code> behaves the same way as
0.9.7 and before; use this to run code for 0.9.7 without any change.
</p></dd>
<dt><span><code>compatible</code></span></dt>
<dd><p><code>Quasirename</code> behaves as described in this entry; if
<var>form</var> lacks a quasiquote, it silently assumes one.
Existing code should work, except the rare case when you intend
to return a quasiquoted form.
</p></dd>
<dt><span><code>warn</code></span></dt>
<dd><p><code>Quasirename</code> behaves as described in this entry, but
warns if <var>form</var> lacks a quasiquote.
</p></dd>
<dt><span><code>strict</code></span></dt>
<dd><p><code>Quasirename</code> raises an error if <var>form</var> lacks a quasiquote.
This will be the default behavior in future.
</p></dd>
</dl>
</dd></dl>

</div>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Traditional-macros.html" accesskey="n" rel="next">Traditional macros</a>, Previous: <a href="Why-hygienic_003f.html" accesskey="p" rel="prev">Why hygienic?</a>, Up: <a href="Macros.html" accesskey="u" rel="up">Macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
