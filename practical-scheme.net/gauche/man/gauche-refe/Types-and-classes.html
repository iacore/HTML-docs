<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Types and classes (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Types and classes (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Types and classes (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Core-library.html" rel="up" title="Core library">
<link href="Equality-and-comparison.html#Equality-and-comparison" rel="next" title="Equality and comparison">
<link href="Core-library.html" rel="prev" title="Core library">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Types-and-classes">
<div class="header">
<p>
Next: <a href="Equality-and-comparison.html#Equality-and-comparison" accesskey="n" rel="next">Equality and comparison</a>, Previous: <a href="Core-library.html" accesskey="p" rel="prev">Core library</a>, Up: <a href="Core-library.html" accesskey="u" rel="up">Core library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Types-and-classes-1"></span><h3 class="section">6.1 Types and classes</h3>

<p>Scheme is a dynamically and strongly typed language.  That is,
every value <em>knows</em> its type at run-time, and the type
determines what kind of operations can be applied on the value.
</p>
<p>The Scheme standard is pretty simple on types; basically,
an object being a type means the type predicate returns true on the object,
and that&rsquo;s all.  Gauche adopts a bit more elaborated system&mdash;types are
first-class objects and you can query various information.
</p>
<p>In Gauche, types are conventionally
named with brackets <code>&lt;</code> and <code>&gt;</code>, e.g. <code>&lt;string&gt;</code>.
It&rsquo;s nothing syntactically special with these brackets; they&rsquo;re valid
characters to consist of variable names.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="#Prescriptive-and-descriptive-types" accesskey="1">Prescriptive and descriptive types</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="#Generic-type-predicates" accesskey="2">Generic type predicates</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="#Predefined-classes" accesskey="3">Predefined classes</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="#Type-expressions-and-type-constructors" accesskey="4">Type expressions and type constructors</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="#Native-types" accesskey="5">Native types</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="subsection" id="Prescriptive-and-descriptive-types">
<div class="header">
<p>
Next: <a href="#Generic-type-predicates" accesskey="n" rel="next">Generic type predicates</a>, Previous: <a href="#Types-and-classes" accesskey="p" rel="prev">Types and classes</a>, Up: <a href="#Types-and-classes" accesskey="u" rel="up">Types and classes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Prescriptive-and-descriptive-types-1"></span><h4 class="subsection">6.1.1 Prescriptive and descriptive types</h4>

<p>Types are used in two ways.  A type can be seen as a template of
the actual values (instances)&mdash;that is, a type <em>prescribes</em>
the structure of, and possible operations on, its instances.
In Gauche, such prescriptive types are represented by <em>classes</em>.
Every value in Gauche belongs to a class, which can be queried with the
<code>class-of</code> procedure.  You can also define your own class with
<code>define-class</code> or <code>define-record-type</code>
(see <a href="Object-system.html">Object system</a>, and see <a href="Record-types.html#Record-types"><code>gauche.record</code> - Record types</a>).
</p>
<p>A type can also be seen as a constraint of a given
expression&mdash;that is, a type <em>describes</em> what characteristics
a certain expression must have.   Gauche has entities
to represent such descriptive types separate from classes.
Descriptive types are created with <em>type constructors</em>
(see <a href="#Type-expressions-and-type-constructors">Type expressions and type constructors</a>).
We also have a set of predefined descriptive types to communicate
to C libraries (see <a href="#Native-types">Native types</a>.
</p>
<p>A value is an instance of a class. For example,
<code>1</code> is an instance of <code>&lt;integer&gt;</code>, and <code>&quot;xyz&quot;</code>
is an instance of <code>&lt;string&gt;</code>.  This presctiptive type relationship
is checked with <code>is-a?</code> procedure: <code>(is-a? 1 &lt;integer&gt;)</code> and
<code>(is-a? &quot;xyz&quot; &lt;string&gt;)</code> both returns <code>#t</code>.
</p>
<p>On the other hand, you may have a procedure that takes either a number
or a string as an argument.  &ldquo;A number or a string&rdquo; is a type constraint,
and can be expressed as a descriptive type, <code>(&lt;/&gt; &lt;number&gt; &lt;string&gt;)</code>.
Descriptive type relationship is checked with <code>of-type?</code> procedure:
<code>(of-type? 1 (&lt;/&gt; &lt;number&gt; &lt;string&gt;))</code> and
<code>(of-type? &quot;xyz&quot; (&lt;/&gt; &lt;number&gt; &lt;string&gt;))</code> both returns <code>#t</code>.
More conveniently, <code>(assume-type arg (&lt;/&gt; &lt;number&gt; &lt;string&gt;))</code>
would raise an error if <var>arg</var> doesn&rsquo;t satisfy the given type constraint.
</p>
<hr>
</div>
<div class="subsection" id="Generic-type-predicates">
<div class="header">
<p>
Next: <a href="#Predefined-classes" accesskey="n" rel="next">Predefined classes</a>, Previous: <a href="#Prescriptive-and-descriptive-types" accesskey="p" rel="prev">Prescriptive and descriptive types</a>, Up: <a href="#Types-and-classes" accesskey="u" rel="up">Types and classes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Generic-type-predicates-1"></span><h4 class="subsection">6.1.2 Generic type predicates</h4>

<p>A &ldquo;type predicate&rdquo; is a predicate that tells if an object is of a specific
type; e.g. <code>number?</code> tells you if the argument is a number.
Since types are first-class in Gauche, we have predicates that can tell
an object is of a <em>given</em> type, as well as predicates to ask
the relationship between types.
</p>
<dl class="def">
<dt id="index-is_002da_003f"><span class="category">Function: </span><span><strong>is-a?</strong> <em>obj class</em><a href='#index-is_002da_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>This is a prescriptive types predicate.
Returns true iff <var>obj</var> is an instance of <var>class</var> or an instance
of descendants of <var>class</var>.
</p>
<div class="example">
<pre class="example">(is-a? 3 &lt;integer&gt;)   &rArr; #t
(is-a? 3 &lt;real&gt;)      &rArr; #t
(is-a? 5+3i &lt;real&gt;)   &rArr; #f
</pre></div>

<p>Note: If <var>obj</var>&rsquo;s class has been redefined, <code>is-a?</code> also
triggers instance update.
See <a href="Class.html#Class-redefinition">Class redefinition</a> for the details.
</p></dd></dl>

<dl class="def">
<dt id="index-of_002dtype_003f"><span class="category">Function: </span><span><strong>of-type?</strong> <em>obj type</em><a href='#index-of_002dtype_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>This is a descriptie type predicate.
Returns true iff <var>obj</var> satisfies the constraints described by <var>type</var>,
which can be either a class (in that case, this is the same as <code>is-a?</code>),
or a descriptive type (see <a href="#Type-expressions-and-type-constructors">Type expressions and type constructors</a> below).
</p>
<div class="example">
<pre class="example">(of-type? 1   (&lt;/&gt; &lt;number&gt; &lt;string&gt;)) &rArr; #t
(of-type? &quot;a&quot; (&lt;/&gt; &lt;number&gt; &lt;string&gt;)) &rArr; #t
(of-type? 'a  (&lt;/&gt; &lt;number&gt; &lt;string&gt;)) &rArr; #f
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-subtype_003f"><span class="category">Function: </span><span><strong>subtype?</strong> <em>sub super</em><a href='#index-subtype_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Returns <code>#t</code> if a type <var>sub</var> is a subtype of a type <var>super</var>
(includes the case that <var>sub</var> is <var>super</var>).
Otherwise, returns <code>#f</code>.
</p>
<p>In general, if <var>sub</var> is a subtype of <var>super</var>, an object that satisfies
the constraints of <var>sub</var> also satisfies the constraints of <var>super</var>,
so you can use the object where objects of type <var>super</var> are expected.
In other words, <var>sub</var> is more restrictive than <var>super</var>.
If both <var>sub</var> and <var>super</var> are classes, <var>sub</var> being a subtype
of <var>super</var> means <var>sub</var> is a subclass of <var>super</var>.
</p>
<div class="example">
<pre class="example">(subtype? &lt;integer&gt; &lt;real&gt;) &rArr; #t
(subtype? &lt;char&gt; (&lt;/&gt; &lt;char&gt; &lt;string&gt;)) &rArr; #t
(subtype? (&lt;/&gt; &lt;integer&gt; &lt;string&gt;) (&lt;/&gt; &lt;number&gt; &lt;string&gt;)) &rArr; #t
</pre></div>

<p>Note that we&rsquo;re not rigorous on this &ldquo;substitution principle&rdquo;,
for we don&rsquo;t aim at guaranteeing type safety through static analysis.
For example, &ldquo;list of integers&rdquo; can be used in place of a generic list
most of the time, and <code>(subtype? (&lt;List&gt; &lt;integer&gt;) &lt;list&gt;)</code> is
<code>#t</code>.  However, if list is mutated, you can&rsquo;t replace
generic list with a list of integers&mdash;because mutators may try to
set non-integer in the list.   That kind of cases needs to be handled
separately, not on relying solely on <code>subtype?</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-subclass_003f"><span class="category">Function: </span><span><strong>subclass?</strong> <em>sub super</em><a href='#index-subclass_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Both arguments must be classes.  Returns <code>#t</code> iff <var>sub</var> is
a subclass of <var>super</var>.  A class is regarded as a subclass of
itself.
</p></dd></dl>


<hr>
</div>
<div class="subsection" id="Predefined-classes">
<div class="header">
<p>
Next: <a href="#Type-expressions-and-type-constructors" accesskey="n" rel="next">Type expressions and type constructors</a>, Previous: <a href="#Generic-type-predicates" accesskey="p" rel="prev">Generic type predicates</a>, Up: <a href="#Types-and-classes" accesskey="u" rel="up">Types and classes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Predefined-classes-1"></span><h4 class="subsection">6.1.3 Predefined classes</h4>

<p>We&rsquo;ll introduce classes for each built-in type as we go through
this chapter.  Here are a few basic classes to start with:
</p>
<dl class="def">
<dt id="index-_003ctop_003e"><span class="category">Builtin Class: </span><span><strong>&lt;top&gt;</strong><a href='#index-_003ctop_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-top"></span>
<p>This class represents the supertype of all the types in Gauche.
That is, for any class <code>X</code>, <code>(subtype? X &lt;top&gt;)</code> is <code>#t</code>,
and for any object <code>x</code>, <code>(is-a? x &lt;top&gt;)</code> is <code>#t</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-_003cbottom_003e"><span class="category">Builtin Class: </span><span><strong>&lt;bottom&gt;</strong><a href='#index-_003cbottom_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-bottom"></span>
<p>This class represents the subtype of all the types in Gauche.
For any class <code>X</code>, <code>(subtype? &lt;bottom&gt; X)</code> is <code>#t</code>,
and for any object <code>x</code>, <code>(is-a? x &lt;bottom&gt;)</code> is <code>#f</code>.
</p>
<p>There&rsquo;s no instance of <code>&lt;bottom&gt;</code>.
</p>
<p>Note: Although <code>&lt;bottom&gt;</code> is subtype of other types,
the class precedence list (CPL) of <code>&lt;bottom&gt;</code> only contains
<code>&lt;bottom&gt;</code> and <code>&lt;top&gt;</code>.  It&rsquo;s because it isn&rsquo;t
always possible to calculate a linear list of all the types.
Even if it is possible, it would be expensive to check and update the
CPL of <code>&lt;bottom&gt;</code> every time a new class is defined or
an existing class is redefined.   Procedures <code>subtype?</code> and
<code>is-a?</code> treat <code>&lt;bottom&gt;</code> specially.
</p>
<p>One of use case of <code>&lt;bottom&gt;</code> is <code>applicable?</code> procedure.
See <a href="Procedures-and-continuations.html#Procedure-class-and-applicability">Procedure class and applicability</a>.
</p></dd></dl>

<dl class="def">
<dt id="index-_003cobject_003e"><span class="category">Builtin Class: </span><span><strong>&lt;object&gt;</strong><a href='#index-_003cobject_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-object"></span>
<p>This class represents a supertype of all user-defined classes.
</p></dd></dl>

<dl class="def">
<dt id="index-class_002dof"><span class="category">Function: </span><span><strong>class-of</strong> <em>obj</em><a href='#index-class_002dof' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Returns a class metaobject of <var>obj</var>.
</p>
<div class="example">
<pre class="example">(class-of 3)         &rArr; #&lt;class &lt;integer&gt;&gt;
(class-of &quot;foo&quot;)     &rArr; #&lt;class &lt;string&gt;&gt;
(class-of &lt;integer&gt;) &rArr; #&lt;class &lt;class&gt;&gt;
</pre></div>

<p>Note: In Gauche, you can redefine existing user-defined classes.
If the new definition has different configuration of the instance,
<code>class-of</code> on existing instance triggers instance updates;
see <a href="Class.html#Class-redefinition">Class redefinition</a> for the details.  Using
<code>current-class-of</code> suppresses instance updates
(see <a href="Instance.html#Accessing-instance">Accessing instance</a>).
</p></dd></dl>

<hr>
</div>
<div class="subsection" id="Type-expressions-and-type-constructors">
<div class="header">
<p>
Next: <a href="#Native-types" accesskey="n" rel="next">Native types</a>, Previous: <a href="#Predefined-classes" accesskey="p" rel="prev">Predefined classes</a>, Up: <a href="#Types-and-classes" accesskey="u" rel="up">Types and classes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Type-expressions-and-type-constructors-1"></span><h4 class="subsection">6.1.4 Type expressions and type constructors</h4>

<p>Types are first-class objects manipulatable at runtime in Gauche,
but they must be known at compile-time
in order to do optimizations or static analysis.  In certain places,
Gauche requires the the value of type-yielding expressions to be statically
computable.  We call such expressions <em>type expressions</em>.
</p>
<p>A type expression is either a global variable reference that are
constantly bound to a type, or a call of type constructor:
</p>
<div class="example">
<pre class="example">&lt;type-expression&gt; : &lt;global-variable-constantly-bound-to-a-type&gt;
                  | &lt;type-constructor-call&gt;

&lt;type-constructor-call&gt; : (&lt;type-constructor&gt; &lt;type-constructor-argument&gt; ...)

&lt;type-constructor-argument&gt; : &lt;type-expression&gt;
                            | &lt;integer-constant&gt;
                            | -&gt;
                            | *
</pre></div>

<p>All built-in classes, such as <code>&lt;integer&gt;</code>, are statically bound
(in precise terms, they are &ldquo;inlinable&rdquo; binding).  If you try to alter
it, a warning is issued, and the further bahvior will be undefined.
In future, we&rsquo;ll make it an error to alter the binding of
global variables bount to types.  Classes defined with <code>define-class</code> and
<code>define-record-type</code> are also bound as inlinable.
</p>
<p>Type constructors are special classes whose instances are
descriptive types.  Type constructors can be invoked as if they are
a procedure, but &lt;type-constructor-call&gt; above is recognized by
the compiler and the derived type is computed at compile-time.
So, at runtime you only see the resulting derived type instance.
</p>
<p>For example, <code>&lt;?&gt;</code> is a type constructor that creates
&ldquo;maybe&rdquo;-like type, e.g. &ldquo;an integer or <code>#f</code>&rdquo;.
(Do not confuse this with Maybe type
defined in SRFI-189, see <a href="Maybe-and-Either-optional-container-types.html#Maybe-and-Either-optional-container-types"><code>srfi.189</code> - Maybe and Either: optional container types</a>).
A type expression <code>(&lt;?&gt; &lt;integer&gt;)</code> yields such type
(printed as <code>#&lt;? &lt;integer&gt;&gt;</code>):
</p>
<div class="example">
<pre class="example">(&lt;?&gt; &lt;integer&gt;) &rArr; #&lt;? &lt;integer&gt;&gt;
</pre></div>

<p>It looks like a procedure call, but it s computed at compile time:
</p>
<div class="example">
<pre class="example">gosh&gt; (disasm (^[] (&lt;?&gt; &lt;integer&gt;)))
CLOSURE #&lt;closure (#f)&gt;
=== main_code (name=#f, cc=0x7f5cd76ab540, codevec=...):
signatureInfo: ((#f))
     0 CONST-RET #&lt;? &lt;integer&gt;&gt;
</pre></div>

<dl class="def">
<dt id="index-_003c_003f_003e"><span class="category">Type Constructor: </span><span><strong>&lt;?&gt;</strong> <em>type</em><a href='#index-_003c_003f_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-_003f"></span>
<p><var>Type</var> must be a type expression.
This creates a maybe-like type, that is, the object is either
of <var>type</var> or <code>#f</code>.   Usually, <code>#f</code> indicates
that the value is invalid (e.g. the return value of <code>assoc</code>).
</p>
<p>This type has ambiguity when <code>#f</code> can be a meaningful value, but
traditionally been used a lot in Scheme, for it is lightweight.
We also have a &ldquo;proper&rdquo; Maybe type in SRFI-189
(see <a href="Maybe-and-Either-optional-container-types.html#Maybe-and-Either-optional-container-types"><code>srfi.189</code> - Maybe and Either: optional container types</a>) but that involves
extra allocation to wrap the value.
</p>
<p>So, those who came from functional programming may find this isn&rsquo;t
a right abstraction, but it has its place in the Scheme world.
</p></dd></dl>

<dl class="def">
<dt id="index-_003c_002f_003e"><span class="category">Type Constructor: </span><span><strong>&lt;/&gt;</strong> <em>type &hellip;</em><a href='#index-_003c_002f_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-_002f"></span>
<p><var>Type</var> &hellip; must be type expressions.
This creates a <em>sum type</em> of <var>type</var> &hellip;.
For example, <code>(&lt;/&gt; &lt;string&gt; &lt;symbol&gt;)</code> is a type that is
either a string or a symbol.
</p></dd></dl>

<dl class="def">
<dt id="index-_003cTuple_003e"><span class="category">Type Constructor: </span><span><strong>&lt;Tuple&gt;</strong> <em>type &hellip;</em><a href='#index-_003cTuple_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-Tuple"></span>
<p><var>Type</var> &hellip; must be type expressions, except that the
last argument that may be an identifier <code>*</code>.
This creates a <em>product type</em> of <var>type</var> &hellip;.
</p>
<p>Actually, this type is looser than the product types in typical
statically-typed languages.  Our tuple is a subtype of list, with
types of each positional element are restricted with <var>type</var> &hellip;.
For example, <code>(&lt;Tuple&gt; &lt;integer&gt; &lt;string&gt;)</code> is a list of two
elements, the first one being an integer and the second being a string.
</p>
<div class="example">
<pre class="example">(of-type? '(3 &quot;abc&quot;) (&lt;Tuple&gt; &lt;integer&gt; &lt;string&gt;)) &rArr; #t
</pre></div>

<p>If you need a more strict and disjoint product type, you can just create
a class or a record.
</p>
<p>If the last argument is <code>*</code>, the resulting type allows
extra elements after the typed elements.
</p>
<div class="example">
<pre class="example">(of-type? '(3 &quot;abc&quot; 1 2) (&lt;Tuple&gt; &lt;integer&gt; &lt;string&gt; *)) &rArr; #t
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-_003c_005e_003e"><span class="category">Type Constructor: </span><span><strong>&lt;^&gt;</strong> <em>type &hellip; <code>-&gt;</code> type &hellip;</em><a href='#index-_003c_005e_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-_005e"></span>
<p>Each <var>type</var> must be type expressions, except that the
one right before <code>-&gt;</code> and the last one may be an identifier <code>*</code>.
</p>
<p>This creates a procedure type, with the constraints in
arguments and return types.
The <var>type</var> &hellip; before <code>-&gt;</code> are the argument types,
and the ones after <code>-&gt;</code> are the result types.
</p>
<p>The <code>*</code> in the last of argument type list and/or result type list
indicates extra elements are allowed.
</p>
<p>In vanilla Scheme, all procedures belong to just one type, that responds
true to <code>procedure?</code>.  It is simple and flexible, but sometimes the
resolution is too coarse to do reasoning on the program.
</p>
<p>In Gauche, we can attach more detailed type information in procedures.
In the current version, some built-in procedures already have such
type information, that can be retrieved with <code>procedure-type</code>:
</p>
<div class="example">
<pre class="example">(procedure-type cons) &rArr; #&lt;^ &lt;top&gt; &lt;top&gt; -&gt; &lt;pair&gt;&gt;
</pre></div>

<p>Not all procedures have such information, though.  Do not expect the
rigorousness of statically typed languages.
</p>
<p>At this moment, Scheme-defined procedures treats all argument types
as <code>&lt;top&gt;</code>.  We&rsquo;ll provide a way to attach type info in future.
</p></dd></dl>

<dl class="def">
<dt id="index-_003cList_003e"><span class="category">Type Constructor: </span><span><strong>&lt;List&gt;</strong> <em>type :optional min-length max-length</em><a href='#index-_003cList_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cVector_003e"><span class="category">Type Constructor: </span><span><strong>&lt;Vector&gt;</strong> <em>type :optional min-length max-length</em><a href='#index-_003cVector_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-List"></span>
<span id="index-Vector"></span>
<p><var>Type</var> must be a type expression.
These create a list or a vector type whose elements are of <var>type</var>,
respectively.
For example, <code>(&lt;List&gt; &lt;string&gt;)</code> is a list of strings.
</p>
<p>The optional arguments must be a literal real numbers that limit
the minimum and maximum (inclusive) length of the list or the vector.
When omitted, <var>min-length</var>
is zero and <var>max-length</var> is <code>inf.0</code>.
</p>
<p>Note: <code>subtype?</code> uses covariant subtyping, that is,
<code>(subtype? (&lt;List&gt; &lt;string&gt;) &lt;list&gt;)</code> is <code>#t</code>.
</p></dd></dl>

<hr>
</div>
<div class="subsection" id="Native-types">
<div class="header">
<p>
Previous: <a href="#Type-expressions-and-type-constructors" accesskey="p" rel="prev">Type expressions and type constructors</a>, Up: <a href="#Types-and-classes" accesskey="u" rel="up">Types and classes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Native-types-1"></span><h4 class="subsection">6.1.5 Native types</h4>

<p>Native types are a set of predefined descriptive types that bridge
Scheme types and C types.  They can be used to access binary blobs
passed from a foreign functions, for example.  They&rsquo;ll also be used
for FFI.
</p>
<p>An example of native types is <code>&lt;int16&gt;</code>, which corresponds to
C&rsquo;s <code>int16_t</code>.  You can use <code>&lt;int16&gt;</code> to check if a Scheme
object can be treated as that type:
</p>
<div class="example">
<pre class="example">(of-type? 357 &lt;int16&gt;)   &rArr; #t
(of-type? 50000 &lt;int16&gt;) &rArr; #f
</pre></div>

<p>Or obtain size and alignemnt info on the running platform:
</p>
<div class="example">
<pre class="example">gosh&gt; (describe &lt;int16&gt;)
#&lt;native-type &lt;int16&gt;&gt; is an instance of class &lt;native-type&gt;
slots:
  name      : &lt;int16&gt;
  super     : #&lt;class &lt;integer&gt;&gt;
  c-type-name: &quot;int16_t&quot;
  size      : 2
  alignment : 2
</pre></div>

<dl class="def">
<dt id="index-_003cfixnum_003e"><span class="category">Native type: </span><span><strong>&lt;fixnum&gt;</strong><a href='#index-_003cfixnum_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cint_003e"><span class="category">Native type: </span><span><strong>&lt;int&gt;</strong><a href='#index-_003cint_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cint8_003e"><span class="category">Native type: </span><span><strong>&lt;int8&gt;</strong><a href='#index-_003cint8_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cint16_003e"><span class="category">Native type: </span><span><strong>&lt;int16&gt;</strong><a href='#index-_003cint16_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cint32_003e"><span class="category">Native type: </span><span><strong>&lt;int32&gt;</strong><a href='#index-_003cint32_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cint64_003e"><span class="category">Native type: </span><span><strong>&lt;int64&gt;</strong><a href='#index-_003cint64_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cshort_003e"><span class="category">Native type: </span><span><strong>&lt;short&gt;</strong><a href='#index-_003cshort_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003clong_003e"><span class="category">Native type: </span><span><strong>&lt;long&gt;</strong><a href='#index-_003clong_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cuint_003e"><span class="category">Native type: </span><span><strong>&lt;uint&gt;</strong><a href='#index-_003cuint_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cuint8_003e"><span class="category">Native type: </span><span><strong>&lt;uint8&gt;</strong><a href='#index-_003cuint8_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cuint16_003e"><span class="category">Native type: </span><span><strong>&lt;uint16&gt;</strong><a href='#index-_003cuint16_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cuint32_003e"><span class="category">Native type: </span><span><strong>&lt;uint32&gt;</strong><a href='#index-_003cuint32_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cuint64_003e"><span class="category">Native type: </span><span><strong>&lt;uint64&gt;</strong><a href='#index-_003cuint64_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cushort_003e"><span class="category">Native type: </span><span><strong>&lt;ushort&gt;</strong><a href='#index-_003cushort_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003culong_003e"><span class="category">Native type: </span><span><strong>&lt;ulong&gt;</strong><a href='#index-_003culong_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cfloat_003e"><span class="category">Native type: </span><span><strong>&lt;float&gt;</strong><a href='#index-_003cfloat_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cdouble_003e"><span class="category">Native type: </span><span><strong>&lt;double&gt;</strong><a href='#index-_003cdouble_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003csize_005ft_003e"><span class="category">Native type: </span><span><strong>&lt;size_t&gt;</strong><a href='#index-_003csize_005ft_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cssize_005ft_003e"><span class="category">Native type: </span><span><strong>&lt;ssize_t&gt;</strong><a href='#index-_003cssize_005ft_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cptrdiff_005ft_003e"><span class="category">Native type: </span><span><strong>&lt;ptrdiff_t&gt;</strong><a href='#index-_003cptrdiff_005ft_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003coff_005ft_003e"><span class="category">Native type: </span><span><strong>&lt;off_t&gt;</strong><a href='#index-_003coff_005ft_003e' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_003cvoid_003e"><span class="category">Native type: </span><span><strong>&lt;void&gt;</strong><a href='#index-_003cvoid_003e' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-fixnum"></span>
<span id="index-int"></span>
<span id="index-int8"></span>
<span id="index-int16"></span>
<span id="index-int32"></span>
<span id="index-int64"></span>
<span id="index-short"></span>
<span id="index-long"></span>
<span id="index-uint"></span>
<span id="index-uint8"></span>
<span id="index-uint16"></span>
<span id="index-uint32"></span>
<span id="index-uint64"></span>
<span id="index-ushort"></span>
<span id="index-ulong"></span>
<span id="index-float"></span>
<span id="index-double"></span>
<span id="index-size_005ft"></span>
<span id="index-ssize_005ft"></span>
<span id="index-ptrdiff_005ft"></span>
<span id="index-off_005ft"></span>
<span id="index-void"></span>
<p>Predefined native types.  The correspondence of Scheme and C types
are shown below:
</p>
<div class="example">
<pre class="example">Native type  Scheme       C            Notes
-----------------------------------------------------------------
&lt;fixnum&gt;     &lt;integer&gt;    ScmSmallInt  Integers within fixnum range

&lt;int&gt;        &lt;integer&gt;    int          Integers representable in C
&lt;int8&gt;       &lt;integer&gt;    int8_t
&lt;int16&gt;      &lt;integer&gt;    int16_t
&lt;int32&gt;      &lt;integer&gt;    int32_t
&lt;int64&gt;      &lt;integer&gt;    int64_t
&lt;short&gt;      &lt;integer&gt;    short
&lt;long&gt;       &lt;integer&gt;    long
&lt;uint&gt;       &lt;integer&gt;    uint         Integers representable in C
&lt;uint8&gt;      &lt;integer&gt;    uint8_t
&lt;uint16&gt;     &lt;integer&gt;    uint16_t
&lt;uint32&gt;     &lt;integer&gt;    uint32_t
&lt;uint6r&gt;     &lt;integer&gt;    uint64_t
&lt;ushort&gt;     &lt;integer&gt;    ushort
&lt;ulong&gt;      &lt;integer&gt;    ulong
&lt;float&gt;      &lt;real&gt;       float        Unboxed value casted to float
&lt;double&gt;     &lt;real&gt;       double

&lt;size_t&gt;     &lt;integer&gt;    size_t       System-dependent types
&lt;ssize_t&gt;    &lt;integer&gt;    ssize_t
&lt;ptrdiff_t&gt;  &lt;integer&gt;    ptrdiff_t
&lt;off_t&gt;      &lt;integer&gt;    off_t

&lt;void&gt;       -            void        (Used only as a return type.
                                        Scheme function returns #&lt;undef&gt;)
</pre></div>
</dd></dl>


</div>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Equality-and-comparison.html#Equality-and-comparison" accesskey="n" rel="next">Equality and comparison</a>, Previous: <a href="Core-library.html" accesskey="p" rel="prev">Core library</a>, Up: <a href="Core-library.html" accesskey="u" rel="up">Core library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
