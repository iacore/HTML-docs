<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Feature conditional (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Feature conditional (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Feature conditional (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Core-syntax.html" rel="up" title="Core syntax">
<link href="Modules.html#Modules" rel="next" title="Modules">
<link href="Inclusions.html" rel="prev" title="Inclusions">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Feature-conditional">
<div class="header">
<p>
Next: <a href="Modules.html#Modules" accesskey="n" rel="next">Modules</a>, Previous: <a href="Inclusions.html" accesskey="p" rel="prev">Inclusions</a>, Up: <a href="Core-syntax.html" accesskey="u" rel="up">Core syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Feature-conditional-1"></span><h3 class="section">4.12 Feature conditional</h3>

<span id="The-cond_002dexpand-macro"></span><h4 class="subheading">The <code>cond-expand</code> macro</h4>

<p>Sometimes you need to have a different piece of code
depending on available features provided by the
implementation and/or platform.  For example,
you may want to switch behavior depending on whether
networking is available, or to embed an
implementation specific procedures in otherwise-portable code.
</p>
<p>In C, you use preprocessor directives such as <code>#ifdef</code>.
In Common Lisp, you use reader macro <code>#+</code> and <code>#-</code>.
In Scheme, you have <code>cond-expand</code>:
</p>
<dl class="def">
<dt id="index-cond_002dexpand"><span class="category">Macro: </span><span><strong>cond-expand</strong> <em>(feature-requirement command-or-definition &hellip;) &hellip;</em><a href='#index-cond_002dexpand' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS base]
This macro expands to <var>command-or-definition</var> &hellip; if
<var>feature-requirement</var> is supported by the current platform.
</p>
<p><var>feature-requirement</var> must be in the following syntax:
</p><div class="example">
<pre class="example"><var>feature-requirement</var>
  : <var>feature-identifier</var>
  | (and <var>feature-requirement</var> &hellip;)
  | (or  <var>feature-requirement</var> &hellip;)
  | (not <var>feature-requirement</var>)
  | (library <var>library-name</var>)
</pre></div>

<p>The macro tests each <var>feature-requirement</var> in order, and
if one is satisfied, the macro itself expands to the
corresponding <var>command-or-definition</var> &hellip;.
</p>
<p>The last clause may have <code>else</code> in the position of
<var>feature-requirement</var>, to make the clause expanded
if none of the previous feature requirement is fulfilled.
</p>
<p>If there&rsquo;s neither a satisfying clause nor <code>else</code> clause,
<code>cond-expand</code> form throws an error.  It is to detect the case early
that the platform doesn&rsquo;t have required features.   If the feature
you&rsquo;re testing is optional, that is, your program works without the
feature as well, add empty <code>else</code> clause as follows.
</p>
<div class="example">
<pre class="example">(cond-expand
  [feature expr]  ; some optional feature
  [else])
</pre></div>


<p><var>feature-identifier</var> is a symbol that indicates a feature.
If such a feature is supported in the current platform, it satisfies the
<var>feature-requirement</var>.  You can do boolean combination
of <var>feature-requirement</var>s to compose more complex conditions.
</p>
<p>The form <code>(library <var>library-name</var>)</code> is added in R7RS,
and it is fulfilled when the named library is available.  For the
<var>library-name</var>, you can use both R7RS library name
(e.g. <code>(srfi 13)</code>) and traditional Gauche module name
(e.g. <code>srfi.13</code>).
</p>
<p>Here&rsquo;s a typical example: Suppose you want to have implementation-specific
part for Gauche, Chicken Scheme and ChibiScheme.  Most modern Scheme
implementations defines a feature-identifier to identify itself.
You can write the conditional part as follows:
</p>
<div class="example">
<pre class="example">(cond-expand
 [gauche  (gauche-specific-code)]
 [(or chicken chibi) (chicken-chibi-specific-code)]
 [else    (fallback-code)]
 )
</pre></div>

<p>It is important that the conditions of <code>cond-expand</code> is
purely examined at the macro-expansion time, and
unfulfilled clauses are discarded.
Thus, for example, you can include macro calls or language extensions
that may not be recognized on some implementations.  You can also
conditionally define global bindings.
</p>
<p>Compare that to <code>cond</code>, which examines conditions at runtime.
If you include unsupported macro call in one of the conditions, it
may raise an error at macro expansion time, even if that clause
will never be executed on the platform.  Also, it is not possible
to conditionally define global bindings using <code>cond</code>.
</p>
<p>There&rsquo;s a caveat, though.  Suppose you want to save the result of macro
expansion, and run the expanded result later on other platforms.
The result code is based on the features of the platform the macro
expansion takes place, which may not agree with the features
of the platform the code will run.  (This issue always arises
in cross-compiling situation in general.)
</p>

<p>NB: When <code>cond-expand</code> was defined as SRFI-0, there was no standard
way to name Scheme libraries.  So SRFI-0 suggested to use <code>srfi-N</code>
as feature identifier to check the support of a specific srfi.
<b>We deprecate this usage</b>.  It should be replaced with
<code>(library srfi.N)</code>.  See <a href="Testing-availability-of-SRFIs.html">Testing availability of SRFIs</a>, for the
details.
</p>
<p>As of 0.9.13, a warning is issued when <code>srfi-N</code> feature identifier
is used in <code>cond-expand</code>
if the environment variable <code>GAUCHE_WARN_SRFI_FEATURE_ID</code> is set.
In future versions the warning will be default; we recommend to
set this environment variable to detect code to update.
</p>
<p>See below for the list of feature identifiers defined in Gauche.
</p></dd></dl>

<span id="Gauche_002dspecific-feature-identifiers"></span><h4 class="subheading">Gauche-specific feature identifiers</h4>

<dl compact="compact">
<dt><span><code>gauche</code></span></dt>
<dt><span><code>gauche-X.X.X</code></span></dt>
<dd><p>Indicates you&rsquo;re running on Gauche.  It is useful to put Gauche-specific
code in a portable program.
<code>X.X.X</code> is the gauche&rsquo;s version
(e.g. <code>gauche-0.9.4</code>), in case you want to have code
for specific Gauche version.  (Such feature identifier is suggested by
R7RS; but it might not be useful if we don&rsquo;t have means to compare versions.
Something to consider in future versions.)
</p>
</dd>
<dt><span><code>gauche.os.windows</code></span></dt>
<dt><span><code>gauche.os.cygwin</code></span></dt>
<dd><p>Defined on Windows-native platform and Cygwin/Windows platform, respectively.
If neither is defined you can assume it&rsquo;s a unix variant.
(Cygwin is supposedly
unix variant, but corners are different enough to deserve it&rsquo;s own
feature identifier.)
</p>
</dd>
<dt><span><code>gauche.ces.utf8</code></span></dt>
<dd><p>This indicates Gauche&rsquo;s internal encoding is <code>utf-8</code>.
Before 1.0, Gauche can be compiled with internal encodings other
than <code>utf-8</code>, and the corresponding feature identifiers are
defined so that the program can switch its behavior according to it.
Now this is the only possible choice.  This is kept for the
backward compatibility.
</p>
</dd>
<dt><span><code>gauche.net.tls</code></span></dt>
<dt><span><code>gauche.net.tls.axtls</code></span></dt>
<dt><span><code>gauche.net.tls.mbedtls</code></span></dt>
<dd><p>Defined if the runtime supports TLS in networking.
The two sub feature identifiers, <code>gauche.net.tls.axtls</code> and
<code>gauche.net.tls.mbedtls</code>, are defined if each subsystem
<code>axTLS</code> and <code>mbedTLS</code> is supported, respectively.
</p>
</dd>
<dt><span><code>gauche.net.ipv6</code></span></dt>
<dd><p>Defined if the runtime supports IPv6.
Note that this only indicates Gauche has been built with IPv6 support;
the OS may not allow IPv6 features, in that case you&rsquo;ll get system error
when you try to use IPv6.
</p>
</dd>
<dt><span><code>gauche.sys.threads</code></span></dt>
<dt><span><code>gauche.sys.pthreads</code></span></dt>
<dt><span><code>gauche.sys.wthreads</code></span></dt>
<dd><p>If the runtime supports multithreading, <code>gauche.sys.threads</code> is
defined (see <a href="Threads.html#Threads"><code>gauche.threads</code> - Threads</a>).  Multithreading is based on either POSIX pthreads
or Windows threads.  The former defines <code>gauche.sys.pthreads</code>,
and the latter defines <code>gauche.sys.wthreads</code>.
</p>
</dd>
<dt><span><code>gauche.sys.sigwait</code></span></dt>
<dt><span><code>gauche.sys.setenv</code></span></dt>
<dt><span><code>gauche.sys.unsetenv</code></span></dt>
<dt><span><code>gauche.sys.clearenv</code></span></dt>
<dt><span><code>gauche.sys.getloadavg</code></span></dt>
<dt><span><code>gauche.sys.getrlimit</code></span></dt>
<dt><span><code>gauche.sys.lchown</code></span></dt>
<dt><span><code>gauche.sys.getpgid</code></span></dt>
<dt><span><code>gauche.sys.nanosleep</code></span></dt>
<dt><span><code>gauche.sys.crypt</code></span></dt>
<dt><span><code>gauche.sys.symlink</code></span></dt>
<dt><span><code>gauche.sys.readlink</code></span></dt>
<dt><span><code>gauche.sys.select</code></span></dt>
<dt><span><code>gauche.sys.fcntl</code></span></dt>
<dt><span><code>gauche.sys.syslog</code></span></dt>
<dt><span><code>gauche.sys.setlogmask</code></span></dt>
<dt><span><code>gauche.sys.openpty</code></span></dt>
<dt><span><code>gauche.sys.forkpty</code></span></dt>
<dd><p>Those are defined based on the availability of these system features
of the platform.
</p></dd>
</dl>


<span id="R7RS-feature-identifiers"></span><h4 class="subheading">R7RS feature identifiers</h4>

<dl compact="compact">
<dt><span><code>r7rs</code></span></dt>
<dd><p>Indicates the implementation complies r7rs.
</p>
</dd>
<dt><span><code>exact-closed</code></span></dt>
<dd><p>Exact arithmetic operations are closed; that is, dividing
an exact number by a non-zero exact number always yields an exact
number.
</p>
</dd>
<dt><span><code>ieee-float</code></span></dt>
<dd><p>Using IEEE floating-point number internally.
</p>
</dd>
<dt><span><code>full-unicode</code></span></dt>
<dd><p>Full unicode support.
</p>
</dd>
<dt><span><code>ratios</code></span></dt>
<dd><p>Rational number support
</p>
</dd>
<dt><span><code>posix</code></span></dt>
<dt><span><code>windows</code></span></dt>
<dd><p>Either one is defined, according to the platform.
</p>
</dd>
<dt><span><code>big-endian</code></span></dt>
<dt><span><code>little-endian</code></span></dt>
<dd><p>Either one is defined, according to the platform.
</p></dd>
</dl>



</div>
<hr>
<div class="header">
<p>
Next: <a href="Modules.html#Modules" accesskey="n" rel="next">Modules</a>, Previous: <a href="Inclusions.html" accesskey="p" rel="prev">Inclusions</a>, Up: <a href="Core-syntax.html" accesskey="u" rel="up">Core syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
