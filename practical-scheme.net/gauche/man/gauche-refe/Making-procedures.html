<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Making procedures (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Making procedures (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Making procedures (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Core-syntax.html" rel="up" title="Core syntax">
<link href="Assignments.html" rel="next" title="Assignments">
<link href="Literals.html" rel="prev" title="Literals">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Making-procedures">
<div class="header">
<p>
Next: <a href="Assignments.html" accesskey="n" rel="next">Assignments</a>, Previous: <a href="Literals.html" accesskey="p" rel="prev">Literals</a>, Up: <a href="Core-syntax.html" accesskey="u" rel="up">Core syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Making-procedures-1"></span><h3 class="section">4.3 Making procedures</h3>

<dl class="def">
<dt id="index-lambda"><span class="category">Special Form: </span><span><strong>lambda</strong> <em>formals body &hellip;</em><a href='#index-lambda' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-_005e-1"><span class="category">Special Form: </span><span><strong>^</strong> <em>formals body &hellip;</em><a href='#index-_005e-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS+]
Evaluates to a procedure.  The environment in effect when this expression
is evaluated is stored in the procedure.   When the procedure
is called, <var>body</var> is evaluated sequentially in the
stored environment extended by the bindings of the formal arguments,
and returns the value(s) of the last expression in the body.
</p>
<p><code>^</code> is a concise alias of <code>lambda</code>.  It is Gauche&rsquo;s extension.
</p>
<div class="example">
<pre class="example">(lambda (a b) (+ a b))
  &rArr; <span class="roman">procedure that adds two arguments</span>

((lambda (a b) (+ a b)) 1 2) &rArr; 3

((^(a b) (+ a b)) 1 2)       &rArr; 3
</pre></div>

<p>Gauche also extends R7RS <code>lambda</code> to take extended syntax in
<var>formals</var> to specify optional and keyword arguments easily.
The same functionality can be written in pure R7RS, with parsing
variable-length arguments explicitly, but the code tends
to be longer and verbose.   It is recommended to use extended syntax
unless you&rsquo;re writing portable code.
</p>
<p><var>Formals</var> should have one of the following forms:
</p><ul>
<li> <code>(<var>variable</var> &hellip;)</code> :
The procedure takes a fixed number of arguments.
The actual arguments are bound to the corresponding variables.

<div class="example">
<pre class="example">((lambda (a) a) 1)    &rArr; 1

((lambda (a) a) 1 2)  &rArr; <i>error - wrong number of arguments</i>
</pre></div>

</li><li> <code><var>variable</var></code> :
The procedure takes any number of arguments.
The actual arguments are collected to form a new list and bound to
the variable.

<div class="example">
<pre class="example">((lambda a a) 1 2 3)  &rArr; (1 2 3)
</pre></div>

</li><li> <code>(<var>variable_0</var> &hellip; <var>variable_N-1</var> . <var>variable_N</var>)</code> :
The procedure takes at least <var>N</var> arguments.  The actual arguments
up to <var>N</var> is bound to the corresponding variables.
If more than <var>N</var> arguments are given, the rest arguments are
collected to form a new list and bound to <var>variable_N</var>.

<div class="example">
<pre class="example">((lambda (a b . c) (print &quot;a=&quot; a &quot; b=&quot; b &quot; c=&quot; c)) 1 2 3 4 5)
 &rArr; <span class="roman">prints</span> a=1 b=2 c=(3 4 5)
</pre></div>

</li><li> <code>(<var>variable</var> &hellip; <var>extended-spec</var> &hellip;)</code> :
Extended argument specification.  Zero or more variables that
specifies required formal arguments, followed
by an <em>extended spec</em>, a list beginning with
a keyword <code>:optional</code>, <code>:key</code> or <code>:rest</code>.

<p>The <var>extended-spec</var> part consists of the optional argument spec,
the keyword argument spec and the rest argument spec.  They can
appear in any combinations.
</p>
<dl compact="compact">
<dt><span><code>:optional <var>optspec</var> &hellip;</code></span></dt>
<dd><p>Specifies optional arguments.  Each <var>optspec</var> can be either
one of the following forms:
</p>
<div class="example">
<pre class="example"><var>variable</var>
(<var>variable</var> <var>init-expr</var>)
</pre></div>

<p>The <var>variable</var> names the formal argument, which is bound to
the value of the actual argument if given, or the value of
the expression <var>init-expr</var> otherwise.   If <var>optspec</var> is just
a variable, and the actual argument is not given to it, then
<var>variable</var> will be bound to <code>#&lt;undef&gt;</code> (see <a href="Undefined-values.html">Undefined values</a>).
</p>
<p>The expression <var>init-expr</var> is only evaluated if the actual
argument for <var>variable</var> is not given.  The scope in which
<var>init-expr</var> is evaluated includes the preceding formal arguments.
</p>
<div class="example">
<pre class="example">((lambda (a b :optional (c (+ a b))) (list a b c))
 1 2)    &rArr; (1 2 3)

((lambda (a b :optional (c (+ a b))) (list a b c))
 1 2 -1) &rArr; (1 2 -1)

((lambda (a b :optional c) (list a b c))
 1 2)    &rArr; (1 2 #&lt;undef&gt;)

((lambda (:optional (a 0) (b (+ a 1))) (list a b))
 )       &rArr; (0 1)
</pre></div>

<p>The procedure signals an error if more actual arguments than
the number of required and optional arguments are given, unless it also has
<code>:key</code> or <code>:rest</code> argument spec.
</p>
<div class="example">
<pre class="example">((lambda (:optional a b) (list a b)) 1 2 3)
 &rArr; <i>error - too many arguments</i>

((lambda (:optional a b :rest r) (list a b r)) 1 2 3)
 &rArr; (1 2 (3))
</pre></div>

</dd>
<dt><span><code>:key <var>keyspec</var> &hellip; [:allow-other-keys [<var>variable</var>]]</code></span></dt>
<dd><p>Specifies keyword arguments.  Each <var>keyspec</var> can be
either one of the following forms.
</p>
<div class="example">
<pre class="example"><var>variable</var>
(<var>variable</var> <var>init-expr</var>)
((<var>keyword</var> <var>variable</var>) <var>init-expr</var>)
</pre></div>

<p>The <var>variable</var> names the formal argument, which is bound to the
actual argument given with the keyword of the same name as <var>variable</var>.
When the actual argument is not given, <var>init-expr</var> is evaluated
and the result is bound to <var>variable</var> in the second and third form,
or <code>#&lt;undef&gt;</code> is bound in the first form.
</p>
<div class="example">
<pre class="example">(define f (lambda (a :key (b (+ a 1)) (c (+ b 1)))
            (list a b c)))

(f 10)            &rArr; (10 11 12)
(f 10 :b 4)       &rArr; (10 4 5)
(f 10 :c 8)       &rArr; (10 11 8)
(f 10 :c 1 :b 3)  &rArr; (10 3 1)
</pre></div>

<p>With the third form you can name the formal argument differently from
the keyword to specify the argument.
</p>
<div class="example">
<pre class="example">((lambda (:key ((:aa a) -1)) a) :aa 2)
  &rArr; 2
</pre></div>

<p>By default, the procedure with keyword argument spec raises an
error if a keyword argument with an unrecognized keyword is given.
Giving <code>:allow-other-keys</code> in the formals suppresses this behavior.
If you give <var>variable</var> after <code>:allow-other-keys</code>,
the list of unrecognized keywords and their arguments are bound to it.
Again, see the example below will help to understand the behavior.
</p>
<div class="example">
<pre class="example">((lambda (:key a) a)
 :a 1 :b 2)  &rArr; <i>error - unknown keyword :b</i>

((lambda (:key a :allow-other-keys) a)
 :a 1 :b 2)  &rArr; 1

((lambda (:key a :allow-other-keys z) (list a z))
 :a 1 :b 2)  &rArr; (1 (:b 2))
</pre></div>

<p>When used with <code>:optional</code> argument spec, the keyword arguments
are searched after all the optional arguments are bound.
</p>
<div class="example">
<pre class="example">((lambda (:optional a b :key c) (list a b c))
 1 2 :c 3)  &rArr; (1 2 3)

((lambda (:optional a b :key c) (list a b c))
 :c 3)      &rArr; (:c 3 #&lt;undef&gt;)

((lambda (:optional a b :key c) (list a b c))
 1 :c 3)     &rArr; <i>error - keyword list not even</i>
</pre></div>


</dd>
<dt><span><code>:rest <var>variable</var></code></span></dt>
<dd>
<p>Specifies the rest argument.   If specified without <code>:optional</code>
argument spec, a list of remaining arguments after required arguments are
taken is bound to <var>variable</var>.  If specified with <code>:optional</code>
argument spec, the actual arguments are first bound to required and
all optional arguments, and the remaining arguments are bound to <var>variable</var>.
</p>
<div class="example">
<pre class="example">((lambda (a b :rest z) (list a b z))
 1 2 3 4 5)  &rArr; (1 2 (3 4 5))

((lambda (a b :optional c d :rest z) (list a b c d z))
 1 2 3 4 5)  &rArr; (1 2 3 4 (5))

((lambda (a b :optional c d :rest z) (list a b c d z))
 1 2 3)      &rArr; (1 2 3 #&lt;undef&gt; ())
</pre></div>

<p>When the rest argument spec is used with the keyword argument spec,
both accesses the same list of actual argument&mdash;the remaining arguments
after required and optional arguments are taken.
</p>
<div class="example">
<pre class="example">((lambda (:optional a :rest r :key k) (list a r k))
 1 :k 3)  &rArr; (1 (:k 3) 3)
</pre></div>

</dd>
</dl>

<p>See also <code>let-optionals*</code>, <code>let-keywords</code> and
<code>let-keywords*</code> macros in <a href="Procedures-and-continuations.html#Optional-argument-parsing">Optional argument parsing</a>
for an alternative way to receive optional/keyword arguments
within the spec of R7RS.
</p>
</li></ul>

</dd></dl>

<dl class="def">
<dt id="index-_005ec"><span class="category">Macro: </span><span><strong>^c</strong> <em>body &hellip;</em><a href='#index-_005ec' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-_005e_005f"></span>
<span id="index-_005ea"></span>
<span id="index-_005eb"></span>
<span id="index-_005ed"></span>
<span id="index-_005ee"></span>
<span id="index-_005ef"></span>
<span id="index-_005eg"></span>
<span id="index-_005eh"></span>
<span id="index-_005ei"></span>
<span id="index-_005ej"></span>
<span id="index-_005ek"></span>
<span id="index-_005el"></span>
<span id="index-_005em"></span>
<span id="index-_005en"></span>
<span id="index-_005eo"></span>
<span id="index-_005ep"></span>
<span id="index-_005eq"></span>
<span id="index-_005er"></span>
<span id="index-_005es"></span>
<span id="index-_005et"></span>
<span id="index-_005eu"></span>
<span id="index-_005ev"></span>
<span id="index-_005ew"></span>
<span id="index-_005ex"></span>
<span id="index-_005ey"></span>
<span id="index-_005ez"></span>
<p>A shorthand notation of <code>(lambda (<var>c</var>) <var>body</var> &hellip;)</code>.
where <var>c</var> can be any character in <code>#[_a-z]</code>.
</p>
<div class="example">
<pre class="example">(map (^x (* x x)) '(1 2 3 4 5)) &rArr; (1 4 9 16 25)
</pre></div>
</dd></dl>


<dl class="def">
<dt id="index-cut"><span class="category">Macro: </span><span><strong>cut</strong> <em>expr-or-slot expr-or-slot2 &hellip;</em><a href='#index-cut' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-cute"><span class="category">Macro: </span><span><strong>cute</strong> <em>expr-or-slot expr-or-slot2 &hellip;</em><a href='#index-cute' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[SRFI-26]
Convenience macros to notate a procedure compactly.
This form can be used to realize partial application,
a.k.a sectioning or projection.
</p>
<p>Each <var>expr-or-slot</var> must be either an expression or a symbol <code>&lt;&gt;</code>,
indicating a &rsquo;slot&rsquo;.
The last <var>expr-or-slot</var> can be a symbol <code>&lt;...&gt;</code>,
indicating a &rsquo;rest-slot&rsquo;.
<code>Cut</code> expands into a <code>lambda</code> form that takes as many arguments
as the number of slots in the given form, and
whose body is an expression
</p><div class="example">
<pre class="example">  (<var>expr-or-slot</var> <var>expr-or-slot2</var> &hellip;)
</pre></div>
<p>where each occurrence of <code>&lt;&gt;</code> is replaced to the corresponding
argument.
In case there is a rest-slot symbol, the resulting procedure is also
of variable arity, and all the extra arguments are passed
to the call of <var>expr-or-slot</var>.  See the fourth example below.
</p>
<div class="example">
<pre class="example">(cut cons (+ a 1) &lt;&gt;)  &equiv; (lambda (x2) (cons (+ a 1) x2))
(cut list 1 &lt;&gt; 3 &lt;&gt; 5) &equiv; (lambda (x2 x4) (list 1 x2 3 x4 5))
(cut list)             &equiv; (lambda () (list))
(cut list 1 &lt;&gt; 3 &lt;...&gt;)
   &equiv; (lambda (x2 . xs) (apply list 1 x2 3 xs))
(cut &lt;&gt; a b)           &equiv; (lambda (f) (f a b))

;; Usage
(map (cut * 2 &lt;&gt;) '(1 2 3 4))
(for-each (cut write &lt;&gt; port) exprs)
</pre></div>

<p><code>Cute</code> is a variation of <code>cut</code> that evaluates <var>expr-or-slot</var>s
before creating the procedure.
</p>
<div class="example">
<pre class="example">(cute cons (+ a 1) &lt;&gt;)
   &equiv; (let ((xa (+ a 1))) (lambda (x2) (cons xa x2)))
</pre></div>

<p>Gauche provides a couple of different ways to write partial
applications concisely; see the <code>$</code> macro below, and also
the <code>pa$</code> procedure (see <a href="Procedures-and-continuations.html#Combinators">Combinators</a>).
</p></dd></dl>

<dl class="def">
<dt id="index-_0024"><span class="category">Macro: </span><span><strong>$</strong> <em>arg &hellip;</em><a href='#index-_0024' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>A macro to chain applications, hinted from Haskell&rsquo;s <code>$</code> operator
(although the meaning is different).
Within the macro arguments <var>arg</var> &hellip;, <code>$</code> delimits
the last argument.  For example, the following code makes
the last argument for the procedure <code>f</code> to be <code>(g c d &hellip;)</code>
</p><div class="example">
<pre class="example">  ($ f a b $ g c d ...)
  &equiv; (f a b (g c d ...))
</pre></div>

<p>The <code>$</code> notation can be chained.
</p>
<div class="example">
<pre class="example">  ($ f a b $ g c d $ h e f ...)
  &equiv; (f a b (g c d (h e f ...)))
</pre></div>

<p>If <code>$*</code> appears in the argument list instead of <code>$</code>,
it fills the rest of the arguments, instead of just the last argument.
</p>
<div class="example">
<pre class="example">  ($ f a b $* g c d ...)
  &equiv; (apply f a b (g c d ...))

  ($ f a b $* g $ h $* hh ...)
  &equiv; (apply f a b (g (apply h (hh ...))))
</pre></div>

<p>Furthermore, if the argument list ends with <code>$</code> or <code>$*</code>,
the whole expression becomes a procedure expecting the last argument(s).
</p>
<div class="example">
<pre class="example">  ($ f a b $ g c d $ h e f $)
  &equiv; (lambda (arg) (f a b (g c d (h e f arg))))
  &equiv; (.$ (cut f a b &lt;&gt;) (cut g c d &lt;&gt;) (cut h e f &lt;&gt;))

  ($ f a b $ g c d $ h e f $*)
  &equiv; (lambda args (f a b (g c d (apply h e f args))))
  &equiv; (.$ (cut f a b &lt;&gt;) (cut g c d &lt;&gt;) (cut h e f &lt;...&gt;))
</pre></div>

<p>The more functional the code becomes, the more you are
tempted to write it as a chain of nested function calls.
Scheme&rsquo;s syntax can get awkward in such code.  Close
parentheses tend to clutter at the end of expressions.
Inner applications tends to pushed toward right columns
with the standard indentation rules.  Compare the
following two code functionally equivalent to each other:
</p>
<div class="example">
<pre class="example">(intersperse &quot;:&quot;
             (map transform-it
                  (delete-duplicates (map cdr
                                          (group-sequence input)))))

($ intersperse &quot;:&quot;
   $ map transform-it
   $ delete-duplicates
   $ map cdr $ group-sequence input)
</pre></div>

<p>It is purely a matter of taste, and also this kind of syntax sugars
can be easily abused.  Use with care, but it may work well if used
sparingly, like spices.
</p>
<p>As a corner case, if neither <code>$</code> nor <code>$*</code> appear in the
argument list, it just calls the function.  It is useful when
the function has long name and you don&rsquo;t want to indent arguments
too further right.
</p>
<div class="example">
<pre class="example">  ($ f a b c) &equiv; (f a b c)
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-case_002dlambda"><span class="category">Macro: </span><span><strong>case-lambda</strong> <em>clause &hellip;</em><a href='#index-case_002dlambda' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS case-lambda]
Each <var>clause</var> should have the form (<var>formals</var> <var>expr</var> &hellip;),
where <var>formals</var> is a formal arguments list as for <code>lambda</code>.
</p>
<p>This expression evaluates to a procedure that accepts a variable
number of arguments and is lexically scoped in the same manner as
procedures resulting from <code>lambda</code> expressions.
When the procedure is called with some arguments,
then the first <var>clause</var> for which the arguments agree with
<var>formals</var> is selected, where agreement is specified as for
the <var>formals</var> of a <code>lambda</code> expression.
The variables of <code>formals</code> are bound to the given
arguments, and the <var>expr</var> &hellip; are evaluated within the environment.
</p>
<p>It is an error for the arguments not to agree with
the <var>formals</var> of any <var>clause</var>.
</p>
<div class="example">
<pre class="example">(define f
  (case-lambda
    [() 'zero]
    [(a) `(one ,a)]
    [(a b) `(two ,a ,b)]))

(f)       &rArr; zero
(f 1)     &rArr; (one 1)
(f 1 2)   &rArr; (two 1 2)
(f 1 2 3) &rArr; <span class="roman">Error: wrong number of arguments to case lambda</span>

(define g
  (case-lambda
    [() 'zero]
    [(a) `(one ,a)]
    [(a . b) `(more ,a ,@b)]))

(g)       &rArr; zero
(g 1)     &rArr; (one 1)
(g 1 2 3) &rArr; (more 1 2 3)
</pre></div>

<p>Note that the clauses are examined sequentially to match the number of
arguments, so in the following example <code>g2</code> never returns <code>(one ...)</code>.
</p>
<div class="example">
<pre class="example">(define g2
  (case-lambda
    [() 'zero]
    [(a . b) `(more ,a ,@b)]
    [(a) `(one ,a)]))

(g2 1)    &rArr; (more 1)
</pre></div>
</dd></dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="Assignments.html" accesskey="n" rel="next">Assignments</a>, Previous: <a href="Literals.html" accesskey="p" rel="prev">Literals</a>, Up: <a href="Core-syntax.html" accesskey="u" rel="up">Core syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
