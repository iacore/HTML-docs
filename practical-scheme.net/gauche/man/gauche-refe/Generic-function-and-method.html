<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Generic function and method (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Generic function and method (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Generic function and method (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Object-system.html" rel="up" title="Object system">
<link href="Metaobject-protocol.html#Metaobject-protocol" rel="next" title="Metaobject protocol">
<link href="Instance.html#Instance" rel="prev" title="Instance">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Generic-function-and-method">
<div class="header">
<p>
Next: <a href="Metaobject-protocol.html#Metaobject-protocol" accesskey="n" rel="next">Metaobject protocol</a>, Previous: <a href="Instance.html#Instance" accesskey="p" rel="prev">Instance</a>, Up: <a href="Object-system.html" accesskey="u" rel="up">Object system</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Generic-function-and-method-1"></span><h3 class="section">7.4 Generic function and method</h3>

<span id="Defining-methods"></span><h4 class="subheading">Defining methods</h4>

<dl class="def">
<dt id="index-define_002dgeneric"><span class="category">Macro: </span><span><strong>define-generic</strong> <em>name :key class</em><a href='#index-define_002dgeneric' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Creates a generic function and bind it to <var>name</var>.
</p>
<p>You don&rsquo;t usually need to use this, since the <code>define-method</code>
macro implicitly creates a generic function if it doesn&rsquo;t exist yet.
</p>
<p>You can pass a subclass of <code>&lt;generic&gt;</code> to the <var>class</var>
keyword argument so that the created generic function will be the
instance of the passed class, instead of the default <code>&lt;generic&gt;</code> class.
It is useful when you defined a subclass of <code>&lt;generic&gt;</code> to customize
generic function application behavior.
</p></dd></dl>

<dl class="def">
<dt id="index-define_002dmethod"><span class="category">Macro: </span><span><strong>define-method</strong> <em>name [qualifier &hellip;] specs body</em><a href='#index-define_002dmethod' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Defines a method whose name is <var>name</var>.  If there&rsquo;s already
a generic function object globally bound to <var>name</var>, the created
method is added to the generic function.  If <var>name</var> is unbound,
or bound to an object except a generic function, then a new generic
function is created, bound to <var>name</var>, then a new method is
added to it.
</p>
<p>The name can be followed by optional <var>qualifiers</var>, each of
which is a keyword.  Currently, only the following qualifier is
valid.
</p>
<dl compact="compact">
<dt><span><code>:locked</code></span></dt>
<dd><p>Declares that you won&rsquo;t redefine
the method with the same specifiers.  Attempt to redefine it
will raise an error.  (You can still define methods with
different specifiers.)
</p>
<p>Most methods concerning basic operations on built-in objects
are locked, for redefining them would case Gauche&rsquo;s infrastracture
unstable.  It also allows Gauche to perform certain optimizations.
</p></dd>
</dl>

<p><var>Specs</var> specifies the arguments and their types for this method.
It&rsquo;s like the argument list of lambda form, except you can specify
the type of each argument.
</p>
<div class="example">
<pre class="example"><i>specs</i> : ( <i>arg</i> &hellip; )
      | ( <i>arg</i> &hellip; . <i>symbol</i> )
      | ( <i>arg</i> &hellip; <i>extended-spec</i> &hellip;)
      | <i>symbol</i>

<i>arg</i>   : ( <i>symbol</i> <i>class</i> )
      | <i>symbol</i>
</pre></div>

<p><i>Class</i> specifies the class that the argument has to belong to.
If <code><i>arg</i></code> is just a symbol, it is equivalent to
<code>(<i>arg</i> &lt;top&gt;)</code>.  You can&rsquo;t specify the type for
the &ldquo;rest&rdquo; argument, for it is always bound to a list.
</p>
<p>You can use extended argument specifications such as
<code>:optional</code>, <code>:key</code> and <code>:rest</code> as well.
(See <a href="Making-procedures.html">Making procedures</a>, for the explanation of extended
argument specifications).  Those extended arguments are treated
as if a single &ldquo;rest&rdquo; argument in terms of dispatching; they
aren&rsquo;t used for method dispatch, and you can&rsquo;t specify classes for
these optional and keyword arguments.
</p>
<p>The list of classes of the argument list is called
<em>method specializer list</em>, based on which the generic
function will select appropriate methods(s).
Here are some examples of <var>specs</var> and the corresponding
specializer list (note that the rest argument isn&rsquo;t considered
as a part of specializer list; we know it&rsquo;s always a list.)
The <code>optional</code> item indicates whether the method takes
rest arguments or not.
</p>
<div class="example">
<pre class="example">specs:        ((self &lt;myclass&gt;) (index &lt;integer&gt;) value)
specializers: (&lt;myclass&gt; &lt;integer&gt; &lt;top&gt;)
optional:     #f

specs:        (obj (attr &lt;string&gt;))
specializers: (&lt;top&gt; &lt;string&gt;)
optional:     #f

specs:        ((self &lt;myclass&gt;) obj . options)
specializers: (&lt;myclass&gt; &lt;top&gt;)
optional:     #t

specs:        ((self &lt;myclass&gt;) obj :optional (a 0) (b 1) :key (c 2))
specializers: (&lt;myclass&gt; &lt;top&gt;)
optional:     #t

specs:        args
specializers: ()
optional:     #t
</pre></div>

<p>If you define a method on <var>name</var> whose specializer list,
and whether it takes rest arguments,
match with one in the generic function&rsquo;s methods,
then the existing method is
replaced by the newly defined one, unless the original method is locked.
</p>
<p>Note: If you&rsquo;re running Gauche with keyword-symbol integrated
mode (see <a href="Keywords.html#Keyword-and-symbol-integration">Keyword and symbol integration</a>), there&rsquo;s an ambiguity
if you specify a keyword as the sole <var>specs</var> (to receive entire
arguments in a single variable).
Gauche parses keywords following <var>name</var>
as qualifiers, so avoid using a keyword as such a variable.
</p></dd></dl>

<span id="Applying-generic-function"></span><h4 class="subheading">Applying generic function</h4>

<p>When a generic function is applied, first it selects
methods whose specializer list matches the given arguments.
For example, suppose a generic function <code>foo</code> has
three methods, whose specializer lists are
<code>(&lt;string&gt; &lt;top&gt;)</code>, <code>(&lt;string&gt; &lt;string&gt;)</code>,
and <code>(&lt;top&gt; &lt;top&gt;)</code>, respectively.
When <code>foo</code> is applied like <code>(foo &quot;abc&quot; 3)</code>,
the first and the third method will be selected.
</p>
<p>Then the selected methods are sorted from the most
<em>specific</em> method to the least specific method.
It is calculated as follows:
</p>
<ul>
<li> Suppose we have a method <code>a</code> that has
specializers <code>(A1 A2 &hellip;)</code>, and
a method <code>b</code> that has <code>(B1 B2 &hellip;)</code>.

</li><li> Find the minimum <var>n</var> where the classes <code>An</code> and <code>Bn</code>
differ.   Then the class of <var>n</var>-th argument is taken,
and its class precedence list is checked.
If <code>An</code> comes before <code>Bn</code> in the CPL, then
method <code>a</code> is more specific than <code>b</code>.
Otherwise, <code>b</code> is more specific than <code>a</code>.

</li><li> If all the specializers of <code>a</code> and <code>b</code> are the same,
except that one has an improper tail (&quot;rest&quot; argument)
and another doesn&rsquo;t, then the method that doesn&rsquo;t have an improper
tail is more specific than the one that has.
</li></ul>

<p>Once methods are sorted, the body of the first method is
called with the actual argument.
</p>
<p>Within the method body, a special local variable <code>next-method</code>
is bound implicitly.
</p>
<dl class="def">
<dt id="index-next_002dmethod"><span class="category">Next method: </span><span><strong>next-method</strong><a href='#index-next_002dmethod' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-next_002dmethod-1"><span class="category">Next method: </span><span><strong>next-method</strong> <em>args &hellip;</em><a href='#index-next_002dmethod-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>This variable is bound within a method body
to a special object that encapsulates
the next method in the sorted method list.
</p>
<p>Calling without arguments invokes the next method with
the same arguments as this method is called with.
Passing <var>args</var> &hellip; explicitly invokes the next method
with the passed arguments.
</p>
<p>If <code>next-method</code> is called in the least specific method,
i.e. there&rsquo;s no &quot;next method&quot;, an error is signaled.
</p></dd></dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="Metaobject-protocol.html#Metaobject-protocol" accesskey="n" rel="next">Metaobject protocol</a>, Previous: <a href="Instance.html#Instance" accesskey="p" rel="prev">Instance</a>, Up: <a href="Object-system.html" accesskey="u" rel="up">Object system</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
