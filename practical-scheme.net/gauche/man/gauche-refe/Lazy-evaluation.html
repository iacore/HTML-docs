<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Lazy evaluation (Gauche Users&rsquo; Reference)</title>

<meta name="description" content="Lazy evaluation (Gauche Users&rsquo; Reference)">
<meta name="keywords" content="Lazy evaluation (Gauche Users&rsquo; Reference)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Function-and-Syntax-Index.html" rel="index" title="Function and Syntax Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Core-library.html" rel="up" title="Core library">
<link href="Exceptions.html#Exceptions" rel="next" title="Exceptions">
<link href="Boxes.html" rel="prev" title="Boxes">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div><hr><div class="section" id="Lazy-evaluation">
<div class="header">
<p>
Next: <a href="Exceptions.html#Exceptions" accesskey="n" rel="next">Exceptions</a>, Previous: <a href="Boxes.html" accesskey="p" rel="prev">Boxes</a>, Up: <a href="Core-library.html" accesskey="u" rel="up">Core library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Lazy-evaluation-1"></span><h3 class="section">6.18 Lazy evaluation</h3>

<p>Gauche has two primitive lazy evaluation mechanisms.
</p>
<p>The first one is an explicit mechanism, defined in the Scheme
standard: You mark
an expression to be evaluated lazily by <code>delay</code>,
and you use <code>force</code> to make the evaluation happen when
needed.  Gauche also support another primitive <code>lazy</code>,
as defined in SRFI-45, for space-efficient tail-recursive
lazy algorithms.
</p>
<p>The second one is a lazy sequence, in which evaluation happens
implicitly.  From a Scheme program, a lazy sequence just looks
as a list&mdash;you can take its <code>car</code> and <code>cdr</code>, and
you can apply <code>map</code> or other list procedures on it.  However,
internally, its element isn&rsquo;t calculated until it is required.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="#Delay-force-and-lazy" accesskey="1">Delay force and lazy</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="#Lazy-sequences" accesskey="2">Lazy sequences</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>


<hr>
<div class="subsection" id="Delay-force-and-lazy">
<div class="header">
<p>
Next: <a href="#Lazy-sequences" accesskey="n" rel="next">Lazy sequences</a>, Previous: <a href="#Lazy-evaluation" accesskey="p" rel="prev">Lazy evaluation</a>, Up: <a href="#Lazy-evaluation" accesskey="u" rel="up">Lazy evaluation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Delay_002c-force-and-lazy"></span><h4 class="subsection">6.18.1 Delay, force and lazy</h4>

<p>Scheme has traditionally provided an explicit delayed evaluation mechanism
using <code>delay</code> and <code>force</code>.  After R5RS, however,
it is found that it didn&rsquo;t mix well with tail-recursive
algorithms: It required unbound memory, despite that the
body of the algorithm could be expressed in iterative manner.
SRFI-45 showed that introducing another primitive syntax <code>lazy</code>
addresses the issue.
For the detailed explanation please look at the SRFI-45 document.
Here we explain how to use those primitives.
</p>
<dl class="def">
<dt id="index-delay"><span class="category">Special Form: </span><span><strong>delay</strong> <em>expression</em><a href='#index-delay' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-lazy"><span class="category">Special Form: </span><span><strong>lazy</strong> <em>expression</em><a href='#index-lazy' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS lazy][SRFI-45]
These forms creates a <em>promise</em> that delays the evaluation
of <var>expression</var>.  <var>Expression</var> will be evaluated
when the promise is passed to <code>force</code>.
</p>
<p>If <var>expression</var> itself is expected to yield a promise,
you should use <code>lazy</code>.  Otherwise, you should use <code>delay</code>.
If you can think in types, the difference may be clearer.
</p>
<div class="example">
<pre class="example">lazy  : Promise a -&gt; Promise a
delay : a -&gt; Promise a
</pre></div>

<p>Since we don&rsquo;t have static typing, we can&rsquo;t enforce this usage.
The programmer has to choose appropriate one from the context.
Generally, <code>lazy</code> appears only to surround the entire
body of function that express a lazy algorithm.
</p>
<p>NB: In R7RS, <code>lazy</code> is called <code>delay-force</code>, for the operation
is conceptually similar to <code>(delay (force expr))</code> (note that the
type of <code>force</code> is <code>Promise a -&gt; a</code>).
</p>
<p>For the real-world example of use of <code>lazy</code>,
you may want to check the implementation of <code>util.stream</code>
(see <a href="Stream-library.html#Stream-library"><code>util.stream</code> - Stream library</a>).
</p>
<p>Note that <var>expression</var> may be evaluated more than once;
it may require the value of its own promise recursively, or more than one
thread may evaluate <var>expression</var> simultaneously.
In general, you shouldn&rsquo;t have side-effecting computation
in <var>expression</var>&mdash;even if it is only evaluated once,
you may not exactly know when the evaluation occur, so
having side-effects there is a bad ides.
(See <code>force</code> below for multi-thread semantics).
</p></dd></dl>

<dl class="def">
<dt id="index-eager"><span class="category">Function: </span><span><strong>eager</strong> <em>obj &hellip;</em><a href='#index-eager' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-make_002dpromise"><span class="category">Function: </span><span><strong>make-promise</strong> <em>obj &hellip;</em><a href='#index-make_002dpromise' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[SRFI-45][SRFI-226]
Returns a promise that returns <var>obj</var> &hellip;.
The name <code>eager</code> is introduced in SRFI-45.  It takes single argument,
but Gauche enhanced it to take multiple values.
SRFI-226 defines <code>make-promise</code>, which is the same
as multi-value <code>eager</code>.
(Note that R7RS-small also defines <code>make-promise</code> in
<code>scheme.lazy</code> library (see <a href="R7RS-small-language.html#R7RS-lazy-evaluation"><code>scheme.lazy</code> - R7RS lazy evaluation</a>).
It is slightly different that it only takes one argument,
and if the argument is a promise, it will be returned as is.
This change is intentional; see SRFI-226 for the details.)
</p>
<p>Since that these are procedures, arguments are evaluated
before they are called; so they work as a type converter
(<code>a -&gt; Promise a</code>) without delaying the evaluation.
Used mainly to construct promise-returning functions.
</p></dd></dl>

<dl class="def">
<dt id="index-force"><span class="category">Function: </span><span><strong>force</strong> <em>promise</em><a href='#index-force' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS lazy][SRFI-226]
If <var>promise</var> is not a promise, it is just returned.
</p>
<p>Otherwise, if <var>promise</var>&rsquo;s value hasn&rsquo;t been computed,
<code>force</code> makes <var>promise</var>&rsquo;s encapsulated expression
be evaluated in the same parameterization where the promise
is created, and returns the result.
</p>
<p>Once <var>promise</var>&rsquo;s value is computed, it is memorized in it
so that subsequent <code>force</code> on it won&rsquo;t cause the computation.
</p>
<p>In Gauche, if the delayed expression yields multiple values, <code>force</code>
yields multiple values as well.  (It is unspecified in R7RS).
</p>

<p>Note that if uncomputed promise is forced simultaneously
by more than one thread, its expression may be evaluated concurrently.
The first delivered value becomes the value of the promise; if another
thread finishes computation and see the value of promise is already
determined, it discards its computation result and returns the determined
value.
</p>
<p>The following example shows the promise is evaluated with the
parameterization of <code>delay</code>:
</p>
<div class="example">
<pre class="example">(define p (make-parameter 1))

(let1 v (parameterize ((p 2)) (delay (p)))
  (parameterize ((p 3))
    (force v)))
  &rArr; 2
</pre></div>

<p>Note: R7RS defines <code>force</code> to evaluate the unevaluated promise&rsquo;s body
in the dynamic environment of the caller of <code>force</code>, but it is
regarded as a mistake.  In general, you shouldn&rsquo;t care which <code>force</code> call
actually evaluates the promise&rsquo;s body, so the result shouldn&rsquo;t depend
on the context of <code>force</code> call.  SRFI-226 addressed it.
</p></dd></dl>

<dl class="def">
<dt id="index-promise_003f"><span class="category">Function: </span><span><strong>promise?</strong> <em>obj</em><a href='#index-promise_003f' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS lazy]
Returns <code>#t</code> iff <var>obj</var> is a promise object.
</p></dd></dl>


<hr>
</div>
<div class="subsection" id="Lazy-sequences">
<div class="header">
<p>
Previous: <a href="#Delay-force-and-lazy" accesskey="p" rel="prev">Delay, force and lazy</a>, Up: <a href="#Lazy-evaluation" accesskey="u" rel="up">Lazy evaluation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<span id="Lazy-sequences-1"></span><h4 class="subsection">6.18.2 Lazy sequences</h4>

<span id="Introduction-3"></span><h4 class="subsubheading">Introduction</h4>

<p>A lazy sequence is a list-like structure
whose elements are calculated lazily.
Internally we have a special type of pairs, whose <code>cdr</code>
is evaluated on demand.
However, in Scheme level, you&rsquo;ll never see a distinct
&ldquo;lazy-pair&rdquo; type.   As soon as you try to access a
lazy pair, Gauche automatically <em>force</em> the delayed
calculation, and the lazy pair turns into an ordinary pair.
</p>
<p>It means you can pass lazy sequences to ordinary list-processing
procedures such as <code>car</code>, <code>cdr</code> or <code>map</code>.
</p>
<p>Look at the following example; <code>generator-&gt;lseq</code>
takes a procedure that generates one value at a time, and
returns a lazy sequence that consists of those values.
</p>
<div class="example">
<pre class="example">(with-input-from-file &quot;file&quot;
  (^[] (let loop ([cs (generator-&gt;lseq read-char)] [i 0])
         (match cs
           [() #f]
           [(#\c (or #\a #\d) #\r . _) i]
           [(c . cs) (loop cs (+ i 1))]))))
</pre></div>

<p>It returns the position of the first occurrence of
character sequence &ldquo;car&rdquo; or &ldquo;cdr&rdquo; in the file <samp>file</samp>.
The loop treats the lazy sequence just like an ordinary list, but
characters are read as needed, so once the sequence is
found, the rest of the file won&rsquo;t be read.  If we do
it eagerly, we would have to read the entire file first no matter how
big it is, or to give up using the mighty <code>match</code> macro and
to write a basic state machine that reads one character one at a time.
</p>
<p>Other than implicit forcing, Gauche&rsquo;s lazy sequences are
slightly different than the typical lazy stream implementations in Scheme
in the following ways:
</p>
<ol>
<li> When you construct a lazy sequence in an iterative lazy algorithm,
only <code>cdr</code> side of the lazy pair is lazily evaluated;
the <code>car</code> side is evaluated immediately.  On the other hand, with
<code>stream-cons</code> in <code>util.stream</code> (see <a href="Stream-library.html#Stream-library"><code>util.stream</code> - Stream library</a>),
both <code>car</code> and <code>cdr</code> sides won&rsquo;t be evaluated until
it is absolutely needed.
</li><li> Gauche&rsquo;s lazy sequence always evaluates <em>one item ahead</em>.
Once you get a lazy pair, its <code>car</code> part is already
calculated, even if you don&rsquo;t use it.  In most cases
you don&rsquo;t need to care, for calculating one item more
is a negligible overhead.  However, when you create
a self-referential lazy structure, in which the earlier
elements of a sequence is used to calculate the latter
elements of itself, a bit of caution is needed;
a valid code for fully lazy circular structure may not
terminate in Gauche&rsquo;s lazy sequences.  We&rsquo;ll show a
concrete example later.  This bit of eagerness
is also visible when side effects are involved;
for example, lazy character sequence reading from a port
may read one character ahead.
</li></ol>

<p>Note: R7RS <code>scheme.lseq</code> (SRFI-127)
provides a portable alternative of lazy sequence
(see <a href="R7RS-large.html#R7RS-lazy-sequences"><code>scheme.lseq</code> - R7RS lazy sequences</a>).  It uses
dedicated APIs (e.g. <code>lseq-cdr</code>) to operate on lazy sequences
so that portable implementation is possible.  In Gauche, we just
use our built-in lazy sequence as SRFI-127 lazy sequence; if you
want your code to be portable, consider using SRFI-127, but be careful
not to mix lazy sequences and ordinary lists; Gauche won&rsquo;t complain,
but other Scheme implementation may choke on it.
</p>
<span id="Primitives"></span><h4 class="subsubheading">Primitives</h4>

<dl class="def">
<dt id="index-generator_002d_003elseq"><span class="category">Function: </span><span><strong>generator-&gt;lseq</strong> <em>generator</em><a href='#index-generator_002d_003elseq' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-generator_002d_003elseq-1"><span class="category">Function: </span><span><strong>generator-&gt;lseq</strong> <em>item &hellip; generator</em><a href='#index-generator_002d_003elseq-1' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>[R7RS lseq]
Creates a lazy sequence that consists of items produced
by <var>generator</var>, which is just a procedure with zero arguments that yields
an item at a time.  Returning EOF marks the end of the sequence
(EOF itself isn&rsquo;t included in the sequence).
For example, <code>read-char</code> can work as a generator.
Gauche has a set of convenient utilities to deal with generators
(see <a href="Generators.html#Generators"><code>gauche.generator</code> - Generators</a>).
</p>
<p>In the second form, the returned lazy sequence is prepended by
<var>item</var> &hellip;.  Since there&rsquo;s no way to distinguish lazy
pairs and ordinary pairs, you can write it as
<code>(cons* item &hellip; (generator-&gt;lseq generator))</code>,
but that&rsquo;s more verbose.
</p>
<p>Internally, Gauche&rsquo;s lazy sequence is optimized to be built
on top of generators, so this procedure is the most efficient
way to build lazy sequences.
</p>
<p>Note: SRFI-127 also has <code>generator-&gt;lseq</code>, which is exactly
the same as this in Gauche.
</p></dd></dl>

<dl class="def">
<dt id="index-lcons"><span class="category">Macro: </span><span><strong>lcons</strong> <em>car cdr</em><a href='#index-lcons' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Returns a lazy pair consists of <var>car</var> and <var>cdr</var>.
The expression <var>car</var> is evaluated at the call of <code>lcons</code>,
but evaluation of <var>cdr</var> is delayed.
</p>
<p>You can&rsquo;t distinguish a lazy pair from an ordinary pair.  If you
access either its <code>car</code> or <code>cdr</code>, or even you ask <code>pair?</code>
to it, its cdr part is implicitly forced and you get an ordinary pair.
</p>
<p>Unlike <code>cons</code>, <var>cdr</var> should be an expression that yields
a (lazy or ordinary) list, including an empty list.
In other words, lazy sequences can always be a null-terminated list
when entirely forced; there are no &ldquo;improper lazy sequences&rdquo;.
(Since Scheme isn&rsquo;t statically typed, we can&rsquo;t force the <var>cdr</var>
expression to be a proper list before actually evaluating it.
Currently if <var>cdr</var> expression yields non-list, we just ignore
it and treat as if it yielded an empty list.)
</p>
<div class="example">
<pre class="example">(define z (lcons (begin (print 1) 'a) (begin (print 2) '())))
 &rArr; ; prints '1', since the car part is evaluated eagerly.

(cdr z) &rArr; () ;; and prints '2'

;; This also prints '2', for accessing car of a lazy pair forces
;; its cdr, even the cdr part isn't used.
(car (lcons 'a (begin (print 2) '()))) &rArr; a

;; So as this; asking pair? to a lazy pair causes forcing its cdr.
(pair? (lcons 'a (begin (print 2) '()))) &rArr; #t

;; To clarify: This doesn't print '2', because the second lazy
;; pair never be accessed, so its cdr isn't evaluated.
(pair? (lcons 'a (lcons 'b (begin (print 2) '())))) &rArr; #t
</pre></div>

<p>Now, let me show you a case where &ldquo;one item ahead&rdquo; evaluation becomes
an issue.  The following is an elegant definition of infinite
Fibonacci sequence using self-referential lazy structure
(<code>lmap</code> is a lazy map, defined in <code>gauche.lazy</code> module):
</p>
<div class="example">
<pre class="example">(use gauche.lazy)  ;; for lmap
(define *fibs* (lcons* 0 1 (lmap + *fibs* (cdr *fibs*)))) ;; BUGGY
</pre></div>

<p>Unfortunately, Gauche can&rsquo;t handle it well.
</p>
<div class="example">
<pre class="example">(car *fibs*)
 &rArr; 0
(cadr *fibs*)
 &rArr; *** ERROR: Attempt to recursively force a lazy pair.
</pre></div>

<p>When we want to access the second argument (<code>cadr</code>) of <code>*fibs*</code>,
we take the car of the second pair,
which is a lazy pair of <code>1</code> and <code>(lmap ...)</code>.  The lazy pair
is forced and its cdr part needs to be calculated.  The first thing
<code>lmap</code> returns needs to see the first and second element of <code>*fibs*</code>,
but the second element of <code>*fibs*</code> is what we&rsquo;re calculating now!
</p>
<p>We can workaround this issue by
avoiding accessing the immediately preceding value.
Fibonacci numbers F(n) = F(n-1) + F(n-2) = 2*F(n-2) + F(n-3), so we
can write our sequence as follows.
</p>
<div class="example">
<pre class="example">(define *fibs*
  (lcons* 0 1 1 (lmap (^[a b] (+ a (* b 2))) *fibs* (cdr *fibs*))))
</pre></div>

<p>And this works!
</p>
<div class="example">
<pre class="example">(take *fibs* 20)
  &rArr; (0 1 1 2 3 5 8 13 21 34 55 89 144 233
     377 610 987 1597 2584 4181)
</pre></div>

<p>Many lazy algorithms are defined in terms of fully-lazy cons
at the bottom.  When you port such algorithms to Gauche using <code>lcons</code>,
keep this bit of eagerness in mind.
</p>
<p>Note also that <code>lcons</code> needs to create a thunk to delay
the evaluation.  So the algorithm to construct lazy list using
<code>lcons</code> has an overhead of making closure for each item.
For performance-critical part,
you want to use <code>generator-&gt;lseq</code> whenever possible.
</p></dd></dl>

<span id="Utilities"></span><h4 class="subsubheading">Utilities</h4>

<dl class="def">
<dt id="index-lcons_002a"><span class="category">Macro: </span><span><strong>lcons*</strong> <em>x &hellip; tail</em><a href='#index-lcons_002a' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-llist_002a"><span class="category">Macro: </span><span><strong>llist*</strong> <em>x &hellip; tail</em><a href='#index-llist_002a' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>A lazy version of <code>cons*</code> (see <a href="Pairs-and-lists.html#List-constructors">List constructors</a>).
Both <code>lcons*</code> and <code>llist*</code> do the same thing; both
names are provided for the symmetry to <code>cons*</code>/<code>list*</code>.
</p>
<p>The <var>tail</var> argument should be an expression that yields a
(possibly lazy) list.   It is evaluated lazily.  Note that
the preceding elements <var>x</var> &hellip; are evaluated eagerly.
The following equivalences hold.
</p>
<div class="example">
<pre class="example">(lcons* a)           &equiv; a
(lcons* a b)         &equiv; (lcons a b)
(lcons* a b ... y z) &equiv; (cons* a b &hellip; (lcons y z))
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-lrange"><span class="category">Function: </span><span><strong>lrange</strong> <em>start :optional end step</em><a href='#index-lrange' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Creates a lazy sequence of numbers starting from <var>start</var>,
increasing by <var>step</var> (default 1), to the maximum value that doesn&rsquo;t
exceed <var>end</var>.  The default of <var>end</var> is <code>+inf.0</code>,
so it creates an infinite list.  (Don&rsquo;t type just
<code>(lrange 0)</code> in REPL, or it won&rsquo;t terminate!)
</p>
<p>If any of <var>start</var> or <code>step</code> is inexact, the resulting sequence
has inexact numbers.
</p>
<div class="example">
<pre class="example">(take (lrange -1) 3) &rArr; (-1 0 1)

(lrange 0.0 5 0.5)
  &rArr; (0.0 0.5 1.0 1.5 2.0 2.5 3.0 3.5 4.0 4.5)

(lrange 1/4 1 1/8)
  &rArr; (1/4 3/8 1/2 5/8 3/4 7/8)
</pre></div>
</dd></dl>

<dl class="def">
<dt id="index-liota"><span class="category">Function: </span><span><strong>liota</strong> <em>:optional (count +inf.0) (start 0) (step 1)</em><a href='#index-liota' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>A lazy version of <code>iota</code> (see <a href="Pairs-and-lists.html#List-constructors">List constructors</a>); returns
a lazy sequence of <var>count</var> integers (default: positive infinity),
starting from <var>start</var> (default: 0), stepping by <var>step</var> (default: 1).
</p>
<p>Just like <code>iota</code>, the result consists of exact numbers
if and only if both <var>start</var> and <var>step</var> are exact; otherwise
the result consists of inexact numbers.
</p></dd></dl>

<dl class="def">
<dt id="index-port_002d_003echar_002dlseq"><span class="category">Function: </span><span><strong>port-&gt;char-lseq</strong> <em>:optional port</em><a href='#index-port_002d_003echar_002dlseq' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-port_002d_003ebyte_002dlseq"><span class="category">Function: </span><span><strong>port-&gt;byte-lseq</strong> <em>:optional port</em><a href='#index-port_002d_003ebyte_002dlseq' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-port_002d_003estring_002dlseq"><span class="category">Function: </span><span><strong>port-&gt;string-lseq</strong> <em>:optional port</em><a href='#index-port_002d_003estring_002dlseq' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-port_002d_003esexp_002dlseq"><span class="category">Function: </span><span><strong>port-&gt;sexp-lseq</strong> <em>:optional port</em><a href='#index-port_002d_003esexp_002dlseq' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>These are the same as the following expressions, respectively.
They are provided for the convenience, since this pattern appears
frequently.
</p>
<div class="example">
<pre class="example">(generator-&gt;lseq (cut read-char port))
(generator-&gt;lseq (cut read-byte port))
(generator-&gt;lseq (cut read-line port))
(generator-&gt;lseq (cut read port))
</pre></div>

<p>If <var>port</var> is omitted, the current input port is used.
</p>
<p>Note that the lazy sequence may buffer some items, so
once you make an lseq from a port, only use the resulting lseq
and don&rsquo;t ever read from <var>port</var> directly.
</p>
<p>Note that the lazy sequence terminates when EOF is read from the port,
but the port isn&rsquo;t closed.  The port should be managed in larger
dynamic extent where the lazy sequence is used.
</p>
<p>You can also convert input data into various lists by
the following expressions (see <a href="Input-and-output.html#Input-utility-functions">Input utility functions</a>).
Those procedures read the port eagerly until EOF and returns
the whole data in a list, while <code>lseq</code> versions read
the port lazily.
</p>
<div class="example">
<pre class="example">(port-&gt;list read-char port)
(port-&gt;list read-byte port)
(port-&gt;string-list port)
(port-&gt;sexp-list port)
</pre></div>

<p>Those procedures make (lazy) lists out of ports.  The opposite can be
done by <code>open-input-char-list</code> and <code>open-input-byte-list</code>;
See <a href="Virtual-ports.html"><code>gauche.vport</code> - Virtual ports</a>, for the details.
</p></dd></dl>


<p>See also <a href="Lazy-sequence-utilities.html#Lazy-sequence-utilities"><code>gauche.lazy</code> - Lazy sequence utilities</a>, for more utility procedures
that creates lazy sequences.
</p>
<span id="Examples"></span><h4 class="subsubheading">Examples</h4>

<p>Let&rsquo;s consider calculating an infinite sequence of prime numbers.
(Note: If you need prime numbers in your application, you don&rsquo;t
need to write one; just use <code>math.prime</code>. see <a href="Prime-numbers.html"><code>math.prime</code> - Prime numbers</a>).
</p>
<p>Just pretend we already have some prime numbers
calculated in a variable <code>*primes*</code>, and you need
to find a prime number equal to or grater than <var>n</var>
(for simplicity, we assume <var>n</var> is an odd number).
</p>
<div class="example">
<pre class="example">(define (next-prime n)
  (let loop ([ps *primes*])
    (let1 p (car ps)
      (cond [(&gt; (* p p) n) n]
            [(zero? (modulo n p)) (next-prime (+ n 2))]
            [else (loop (cdr ps))]))))
</pre></div>

<p>This procedure loops over the list of prime numbers, and
if no prime number <var>p</var> less than or equal to <code>(sqrt n)</code>
divides <var>n</var>, we can say <var>n</var> is prime.  (Actual test
is done by <code>(&gt; (* p p) n)</code> instead of <code>(&gt; p (sqrt n))</code>,
for the former is faster.)
If we find some <var>p</var> divides <var>n</var>, we try a new value
<code>(+ n 2)</code> with <code>next-prime</code>.
</p>
<p>Using <code>next-prime</code>, we can make a generator that keeps generating
prime numbers.  The following procedure returns a generator
that returns primes above <var>last</var>.
</p>
<div class="example">
<pre class="example">(define (gen-primes-above last)
  (^[] (set! last (next-prime (+ last 2))) last))
</pre></div>

<p>Using <code>generator-&gt;lseq</code>, we can turn the generator returned
by <code>gen-primes-above</code> into a lazy list, which can be used
as the value of <code>*prime*</code>.  The only caveat is that we need
to have some pre-calculated prime numbers:
</p>
<div class="example">
<pre class="example">(define *primes* (generator-&gt;lseq 2 3 5 (gen-primes-above 5)))
</pre></div>

<p>Be careful not to evaluate <code>*primes*</code> directly on REPL,
since it contains an infinite list and it&rsquo;ll blow up your REPL.
You can look at the first 20 prime numbers instead:
</p>
<div class="example">
<pre class="example">(take *primes* 20)
 &rArr; (2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71)
</pre></div>

<p>Or find what the 10000-th prime number is:
</p>
<div class="example">
<pre class="example">(~ *primes* 10000)
 &rArr; 104743
</pre></div>

<p>Or count how many prime numbers there are below 1000000:
</p>
<div class="example">
<pre class="example">(any (^[p i] (and (&gt;= p 1000000) i)) *primes* (lrange 0))
 &rArr; 78498
</pre></div>

<p>Note: If you&rsquo;re familiar with the lazy functional approach, this example
may look strange.  Why do we use side-effecting generators
while we can define a sequence of prime numbers in
pure functional way, as follows?
</p>
<div class="example">
<pre class="example">(use gauche.lazy)

(define (prime? n)
  (not (any (^p (zero? (mod n p)))
            (ltake-while (^k (&lt;= (* k k) n)) *primes*))))

(define (primes-from k)
  (if (prime? k)
    (lcons k (primes-from (+ k 2)))
    (primes-from (+ k 2))))

(define *primes* (llist* 2 3 5 (primes-from 7)))
</pre></div>

<p>(The module <code>gauche.lazy</code> provides <code>ltake-while</code>, which
is a lazy version of <code>take-while</code>.  We don&rsquo;t need lazy version
of <code>any</code>, since it immediately stops when the predicate returns
a true value.)
</p>
<p>The use of <code>lcons</code> and co-recursion in <code>primes-from</code>
is a typical idiom in functional programming.  It&rsquo;s perfectly ok
to do so in Gauche; except that the generator version is <em>much</em> faster
(when you take first 5000 primes, generator version ran 17 times faster
than co-recursion version on the author&rsquo;s machine).
</p>
<p>It doesn&rsquo;t mean you should avoid co-recursive code; if an algorithm
can be expressed nicely in co-recursion, it&rsquo;s perfectly ok.  However,
watch out the subtle semantic difference from lazy functional
languages&mdash;straightforward porting may or may not work.
</p>
</div>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Exceptions.html#Exceptions" accesskey="n" rel="next">Exceptions</a>, Previous: <a href="Boxes.html" accesskey="p" rel="prev">Boxes</a>, Up: <a href="Core-library.html" accesskey="u" rel="up">Core library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Function-and-Syntax-Index.html" title="Index" rel="index">Index</a>]</p>
</div>


<hr><div style="width:100%;background-color:#cfc;"><form action="https://practical-scheme.net/gauche/man/"style="padding:5px 10px"><a href="https://practical-scheme.net/gauche/memo.html">For Gauche 0.9.14</a><span style="float: right">Search (procedure/syntax/module): <input type="text" name="p"><input type="hidden" name="l" value="en"></span></form></div>
</body>
</html>
