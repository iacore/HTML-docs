<!doctype html><html>
<meta charset=utf-8>
<meta name=viewport content="width=device-width,initial-scale=1">
<link rel=icon href=../../favicon.svg>
<meta property="og:title" content="The Odin Programming Language Specification">
<meta property="og:description" content="The Odin Programming Language Specification">
<meta property="og:type" content="article">
<meta property="og:url" content="https://odin-lang.org/docs/spec/">
<meta property="og:image" content="https://odin-lang.org/images/logo-slim.png">
<link rel=stylesheet href=../../scss/custom.min.css>
<link rel=stylesheet href=../../lib/highlight/styles/github-dark.min.css>
<script src=../../lib/highlight/highlight.min.js></script>
<script>hljs.registerLanguage("odin",function(a){return{aliases:["odin","odinlang","odin-lang"],keywords:{keyword:"auto_cast bit_set break case cast context continue defer distinct do dynamic else enum fallthrough for foreign if import in map matrix not_in or_else or_return package proc return struct switch transmute type_of typeid union using when where",literal:"true false nil",built_in:"abs align_of cap clamp complex conj expand_to_tuple imag jmag kmag len max min offset_of quaternion real size_of soa_unzip soa_zip swizzle type_info_of type_of typeid_of"},illegal:"</",contains:[a.C_LINE_COMMENT_MODE,a.C_BLOCK_COMMENT_MODE,{className:"string",variants:[a.QUOTE_STRING_MODE,{begin:"'",end:"[^\\\\]'"},{begin:"`",end:"`"}]},{className:"number",variants:[{begin:a.C_NUMBER_RE+"[ijk]",relevance:1},a.C_NUMBER_MODE]}]}})</script>
<script>hljs.highlightAll()</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67516878-2"></script>
<script>window.dataLayer=window.dataLayer||[];function gtag(){dataLayer.push(arguments)}gtag('js',new Date),gtag('config','UA-67516878-2')</script>
<link href=../../css/style.css rel=stylesheet>
<title>The Odin Programming Language Specification | Odin Programming Language </title>
<body><header class=sticky-top>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary odin-menu">
<div class=container>
<a class=navbar-brand href=../../index.html>
<img src=../../logo.svg height=30 alt=Odin></a>
<button class=navbar-toggler type=button data-bs-toggle=collapse data-bs-target=#odin-navbar-content aria-controls=odin-navbar-content aria-expanded=false aria-label="Toggle navigation">
<span class=navbar-toggler-icon></span>
</button>
<div class="collapse navbar-collapse" id=odin-navbar-content>
<ul class="navbar-nav ms-md-auto">
<li class=nav-item>
<a class=nav-link href=../../index.html>Home</a>
</li>
<li class=nav-item>
<a class="nav-link active" href=../../docs aria-current=page>Docs</a>
</li>
<li class=nav-item>
<a class=nav-link href=https://pkg.odin-lang.org/>Packages</a>
</li>
<li class=nav-item>
<a class=nav-link href=../../news>News</a>
</li>
<li class=nav-item>
<a class=nav-link href=../../showcase>Showcase</a>
</li>
<li class=nav-item>
<a class=nav-link href=../../community>Community</a>
</li>
<li class=nav-item>
<a class=nav-link href=https://github.com/odin-lang/Odin target=_blank>GitHub</a>
</li>
</ul>
</div>
</div>
</nav>
</header>
<main>
<div class=container>
<div class="row odin-main">
<nav class="col-lg-2 odin-sidebar-border navbar-light">
<div class="sticky-top odin-below-navbar py-3">
<ul class="nav nav-pills d-flex flex-column">
<li class=nav-item>
<a class=nav-link href=../install/index.html>Getting Started</a>
</li>
<li class=nav-item>
<a class=nav-link href=../overview/index.html>Overview</a>
</li>
<li class=nav-item>
<a class=nav-link href=../faq/index.html>FAQ</a>
</li>
<li class=nav-item>
<a class=nav-link href=../packages/index.html>Packages</a>
</li>
<li class=nav-item>
<a class=nav-link href=../nightly/index.html>Nightly Builds</a>
</li>
<li class=nav-item>
<a class="nav-link active" href=index.html aria-current=page>Specification</a>
</li>
</ul>
</div>
</nav>
<article class="col-lg-8 p-4">
<header>
<nav aria-label=breadcrumb>
<ol class=breadcrumb>
<li class=breadcrumb-item><a href=../../docs>Docs</a></li>
<li class=breadcrumb-item><a href=http://odin-lang.org/spec>Spec</a></li>
</ol>
</nav>
<h1>The Odin Programming Language Specification</h1>
</header>
<div class=odin-article>
<h2 id=introduction>Introduction <a class=text-decoration-none href=#introduction>#</a></h2>
<p>This is a reference manual for the Odin programming language.</p>
<p>Odin is a general-purpose language designed for systems programming. It is a strongly typed language with manual memory management. Programs are constructed from <em>packages</em>.</p>
<p><strong>NOTE:</strong> <strong>THIS SPECIFICATION IS UNFINISHED AND VERY OUT OF DATE.</strong></p>
<h2 id=notation>Notation <a class=text-decoration-none href=#notation>#</a></h2>
<p>The syntax is specified using Extended Backus-Naur Form (EBNF):</p>
<pre><code>Production  = production_name &quot;=&quot; [ Expression ] &quot;.&quot; .
Expression  = Alternative { &quot;|&quot; Alternative } .
Alternative = Term { Term } .
Term        = production_name | token [ &quot;…&quot; token ] | Group | Option | Repetition .
Group       = &quot;(&quot; Expression &quot;)&quot; .
Option      = &quot;[&quot; Expression &quot;]&quot; .
Repetition  = &quot;{&quot; Expression &quot;}&quot; .
</code></pre><p>Productions are expressions constructed from terms and the following operators, in increasing precedence:</p>
<pre><code>|   alternation
()  grouping
[]  option (0 or 1 times)
{}  repetition (0 to n times)
</code></pre><h2 id=source-code-representation>Source code representation <a class=text-decoration-none href=#source-code-representation>#</a></h2>
<p>Source code is Unicode text encoded in UTF-8. The text is not canonicalized, so a single accented code point is distinct from the same character constructed from combining an accent and a letter; those are treated as two separate code points. In this document, the term <em>character</em> will be used to refer to a Unicode code point in the source text.</p>
<p>Each code point is distinct; there is case sensitivity.</p>
<p>Implementation restriction: A compile <em>must</em> disallow the NUL character (U+0000) in the source text.
Implementation restriction: A compile <em>may</em> ignore a UTF-8-encoded byte order mark (U+FEFF) if it is the first Unicode code point in the source text. A byte order mark <em>must</em> be disallowed anywhere else in the source text.</p>
<h3 id=characters>Characters <a class=text-decoration-none href=#characters>#</a></h3>
<p>The following terms are used to denote specific Unicode character classes:</p>
<pre><code>newline        = /* the Unicode code point U+000A */
unicode_char   = /* an arbitrary Unicode code point except newline */
unicode_letter = /* a Unicode code point classified as &quot;Letter&quot; */
unicode_digit  = /* a Unicode code point classified as &quot;Number, decimal digit&quot; */
</code></pre><p>In <a href=https://www.unicode.org/versions/Unicode8.0.0/>The Unicode Standard 8.0</a>, Section 4.5 &ldquo;General Category&rdquo; defines a set of character categories. Odin treats all characters in any of the Letter categories Lu, Ll, Lt, Lm, or Lo as Unicode letters, and those in the Number category Nd as Unicode digits.</p>
<h3 id=letters-and-digits>Letters and digits <a class=text-decoration-none href=#letters-and-digits>#</a></h3>
<p>The underscore character <code>_</code> (U+005F) is considered a letter.</p>
<pre><code>letter        = unicode_letter | &quot;_&quot; .
binary_digit  = &quot;0&quot; … &quot;1&quot; .
octal_digit   = &quot;0&quot; … &quot;7&quot; .
decimal_digit = &quot;0&quot; … &quot;9&quot; .
dozenal_digit = &quot;0&quot; … &quot;9&quot; | &quot;A&quot; … &quot;B&quot; | &quot;a&quot; … &quot;b&quot; .
hex_digit     = &quot;0&quot; … &quot;9&quot; | &quot;A&quot; … &quot;F&quot; | &quot;a&quot; … &quot;f&quot; .

binary_char  = binary_digit  | &quot;_&quot; .
octal_char   = octal_digit   | &quot;_&quot; .
decimal_char = decimal_digit | &quot;_&quot; .
dozenal_char = dozenal_digit | &quot;_&quot; .
hex_char     = hex_digit     | &quot;_&quot; .
</code></pre><h2 id=lexical-elements>Lexical elements <a class=text-decoration-none href=#lexical-elements>#</a></h2>
<h3 id=comments>Comments <a class=text-decoration-none href=#comments>#</a></h3>
<p>Comments serve as program documentation. There are three forms:</p>
<ol>
<li><em>Line comments</em> start with the character sequence <code>//</code> and stop at the end of the line</li>
<li><em>General comments</em> start with the character sequence <code>/*</code> and stop with a pairing character sequence <code>*/</code> to allow for nested general comments</li>
<li><em>Hash-bang comments</em> start with the character sequence <code>#!</code> and stop at the end of the line</li>
</ol>
<p>A comment cannot start inside a <em>rune</em> or <em>string</em> literal, or inside a line or hash-bang comment.</p>
<h3 id=tokens>Tokens <a class=text-decoration-none href=#tokens>#</a></h3>
<p>Tokens form the vocabulary of the Odin language. There four classes: <em>identifiers</em>, <em>keywords</em>, <em>operators</em> and <em>punctuation</em>, and <em>literals</em>. <em>White space</em>, formed from spaces (U+0020), horizontal tabs (U+0009), carriage returns (U+000D), and new lines (U+000A), is ignored except as it separates tokens that would otherwise combine into a single token</p>
<blockquote>
</blockquote>
</div>
</article>
<div class="col-lg-2 odin-toc-border navbar-light">
<div class="sticky-top odin-below-navbar py-3">
<nav id=TableOfContents>
<ul>
<li><a href=#introduction>Introduction</a></li>
<li><a href=#notation>Notation</a></li>
<li><a href=#source-code-representation>Source code representation</a>
<ul>
<li><a href=#characters>Characters</a></li>
<li><a href=#letters-and-digits>Letters and digits</a></li>
</ul>
</li>
<li><a href=#lexical-elements>Lexical elements</a>
<ul>
<li><a href=#comments>Comments</a></li>
<li><a href=#tokens>Tokens</a></li>
</ul>
</li>
</ul>
</nav>
</div>
</div>
</div>
</div>
</main>
<footer class=odin-footer>
<div class="container pb-5 pt-5">
<div class="row g-4">
<div class=col>
<a class=navbar-brand href=../../index.html>
<img class=mb-3 src=../../logo.svg height=30 alt=Odin></a>
<p>
The Data-Oriented Language for Sane Software Development.
</p>
</div>
<nav class=col-md-auto>
<h4 class=fw-normal>Resources</h4>
<ul class=list-unstyled>
<li><a href=../../docs class=link-light>Docs</a></li>
<li><a href=https://pkg.odin-lang.org/ class=link-light>Packages</a></li>
<li><a href=../../news class=link-light>News</a></li>
</ul>
</nav>
<nav class=col-md-auto>
<h4 class=fw-normal>Community</h4>
<ul class=list-unstyled>
<li><a href=https://github.com/odin-lang/Odin target=_blank class=link-light>GitHub</a></li>
<li><a href=https://discord.com/invite/sVBPHEv target=_blank class=link-light>Discord</a></li>
<li><a href=https://www.twitch.tv/ginger_bill target=_blank class=link-light>Twitch</a></li>
<li><a href=https://www.youtube.com/channel/UCUSck1dOH7VKmG4lRW7tZXg target=_blank class=link-light>YouTube</a></li>
<li><a href=../../showcase target=_blank class=link-light>Showcase</a></li>
</ul>
</nav>
<nav class=col-md-auto>
<h4 class=fw-normal>Contribute</h4>
<ul class=list-unstyled>
<li><a href=https://github.com/odin-lang/Odin/issues target=_blank class=link-light>Issues</a></li>
<li><a href=https://www.patreon.com/gingerbill target=_blank class=link-light>Donate</a></li>
</ul>
</nav>
</div>
<div class="mt-4 text-muted">© 2016–2022 Ginger Bill</div>
</div>
</footer>
<script src=../../lib/bootstrap/js/bootstrap.min.js></script>
<script src=../../js/script.js></script>
</body>
</html>