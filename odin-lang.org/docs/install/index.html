<!doctype html><html>
<meta charset=utf-8>
<meta name=viewport content="width=device-width,initial-scale=1">
<link rel=icon href=../../favicon.svg>
<meta property="og:title" content="Getting Started">
<meta property="og:description" content="Getting Started with Odin. Downloading, installing, and getting your first program to compile and run.">
<meta property="og:type" content="article">
<meta property="og:url" content="https://odin-lang.org/docs/install/">
<meta property="og:image" content="https://odin-lang.org/images/logo-slim.png">
<link rel=stylesheet href=../../scss/custom.min.css>
<link rel=stylesheet href=../../lib/highlight/styles/github-dark.min.css>
<script src=../../lib/highlight/highlight.min.js></script>
<script>hljs.registerLanguage("odin",function(a){return{aliases:["odin","odinlang","odin-lang"],keywords:{keyword:"auto_cast bit_set break case cast context continue defer distinct do dynamic else enum fallthrough for foreign if import in map matrix not_in or_else or_return package proc return struct switch transmute type_of typeid union using when where",literal:"true false nil",built_in:"abs align_of cap clamp complex conj expand_to_tuple imag jmag kmag len max min offset_of quaternion real size_of soa_unzip soa_zip swizzle type_info_of type_of typeid_of"},illegal:"</",contains:[a.C_LINE_COMMENT_MODE,a.C_BLOCK_COMMENT_MODE,{className:"string",variants:[a.QUOTE_STRING_MODE,{begin:"'",end:"[^\\\\]'"},{begin:"`",end:"`"}]},{className:"number",variants:[{begin:a.C_NUMBER_RE+"[ijk]",relevance:1},a.C_NUMBER_MODE]}]}})</script>
<script>hljs.highlightAll()</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67516878-2"></script>
<script>window.dataLayer=window.dataLayer||[];function gtag(){dataLayer.push(arguments)}gtag('js',new Date),gtag('config','UA-67516878-2')</script>
<link href=../../css/style.css rel=stylesheet>
<title>Getting Started | Odin Programming Language </title>
<body><header class=sticky-top>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary odin-menu">
<div class=container>
<a class=navbar-brand href=../../index.html>
<img src=../../logo.svg height=30 alt=Odin></a>
<button class=navbar-toggler type=button data-bs-toggle=collapse data-bs-target=#odin-navbar-content aria-controls=odin-navbar-content aria-expanded=false aria-label="Toggle navigation">
<span class=navbar-toggler-icon></span>
</button>
<div class="collapse navbar-collapse" id=odin-navbar-content>
<ul class="navbar-nav ms-md-auto">
<li class=nav-item>
<a class=nav-link href=../../index.html>Home</a>
</li>
<li class=nav-item>
<a class="nav-link active" href=../../docs aria-current=page>Docs</a>
</li>
<li class=nav-item>
<a class=nav-link href=https://pkg.odin-lang.org/>Packages</a>
</li>
<li class=nav-item>
<a class=nav-link href=../../news>News</a>
</li>
<li class=nav-item>
<a class=nav-link href=../../showcase>Showcase</a>
</li>
<li class=nav-item>
<a class=nav-link href=../../community>Community</a>
</li>
<li class=nav-item>
<a class=nav-link href=https://github.com/odin-lang/Odin target=_blank>GitHub</a>
</li>
</ul>
</div>
</div>
</nav>
</header>
<main>
<div class=container>
<div class="row odin-main">
<nav class="col-lg-2 odin-sidebar-border navbar-light">
<div class="sticky-top odin-below-navbar py-3">
<ul class="nav nav-pills d-flex flex-column">
<li class=nav-item>
<a class="nav-link active" href=index.html aria-current=page>Getting Started</a>
</li>
<li class=nav-item>
<a class=nav-link href=../overview/index.html>Overview</a>
</li>
<li class=nav-item>
<a class=nav-link href=../faq/index.html>FAQ</a>
</li>
<li class=nav-item>
<a class=nav-link href=../packages/index.html>Packages</a>
</li>
<li class=nav-item>
<a class=nav-link href=../nightly/index.html>Nightly Builds</a>
</li>
<li class=nav-item>
<a class=nav-link href=../spec/index.html>Specification</a>
</li>
</ul>
</div>
</nav>
<article class="col-lg-8 p-4">
<header>
<nav aria-label=breadcrumb>
<ol class=breadcrumb>
<li class=breadcrumb-item><a href=../../docs>Docs</a></li>
<li class=breadcrumb-item><a href=http://odin-lang.org/install>Install</a></li>
</ol>
</nav>
<h1>Getting Started</h1>
</header>
<div class=odin-article>
<p>Odin is dead-simple to get started with!</p>
<p><a href=https://github.com/odin-lang/Odin/releases class="btn btn-outline-primary">Latest Release</a>
<a href=../nightly class="btn btn-outline-primary">Latest Nightly Builds</a></p>
<h2 id=clone-or-download-odin-binaries>Clone or download Odin binaries <a class=text-decoration-none href=#clone-or-download-odin-binaries>#</a></h2>
<p>You can either:</p>
<p>Clone the repository <code>git clone https://github.com/odin-lang/Odin</code> (recommended).</p>
<p>Or download the latest binaries and add them to your path:</p>
<p>Download <a href=https://github.com/odin-lang/Odin/releases/latest>the latest release</a>.</p>
<p>Download <a href=../nightly/index.html>the latest nightly build</a>.</p>
<p><strong>Note</strong>: Cloning the repository is recommended in order to make <a href=#updating-the-compiler>updating</a> easier.</p>
<h3 id=support>Support <a class=text-decoration-none href=#support>#</a></h3>
<p>Odin supports x86-64/AMD64 on Windows, Linux and macOS, and ARM64 on macOS. Odin also relies on LLVM (for code generation) and an external linker.</p>
<h3 id=requirements>Requirements <a class=text-decoration-none href=#requirements>#</a></h3>
<p>The following platform-specific steps are necessary:</p>
<ul>
<li>
<p>Windows</p>
<ul>
<li>Have Visual Studio installed (VS2019-2022 is recommend, VS2017 will likely work, for the linker)</li>
<li>Open a valid command prompt:
<ul>
<li><strong>Basic:</strong> run the <code>x64 Native Tools Command Prompt for VS2017</code> shortcut bundled with VS 2017, or</li>
<li><strong>Advanced:</strong> run <code>vcvarsall.bat x64</code> from a blank <code>cmd</code> session</li>
</ul>
</li>
</ul>
</li>
<li>
<p>MacOS</p>
<ul>
<li>Install the latest XCode (from the App Store or the <a href=https://developer.apple.com/xcode/>Xcode website</a>)</li>
<li>Install XCode command-line tools <code>xcode-select --install</code></li>
<li>Install <a href=https://brew.sh/>Homebrew</a></li>
<li>Install LLVM through Homebrew with: <code>brew install llvm@11</code></li>
<li>Make sure the LLVM binaries and the linker are added to your <code>$PATH</code> environmental variable (see <code>brew info llvm@11</code>)</li>
</ul>
</li>
<li>
<p>GNU/Linux and other *Nix</p>
<ul>
<li>For Linux: clang and llvm (version 11.1, 12 or 13; using your distro&rsquo;s package manager)</li>
<li>For FreeBSD: <code>pkg install bash git llvm14</code></li>
<li>Make sure the LLVM binaries and the linker are added to your <code>$PATH</code> environmental variable</li>
</ul>
</li>
</ul>
<h2 id=building-odin>Building Odin <a class=text-decoration-none href=#building-odin>#</a></h2>
<p>Now, it&rsquo;s time to build Odin and get started!</p>
<h4 id=for-windows>For Windows <a class=text-decoration-none href=#for-windows>#</a></h4>
<p>There&rsquo;s a couple prerequisites here. First, make sure you have Visual Studio installed; you have to compile Odin from source, and Odin also requires <code>link.exe</code> from VS anyway. The necessary LLVM components for Windows are included in the Odin repository.</p>
<p>Now, it&rsquo;s time to build Odin and get started! Open the X64 Visual Studio command prompt (<a href=https://docs.microsoft.com/en-us/dotnet/framework/tools/developer-command-prompt-for-vs>if you don&rsquo;t typically use it, here&rsquo;s how to find it</a>) and navigate to the directory where you downloaded Odin. Run the <code>build.bat</code> file, and you should have a successfully built Odin compiler!</p>
<p>To use Odin <code>link.exe</code> is required to be in the PATH of the callee as mentioned, this can either be achieved but calling Odin from the X64 Visual Studio command prompt or by calling the vcvarsall.bat (with x64 as an argument) script either in your shell or in your build script.</p>
<h4 id=for-macos>For MacOS <a class=text-decoration-none href=#for-macos>#</a></h4>
<p>Make sure all requirements for MacOS are installed, after installing LLVM through Homebrew make sure to add it to the PATH:</p>
<ul>
<li>run <code>echo 'export PATH="/usr/local/opt/llvm/bin:$PATH"' >> ~/.zshrc_profile</code> to add LLVM to your PATH.</li>
</ul>
<p>Then run <code>source ~/.bash_profile</code> or <code>source ~/.zshrc</code> to update your PATH variable in the current terminal session depending on your shell.</p>
<p>On newer versions of macOS, some headers are not installed by default. Open <code>macOS_SDK_headers_for_macOS_*.pkg</code> in <code>/Library/Developer/CommandLineTools/Packages/</code>.</p>
<p>Now navigate to the Odin directory in your terminal, use <code>make</code>, and you should have a newly-built, fresh Odin compiler!</p>
<p>Now you can export the odin folder to the PATH</p>
<h4 id=for-linux-and-other-nix>For Linux and other *Nix <a class=text-decoration-none href=#for-linux-and-other-nix>#</a></h4>
<p>For Linux, make sure you have <code>llvm</code> and <code>clang</code> installed through your package managers.</p>
<p>For FreeBSD make sure you have <code>bash</code>, <code>git</code> and the latest version of LLVM (the base <code>llvm</code> package is most of the times outdated).</p>
<p>Now navigate to the Odin directory in your terminal, use <code>make</code>, and you should have a newly-built, fresh Odin compiler!</p>
<p><strong>Notes for Linux:</strong> The compiler currently relies on the <code>core</code> and <code>shared</code> library collection being relative to the compiler executable. Installing the compiler in the usual sense (to <code>/usr/local/bin</code> or similar) is therefore not as straight forward as you need to make sure the mentioned libraries are available. As a result, it is recommended to simply explicitly invoke the compiler with <code>/path/to/odin</code> in your preferred build system, or add <code>/path/to/odin</code> to <code>$PATH</code>.</p>
<h3 id=updating-the-compiler>Updating the compiler <a class=text-decoration-none href=#updating-the-compiler>#</a></h3>
<p>For a compiler that&rsquo;s in-development like Odin, things move fast. Make sure you keep your compiler up-to-date by running <code>git pull</code> and then rebuilding every now and then. (or, if you use releases, redownload and rebuild)</p>
<h2 id=what-next>What Next? <a class=text-decoration-none href=#what-next>#</a></h2>
<p>Why not check out the <a href=../overview/index.html>Odin Overview</a> for more information on the Odin Programming Language!</p>
</div>
</article>
<div class="col-lg-2 odin-toc-border navbar-light">
<div class="sticky-top odin-below-navbar py-3">
<nav id=TableOfContents>
<ul>
<li><a href=#clone-or-download-odin-binaries>Clone or download Odin binaries</a>
<ul>
<li><a href=#support>Support</a></li>
<li><a href=#requirements>Requirements</a></li>
</ul>
</li>
<li><a href=#building-odin>Building Odin</a>
<ul>
<li></li>
<li><a href=#updating-the-compiler>Updating the compiler</a></li>
</ul>
</li>
<li><a href=#what-next>What Next?</a></li>
</ul>
</nav>
</div>
</div>
</div>
</div>
</main>
<footer class=odin-footer>
<div class="container pb-5 pt-5">
<div class="row g-4">
<div class=col>
<a class=navbar-brand href=../../index.html>
<img class=mb-3 src=../../logo.svg height=30 alt=Odin></a>
<p>
The Data-Oriented Language for Sane Software Development.
</p>
</div>
<nav class=col-md-auto>
<h4 class=fw-normal>Resources</h4>
<ul class=list-unstyled>
<li><a href=../../docs class=link-light>Docs</a></li>
<li><a href=https://pkg.odin-lang.org/ class=link-light>Packages</a></li>
<li><a href=../../news class=link-light>News</a></li>
</ul>
</nav>
<nav class=col-md-auto>
<h4 class=fw-normal>Community</h4>
<ul class=list-unstyled>
<li><a href=https://github.com/odin-lang/Odin target=_blank class=link-light>GitHub</a></li>
<li><a href=https://discord.com/invite/sVBPHEv target=_blank class=link-light>Discord</a></li>
<li><a href=https://www.twitch.tv/ginger_bill target=_blank class=link-light>Twitch</a></li>
<li><a href=https://www.youtube.com/channel/UCUSck1dOH7VKmG4lRW7tZXg target=_blank class=link-light>YouTube</a></li>
<li><a href=../../showcase target=_blank class=link-light>Showcase</a></li>
</ul>
</nav>
<nav class=col-md-auto>
<h4 class=fw-normal>Contribute</h4>
<ul class=list-unstyled>
<li><a href=https://github.com/odin-lang/Odin/issues target=_blank class=link-light>Issues</a></li>
<li><a href=https://www.patreon.com/gingerbill target=_blank class=link-light>Donate</a></li>
</ul>
</nav>
</div>
<div class="mt-4 text-muted">© 2016–2022 Ginger Bill</div>
</div>
</footer>
<script src=../../lib/bootstrap/js/bootstrap.min.js></script>
<script src=../../js/script.js></script>
</body>
</html>