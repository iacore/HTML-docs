<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title>Values &ndash; Wren</title>
<script type="application/javascript" src="prism.js" data-manual></script>
<script type="application/javascript" src="codejar.js"></script>
<script type="application/javascript" src="wren.js"></script>
<link rel="stylesheet" type="text/css" href="prism.css" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic,700italic|Source+Code+Pro:400|Lato:400|Sanchez:400italic,400' rel='stylesheet' type='text/css'>
<!-- Tell mobile browsers we're optimized for them and they don't need to crop
     the viewport. -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
</head>
<body id="top">
<header>
  <div class="page">
    <div class="main-column">
      <h1><a href="index.html">wren</a></h1>
      <h2>a classy little scripting language</h2>
    </div>
  </div>
</header>
<div class="page">
  <nav class="big">
    <a href="index.html"><img src="wren.svg" class="logo"></a>
    <ul>
      <li><a href="getting-started.html">Getting Started</a></li>
      <li><a href="contributing.html">Contributing</a></li>
      <li><a href="blog">Blog</a></li>
      <li><a href="try">Try it!</a></li>
    </ul>
    <section>
      <h2>guides</h2>
      <ul>
        <li><a href="syntax.html">Syntax</a></li>
        <li><a href="values.html">Values</a></li>
        <li><a href="lists.html">Lists</a></li>
        <li><a href="maps.html">Maps</a></li>
        <li><a href="method-calls.html">Method Calls</a></li>
        <li><a href="control-flow.html">Control Flow</a></li>
        <li><a href="variables.html">Variables</a></li>
        <li><a href="classes.html">Classes</a></li>
        <li><a href="functions.html">Functions</a></li>
        <li><a href="concurrency.html">Concurrency</a></li>
        <li><a href="error-handling.html">Error Handling</a></li>
        <li><a href="modularity.html">Modularity</a></li>
      </ul>
    </section>
    <section>
      <h2>API docs</h2>
      <ul>
        <li><a href="modules">Modules</a></li>
      </ul>
    </section>
    <section>
      <h2>reference</h2>
      <ul>
        <li><a href="cli">Wren CLI</a></li>
        <li><a href="embedding">Embedding</a></li>
        <li><a href="performance.html">Performance</a></li>
        <li><a href="qa.html">Q &amp; A</a></li>
      </ul>
    </section>
  </nav>
  <nav class="small">
    <table>
      <tr>
        <div><a href="getting-started.html">Getting Started</a></div>
        <div><a href="contributing.html">Contributing</a></div>
        <div><a href="blog">Blog</a></div>
        <div><a href="try">Try it!</a></div>
      </tr>
      <tr>
        <td colspan="2"><h2>guides</h2></td>
        <td><h2>reference</h2></td>
      </tr>
      <tr>
        <td>
          <ul>
            <li><a href="syntax.html">Syntax</a></li>
            <li><a href="values.html">Values</a></li>
            <li><a href="lists.html">Lists</a></li>
            <li><a href="maps.html">Maps</a></li>
            <li><a href="method-calls.html">Method Calls</a></li>
            <li><a href="control-flow.html">Control Flow</a></li>
          </ul>
        </td>
        <td>
          <ul>
            <li><a href="variables.html">Variables</a></li>
            <li><a href="classes.html">Classes</a></li>
            <li><a href="functions.html">Functions</a></li>
            <li><a href="concurrency.html">Concurrency</a></li>
            <li><a href="error-handling.html">Error Handling</a></li>
            <li><a href="modularity.html">Modularity</a></li>
          </ul>
        </td>
        <td>
          <ul>
            <li><a href="modules">API/Modules</a></li>
            <li><a href="embedding">Embedding</a></li>
            <li><a href="performance.html">Performance</a></li>
            <li><a href="qa.html">Q &amp; A</a></li>
          </ul>
        </td>
      </tr>
    </table>
  </nav>
  <main>
    <h2>Values</h2>
    <p>Values are the built-in atomic object types that all other objects are composed
of. They can be created through <em>literals</em>, expressions that evaluate to a
value. All values are <em>immutable</em>&mdash;once created, they do not change. The
number <code>3</code> is always the number <code>3</code>. The string <code>"frozen"</code> can never have its
character array modified in place.</p>
<h2>Booleans <a href="#booleans" name="booleans" class="header-anchor">#</a></h2>
<p>A boolean value represents truth or falsehood. There are two boolean literals,
<code>true</code> and <code>false</code>. Their class is <a href="modules/core/bool.html">Bool</a>.</p>
<h2>Numbers <a href="#numbers" name="numbers" class="header-anchor">#</a></h2>
<p>Like other scripting languages, Wren has a single numeric type:
double-precision floating point. Number literals look like you expect coming
from other languages:</p>
<pre class="snippet">
0
1234
-5678
3.14159
1.0
-12.34
0.0314159e02
0.0314159e+02
314.159e-02
0xcaffe2
</pre>

<p>Numbers are instances of the <a href="modules/core/num.html">Num</a> class.</p>
<h2>Strings <a href="#strings" name="strings" class="header-anchor">#</a></h2>
<p>A string is an array of bytes. Typically, they store characters encoded in
UTF-8, but you can put any byte values in there, even zero or invalid UTF-8
sequences. (You might have some trouble <em>printing</em> the latter to your terminal,
though.)</p>
<p>String literals are surrounded in double quotes:</p>
<pre class="snippet">
"hi there"
</pre>

<p>They can also span multiple lines. When they do, the newline character within
the string will always be <code>\n</code> (<code>\r\n</code> is normalized to <code>\n</code>). </p>
<pre class="snippet">
"hi
there,
again"
</pre>

<h3>Escaping <a href="#escaping" name="escaping" class="header-anchor">#</a></h3>
<p>A handful of escape characters are supported:</p>
<pre class="snippet">
"\0" // The NUL byte: 0.
"\"" // A double quote character.
"\\" // A backslash.
"\%" // A percent sign.
"\a" // Alarm beep. (Who uses this?)
"\b" // Backspace.
"\e" // ESC character.
"\f" // Formfeed.
"\n" // Newline.
"\r" // Carriage return.
"\t" // Tab.
"\v" // Vertical tab.


"\x48"        // Unencoded byte     (2 hex digits)
"\u0041"      // Unicode code point (4 hex digits)
"\U0001F64A"  // Unicode code point (8 hex digits)
</pre>

<p>A <code>\x</code> followed by two hex digits specifies a single unencoded byte:</p>
<pre class="snippet">
System.print("\x48\x69\x2e") //> Hi.
</pre>

<p>A <code>\u</code> followed by four hex digits can be used to specify a Unicode code point:</p>
<pre class="snippet">
System.print("\u0041\u0b83\u00DE") //> AஃÞ
</pre>

<p>A capital <code>\U</code> followed by <em>eight</em> hex digits allows Unicode code points outside
of the basic multilingual plane, like all-important emoji:</p>
<pre class="snippet">
System.print("\U0001F64A\U0001F680") //> 🙊🚀
</pre>

<p>Strings are instances of class <a href="modules/core/string.html">String</a>.</p>
<h3>Interpolation <a href="#interpolation" name="interpolation" class="header-anchor">#</a></h3>
<p>String literals also allow <em>interpolation</em>. If you have a percent sign (<code>%</code>)
followed by a parenthesized expression, the expression is evaluated. The
resulting object&rsquo;s <code>toString</code> method is called and the result is inserted in the
string:</p>
<pre class="snippet">
System.print("Math %(3 + 4 * 5) is fun!") //> Math 23 is fun!
</pre>

<p>Arbitrarily complex expressions are allowed inside the parentheses:</p>
<pre class="snippet">
System.print("wow %((1..3).map {|n| n * n}.join())") //> wow 149
</pre>

<p>An interpolated expression can even contain a string literal which in turn has
its own nested interpolations, but doing that gets unreadable pretty quickly.</p>
<h3>Raw strings <a href="#raw-strings" name="raw-strings" class="header-anchor">#</a></h3>
<p>A string literal can also be created using triple quotes <code>"""</code> which is
parsed as a raw string. A raw string is no different
from any other string, it&rsquo;s just parsed in a different way.</p>
<p><strong>Raw strings do not process escapes and do not apply any interpolation</strong>.</p>
<pre class="snippet">
"""hi there"""
</pre>

<p>When a raw string spans multiple lines and a triple quote is on it&rsquo;s own line,
any whitespace on that line will be ignored. This means the opening and closing
lines are not counted as part of the string when the triple quotes are separate lines,
as long as they only contain whitespace (spaces + tabs).</p>
<pre class="snippet">
  """
    Hello world
  """
</pre>

<p>The resulting value in the string above has no newlines or trailing whitespace. 
Note the spaces in front of the Hello are preserved. </p>
<pre class="snippet">
    Hello world
</pre>

<p>A raw string will be parsed exactly as is in the file, unmodified.
This means it can contain quotes, invalid syntax, other data formats 
and so on without being modified by Wren.</p>
<pre class="snippet">
"""
  {
    "hello": "wren",
    "from" : "json"
  }
"""
</pre>

<p>One more example, embedding wren code inside a string safely.</p>
<pre class="snippet">
"""
A markdown string with embedded wren code example.

    class Example {
      construct code() {
        //
      }
    }
"""
</pre>

<h2>Ranges <a href="#ranges" name="ranges" class="header-anchor">#</a></h2>
<p>A range is a little object that represents a consecutive range of numbers. They
don&rsquo;t have their own dedicated literal syntax. Instead, the number class
implements the <code>..</code> and <code>...</code> <a href="method-calls.html">operators</a> to create them:</p>
<pre class="snippet">
3..8
</pre>

<p>This creates a range from three to eight, including eight itself. If you want a
half-inclusive range, use <code>...</code>:</p>
<pre class="snippet">
4...6
</pre>

<p>This creates a range from four to six <em>not</em> including six itself. Ranges are
commonly used for <a href="control-flow.html">iterating</a> over a
sequences of numbers, but are useful in other places too. You can pass them to
a <a href="lists.html">list</a>&lsquo;s subscript operator to return a subset of the list, for
example, or on a String, the substring in that range:</p>
<pre class="snippet">
var list = ["a", "b", "c", "d", "e"]
var slice = list[1..3]
System.print(slice) //> [b, c, d]

var string = "hello wren"
var wren = string[-4..-1]
System.print(wren) //> wren
</pre>

<p>Their class is <a href="modules/core/range.html">Range</a>.</p>
<h2>Null <a href="#null" name="null" class="header-anchor">#</a></h2>
<p>Wren has a special value <code>null</code>, which is the only instance of the class
<a href="modules/core/null.html">Null</a>. (Note the difference in case.) It functions a bit like <code>void</code> in some
languages: it indicates the absence of a value. If you call a method that
doesn&rsquo;t return anything and get its returned value, you get <code>null</code> back.</p>
<p><br><hr>
<a class="right" href="lists.html">Lists &rarr;</a>
<a href="syntax.html">&larr; Syntax</a></p>
  </main>
</div>
<footer>
  <div class="page">
    <div class="main-column">
    <p>Wren lives
      <a href="https://github.com/wren-lang/wren">on GitHub</a>
      &mdash; Made with &#x2764; by
      <a href="http://journal.stuffwithstuff.com/">Bob Nystrom</a> and
      <a href="https://github.com/wren-lang/wren/blob/main/AUTHORS">friends</a>.
    </p>
    <div class="main-column">
  </div>
</footer>
</body>
</html>
