// Generated by CoffeeScript 1.9.2
(function() {
  var Highlighter, LuaHighlighter, MoonHighlighter, builtins, escape_html, is_alpha, unescape_html, wrap_word,
    slice = [].slice,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  is_alpha = function(str) {
    return str.match(/^[a-zA-Z_]+$/);
  };

  wrap_word = function(str) {
    if (is_alpha(str)) {
      return "\\b" + str + "\\b";
    } else {
      return str;
    }
  };

  unescape_html = function(text) {
    var node;
    node = document.createElement("div");
    node.innerHTML = text;
    if (node.childNodes.length > 0) {
      return node.childNodes[0].nodeValue;
    } else {
      return "";
    }
  };

  escape_html = function(text) {
    return text.replace(/</g, "&lt;").replace(/>/g, "&gt;");
  };

  Highlighter = (function() {
    Highlighter.prototype.attr_name = "class";

    function Highlighter() {
      var comp, i, j, len, literal_matches, matches, name, p, pattern, pattern_matches, patterns, ref;
      this.match_name_table = [];
      literal_matches = [];
      pattern_matches = [];
      ref = this.matches;
      for (name in ref) {
        patterns = ref[name];
        if (!(patterns instanceof Array)) {
          patterns = [patterns];
        }
        for (j = 0, len = patterns.length; j < len; j++) {
          p = patterns[j];
          if (p instanceof RegExp) {
            pattern_matches.push([name, p.source]);
          } else {
            literal_matches.push([name, this.escape(p)]);
          }
        }
      }
      comp = function(a, b) {
        return b[1].length - a[1].length;
      };
      literal_matches.sort(comp);
      pattern_matches.sort(comp);
      matches = literal_matches.concat(pattern_matches);
      matches = (function() {
        var k, ref1, ref2, results;
        results = [];
        for (i = k = 0, ref1 = matches.length; 0 <= ref1 ? k < ref1 : k > ref1; i = 0 <= ref1 ? ++k : --k) {
          ref2 = matches[i], name = ref2[0], pattern = ref2[1];
          this.match_name_table.push(name);
          results.push("(" + wrap_word(pattern) + ")");
        }
        return results;
      }).call(this);
      this.patt = matches.join("|");
      this.r = new RegExp(this.patt, "g");
      this.replace_all();
    }

    Highlighter.prototype.replace_all = function() {
      var cls_name, j, len, node, nodes, results;
      cls_name = "." + this.name + "-code";
      nodes = document.querySelectorAll(cls_name);
      results = [];
      for (j = 0, len = nodes.length; j < len; j++) {
        node = nodes[j];
        results.push(node.innerHTML = this.format_text(unescape_html(node.innerHTML)));
      }
      return results;
    };

    Highlighter.prototype.format_text = function(text) {
      return text.replace(this.r, (function(_this) {
        return function() {
          var i, match, name, params;
          match = arguments[0], params = 2 <= arguments.length ? slice.call(arguments, 1) : [];
          i = 0;
          while (!params[i] && i < params.length) {
            i++;
          }
          name = _this.match_name_table[i];
          return _this.style(match, _this.get_style(name, match));
        };
      })(this));
    };

    Highlighter.prototype.get_style = function(name, value) {
      var fn, ref;
      fn = ((ref = this.theme) != null ? ref[name] : void 0) || ("l_" + name);
      if (typeof fn === "function") {
        return fn(value);
      } else {
        return fn;
      }
    };

    Highlighter.prototype.style = function(text, style) {
      text = escape_html(text);
      return "<span " + this.attr_name + "=\"" + style + "\">" + text + "</span>";
    };

    Highlighter.prototype.escape = function(text) {
      return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    };

    return Highlighter;

  })();

  builtins = ["table.insert", "assert", "print", "ipairs", "pairs", "require", "module", "package.seeall"];

  LuaHighlighter = (function(superClass) {
    extend(LuaHighlighter, superClass);

    function LuaHighlighter() {
      return LuaHighlighter.__super__.constructor.apply(this, arguments);
    }

    LuaHighlighter.prototype.name = "lua";

    LuaHighlighter.prototype.matches = {
      fn_symbol: ["function"],
      keyword: ["for", "end", "local", "if", "then", "return", "do", "and", "or", "else", "not", "while", "elseif", "break"],
      special: ["nil", "true", "false"],
      symbol: ['=', '.', '{', '}', ':'],
      builtins: builtins,
      atom: /[_A-Za-z][_A-Za-z0-9]*/,
      number: /-?\d+/,
      string: [/"[^"]*"/, /\[\[.*?]]/],
      comment: /--[^\n]*(?:\n|$)/
    };

    return LuaHighlighter;

  })(Highlighter);

  MoonHighlighter = (function(superClass) {
    extend(MoonHighlighter, superClass);

    function MoonHighlighter() {
      return MoonHighlighter.__super__.constructor.apply(this, arguments);
    }

    MoonHighlighter.prototype.name = "moon";

    MoonHighlighter.prototype.matches = {
      keyword: ["class", "extends", "if", "then", "super", "do", "with", "import", "export", "while", "elseif", "return", "for", "in", "from", "when", "using", "else", "and", "or", "not", "switch", "break", "continue"],
      special: ["nil", "true", "false"],
      builtins: builtins,
      self: ["self"],
      symbol: ['!', '\\', '=', '+=', '-=', '...', '*', '>', '<', '#'],
      bold_symbol: [':', '.'],
      fn_symbol: ['->', '=>', '}', '{', '[', ']'],
      self_var: /@[a-zA-Z_][a-zA-Z_0-9]*/,
      table_key: /[_A-Za-z][a-zA-Z_0-9]*(?=:)/,
      proper: /[A-Z][a-zA-Z_0-9]*/,
      atom: /[_A-Za-z]\w*/,
      number: /-?\d+/,
      string: [/"[^"]*"/, /\[\[.*?]]/],
      comment: /--[^\n]*(?:\n|$)/
    };

    return MoonHighlighter;

  })(Highlighter);

  window.LuaHighlighter = LuaHighlighter;

  window.MoonHighlighter = MoonHighlighter;

}).call(this);
