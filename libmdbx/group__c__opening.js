var group__c__opening =
[
    [ "MDBX_env_flags_t", "group__c__opening.html#ga316b392e2fa473ddf42874a86bfb60dd", null ],
    [ "MDBX_env_flags_t", "group__c__opening.html#ga9138119a904355d245777c4119534061", [
      [ "MDBX_ENV_DEFAULTS", "group__c__opening.html#gga9138119a904355d245777c4119534061a261dfd8a866bf0fcf4d3214f30e18d7b", null ],
      [ "MDBX_VALIDATION", "group__c__opening.html#gga9138119a904355d245777c4119534061af6ccfacef80badbb16454be9548372f3", null ],
      [ "MDBX_NOSUBDIR", "group__c__opening.html#gga9138119a904355d245777c4119534061ad5c4ae6a8fefe9b05ce17ec3e3bbda4f", null ],
      [ "MDBX_RDONLY", "group__c__opening.html#gga9138119a904355d245777c4119534061adc513be607ba103d642f49afc2d50e50", null ],
      [ "MDBX_EXCLUSIVE", "group__c__opening.html#gga9138119a904355d245777c4119534061aa516c74e6fed22c9812bb909c8c459ed", null ],
      [ "MDBX_ACCEDE", "group__c__opening.html#gga9138119a904355d245777c4119534061ac699e93388dfc6a01a8d652ad0443a24", null ],
      [ "MDBX_WRITEMAP", "group__c__opening.html#gga9138119a904355d245777c4119534061a9498805abc6c8a3f8345a5c73631cca0", null ],
      [ "MDBX_NOTLS", "group__c__opening.html#gga9138119a904355d245777c4119534061a79e1de212a2f3a1018c2da2c4b511cb8", null ],
      [ "MDBX_NORDAHEAD", "group__c__opening.html#gga9138119a904355d245777c4119534061a16a07f878f8053cc79990063ca9510e7", null ],
      [ "MDBX_NOMEMINIT", "group__c__opening.html#gga9138119a904355d245777c4119534061aa7a40c9090db0a0c4df61ed0d1d76360", null ],
      [ "MDBX_COALESCE", "group__c__opening.html#gga9138119a904355d245777c4119534061a30da8fd3a91328e5719864de3eadeabf", null ],
      [ "MDBX_LIFORECLAIM", "group__c__opening.html#gga9138119a904355d245777c4119534061a37585486d4e99bcb599cc15b408137b0", null ],
      [ "MDBX_PAGEPERTURB", "group__c__opening.html#gga9138119a904355d245777c4119534061a923f8699a2442381a0f1b843c011d74f", null ],
      [ "MDBX_SYNC_DURABLE", "group__c__opening.html#gga9138119a904355d245777c4119534061a8079fe157295e6743580c589af5a063c", null ],
      [ "MDBX_NOMETASYNC", "group__c__opening.html#gga9138119a904355d245777c4119534061a323dae93247c4977621be38614d77b38", null ],
      [ "MDBX_SAFE_NOSYNC", "group__c__opening.html#gga9138119a904355d245777c4119534061a586d75ff2fd68aed647a60ea7984ed85", null ],
      [ "MDBX_MAPASYNC", "group__c__opening.html#gga9138119a904355d245777c4119534061aa9fc9f8b1a9a9a8ec58407abf8befbc1", null ],
      [ "MDBX_UTTERLY_NOSYNC", "group__c__opening.html#gga9138119a904355d245777c4119534061aec1ed5ccbf0626391088f787ffc4962c", null ]
    ] ],
    [ "mdbx_env_close", "group__c__opening.html#ga3945614a024b9f9eb48e4ffa205b68be", null ],
    [ "mdbx_env_close_ex", "group__c__opening.html#gaa0c00950c8ad451f8674d127d53823fa", null ],
    [ "mdbx_env_create", "group__c__opening.html#ga109eb833997698fbb77912716596fe8e", null ],
    [ "mdbx_env_open", "group__c__opening.html#gabb7dd3b10dd31639ba252df545e11768", null ]
];