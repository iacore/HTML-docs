var group__c__api_struct_m_d_b_x__txn__info =
[
    [ "txn_id", "group__c__api.html#a8fa3b9f102114b6a412335cbe2c0802e", null ],
    [ "txn_reader_lag", "group__c__api.html#a383fe4a97f9108000d5c69590738f8ad", null ],
    [ "txn_space_dirty", "group__c__api.html#a0a13745cd97c277cd810f3bbf60c0d9f", null ],
    [ "txn_space_leftover", "group__c__api.html#a461b926cfd0e656bebac9bab5997d6b1", null ],
    [ "txn_space_limit_hard", "group__c__api.html#aa93c141047f682507850e6f7a08f5557", null ],
    [ "txn_space_limit_soft", "group__c__api.html#a8aa8674b1b08e6f956d893c02c1b4514", null ],
    [ "txn_space_retired", "group__c__api.html#ac8a5a3e9a6f4bfba24ca9dd8d957030f", null ],
    [ "txn_space_used", "group__c__api.html#a48515214b62493d820cbafda219bcc48", null ]
];