var group__cxx__api_classmdbx_1_1env__managed =
[
    [ "create_parameters", "structmdbx_1_1env__managed_1_1create__parameters.html", "structmdbx_1_1env__managed_1_1create__parameters" ],
    [ "env_managed", "group__cxx__api.html#abfe55de78e3a6432df6908a1e3fba6e7", null ],
    [ "env_managed", "group__cxx__api.html#a5c35257f454a501eb3c9ecf83d7cfd9b", null ],
    [ "env_managed", "group__cxx__api.html#a8b32472b6cf32d4a07a172d02d8ad43d", null ],
    [ "env_managed", "group__cxx__api.html#a3400a6cf2c8365a6872e88c21a1d7ad3", null ],
    [ "env_managed", "group__cxx__api.html#aa023ec77d196805e5b43f1b454a3321e", null ],
    [ "env_managed", "group__cxx__api.html#a02d37e1b1cf8793ba0034f873cf803b1", null ],
    [ "env_managed", "group__cxx__api.html#ac64a6ed623f9af431888bc7c4054e3c5", null ],
    [ "env_managed", "group__cxx__api.html#ac6efdf14691260d6c4acff0458503675", null ],
    [ "env_managed", "group__cxx__api.html#aaead5d44b3d5f659140747ca97026643", null ],
    [ "env_managed", "group__cxx__api.html#aa9034a5d9bcdbdf76981ffa890ff494d", null ],
    [ "env_managed", "group__cxx__api.html#a11c7da30eb0a1a068e7b294552a52f96", null ],
    [ "env_managed", "group__cxx__api.html#a973c6f89f35de0ed735a7af5e13dca6f", null ],
    [ "env_managed", "group__cxx__api.html#acfff4ad7f49f663d99ddc2b45f60e824", null ],
    [ "~env_managed", "group__cxx__api.html#abb1770c8392e056b6727a5f9d84f649a", null ],
    [ "close", "group__cxx__api.html#ab8b0f132c4b67d1bc5b10e6c6047f2ac", null ],
    [ "operator=", "group__cxx__api.html#a586a1cabb1c093e13a7a1130aaed57bc", null ],
    [ "operator=", "group__cxx__api.html#a2b6e777b566e8d4d47b7e16dd8fc290d", null ]
];