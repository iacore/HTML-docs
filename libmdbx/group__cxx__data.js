var group__cxx__data =
[
    [ "mdbx::allocation_aware_details", "namespacemdbx_1_1allocation__aware__details.html", null ],
    [ "mdbx::MutableByteProducer", "conceptmdbx_1_1_mutable_byte_producer.html", null ],
    [ "mdbx::ImmutableByteProducer", "conceptmdbx_1_1_immutable_byte_producer.html", null ],
    [ "mdbx::SliceTranscoder", "conceptmdbx_1_1_slice_transcoder.html", null ],
    [ "mdbx::slice", "group__cxx__data.html#structmdbx_1_1slice", [
      [ "slice", "group__cxx__api.html#gaa38fb1a5d28f8fca0d1ba493485d4941", null ],
      [ "slice", "group__cxx__api.html#ga4285f0c8171bbe9cbd481c4283b4969c", null ],
      [ "slice", "group__cxx__api.html#ga797eafc950571df38cdcdbb41fd564b0", null ],
      [ "slice", "group__cxx__data.html#ac5be9ef83ee14b6dda2ad25311a7bc5b", null ],
      [ "slice", "group__cxx__api.html#ga970dbdf1a67a57c5e7ef4e5daae84fb4", null ],
      [ "slice", "group__cxx__data.html#a8c4cbc117ba3e270f4557d9325328ae1", null ],
      [ "slice", "group__cxx__api.html#ga6b0f91151d47daac014f3a7af595416d", null ],
      [ "slice", "group__cxx__data.html#a2a12cb600b7be9b0563361dc31067e56", null ],
      [ "slice", "group__cxx__api.html#gab4cb9f5defb50c0bc85a5961c4979391", null ],
      [ "slice", "group__cxx__api.html#gaad2b4b4181c6e71f9e298172e718ebe7", null ],
      [ "slice", "group__cxx__data.html#a8387dd2ad3fbda66bd1c0a4f047398b2", null ],
      [ "slice", "group__cxx__data.html#afc30d112ba9835cd9113f2c64042faf2", null ],
      [ "slice", "group__cxx__data.html#abc2c251b9e3061b4ab9e477b85504fdf", null ],
      [ "as_base58_string", "group__cxx__api.html#ga2e7c4d5d65bd55e421e4b8493c30b954", null ],
      [ "as_base64_string", "group__cxx__api.html#gaa123c2320a66845bd0df03a63986a9b2", null ],
      [ "as_hex_string", "group__cxx__api.html#ga2095f8b3284668474938d1fa722151b2", null ],
      [ "as_string", "group__cxx__data.html#a0c4da728f173240bba21e32473892d49", null ],
      [ "assign", "group__cxx__api.html#ga5993b2fba342e7334a3b1867fc8f9e26", null ],
      [ "assign", "group__cxx__data.html#a1a1aae5cddd5f94e3044a282315031c9", null ],
      [ "assign", "group__cxx__api.html#ga39cd2a1bf72b7eb2f45fc066368be24e", null ],
      [ "assign", "group__cxx__data.html#a59457c925175a84482431990b1ac383a", null ],
      [ "assign", "group__cxx__data.html#a968cd410b85cac671e091cb05f4ca2bf", null ],
      [ "assign", "group__cxx__api.html#gabb454a827ec3cfe8a7fe778bbfed301d", null ],
      [ "assign", "group__cxx__api.html#gaf6b0854d1b1004d154fcb7a3ed111957", null ],
      [ "assign", "group__cxx__api.html#ga5c2a277b73fb6d72597249fe8c826ae1", null ],
      [ "assign", "group__cxx__api.html#ga1cac4c0dba09131704fc13e23e7caee8", null ],
      [ "assign", "group__cxx__api.html#ga2bb10fb6c7885ffdb5668b1472ce4c61", null ],
      [ "at", "group__cxx__api.html#ga8f1b4707a8a469f9990951fde23d30bb", null ],
      [ "base58_decode", "group__cxx__api.html#gae6c365dc41c3b12d6c101b07a6783506", null ],
      [ "base64_decode", "group__cxx__api.html#ga5b476485fbdef0588c2923b51506d3b7", null ],
      [ "byte_ptr", "group__cxx__api.html#gaea20f2081d9e0a1db4b608844b00d29b", null ],
      [ "byte_ptr", "group__cxx__api.html#ga95327f57b18a9cf4de87beba0f83109a", null ],
      [ "char_ptr", "group__cxx__api.html#ga1b8a7881532360caaf1882cc7280fe87", null ],
      [ "char_ptr", "group__cxx__api.html#ga657d2e7181d94fb94459d90f0f2b18b5", null ],
      [ "clear", "group__cxx__api.html#ga48bf7dabdbc3e3c5902cb8cb5dc9eab0", null ],
      [ "data", "group__cxx__api.html#gac7021bc86d1e27c3d98635b732a6b897", null ],
      [ "data", "group__cxx__api.html#gaebc3b5cca084173ab068fef2628bb24b", null ],
      [ "empty", "group__cxx__api.html#ga8a386b381321ee45d2855b5185454b06", null ],
      [ "encode_base58", "group__cxx__api.html#ga03e41dd999e48de247e8764e7b45a304", null ],
      [ "encode_base64", "group__cxx__api.html#ga5a8c5348f8d8d8709db2f8dfaf7cb038", null ],
      [ "encode_hex", "group__cxx__api.html#ga82d9880b81221740c1cdfd54277cb6d1", null ],
      [ "end", "group__cxx__api.html#gab7d8938d16154ea48975d473edd4af22", null ],
      [ "end", "group__cxx__api.html#ga3d16c2c5adb618a88c80cb230f2d3be6", null ],
      [ "end_byte_ptr", "group__cxx__api.html#ga69fd6dff69e118f8ddd199add1640f87", null ],
      [ "end_byte_ptr", "group__cxx__api.html#ga6f63823efada561aa67ad2542c34ad86", null ],
      [ "end_char_ptr", "group__cxx__api.html#gaa2ae44807dd8ccbf41e6020f2d514d35", null ],
      [ "end_char_ptr", "group__cxx__api.html#gac7e2566801a329dbcf07bf8af14e4aff", null ],
      [ "ends_with", "group__cxx__api.html#ga4a43eaf8c80843b46f623bc493222781", null ],
      [ "hash_value", "group__cxx__api.html#gaa9558d7562a418674768e9160491da3f", null ],
      [ "head", "group__cxx__api.html#ga44351506fa137dcc32ca4e02c5eb7cbd", null ],
      [ "hex_decode", "group__cxx__api.html#ga853b801a14663adc1efe2861b2b5576f", null ],
      [ "invalidate", "group__cxx__api.html#gafd1e9c857f06f54e08bfa187bf243fe1", null ],
      [ "is_base58", "group__cxx__api.html#gab9911d80393f36955f286b524d993d2b", null ],
      [ "is_base64", "group__cxx__api.html#ga9cb63cf592957a238c86009f6c031b21", null ],
      [ "is_hex", "group__cxx__api.html#gaf9e9f01aa3760715165cb38311d20cdd", null ],
      [ "is_null", "group__cxx__api.html#gadcdb4e026c0b3688425bdf7107a39622", null ],
      [ "is_printable", "group__cxx__data.html#a39a1e87bfdb7ce82a3aac6b7f36f3e08", null ],
      [ "is_valid", "group__cxx__data.html#a7ed2e15aebfa49cb277984c13a8e8de4", null ],
      [ "length", "group__cxx__api.html#ga1f22e5234dd01576e39e30910c6908e6", null ],
      [ "middle", "group__cxx__api.html#ga18385566c1dbefe195f19292b9ca2c3f", null ],
      [ "operator::std::basic_string", "group__cxx__data.html#a2204f98adf0cafb874f9682ad81bdc4b", null ],
      [ "operator::std::basic_string_view", "group__cxx__data.html#af0e515b7dd2758632d4e633b3337c7e8", null ],
      [ "operator=", "group__cxx__api.html#ga23f41ea724d8878c14f0b407655e46e3", null ],
      [ "operator=", "group__cxx__data.html#a28ecbda9f6ff2540040e865b8a78df18", null ],
      [ "operator=", "group__cxx__data.html#a6f158cb15299c1802c85b84397af67a0", null ],
      [ "operator=", "group__cxx__data.html#abec53a2eebbc9c3ebe8da4200db43063", null ],
      [ "operator=", "group__cxx__api.html#ga5a81234bece30a8a472f7ca0ef4ebc88", null ],
      [ "operator[]", "group__cxx__api.html#ga77b4684b8115062a6eeb332f08a0fecc", null ],
      [ "remove_prefix", "group__cxx__api.html#ga9cd0982cb9fa6eed01a48fc05c8c7ec1", null ],
      [ "remove_suffix", "group__cxx__api.html#ga1c89ebdb77acb3f278b50c680e8b7d07", null ],
      [ "safe_head", "group__cxx__api.html#ga87d2f66875e4ffc47c76f577d32bff5d", null ],
      [ "safe_middle", "group__cxx__api.html#ga87248db88360ce8adfa4c6dd024006e8", null ],
      [ "safe_remove_prefix", "group__cxx__api.html#ga6fbe96f44c47c47bb524a80e2172a2ee", null ],
      [ "safe_remove_suffix", "group__cxx__api.html#ga96f8c368da6326b7c1d7b639e75a14a6", null ],
      [ "safe_tail", "group__cxx__api.html#ga696ca1e1dc1bad744f5d132c49eb8847", null ],
      [ "set_end", "group__cxx__api.html#ga84b994a6bbda792cd670ecac77f066fd", null ],
      [ "set_length", "group__cxx__api.html#gaae93b06c7213a8451033d3054eb14937", null ],
      [ "size", "group__cxx__api.html#ga3ce13eb9d409bc94a33eca195533f4bf", null ],
      [ "starts_with", "group__cxx__api.html#ga91a146d36fac778170958a1cd2ab4437", null ],
      [ "string_view", "group__cxx__data.html#a9b774118cb7850baa706e8b74ece0be5", null ],
      [ "swap", "group__cxx__data.html#a34d35f30b354db041ef0647970d7ac35", null ],
      [ "swap", "group__cxx__api.html#ga33c21b201a228386fbafbde3fedd04b3", null ],
      [ "tail", "group__cxx__api.html#gaa3abf94daa6077ebbf429187c2029086", null ],
      [ "operator!=", "group__cxx__data.html#aaa7e1c7159660896d5573d92dd57c5ca", null ],
      [ "operator<", "group__cxx__data.html#adb7349d8724735c67f5bf06550c8f1fe", null ],
      [ "operator<=", "group__cxx__data.html#a2071047a9332799185ac96798730ed4a", null ],
      [ "operator==", "group__cxx__data.html#aa11b3c7589162ca7be1a433924adc77b", null ],
      [ "operator>", "group__cxx__data.html#a0a038605fe8f60588b31f7872b1411c9", null ],
      [ "operator>=", "group__cxx__data.html#a3876d218971e4e5ea14092c070638d7b", null ]
    ] ],
    [ "mdbx::default_capacity_policy", "group__cxx__data.html#structmdbx_1_1default__capacity__policy", null ],
    [ "mdbx::to_hex", "group__cxx__data.html#structmdbx_1_1to__hex", [
      [ "to_hex", "group__cxx__data.html#ad4154ad986698d838f4692e537ca745f", null ],
      [ "as_buffer", "group__cxx__data.html#a13f72d26f567de820acd25a1bff257a1", null ],
      [ "as_string", "group__cxx__data.html#a77b71c9066450c272f594b621d133826", null ],
      [ "envisage_result_length", "group__cxx__data.html#a11d8ec1356934c67975106d6bdbda5c8", null ],
      [ "is_empty", "group__cxx__data.html#a46c6337581ebf172338d8679f22ce154", null ],
      [ "is_erroneous", "group__cxx__data.html#a8e1ecbe74b80fffb6d294e7fce8fb3ba", null ],
      [ "output", "group__cxx__data.html#a4dcbb76524d5053e3642f686688976f9", null ],
      [ "write_bytes", "group__cxx__data.html#aa3824c315eca31836238cbe12c08888e", null ],
      [ "source", "group__cxx__data.html#a524c73a88b07fc9ea9d5ac4805025792", null ],
      [ "uppercase", "group__cxx__data.html#ab335d3d9892801523e221b974e479485", null ],
      [ "wrap_width", "group__cxx__data.html#a56ebeda48e4a733ca11bcc2f701444c7", null ]
    ] ],
    [ "mdbx::to_base58", "group__cxx__data.html#structmdbx_1_1to__base58", [
      [ "to_base58", "group__cxx__data.html#a9be0cd80de2ddbec8034472812ef9866", null ],
      [ "as_buffer", "group__cxx__data.html#a31da901b492b758114e4d54861751689", null ],
      [ "as_string", "group__cxx__data.html#ac9015eb84e5c785914237e8b4b368a5f", null ],
      [ "envisage_result_length", "group__cxx__data.html#a936a2b79ab6a0778d751b0894cec7900", null ],
      [ "is_empty", "group__cxx__data.html#a83e879dea4a7ee7e1eea3b6e66cb5b1d", null ],
      [ "is_erroneous", "group__cxx__data.html#aace2390f4f882b4e4e339776ea643fa0", null ],
      [ "output", "group__cxx__data.html#aaca4c81aca6e93277a10803f35e8d4f2", null ],
      [ "write_bytes", "group__cxx__data.html#afdd9a40d56b2237e83cb0eaa360aff4b", null ],
      [ "source", "group__cxx__data.html#a877fbccbf9cdc4c7b8a45b0dd80e93db", null ],
      [ "wrap_width", "group__cxx__data.html#ae8af87fab243c5a342bd276f4949ffb7", null ]
    ] ],
    [ "mdbx::to_base64", "group__cxx__data.html#structmdbx_1_1to__base64", [
      [ "to_base64", "group__cxx__data.html#a8a0ecabce2c0866af7e7e38e7ad7ef93", null ],
      [ "as_buffer", "group__cxx__data.html#a6c5649be4d6057d8e99f1e837a3e1f03", null ],
      [ "as_string", "group__cxx__data.html#a9c33fe96a8594fe05cc4cb22b1c5a023", null ],
      [ "envisage_result_length", "group__cxx__data.html#a835e69c5cec8225cade86b0c4c791478", null ],
      [ "is_empty", "group__cxx__data.html#ac97e261ee33acf0659b5d8eb8d99fd7e", null ],
      [ "is_erroneous", "group__cxx__data.html#af870f7c26b3b59019b21496c6c8b5c92", null ],
      [ "output", "group__cxx__data.html#a3fe400a6462c31f6f12579a7e0334014", null ],
      [ "write_bytes", "group__cxx__data.html#af2ea58fe81b53c3c108f09360b2561af", null ],
      [ "source", "group__cxx__data.html#a169245978a989b5741ce5af99029eca3", null ],
      [ "wrap_width", "group__cxx__data.html#aed4cc244c3c7a4f180fddd3b5babad3b", null ]
    ] ],
    [ "mdbx::from_hex", "group__cxx__data.html#structmdbx_1_1from__hex", [
      [ "from_hex", "group__cxx__data.html#a7c6ac407bc1da6a34392749bfd3c2395", null ],
      [ "as_buffer", "group__cxx__data.html#a58cf528fe3e8ac0619a66aa9c436c97d", null ],
      [ "as_string", "group__cxx__data.html#a8fe5aa51938597ffdbdae53583ba8eb1", null ],
      [ "envisage_result_length", "group__cxx__data.html#aecf019ee31e87097ee9e25838f0f19d6", null ],
      [ "is_empty", "group__cxx__data.html#a9781296a8c3fe562d355a9b9456b8df3", null ],
      [ "is_erroneous", "group__cxx__data.html#ae4ee432e448c524c64073bd4e067db55", null ],
      [ "write_bytes", "group__cxx__data.html#acdcb483d3a6209ecec22bc996cea6b59", null ],
      [ "ignore_spaces", "group__cxx__data.html#a5c0f956dd21381b0955c61d6c7266fe6", null ],
      [ "source", "group__cxx__data.html#afe07bbe916d8c17f210c127b62a81071", null ]
    ] ],
    [ "mdbx::from_base58", "group__cxx__data.html#structmdbx_1_1from__base58", [
      [ "from_base58", "group__cxx__data.html#a6afd0045a62b0cf8336ff4bf520aecfc", null ],
      [ "as_buffer", "group__cxx__data.html#a1392707a0236d35db3986bcd5ffa9a94", null ],
      [ "as_string", "group__cxx__data.html#a3e7092a8cef41443be83bc46b27b0e13", null ],
      [ "envisage_result_length", "group__cxx__data.html#ac394d3fd780c6c0088983bcd9ebb193b", null ],
      [ "is_empty", "group__cxx__data.html#a869fef3fd9ec3c9e9b0ad4ebb5035d44", null ],
      [ "is_erroneous", "group__cxx__data.html#a17023447aad4d044061970c3a719c6c6", null ],
      [ "write_bytes", "group__cxx__data.html#a5ea2655ae544feeb3f9e675b05f30745", null ],
      [ "ignore_spaces", "group__cxx__data.html#a321ddc91e8f728c8afb5eebd3af07489", null ],
      [ "source", "group__cxx__data.html#a515bbd8e1acfca246bbb3e7972604868", null ]
    ] ],
    [ "mdbx::from_base64", "group__cxx__data.html#structmdbx_1_1from__base64", [
      [ "from_base64", "group__cxx__data.html#abd255392596df765deba4c9835adce93", null ],
      [ "as_buffer", "group__cxx__data.html#aaf62c0fafa031e3e0c3e437353723b00", null ],
      [ "as_string", "group__cxx__data.html#a3f50a0fcc60c546fefea2afb1929a67f", null ],
      [ "envisage_result_length", "group__cxx__data.html#ac820289591a2d2114232cb4d4471af98", null ],
      [ "is_empty", "group__cxx__data.html#a5f0d8f6aafbfcaedef90a850509318b3", null ],
      [ "is_erroneous", "group__cxx__data.html#abe27a04f4c3ebe69122953a300233db7", null ],
      [ "write_bytes", "group__cxx__data.html#a3b736ac0a82b59700b2aa9dfbf58560f", null ],
      [ "ignore_spaces", "group__cxx__data.html#abb1c4a6187509f5447f329b06417e348", null ],
      [ "source", "group__cxx__data.html#ac565c8e62386c2732d53de7c5d8b5970", null ]
    ] ],
    [ "mdbx::buffer< ALLOCATOR, CAPACITY_POLICY >", "group__cxx__api.html#classmdbx_1_1buffer", [
      [ "allocator_traits", "group__cxx__api.html#a247c67348fba7bc0f3c608189e9f218b", null ],
      [ "allocator_type", "group__cxx__api.html#a3fb4f268538e240e55fa4a5919edcc63", null ],
      [ "reservation_policy", "group__cxx__api.html#a8f2e8cfc91b516243ebdd4b87ff28e43", null ],
      [ "buffer", "group__cxx__api.html#ae317e9135e7cf8b34ed398d3d57e9d8c", null ],
      [ "buffer", "group__cxx__api.html#a816fce1f45626de78eb56ac7bcda041c", null ],
      [ "buffer", "group__cxx__api.html#a7747cfc319257a18169c653251cbb58f", null ],
      [ "buffer", "group__cxx__api.html#a569bf6665c77570dd676cb876dafa87f", null ],
      [ "buffer", "group__cxx__api.html#ab6cb60b48e935137de4b90b2732be9cf", null ],
      [ "buffer", "group__cxx__api.html#ae99896e078c1f37cfd1b144b65eb50aa", null ],
      [ "buffer", "group__cxx__api.html#ae33c6863e70ece902c6b506c243efca8", null ],
      [ "buffer", "group__cxx__api.html#aaba3708e2308bc482bc874fa8db5fcf8", null ],
      [ "buffer", "group__cxx__api.html#a26f5015bfd5037297f3e1ba095fdb98e", null ],
      [ "buffer", "group__cxx__api.html#aedd6ccf11dd071f66c4ee3573e767534", null ],
      [ "buffer", "group__cxx__api.html#a4d117202c9a4dab4820e2d7e8bd4a9da", null ],
      [ "buffer", "group__cxx__api.html#a971bb96506ee4096e6e43284647821f0", null ],
      [ "buffer", "group__cxx__api.html#aa7b16d1927f484ff76b5d5b88aa77b99", null ],
      [ "buffer", "group__cxx__api.html#a697c1abacd3831a6774c7e9e84e02d45", null ],
      [ "buffer", "group__cxx__api.html#a6c5af8502c441ac76b8988d826f08835", null ],
      [ "buffer", "group__cxx__api.html#a57bd64c633cb2ecc78f2022aaff1f5c3", null ],
      [ "buffer", "group__cxx__api.html#af0a615f15268041921666b867cf915dc", null ],
      [ "buffer", "group__cxx__api.html#a0169d195a67c24ef586201463a2d742b", null ],
      [ "buffer", "group__cxx__api.html#a419bcdad426d842a29f9bd4b851a6927", null ],
      [ "buffer", "group__cxx__api.html#a7a5217759ed60ec30ee26c80df5f4c09", null ],
      [ "buffer", "group__cxx__api.html#ae2a361c53a27c5ed07d01b222c947f39", null ],
      [ "buffer", "group__cxx__api.html#gaf4e34ced6e58a487af500f71b793c74c", null ],
      [ "add_header", "group__cxx__api.html#a93b770d68a7a6200db82129457f9158e", null ],
      [ "add_header", "group__cxx__api.html#a38cefad1f1f3c860b37faf52f8dc9a3b", null ],
      [ "append", "group__cxx__api.html#ab756f7a314a35d8da03fc546200fef5a", null ],
      [ "append", "group__cxx__api.html#ac3572d5936aeed9d1e27854ac39f85ba", null ],
      [ "append_base58", "group__cxx__api.html#a7dfe03f763b3679b516bcaf2e38a2c8a", null ],
      [ "append_base64", "group__cxx__api.html#a6d526dc061f1b078938aa877fc5a4e88", null ],
      [ "append_decoded_base58", "group__cxx__api.html#a94c38937bf879fed9d7cf543559a197d", null ],
      [ "append_decoded_base64", "group__cxx__api.html#ab8477ed0e6f29f7010b0c2f67b936282", null ],
      [ "append_decoded_hex", "group__cxx__api.html#a2643d0fdb6c755f256469bc6a026575f", null ],
      [ "append_hex", "group__cxx__api.html#aea5436a9e30e6948105cbbd9366e6ea1", null ],
      [ "append_producer", "group__cxx__api.html#af3b19c46d48a2ffab6a66e0acfaa473e", null ],
      [ "append_producer", "group__cxx__api.html#af2efa15e3da1073bf8c15cbbafc2aa69", null ],
      [ "as_string", "group__cxx__api.html#a2873e5958652b9d4436bd1c767da1173", null ],
      [ "assign", "group__cxx__api.html#a890a0cbf7f9dd5e5398115cabaab9034", null ],
      [ "assign", "group__cxx__api.html#aa96c613562dfde5e6260dcd5f1d0f264", null ],
      [ "assign", "group__cxx__api.html#a42844ec13827e1fbe516426aaaa9fa22", null ],
      [ "assign", "group__cxx__api.html#ab26443e009de6f068ba602b7849040e5", null ],
      [ "assign", "group__cxx__api.html#a1ea7dffa78fe95a0a1cdbf7e0dee5371", null ],
      [ "assign", "group__cxx__api.html#a1f471aac40e400953a31795e58ba3d49", null ],
      [ "assign", "group__cxx__api.html#a0141a53e5b1f1fd179ed280d32427cba", null ],
      [ "assign", "group__cxx__api.html#a5fe9b0fd6f77fe754d6292a3f38b9333", null ],
      [ "assign", "group__cxx__api.html#a5aeb04d67779fe9607b9430528e572a1", null ],
      [ "assign_freestanding", "group__cxx__api.html#a287fd7c92ea7037ac9ed1c12ec2a470f", null ],
      [ "assign_reference", "group__cxx__api.html#aa44a82240d7c74cecb4c883e3278935a", null ],
      [ "at", "group__cxx__api.html#ac4310c2455bd5c6261a1f17735ca294d", null ],
      [ "at", "group__cxx__api.html#abe7ba666c0b5b7b3605215703162b049", null ],
      [ "byte_ptr", "group__cxx__api.html#a83184ac4bbfbb3a01de9447a2d220a56", null ],
      [ "byte_ptr", "group__cxx__api.html#a6aa94acde2a89476d72cdb6ccd3e2965", null ],
      [ "capacity", "group__cxx__api.html#a1a9bcf38ddff85266c75f64b3cfab688", null ],
      [ "char_ptr", "group__cxx__api.html#a58841de0881e488d8c5cc19780ee706a", null ],
      [ "char_ptr", "group__cxx__api.html#ad0f0f9ea829940d0737b5d21d9135c79", null ],
      [ "clear", "group__cxx__api.html#acb32eb606812109615b641945d7fc485", null ],
      [ "data", "group__cxx__api.html#ac34c10de8bad4ce1b3e0170a6413e419", null ],
      [ "data", "group__cxx__api.html#a5be747a95998e2e133b79fcc719cc4e6", null ],
      [ "empty", "group__cxx__api.html#a37d362930d89d767acf91bc645c67bc5", null ],
      [ "end", "group__cxx__api.html#ab3844962e1744d92083e87c476255cb7", null ],
      [ "end", "group__cxx__api.html#aab5f0ed96f69479026cd7324604d809e", null ],
      [ "end_byte_ptr", "group__cxx__api.html#a99795b4cbd61f5ce3ebf149ce7902c5c", null ],
      [ "end_byte_ptr", "group__cxx__api.html#ab011d185ea6abb91a6a5097605560783", null ],
      [ "end_char_ptr", "group__cxx__api.html#adc272374dd59e5ce5d6d19fb2ef1b5a2", null ],
      [ "end_char_ptr", "group__cxx__api.html#a75da6fbccaa9e2defc03c9dc9a2b46cb", null ],
      [ "ends_with", "group__cxx__api.html#a73406feaa5c09ab76453cd326411f374", null ],
      [ "get_allocator", "group__cxx__api.html#a2e25f7ed89d769dcc008d9830ca9993e", null ],
      [ "hash_value", "group__cxx__api.html#a13d741351c61287a9c6c56822b1ca6ca", null ],
      [ "head", "group__cxx__api.html#a6e652a0e75b7e7ba22de7b69f6210588", null ],
      [ "headroom", "group__cxx__api.html#adeaffb963f0b072992c7a160f1159b77", null ],
      [ "is_freestanding", "group__cxx__api.html#ac35aa6ef5ab91192eabe5fa3d80b4b61", null ],
      [ "is_null", "group__cxx__api.html#ae71cf52488d3ded238d7e216727ed99c", null ],
      [ "is_reference", "group__cxx__api.html#af609318a60019ad3f1de30f497b4abb7", null ],
      [ "length", "group__cxx__api.html#ab7f6c9ecbce0e6cc73c1f08f6397d5c7", null ],
      [ "make_freestanding", "group__cxx__api.html#aa80450b8539ee20ead1589d6f65daa7a", null ],
      [ "middle", "group__cxx__api.html#a5aa1b7bc494d83368df7f024bf7864c7", null ],
      [ "operator const struct slice &", "group__cxx__api.html#ab866971150cc1d98e3d45f51eddad53c", null ],
      [ "operator::std::basic_string", "group__cxx__api.html#a2c743b5642d534b8fd59c4f14d861c56", null ],
      [ "operator::std::basic_string_view", "group__cxx__api.html#a84f0c51308b77a98a5f85bf763a2804a", null ],
      [ "operator=", "group__cxx__api.html#aee22e1e7219ef46732296de0a6b8d08c", null ],
      [ "operator=", "group__cxx__api.html#a57524dc8368b09993a560db9bcb8616e", null ],
      [ "operator=", "group__cxx__api.html#a29c5902aa9bb8413a7e4eac92bea5d44", null ],
      [ "operator=", "group__cxx__api.html#a79a2810db30d6dfa02304f4c472b349f", null ],
      [ "operator=", "group__cxx__api.html#a92c946c443bbac69891ba54984c8d523", null ],
      [ "operator[]", "group__cxx__api.html#aa4f9558884934aa4903c9b1d7a08da2a", null ],
      [ "operator[]", "group__cxx__api.html#a48fe31d6a526ecdd39217c6ecf1d1418", null ],
      [ "remove_prefix", "group__cxx__api.html#aa37436bde6dce8d0c875435de8a55957", null ],
      [ "remove_suffix", "group__cxx__api.html#a585e78e440c430c0ddae81c9c90c51b1", null ],
      [ "reserve", "group__cxx__api.html#a1eb8f6e935b2c19a883c8e1370cf3fdf", null ],
      [ "reserve_headroom", "group__cxx__api.html#a202767525b7633fcdbb923951790fe1a", null ],
      [ "reserve_tailroom", "group__cxx__api.html#a1413ccf2668d4d5ef728b85a51c18154", null ],
      [ "safe_head", "group__cxx__api.html#a4603c88923169a30512765958fe9a2d7", null ],
      [ "safe_middle", "group__cxx__api.html#a0875e91f1aee5549e9a5c1bdf457a7eb", null ],
      [ "safe_remove_prefix", "group__cxx__api.html#a2e80b9835e5b9d647f4e557dbdab7c83", null ],
      [ "safe_remove_suffix", "group__cxx__api.html#a2e71b043ae020adcb2218381d4a10718", null ],
      [ "safe_tail", "group__cxx__api.html#afdbab35f1faf2dfea733ec49496851dc", null ],
      [ "set_end", "group__cxx__api.html#a9130cbc5a80ffe7bb7469b60b481bafa", null ],
      [ "set_length", "group__cxx__api.html#a0eb688e0e963c7507260ebb1e367be01", null ],
      [ "shrink_to_fit", "group__cxx__api.html#ac94706b85de2485568450d7aeaab8a18", null ],
      [ "size", "group__cxx__api.html#aff52aba55a30a4341af83cc6b0c885a8", null ],
      [ "slice", "group__cxx__api.html#abde79e0a7bae6b274ae25ec3f8caf411", null ],
      [ "starts_with", "group__cxx__api.html#a652652d75e1e140970516d57c3916284", null ],
      [ "string_view", "group__cxx__api.html#a414d15bb59a691af301e7420547f6ee6", null ],
      [ "swap", "group__cxx__api.html#a3d4dc0caeee6d35814c5e99fbc8019b5", null ],
      [ "tail", "group__cxx__api.html#ab72908d3138bc6c040fb2c12bf8ab10f", null ],
      [ "tailroom", "group__cxx__api.html#ab2e3b980da0af359fe133264446b3cf2", null ],
      [ "txn", "group__cxx__api.html#afa3b547f6b8fae932f3413d1646e13eb", null ]
    ] ],
    [ "mdbx::value_result", "group__cxx__data.html#structmdbx_1_1value__result", [
      [ "value_result", "group__cxx__data.html#a853cb39997b921450fab4702c483c572", null ],
      [ "value_result", "group__cxx__data.html#a71652be6b73e5f5068270917d3e1c6ba", null ],
      [ "operator=", "group__cxx__data.html#a371805dacc95b36ef8e6ead46a47d3a4", null ],
      [ "done", "group__cxx__data.html#afdd0b1ff8d3d0c096a124c635b265311", null ],
      [ "value", "group__cxx__data.html#a3f64dc2d520aa91aad39cb743e2e82f6", null ]
    ] ],
    [ "mdbx::pair", "group__cxx__data.html#structmdbx_1_1pair", [
      [ "pair", "group__cxx__data.html#af4ed482b7b0024693cb4ba10c1a2d036", null ],
      [ "pair", "group__cxx__data.html#abdb51fead77d18d9b2be47ce77177a09", null ],
      [ "operator=", "group__cxx__data.html#a5f050c44e9cf57b5cbab15d61e742a35", null ],
      [ "key", "group__cxx__data.html#ac25d132b3bc4f75a9a4b1589268c0554", null ],
      [ "value", "group__cxx__data.html#a4c9c832eb1c42ad698bb3dda03aafa93", null ]
    ] ],
    [ "mdbx::pair_result", "group__cxx__data.html#structmdbx_1_1pair__result", [
      [ "pair_result", "group__cxx__data.html#a8870f6889f933a26bd4d34dd43cf92c2", null ],
      [ "pair_result", "group__cxx__data.html#a4e3003b7920ac1423246456a93923b3c", null ],
      [ "operator=", "group__cxx__data.html#ac75dde916b3e2ec44a4826a48c47a7de", null ],
      [ "done", "group__cxx__data.html#aab7e5bb3d732b906f5b0fd321b09638e", null ]
    ] ],
    [ "mdbx::make_buffer", "group__cxx__data.html#ga53d15f0080b1bd0856f067503b49445c", null ],
    [ "mdbx::make_buffer", "group__cxx__data.html#ga296883cd48ff800385556c1151433feb", null ],
    [ "mdbx::make_string", "group__cxx__data.html#ga9cb9c8d21f802ad1e9c5c5840c6b5ebb", null ],
    [ "mdbx::make_string", "group__cxx__data.html#ga699ef719bae005ec7b34d681589c6de1", null ],
    [ "mdbx::operator<<", "group__cxx__data.html#ga4c98eb74e342b3826352a718981d5a92", null ],
    [ "mdbx::operator<<", "group__cxx__data.html#ga55fb37ce5d8814121971fa307f3190fe", null ],
    [ "mdbx::operator<<", "group__cxx__data.html#ga6367fe947a53c5bd07e3e4b02f0da8fd", null ]
];