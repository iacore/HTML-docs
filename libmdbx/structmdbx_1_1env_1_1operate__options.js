var structmdbx_1_1env_1_1operate__options =
[
    [ "operate_options", "structmdbx_1_1env_1_1operate__options.html#a248883e27f97a3f9e10c8d5008bd1f59", null ],
    [ "operate_options", "structmdbx_1_1env_1_1operate__options.html#a37cfbcb98386344668e6d8292e34dce5", null ],
    [ "operate_options", "structmdbx_1_1env_1_1operate__options.html#a85a2060ec66d565756f0a04a0a25be40", null ],
    [ "operator=", "structmdbx_1_1env_1_1operate__options.html#a9e0a81cfb8a6001477fe668386e8403c", null ],
    [ "disable_clear_memory", "structmdbx_1_1env_1_1operate__options.html#a585f1ab43d99764e3f6ba7495a5baac6", null ],
    [ "disable_readahead", "structmdbx_1_1env_1_1operate__options.html#a16fca17f0aaea463e6f137939bb4b270", null ],
    [ "exclusive", "structmdbx_1_1env_1_1operate__options.html#a62132f0072b7053de9fe0db7f91908c9", null ],
    [ "nested_write_transactions", "structmdbx_1_1env_1_1operate__options.html#a763b06e65dc65f82f53c8f6fdd86732b", null ],
    [ "orphan_read_transactions", "structmdbx_1_1env_1_1operate__options.html#aecfa0491ffd6516fd11bd7a271de47ff", null ]
];