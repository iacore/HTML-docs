var group__api__macros =
[
    [ "LIBMDBX_INLINE_API", "group__api__macros.html#gad3bb6df5174b93f760bd5d928bca3f2e", null ],
    [ "MDBX_CONST_FUNCTION", "group__api__macros.html#gade50c074dea6f06ce2f2607139b05f03", null ],
    [ "MDBX_CXX01_CONSTEXPR", "group__api__macros.html#gaf416131d9d4705fb6cac19a71fdda78b", null ],
    [ "MDBX_CXX01_CONSTEXPR_VAR", "group__api__macros.html#gae3efa0100ba0177d51c8588944d812c1", null ],
    [ "MDBX_CXX11_CONSTEXPR", "group__api__macros.html#ga4912ff2f7d4f6a15b96c54e5cf8ee0f5", null ],
    [ "MDBX_CXX11_CONSTEXPR_VAR", "group__api__macros.html#ga981241dcda96409c7c5cb142bfce46cc", null ],
    [ "MDBX_CXX14_CONSTEXPR", "group__api__macros.html#gad12bcc78029f8edc91982e1bac29eeb9", null ],
    [ "MDBX_CXX14_CONSTEXPR_VAR", "group__api__macros.html#gaad1c038ebd4960bdf041667377f0e33d", null ],
    [ "MDBX_CXX17_NOEXCEPT", "group__api__macros.html#gad0163e55bb280ffb5a61e6170f3c0824", null ],
    [ "MDBX_DEPRECATED", "group__api__macros.html#gaceae4b96a81a5b844a664ad8a61646e3", null ],
    [ "MDBX_MAYBE_UNUSED", "group__api__macros.html#ga8759a14431cf0be314072a3a7d1b4bad", null ],
    [ "MDBX_NORETURN", "group__api__macros.html#gaf4b1af5aea6ecb2ebc13ec7019625c29", null ],
    [ "MDBX_NOTHROW_CONST_FUNCTION", "group__api__macros.html#gacb2f35485d394020899a778cf2f4d5c2", null ],
    [ "MDBX_NOTHROW_PURE_FUNCTION", "group__api__macros.html#gad5691bbd85b2cd95f66e527e46b50d3e", null ],
    [ "MDBX_PURE_FUNCTION", "group__api__macros.html#ga0f30b347da56ef3f5eee3b370e3a9402", null ]
];