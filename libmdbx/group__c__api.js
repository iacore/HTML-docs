var group__c__api =
[
    [ "Error handling", "group__c__err.html", "group__c__err" ],
    [ "Opening & Closing", "group__c__opening.html", "group__c__opening" ],
    [ "Transactions", "group__c__transactions.html", "group__c__transactions" ],
    [ "Databases", "group__c__dbi.html", "group__c__dbi" ],
    [ "Create/Read/Update/Delete (see Quick Reference in details)", "group__c__crud.html", "group__c__crud" ],
    [ "Cursors", "group__c__cursors.html", "group__c__cursors" ],
    [ "Statistics & Information", "group__c__statinfo.html", "group__c__statinfo" ],
    [ "Settings", "group__c__settings.html", "group__c__settings" ],
    [ "Logging and runtime debug", "group__c__debug.html", "group__c__debug" ],
    [ "Range query estimation", "group__c__rqest.html", "group__c__rqest" ],
    [ "Extra operations", "group__c__extra.html", "group__c__extra" ],
    [ "Value-to-Key functions", "group__value2key.html", "group__value2key" ],
    [ "Key-to-Value functions", "group__key2value.html", "group__key2value" ],
    [ "B-tree Traversal", "group__btree__traversal.html", "group__btree__traversal" ],
    [ "MDBX_version_info", "group__c__api.html#struct_m_d_b_x__version__info", [
      [ "git", "group__c__api.html#acb0c7b64b8ea3f7d1d44ab2a30cac3f5", null ],
      [ "major", "group__c__api.html#a93d4d6afac37c261472507e353b7a062", null ],
      [ "minor", "group__c__api.html#a7cf7e5566d7a254322da7184031ab1f7", null ],
      [ "release", "group__c__api.html#a4e94c02e5ec71cb6fd1c2b9f90299d5b", null ],
      [ "revision", "group__c__api.html#a77b126b435d465e727b5461a61733903", null ],
      [ "sourcery", "group__c__api.html#a9064639a0c91db8d767c2e6786e286ec", null ]
    ] ],
    [ "MDBX_build_info", "group__c__api.html#struct_m_d_b_x__build__info", [
      [ "compiler", "group__c__api.html#afda983432d95948e729a00f09348b47e", null ],
      [ "datetime", "group__c__api.html#a48f66b2e4a718598c39a5c26f4ce4db2", null ],
      [ "flags", "group__c__api.html#aabda980b6fafcc982f6e4f7167420bcf", null ],
      [ "options", "group__c__api.html#a8cb0cf4a47cdd43be9c7e483f47ba3a1", null ],
      [ "target", "group__c__api.html#ac63daab4e4e383a721701fbf6ab66bf5", null ]
    ] ],
    [ "MDBX_stat", "group__c__api.html#struct_m_d_b_x__stat", [
      [ "ms_branch_pages", "group__c__api.html#a5f18be64fd542852a2c849956045210b", null ],
      [ "ms_depth", "group__c__api.html#ac912b2789383cfb1896b5aa90a0ffc1e", null ],
      [ "ms_entries", "group__c__api.html#a5f865cc51d11174c7f516a00fa1be69e", null ],
      [ "ms_leaf_pages", "group__c__api.html#ae93ba31086818b08d3fd62065881c59c", null ],
      [ "ms_mod_txnid", "group__c__api.html#ad901e3f906d39b14c4cead6fa367168f", null ],
      [ "ms_overflow_pages", "group__c__api.html#a9c3c01c5201f2fc91f92c29e7c6cfedd", null ],
      [ "ms_psize", "group__c__api.html#abaf71a83303b0a23f9f298da6c730745", null ]
    ] ],
    [ "MDBX_envinfo", "group__c__api.html#struct_m_d_b_x__envinfo", [
      [ "mi_autosync_period_seconds16dot16", "group__c__api.html#aeccf072444af6392f2dfd37358e475d2", null ],
      [ "mi_autosync_threshold", "group__c__api.html#a550ce8091fb61b69b42b0c9b248ff940", null ],
      [ "mi_bootid", "group__c__api.html#a3ea963b0b66534d072e634fd14b0e459", null ],
      [ "mi_dxb_pagesize", "group__c__api.html#a0eb186eea75d9cecc94d15992bc79654", null ],
      [ "mi_geo", "group__c__api.html#a9ad40e0adab91b5b7bedb4823b4ebfdb", null ],
      [ "mi_last_pgno", "group__c__api.html#acb89c98ff9b18341ffbf12f1c65e03dc", null ],
      [ "mi_latter_reader_txnid", "group__c__api.html#a993a3b51fc5bd7ded9468f5d2e63c099", null ],
      [ "mi_mapsize", "group__c__api.html#a040372f37123dd4f9000d6190011bf3f", null ],
      [ "mi_maxreaders", "group__c__api.html#a1150fcaca8be362dbc71267a757ec435", null ],
      [ "mi_meta0_sign", "group__c__api.html#ab056131ff1f41b2e41a162bdf3a823b8", null ],
      [ "mi_meta0_txnid", "group__c__api.html#a7e362f67cdc659dafccbd7397d112d70", null ],
      [ "mi_meta1_sign", "group__c__api.html#a0b26f4f6126888349e9ca8c46906cb45", null ],
      [ "mi_meta1_txnid", "group__c__api.html#ad6cf0c3db7807d34119af83288fd82f3", null ],
      [ "mi_meta2_sign", "group__c__api.html#a77762937ae9ead5d4bf5b4c7d4c4edbc", null ],
      [ "mi_meta2_txnid", "group__c__api.html#a001963011e0789749ab4c8408f6f007d", null ],
      [ "mi_mode", "group__c__api.html#a8abf704de1ba63fc47b3000536f20e3c", null ],
      [ "mi_numreaders", "group__c__api.html#a1ad65f8a986006cc02e8a8bbf4ed4602", null ],
      [ "mi_pgop_stat", "group__c__api.html#a1c2f3023cebcd09431c9c7e0f75d62b7", null ],
      [ "mi_recent_txnid", "group__c__api.html#a5368f5b58a20e205913a9d25b1417f8c", null ],
      [ "mi_self_latter_reader_txnid", "group__c__api.html#ae4e046a4965798631d8ebc78b7810ebc", null ],
      [ "mi_since_reader_check_seconds16dot16", "group__c__api.html#a71957c49d40cba112b6bb104a9b5202b", null ],
      [ "mi_since_sync_seconds16dot16", "group__c__api.html#a2288193669b48107ead4ef049d999c3f", null ],
      [ "mi_sys_pagesize", "group__c__api.html#aedfadab30b250fedb2422f8bbe122a69", null ],
      [ "mi_unsync_volume", "group__c__api.html#aedaae3f60aaa621c42e0dfd35a962d2f", null ]
    ] ],
    [ "MDBX_txn_info", "group__c__api.html#struct_m_d_b_x__txn__info", [
      [ "txn_id", "group__c__api.html#a8fa3b9f102114b6a412335cbe2c0802e", null ],
      [ "txn_reader_lag", "group__c__api.html#a383fe4a97f9108000d5c69590738f8ad", null ],
      [ "txn_space_dirty", "group__c__api.html#a0a13745cd97c277cd810f3bbf60c0d9f", null ],
      [ "txn_space_leftover", "group__c__api.html#a461b926cfd0e656bebac9bab5997d6b1", null ],
      [ "txn_space_limit_hard", "group__c__api.html#aa93c141047f682507850e6f7a08f5557", null ],
      [ "txn_space_limit_soft", "group__c__api.html#a8aa8674b1b08e6f956d893c02c1b4514", null ],
      [ "txn_space_retired", "group__c__api.html#ac8a5a3e9a6f4bfba24ca9dd8d957030f", null ],
      [ "txn_space_used", "group__c__api.html#a48515214b62493d820cbafda219bcc48", null ]
    ] ],
    [ "MDBX_commit_latency", "group__c__api.html#struct_m_d_b_x__commit__latency", [
      [ "audit", "group__c__api.html#abd60c7295a4970c49fb8833703a54758", null ],
      [ "ending", "group__c__api.html#acf2390b715b885cb8f484bc30fdd3381", null ],
      [ "gc", "group__c__api.html#ad901e40e841a3bfb94b865008cfd9c2f", null ],
      [ "preparation", "group__c__api.html#aa2e2b38a085e90af171c8561e8f0e9e9", null ],
      [ "sync", "group__c__api.html#a89ade90ede0e8ccec1d729e81ae6b956", null ],
      [ "whole", "group__c__api.html#aa97dacaedc16f1c31df6888096e63809", null ],
      [ "write", "group__c__api.html#ac8928e8af622cc1e7b2b28ceae44a013", null ]
    ] ],
    [ "MDBX_canary", "group__c__api.html#struct_m_d_b_x__canary", [
      [ "v", "group__c__api.html#a133616af3574000dca91d54ae42fb207", null ],
      [ "x", "group__c__api.html#aec4d5064ce5ecf2a922fded78afd9809", null ],
      [ "y", "group__c__api.html#a758ceb1c73be6fe6a04b8cf134cd9653", null ],
      [ "z", "group__c__api.html#aa6dccd072653cacac8db171f09c61225", null ]
    ] ],
    [ "MDBX_version_info.git", "group__c__api.html#struct_m_d_b_x__version__info_8git", [
      [ "commit", "group__c__api.html#afffca4d67ea0a788813031b8bbc3b329", null ],
      [ "datetime", "group__c__api.html#adfeaaeb4316477bd556ea5e8c3295887", null ],
      [ "describe", "group__c__api.html#a0d16430e9f0aa8879ba9dac2540543fc", null ],
      [ "tree", "group__c__api.html#ac0af77cf8294ff93a5cdb2963ca9f038", null ]
    ] ],
    [ "MDBX_envinfo.mi_geo", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__geo", [
      [ "current", "group__c__api.html#a43b5c9175984c071f30b873fdce0a000", null ],
      [ "grow", "group__c__api.html#a4d200fce73a8e1cc965cfc2c43343824", null ],
      [ "lower", "group__c__api.html#a81e073b428b50247daba38531dcf412a", null ],
      [ "shrink", "group__c__api.html#a91808da94826016395b10b662ed4363b", null ],
      [ "upper", "group__c__api.html#a0122b4c2c01ee1c698ecc309d2b8eb5a", null ]
    ] ],
    [ "MDBX_envinfo.mi_bootid", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid", [
      [ "current", "group__c__api.html#a43b5c9175984c071f30b873fdce0a000", null ],
      [ "meta0", "group__c__api.html#a693c1c0352f1f8a95cf36d4d71a57ac1", null ],
      [ "meta1", "group__c__api.html#ab6c6efe8d7ca0aa0d8b7c03370ba7525", null ],
      [ "meta2", "group__c__api.html#a8daee5ed440dd7940e0b42c74ad871e3", null ]
    ] ],
    [ "MDBX_envinfo.mi_bootid.current", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8current", [
      [ "x", "group__c__api.html#a9dd4e461268c8034f5c8564e155c67a6", null ],
      [ "y", "group__c__api.html#a415290769594460e2e485922904f345d", null ]
    ] ],
    [ "MDBX_envinfo.mi_bootid.meta0", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta0", [
      [ "x", "group__c__api.html#a9dd4e461268c8034f5c8564e155c67a6", null ],
      [ "y", "group__c__api.html#a415290769594460e2e485922904f345d", null ]
    ] ],
    [ "MDBX_envinfo.mi_bootid.meta1", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta1", [
      [ "x", "group__c__api.html#a9dd4e461268c8034f5c8564e155c67a6", null ],
      [ "y", "group__c__api.html#a415290769594460e2e485922904f345d", null ]
    ] ],
    [ "MDBX_envinfo.mi_bootid.meta2", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta2", [
      [ "x", "group__c__api.html#a9dd4e461268c8034f5c8564e155c67a6", null ],
      [ "y", "group__c__api.html#a415290769594460e2e485922904f345d", null ]
    ] ],
    [ "MDBX_envinfo.mi_pgop_stat", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__pgop__stat", [
      [ "clone", "group__c__api.html#ad329fd777726c300d7a044e482b967e7", null ],
      [ "cow", "group__c__api.html#a81566e986cf8cc685a05ac5b634af7f8", null ],
      [ "fsync", "group__c__api.html#ae590322920bebc1156ab585bd6597d76", null ],
      [ "gcrtime_seconds16dot16", "group__c__api.html#ad5a022e40d7a1c044ec5cdbd6f8c4bd8", null ],
      [ "merge", "group__c__api.html#a65464c31b2e6ac04da1fcaa37c9bd9c7", null ],
      [ "msync", "group__c__api.html#a9203afd58680f38ca965857947803e55", null ],
      [ "newly", "group__c__api.html#aa6bb8334a35c997792de2d48d43e3353", null ],
      [ "spill", "group__c__api.html#a83e30f61fbdbe1d3b341882e4f9f4733", null ],
      [ "split", "group__c__api.html#aeefec303079ad17405c889e092e105b0", null ],
      [ "unspill", "group__c__api.html#afb8d02133b1fd21a4966d02176e66be9", null ],
      [ "wops", "group__c__api.html#affcaae187bb01c8297591440736e102a", null ]
    ] ],
    [ "LIBMDBX_API", "group__c__api.html#ga3ff81c5c75bd3d7836f03a05649d5e8f", null ],
    [ "LIBMDBX_API_TYPE", "group__c__api.html#ga570e05a7ca62d5ad7496b4690cd77871", null ],
    [ "LIBMDBX_VERINFO_API", "group__c__api.html#gafd01246bf5e9bf5e540a0db877043f0e", null ],
    [ "MDBX_DATANAME", "group__c__api.html#gaea0edfb8c722071d05f8553598f13568", null ],
    [ "MDBX_LOCK_SUFFIX", "group__c__api.html#gaad8eaf3ef18ca75672d45300178698b3", null ],
    [ "MDBX_LOCKNAME", "group__c__api.html#ga0391caec5599e8afd9c438cb44bda78c", null ],
    [ "MDBX_MAP_RESIZED", "group__c__api.html#ga05cda0dc9e94754d5450bf5d6b74deca", null ],
    [ "MDBX_VERSION_MAJOR", "group__c__api.html#ga38aaa52a64a59d2a35d8a7e1ca70eb22", null ],
    [ "MDBX_VERSION_MINOR", "group__c__api.html#gaad2e1a42ca7d9fa6099524db3804ad69", null ],
    [ "MDBX_env", "group__c__api.html#gaaf0e4c9dc12dce0c2a98566436c94222", null ],
    [ "mdbx_filehandle_t", "group__c__api.html#gab037794c5da06d37484e93f4b4c65791", null ],
    [ "mdbx_mode_t", "group__c__api.html#ga209c49f8acf4fd9e042301373c64badb", null ],
    [ "mdbx_pid_t", "group__c__api.html#gaee85974ec8807e4c3ef3cd8247300c6a", null ],
    [ "MDBX_preserve_func", "group__c__api.html#ga24e04218b7b2318ee2d171a7baca4bff", null ],
    [ "mdbx_tid_t", "group__c__api.html#ga8f4fcf785822f06763e4f81cd1a62240", null ],
    [ "MDBX_txn_flags_t", "group__c__api.html#ga6fc43913066697c91a3f05b1d51d6d92", null ],
    [ "MDBX_val", "group__c__api.html#ga5eedfc4a83296bbf0f8ff93359cb0900", null ],
    [ "MDBX_constants", "group__c__api.html#ga190b5c6e7f916f612c5819e1ab8acfdf", [
      [ "MDBX_MAX_DBI", "group__c__api.html#gga190b5c6e7f916f612c5819e1ab8acfdfac5988778fc33789ece8796cf9d2e77c2", null ],
      [ "MDBX_MAXDATASIZE", "group__c__api.html#gga190b5c6e7f916f612c5819e1ab8acfdfa92ddaf7c24c6a036bce86f28e9d60f2f", null ],
      [ "MDBX_MIN_PAGESIZE", "group__c__api.html#gga190b5c6e7f916f612c5819e1ab8acfdfad5226a9d20e598c026beabe913090cd8", null ],
      [ "MDBX_MAX_PAGESIZE", "group__c__api.html#gga190b5c6e7f916f612c5819e1ab8acfdfa167faeb1842bdc6ada334963520fc4dd", null ]
    ] ],
    [ "MDBX_option_t", "group__c__api.html#ga671855605968c3b1f89a72e2d7b71eb3", [
      [ "MDBX_opt_max_db", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3a5501f6899c2a3c82bedcbc079b850247", null ],
      [ "MDBX_opt_max_readers", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3aa654ce6b9cfa1345330e7ed881eb44fc", null ],
      [ "MDBX_opt_sync_bytes", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3a59f87bdc6be137cc4507bea7171d7a85", null ],
      [ "MDBX_opt_sync_period", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3abd7019e0ab56b583338765413e755c0c", null ],
      [ "MDBX_opt_rp_augment_limit", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3a9fe64bcad43752eac6596cbb49a2be2d", null ],
      [ "MDBX_opt_loose_limit", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3a8062929c24422aa10654671abd707401", null ],
      [ "MDBX_opt_dp_reserve_limit", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3ac98546b254e33d482e15ae1208c34174", null ],
      [ "MDBX_opt_txn_dp_limit", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3a262cec5ffe0762dd2254fe6dd7b2e6c7", null ],
      [ "MDBX_opt_txn_dp_initial", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3a014fb4148d953aef369680b4f79ea6dc", null ],
      [ "MDBX_opt_spill_max_denominator", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3ab1150ff5efb3992daf6626909d17a1e2", null ],
      [ "MDBX_opt_spill_min_denominator", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3a5b1a57f0af52b62ac57d594f939e06a9", null ],
      [ "MDBX_opt_spill_parent4child_denominator", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3a6b1f87c2b59d261c2e8c990671fda0cc", null ],
      [ "MDBX_opt_merge_threshold_16dot16_percent", "group__c__api.html#gga671855605968c3b1f89a72e2d7b71eb3a552388b6d20c8e5ba2db00a025f22e5a", null ]
    ] ],
    [ "mdbx_env_get_hsr", "group__c__api.html#ga0378c6d00a76cdd588ce680f13bfeb09", null ],
    [ "mdbx_liberr2str", "group__c__api.html#gab1294012d98d6a2d3730cf3b8b52364a", null ],
    [ "mdbx_replace_ex", "group__c__api.html#ga624b229dcc6d5784ecbda4785a70c014", null ],
    [ "mdbx_build", "group__c__api.html#ga993cb09fe3e5a4e7729956a7808fd8ca", null ],
    [ "mdbx_version", "group__c__api.html#ga811bbc5869aef1bb33a1fd109680fada", null ]
];