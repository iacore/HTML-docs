var group__c__settings =
[
    [ "MDBX_option_t", "group__c__settings.html#gaeac57a77220d12b8dbf8ff7ef1e770cf", null ],
    [ "mdbx_env_get_option", "group__c__settings.html#ga4141e15ff56b2019ae669ae072f432c7", null ],
    [ "mdbx_env_set_flags", "group__c__settings.html#gaf24ab36da42e5b2b70d02f6027f6d331", null ],
    [ "mdbx_env_set_geometry", "group__c__settings.html#ga79065e4f3c5fb2ad37a52b59224d583e", null ],
    [ "mdbx_env_set_mapsize", "group__c__settings.html#gaed9eb291552790f75087b3b097134ae0", null ],
    [ "mdbx_env_set_maxdbs", "group__c__settings.html#ga39ab74bb9bbfaab1cc586642fec4559a", null ],
    [ "mdbx_env_set_maxreaders", "group__c__settings.html#gadefa6e24ea453f2631fb6cc7871cfda6", null ],
    [ "mdbx_env_set_option", "group__c__settings.html#gaa008fb6a361871a0a6e740fd814f5874", null ],
    [ "mdbx_env_set_syncbytes", "group__c__settings.html#ga7e64d98f7a8c3bdf7727627782f1be0a", null ],
    [ "mdbx_env_set_syncperiod", "group__c__settings.html#ga465c4c08a727aef512b8e0838b236d4a", null ],
    [ "mdbx_env_set_userctx", "group__c__settings.html#gabc6cbea17093abce0a055871492faf39", null ]
];