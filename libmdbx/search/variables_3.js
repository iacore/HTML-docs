var searchData=
[
  ['datetime_0',['datetime',['../group__c__api.html#a48f66b2e4a718598c39a5c26f4ce4db2',1,'MDBX_build_info']]],
  ['db_5fcorrupted_1',['db_corrupted',['../group__cxx__exceptions.html#ga4d30f06674ca02924fc6f3a270d2ca3d',1,'mdbx']]],
  ['db_5ffull_2',['db_full',['../group__cxx__exceptions.html#ga533bcd5fb5b964748ad689e6dbfb5d21',1,'mdbx']]],
  ['db_5finvalid_3',['db_invalid',['../group__cxx__exceptions.html#gabc4d4aef025dd397f2dc5b74c77fa3e1',1,'mdbx']]],
  ['db_5ftoo_5flarge_4',['db_too_large',['../group__cxx__exceptions.html#ga3025a206a092724211830a21df3b2f9c',1,'mdbx']]],
  ['db_5funable_5fextend_5',['db_unable_extend',['../group__cxx__exceptions.html#ga83c92da61752af74b60d0bf0e2cbce3e',1,'mdbx']]],
  ['db_5fversion_5fmismatch_6',['db_version_mismatch',['../group__cxx__exceptions.html#gaef1ed3fe29eca1b7072620d2f81d8921',1,'mdbx']]],
  ['db_5fwanna_5fwrite_5ffor_5frecovery_7',['db_wanna_write_for_recovery',['../group__cxx__exceptions.html#ga2660c226ed4443b569718c81f3e99ef5',1,'mdbx']]],
  ['dbi_8',['dbi',['../group__cxx__api.html#ae2fffa7bc45893a1f8fda0798d027358',1,'mdbx::map_handle']]],
  ['disable_5fclear_5fmemory_9',['disable_clear_memory',['../structmdbx_1_1env_1_1operate__options.html#a585f1ab43d99764e3f6ba7495a5baac6',1,'mdbx::env::operate_options']]],
  ['disable_5freadahead_10',['disable_readahead',['../structmdbx_1_1env_1_1operate__options.html#a16fca17f0aaea463e6f137939bb4b270',1,'mdbx::env::operate_options']]],
  ['done_11',['done',['../group__cxx__data.html#afdd0b1ff8d3d0c096a124c635b265311',1,'mdbx::value_result::done()'],['../group__cxx__data.html#aab7e5bb3d732b906f5b0fd321b09638e',1,'mdbx::pair_result::done()']]],
  ['durability_12',['durability',['../structmdbx_1_1env_1_1operate__parameters.html#a0adfcbcb58a7a764b34decc59c5c6572',1,'mdbx::env::operate_parameters']]]
];
