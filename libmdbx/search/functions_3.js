var searchData=
[
  ['data_0',['data',['../group__cxx__api.html#gac7021bc86d1e27c3d98635b732a6b897',1,'mdbx::slice::data() const noexcept'],['../group__cxx__api.html#gaebc3b5cca084173ab068fef2628bb24b',1,'mdbx::slice::data() noexcept'],['../group__cxx__api.html#ac34c10de8bad4ce1b3e0170a6413e419',1,'mdbx::buffer::data() const noexcept'],['../group__cxx__api.html#a5be747a95998e2e133b79fcc719cc4e6',1,'mdbx::buffer::data() noexcept']]],
  ['dbsize_5fmax_1',['dbsize_max',['../group__cxx__api.html#ga0fcfee07c4671e1b7130b45472ca9043',1,'mdbx::env::limits::dbsize_max()'],['../group__cxx__api.html#aa0c5652df5c0fb369cdc4414855269bc',1,'mdbx::env::dbsize_max()']]],
  ['dbsize_5fmin_2',['dbsize_min',['../group__cxx__api.html#ga3a4b6f81e70d6ff35c766a794fa0327f',1,'mdbx::env::limits::dbsize_min()'],['../group__cxx__api.html#a263cf136965400e117923e4629671576',1,'mdbx::env::dbsize_min() const']]],
  ['default_5fpagesize_3',['default_pagesize',['../group__cxx__api.html#a5d394ed1ca0f809c5ff6b7420f88cc98',1,'mdbx::env']]],
  ['drop_5fmap_4',['drop_map',['../group__cxx__api.html#ga789cf4dbf6a921b4e0d106156af0d30b',1,'mdbx::txn::drop_map(map_handle map)'],['../group__cxx__api.html#a51f25e00c19df9ae0e67be07923ad382',1,'mdbx::txn::drop_map(const char *name, bool throw_if_absent=false)'],['../group__cxx__api.html#gade988976281cb1e585c67f42667092de',1,'mdbx::txn::drop_map(const ::std::string &amp;name, bool throw_if_absent=false)']]],
  ['durability_5ffrom_5fflags_5',['durability_from_flags',['../structmdbx_1_1env_1_1operate__parameters.html#abbc7316519a8eb6b98a481ea8c0c0281',1,'mdbx::env::operate_parameters']]]
];
