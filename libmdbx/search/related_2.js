var searchData=
[
  ['operator_21_3d_0',['operator!=',['../group__cxx__exceptions.html#a8221bfe42e9570d1c5a9b21b16dada2a',1,'mdbx::error::operator!=()'],['../group__cxx__data.html#aaa7e1c7159660896d5573d92dd57c5ca',1,'mdbx::slice::operator!=()'],['../group__cxx__api.html#a41a4e8d53e6ce9f9d85f2512bff5cfe7',1,'mdbx::env::operator!=()'],['../group__cxx__api.html#ac5d2615f25f19e9059a7f033dc2dac80',1,'mdbx::txn::operator!=()'],['../group__cxx__api.html#ab149c38ca2aad2a97a70fe18ebbdd368',1,'mdbx::cursor::operator!=()']]],
  ['operator_3c_1',['operator&lt;',['../group__cxx__data.html#adb7349d8724735c67f5bf06550c8f1fe',1,'mdbx::slice']]],
  ['operator_3c_3d_2',['operator&lt;=',['../group__cxx__data.html#a2071047a9332799185ac96798730ed4a',1,'mdbx::slice']]],
  ['operator_3d_3d_3',['operator==',['../group__cxx__exceptions.html#a666d71c1eaafd866151a14006e5b5174',1,'mdbx::error::operator==()'],['../group__cxx__data.html#aa11b3c7589162ca7be1a433924adc77b',1,'mdbx::slice::operator==()'],['../group__cxx__api.html#a09fa9e97e93b0a81b2fc1005a6964523',1,'mdbx::env::operator==()'],['../group__cxx__api.html#ab7ae0861815b1e4ff40e883ce2a29919',1,'mdbx::txn::operator==()'],['../group__cxx__api.html#a03d5183c722e4010ab90d6629da6dd24',1,'mdbx::cursor::operator==()']]],
  ['operator_3e_4',['operator&gt;',['../group__cxx__data.html#a0a038605fe8f60588b31f7872b1411c9',1,'mdbx::slice']]],
  ['operator_3e_3d_5',['operator&gt;=',['../group__cxx__data.html#a3876d218971e4e5ea14092c070638d7b',1,'mdbx::slice']]]
];
