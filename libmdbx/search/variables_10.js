var searchData=
[
  ['shrink_5fthreshold_0',['shrink_threshold',['../structmdbx_1_1env_1_1geometry.html#ab233e3401b58b5d88362ce9457870d54',1,'mdbx::env::geometry']]],
  ['size_5flower_1',['size_lower',['../structmdbx_1_1env_1_1geometry.html#abfd123ee2277df77373cc6d59ae0472c',1,'mdbx::env::geometry']]],
  ['size_5fnow_2',['size_now',['../structmdbx_1_1env_1_1geometry.html#ab204b6da9885831ca0314a2f8c2ae556',1,'mdbx::env::geometry']]],
  ['size_5fupper_3',['size_upper',['../structmdbx_1_1env_1_1geometry.html#a4368dc988eefc0e1a49174afe4598222',1,'mdbx::env::geometry']]],
  ['slot_4',['slot',['../structmdbx_1_1env_1_1reader__info.html#ad4f16ee1d59c595ae989c2afe1642630',1,'mdbx::env::reader_info']]],
  ['something_5fbusy_5',['something_busy',['../group__cxx__exceptions.html#ga1a0d01eea9cdca3e29529a8d881e761d',1,'mdbx']]],
  ['source_6',['source',['../group__cxx__data.html#a524c73a88b07fc9ea9d5ac4805025792',1,'mdbx::to_hex::source()'],['../group__cxx__data.html#a877fbccbf9cdc4c7b8a45b0dd80e93db',1,'mdbx::to_base58::source()'],['../group__cxx__data.html#a169245978a989b5741ce5af99029eca3',1,'mdbx::to_base64::source()'],['../group__cxx__data.html#afe07bbe916d8c17f210c127b62a81071',1,'mdbx::from_hex::source()'],['../group__cxx__data.html#a515bbd8e1acfca246bbb3e7972604868',1,'mdbx::from_base58::source()'],['../group__cxx__data.html#ac565c8e62386c2732d53de7c5d8b5970',1,'mdbx::from_base64::source()']]],
  ['sourcery_7',['sourcery',['../group__c__api.html#a9064639a0c91db8d767c2e6786e286ec',1,'MDBX_version_info']]],
  ['state_8',['state',['../structmdbx_1_1map__handle_1_1info.html#a6b69e8a5900ec845f5ea12f24fb3fcad',1,'mdbx::map_handle::info']]],
  ['sync_9',['sync',['../group__c__api.html#a89ade90ede0e8ccec1d729e81ae6b956',1,'MDBX_commit_latency']]]
];
