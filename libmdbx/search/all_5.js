var searchData=
[
  ['fatal_0',['fatal',['../group__cxx__exceptions.html#a1390c6c0748daae065821ba0deb55812',1,'mdbx::fatal::fatal(const ::mdbx::error &amp;) noexcept'],['../group__cxx__exceptions.html#a288bd58802d4ac3a721a8b23fafabeab',1,'mdbx::fatal::fatal(const exception &amp;src) noexcept'],['../group__cxx__exceptions.html#a0ac34c5fe67fe10722497dd5c022797a',1,'mdbx::fatal::fatal(exception &amp;&amp;src) noexcept'],['../group__cxx__exceptions.html#a7afdb991100f613a21d1de8a20850d61',1,'mdbx::fatal::fatal(const fatal &amp;src) noexcept'],['../group__cxx__exceptions.html#ac078e3296eba1188cb544e6f1aee7bf5',1,'mdbx::fatal::fatal(fatal &amp;&amp;src) noexcept'],['../group__cxx__exceptions.html#classmdbx_1_1fatal',1,'mdbx::fatal']]],
  ['file_5fmode_5fbits_1',['file_mode_bits',['../structmdbx_1_1env__managed_1_1create__parameters.html#a651ae24ab2bc56c43140eab3804891a5',1,'mdbx::env_managed::create_parameters']]],
  ['filehandle_2',['filehandle',['../group__cxx__api.html#ga33f7be477cbfcc962197cf57d0268c7a',1,'mdbx']]],
  ['find_3',['find',['../group__cxx__api.html#ga58187a4f183afe0238912b9a4c2d1279',1,'mdbx::cursor']]],
  ['find_5fkey_4',['find_key',['../group__cxx__api.html#abac5f7d4c71d8424cffea5a19925852daac833cce2cefd8b997f92db4f5b16765',1,'mdbx::cursor']]],
  ['find_5fmultivalue_5',['find_multivalue',['../group__cxx__api.html#ga0d6a71c4470f1bb486c6be590887aaf9',1,'mdbx::cursor']]],
  ['first_6',['first',['../group__cxx__api.html#abac5f7d4c71d8424cffea5a19925852dab4550d1bd5498d6e1b3cec58953de35f',1,'mdbx::cursor']]],
  ['flags_7',['flags',['../group__cxx__api.html#a1468da7d5cad42eb67ee1e0e4c1a9d6f',1,'mdbx::map_handle::flags()'],['../group__c__api.html#aabda980b6fafcc982f6e4f7167420bcf',1,'MDBX_build_info::flags()'],['../structmdbx_1_1map__handle_1_1info.html#ae89276f59c5f8d8b20c87f978d020844',1,'mdbx::map_handle::info::flags()'],['../group__cxx__api.html#ga90b4be27d4c242587fc2a8420277d45e',1,'mdbx::txn::flags()']]],
  ['from_5fbase58_8',['from_base58',['../group__cxx__data.html#a6afd0045a62b0cf8336ff4bf520aecfc',1,'mdbx::from_base58::from_base58()'],['../group__cxx__data.html#structmdbx_1_1from__base58',1,'mdbx::from_base58']]],
  ['from_5fbase64_9',['from_base64',['../group__cxx__data.html#abd255392596df765deba4c9835adce93',1,'mdbx::from_base64::from_base64()'],['../group__cxx__data.html#structmdbx_1_1from__base64',1,'mdbx::from_base64']]],
  ['from_5fhex_10',['from_hex',['../group__cxx__data.html#a7c6ac407bc1da6a34392749bfd3c2395',1,'mdbx::from_hex::from_hex()'],['../group__cxx__data.html#structmdbx_1_1from__hex',1,'mdbx::from_hex']]]
];
