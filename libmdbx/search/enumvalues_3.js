var searchData=
[
  ['eb_0',['EB',['../structmdbx_1_1env_1_1geometry.html#ae4f041c107c53c3de7d3df8c4b6f0d71a9030d9839b65a08b02877374e43c5678',1,'mdbx::env::geometry']]],
  ['eib_1',['EiB',['../structmdbx_1_1env_1_1geometry.html#ae4f041c107c53c3de7d3df8c4b6f0d71a7d99f03b14c973413607e8465a4e62fa',1,'mdbx::env::geometry']]],
  ['ensure_5funused_2',['ensure_unused',['../group__cxx__api.html#a903e64cb8e0eaca469560f403d509858a30017f7b7d020093b401ee4b5d4def66',1,'mdbx::env']]],
  ['exit_5floop_3',['exit_loop',['../group__cxx__api.html#ggabccc2fb093eebf6e0c622f620b4ff6f7a4e28715b40eb12f7b49725315b94dd4d',1,'mdbx']]],
  ['extra_5finplace_5fstorage_4',['extra_inplace_storage',['../group__cxx__data.html#a93d255ecdabb5016aa672b64a5af2d13a76f79d93a8767799d886ba1765fd4c7b',1,'mdbx::default_capacity_policy::extra_inplace_storage()'],['../group__cxx__api.html#a37a48f42db9bca3e235e4fe97bfdf8fca3b611f29e500d487fcf10968732d404c',1,'mdbx::buffer::extra_inplace_storage()']]]
];
