var searchData=
[
  ['ignore_5fspaces_0',['ignore_spaces',['../group__cxx__data.html#a5c0f956dd21381b0955c61d6c7266fe6',1,'mdbx::from_hex::ignore_spaces()'],['../group__cxx__data.html#a321ddc91e8f728c8afb5eebd3af07489',1,'mdbx::from_base58::ignore_spaces()'],['../group__cxx__data.html#abb1c4a6187509f5447f329b06417e348',1,'mdbx::from_base64::ignore_spaces()']]],
  ['incompatible_5foperation_1',['incompatible_operation',['../group__cxx__exceptions.html#ga080c7d3506129b0b279b9a80c0bdee10',1,'mdbx']]],
  ['inplace_5f_2',['inplace_',['../unionmdbx_1_1buffer_1_1silo_1_1bin.html#a5c4ab46c6e785d61bcf7119a8f7b09fa',1,'mdbx::buffer::silo::bin']]],
  ['internal_5fpage_5ffull_3',['internal_page_full',['../group__cxx__exceptions.html#gacfd49da76dd89a90adcfffcae3f5e175',1,'mdbx']]],
  ['internal_5fproblem_4',['internal_problem',['../group__cxx__exceptions.html#gafb972cb753c11f9a34ad1b0999bd93e6',1,'mdbx']]]
];
