var searchData=
[
  ['what_0',['what',['../group__cxx__exceptions.html#a75485efc39aeca9ff83ff6e0788907ee',1,'mdbx::error']]],
  ['wrap_1',['wrap',['../group__cxx__data.html#a36cc25297ca7909254e0ad12dcdf94c1',1,'mdbx::slice::wrap(const char(&amp;text)[SIZE])'],['../group__cxx__data.html#af1136ec23d80c54d9b9b9efa9eddc5c0',1,'mdbx::slice::wrap(const POD &amp;pod)'],['../group__cxx__api.html#aa2826a12309f0483393de56a2db2b544',1,'mdbx::buffer::wrap()']]],
  ['write_5fbytes_2',['write_bytes',['../group__cxx__data.html#aa3824c315eca31836238cbe12c08888e',1,'mdbx::to_hex::write_bytes()'],['../group__cxx__data.html#afdd9a40d56b2237e83cb0eaa360aff4b',1,'mdbx::to_base58::write_bytes()'],['../group__cxx__data.html#af2ea58fe81b53c3c108f09360b2561af',1,'mdbx::to_base64::write_bytes()'],['../group__cxx__data.html#acdcb483d3a6209ecec22bc996cea6b59',1,'mdbx::from_hex::write_bytes()'],['../group__cxx__data.html#a5ea2655ae544feeb3f9e675b05f30745',1,'mdbx::from_base58::write_bytes()'],['../group__cxx__data.html#a3b736ac0a82b59700b2aa9dfbf58560f',1,'mdbx::from_base64::write_bytes()']]]
];
