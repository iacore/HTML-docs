var searchData=
[
  ['pb_0',['PB',['../structmdbx_1_1env_1_1geometry.html#ae4f041c107c53c3de7d3df8c4b6f0d71a6657d9be538d290ed9e6e2f697c4706d',1,'mdbx::env::geometry']]],
  ['pettiness_5fthreshold_1',['pettiness_threshold',['../group__cxx__data.html#a93d255ecdabb5016aa672b64a5af2d13a71f76d6edfd92dd0d62258032f28542f',1,'mdbx::default_capacity_policy::pettiness_threshold()'],['../group__cxx__api.html#a37a48f42db9bca3e235e4fe97bfdf8fca0d5f6930bd0fdc8ecaa84a40aea57429',1,'mdbx::buffer::pettiness_threshold()']]],
  ['pib_2',['PiB',['../structmdbx_1_1env_1_1geometry.html#ae4f041c107c53c3de7d3df8c4b6f0d71abb0ff2729576f962ed62664d89c0a7d0',1,'mdbx::env::geometry']]],
  ['previous_3',['previous',['../group__cxx__api.html#abac5f7d4c71d8424cffea5a19925852daa87699467ce3f647d15c395a09983ce6',1,'mdbx::cursor']]]
];
