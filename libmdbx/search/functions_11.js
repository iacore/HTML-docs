var searchData=
[
  ['update_0',['update',['../group__cxx__api.html#gac0a3bfc1c3715f615a95870115c19d8a',1,'mdbx::txn::update()'],['../group__cxx__api.html#ga23a5e42a2a4b3765883143aa3fbe9a7e',1,'mdbx::cursor::update()']]],
  ['update_5freserve_1',['update_reserve',['../group__cxx__api.html#gab90cac3d642f62cf03a3594e7a051857',1,'mdbx::txn::update_reserve()'],['../group__cxx__api.html#gad70b28ea2cdb2f95f3aedc1fcca89f49',1,'mdbx::cursor::update_reserve()']]],
  ['upsert_2',['upsert',['../group__cxx__api.html#gad492368f234489527127e01c1e007d17',1,'mdbx::txn::upsert()'],['../group__cxx__api.html#ga2b2d15a496b76ef1a946c42b9ab7f0ef',1,'mdbx::cursor::upsert()']]],
  ['upsert_5freserve_3',['upsert_reserve',['../group__cxx__api.html#ga84eacf4ce9d0de109a6c29ee0fe13df5',1,'mdbx::txn::upsert_reserve()'],['../group__cxx__api.html#ga63fa1ffc2d64afa169d28370cfe6a0f1',1,'mdbx::cursor::upsert_reserve()']]]
];
