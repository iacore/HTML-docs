var searchData=
[
  ['range_20query_20estimation_0',['Range query estimation',['../group__c__rqest.html',1,'']]],
  ['reader_5finfo_1',['reader_info',['../structmdbx_1_1env_1_1reader__info.html',1,'mdbx::env::reader_info'],['../group__cxx__api.html#gafd1317bf3bb1261bfa01202435602ee0',1,'mdbx::env::reader_info::reader_info()']]],
  ['reader_5fslot_5fbusy_2',['reader_slot_busy',['../group__cxx__exceptions.html#ga8380d5b4db2bf3cb51182dc308591514',1,'mdbx']]],
  ['readonly_3',['readonly',['../group__cxx__api.html#a66d3e23137a274723052cb4d55485912aa5c227c6858f038ab52d0c3b73d5b84c',1,'mdbx::env']]],
  ['reclaiming_4',['reclaiming',['../structmdbx_1_1env_1_1operate__parameters.html#a9dff418d8fafaefe79f3ceac6cd87ea4',1,'mdbx::env::operate_parameters']]],
  ['reclaiming_5ffrom_5fflags_5',['reclaiming_from_flags',['../group__cxx__api.html#ga82982933871b14a4bcc9f2de7ec2caf8',1,'mdbx::env::operate_parameters']]],
  ['reclaiming_5foptions_6',['reclaiming_options',['../structmdbx_1_1env_1_1reclaiming__options.html',1,'mdbx::env::reclaiming_options'],['../structmdbx_1_1env_1_1reclaiming__options.html#a3da9d5bf44f82edfc72d35d2b6de9196',1,'mdbx::env::reclaiming_options::reclaiming_options(MDBX_env_flags_t) noexcept'],['../structmdbx_1_1env_1_1reclaiming__options.html#a8c1467a637f62a78d3865c66a5fb5166',1,'mdbx::env::reclaiming_options::reclaiming_options() noexcept'],['../structmdbx_1_1env_1_1reclaiming__options.html#ad5ee1e0fd9ce5982bf1a12e6a47d90b0',1,'mdbx::env::reclaiming_options::reclaiming_options(const reclaiming_options &amp;) noexcept=default']]],
  ['release_7',['release',['../group__c__api.html#a4e94c02e5ec71cb6fd1c2b9f90299d5b',1,'MDBX_version_info']]],
  ['remote_5fmedia_8',['remote_media',['../group__cxx__exceptions.html#ga627c1d5ed1cd62b0b8d9315f5893b32e',1,'mdbx']]],
  ['remove_9',['remove',['../group__cxx__api.html#a4913343403889c7b020ede3cf7c37b87',1,'mdbx::env::remove(const ::std::string &amp;pathname, const remove_mode mode=just_remove)'],['../group__cxx__api.html#ae81345466aa7fe94f382d30eef7d1345',1,'mdbx::env::remove(const MDBX_STD_FILESYSTEM_PATH &amp;pathname, const remove_mode mode=just_remove)'],['../group__cxx__api.html#a1a09d030ecc8e1862eb3676084c73044',1,'mdbx::env::remove(const ::std::wstring &amp;pathname, const remove_mode mode=just_remove)'],['../group__cxx__api.html#a99b81c8f46ad845447fe1546b2128087',1,'mdbx::env::remove(const wchar_t *pathname, const remove_mode mode=just_remove)'],['../group__cxx__api.html#a2b8ab900444fcbcf4f8ff06646900057',1,'mdbx::env::remove(const char *pathname, const remove_mode mode=just_remove)']]],
  ['remove_5fmode_10',['remove_mode',['../group__cxx__api.html#a903e64cb8e0eaca469560f403d509858',1,'mdbx::env']]],
  ['remove_5fprefix_11',['remove_prefix',['../group__cxx__api.html#aa37436bde6dce8d0c875435de8a55957',1,'mdbx::buffer::remove_prefix()'],['../group__cxx__api.html#ga9cd0982cb9fa6eed01a48fc05c8c7ec1',1,'mdbx::slice::remove_prefix(size_t n) noexcept']]],
  ['remove_5fsuffix_12',['remove_suffix',['../group__cxx__api.html#ga1c89ebdb77acb3f278b50c680e8b7d07',1,'mdbx::slice::remove_suffix()'],['../group__cxx__api.html#a585e78e440c430c0ddae81c9c90c51b1',1,'mdbx::buffer::remove_suffix()']]],
  ['renew_13',['renew',['../group__cxx__api.html#ga082200dc8dd1afa5c93d074fa8b266b6',1,'mdbx::cursor']]],
  ['renew_5freading_14',['renew_reading',['../group__cxx__api.html#ga5ac437cc68a3234b6b7cb44eb3dd4cd7',1,'mdbx::txn']]],
  ['replace_15',['replace',['../group__cxx__api.html#ga9bad59fabcebce0d3b961c8072ccdb30',1,'mdbx::txn::replace(map_handle map, const slice &amp;key, const slice &amp;new_value, const typename buffer&lt; ALLOCATOR, CAPACITY_POLICY &gt;::allocator_type &amp;allocator=buffer&lt; ALLOCATOR, CAPACITY_POLICY &gt;::allocator_type())'],['../group__cxx__api.html#ga08087d8b3af447aa2bbde857c743e9bd',1,'mdbx::txn::replace(map_handle map, const slice &amp;key, slice old_value, const slice &amp;new_value)']]],
  ['replace_5freserve_16',['replace_reserve',['../group__cxx__api.html#gaaf9c483fcef427f65ab82e64553b5650',1,'mdbx::txn']]],
  ['reservation_5fpolicy_17',['reservation_policy',['../group__cxx__api.html#a8f2e8cfc91b516243ebdd4b87ff28e43',1,'mdbx::buffer']]],
  ['reserve_18',['reserve',['../group__cxx__api.html#a1eb8f6e935b2c19a883c8e1370cf3fdf',1,'mdbx::buffer']]],
  ['reserve_5fheadroom_19',['reserve_headroom',['../group__cxx__api.html#a202767525b7633fcdbb923951790fe1a',1,'mdbx::buffer']]],
  ['reserve_5ftailroom_20',['reserve_tailroom',['../group__cxx__api.html#a1413ccf2668d4d5ef728b85a51c18154',1,'mdbx::buffer']]],
  ['reset_5freading_21',['reset_reading',['../group__cxx__api.html#ga0b5b68c455626d6a594c58c7c5d6babf',1,'mdbx::txn']]],
  ['rethrow_5fcaptured_22',['rethrow_captured',['../group__cxx__api.html#ga0cfd7a0cec8efdef187ec8ecf23d3101',1,'mdbx::exception_thunk']]],
  ['reverse_23',['reverse',['../group__cxx__api.html#ggac353595aa9d5601f1a5888b26761db04a4d9c2073afa3c2abb817dceb22c34de6',1,'mdbx']]],
  ['revision_24',['revision',['../group__c__api.html#a77b126b435d465e727b5461a61733903',1,'MDBX_version_info']]],
  ['robust_5fsynchronous_25',['robust_synchronous',['../group__cxx__api.html#a21b1c4fc9a921d3ff8885aef43099336ad7e0f6fd854c5f5999744e94b04da73f',1,'mdbx::env']]],
  ['round_26',['round',['../group__cxx__data.html#a20e4e392c120cd95585bb35ad7ade0e7',1,'mdbx::default_capacity_policy']]]
];
