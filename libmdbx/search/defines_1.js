var searchData=
[
  ['mdbx_5fassert_5fcxx20_5fconcept_5fsatisfied_0',['MDBX_ASSERT_CXX20_CONCEPT_SATISFIED',['../mdbx_8h_09_09.html#a371451a2bcbf45ce5e0cd2bc96454512',1,'mdbx.h++']]],
  ['mdbx_5fconstexpr_5fassert_1',['MDBX_CONSTEXPR_ASSERT',['../mdbx_8h_09_09.html#ad4e2da44a67b8556ed7d065da9af9386',1,'mdbx.h++']]],
  ['mdbx_5fcxx17_5fconstexpr_2',['MDBX_CXX17_CONSTEXPR',['../mdbx_8h_09_09.html#ac481c8c5dbfd2f48685ea99bcbc4c5e0',1,'mdbx.h++']]],
  ['mdbx_5fcxx17_5ffallthrough_3',['MDBX_CXX17_FALLTHROUGH',['../mdbx_8h_09_09.html#a2580391d17f3e2715ed751d3c1523c51',1,'mdbx.h++']]],
  ['mdbx_5fcxx20_5fconcept_4',['MDBX_CXX20_CONCEPT',['../mdbx_8h_09_09.html#a3c1eea65e3800966a7b6ce0fcc755aea',1,'mdbx.h++']]],
  ['mdbx_5fcxx20_5fconstexpr_5',['MDBX_CXX20_CONSTEXPR',['../mdbx_8h_09_09.html#a006b5a41dcea1c018efaa0b36fb94cc5',1,'mdbx.h++']]],
  ['mdbx_5fcxx20_5flikely_6',['MDBX_CXX20_LIKELY',['../mdbx_8h_09_09.html#ae9d4ad5b6de631cd6138b3e0c4e24e67',1,'mdbx.h++']]],
  ['mdbx_5fcxx20_5funlikely_7',['MDBX_CXX20_UNLIKELY',['../mdbx_8h_09_09.html#aa172d02bbe691be5b5d17f3634a117d5',1,'mdbx.h++']]],
  ['mdbx_5fdisable_5fgnu_5fsource_8',['MDBX_DISABLE_GNU_SOURCE',['../options_8h.html#a7176ae0513693ef6459a51783e36b4a6',1,'options.h']]],
  ['mdbx_5fif_5fconstexpr_9',['MDBX_IF_CONSTEXPR',['../mdbx_8h_09_09.html#adf33bc26891a3a7829da8ec71590f298',1,'mdbx.h++']]],
  ['mdbx_5flikely_10',['MDBX_LIKELY',['../mdbx_8h_09_09.html#a58289a14f228237d10a64d1e74587c0c',1,'mdbx.h++']]],
  ['mdbx_5funlikely_11',['MDBX_UNLIKELY',['../mdbx_8h_09_09.html#ab9acb957dc9252e591908ab8174a992c',1,'mdbx.h++']]]
];
