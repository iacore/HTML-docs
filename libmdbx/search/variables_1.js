var searchData=
[
  ['bad_5fmap_5fid_0',['bad_map_id',['../group__cxx__exceptions.html#ga3938baa2ebd54e5d9e1242cf50ee7961',1,'mdbx']]],
  ['bad_5ftransaction_1',['bad_transaction',['../group__cxx__exceptions.html#ga8bb116541c5ae31275f9b57659f56c27',1,'mdbx']]],
  ['bad_5fvalue_5fsize_2',['bad_value_size',['../group__cxx__exceptions.html#gaa4a8b26dcd75a4a23e4d7f48b27a863f',1,'mdbx']]],
  ['bytes_3',['bytes',['../structmdbx_1_1env_1_1geometry_1_1size.html#a99a981b9a905ad1b2658b68dd4747fe3',1,'mdbx::env::geometry::size']]],
  ['bytes_5fretained_4',['bytes_retained',['../structmdbx_1_1env_1_1reader__info.html#af64f7405502194e928947e603ca62f15',1,'mdbx::env::reader_info']]],
  ['bytes_5fused_5',['bytes_used',['../structmdbx_1_1env_1_1reader__info.html#a3c8bba2312c8b73bf34d369fdb340e21',1,'mdbx::env::reader_info']]]
];
