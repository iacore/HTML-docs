var searchData=
[
  ['pagesize_0',['pagesize',['../structmdbx_1_1env_1_1geometry.html#a45048bf2de9120d01dae2151c060d459',1,'mdbx::env::geometry']]],
  ['pagesize_5fmax_1',['pagesize_max',['../group__cxx__api.html#ga60544c86f38e792ca8164028c833583f',1,'mdbx::env::limits']]],
  ['pagesize_5fmin_2',['pagesize_min',['../group__cxx__api.html#gad5b9d94b98f432a7e168894337c92742',1,'mdbx::env::limits']]],
  ['pair_3',['pair',['../group__cxx__data.html#structmdbx_1_1pair',1,'mdbx::pair'],['../group__cxx__data.html#af4ed482b7b0024693cb4ba10c1a2d036',1,'mdbx::pair::pair(const slice &amp;key, const slice &amp;value) noexcept'],['../group__cxx__data.html#abdb51fead77d18d9b2be47ce77177a09',1,'mdbx::pair::pair(const pair &amp;) noexcept=default']]],
  ['pair_5fresult_4',['pair_result',['../group__cxx__data.html#structmdbx_1_1pair__result',1,'mdbx::pair_result'],['../group__cxx__data.html#a8870f6889f933a26bd4d34dd43cf92c2',1,'mdbx::pair_result::pair_result(const slice &amp;key, const slice &amp;value, bool done) noexcept'],['../group__cxx__data.html#a4e3003b7920ac1423246456a93923b3c',1,'mdbx::pair_result::pair_result(const pair_result &amp;) noexcept=default']]],
  ['pairsize4page_5fmax_5',['pairsize4page_max',['../group__cxx__api.html#ga48fe9085a451c56fa2222c15b89f4427',1,'mdbx::env::limits::pairsize4page_max(intptr_t pagesize, value_mode)'],['../group__cxx__api.html#ga410cb16e1f8968b4381abb676ec17225',1,'mdbx::env::limits::pairsize4page_max(const env &amp;, value_mode)'],['../group__cxx__api.html#gaa68b358b89140734e98d6c93af93c764',1,'mdbx::env::limits::pairsize4page_max(const env &amp;, MDBX_db_flags_t flags)'],['../group__cxx__api.html#ga7527318471ba71693b1853b85ec8e465',1,'mdbx::env::limits::pairsize4page_max(intptr_t pagesize, MDBX_db_flags_t flags)']]],
  ['panic_6',['panic',['../group__cxx__exceptions.html#a526885b14e8a019cf03b1f4ec0d8c05a',1,'mdbx::error']]],
  ['panic_5fon_5ffailure_7',['panic_on_failure',['../group__cxx__api.html#gad133742b9c84a22802ad3bdd44790da1',1,'mdbx::error::panic_on_failure(const char *context_where, const char *func_who) const noexcept'],['../group__cxx__api.html#ga6fbcaa05c7cc94d719557f63d03265fd',1,'mdbx::error::panic_on_failure(int error_code, const char *context_where, const char *func_who) noexcept']]],
  ['path_8',['path',['../group__cxx__api.html#ga799e7517341fd4b74ee0322e6535d48c',1,'mdbx']]],
  ['pb_9',['PB',['../structmdbx_1_1env_1_1geometry.html#ae4f041c107c53c3de7d3df8c4b6f0d71a6657d9be538d290ed9e6e2f697c4706d',1,'mdbx::env::geometry']]],
  ['permission_5fdenied_5for_5fnot_5fwriteable_10',['permission_denied_or_not_writeable',['../group__cxx__exceptions.html#gaa3f79596f23cee42eb291a70326fdb89',1,'mdbx']]],
  ['pettiness_5fthreshold_11',['pettiness_threshold',['../group__cxx__api.html#a37a48f42db9bca3e235e4fe97bfdf8fca0d5f6930bd0fdc8ecaa84a40aea57429',1,'mdbx::buffer::pettiness_threshold()'],['../group__cxx__data.html#a93d255ecdabb5016aa672b64a5af2d13a71f76d6edfd92dd0d62258032f28542f',1,'mdbx::default_capacity_policy::pettiness_threshold()']]],
  ['pib_12',['PiB',['../structmdbx_1_1env_1_1geometry.html#ae4f041c107c53c3de7d3df8c4b6f0d71abb0ff2729576f962ed62664d89c0a7d0',1,'mdbx::env::geometry']]],
  ['pid_13',['pid',['../structmdbx_1_1env_1_1reader__info.html#aefef77baeaa3cc83a4988b76a885feb1',1,'mdbx::env::reader_info']]],
  ['poll_5fsync_5fto_5fdisk_14',['poll_sync_to_disk',['../group__cxx__api.html#a97644bb1dfc3ded8f70d8c1d924296d0',1,'mdbx::env']]],
  ['polymorphic_5fallocator_15',['polymorphic_allocator',['../group__cxx__api.html#ga2fd4b7928a53c363af99fb051ccb2960',1,'mdbx']]],
  ['preparation_16',['preparation',['../group__c__api.html#aa2e2b38a085e90af171c8561e8f0e9e9',1,'MDBX_commit_latency']]],
  ['prepare_5fread_17',['prepare_read',['../group__cxx__api.html#ga9345f65285fc7730489edebb81983263',1,'mdbx::env']]],
  ['previous_18',['previous',['../group__cxx__api.html#abac5f7d4c71d8424cffea5a19925852daa87699467ce3f647d15c395a09983ce6',1,'mdbx::cursor']]],
  ['ptr_5f_19',['ptr_',['../structmdbx_1_1buffer_1_1silo_1_1bin_1_1allocated.html#a8a8376a6625411885b57e05ba232c269',1,'mdbx::buffer::silo::bin::allocated']]],
  ['put_20',['put',['../group__cxx__api.html#gad0523038c051044c70c5ee7dc633ff1d',1,'mdbx::txn::put(map_handle map, const slice &amp;key, slice *value, MDBX_put_flags_t flags) noexcept'],['../group__cxx__api.html#ga47b217817b6798bf139fcc73d748bf4c',1,'mdbx::txn::put(map_handle map, const slice &amp;key, slice value, put_mode mode)'],['../group__cxx__api.html#ga92f36260e3cfc3bffe223bc16f4f4502',1,'mdbx::cursor::put()']]],
  ['put_5fcanary_21',['put_canary',['../group__cxx__api.html#ga84b238f1e11bb0c516e27651c459b5b7',1,'mdbx::txn']]],
  ['put_5fmode_22',['put_mode',['../group__cxx__api.html#gaa31b829631ada2e626335edb8eab32c8',1,'mdbx']]],
  ['put_5fmultiple_23',['put_multiple',['../group__cxx__api.html#gae1a3dc465a5d800e79550a5629ff1ef0',1,'mdbx::txn::put_multiple(map_handle map, const slice &amp;key, const size_t value_length, const void *values_array, size_t values_count, put_mode mode, bool allow_partial=false)'],['../group__cxx__api.html#af9ef95cdbafc46860f82c95d6be23eff',1,'mdbx::txn::put_multiple(map_handle map, const slice &amp;key, const ::std::vector&lt; VALUE &gt; &amp;vector, put_mode mode)']]]
];
