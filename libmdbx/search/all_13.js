var searchData=
[
  ['update_0',['update',['../group__cxx__api.html#gac0a3bfc1c3715f615a95870115c19d8a',1,'mdbx::txn::update()'],['../group__cxx__api.html#ga23a5e42a2a4b3765883143aa3fbe9a7e',1,'mdbx::cursor::update()'],['../group__cxx__api.html#ggaa31b829631ada2e626335edb8eab32c8aa53910e84062b1d8c2a09e6009034ffb',1,'mdbx::update()']]],
  ['update_5freserve_1',['update_reserve',['../group__cxx__api.html#gab90cac3d642f62cf03a3594e7a051857',1,'mdbx::txn::update_reserve()'],['../group__cxx__api.html#gad70b28ea2cdb2f95f3aedc1fcca89f49',1,'mdbx::cursor::update_reserve()']]],
  ['uppercase_2',['uppercase',['../group__cxx__data.html#ab335d3d9892801523e221b974e479485',1,'mdbx::to_hex']]],
  ['upsert_3',['upsert',['../group__cxx__api.html#gad492368f234489527127e01c1e007d17',1,'mdbx::txn::upsert()'],['../group__cxx__api.html#ga2b2d15a496b76ef1a946c42b9ab7f0ef',1,'mdbx::cursor::upsert()'],['../group__cxx__api.html#ggaa31b829631ada2e626335edb8eab32c8a8c16d319effaf8fc5cb733d31e2f557a',1,'mdbx::upsert()']]],
  ['upsert_5freserve_4',['upsert_reserve',['../group__cxx__api.html#ga84eacf4ce9d0de109a6c29ee0fe13df5',1,'mdbx::txn::upsert_reserve()'],['../group__cxx__api.html#ga63fa1ffc2d64afa169d28370cfe6a0f1',1,'mdbx::cursor::upsert_reserve()']]],
  ['usage_5',['Usage',['../md_usage.html',1,'']]],
  ['usage_2emd_6',['usage.md',['../usage_8md.html',1,'']]],
  ['use_5fsubdirectory_7',['use_subdirectory',['../structmdbx_1_1env__managed_1_1create__parameters.html#ae1d9013b3bb9123b8e26a1391a785e5d',1,'mdbx::env_managed::create_parameters']]],
  ['usual_8',['usual',['../group__cxx__api.html#ggac353595aa9d5601f1a5888b26761db04a0ec341f6f51f9929953c705d71251c0c',1,'mdbx']]]
];
