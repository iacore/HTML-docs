var searchData=
[
  ['last_0',['last',['../group__cxx__api.html#abac5f7d4c71d8424cffea5a19925852daaa38d8e2910196b9e1f6c71bfe3b11b3',1,'mdbx::cursor']]],
  ['lastbyte_1',['lastbyte',['../unionmdbx_1_1buffer_1_1silo_1_1bin.html#ae99ea90a01170d30032b63d3062782fb',1,'mdbx::buffer::silo::bin::lastbyte() const noexcept'],['../unionmdbx_1_1buffer_1_1silo_1_1bin.html#a0740ed10367d3dd0dc395c421b851513',1,'mdbx::buffer::silo::bin::lastbyte() noexcept']]],
  ['lazy_5fweak_5ftail_2',['lazy_weak_tail',['../group__cxx__api.html#a21b1c4fc9a921d3ff8885aef43099336a038a39d79ccf3740408d13ff2c6706aa',1,'mdbx::env']]],
  ['le_5flastbyte_5fmask_3',['le_lastbyte_mask',['../unionmdbx_1_1buffer_1_1silo_1_1bin.html#a7975d3ec97f2afe9fb3817ba4c851b70a7e25d799ec2e21363e7bde6711664371',1,'mdbx::buffer::silo::bin']]],
  ['legacy_5fallocator_4',['legacy_allocator',['../group__cxx__api.html#ga12630ae3f943acfb6834e2b2049fa1c1',1,'mdbx']]],
  ['length_5',['length',['../group__cxx__api.html#ga1f22e5234dd01576e39e30910c6908e6',1,'mdbx::slice::length()'],['../group__cxx__api.html#ab7f6c9ecbce0e6cc73c1f08f6397d5c7',1,'mdbx::buffer::length()']]],
  ['libmdbx_5fapi_6',['LIBMDBX_API',['../group__c__api.html#ga3ff81c5c75bd3d7836f03a05649d5e8f',1,'mdbx.h']]],
  ['libmdbx_5fapi_5ftype_7',['LIBMDBX_API_TYPE',['../group__c__api.html#ga570e05a7ca62d5ad7496b4690cd77871',1,'mdbx.h']]],
  ['libmdbx_5fh_8',['LIBMDBX_H',['../mdbx_8h.html#ab6255400430198475f79728c0673aa65',1,'mdbx.h']]],
  ['libmdbx_5finline_5fapi_9',['LIBMDBX_INLINE_API',['../group__api__macros.html#gad3bb6df5174b93f760bd5d928bca3f2e',1,'mdbx.h']]],
  ['libmdbx_5fverinfo_5fapi_10',['LIBMDBX_VERINFO_API',['../group__c__api.html#gafd01246bf5e9bf5e540a0db877043f0e',1,'mdbx.h']]],
  ['lifo_11',['lifo',['../structmdbx_1_1env_1_1reclaiming__options.html#af71204a32d62c11f34665edebf864259',1,'mdbx::env::reclaiming_options']]],
  ['limits_12',['limits',['../structmdbx_1_1env_1_1limits.html#a56762c371ba98ca73be06409c4e53508',1,'mdbx::env::limits::limits()'],['../structmdbx_1_1env_1_1limits.html',1,'mdbx::env::limits']]],
  ['logging_20and_20runtime_20debug_13',['Logging and runtime debug',['../group__c__debug.html',1,'']]],
  ['loop_5fcontrol_14',['loop_control',['../group__cxx__api.html#gabccc2fb093eebf6e0c622f620b4ff6f7',1,'mdbx']]],
  ['lower_5fbound_15',['lower_bound',['../group__cxx__api.html#gaae9104548778015e0af83e6ceb6f550a',1,'mdbx::cursor']]],
  ['lower_5fbound_5fmultivalue_16',['lower_bound_multivalue',['../group__cxx__api.html#ga44c4c66afa585874db9c07df99d40f46',1,'mdbx::cursor']]]
];
