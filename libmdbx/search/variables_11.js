var searchData=
[
  ['target_0',['target',['../group__c__api.html#ac63daab4e4e383a721701fbf6ab66bf5',1,'MDBX_build_info']]],
  ['thread_1',['thread',['../structmdbx_1_1env_1_1reader__info.html#a08f041a919dc7f6a2bda53636da5458b',1,'mdbx::env::reader_info']]],
  ['thread_5fmismatch_2',['thread_mismatch',['../group__cxx__exceptions.html#gae4a2402eebb90c769547798a2c7de758',1,'mdbx']]],
  ['transaction_5ffull_3',['transaction_full',['../group__cxx__exceptions.html#ga262dc6d7f20a6a81c8562f098e02e5b0',1,'mdbx']]],
  ['transaction_5fid_4',['transaction_id',['../structmdbx_1_1env_1_1reader__info.html#a4224cbb41bebdd19c2ed93392e6c20cb',1,'mdbx::env::reader_info']]],
  ['transaction_5flag_5',['transaction_lag',['../structmdbx_1_1env_1_1reader__info.html#a15d024d3a0388adae28db43ae11efa95',1,'mdbx::env::reader_info']]],
  ['transaction_5foverlapping_6',['transaction_overlapping',['../group__cxx__exceptions.html#gacc2ef1126ff7944e87cc3f349a1ffff0',1,'mdbx']]],
  ['txn_5fid_7',['txn_id',['../group__c__api.html#a8fa3b9f102114b6a412335cbe2c0802e',1,'MDBX_txn_info']]],
  ['txn_5freader_5flag_8',['txn_reader_lag',['../group__c__api.html#a383fe4a97f9108000d5c69590738f8ad',1,'MDBX_txn_info']]],
  ['txn_5fspace_5fdirty_9',['txn_space_dirty',['../group__c__api.html#a0a13745cd97c277cd810f3bbf60c0d9f',1,'MDBX_txn_info']]],
  ['txn_5fspace_5fleftover_10',['txn_space_leftover',['../group__c__api.html#a461b926cfd0e656bebac9bab5997d6b1',1,'MDBX_txn_info']]],
  ['txn_5fspace_5flimit_5fhard_11',['txn_space_limit_hard',['../group__c__api.html#aa93c141047f682507850e6f7a08f5557',1,'MDBX_txn_info']]],
  ['txn_5fspace_5flimit_5fsoft_12',['txn_space_limit_soft',['../group__c__api.html#a8aa8674b1b08e6f956d893c02c1b4514',1,'MDBX_txn_info']]],
  ['txn_5fspace_5fretired_13',['txn_space_retired',['../group__c__api.html#ac8a5a3e9a6f4bfba24ca9dd8d957030f',1,'MDBX_txn_info']]],
  ['txn_5fspace_5fused_14',['txn_space_used',['../group__c__api.html#a48515214b62493d820cbafda219bcc48',1,'MDBX_txn_info']]]
];
