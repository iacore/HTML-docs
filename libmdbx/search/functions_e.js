var searchData=
[
  ['reader_5finfo_0',['reader_info',['../group__cxx__api.html#gafd1317bf3bb1261bfa01202435602ee0',1,'mdbx::env::reader_info']]],
  ['reclaiming_5ffrom_5fflags_1',['reclaiming_from_flags',['../group__cxx__api.html#ga82982933871b14a4bcc9f2de7ec2caf8',1,'mdbx::env::operate_parameters']]],
  ['reclaiming_5foptions_2',['reclaiming_options',['../structmdbx_1_1env_1_1reclaiming__options.html#a8c1467a637f62a78d3865c66a5fb5166',1,'mdbx::env::reclaiming_options::reclaiming_options() noexcept'],['../structmdbx_1_1env_1_1reclaiming__options.html#ad5ee1e0fd9ce5982bf1a12e6a47d90b0',1,'mdbx::env::reclaiming_options::reclaiming_options(const reclaiming_options &amp;) noexcept=default'],['../structmdbx_1_1env_1_1reclaiming__options.html#a3da9d5bf44f82edfc72d35d2b6de9196',1,'mdbx::env::reclaiming_options::reclaiming_options(MDBX_env_flags_t) noexcept']]],
  ['remove_3',['remove',['../group__cxx__api.html#ae81345466aa7fe94f382d30eef7d1345',1,'mdbx::env::remove(const MDBX_STD_FILESYSTEM_PATH &amp;pathname, const remove_mode mode=just_remove)'],['../group__cxx__api.html#a1a09d030ecc8e1862eb3676084c73044',1,'mdbx::env::remove(const ::std::wstring &amp;pathname, const remove_mode mode=just_remove)'],['../group__cxx__api.html#a99b81c8f46ad845447fe1546b2128087',1,'mdbx::env::remove(const wchar_t *pathname, const remove_mode mode=just_remove)'],['../group__cxx__api.html#a4913343403889c7b020ede3cf7c37b87',1,'mdbx::env::remove(const ::std::string &amp;pathname, const remove_mode mode=just_remove)'],['../group__cxx__api.html#a2b8ab900444fcbcf4f8ff06646900057',1,'mdbx::env::remove(const char *pathname, const remove_mode mode=just_remove)']]],
  ['remove_5fprefix_4',['remove_prefix',['../group__cxx__api.html#ga9cd0982cb9fa6eed01a48fc05c8c7ec1',1,'mdbx::slice::remove_prefix()'],['../group__cxx__api.html#aa37436bde6dce8d0c875435de8a55957',1,'mdbx::buffer::remove_prefix()']]],
  ['remove_5fsuffix_5',['remove_suffix',['../group__cxx__api.html#ga1c89ebdb77acb3f278b50c680e8b7d07',1,'mdbx::slice::remove_suffix()'],['../group__cxx__api.html#a585e78e440c430c0ddae81c9c90c51b1',1,'mdbx::buffer::remove_suffix()']]],
  ['renew_6',['renew',['../group__cxx__api.html#ga082200dc8dd1afa5c93d074fa8b266b6',1,'mdbx::cursor']]],
  ['renew_5freading_7',['renew_reading',['../group__cxx__api.html#ga5ac437cc68a3234b6b7cb44eb3dd4cd7',1,'mdbx::txn']]],
  ['replace_8',['replace',['../group__cxx__api.html#ga08087d8b3af447aa2bbde857c743e9bd',1,'mdbx::txn::replace(map_handle map, const slice &amp;key, slice old_value, const slice &amp;new_value)'],['../group__cxx__api.html#ga9bad59fabcebce0d3b961c8072ccdb30',1,'mdbx::txn::replace(map_handle map, const slice &amp;key, const slice &amp;new_value, const typename buffer&lt; ALLOCATOR, CAPACITY_POLICY &gt;::allocator_type &amp;allocator=buffer&lt; ALLOCATOR, CAPACITY_POLICY &gt;::allocator_type())']]],
  ['replace_5freserve_9',['replace_reserve',['../group__cxx__api.html#gaaf9c483fcef427f65ab82e64553b5650',1,'mdbx::txn']]],
  ['reserve_10',['reserve',['../group__cxx__api.html#a1eb8f6e935b2c19a883c8e1370cf3fdf',1,'mdbx::buffer']]],
  ['reserve_5fheadroom_11',['reserve_headroom',['../group__cxx__api.html#a202767525b7633fcdbb923951790fe1a',1,'mdbx::buffer']]],
  ['reserve_5ftailroom_12',['reserve_tailroom',['../group__cxx__api.html#a1413ccf2668d4d5ef728b85a51c18154',1,'mdbx::buffer']]],
  ['reset_5freading_13',['reset_reading',['../group__cxx__api.html#ga0b5b68c455626d6a594c58c7c5d6babf',1,'mdbx::txn']]],
  ['rethrow_5fcaptured_14',['rethrow_captured',['../group__cxx__api.html#ga0cfd7a0cec8efdef187ec8ecf23d3101',1,'mdbx::exception_thunk']]],
  ['round_15',['round',['../group__cxx__data.html#a20e4e392c120cd95585bb35ad7ade0e7',1,'mdbx::default_capacity_policy']]]
];
