var searchData=
[
  ['wait_5ffor_5funused_0',['wait_for_unused',['../group__cxx__api.html#a903e64cb8e0eaca469560f403d509858a78c4dbcc2ad92529dc8040940fa52172',1,'mdbx::env']]],
  ['what_1',['what',['../group__cxx__exceptions.html#a75485efc39aeca9ff83ff6e0788907ee',1,'mdbx::error']]],
  ['whole_2',['whole',['../group__c__api.html#aa97dacaedc16f1c31df6888096e63809',1,'MDBX_commit_latency']]],
  ['whole_5ffragile_3',['whole_fragile',['../group__cxx__api.html#a21b1c4fc9a921d3ff8885aef43099336aef21f29e5ff6ebba116f8208a2f1ff20',1,'mdbx::env']]],
  ['wrap_4',['wrap',['../group__cxx__data.html#a36cc25297ca7909254e0ad12dcdf94c1',1,'mdbx::slice::wrap(const char(&amp;text)[SIZE])'],['../group__cxx__data.html#af1136ec23d80c54d9b9b9efa9eddc5c0',1,'mdbx::slice::wrap(const POD &amp;pod)'],['../group__cxx__api.html#aa2826a12309f0483393de56a2db2b544',1,'mdbx::buffer::wrap()']]],
  ['wrap_5fwidth_5',['wrap_width',['../group__cxx__data.html#aed4cc244c3c7a4f180fddd3b5babad3b',1,'mdbx::to_base64::wrap_width()'],['../group__cxx__data.html#ae8af87fab243c5a342bd276f4949ffb7',1,'mdbx::to_base58::wrap_width()'],['../group__cxx__data.html#a56ebeda48e4a733ca11bcc2f701444c7',1,'mdbx::to_hex::wrap_width()']]],
  ['write_6',['write',['../group__c__api.html#ac8928e8af622cc1e7b2b28ceae44a013',1,'MDBX_commit_latency']]],
  ['write_5fbytes_7',['write_bytes',['../group__cxx__data.html#aa3824c315eca31836238cbe12c08888e',1,'mdbx::to_hex::write_bytes()'],['../group__cxx__data.html#afdd9a40d56b2237e83cb0eaa360aff4b',1,'mdbx::to_base58::write_bytes()'],['../group__cxx__data.html#af2ea58fe81b53c3c108f09360b2561af',1,'mdbx::to_base64::write_bytes()'],['../group__cxx__data.html#acdcb483d3a6209ecec22bc996cea6b59',1,'mdbx::from_hex::write_bytes()'],['../group__cxx__data.html#a5ea2655ae544feeb3f9e675b05f30745',1,'mdbx::from_base58::write_bytes()'],['../group__cxx__data.html#a3b736ac0a82b59700b2aa9dfbf58560f',1,'mdbx::from_base64::write_bytes()']]],
  ['write_5ffile_5fio_8',['write_file_io',['../group__cxx__api.html#a66d3e23137a274723052cb4d55485912ae938247fca29e9de3183ed62fdda4c62',1,'mdbx::env']]],
  ['write_5fmapped_5fio_9',['write_mapped_io',['../group__cxx__api.html#a66d3e23137a274723052cb4d55485912a4daf9ddda3049cc8149476b9c95dafac',1,'mdbx::env']]]
];
