var searchData=
[
  ['reader_5fslot_5fbusy_0',['reader_slot_busy',['../group__cxx__exceptions.html#ga8380d5b4db2bf3cb51182dc308591514',1,'mdbx']]],
  ['reclaiming_1',['reclaiming',['../structmdbx_1_1env_1_1operate__parameters.html#a9dff418d8fafaefe79f3ceac6cd87ea4',1,'mdbx::env::operate_parameters']]],
  ['release_2',['release',['../group__c__api.html#a4e94c02e5ec71cb6fd1c2b9f90299d5b',1,'MDBX_version_info']]],
  ['remote_5fmedia_3',['remote_media',['../group__cxx__exceptions.html#ga627c1d5ed1cd62b0b8d9315f5893b32e',1,'mdbx']]],
  ['revision_4',['revision',['../group__c__api.html#a77b126b435d465e727b5461a61733903',1,'MDBX_version_info']]]
];
