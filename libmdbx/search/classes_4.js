var searchData=
[
  ['env_0',['env',['../group__cxx__api.html#classmdbx_1_1env',1,'mdbx']]],
  ['env_5fmanaged_1',['env_managed',['../group__cxx__api.html#classmdbx_1_1env__managed',1,'mdbx']]],
  ['error_2',['error',['../group__cxx__exceptions.html#classmdbx_1_1error',1,'mdbx']]],
  ['exception_3',['exception',['../group__cxx__exceptions.html#classmdbx_1_1exception',1,'mdbx']]],
  ['exception_5fthunk_4',['exception_thunk',['../group__cxx__exceptions.html#classmdbx_1_1exception__thunk',1,'mdbx']]]
];
