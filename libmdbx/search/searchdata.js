var indexSectionsWithContent =
{
  0: "abcdefghijklmnoprstuvwxyz~",
  1: "abcdefghilmoprstv",
  2: "ms",
  3: "cimou",
  4: "abcdefghiklmoprstuvw~",
  5: "abcdefghiklmnoprstuvwxyz",
  6: "abcfilmprsv",
  7: "dklmprv",
  8: "bcdefghijklmnoprstuw",
  9: "ceot",
  10: "lm",
  11: "bcdeklorstv",
  12: "cdiou",
  13: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "groups",
  12: "pages",
  13: "concepts"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Modules",
  12: "Pages",
  13: "Concepts"
};

