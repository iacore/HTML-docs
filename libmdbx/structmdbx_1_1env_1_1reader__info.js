var structmdbx_1_1env_1_1reader__info =
[
    [ "reader_info", "group__cxx__api.html#gafd1317bf3bb1261bfa01202435602ee0", null ],
    [ "bytes_retained", "structmdbx_1_1env_1_1reader__info.html#af64f7405502194e928947e603ca62f15", null ],
    [ "bytes_used", "structmdbx_1_1env_1_1reader__info.html#a3c8bba2312c8b73bf34d369fdb340e21", null ],
    [ "pid", "structmdbx_1_1env_1_1reader__info.html#aefef77baeaa3cc83a4988b76a885feb1", null ],
    [ "slot", "structmdbx_1_1env_1_1reader__info.html#ad4f16ee1d59c595ae989c2afe1642630", null ],
    [ "thread", "structmdbx_1_1env_1_1reader__info.html#a08f041a919dc7f6a2bda53636da5458b", null ],
    [ "transaction_id", "structmdbx_1_1env_1_1reader__info.html#a4224cbb41bebdd19c2ed93392e6c20cb", null ],
    [ "transaction_lag", "structmdbx_1_1env_1_1reader__info.html#a15d024d3a0388adae28db43ae11efa95", null ]
];