var concepts =
[
    [ "mdbx", "namespacemdbx.html", [
      [ "MutableByteProducer", "conceptmdbx_1_1_mutable_byte_producer.html", null ],
      [ "ImmutableByteProducer", "conceptmdbx_1_1_immutable_byte_producer.html", null ],
      [ "SliceTranscoder", "conceptmdbx_1_1_slice_transcoder.html", null ]
    ] ]
];