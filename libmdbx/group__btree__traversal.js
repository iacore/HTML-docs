var group__btree__traversal =
[
    [ "MDBX_PGWALK_GC", "group__btree__traversal.html#ga67685b05b793906addc462320387b22d", null ],
    [ "MDBX_PGWALK_MAIN", "group__btree__traversal.html#ga5a9eb071b1245f1dd1e7b71994f97ff5", null ],
    [ "MDBX_PGWALK_META", "group__btree__traversal.html#gaf45421fc87559bd261ca4b9dd88a792e", null ],
    [ "MDBX_page_type_t", "group__btree__traversal.html#ga9031e966b345d74d3aaebddcf21dcef1", null ],
    [ "MDBX_pgvisitor_func", "group__btree__traversal.html#ga5c7f54c9ac6f3a1b422475a13f6b0987", null ],
    [ "MDBX_page_type_t", "group__btree__traversal.html#ga5c7b55f6338d1c01a3ad3c756672d741", [
      [ "MDBX_page_broken", "group__btree__traversal.html#gga5c7b55f6338d1c01a3ad3c756672d741a742786634caf4fb397fe4156a19b8f3a", null ],
      [ "MDBX_page_meta", "group__btree__traversal.html#gga5c7b55f6338d1c01a3ad3c756672d741af61691612a6a58336fa6d8dca6c73ebf", null ],
      [ "MDBX_page_large", "group__btree__traversal.html#gga5c7b55f6338d1c01a3ad3c756672d741a639ec5faef9079064ab71f1d4b505dd1", null ],
      [ "MDBX_page_branch", "group__btree__traversal.html#gga5c7b55f6338d1c01a3ad3c756672d741aaa19e73f3977e088acb0015d10697207", null ],
      [ "MDBX_page_leaf", "group__btree__traversal.html#gga5c7b55f6338d1c01a3ad3c756672d741aa65b260a41786e19d9221f0b49be4e95", null ],
      [ "MDBX_page_dupfixed_leaf", "group__btree__traversal.html#gga5c7b55f6338d1c01a3ad3c756672d741ae98ed0fd57e365689e0475eedda44b50", null ],
      [ "MDBX_subpage_leaf", "group__btree__traversal.html#gga5c7b55f6338d1c01a3ad3c756672d741a4a4e963265a3268e0321888952466760", null ],
      [ "MDBX_subpage_dupfixed_leaf", "group__btree__traversal.html#gga5c7b55f6338d1c01a3ad3c756672d741ad59a48d5358a1a66cb0a69e041975723", null ],
      [ "MDBX_subpage_broken", "group__btree__traversal.html#gga5c7b55f6338d1c01a3ad3c756672d741abe0a832d1678847d0236ba5984556398", null ]
    ] ],
    [ "mdbx_env_open_for_recovery", "group__btree__traversal.html#gad919724e8d6d833c75a1bd5edba32780", null ],
    [ "mdbx_env_pgwalk", "group__btree__traversal.html#ga86ade5c52bd1f08bf5acb8371ca98d6a", null ],
    [ "mdbx_env_turn_for_recovery", "group__btree__traversal.html#gabb2cabe20c67fbefd929903679ad5abf", null ]
];