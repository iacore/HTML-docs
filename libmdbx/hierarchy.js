var hierarchy =
[
    [ "mdbx::buffer< ALLOCATOR, CAPACITY_POLICY >::silo::bin::allocated", "structmdbx_1_1buffer_1_1silo_1_1bin_1_1allocated.html", null ],
    [ "mdbx::buffer< ALLOCATOR, CAPACITY_POLICY >::silo::bin", "unionmdbx_1_1buffer_1_1silo_1_1bin.html", null ],
    [ "mdbx::buffer< ALLOCATOR, CAPACITY_POLICY >", "group__cxx__api.html#classmdbx_1_1buffer", null ],
    [ "mdbx::allocation_aware_details::copy_assign_alloc< T, A, PoCCA >", "namespacemdbx_1_1allocation__aware__details.html#structmdbx_1_1allocation__aware__details_1_1copy__assign__alloc", null ],
    [ "mdbx::env_managed::create_parameters", "structmdbx_1_1env__managed_1_1create__parameters.html", null ],
    [ "mdbx::cursor", "group__cxx__api.html#classmdbx_1_1cursor", [
      [ "mdbx::cursor_managed", "group__cxx__api.html#classmdbx_1_1cursor__managed", null ]
    ] ],
    [ "mdbx::default_capacity_policy", "group__cxx__data.html#structmdbx_1_1default__capacity__policy", null ],
    [ "mdbx::env", "group__cxx__api.html#classmdbx_1_1env", [
      [ "mdbx::env_managed", "group__cxx__api.html#classmdbx_1_1env__managed", null ]
    ] ],
    [ "mdbx::error", "group__cxx__exceptions.html#classmdbx_1_1error", null ],
    [ "std::exception", null, [
      [ "std::runtime_error", null, [
        [ "mdbx::exception", "group__cxx__exceptions.html#classmdbx_1_1exception", [
          [ "mdbx::fatal", "group__cxx__exceptions.html#classmdbx_1_1fatal", null ]
        ] ]
      ] ]
    ] ],
    [ "mdbx::exception_thunk", "group__cxx__exceptions.html#classmdbx_1_1exception__thunk", null ],
    [ "mdbx::from_base58", "group__cxx__data.html#structmdbx_1_1from__base58", null ],
    [ "mdbx::from_base64", "group__cxx__data.html#structmdbx_1_1from__base64", null ],
    [ "mdbx::from_hex", "group__cxx__data.html#structmdbx_1_1from__hex", null ],
    [ "mdbx::env::geometry", "structmdbx_1_1env_1_1geometry.html", null ],
    [ "std::hash<::mdbx::slice >", "group__cxx__api.html#structstd_1_1hash_3_1_1mdbx_1_1slice_01_4", null ],
    [ "mdbx::map_handle::info", "structmdbx_1_1map__handle_1_1info.html", null ],
    [ "mdbx::env::limits", "structmdbx_1_1env_1_1limits.html", null ],
    [ "mdbx::map_handle", "group__cxx__api.html#structmdbx_1_1map__handle", null ],
    [ "MDBX_build_info", "group__c__api.html#struct_m_d_b_x__build__info", null ],
    [ "MDBX_canary", "group__c__api.html#struct_m_d_b_x__canary", null ],
    [ "MDBX_commit_latency", "group__c__api.html#struct_m_d_b_x__commit__latency", null ],
    [ "MDBX_envinfo", "group__c__api.html#struct_m_d_b_x__envinfo", null ],
    [ "MDBX_envinfo.mi_bootid", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid", null ],
    [ "MDBX_envinfo.mi_bootid.current", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8current", null ],
    [ "MDBX_envinfo.mi_bootid.meta0", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta0", null ],
    [ "MDBX_envinfo.mi_bootid.meta1", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta1", null ],
    [ "MDBX_envinfo.mi_bootid.meta2", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta2", null ],
    [ "MDBX_envinfo.mi_geo", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__geo", null ],
    [ "MDBX_envinfo.mi_pgop_stat", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__pgop__stat", null ],
    [ "MDBX_stat", "group__c__api.html#struct_m_d_b_x__stat", null ],
    [ "MDBX_txn_info", "group__c__api.html#struct_m_d_b_x__txn__info", null ],
    [ "MDBX_val", null, [
      [ "mdbx::slice", "group__cxx__data.html#structmdbx_1_1slice", null ]
    ] ],
    [ "MDBX_version_info", "group__c__api.html#struct_m_d_b_x__version__info", null ],
    [ "MDBX_version_info.git", "group__c__api.html#struct_m_d_b_x__version__info_8git", null ],
    [ "mdbx::allocation_aware_details::move_assign_alloc< T, A, PoCMA >", "namespacemdbx_1_1allocation__aware__details.html#structmdbx_1_1allocation__aware__details_1_1move__assign__alloc", null ],
    [ "mdbx::env::operate_options", "structmdbx_1_1env_1_1operate__options.html", null ],
    [ "mdbx::env::operate_parameters", "structmdbx_1_1env_1_1operate__parameters.html", null ],
    [ "mdbx::pair", "group__cxx__data.html#structmdbx_1_1pair", [
      [ "mdbx::pair_result", "group__cxx__data.html#structmdbx_1_1pair__result", [
        [ "mdbx::cursor::move_result", "structmdbx_1_1cursor_1_1move__result.html", null ]
      ] ]
    ] ],
    [ "mdbx::env::reader_info", "structmdbx_1_1env_1_1reader__info.html", null ],
    [ "mdbx::env::reclaiming_options", "structmdbx_1_1env_1_1reclaiming__options.html", null ],
    [ "mdbx::env::geometry::size", "structmdbx_1_1env_1_1geometry_1_1size.html", null ],
    [ "mdbx::allocation_aware_details::swap_alloc< T, A, PoCS >", "namespacemdbx_1_1allocation__aware__details.html#structmdbx_1_1allocation__aware__details_1_1swap__alloc", null ],
    [ "mdbx::to_base58", "group__cxx__data.html#structmdbx_1_1to__base58", null ],
    [ "mdbx::to_base64", "group__cxx__data.html#structmdbx_1_1to__base64", null ],
    [ "mdbx::to_hex", "group__cxx__data.html#structmdbx_1_1to__hex", null ],
    [ "mdbx::txn", "group__cxx__api.html#classmdbx_1_1txn", [
      [ "mdbx::txn_managed", "group__cxx__api.html#classmdbx_1_1txn__managed", null ]
    ] ],
    [ "mdbx::value_result", "group__cxx__data.html#structmdbx_1_1value__result", null ]
];