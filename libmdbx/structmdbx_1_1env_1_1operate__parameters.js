var structmdbx_1_1env_1_1operate__parameters =
[
    [ "operate_parameters", "structmdbx_1_1env_1_1operate__parameters.html#a2df142a8b991a7c56bca0fd477aeea39", null ],
    [ "operate_parameters", "structmdbx_1_1env_1_1operate__parameters.html#a1eee0733f85732f7cd03e97b602184a9", null ],
    [ "operate_parameters", "structmdbx_1_1env_1_1operate__parameters.html#a4169245325eba7f1e5f1490b6bdc4f67", null ],
    [ "make_flags", "structmdbx_1_1env_1_1operate__parameters.html#afa4ee36c77dc343ece8ede93cb628cff", null ],
    [ "operator=", "structmdbx_1_1env_1_1operate__parameters.html#a220a3dc9785a324ff12ef3fa98905b51", null ],
    [ "durability", "structmdbx_1_1env_1_1operate__parameters.html#a0adfcbcb58a7a764b34decc59c5c6572", null ],
    [ "max_maps", "structmdbx_1_1env_1_1operate__parameters.html#abf8fed3703f3b24903387b90a3cf96c3", null ],
    [ "max_readers", "structmdbx_1_1env_1_1operate__parameters.html#a8d739b34287e1f6488f2e28b0811420f", null ],
    [ "mode", "structmdbx_1_1env_1_1operate__parameters.html#aa623af7152b4a7d9b39a32cbe50495ad", null ],
    [ "options", "structmdbx_1_1env_1_1operate__parameters.html#a1af581bbe6716fb73cf3d255250e5240", null ],
    [ "reclaiming", "structmdbx_1_1env_1_1operate__parameters.html#a9dff418d8fafaefe79f3ceac6cd87ea4", null ]
];