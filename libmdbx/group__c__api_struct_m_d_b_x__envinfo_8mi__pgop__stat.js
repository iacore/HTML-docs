var group__c__api_struct_m_d_b_x__envinfo_8mi__pgop__stat =
[
    [ "clone", "group__c__api.html#ad329fd777726c300d7a044e482b967e7", null ],
    [ "cow", "group__c__api.html#a81566e986cf8cc685a05ac5b634af7f8", null ],
    [ "fsync", "group__c__api.html#ae590322920bebc1156ab585bd6597d76", null ],
    [ "gcrtime_seconds16dot16", "group__c__api.html#ad5a022e40d7a1c044ec5cdbd6f8c4bd8", null ],
    [ "merge", "group__c__api.html#a65464c31b2e6ac04da1fcaa37c9bd9c7", null ],
    [ "msync", "group__c__api.html#a9203afd58680f38ca965857947803e55", null ],
    [ "newly", "group__c__api.html#aa6bb8334a35c997792de2d48d43e3353", null ],
    [ "spill", "group__c__api.html#a83e30f61fbdbe1d3b341882e4f9f4733", null ],
    [ "split", "group__c__api.html#aeefec303079ad17405c889e092e105b0", null ],
    [ "unspill", "group__c__api.html#afb8d02133b1fd21a4966d02176e66be9", null ],
    [ "wops", "group__c__api.html#affcaae187bb01c8297591440736e102a", null ]
];