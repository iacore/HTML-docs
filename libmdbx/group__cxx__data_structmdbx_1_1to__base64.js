var group__cxx__data_structmdbx_1_1to__base64 =
[
    [ "to_base64", "group__cxx__data.html#a8a0ecabce2c0866af7e7e38e7ad7ef93", null ],
    [ "as_buffer", "group__cxx__data.html#a6c5649be4d6057d8e99f1e837a3e1f03", null ],
    [ "as_string", "group__cxx__data.html#a9c33fe96a8594fe05cc4cb22b1c5a023", null ],
    [ "envisage_result_length", "group__cxx__data.html#a835e69c5cec8225cade86b0c4c791478", null ],
    [ "is_empty", "group__cxx__data.html#ac97e261ee33acf0659b5d8eb8d99fd7e", null ],
    [ "is_erroneous", "group__cxx__data.html#af870f7c26b3b59019b21496c6c8b5c92", null ],
    [ "output", "group__cxx__data.html#a3fe400a6462c31f6f12579a7e0334014", null ],
    [ "write_bytes", "group__cxx__data.html#af2ea58fe81b53c3c108f09360b2561af", null ],
    [ "source", "group__cxx__data.html#a169245978a989b5741ce5af99029eca3", null ],
    [ "wrap_width", "group__cxx__data.html#aed4cc244c3c7a4f180fddd3b5babad3b", null ]
];