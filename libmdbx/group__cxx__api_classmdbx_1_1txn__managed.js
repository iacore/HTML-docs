var group__cxx__api_classmdbx_1_1txn__managed =
[
    [ "commit_latency", "group__cxx__api.html#a02cae5eec5e2db442e2d551e85741d58", null ],
    [ "txn_managed", "group__cxx__api.html#ad31423847cfb898d5f82c01ab4ce3671", null ],
    [ "txn_managed", "group__cxx__api.html#adeaaa0c7227b51bcdf033ef654688bcd", null ],
    [ "txn_managed", "group__cxx__api.html#aeeb110a786b90fe03d9b17965989c70c", null ],
    [ "~txn_managed", "group__cxx__api.html#a0627f2dae229a2d4277095208f8471ee", null ],
    [ "abort", "group__cxx__api.html#a65194f98ba6ca8a7c52cbd0482cac978", null ],
    [ "commit", "group__cxx__api.html#aa48fcc76e7819a43e4b1cb74a7d33743", null ],
    [ "commit", "group__cxx__api.html#a994d48100ba06a56ebe869161a4a4a0e", null ],
    [ "commit", "group__cxx__api.html#aea677b1b17537ed6997f9b2d3dc831d4", null ],
    [ "commit_get_latency", "group__cxx__api.html#a7220854d6218c7d6881eb2e98840a67a", null ],
    [ "operator=", "group__cxx__api.html#aca8c4425aeb20565c170fa30ade224d7", null ],
    [ "operator=", "group__cxx__api.html#a43c69a1124ddd9e5c0ae72fd4b421a71", null ],
    [ "env", "group__cxx__api.html#a028b54aa181a8d0ae21d22e0c671b554", null ],
    [ "txn", "group__cxx__api.html#afa3b547f6b8fae932f3413d1646e13eb", null ]
];