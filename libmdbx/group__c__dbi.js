var group__c__dbi =
[
    [ "MDBX_db_flags_t", "group__c__dbi.html#gaff44dbad00df293bcbbc2c87e811c663", null ],
    [ "MDBX_dbi", "group__c__dbi.html#gabd1b4fbb286e8269b6ceb79dbacd9417", null ],
    [ "MDBX_db_flags_t", "group__c__dbi.html#gafe3bddb297b3ab0d828a487c5726f76a", [
      [ "MDBX_DB_DEFAULTS", "group__c__dbi.html#ggafe3bddb297b3ab0d828a487c5726f76aa31d4843d8400091f8314d2b4f62794b4", null ],
      [ "MDBX_REVERSEKEY", "group__c__dbi.html#ggafe3bddb297b3ab0d828a487c5726f76aa642007de41f20a9fbf18196b74e45d4c", null ],
      [ "MDBX_DUPSORT", "group__c__dbi.html#ggafe3bddb297b3ab0d828a487c5726f76aaed33d978b4e23b7c1659e8106e9c7acd", null ],
      [ "MDBX_INTEGERKEY", "group__c__dbi.html#ggafe3bddb297b3ab0d828a487c5726f76aa91b02148f7ea36f66c77ea5bc1e69437", null ],
      [ "MDBX_DUPFIXED", "group__c__dbi.html#ggafe3bddb297b3ab0d828a487c5726f76aa8517d5462fff6156349985b9366b56a4", null ],
      [ "MDBX_INTEGERDUP", "group__c__dbi.html#ggafe3bddb297b3ab0d828a487c5726f76aa007cf37d6fcf269801f9fa95230983e8", null ],
      [ "MDBX_REVERSEDUP", "group__c__dbi.html#ggafe3bddb297b3ab0d828a487c5726f76aaafa2501551cfb4ef3126db95b63779c9", null ],
      [ "MDBX_CREATE", "group__c__dbi.html#ggafe3bddb297b3ab0d828a487c5726f76aad0235ae3602938cc0ca1c453d8c9fe27", null ],
      [ "MDBX_DB_ACCEDE", "group__c__dbi.html#ggafe3bddb297b3ab0d828a487c5726f76aa0bab98f622248becbe218e08d184cddf", null ]
    ] ],
    [ "mdbx_dbi_close", "group__c__dbi.html#ga8d23ee97867170f184fc8f7faa80e392", null ],
    [ "mdbx_dbi_open", "group__c__dbi.html#ga9bef4a9fdf27655e9343bbbf8b6fc5a1", null ],
    [ "mdbx_dbi_open_ex", "group__c__dbi.html#ga4c0707cc17d08231d561903f78656cb6", null ]
];