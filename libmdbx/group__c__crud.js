var group__c__crud =
[
    [ "MDBX_canary", "group__c__api.html#struct_m_d_b_x__canary", [
      [ "v", "group__c__api.html#a133616af3574000dca91d54ae42fb207", null ],
      [ "x", "group__c__api.html#aec4d5064ce5ecf2a922fded78afd9809", null ],
      [ "y", "group__c__api.html#a758ceb1c73be6fe6a04b8cf134cd9653", null ],
      [ "z", "group__c__api.html#aa6dccd072653cacac8db171f09c61225", null ]
    ] ],
    [ "MDBX_cmp_func", "group__c__crud.html#gad1f21a001e784103239137590f44cf2d", null ],
    [ "MDBX_put_flags_t", "group__c__crud.html#gab94f4eb14d41685d00a0bbb402f743d8", null ],
    [ "MDBX_put_flags_t", "group__c__crud.html#ga5b8137591c45143c6d9439799be77136", [
      [ "MDBX_UPSERT", "group__c__crud.html#gga5b8137591c45143c6d9439799be77136aa49ff1a4cdea24aa18e023e97fe2dbae", null ],
      [ "MDBX_NOOVERWRITE", "group__c__crud.html#gga5b8137591c45143c6d9439799be77136afb00dfd7cb8ff336923a2ec836c446cb", null ],
      [ "MDBX_NODUPDATA", "group__c__crud.html#gga5b8137591c45143c6d9439799be77136a72e67ca76a3557394d9e3aa3c387d0d6", null ],
      [ "MDBX_CURRENT", "group__c__crud.html#gga5b8137591c45143c6d9439799be77136a64de8eefaa3b1680ad366f64655a2bb8", null ],
      [ "MDBX_ALLDUPS", "group__c__crud.html#gga5b8137591c45143c6d9439799be77136a311b43fea13e44e2603561ec725e4f9c", null ],
      [ "MDBX_RESERVE", "group__c__crud.html#gga5b8137591c45143c6d9439799be77136abbdedd48fe94bb9b50c467d1b920f959", null ],
      [ "MDBX_APPEND", "group__c__crud.html#gga5b8137591c45143c6d9439799be77136a99e8cb1fb3cbaeb7aeed5b542abc4ded", null ],
      [ "MDBX_APPENDDUP", "group__c__crud.html#gga5b8137591c45143c6d9439799be77136a66e71b110d80967f4208d8736a567c13", null ],
      [ "MDBX_MULTIPLE", "group__c__crud.html#gga5b8137591c45143c6d9439799be77136a2c3d75f9a4f36afd99a5c0718ef024de", null ]
    ] ],
    [ "mdbx_canary_get", "group__c__crud.html#ga4a8198d8178138bf9d53b48cab399df4", null ],
    [ "mdbx_canary_put", "group__c__crud.html#ga4630bfb654e83c5dcde321c5d635a457", null ],
    [ "mdbx_cmp", "group__c__crud.html#gaa37e47d4c48e9ec94c024f23b4956af7", null ],
    [ "mdbx_cursor_count", "group__c__crud.html#gac38dbfb283fef37ae18d85d8546e29ec", null ],
    [ "mdbx_cursor_del", "group__c__crud.html#ga124fee3155425c8d20ca86bfd2d10831", null ],
    [ "mdbx_cursor_get", "group__c__crud.html#gaea98ab004e84cd38f91ae94a6866eeaf", null ],
    [ "mdbx_cursor_get_batch", "group__c__crud.html#ga681163243889233009cf563851d614bf", null ],
    [ "mdbx_cursor_put", "group__c__crud.html#gacd7da9871acab9043665298f3c4996a3", null ],
    [ "mdbx_dbi_sequence", "group__c__crud.html#ga27bedd3c425e93440ad82290a5cf2f03", null ],
    [ "mdbx_dcmp", "group__c__crud.html#gae7986004f7db42d50b08864b550a5007", null ],
    [ "mdbx_del", "group__c__crud.html#gaaab819c1e740210d211ebabad424795b", null ],
    [ "mdbx_drop", "group__c__crud.html#gadc9d57659242902003bb5459bc063b32", null ],
    [ "mdbx_get", "group__c__crud.html#ga64b0d190e0740025a1858de4086e5860", null ],
    [ "mdbx_get_equal_or_great", "group__c__crud.html#ga8b8994d52781a4dbdc5146d069a77ff2", null ],
    [ "mdbx_get_ex", "group__c__crud.html#gad5c9efb2acda533480b608dae1464e7a", null ],
    [ "mdbx_put", "group__c__crud.html#gad0e171200fd96e98de0a2d4e120fa791", null ],
    [ "mdbx_replace", "group__c__crud.html#gaad688c4b0fbbcff676f181dc0437befa", null ]
];