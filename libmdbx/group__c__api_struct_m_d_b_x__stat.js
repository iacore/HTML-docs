var group__c__api_struct_m_d_b_x__stat =
[
    [ "ms_branch_pages", "group__c__api.html#a5f18be64fd542852a2c849956045210b", null ],
    [ "ms_depth", "group__c__api.html#ac912b2789383cfb1896b5aa90a0ffc1e", null ],
    [ "ms_entries", "group__c__api.html#a5f865cc51d11174c7f516a00fa1be69e", null ],
    [ "ms_leaf_pages", "group__c__api.html#ae93ba31086818b08d3fd62065881c59c", null ],
    [ "ms_mod_txnid", "group__c__api.html#ad901e3f906d39b14c4cead6fa367168f", null ],
    [ "ms_overflow_pages", "group__c__api.html#a9c3c01c5201f2fc91f92c29e7c6cfedd", null ],
    [ "ms_psize", "group__c__api.html#abaf71a83303b0a23f9f298da6c730745", null ]
];