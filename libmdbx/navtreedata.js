/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "libmdbx", "index.html", [
    [ "Overall", "index.html", [
      [ "Brief", "index.html#brief", null ],
      [ "Table of Contents", "index.html#toc", null ],
      [ "MithrilDB", "index.html#MithrilDB", null ],
      [ "History", "index.html#autotoc_md2", [
        [ "Acknowledgments", "index.html#autotoc_md3", null ]
      ] ],
      [ "Contributors", "index.html#autotoc_md4", null ],
      [ "License", "index.html#autotoc_md5", null ]
    ] ],
    [ "Introduction", "md_intro.html", [
      [ "Characteristics", "md_intro.html#characteristics", [
        [ "Preface", "md_intro.html#preface", null ],
        [ "Features", "md_intro.html#autotoc_md6", null ],
        [ "Limitations", "md_intro.html#autotoc_md7", null ],
        [ "Gotchas", "md_intro.html#autotoc_md8", null ],
        [ "Comparison with other databases", "md_intro.html#autotoc_md9", null ]
      ] ],
      [ "Improvements beyond LMDB", "md_intro.html#improvements", [
        [ "Added Features", "md_intro.html#autotoc_md10", null ],
        [ "Other fixes and specifics", "md_intro.html#autotoc_md11", null ]
      ] ],
      [ "Restrictions & Caveats", "md_intro.html#restrictions", [
        [ "Long-lived read transactions", "md_intro.html#long-lived-read", null ],
        [ "Large data items and huge transactions", "md_intro.html#autotoc_md12", null ],
        [ "Space reservation", "md_intro.html#autotoc_md13", null ],
        [ "Remote filesystems", "md_intro.html#autotoc_md14", null ],
        [ "Child processes", "md_intro.html#autotoc_md15", null ],
        [ "Read-only mode", "md_intro.html#autotoc_md16", null ],
        [ "Troubleshooting the LCK-file", "md_intro.html#autotoc_md17", null ],
        [ "One thread - One transaction", "md_intro.html#autotoc_md18", null ],
        [ "Do not open twice", "md_intro.html#autotoc_md19", null ]
      ] ],
      [ "Performance comparison", "md_intro.html#performance", [
        [ "Integral performance", "md_intro.html#autotoc_md20", null ],
        [ "Read Scalability", "md_intro.html#autotoc_md22", null ],
        [ "Sync-write mode", "md_intro.html#autotoc_md24", null ],
        [ "Lazy-write mode", "md_intro.html#autotoc_md26", null ],
        [ "Async-write mode", "md_intro.html#autotoc_md28", null ],
        [ "Cost comparison", "md_intro.html#autotoc_md30", null ]
      ] ]
    ] ],
    [ "Usage", "md_usage.html", [
      [ "Building & Embedding", "md_usage.html#getting", [
        [ "Source code embedding", "md_usage.html#autotoc_md31", null ],
        [ "Building and Testing", "md_usage.html#autotoc_md32", [
          [ "Testing", "md_usage.html#autotoc_md33", null ],
          [ "Common important details", "md_usage.html#autotoc_md34", [
            [ "Build reproducibility", "md_usage.html#autotoc_md35", null ],
            [ "Containers", "md_usage.html#autotoc_md36", null ],
            [ "DSO/DLL unloading and destructors of Thread-Local-Storage objects", "md_usage.html#autotoc_md37", null ]
          ] ],
          [ "Linux and other platforms with GNU Make", "md_usage.html#autotoc_md38", null ],
          [ "FreeBSD and related platforms", "md_usage.html#autotoc_md39", null ],
          [ "Windows", "md_usage.html#autotoc_md40", null ],
          [ "Windows Subsystem for Linux", "md_usage.html#autotoc_md41", null ],
          [ "MacOS", "md_usage.html#autotoc_md42", null ],
          [ "Android", "md_usage.html#autotoc_md43", null ],
          [ "iOS", "md_usage.html#autotoc_md44", null ]
        ] ]
      ] ],
      [ "Getting started", "md_usage.html#starting", [
        [ "Cursors", "md_usage.html#Cursors", null ],
        [ "Summarizing the opening", "md_usage.html#autotoc_md45", null ],
        [ "Threads and processes", "md_usage.html#autotoc_md46", null ],
        [ "Transactions, rollbacks etc", "md_usage.html#autotoc_md47", null ],
        [ "Duplicate keys aka Multi-values", "md_usage.html#autotoc_md48", null ],
        [ "Some optimization", "md_usage.html#autotoc_md49", null ],
        [ "Cleaning up", "md_usage.html#autotoc_md50", null ],
        [ "Now read up on the full API!", "md_usage.html#autotoc_md51", null ]
      ] ],
      [ "Bindings", "md_usage.html#bindings", null ]
    ] ],
    [ "ChangeLog", "md__change_log.html", [
      [ "В разработке v0.12.2", "md__change_log.html#autotoc_md58", null ],
      [ "v0.12.1 (Positive Proxima) at 2022-08-24", "md__change_log.html#autotoc_md60", null ],
      [ "v0.12.0 at 2022-06-19", "md__change_log.html#autotoc_md61", null ],
      [ "v0.11.10 (the TriColor) at 2022-08-22", "md__change_log.html#autotoc_md63", null ],
      [ "v0.11.9 (Чирчик-1992) at 2022-08-02", "md__change_log.html#autotoc_md65", null ],
      [ "v0.11.8 (Baked Apple) at 2022-06-12", "md__change_log.html#autotoc_md67", null ],
      [ "v0.11.7 (Resurrected Sarmat) at 2022-04-22", "md__change_log.html#autotoc_md69", null ],
      [ "v0.11.6 at 2022-03-24", "md__change_log.html#autotoc_md72", null ],
      [ "v0.11.5 at 2022-02-23", "md__change_log.html#autotoc_md73", null ],
      [ "v0.11.4 at 2022-02-02", "md__change_log.html#autotoc_md74", null ],
      [ "v0.11.3 at 2021-12-31", "md__change_log.html#autotoc_md75", null ],
      [ "v0.11.2 at 2021-12-02", "md__change_log.html#autotoc_md76", null ],
      [ "v0.11.1 at 2021-10-23", "md__change_log.html#autotoc_md77", [
        [ "Backward compatibility break:", "md__change_log.html#autotoc_md78", null ]
      ] ],
      [ "v0.10.5 at 2021-10-13 (obsolete, please use v0.11.1)", "md__change_log.html#autotoc_md80", null ],
      [ "v0.10.4 at 2021-10-10", "md__change_log.html#autotoc_md81", null ],
      [ "v0.10.3 at 2021-08-27", "md__change_log.html#autotoc_md82", null ],
      [ "v0.10.2 at 2021-07-26", "md__change_log.html#autotoc_md83", null ],
      [ "v0.10.1 at 2021-06-01", "md__change_log.html#autotoc_md84", null ],
      [ "v0.10.0 at 2021-05-09", "md__change_log.html#autotoc_md85", null ],
      [ "v0.9.3 at 2021-02-02", "md__change_log.html#autotoc_md87", null ],
      [ "v0.9.2 at 2020-11-27", "md__change_log.html#autotoc_md88", null ],
      [ "v0.9.1 2020-09-30", "md__change_log.html#autotoc_md89", null ],
      [ "v0.9.0 2020-07-31 (not a release, but API changes)", "md__change_log.html#autotoc_md90", null ],
      [ "2020-07-06", "md__change_log.html#autotoc_md92", null ],
      [ "2020-06-12", "md__change_log.html#autotoc_md93", null ],
      [ "2020-06-05", "md__change_log.html#autotoc_md94", null ],
      [ "2020-03-18", "md__change_log.html#autotoc_md95", null ],
      [ "2020-01-21", "md__change_log.html#autotoc_md96", null ],
      [ "2019-12-31", "md__change_log.html#autotoc_md97", null ],
      [ "2019-12-02", "md__change_log.html#autotoc_md98", null ]
    ] ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Concepts", "concepts.html", "concepts" ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group__c__api.html#a8fa3b9f102114b6a412335cbe2c0802e",
"group__c__debug.html#gacaf898a6adfec680b34cff3c077449a3",
"group__cxx__api.html#a1f471aac40e400953a31795e58ba3d49",
"group__cxx__api.html#acb32eb606812109615b641945d7fc485",
"group__cxx__api.html#ga82d9880b81221740c1cdfd54277cb6d1",
"group__cxx__data.html#ac25d132b3bc4f75a9a4b1589268c0554",
"structmdbx_1_1cursor_1_1move__result.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';