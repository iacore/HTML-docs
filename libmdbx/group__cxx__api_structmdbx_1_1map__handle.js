var group__cxx__api_structmdbx_1_1map__handle =
[
    [ "info", "structmdbx_1_1map__handle_1_1info.html", "structmdbx_1_1map__handle_1_1info" ],
    [ "flags", "group__cxx__api.html#a1468da7d5cad42eb67ee1e0e4c1a9d6f", null ],
    [ "state", "group__cxx__api.html#a96e798488765ea23868d82e18fa9cc33", null ],
    [ "map_handle", "group__cxx__api.html#a6174410cbb8c44309fc0b02bf2b99911", null ],
    [ "map_handle", "group__cxx__api.html#adcdfdbe5fbae8b03b0f1dc26c421d8f6", null ],
    [ "map_handle", "group__cxx__api.html#a11d6e032e0a915ee64a751fa9d6e17e8", null ],
    [ "operator=", "group__cxx__api.html#a265252fefca6ed3b7f5b0c34bcc6956a", null ],
    [ "dbi", "group__cxx__api.html#ae2fffa7bc45893a1f8fda0798d027358", null ]
];