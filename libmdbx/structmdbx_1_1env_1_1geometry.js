var structmdbx_1_1env_1_1geometry =
[
    [ "size", "structmdbx_1_1env_1_1geometry_1_1size.html", "structmdbx_1_1env_1_1geometry_1_1size" ],
    [ "geometry", "structmdbx_1_1env_1_1geometry.html#a3c26e636d4c717801347aaa46bbd4f38", null ],
    [ "geometry", "structmdbx_1_1env_1_1geometry.html#af1a6f38507d8e4a6a1f1021c70c16fc5", null ],
    [ "geometry", "structmdbx_1_1env_1_1geometry.html#adecf4bdc5cad89a204e324f414678b42", null ],
    [ "make_dynamic", "group__cxx__api.html#gac0c48341f24fffd1e8024a4c544a9ce4", null ],
    [ "make_fixed", "group__cxx__api.html#gaedc1bfaf2d0a777135544b54bc74204a", null ],
    [ "growth_step", "structmdbx_1_1env_1_1geometry.html#a7f95548fe4c4b4149884ac9150dc8d73", null ],
    [ "pagesize", "structmdbx_1_1env_1_1geometry.html#a45048bf2de9120d01dae2151c060d459", null ],
    [ "shrink_threshold", "structmdbx_1_1env_1_1geometry.html#ab233e3401b58b5d88362ce9457870d54", null ],
    [ "size_lower", "structmdbx_1_1env_1_1geometry.html#abfd123ee2277df77373cc6d59ae0472c", null ],
    [ "size_now", "structmdbx_1_1env_1_1geometry.html#ab204b6da9885831ca0314a2f8c2ae556", null ],
    [ "size_upper", "structmdbx_1_1env_1_1geometry.html#a4368dc988eefc0e1a49174afe4598222", null ]
];