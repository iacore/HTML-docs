var group__c__statinfo =
[
    [ "MDBX_stat", "group__c__api.html#struct_m_d_b_x__stat", [
      [ "ms_branch_pages", "group__c__api.html#a5f18be64fd542852a2c849956045210b", null ],
      [ "ms_depth", "group__c__api.html#ac912b2789383cfb1896b5aa90a0ffc1e", null ],
      [ "ms_entries", "group__c__api.html#a5f865cc51d11174c7f516a00fa1be69e", null ],
      [ "ms_leaf_pages", "group__c__api.html#ae93ba31086818b08d3fd62065881c59c", null ],
      [ "ms_mod_txnid", "group__c__api.html#ad901e3f906d39b14c4cead6fa367168f", null ],
      [ "ms_overflow_pages", "group__c__api.html#a9c3c01c5201f2fc91f92c29e7c6cfedd", null ],
      [ "ms_psize", "group__c__api.html#abaf71a83303b0a23f9f298da6c730745", null ]
    ] ],
    [ "MDBX_envinfo", "group__c__api.html#struct_m_d_b_x__envinfo", [
      [ "mi_autosync_period_seconds16dot16", "group__c__api.html#aeccf072444af6392f2dfd37358e475d2", null ],
      [ "mi_autosync_threshold", "group__c__api.html#a550ce8091fb61b69b42b0c9b248ff940", null ],
      [ "mi_bootid", "group__c__api.html#a3ea963b0b66534d072e634fd14b0e459", null ],
      [ "mi_dxb_pagesize", "group__c__api.html#a0eb186eea75d9cecc94d15992bc79654", null ],
      [ "mi_geo", "group__c__api.html#a9ad40e0adab91b5b7bedb4823b4ebfdb", null ],
      [ "mi_last_pgno", "group__c__api.html#acb89c98ff9b18341ffbf12f1c65e03dc", null ],
      [ "mi_latter_reader_txnid", "group__c__api.html#a993a3b51fc5bd7ded9468f5d2e63c099", null ],
      [ "mi_mapsize", "group__c__api.html#a040372f37123dd4f9000d6190011bf3f", null ],
      [ "mi_maxreaders", "group__c__api.html#a1150fcaca8be362dbc71267a757ec435", null ],
      [ "mi_meta0_sign", "group__c__api.html#ab056131ff1f41b2e41a162bdf3a823b8", null ],
      [ "mi_meta0_txnid", "group__c__api.html#a7e362f67cdc659dafccbd7397d112d70", null ],
      [ "mi_meta1_sign", "group__c__api.html#a0b26f4f6126888349e9ca8c46906cb45", null ],
      [ "mi_meta1_txnid", "group__c__api.html#ad6cf0c3db7807d34119af83288fd82f3", null ],
      [ "mi_meta2_sign", "group__c__api.html#a77762937ae9ead5d4bf5b4c7d4c4edbc", null ],
      [ "mi_meta2_txnid", "group__c__api.html#a001963011e0789749ab4c8408f6f007d", null ],
      [ "mi_mode", "group__c__api.html#a8abf704de1ba63fc47b3000536f20e3c", null ],
      [ "mi_numreaders", "group__c__api.html#a1ad65f8a986006cc02e8a8bbf4ed4602", null ],
      [ "mi_pgop_stat", "group__c__api.html#a1c2f3023cebcd09431c9c7e0f75d62b7", null ],
      [ "mi_recent_txnid", "group__c__api.html#a5368f5b58a20e205913a9d25b1417f8c", null ],
      [ "mi_self_latter_reader_txnid", "group__c__api.html#ae4e046a4965798631d8ebc78b7810ebc", null ],
      [ "mi_since_reader_check_seconds16dot16", "group__c__api.html#a71957c49d40cba112b6bb104a9b5202b", null ],
      [ "mi_since_sync_seconds16dot16", "group__c__api.html#a2288193669b48107ead4ef049d999c3f", null ],
      [ "mi_sys_pagesize", "group__c__api.html#aedfadab30b250fedb2422f8bbe122a69", null ],
      [ "mi_unsync_volume", "group__c__api.html#aedaae3f60aaa621c42e0dfd35a962d2f", null ]
    ] ],
    [ "MDBX_txn_info", "group__c__api.html#struct_m_d_b_x__txn__info", [
      [ "txn_id", "group__c__api.html#a8fa3b9f102114b6a412335cbe2c0802e", null ],
      [ "txn_reader_lag", "group__c__api.html#a383fe4a97f9108000d5c69590738f8ad", null ],
      [ "txn_space_dirty", "group__c__api.html#a0a13745cd97c277cd810f3bbf60c0d9f", null ],
      [ "txn_space_leftover", "group__c__api.html#a461b926cfd0e656bebac9bab5997d6b1", null ],
      [ "txn_space_limit_hard", "group__c__api.html#aa93c141047f682507850e6f7a08f5557", null ],
      [ "txn_space_limit_soft", "group__c__api.html#a8aa8674b1b08e6f956d893c02c1b4514", null ],
      [ "txn_space_retired", "group__c__api.html#ac8a5a3e9a6f4bfba24ca9dd8d957030f", null ],
      [ "txn_space_used", "group__c__api.html#a48515214b62493d820cbafda219bcc48", null ]
    ] ],
    [ "MDBX_commit_latency", "group__c__api.html#struct_m_d_b_x__commit__latency", [
      [ "audit", "group__c__api.html#abd60c7295a4970c49fb8833703a54758", null ],
      [ "ending", "group__c__api.html#acf2390b715b885cb8f484bc30fdd3381", null ],
      [ "gc", "group__c__api.html#ad901e40e841a3bfb94b865008cfd9c2f", null ],
      [ "preparation", "group__c__api.html#aa2e2b38a085e90af171c8561e8f0e9e9", null ],
      [ "sync", "group__c__api.html#a89ade90ede0e8ccec1d729e81ae6b956", null ],
      [ "whole", "group__c__api.html#aa97dacaedc16f1c31df6888096e63809", null ],
      [ "write", "group__c__api.html#ac8928e8af622cc1e7b2b28ceae44a013", null ]
    ] ],
    [ "MDBX_envinfo.mi_geo", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__geo", [
      [ "current", "group__c__api.html#a43b5c9175984c071f30b873fdce0a000", null ],
      [ "grow", "group__c__api.html#a4d200fce73a8e1cc965cfc2c43343824", null ],
      [ "lower", "group__c__api.html#a81e073b428b50247daba38531dcf412a", null ],
      [ "shrink", "group__c__api.html#a91808da94826016395b10b662ed4363b", null ],
      [ "upper", "group__c__api.html#a0122b4c2c01ee1c698ecc309d2b8eb5a", null ]
    ] ],
    [ "MDBX_envinfo.mi_bootid", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid", [
      [ "current", "group__c__api.html#a43b5c9175984c071f30b873fdce0a000", null ],
      [ "meta0", "group__c__api.html#a693c1c0352f1f8a95cf36d4d71a57ac1", null ],
      [ "meta1", "group__c__api.html#ab6c6efe8d7ca0aa0d8b7c03370ba7525", null ],
      [ "meta2", "group__c__api.html#a8daee5ed440dd7940e0b42c74ad871e3", null ]
    ] ],
    [ "MDBX_envinfo.mi_bootid.current", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8current", [
      [ "x", "group__c__api.html#a9dd4e461268c8034f5c8564e155c67a6", null ],
      [ "y", "group__c__api.html#a415290769594460e2e485922904f345d", null ]
    ] ],
    [ "MDBX_envinfo.mi_bootid.meta0", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta0", [
      [ "x", "group__c__api.html#a9dd4e461268c8034f5c8564e155c67a6", null ],
      [ "y", "group__c__api.html#a415290769594460e2e485922904f345d", null ]
    ] ],
    [ "MDBX_envinfo.mi_bootid.meta1", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta1", [
      [ "x", "group__c__api.html#a9dd4e461268c8034f5c8564e155c67a6", null ],
      [ "y", "group__c__api.html#a415290769594460e2e485922904f345d", null ]
    ] ],
    [ "MDBX_envinfo.mi_bootid.meta2", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta2", [
      [ "x", "group__c__api.html#a9dd4e461268c8034f5c8564e155c67a6", null ],
      [ "y", "group__c__api.html#a415290769594460e2e485922904f345d", null ]
    ] ],
    [ "MDBX_envinfo.mi_pgop_stat", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__pgop__stat", [
      [ "clone", "group__c__api.html#ad329fd777726c300d7a044e482b967e7", null ],
      [ "cow", "group__c__api.html#a81566e986cf8cc685a05ac5b634af7f8", null ],
      [ "fsync", "group__c__api.html#ae590322920bebc1156ab585bd6597d76", null ],
      [ "gcrtime_seconds16dot16", "group__c__api.html#ad5a022e40d7a1c044ec5cdbd6f8c4bd8", null ],
      [ "merge", "group__c__api.html#a65464c31b2e6ac04da1fcaa37c9bd9c7", null ],
      [ "msync", "group__c__api.html#a9203afd58680f38ca965857947803e55", null ],
      [ "newly", "group__c__api.html#aa6bb8334a35c997792de2d48d43e3353", null ],
      [ "spill", "group__c__api.html#a83e30f61fbdbe1d3b341882e4f9f4733", null ],
      [ "split", "group__c__api.html#aeefec303079ad17405c889e092e105b0", null ],
      [ "unspill", "group__c__api.html#afb8d02133b1fd21a4966d02176e66be9", null ],
      [ "wops", "group__c__api.html#affcaae187bb01c8297591440736e102a", null ]
    ] ],
    [ "MDBX_dbi_state_t", "group__c__statinfo.html#gabf461b23a4acff93f3bfd4d7bea4f538", null ],
    [ "MDBX_reader_list_func", "group__c__statinfo.html#gaabe986edb8252018c183e741b1a5c7f7", null ],
    [ "MDBX_dbi_state_t", "group__c__statinfo.html#ga732d2c2ae1d596988ffff051baf34227", [
      [ "MDBX_DBI_DIRTY", "group__c__statinfo.html#gga732d2c2ae1d596988ffff051baf34227a57a012585ae45f7a7dab66011557cb55", null ],
      [ "MDBX_DBI_STALE", "group__c__statinfo.html#gga732d2c2ae1d596988ffff051baf34227adff50c679958b5172efffb3a2faeea04", null ],
      [ "MDBX_DBI_FRESH", "group__c__statinfo.html#gga732d2c2ae1d596988ffff051baf34227adf2b8552b658cb4219ea2a137109bf41", null ],
      [ "MDBX_DBI_CREAT", "group__c__statinfo.html#gga732d2c2ae1d596988ffff051baf34227aec988b325fc2c167df474b36d305de29", null ]
    ] ],
    [ "mdbx_dbi_dupsort_depthmask", "group__c__statinfo.html#ga6bb9689c132368ab6e04997641e42217", null ],
    [ "mdbx_dbi_flags", "group__c__statinfo.html#gadd1dd791801fd714d19ff5e34061d95e", null ],
    [ "mdbx_dbi_flags_ex", "group__c__statinfo.html#ga7a45ff5928f2d810684386ccdaf198a2", null ],
    [ "mdbx_dbi_stat", "group__c__statinfo.html#ga7582460beceb078cec8b515a5280d568", null ],
    [ "mdbx_default_pagesize", "group__c__statinfo.html#gabb0548d1eb2ff5c4d2a3105a190bf11a", null ],
    [ "mdbx_env_get_fd", "group__c__statinfo.html#ga89a6edc60c9f9d541572d224a677fae6", null ],
    [ "mdbx_env_get_flags", "group__c__statinfo.html#gadd6bfdb43e732b45d2f2fd9787258ea7", null ],
    [ "mdbx_env_get_maxdbs", "group__c__statinfo.html#gaa6da1288199bc5baff69822048bdef8e", null ],
    [ "mdbx_env_get_maxkeysize", "group__c__statinfo.html#gab10f218c84e8c4539ff0806242d072e9", null ],
    [ "mdbx_env_get_maxkeysize_ex", "group__c__statinfo.html#ga1a56fdbce56c043800dcfab26617d9c1", null ],
    [ "mdbx_env_get_maxreaders", "group__c__statinfo.html#ga14f2fdb1ea1aeb11872d498c9ea7a17e", null ],
    [ "mdbx_env_get_maxvalsize_ex", "group__c__statinfo.html#ga300da49c69eeb4872b0e9f5c531e8584", null ],
    [ "mdbx_env_get_pairsize4page_max", "group__c__statinfo.html#ga375ffeb54a3e6cc86a9bec6fd7b2a026", null ],
    [ "mdbx_env_get_path", "group__c__statinfo.html#ga008167b2480d31aba724f66f846ee818", null ],
    [ "mdbx_env_get_syncbytes", "group__c__statinfo.html#ga4ce9ac9aec334cdf630a0a3a80fb542f", null ],
    [ "mdbx_env_get_syncperiod", "group__c__statinfo.html#ga14f5c91d187bb4fefbd57a23545b7f5c", null ],
    [ "mdbx_env_get_userctx", "group__c__statinfo.html#ga93f5701d86122e3b1659a2f0170f7e11", null ],
    [ "mdbx_env_get_valsize4page_max", "group__c__statinfo.html#ga77f9a269692a6f05a1db775cc7194eb0", null ],
    [ "mdbx_env_info", "group__c__statinfo.html#ga981cca20d744de68eaf69fa69b4d8d3d", null ],
    [ "mdbx_env_info_ex", "group__c__statinfo.html#ga120fc97344dd217645dcaf8f8b296fe0", null ],
    [ "mdbx_env_stat", "group__c__statinfo.html#gaa6acb04171776e09db1b7f628418d95d", null ],
    [ "mdbx_env_stat_ex", "group__c__statinfo.html#ga31ec74cce3bb081e6b37dbc85a17f86b", null ],
    [ "mdbx_get_sysraminfo", "group__c__statinfo.html#gac6d51331881b1c0c4623663040f03d05", null ],
    [ "mdbx_is_dirty", "group__c__statinfo.html#ga71ec584464114d2924b71eeb6b80e3fb", null ],
    [ "mdbx_limits_dbsize_max", "group__c__statinfo.html#gacd9d3dd0df9f165386142c2e5864f8ec", null ],
    [ "mdbx_limits_dbsize_min", "group__c__statinfo.html#gad025e0d98071faf457b257c6148503a0", null ],
    [ "mdbx_limits_keysize_max", "group__c__statinfo.html#gab833240078c4d8effadf1b7bf3a7a44f", null ],
    [ "mdbx_limits_pairsize4page_max", "group__c__statinfo.html#gaca5c03fb63421882797a56854aa24098", null ],
    [ "mdbx_limits_pgsize_max", "group__c__statinfo.html#ga594640ddfcdc71aa8385f1b14e4fbb89", null ],
    [ "mdbx_limits_pgsize_min", "group__c__statinfo.html#ga829094bd3ef359c24c8c48778fad543b", null ],
    [ "mdbx_limits_txnsize_max", "group__c__statinfo.html#ga57070a38056c932d3fcafdb06892c560", null ],
    [ "mdbx_limits_valsize4page_max", "group__c__statinfo.html#ga52ea51805876cb1e3289b18451897337", null ],
    [ "mdbx_limits_valsize_max", "group__c__statinfo.html#ga0eb9b32d2b89732b631528bd4c1bf8c3", null ],
    [ "mdbx_reader_list", "group__c__statinfo.html#gabdb3338fbba53434f29bfecccbeb1082", null ],
    [ "mdbx_txn_id", "group__c__statinfo.html#ga07cb99c5d46db8413beeeb7dfbe56435", null ],
    [ "mdbx_txn_info", "group__c__statinfo.html#ga6ec787bbb13da2028fb9c70993ef6ee1", null ],
    [ "mdbx_txn_straggler", "group__c__statinfo.html#gaed7f513d5f19ea01d432be5d3fabebd4", null ]
];