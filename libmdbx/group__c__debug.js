var group__c__debug =
[
    [ "MDBX_LOGGER_DONTCHANGE", "group__c__debug.html#ga9727175fb46ed69e846d7d09789913f2", null ],
    [ "MDBX_assert_func", "group__c__debug.html#gacaf898a6adfec680b34cff3c077449a3", null ],
    [ "MDBX_debug_flags_t", "group__c__debug.html#gac1993aef64a6e14be98ccabe3ef40c96", null ],
    [ "MDBX_debug_func", "group__c__debug.html#ga910fab151cba94f95250501f73e53af2", null ],
    [ "MDBX_log_level_t", "group__c__debug.html#ga56bf5113df16fc7a1f2897c73a41656e", null ],
    [ "MDBX_debug_flags_t", "group__c__debug.html#ga90414ba5e7481fd2c34f629ffc32f7d4", [
      [ "MDBX_DBG_NONE", "group__c__debug.html#gga90414ba5e7481fd2c34f629ffc32f7d4a7e3810c8d3fc9d5797b234205d7d2031", null ],
      [ "MDBX_DBG_ASSERT", "group__c__debug.html#gga90414ba5e7481fd2c34f629ffc32f7d4a08793af3116fc86abbd5aef443872476", null ],
      [ "MDBX_DBG_AUDIT", "group__c__debug.html#gga90414ba5e7481fd2c34f629ffc32f7d4a9cf49d3256e2dd72325b804fe470465c", null ],
      [ "MDBX_DBG_JITTER", "group__c__debug.html#gga90414ba5e7481fd2c34f629ffc32f7d4ae3e24322397c719de711a72118be2e73", null ],
      [ "MDBX_DBG_DUMP", "group__c__debug.html#gga90414ba5e7481fd2c34f629ffc32f7d4a8394df2f1d6387bba8d6b76c37c1c150", null ],
      [ "MDBX_DBG_LEGACY_MULTIOPEN", "group__c__debug.html#gga90414ba5e7481fd2c34f629ffc32f7d4a5e8d81ab9cc65a4c79e4d5a9c8ecdf27", null ],
      [ "MDBX_DBG_LEGACY_OVERLAP", "group__c__debug.html#gga90414ba5e7481fd2c34f629ffc32f7d4a2a525bec40e60d071b806189ddd022c2", null ],
      [ "MDBX_DBG_DONT_UPGRADE", "group__c__debug.html#gga90414ba5e7481fd2c34f629ffc32f7d4a0ab5bdcaa73f3b6211c45203c69beab3", null ],
      [ "MDBX_DBG_DONTCHANGE", "group__c__debug.html#gga90414ba5e7481fd2c34f629ffc32f7d4a8d6b62b8396aa059ad2d9c310edc880c", null ]
    ] ],
    [ "MDBX_log_level_t", "group__c__debug.html#ga7d1aeb940f23f658bcb4cd845b1671a3", [
      [ "MDBX_LOG_FATAL", "group__c__debug.html#gga7d1aeb940f23f658bcb4cd845b1671a3aadebf8497307d612165e88107718c334", null ],
      [ "MDBX_LOG_ERROR", "group__c__debug.html#gga7d1aeb940f23f658bcb4cd845b1671a3adc52f4956dbcdba9f7024251fc152701", null ],
      [ "MDBX_LOG_WARN", "group__c__debug.html#gga7d1aeb940f23f658bcb4cd845b1671a3a4eecab660672dcd5dc1dcedd41327a77", null ],
      [ "MDBX_LOG_NOTICE", "group__c__debug.html#gga7d1aeb940f23f658bcb4cd845b1671a3adee4a981a2c66adc66f4d89c5d0a0e06", null ],
      [ "MDBX_LOG_VERBOSE", "group__c__debug.html#gga7d1aeb940f23f658bcb4cd845b1671a3afdb94b7608cec5299e720dc360b8df33", null ],
      [ "MDBX_LOG_DEBUG", "group__c__debug.html#gga7d1aeb940f23f658bcb4cd845b1671a3a5b2f058e7035c7414a46b3ef728078e1", null ],
      [ "MDBX_LOG_TRACE", "group__c__debug.html#gga7d1aeb940f23f658bcb4cd845b1671a3a513a2a804d91fd36c695a9f02c643860", null ],
      [ "MDBX_LOG_EXTRA", "group__c__debug.html#gga7d1aeb940f23f658bcb4cd845b1671a3a1af4c1c3469ef696686e2d883d953448", null ],
      [ "MDBX_LOG_DONTCHANGE", "group__c__debug.html#gga7d1aeb940f23f658bcb4cd845b1671a3afb254924dfd2c470bb6b9aff1bc5f7b1", null ]
    ] ],
    [ "mdbx_assert_fail", "group__c__debug.html#gab39494c4a81320cc2e8f69c5f13dffc0", null ],
    [ "mdbx_dump_val", "group__c__debug.html#ga0577c0ebf208c4aa9685edba45d1848e", null ],
    [ "mdbx_env_set_assert", "group__c__debug.html#ga9ae6251c298e60546403f6ae5463389a", null ],
    [ "mdbx_panic", "group__c__debug.html#ga1858514aeed7cc00250cc9dbe550d2d1", null ],
    [ "mdbx_setup_debug", "group__c__debug.html#gae5f003fad215cdd8253ef43b55a02ae0", null ]
];