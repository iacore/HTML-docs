var structmdbx_1_1map__handle_1_1info =
[
    [ "info", "group__cxx__api.html#ga6063701cfb59a66dd609146aa607b057", null ],
    [ "info", "structmdbx_1_1map__handle_1_1info.html#a890acd2999b5a5091fe4915d6a398c83", null ],
    [ "key_mode", "group__cxx__api.html#ga1598464a983c00c07d966f5c79c25d03", null ],
    [ "operator=", "structmdbx_1_1map__handle_1_1info.html#af81cd2bf01363d02eadcbbce91a00091", null ],
    [ "value_mode", "group__cxx__api.html#ga972b4607700bb6ad304a54fea1bf0355", null ],
    [ "flags", "structmdbx_1_1map__handle_1_1info.html#ae89276f59c5f8d8b20c87f978d020844", null ],
    [ "state", "structmdbx_1_1map__handle_1_1info.html#a6b69e8a5900ec845f5ea12f24fb3fcad", null ]
];