var group__cxx__data_structmdbx_1_1from__base64 =
[
    [ "from_base64", "group__cxx__data.html#abd255392596df765deba4c9835adce93", null ],
    [ "as_buffer", "group__cxx__data.html#aaf62c0fafa031e3e0c3e437353723b00", null ],
    [ "as_string", "group__cxx__data.html#a3f50a0fcc60c546fefea2afb1929a67f", null ],
    [ "envisage_result_length", "group__cxx__data.html#ac820289591a2d2114232cb4d4471af98", null ],
    [ "is_empty", "group__cxx__data.html#a5f0d8f6aafbfcaedef90a850509318b3", null ],
    [ "is_erroneous", "group__cxx__data.html#abe27a04f4c3ebe69122953a300233db7", null ],
    [ "write_bytes", "group__cxx__data.html#a3b736ac0a82b59700b2aa9dfbf58560f", null ],
    [ "ignore_spaces", "group__cxx__data.html#abb1c4a6187509f5447f329b06417e348", null ],
    [ "source", "group__cxx__data.html#ac565c8e62386c2732d53de7c5d8b5970", null ]
];