var group__c__extra =
[
    [ "B-tree Traversal", "group__btree__traversal.html", "group__btree__traversal" ],
    [ "MDBX_copy_flags_t", "group__c__extra.html#ga8751dcaa7c51fa1c5acf2b3a46f750bc", null ],
    [ "MDBX_env_delete_mode_t", "group__c__extra.html#gaaf3e4a27bc19ebbdc523dfa3f4b65625", null ],
    [ "MDBX_copy_flags_t", "group__c__extra.html#ga1a81f83c4374e9a768842215628914ca", [
      [ "MDBX_CP_DEFAULTS", "group__c__extra.html#gga1a81f83c4374e9a768842215628914caa4aaba6490c1c588c12c4ddfa361397b8", null ],
      [ "MDBX_CP_COMPACT", "group__c__extra.html#gga1a81f83c4374e9a768842215628914caa5948ed9a8b8cabe4de4cffc3dc5a4a70", null ],
      [ "MDBX_CP_FORCE_DYNAMIC_SIZE", "group__c__extra.html#gga1a81f83c4374e9a768842215628914caaaf15be75235dfd417f57422ea9c8826f", null ]
    ] ],
    [ "MDBX_env_delete_mode_t", "group__c__extra.html#gacaa2c29888b090116824f9324b1385db", [
      [ "MDBX_ENV_JUST_DELETE", "group__c__extra.html#ggacaa2c29888b090116824f9324b1385dbaf2553a8da7fe03ee8d15f4405c1fe437", null ],
      [ "MDBX_ENV_ENSURE_UNUSED", "group__c__extra.html#ggacaa2c29888b090116824f9324b1385dbaf6041dd276cc8af5f4fcf4d39d665230", null ],
      [ "MDBX_ENV_WAIT_FOR_UNUSED", "group__c__extra.html#ggacaa2c29888b090116824f9324b1385dba56f5b7cacf64b8b4e16f6a39a8ee47ed", null ]
    ] ],
    [ "mdbx_env_copy", "group__c__extra.html#ga165f1beca577d07915de13d6a09e226e", null ],
    [ "mdbx_env_copy2fd", "group__c__extra.html#gacc6c972bbb76505d0b641e65b985b24f", null ],
    [ "mdbx_env_delete", "group__c__extra.html#gaeae135556b04c19275d3127d106d50ba", null ],
    [ "mdbx_env_sync", "group__c__extra.html#ga2d26ce1c3337fd792ed5ca2b598d163c", null ],
    [ "mdbx_env_sync_ex", "group__c__extra.html#gae761676e8dd87488002ab1cb8d7ff0be", null ],
    [ "mdbx_env_sync_poll", "group__c__extra.html#ga5dad8ba58835cd5d51a2174c321f57de", null ],
    [ "mdbx_get_datacmp", "group__c__extra.html#ga02a5f09720db349790afbfbbbbd1a2ab", null ],
    [ "mdbx_get_keycmp", "group__c__extra.html#ga46f8afaeba30792fb13c363536aaea9d", null ],
    [ "mdbx_is_readahead_reasonable", "group__c__extra.html#ga47868c0824f38ee163fa01aaf16b58a6", null ],
    [ "mdbx_reader_check", "group__c__extra.html#ga15d520b1259aa361a4214f8d8f88c6c5", null ],
    [ "mdbx_thread_register", "group__c__extra.html#ga5729b25e78d6626f1b5f6294be2db264", null ],
    [ "mdbx_thread_unregister", "group__c__extra.html#gae5f6a8669d97aa49efe76eaf81b661bf", null ]
];