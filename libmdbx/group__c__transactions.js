var group__c__transactions =
[
    [ "MDBX_txn", "group__c__transactions.html#gad3eb5aa2a23beb3dc6274c46dc236546", null ],
    [ "MDBX_txn_flags_t", "group__c__transactions.html#ga6ddf4a553ef12632520817465f53d0d6", [
      [ "MDBX_TXN_READWRITE", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6ae90b953ba7bf0066da97b3633c020ed3", null ],
      [ "MDBX_TXN_RDONLY", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6a0e14b5c92c691efc83e351a2211f7bb2", null ],
      [ "MDBX_TXN_RDONLY_PREPARE", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6aacdd54642f8085c8af3b7a577bc0f54b", null ],
      [ "MDBX_TXN_TRY", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6afa6d366d8dab5bfc20b69789b54172fa", null ],
      [ "MDBX_TXN_NOMETASYNC", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6a2c7655607ad39127953df54435538c7f", null ],
      [ "MDBX_TXN_NOSYNC", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6ad3aa95a49a7249df96be07ffeff0412c", null ],
      [ "MDBX_TXN_INVALID", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6ab6493187d4c38216cef11bd184a48f2e", null ],
      [ "MDBX_TXN_FINISHED", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6afdf42bee0bb291425906f4b828fcf901", null ],
      [ "MDBX_TXN_ERROR", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6a1c3554d09d4a192394dce1a489e4a9a7", null ],
      [ "MDBX_TXN_DIRTY", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6ad889b6c7028afed8f72e949f85c22a20", null ],
      [ "MDBX_TXN_SPILLS", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6a64f18109c906c7fb85e58d103f75f498", null ],
      [ "MDBX_TXN_HAS_CHILD", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6aa0eef211ba4cbfa9d3e2b241fa2667ad", null ],
      [ "MDBX_TXN_BLOCKED", "group__c__transactions.html#gga6ddf4a553ef12632520817465f53d0d6abb6b3ea785a39d5794e354311644885c", null ]
    ] ],
    [ "mdbx_txn_abort", "group__c__transactions.html#ga9773fd77d15cc810fc4eff0060f1a596", null ],
    [ "mdbx_txn_begin", "group__c__transactions.html#ga9621bd463ec9efb2373f25dfb016938a", null ],
    [ "mdbx_txn_begin_ex", "group__c__transactions.html#ga2cfa3a3e97def47cafb4bc9b9e13da3d", null ],
    [ "mdbx_txn_break", "group__c__transactions.html#ga04465e60a2337ce8472274b1a580b7fc", null ],
    [ "mdbx_txn_commit", "group__c__transactions.html#ga3146a36cf1d0603cc6e3d52e97ec38a9", null ],
    [ "mdbx_txn_commit_ex", "group__c__transactions.html#ga4d26c86ed64f94cc533642329fcca654", null ],
    [ "mdbx_txn_env", "group__c__transactions.html#ga6593470519d35592a49e3a62a336a6e1", null ],
    [ "mdbx_txn_flags", "group__c__transactions.html#ga4adb6ddadc77a05797026c3d4c18056e", null ],
    [ "mdbx_txn_get_userctx", "group__c__transactions.html#ga93e05047dbf6681f1198be927cff420c", null ],
    [ "mdbx_txn_renew", "group__c__transactions.html#gaf3cd3707d0154f81f7f73484068246d1", null ],
    [ "mdbx_txn_reset", "group__c__transactions.html#gae9f34737fe60b0ba538d5a09b6a25c8d", null ],
    [ "mdbx_txn_set_userctx", "group__c__transactions.html#ga2166d11669c64307195788186b090e41", null ]
];