var annotated_dup =
[
    [ "mdbx", "namespacemdbx.html", [
      [ "allocation_aware_details", "namespacemdbx_1_1allocation__aware__details.html", [
        [ "copy_assign_alloc", "namespacemdbx_1_1allocation__aware__details.html#structmdbx_1_1allocation__aware__details_1_1copy__assign__alloc", null ],
        [ "move_assign_alloc", "namespacemdbx_1_1allocation__aware__details.html#structmdbx_1_1allocation__aware__details_1_1move__assign__alloc", null ],
        [ "swap_alloc", "namespacemdbx_1_1allocation__aware__details.html#structmdbx_1_1allocation__aware__details_1_1swap__alloc", null ]
      ] ],
      [ "buffer", "group__cxx__api.html#classmdbx_1_1buffer", "group__cxx__api_classmdbx_1_1buffer" ],
      [ "cursor", "group__cxx__api.html#classmdbx_1_1cursor", "group__cxx__api_classmdbx_1_1cursor" ],
      [ "cursor_managed", "group__cxx__api.html#classmdbx_1_1cursor__managed", "group__cxx__api_classmdbx_1_1cursor__managed" ],
      [ "default_capacity_policy", "group__cxx__data.html#structmdbx_1_1default__capacity__policy", null ],
      [ "env", "group__cxx__api.html#classmdbx_1_1env", "group__cxx__api_classmdbx_1_1env" ],
      [ "env_managed", "group__cxx__api.html#classmdbx_1_1env__managed", "group__cxx__api_classmdbx_1_1env__managed" ],
      [ "error", "group__cxx__exceptions.html#classmdbx_1_1error", "group__cxx__exceptions_classmdbx_1_1error" ],
      [ "exception", "group__cxx__exceptions.html#classmdbx_1_1exception", "group__cxx__exceptions_classmdbx_1_1exception" ],
      [ "exception_thunk", "group__cxx__exceptions.html#classmdbx_1_1exception__thunk", "group__cxx__exceptions_classmdbx_1_1exception__thunk" ],
      [ "fatal", "group__cxx__exceptions.html#classmdbx_1_1fatal", "group__cxx__exceptions_classmdbx_1_1fatal" ],
      [ "from_base58", "group__cxx__data.html#structmdbx_1_1from__base58", "group__cxx__data_structmdbx_1_1from__base58" ],
      [ "from_base64", "group__cxx__data.html#structmdbx_1_1from__base64", "group__cxx__data_structmdbx_1_1from__base64" ],
      [ "from_hex", "group__cxx__data.html#structmdbx_1_1from__hex", "group__cxx__data_structmdbx_1_1from__hex" ],
      [ "map_handle", "group__cxx__api.html#structmdbx_1_1map__handle", "group__cxx__api_structmdbx_1_1map__handle" ],
      [ "pair", "group__cxx__data.html#structmdbx_1_1pair", "group__cxx__data_structmdbx_1_1pair" ],
      [ "pair_result", "group__cxx__data.html#structmdbx_1_1pair__result", "group__cxx__data_structmdbx_1_1pair__result" ],
      [ "slice", "group__cxx__data.html#structmdbx_1_1slice", "group__cxx__data_structmdbx_1_1slice" ],
      [ "to_base58", "group__cxx__data.html#structmdbx_1_1to__base58", "group__cxx__data_structmdbx_1_1to__base58" ],
      [ "to_base64", "group__cxx__data.html#structmdbx_1_1to__base64", "group__cxx__data_structmdbx_1_1to__base64" ],
      [ "to_hex", "group__cxx__data.html#structmdbx_1_1to__hex", "group__cxx__data_structmdbx_1_1to__hex" ],
      [ "txn", "group__cxx__api.html#classmdbx_1_1txn", "group__cxx__api_classmdbx_1_1txn" ],
      [ "txn_managed", "group__cxx__api.html#classmdbx_1_1txn__managed", "group__cxx__api_classmdbx_1_1txn__managed" ],
      [ "value_result", "group__cxx__data.html#structmdbx_1_1value__result", "group__cxx__data_structmdbx_1_1value__result" ]
    ] ],
    [ "std", "namespacestd.html", [
      [ "hash<::mdbx::slice >", "group__cxx__api.html#structstd_1_1hash_3_1_1mdbx_1_1slice_01_4", "group__cxx__api_structstd_1_1hash_3_1_1mdbx_1_1slice_01_4" ]
    ] ],
    [ "MDBX_build_info", "group__c__api.html#struct_m_d_b_x__build__info", "group__c__api_struct_m_d_b_x__build__info" ],
    [ "MDBX_canary", "group__c__api.html#struct_m_d_b_x__canary", "group__c__api_struct_m_d_b_x__canary" ],
    [ "MDBX_commit_latency", "group__c__api.html#struct_m_d_b_x__commit__latency", "group__c__api_struct_m_d_b_x__commit__latency" ],
    [ "MDBX_envinfo", "group__c__api.html#struct_m_d_b_x__envinfo", "group__c__api_struct_m_d_b_x__envinfo" ],
    [ "MDBX_envinfo.mi_bootid", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid", "group__c__api_struct_m_d_b_x__envinfo_8mi__bootid" ],
    [ "MDBX_envinfo.mi_bootid.current", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8current", "group__c__api_struct_m_d_b_x__envinfo_8mi__bootid_8current" ],
    [ "MDBX_envinfo.mi_bootid.meta0", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta0", "group__c__api_struct_m_d_b_x__envinfo_8mi__bootid_8meta0" ],
    [ "MDBX_envinfo.mi_bootid.meta1", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta1", "group__c__api_struct_m_d_b_x__envinfo_8mi__bootid_8meta1" ],
    [ "MDBX_envinfo.mi_bootid.meta2", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__bootid_8meta2", "group__c__api_struct_m_d_b_x__envinfo_8mi__bootid_8meta2" ],
    [ "MDBX_envinfo.mi_geo", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__geo", "group__c__api_struct_m_d_b_x__envinfo_8mi__geo" ],
    [ "MDBX_envinfo.mi_pgop_stat", "group__c__api.html#struct_m_d_b_x__envinfo_8mi__pgop__stat", "group__c__api_struct_m_d_b_x__envinfo_8mi__pgop__stat" ],
    [ "MDBX_stat", "group__c__api.html#struct_m_d_b_x__stat", "group__c__api_struct_m_d_b_x__stat" ],
    [ "MDBX_txn_info", "group__c__api.html#struct_m_d_b_x__txn__info", "group__c__api_struct_m_d_b_x__txn__info" ],
    [ "MDBX_version_info", "group__c__api.html#struct_m_d_b_x__version__info", "group__c__api_struct_m_d_b_x__version__info" ],
    [ "MDBX_version_info.git", "group__c__api.html#struct_m_d_b_x__version__info_8git", "group__c__api_struct_m_d_b_x__version__info_8git" ]
];