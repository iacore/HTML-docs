var group__cxx__api_classmdbx_1_1env =
[
    [ "geometry", "structmdbx_1_1env_1_1geometry.html", "structmdbx_1_1env_1_1geometry" ],
    [ "limits", "structmdbx_1_1env_1_1limits.html", "structmdbx_1_1env_1_1limits" ],
    [ "operate_options", "structmdbx_1_1env_1_1operate__options.html", "structmdbx_1_1env_1_1operate__options" ],
    [ "operate_parameters", "structmdbx_1_1env_1_1operate__parameters.html", "structmdbx_1_1env_1_1operate__parameters" ],
    [ "reader_info", "structmdbx_1_1env_1_1reader__info.html", "structmdbx_1_1env_1_1reader__info" ],
    [ "reclaiming_options", "structmdbx_1_1env_1_1reclaiming__options.html", "structmdbx_1_1env_1_1reclaiming__options" ],
    [ "info", "group__cxx__api.html#a6b4196213fa97a9608402e99a20c24d5", null ],
    [ "stat", "group__cxx__api.html#af10e2a3734585c9c8b3970a00a112b75", null ],
    [ "durability", "group__cxx__api.html#a21b1c4fc9a921d3ff8885aef43099336", [
      [ "robust_synchronous", "group__cxx__api.html#a21b1c4fc9a921d3ff8885aef43099336ad7e0f6fd854c5f5999744e94b04da73f", null ],
      [ "half_synchronous_weak_last", "group__cxx__api.html#a21b1c4fc9a921d3ff8885aef43099336af49dcd169b42a9c4a9a92881a31220ed", null ],
      [ "lazy_weak_tail", "group__cxx__api.html#a21b1c4fc9a921d3ff8885aef43099336a038a39d79ccf3740408d13ff2c6706aa", null ],
      [ "whole_fragile", "group__cxx__api.html#a21b1c4fc9a921d3ff8885aef43099336aef21f29e5ff6ebba116f8208a2f1ff20", null ]
    ] ],
    [ "mode", "group__cxx__api.html#a66d3e23137a274723052cb4d55485912", [
      [ "readonly", "group__cxx__api.html#a66d3e23137a274723052cb4d55485912aa5c227c6858f038ab52d0c3b73d5b84c", null ],
      [ "write_file_io", "group__cxx__api.html#a66d3e23137a274723052cb4d55485912ae938247fca29e9de3183ed62fdda4c62", null ],
      [ "write_mapped_io", "group__cxx__api.html#a66d3e23137a274723052cb4d55485912a4daf9ddda3049cc8149476b9c95dafac", null ]
    ] ],
    [ "remove_mode", "group__cxx__api.html#a903e64cb8e0eaca469560f403d509858", [
      [ "just_remove", "group__cxx__api.html#a903e64cb8e0eaca469560f403d509858abf2a7d31fae829559bd1e293daed5cf0", null ],
      [ "ensure_unused", "group__cxx__api.html#a903e64cb8e0eaca469560f403d509858a30017f7b7d020093b401ee4b5d4def66", null ],
      [ "wait_for_unused", "group__cxx__api.html#a903e64cb8e0eaca469560f403d509858a78c4dbcc2ad92529dc8040940fa52172", null ]
    ] ],
    [ "env", "group__cxx__api.html#ga820b9df67456914d3d9785eac465c9d2", null ],
    [ "env", "group__cxx__api.html#ac2faa99fdc74340ff0ca8ea517dd21d7", null ],
    [ "env", "group__cxx__api.html#a9c3ced91b6300b294dfb05393e617748", null ],
    [ "env", "group__cxx__api.html#ga8a2e3fb801d29e9d88d4388079352cef", null ],
    [ "~env", "group__cxx__api.html#gabb17706513641a304aafbdd62bc930fc", null ],
    [ "alter_flags", "group__cxx__api.html#ga9972b682eed6bb866bef90ac2a20517e", null ],
    [ "check_readers", "group__cxx__api.html#gadc736f8be68d4377511f303169724bbc", null ],
    [ "close_map", "group__cxx__api.html#ga149bf12e28c4e142f6eb251dc5269894", null ],
    [ "copy", "group__cxx__api.html#abac5360bc339ddc49a4e49f9c4bdda69", null ],
    [ "copy", "group__cxx__api.html#aee568ff580ba3d5b5f6e2375f83eff84", null ],
    [ "copy", "group__cxx__api.html#a4db0f3937bb53d3d5b1c94b8f69af65b", null ],
    [ "copy", "group__cxx__api.html#af4676e5e6fcded5d12359e0643516068", null ],
    [ "copy", "group__cxx__api.html#a2e15b849a755f0a1fd88d02423b2d824", null ],
    [ "copy", "group__cxx__api.html#a3f8096c27fc16046791decc5cc82b486", null ],
    [ "dbsize_max", "group__cxx__api.html#aa0c5652df5c0fb369cdc4414855269bc", null ],
    [ "dbsize_min", "group__cxx__api.html#a263cf136965400e117923e4629671576", null ],
    [ "enumerate_readers", "group__cxx__api.html#gac99902648fe38c252d6e7eb87cf04c8a", null ],
    [ "get_context", "group__cxx__api.html#ga922c9edf5fdaf87f88e48ffe212cc43f", null ],
    [ "get_durability", "group__cxx__api.html#ga2aa53a3ae3326d161295006989a56501", null ],
    [ "get_filehandle", "group__cxx__api.html#gae94e7ca5c230bba377f163a8a2c6b1e3", null ],
    [ "get_flags", "group__cxx__api.html#ga3a007be7c7a6494e9dddb66a4806ec0d", null ],
    [ "get_HandleSlowReaders", "group__cxx__api.html#ga4b189a17414e61a2ee37a66f8fca6eae", null ],
    [ "get_info", "group__cxx__api.html#gad482316dc6db5a717e6477c782326e1d", null ],
    [ "get_info", "group__cxx__api.html#gaac3aaa152b243871b2815530d58a060f", null ],
    [ "get_mode", "group__cxx__api.html#ga642bc7b7a341e5f5b83decd9fde0917b", null ],
    [ "get_operation_parameters", "group__cxx__api.html#ga3400c1e1c182f9d93599d6107cb81726", null ],
    [ "get_options", "group__cxx__api.html#ga7adb5b08274191450202d99d47d90692", null ],
    [ "get_pagesize", "group__cxx__api.html#aa7e2c5c2409c315b34a41fe38e12f6fd", null ],
    [ "get_path", "group__cxx__api.html#a1538ddd30d86bda4bd6241f6c77d85b0", null ],
    [ "get_reclaiming", "group__cxx__api.html#ga774529f25abc2f7134981a16ee4b55f5", null ],
    [ "get_stat", "group__cxx__api.html#gab05da202d3b8db18ab0dfe25bb2d3736", null ],
    [ "get_stat", "group__cxx__api.html#ga56002b6cb88bf7cb2d65d12fbd72b769", null ],
    [ "is_empty", "group__cxx__api.html#ab7f41d8cfacd43b05a74b2838855167f", null ],
    [ "is_pristine", "group__cxx__api.html#a830cf8334bbdfd956f5cca0b81887028", null ],
    [ "key_max", "group__cxx__api.html#a99df08c6183f1d34e008e6d0243c2619", null ],
    [ "key_min", "group__cxx__api.html#afc7a93cb0882d7642df0d3e4bfd7c889", null ],
    [ "max_maps", "group__cxx__api.html#gac55070e119d22b27b200c58d7c581f2c", null ],
    [ "max_readers", "group__cxx__api.html#gac8816034ea26391b90835aa6fcc270c3", null ],
    [ "operator const MDBX_env *", "group__cxx__api.html#gaaec8f0dccd4d6e6583ce6aeb99db0763", null ],
    [ "operator MDBX_env *", "group__cxx__api.html#ga33cbaa92d02830570a8a668a2dacfc40", null ],
    [ "operator=", "group__cxx__api.html#ga1eb106aa826b3f634d92e4eec389fb8c", null ],
    [ "poll_sync_to_disk", "group__cxx__api.html#a97644bb1dfc3ded8f70d8c1d924296d0", null ],
    [ "prepare_read", "group__cxx__api.html#ga9345f65285fc7730489edebb81983263", null ],
    [ "set_context", "group__cxx__api.html#ga694991af52d298824a98270577702997", null ],
    [ "set_geometry", "group__cxx__api.html#gae9a67c291124611cbcfa46a0c807c88f", null ],
    [ "set_HandleSlowReaders", "group__cxx__api.html#ga0402de0ad8a5bf4cdc6f82f992b7c9e8", null ],
    [ "set_sync_period", "group__cxx__api.html#ga4d22cc9e2141fb93a8829b8e4380534a", null ],
    [ "set_sync_period", "group__cxx__api.html#ga00a5b449161b1aeb4dedb0bc155a479a", null ],
    [ "set_sync_threshold", "group__cxx__api.html#ga15083dd76d08f191a88fda69ee0e24b4", null ],
    [ "start_read", "group__cxx__api.html#gac377d5feb84c8d6af2b79b136a2649c3", null ],
    [ "start_write", "group__cxx__api.html#ga5094eedaff003fdf1b41f275eb958135", null ],
    [ "sync_to_disk", "group__cxx__api.html#ga5dc4e6269e8905c512c023967c33af9a", null ],
    [ "transaction_size_max", "group__cxx__api.html#aaefce64356669fecc60bf841e9f1ac33", null ],
    [ "try_start_write", "group__cxx__api.html#gaf8ef02e8b25659e7fe350a41e8a3bc77", null ],
    [ "value_max", "group__cxx__api.html#add30d4e021602c6a449e7721681d7bd7", null ],
    [ "value_min", "group__cxx__api.html#af1901bf285a87e565f56ee7d6e06f486", null ],
    [ "operator!=", "group__cxx__api.html#a41a4e8d53e6ce9f9d85f2512bff5cfe7", null ],
    [ "operator==", "group__cxx__api.html#a09fa9e97e93b0a81b2fc1005a6964523", null ],
    [ "txn", "group__cxx__api.html#afa3b547f6b8fae932f3413d1646e13eb", null ],
    [ "handle_", "group__cxx__api.html#a6d56c15bb55ce7db99c41ce5acb9a9a6", null ]
];