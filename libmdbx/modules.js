var modules =
[
    [ "C API", "group__c__api.html", "group__c__api" ],
    [ "Common Macros", "group__api__macros.html", "group__api__macros" ],
    [ "SYNC MODES", "group__sync__modes.html", "group__sync__modes" ],
    [ "C++ API", "group__cxx__api.html", "group__cxx__api" ],
    [ "Build options", "group__build__option.html", "group__build__option" ]
];