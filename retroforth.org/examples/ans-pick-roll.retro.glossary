+

  Data:  nn-n
  Addr:  -
  Float: -

Add `n1` to `n2` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 + 

,

  Data:  n-
  Addr:  -
  Float: -

Store the specified value into the memory at `here` and increment `Heap` by 1.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    $a , $b , $c , #0 ,

-

  Data:  nn-n
  Addr:  -
  Float: -

Subtract `n2` from `n1` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #2 #1 - 

---reveal---

  Data:  -
  Addr:  -
  Float: -

Switch to the exposed (public) portion of a lexical namespace.

Class: class:word | Namespace: global | Interface Layer: all

;

  Data:  -
  Addr:  -
  Float: -

End the current definition.

Class: class:macro | Namespace: global | Interface Layer: all

[

  Data:  -
  Addr:  -
  Float: -

Begin a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

]

  Data:  -
  Addr:  -
  Float: -

End a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

depth

  Data:  -n
  Addr:  -
  Float: -

Return the number of items on the stack.

Class: class:word | Namespace: global | Interface Layer: all

dip

  Data:  nq-n
  Addr:  -
  Float: -

Temporarily remove n from the stack, execute the quotation, and then restore n to the stack.

Class: class:word | Namespace: global | Interface Layer: all

drop

  Data:  n-
  Addr:  -
  Float: -

Discard the top value on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

dup

  Data:  n-nn
  Addr:  -
  Float: -

Duplicate the top item on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

fetch

  Data:  a-n
  Addr:  -
  Float: -

Fetch the value stored at the specified address.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    &Version fetch

fetch-next

  Data:  a-an
  Addr:  -
  Float: -

Fetch the value stored at the specified address. Returns the next address and the value.

Class: class:word | Namespace: global | Interface Layer: all

here

  Data:  -a
  Addr:  -
  Float: -

Return the next free address in memory.

Class: class:word | Namespace: global | Interface Layer: all

n:dec

  Data:  n-m
  Addr:  -
  Float: -

Decrement n by one.

Class: class:word | Namespace: n | Interface Layer: all

Example #1:

    #100 n:dec

over

  Data:  nm-nmn
  Addr:  -
  Float: -

Put a copy of n over m.

Class: class:word | Namespace: global | Interface Layer: all

swap

  Data:  nm-mn
  Addr:  -
  Float: -

Exchange the position of the top two items on the stack

Class: class:primitive | Namespace: global | Interface Layer: all

times

  Data:  nq-
  Addr:  -
  Float: -

Run the specified quote the specified number of times.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #12 [ $- c:put ] times

v:preserve

  Data:  aq-
  Addr:  -
  Float: -

Make a copy of the value at the address, then run the quote. Once the quote completes, restore the address to the specified value.

Class: class:word | Namespace: v | Interface Layer: all

{{

  Data:  -
  Addr:  -
  Float: -

Begin a lexically scoped area.

Class: class:word | Namespace: global | Interface Layer: all

}}

  Data:  -
  Addr:  -
  Float: -

End a lexically scoped area. This will hide any headers between `{{` and `---reveal---`, leaving only headers between `---reveal---` and the `}}` visible.

Class: class:word | Namespace: global | Interface Layer: all

sigil:&

  Data:  s-a
  Addr:  -
  Float: -

Return a pointer to a named item. If name is not found, returns 0.

Interpret Time:
  Lookup name in dictionary, return contents of the xt field on the stack.

Compile Time:
  Lookup name in dictionary, compile code to push the contents of the xt field into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:(

  Data:  s-
  Addr:  -
  Float: -

Process token as a comment.

Interpret Time:
  Discard the string.

Compile Time:
  Discard the string.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil::

  Data:  s-
  Addr:  -
  Float: -

Hook. Process token as a new definition.

Interpret Time:
  Create a header pointing to `here` with the provided string as the name. Sets class to `class:word`.

Class: class:macro | Namespace: sigil | Interface Layer: all

