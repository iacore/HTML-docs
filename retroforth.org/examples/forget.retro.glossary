+

  Data:  nn-n
  Addr:  -
  Float: -

Add `n1` to `n2` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 + 

---reveal---

  Data:  -
  Addr:  -
  Float: -

Switch to the exposed (public) portion of a lexical namespace.

Class: class:word | Namespace: global | Interface Layer: all

;

  Data:  -
  Addr:  -
  Float: -

End the current definition.

Class: class:macro | Namespace: global | Interface Layer: all

FALSE

  Data:  -n
  Addr:  -
  Float: -

Returns `0`, the value used to indicate a FALSE result.

Class: class:word | Namespace: global | Interface Layer: all

TRUE

  Data:  -n
  Addr:  -
  Float: -

Returns `-1`, the value used to indicate a TRUE result.

Class: class:word | Namespace: global | Interface Layer: all

[

  Data:  -
  Addr:  -
  Float: -

Begin a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

]

  Data:  -
  Addr:  -
  Float: -

End a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

and

  Data:  nm-o
  Addr:  -
  Float: -

Perform a bitwise AND operation between the two provided values.

Class: class:primitive | Namespace: global | Interface Layer: all

bi@

  Data:  xyq-?
  Addr:  -
  Float: -

Execute q against x, then execute q against y.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #10 #20 [ #3 * ] bi@

choose

  Data:  fqq-
  Addr:  -
  Float: -

Execute q1 if the flag is true (-1) or q2 if the flag is false (0). Only these flags are valid when using `choose`; passing other values as flags will result in memory corruption.

Class: class:word | Namespace: global | Interface Layer: all

d:for-each

  Data:  q-
  Addr:  -
  Float: -

Execute the specified quote once for each header in the dictionary. Before running the quote, this also pushes a pointer to the header onto the stack.

Class: class:word | Namespace: d | Interface Layer: all

d:last

  Data:  -d
  Addr:  -
  Float: -

Return the most recent dictionary header.

Class: class:word | Namespace: d | Interface Layer: all

d:link

  Data:  d-a
  Addr:  -
  Float: -

Given a dictionary header, return the link field.

Class: class:word | Namespace: d | Interface Layer: all

d:lookup

  Data:  s-d
  Addr:  -
  Float: -

Lookup the specified name in the dictionary and return a pointer to its dictionary header. This returns zero if the word is not found. This also sets an internal variable ('which' in retro.muri) to the header address.

Class: class:word | Namespace: d | Interface Layer: all

d:name

  Data:  d-s
  Addr:  -
  Float: -

Given a dictionary header, return the name field.

Class: class:word | Namespace: d | Interface Layer: all

drop

  Data:  n-
  Addr:  -
  Float: -

Discard the top value on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

dup

  Data:  n-nn
  Addr:  -
  Float: -

Duplicate the top item on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

dup-pair

  Data:  nm-nmnm
  Addr:  -
  Float: -

Duplicate the top two items on the stack.

Class: class:word | Namespace: global | Interface Layer: all

f:depth

  Data:  -n
  Addr:  -
  Float: -

Return the number of items on the floating-point stack.

Class: class:word | Namespace: f | Interface Layer: rre

f:drop

  Data:  -
  Addr:  -
  Float: F-

Discard the top item on the floating-point stack.

Class: class:word | Namespace: f | Interface Layer: rre

fetch

  Data:  a-n
  Addr:  -
  Float: -

Fetch the value stored at the specified address.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    &Version fetch

here

  Data:  -a
  Addr:  -
  Float: -

Return the next free address in memory.

Class: class:word | Namespace: global | Interface Layer: all

if

  Data:  fq-
  Addr:  -
  Float: -

Execute the quote if the flag is `TRUE`.

Class: class:word | Namespace: global | Interface Layer: all

include

  Data:  s-
  Addr:  -
  Float: -

Run the code in the specified file. 

Class: class:word | Namespace: global | Interface Layer: rre

listen

  Data:  -
  Addr:  -
  Float: -

"Run interactive ""listener"" (a REPL)."

Class: class:word | Namespace: global | Interface Layer: rre

n:-zero?

  Data:  n-f
  Addr:  -
  Float: -

Return `TRUE` if number is not zero, or `FALSE` otherwise.

Class: class:word | Namespace: n | Interface Layer: all

n:zero?

  Data:  n-f
  Addr:  -
  Float: -

Return `TRUE` if number is zero, or `FALSE` otherwise.

Class: class:word | Namespace: n | Interface Layer: all

nl

  Data:  -
  Addr:  -
  Float: -

Display a newline.

Class: class:word | Namespace: global | Interface Layer: all

reset

  Data:  ...-
  Addr:  -
  Float: -

Remove all items from the stack.

Class: class:word | Namespace: global | Interface Layer: all

s:copy

  Data:  sa-
  Addr:  -
  Float: -

Copy a string (s) to a destination (a). This will include the terminator character when copying.

Class: class:word | Namespace: s | Interface Layer: all

s:eq?

  Data:  ss-f
  Addr:  -
  Float: -

Compare two strings for equality. Return `TRUE` if identical or `FALSE` if not.

Class: class:word | Namespace: s | Interface Layer: all

Example #1:

    'hello 'again s:eq?
    'test  'test  s:eq?

s:format

  Data:  ...s-s
  Addr:  -
  Float: -

Construct a new string using the template passed and items from the stack.

Class: class:word | Namespace: s | Interface Layer: all

s:put

  Data:  s-
  Addr:  -
  Float: -

Display a string.

Class: class:word | Namespace: global | Interface Layer: all

sip

  Data:  nq(?n-?)-n
  Addr:  -
  Float: -

Run quote. After execution completes, put a copy of n back on top of the stack.

Class: class:word | Namespace: global | Interface Layer: all

sp

  Data:  -
  Addr:  -
  Float: -

Display a space (`ASCII:SPACE`)

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    :spaces (n-)  [ sp ] times ;
    #12 spaces

store

  Data:  na-
  Addr:  -
  Float: -

Store a value into the specified address.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    'Base var
    #10 &Base store

swap

  Data:  nm-mn
  Addr:  -
  Float: -

Exchange the position of the top two items on the stack

Class: class:primitive | Namespace: global | Interface Layer: all

times

  Data:  nq-
  Addr:  -
  Float: -

Run the specified quote the specified number of times.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #12 [ $- c:put ] times

until

  Data:  q(-f)-
  Addr:  -
  Float: -

Execute quote repeatedly while the quote returns a value of `FALSE`. The quote should return a flag of either `TRUE` or `FALSE`, though `until` will treat any non-zero value as `TRUE`.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #10 [ dup n:put nl n:dec dup n:zero? ] until

var

  Data:  s-
  Addr:  -
  Float: -

Create a variable. The variable is initialized to 0.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    'Base var

{{

  Data:  -
  Addr:  -
  Float: -

Begin a lexically scoped area.

Class: class:word | Namespace: global | Interface Layer: all

}}

  Data:  -
  Addr:  -
  Float: -

End a lexically scoped area. This will hide any headers between `{{` and `---reveal---`, leaving only headers between `---reveal---` and the `}}` visible.

Class: class:word | Namespace: global | Interface Layer: all

sigil:!

  Data:  ns-
  Addr:  -
  Float: -

Store a value into a variable.

Interpret Time:
  Store a value into the named variable.

Compile Time:
  Compile the code to store a value into a named variable.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:#

  Data:  s-n
  Addr:  -
  Float: -

Process token as a number.

Interpret Time:
  Convert the string into a number and leave on the stack.

Compile Time:
  Convert the string into a number and compile into the current definition as a literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:&

  Data:  s-a
  Addr:  -
  Float: -

Return a pointer to a named item. If name is not found, returns 0.

Interpret Time:
  Lookup name in dictionary, return contents of the xt field on the stack.

Compile Time:
  Lookup name in dictionary, compile code to push the contents of the xt field into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:'

  Data:  s-s
  Addr:  -
  Float: -

Process token as a string.

Interpret Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space.

Compile Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space. Then compile the string into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:(

  Data:  s-
  Addr:  -
  Float: -

Process token as a comment.

Interpret Time:
  Discard the string.

Compile Time:
  Discard the string.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil::

  Data:  s-
  Addr:  -
  Float: -

Hook. Process token as a new definition.

Interpret Time:
  Create a header pointing to `here` with the provided string as the name. Sets class to `class:word`.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:@

  Data:  s-n
  Addr:  -
  Float: -

Fetch from a stored variable.

Interpret Time:
  Fetch a value from a named variable.

Compile Time:
  Compile the code to fetch a value from a named variable into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

