*

  Data:  nn-n
  Addr:  -
  Float: -

Multiply `n1` by `n2` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #2 #6 *
    #-1 #100 *

+

  Data:  nn-n
  Addr:  -
  Float: -

Add `n1` to `n2` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 + 

,

  Data:  n-
  Addr:  -
  Float: -

Store the specified value into the memory at `here` and increment `Heap` by 1.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    $a , $b , $c , #0 ,

-

  Data:  nn-n
  Addr:  -
  Float: -

Subtract `n2` from `n1` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #2 #1 - 

---reveal---

  Data:  -
  Addr:  -
  Float: -

Switch to the exposed (public) portion of a lexical namespace.

Class: class:word | Namespace: global | Interface Layer: all

-eq?

  Data:  nn-f
  Addr:  -
  Float: -

Compare two values for inequality. Returns `TRUE` if they are not equal or `FALSE` otherwise.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 -eq?
    $a $b -eq?

/

  Data:  nm-v
  Addr:  -
  Float: -

Divide `n` by `m` and return the integer part of the quotient.

Class: class:word | Namespace: global | Interface Layer: all

/mod

  Data:  nm-rv
  Addr:  -
  Float: -

Divide `n` by `m` and return the integer part of the quotient and remainder.

Class: class:primitive | Namespace: global | Interface Layer: all

0;

  Data:  n-n || n-
  Addr:  -
  Float: -

If `n` is zero, drop `n` and exit the current word. If non-zero, leave `n` alone and allow execution to continue.

Class: class:macro | Namespace: global | Interface Layer: all

;

  Data:  -
  Addr:  -
  Float: -

End the current definition.

Class: class:macro | Namespace: global | Interface Layer: all

ASCII:SPACE

  Data:  -n
  Addr:  -
  Float: -

Constant. Refers to specific ASCII code.

Class: class:data | Namespace: ASCII | Interface Layer: all

TIB

  Data:  -a
  Addr:  -
  Float: -

Constant. Returns a pointer to the text input buffer.

Class: class:data | Namespace: global | Interface Layer: rre

[

  Data:  -
  Addr:  -
  Float: -

Begin a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

]

  Data:  -
  Addr:  -
  Float: -

End a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

a:for-each

  Data:  aq-
  Addr:  -
  Float: -

Execute the quote once for each item in the array.

Class: class:word | Namespace: a | Interface Layer: all

again

  Data:  -
  Addr:  -
  Float: -

Close an unconditional loop. Branches back to the prior `repeat`.

Class: class:macro | Namespace: global | Interface Layer: all

allot

  Data:  n-
  Addr:  -
  Float: -

Allocate the specified number of cells from the `Heap`.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    'Buffer d:create  #100 allot

and

  Data:  nm-o
  Addr:  -
  Float: -

Perform a bitwise AND operation between the two provided values.

Class: class:primitive | Namespace: global | Interface Layer: all

c:put

  Data:  c-
  Addr:  -
  Float: -

Vectored. Display a single character.

Class: class:word | Namespace: global | Interface Layer: all

call

  Data:  a-
  Addr:  -
  Float: -

Call a function.

Class: class:primitive | Namespace: global | Interface Layer: all

case

  Data:  nmq- || nmq-n
  Addr:  -
  Float: -

If `n` is equal to `m`, drop both and execute the specified quote before exiting the calling word. If not equal, leave `n` on the stack and let execution continue.

Class: class:word | Namespace: global | Interface Layer: all

choose

  Data:  fqq-
  Addr:  -
  Float: -

Execute q1 if the flag is true (-1) or q2 if the flag is false (0). Only these flags are valid when using `choose`; passing other values as flags will result in memory corruption.

Class: class:word | Namespace: global | Interface Layer: all

const

  Data:  ns-
  Addr:  -
  Float: -

Create a constant returning the specified value.

Class: class:word | Namespace: global | Interface Layer: all

d:create

  Data:  s-
  Addr:  -
  Float: -

Hook. Create a new dictionary header named the specified string. The new header will point to `here` and have a class of `class:data`.

Class: class:word | Namespace: d | Interface Layer: all

d:name

  Data:  d-s
  Addr:  -
  Float: -

Given a dictionary header, return the name field.

Class: class:word | Namespace: d | Interface Layer: all

d:xt

  Data:  d-a
  Addr:  -
  Float: -

Given a dictionary header, return the xt field.

Class: class:word | Namespace: d | Interface Layer: all

drop

  Data:  n-
  Addr:  -
  Float: -

Discard the top value on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

dup

  Data:  n-nn
  Addr:  -
  Float: -

Duplicate the top item on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

eq?

  Data:  nn-f
  Addr:  -
  Float: -

Compare two values for equality. Returns `TRUE` if they are equal or `FALSE` otherwise.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 eq?
    $a $b eq?

fetch

  Data:  a-n
  Addr:  -
  Float: -

Fetch the value stored at the specified address.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    &Version fetch

fetch-next

  Data:  a-an
  Addr:  -
  Float: -

Fetch the value stored at the specified address. Returns the next address and the value.

Class: class:word | Namespace: global | Interface Layer: all

file:R

  Data:  -n
  Addr:  -
  Float: -

Constant for opening a file in READ mode.

Class: class:data | Namespace: file | Interface Layer: rre

file:W

  Data:  -n
  Addr:  -
  Float: -

Constant for opening a file in WRITE mode.

Class: class:data | Namespace: file | Interface Layer: rre

file:close

  Data:  h-
  Addr:  -
  Float: -

Given a file handle, close the file.

Class: class:word | Namespace: file | Interface Layer: rre

file:for-each-line

  Data:  sq-
  Addr:  -
  Float: -

Given a file name, open it and run the quote once for each line in the file.

Class: class:word | Namespace: file | Interface Layer: rre

file:open

  Data:  sm-h
  Addr:  -
  Float: -

Open a named file (s) with the given mode (m). Returns a handle identifying the file.

Class: class:word | Namespace: file | Interface Layer: rre

Example #1:

  '/etc/motd file:R file:open

file:read

  Data:  h-c
  Addr:  -
  Float: -

Given a file handle, read and return the next character in it.

Class: class:word | Namespace: file | Interface Layer: rre

file:size

  Data:  h-n
  Addr:  -
  Float: -

Given a file handle, return the size of the file (in bytes).

Class: class:word | Namespace: file | Interface Layer: rre

file:write

  Data:  ch-
  Addr:  -
  Float: -

Write a character to the file represented by the handle.

Class: class:word | Namespace: file | Interface Layer: rre

gc

  Data:  a-
  Addr:  -
  Float: -

Save value of `Heap`, run the function provided, then restore `Heap`.

Class: class:word | Namespace: global | Interface Layer: all

gt?

  Data:  nn-f
  Addr:  -
  Float: -

Compare n1 and n2. Return `TRUE` if n1 is greater than n2, or `FALSE` otherwise.

Class: class:primitive | Namespace: global | Interface Layer: all

here

  Data:  -a
  Addr:  -
  Float: -

Return the next free address in memory.

Class: class:word | Namespace: global | Interface Layer: all

if

  Data:  fq-
  Addr:  -
  Float: -

Execute the quote if the flag is `TRUE`.

Class: class:word | Namespace: global | Interface Layer: all

lt?

  Data:  nn-f
  Addr:  -
  Float: -

Compare n1 and n2. Return `TRUE` if n1 is less than n2, or `FALSE` otherwise.

Class: class:primitive | Namespace: global | Interface Layer: all

n:-zero?

  Data:  n-f
  Addr:  -
  Float: -

Return `TRUE` if number is not zero, or `FALSE` otherwise.

Class: class:word | Namespace: n | Interface Layer: all

n:between?

  Data:  nlu-f
  Addr:  -
  Float: -

Return TRUE if number is between the lower (l) and upper (u) bounds. If not, return FALSE. This is inclusive of the limits.

Class: class:word | Namespace: n | Interface Layer: all

Example #1:

    #3 #1 #100 n:between?
    $q $a $b   n:between?

n:dec

  Data:  n-m
  Addr:  -
  Float: -

Decrement n by one.

Class: class:word | Namespace: n | Interface Layer: all

Example #1:

    #100 n:dec

n:inc

  Data:  n-m
  Addr:  -
  Float: -

Increment n by one.

Class: class:word | Namespace: n | Interface Layer: all

Example #1:

    #100 n:inc

n:put

  Data:  n-
  Addr:  -
  Float: -

Display a number.

Class: class:word | Namespace: global | Interface Layer: all

n:zero?

  Data:  n-f
  Addr:  -
  Float: -

Return `TRUE` if number is zero, or `FALSE` otherwise.

Class: class:word | Namespace: n | Interface Layer: all

nip

  Data:  nm-m
  Addr:  -
  Float: -

Remove the second item from the stack.

Class: class:word | Namespace: global | Interface Layer: all

nl

  Data:  -
  Addr:  -
  Float: -

Display a newline.

Class: class:word | Namespace: global | Interface Layer: all

not

  Data:  n-m
  Addr:  -
  Float: -

Perform a logical NOT operation.

Class: class:word | Namespace: global | Interface Layer: all

or

  Data:  mn-o
  Addr:  -
  Float: -

Perform a bitwise OR between the provided values.

Class: class:primitive | Namespace: global | Interface Layer: all

over

  Data:  nm-nmn
  Addr:  -
  Float: -

Put a copy of n over m.

Class: class:word | Namespace: global | Interface Layer: all

reorder

  Data:  ...ss-?
  Addr:  -
  Float: -

Restructure the order of items on the stack.

Class: class:word | Namespace: global | Interface Layer: all

repeat

  Data:  -
  Addr:  -
  Float: -

Begin an unconditional loop.

Class: class:macro | Namespace: global | Interface Layer: all

s:copy

  Data:  sa-
  Addr:  -
  Float: -

Copy a string (s) to a destination (a). This will include the terminator character when copying.

Class: class:word | Namespace: s | Interface Layer: all

s:eq?

  Data:  ss-f
  Addr:  -
  Float: -

Compare two strings for equality. Return `TRUE` if identical or `FALSE` if not.

Class: class:word | Namespace: s | Interface Layer: all

Example #1:

    'hello 'again s:eq?
    'test  'test  s:eq?

s:format

  Data:  ...s-s
  Addr:  -
  Float: -

Construct a new string using the template passed and items from the stack.

Class: class:word | Namespace: s | Interface Layer: all

s:length

  Data:  s-n
  Addr:  -
  Float: -

Return the number of characters in a string, excluding the NULL terminator.

Class: class:word | Namespace: s | Interface Layer: all

s:put

  Data:  s-
  Addr:  -
  Float: -

Display a string.

Class: class:word | Namespace: global | Interface Layer: all

s:tokenize

  Data:  sc-a
  Addr:  -
  Float: -

Takes a string and a character to use as a separator. It splits the string into substrings and returns an array containing pointers to each of them.

Class: class:word | Namespace: s | Interface Layer: all

script:get-argument

  Data:  n-s
  Addr:  -
  Float: -

Given an argument number, return the argument as a string.

Class: class:word | Namespace: script | Interface Layer: rre

shift

  Data:  mn-o
  Addr:  -
  Float: -

Peform a bitwise shift of m by n bits. If n is positive, shift right. If negative, the shift will be to the left.

Class: class:primitive | Namespace: global | Interface Layer: all

sp

  Data:  -
  Addr:  -
  Float: -

Display a space (`ASCII:SPACE`)

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    :spaces (n-)  [ sp ] times ;
    #12 spaces

store

  Data:  na-
  Addr:  -
  Float: -

Store a value into the specified address.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    'Base var
    #10 &Base store

swap

  Data:  nm-mn
  Addr:  -
  Float: -

Exchange the position of the top two items on the stack

Class: class:primitive | Namespace: global | Interface Layer: all

times

  Data:  nq-
  Addr:  -
  Float: -

Run the specified quote the specified number of times.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #12 [ $- c:put ] times

unpack

  Data:  n-nnnn
  Addr:  -
  Float: -

Unpack a 32-bit value into four 8-bit values.

Class: class:word | Namespace: global | Interface Layer: all

until

  Data:  q(-f)-
  Addr:  -
  Float: -

Execute quote repeatedly while the quote returns a value of `FALSE`. The quote should return a flag of either `TRUE` or `FALSE`, though `until` will treat any non-zero value as `TRUE`.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #10 [ dup n:put nl n:dec dup n:zero? ] until

v:dec

  Data:  a-
  Addr:  -
  Float: -

Decrement the value stored at the specified address by 1.

Class: class:word | Namespace: v | Interface Layer: all

v:inc

  Data:  a-
  Addr:  -
  Float: -

Increment the value stored at the specified address by 1.

Class: class:word | Namespace: v | Interface Layer: all

v:preserve

  Data:  aq-
  Addr:  -
  Float: -

Make a copy of the value at the address, then run the quote. Once the quote completes, restore the address to the specified value.

Class: class:word | Namespace: v | Interface Layer: all

var

  Data:  s-
  Addr:  -
  Float: -

Create a variable. The variable is initialized to 0.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    'Base var

var-n

  Data:  ns-
  Addr:  -
  Float: -

Create a variable with the specified initial value.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #10 'Base var-n

xor

  Data:  mn-o
  Addr:  -
  Float: -

Perform a bitwise XOR operation.

Class: class:primitive | Namespace: global | Interface Layer: all

{{

  Data:  -
  Addr:  -
  Float: -

Begin a lexically scoped area.

Class: class:word | Namespace: global | Interface Layer: all

}}

  Data:  -
  Addr:  -
  Float: -

End a lexically scoped area. This will hide any headers between `{{` and `---reveal---`, leaving only headers between `---reveal---` and the `}}` visible.

Class: class:word | Namespace: global | Interface Layer: all

sigil:!

  Data:  ns-
  Addr:  -
  Float: -

Store a value into a variable.

Interpret Time:
  Store a value into the named variable.

Compile Time:
  Compile the code to store a value into a named variable.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:#

  Data:  s-n
  Addr:  -
  Float: -

Process token as a number.

Interpret Time:
  Convert the string into a number and leave on the stack.

Compile Time:
  Convert the string into a number and compile into the current definition as a literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:$

  Data:  s-c
  Addr:  -
  Float: -

Process token as an ASCII character.

Interpret Time:
  Fetch first character from string. Leave on stack.

Compile Time:
  Fetch first character from the string. Compile into the current definition as  literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:&

  Data:  s-a
  Addr:  -
  Float: -

Return a pointer to a named item. If name is not found, returns 0.

Interpret Time:
  Lookup name in dictionary, return contents of the xt field on the stack.

Compile Time:
  Lookup name in dictionary, compile code to push the contents of the xt field into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:'

  Data:  s-s
  Addr:  -
  Float: -

Process token as a string.

Interpret Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space.

Compile Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space. Then compile the string into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:(

  Data:  s-
  Addr:  -
  Float: -

Process token as a comment.

Interpret Time:
  Discard the string.

Compile Time:
  Discard the string.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil::

  Data:  s-
  Addr:  -
  Float: -

Hook. Process token as a new definition.

Interpret Time:
  Create a header pointing to `here` with the provided string as the name. Sets class to `class:word`.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:@

  Data:  s-n
  Addr:  -
  Float: -

Fetch from a stored variable.

Interpret Time:
  Fetch a value from a named variable.

Compile Time:
  Compile the code to fetch a value from a named variable into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

