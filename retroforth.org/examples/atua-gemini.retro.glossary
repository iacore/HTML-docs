+

  Data:  nn-n
  Addr:  -
  Float: -

Add `n1` to `n2` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 + 

-if;

  Data:  fq-
  Addr:  -
  Float: -

Execute the quotation if the flag is `FALSE`. If false, also exit the word.

Class: class:word | Namespace: global | Interface Layer: all

;

  Data:  -
  Addr:  -
  Float: -

End the current definition.

Class: class:macro | Namespace: global | Interface Layer: all

ASCII:CR

  Data:  -n
  Addr:  -
  Float: -

Constant. Refers to specific ASCII code.

Class: class:data | Namespace: ASCII | Interface Layer: all

ASCII:LF

  Data:  -n
  Addr:  -
  Float: -

Constant. Refers to specific ASCII code.

Class: class:data | Namespace: ASCII | Interface Layer: all

FALSE

  Data:  -n
  Addr:  -
  Float: -

Returns `0`, the value used to indicate a FALSE result.

Class: class:word | Namespace: global | Interface Layer: all

TRUE

  Data:  -n
  Addr:  -
  Float: -

Returns `-1`, the value used to indicate a TRUE result.

Class: class:word | Namespace: global | Interface Layer: all

[

  Data:  -
  Addr:  -
  Float: -

Begin a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

]

  Data:  -
  Addr:  -
  Float: -

End a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

a:for-each

  Data:  aq-
  Addr:  -
  Float: -

Execute the quote once for each item in the array.

Class: class:word | Namespace: a | Interface Layer: all

bi@

  Data:  xyq-?
  Addr:  -
  Float: -

Execute q against x, then execute q against y.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #10 #20 [ #3 * ] bi@

c:put

  Data:  c-
  Addr:  -
  Float: -

Vectored. Display a single character.

Class: class:word | Namespace: global | Interface Layer: all

file:exists?

  Data:  s-f
  Addr:  -
  Float: -

Given a file name, return `TRUE` if it exists or `FALSE` if it does not.

Class: class:word | Namespace: file | Interface Layer: rre

file:slurp

  Data:  as-
  Addr:  -
  Float: -

Given an address and a file name, read the file contents into memory starting at the address.

Class: class:word | Namespace: file | Interface Layer: rre

here

  Data:  -a
  Addr:  -
  Float: -

Return the next free address in memory.

Class: class:word | Namespace: global | Interface Layer: all

if

  Data:  fq-
  Addr:  -
  Float: -

Execute the quote if the flag is `TRUE`.

Class: class:word | Namespace: global | Interface Layer: all

s:append

  Data:  ss-s
  Addr:  -
  Float: -

Return a new string consisting of s1 followed by s2.

Class: class:word | Namespace: s | Interface Layer: all

s:const

  Data:  ss-
  Addr:  -
  Float: -

Create a constant named s2, returning a pointer to s1. This will use `s:keep` to preserve the original string.

Class: class:word | Namespace: s | Interface Layer: all

s:ends-with?

  Data:  ss-f
  Addr:  -
  Float: -

Return `TRUE` if s1 ends with s2 or `FALSE` otherwise.

Class: class:word | Namespace: s | Interface Layer: all

s:get

  Data:  -s
  Addr:  -
  Float: -

Read input from standard in (via `c:get`) until a CR or LF is encountered. Returns a string.

Class: class:word | Namespace: all | Interface Layer: rre

s:keep

  Data:  s-s
  Addr:  -
  Float: -

Store a string into the heap and return a pointer to the start of it.

Class: class:word | Namespace: s | Interface Layer: all

s:put

  Data:  s-
  Addr:  -
  Float: -

Display a string.

Class: class:word | Namespace: global | Interface Layer: all

sp

  Data:  -
  Addr:  -
  Float: -

Display a space (`ASCII:SPACE`)

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    :spaces (n-)  [ sp ] times ;
    #12 spaces

store

  Data:  na-
  Addr:  -
  Float: -

Store a value into the specified address.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    'Base var
    #10 &Base store

var

  Data:  s-
  Addr:  -
  Float: -

Create a variable. The variable is initialized to 0.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    'Base var

{

  Data:  -
  Addr:  -
  Float: -

Begin an array. This is intended to make creating arrays a bit cleaner than using a quotation and `a:counted-results`.

Class: class:word | Namespace: global | Interface Layer: all

}

  Data:  -a
  Addr:  -
  Float: -

Complete an array begun by `{`. Returns a pointer to the data.

Class: class:word | Namespace: global | Interface Layer: all

sigil:!

  Data:  ns-
  Addr:  -
  Float: -

Store a value into a variable.

Interpret Time:
  Store a value into the named variable.

Compile Time:
  Compile the code to store a value into a named variable.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:#

  Data:  s-n
  Addr:  -
  Float: -

Process token as a number.

Interpret Time:
  Convert the string into a number and leave on the stack.

Compile Time:
  Convert the string into a number and compile into the current definition as a literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:&

  Data:  s-a
  Addr:  -
  Float: -

Return a pointer to a named item. If name is not found, returns 0.

Interpret Time:
  Lookup name in dictionary, return contents of the xt field on the stack.

Compile Time:
  Lookup name in dictionary, compile code to push the contents of the xt field into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:'

  Data:  s-s
  Addr:  -
  Float: -

Process token as a string.

Interpret Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space.

Compile Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space. Then compile the string into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil::

  Data:  s-
  Addr:  -
  Float: -

Hook. Process token as a new definition.

Interpret Time:
  Create a header pointing to `here` with the provided string as the name. Sets class to `class:word`.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:@

  Data:  s-n
  Addr:  -
  Float: -

Fetch from a stored variable.

Interpret Time:
  Fetch a value from a named variable.

Compile Time:
  Compile the code to fetch a value from a named variable into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

