<html><head>
<style>

    * {
      color: #cccccc;
      background: #2d2d2d;
      max-width: 700px;
    }
    
    tt, pre {
      background: #1d1f21; color: #b5bd68; font-family: monospace; 
      white-space: pre;
      display: block;
      width: 100%;
    }
    
    .indentedcode {
      margin-left: 2em;
      margin-right: 2em;
    }
    
    .codeblock {
      background: #1d1f21; color: #b5bd68; font-family: monospace; 
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
      padding: 7px;
    }
    
    .indentedlist {
      margin-left: 2em;
      color: #cccccc;
      background: #2d2d2d;
    }
    
    span { white-space: pre; background: #1d1f21; }
    .text  { color: #c5c8c6; white-space: pre }
    .colon { color: #cc6666; }
    .note  { color: #969896; }
    .str   { color: #f0c674; }
    .num   { color: #8abeb7; }
    .fnum  { color: #8abeb7; font-weight: bold; }
    .ptr   { color: #b294bb; font-weight: bold; }
    .fetch { color: #b294bb; }
    .store { color: #b294bb; }
    .char  { color: #81a2be; }
    .inst  { color: #de935f; }
    .defer { color: #888888; }
    .imm   { color: #de935f; }
    .prim  { color: #b5bd68; font-weight: bold; }
</style>
</head><body>
#!/usr/bin/env retro
<p>
<p>
I've been running a Gopher server written in RETRO since
2018. This server, named Atua, has served me quite well.
But it has one limit that sometimes proves annoying: there
is no support for generating a directory listing. Atua
only serves the data in a <tt style='display:inline'>gophermap</tt>.
<p>
I decided to rectify this in a way. Rather than altering
Atua to add more complexity, I decided to write a tool
which can generate the <tt style='display:inline'>gophermap</tt> automatically.
<p>
As a practical matter, the list will exclude files named
<tt style='display:inline'>gophermap</tt>, <tt style='display:inline'>HEADER</tt>, and <tt style='display:inline'>FOOTER</tt>. The generated file
will consist of the contents of <tt style='display:inline'>HEADER</tt>, the directory
entries, and the contents of <tt style='display:inline'>FOOTER</tt>.
<p>
Output will be written to standard output. Redirect to the
<tt style='display:inline'>gophermap</tt> file, or pipe it to another process for
examination or manipulation.
<p>
<p>
I begin by defining a word for dealing with pipes.
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='colon'>:pipe&gt;</span> &nbsp;<span class='note'>(s-s)</span> &nbsp;file:R unix:popen <span class='imm'>[</span> file:read-line <span class='imm'>]</span> <span class='imm'>[</span> unix:pclose <span class='imm'>]</span> bi <span class='imm'>;</span> </tt>
<tt>~~~</tt></div><p>
I then create a word to return the number of files in the
current directory. This makes use of a Unix pipe to run
<tt style='display:inline'>ls -l | wc -l</tt> and capture the result. I trim off any
whitespace and convert to a number.
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='colon'>:unix:count-files</span> <span class='note'>(-n)</span> </tt>
<tt>&nbsp;&nbsp;<span class='str'>'ls_-1_|_wc_-l</span> pipe&gt; s:trim s:to-number <span class='imm'>;</span> </tt>
<tt>~~~</tt></div><p>
Next, a word to identify the current working directory. This
also uses a pipe to <tt style='display:inline'>pwd</tt>.
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='colon'>:unix:get-cwd</span> <span class='note'>(-s)</span> </tt>
<tt>&nbsp;&nbsp;<span class='str'>'pwd</span> pipe&gt; s:trim <span class='str'>'/</span> s:append <span class='imm'>;</span> </tt>
<tt>~~~</tt></div><p>
The program accepts a single command line argument: the
physical base path to exclude. In Atua, there is a root
directory, and all selector paths are relative to this.
<p>
E.g., if the actual root is <tt style='display:inline'>/home/atua/gopherspace/</tt> then
launching this program as:
<p>
<tt class='indentedcode'>atua-gophermap.forth&nbsp;/home/atua/gopherspace</tt>
<p>
will strip the actual root path off, allowing the selectors
to work as expected.
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='num'>#0</span> script:get-argument s:length <span class='str'>'SKIP</span> const </tt>
<tt>~~~</tt></div><p>
So with these defined, I define a couple of constants using
them for later use.
<p>
<div class='codeblock'><tt>~~~</tt><tt>unix:get-cwd SKIP <span class='prim'>+</span> &nbsp;<span class='str'>'BASE</span> s:const </tt>
<tt>unix:count-files &nbsp;&nbsp;&nbsp;&nbsp;<span class='str'>'FILES</span> const </tt>
<tt>~~~</tt></div><p>
Ok, now for a useful combinator. I want to be able to run
something once for each file or directory in the current
directory. One option would be to read the names and
construct a set, then use <tt style='display:inline'>a:for-each</tt>. I decided to take
a different path: I implement a word to open a pipe, read a
single line, then run a quote against it.
<p>
With this, something like <tt style='display:inline'>ls</tt> can be defined as:
<p>
<tt class='indentedcode'>:ls&nbsp;[&nbsp;s:put&nbsp;nl&nbsp;]&nbsp;unix:for-each-file&nbsp;;</tt>
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='colon'>:unix:for-each-file</span> <span class='note'>(q-)</span> </tt>
<tt>&nbsp;&nbsp;<span class='str'>'ls_-1_-p</span> file:R unix:popen </tt>
<tt>&nbsp;&nbsp;unix:count-files-in-cwd </tt>
<tt>&nbsp;&nbsp;<span class='imm'>[</span> <span class='imm'>[</span> file:read-line s:temp <span class='prim'>over</span> <span class='prim'>call</span> <span class='imm'>]</span> sip <span class='imm'>]</span> times </tt>
<tt>&nbsp;&nbsp;unix:pclose <span class='prim'>drop</span> <span class='imm'>;</span> </tt>
<tt>~~~</tt></div><p>
<p>
Begin by displaying HEADER (if it exists).
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='str'>'HEADER</span> file:exists? </tt>
<tt>&nbsp;&nbsp;<span class='imm'>[</span> here <span class='str'>'HEADER</span> file:slurp here s:put nl <span class='imm'>]</span> <span class='prim'>if</span> </tt>
<tt>~~~</tt></div><p>
Next, list any directories. If a file name ends with a <tt style='display:inline'>/</tt>,
I assume it is a directory.
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='colon'>:dir?</span> <span class='note'>(s-sf)</span> </tt>
<tt>&nbsp;&nbsp;<span class='prim'>dup</span> s:length <span class='prim'>over</span> <span class='prim'>+</span> n:dec <span class='prim'>fetch</span> <span class='char'>$/</span> <span class='prim'>eq?</span> <span class='imm'>;</span> </tt>
<tt>~~~</tt></div><p>
A directory entry needs the following form:
<p>
<tt class='indentedcode'>0description&lt;tab&gt;selector&lt;newline&gt;</tt>
<p>
I am using the directory name as the description (with a
trailing slash), and the relative path (without the final
slash) as the selector.
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='colon'>:selector</span> <span class='note'>(filename-selector)</span> </tt>
<tt>&nbsp;&nbsp;BASE s:prepend s:chop <span class='imm'>;</span> </tt>
<tt>&nbsp;</tt>
<tt><span class='colon'>:dir-entry</span> <span class='note'>(filename)</span> </tt>
<tt>&nbsp;&nbsp;<span class='char'>$1</span> c:put <span class='prim'>dup</span> s:put tab selector s:put nl <span class='imm'>;</span> </tt>
<tt>&nbsp;</tt>
<tt><span class='imm'>[</span> dir? <span class='ptr'>&amp;dir-entry</span> <span class='ptr'>&amp;drop</span> choose <span class='imm'>]</span> unix:for-each-file </tt>
<tt>~~~</tt></div><p>
Next, list files. This is harder because files can have
different types.
<p>
I start with a word to decide if the item is a file. This
will ignore directories (ending in a <tt style='display:inline'>/</tt>), <tt style='display:inline'>HEADER</tt>, <tt style='display:inline'>FOOTER</tt>,
and <tt style='display:inline'>gophermap</tt> files.
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='colon'>:file?</span> <span class='note'>(s-sf)</span> </tt>
<tt>&nbsp;&nbsp;<span class='prim'>dup</span> <span class='str'>'HEADER</span> &nbsp;&nbsp;&nbsp;<span class='imm'>[</span> FALSE <span class='imm'>]</span> s:case </tt>
<tt>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='str'>'FOOTER</span> &nbsp;&nbsp;&nbsp;<span class='imm'>[</span> FALSE <span class='imm'>]</span> s:case </tt>
<tt>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='str'>'gophermap</span> <span class='imm'>[</span> FALSE <span class='imm'>]</span> s:case </tt>
<tt>&nbsp;&nbsp;<span class='prim'>drop</span> dir? not <span class='imm'>;</span> </tt>
<tt>~~~</tt></div><p>
Then I look to see if it has a file extension.
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='colon'>:has-extension?</span> <span class='note'>(s-sf)</span> </tt>
<tt>&nbsp;&nbsp;<span class='prim'>dup</span> <span class='char'>$.</span> s:contains/char? <span class='imm'>;</span> </tt>
<tt>~~~</tt></div><p>
If there is an extension, it can be mapped to a type code.
I do this with a simple <tt style='display:inline'>s:case</tt> construct, defaulting to
a binary (type 9) file if I don't recognize the extension.
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='colon'>:file-type</span> </tt>
<tt>&nbsp;&nbsp;<span class='prim'>dup</span> <span class='char'>$.</span> s:split/char <span class='prim'>drop</span> </tt>
<tt>&nbsp;&nbsp;<span class='str'>'.forth</span> <span class='imm'>[</span> <span class='char'>$0</span> <span class='imm'>]</span> s:case </tt>
<tt>&nbsp;&nbsp;<span class='str'>'.md</span> &nbsp;&nbsp;&nbsp;<span class='imm'>[</span> <span class='char'>$0</span> <span class='imm'>]</span> s:case </tt>
<tt>&nbsp;&nbsp;<span class='str'>'.txt</span> &nbsp;&nbsp;<span class='imm'>[</span> <span class='char'>$0</span> <span class='imm'>]</span> s:case </tt>
<tt>&nbsp;&nbsp;<span class='str'>'.htm</span> &nbsp;&nbsp;<span class='imm'>[</span> <span class='char'>$h</span> <span class='imm'>]</span> s:case </tt>
<tt>&nbsp;&nbsp;<span class='str'>'.html</span> &nbsp;<span class='imm'>[</span> <span class='char'>$h</span> <span class='imm'>]</span> s:case </tt>
<tt>&nbsp;&nbsp;<span class='prim'>drop</span> <span class='char'>$9</span> <span class='imm'>;</span> </tt>
<tt>~~~</tt></div><p>
Finishing up the file listing, the <tt style='display:inline'>file-entry</tt> determines
the file type and prints out the appropriate line.
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='colon'>:selector</span> <span class='note'>(filename-selector)</span> </tt>
<tt>&nbsp;&nbsp;BASE s:prepend <span class='imm'>;</span> </tt>
<tt>&nbsp;</tt>
<tt><span class='colon'>:file-entry</span> <span class='note'>(filename)</span> </tt>
<tt>&nbsp;&nbsp;has-extension? <span class='imm'>[</span> file-type <span class='imm'>]</span> <span class='imm'>[</span> <span class='char'>$9</span> <span class='imm'>]</span> choose </tt>
<tt>&nbsp;&nbsp;c:put <span class='prim'>dup</span> s:put tab selector s:put nl <span class='imm'>;</span> </tt>
<tt>&nbsp;</tt>
<tt><span class='imm'>[</span> file? <span class='ptr'>&amp;file-entry</span> <span class='ptr'>&amp;drop</span> choose <span class='imm'>]</span> unix:for-each-file </tt>
<tt>~~~</tt></div><p>
End by displaying FOOTER (if it exists).
<p>
<div class='codeblock'><tt>~~~</tt><tt><span class='str'>'FOOTER</span> file:exists? </tt>
<tt>&nbsp;&nbsp;<span class='imm'>[</span> here <span class='str'>'FOOTER</span> file:slurp here s:put nl <span class='imm'>]</span> <span class='prim'>if</span> </tt>
<tt>~~~</tt></div><p>
<h1>Conclusion</h1>
<p>
This was a quick little thing that will make using Atua nicer
in the future. The techniques used here can be beneficial in
other filesystem related tasks as well, so I expect to reuse
portions of this code in the future.
