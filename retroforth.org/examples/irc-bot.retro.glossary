+

  Data:  nn-n
  Addr:  -
  Float: -

Add `n1` to `n2` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 + 

---reveal---

  Data:  -
  Addr:  -
  Float: -

Switch to the exposed (public) portion of a lexical namespace.

Class: class:word | Namespace: global | Interface Layer: all

;

  Data:  -
  Addr:  -
  Float: -

End the current definition.

Class: class:macro | Namespace: global | Interface Layer: all

ASCII:CR

  Data:  -n
  Addr:  -
  Float: -

Constant. Refers to specific ASCII code.

Class: class:data | Namespace: ASCII | Interface Layer: all

ASCII:LF

  Data:  -n
  Addr:  -
  Float: -

Constant. Refers to specific ASCII code.

Class: class:data | Namespace: ASCII | Interface Layer: all

[

  Data:  -
  Addr:  -
  Float: -

Begin a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

]

  Data:  -
  Addr:  -
  Float: -

End a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

a:for-each

  Data:  aq-
  Addr:  -
  Float: -

Execute the quote once for each item in the array.

Class: class:word | Namespace: a | Interface Layer: all

again

  Data:  -
  Addr:  -
  Float: -

Close an unconditional loop. Branches back to the prior `repeat`.

Class: class:macro | Namespace: global | Interface Layer: all

allot

  Data:  n-
  Addr:  -
  Float: -

Allocate the specified number of cells from the `Heap`.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    'Buffer d:create  #100 allot

bi

  Data:  xqq-?
  Addr:  -
  Float: -

Execute q1 against x, then execute q2 against a copy of x.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #100 [ #10 * ] [ #10 - ] bi

bi@

  Data:  xyq-?
  Addr:  -
  Float: -

Execute q against x, then execute q against y.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #10 #20 [ #3 * ] bi@

buffer:add

  Data:  n-
  Addr:  -
  Float: -

Append a value to the current buffer.

Class: class:word | Namespace: buffer | Interface Layer: all

buffer:get

  Data:  -n
  Addr:  -
  Float: -

Remove the last value from the current buffer.

Class: class:word | Namespace: buffer | Interface Layer: all

buffer:preserve

  Data:  q-
  Addr:  -
  Float: -

Save and restore the current buffer before and after executing the specified quote.

Class: class:word | Namespace: buffer | Interface Layer: all

buffer:set

  Data:  a-
  Addr:  -
  Float: -

Assign a new buffer as the current one.

Class: class:word | Namespace: buffer | Interface Layer: all

call

  Data:  a-
  Addr:  -
  Float: -

Call a function.

Class: class:primitive | Namespace: global | Interface Layer: all

const

  Data:  ns-
  Addr:  -
  Float: -

Create a constant returning the specified value.

Class: class:word | Namespace: global | Interface Layer: all

d:create

  Data:  s-
  Addr:  -
  Float: -

Hook. Create a new dictionary header named the specified string. The new header will point to `here` and have a class of `class:data`.

Class: class:word | Namespace: d | Interface Layer: all

dip

  Data:  nq-n
  Addr:  -
  Float: -

Temporarily remove n from the stack, execute the quotation, and then restore n to the stack.

Class: class:word | Namespace: global | Interface Layer: all

drop

  Data:  n-
  Addr:  -
  Float: -

Discard the top value on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

drop-pair

  Data:  nn-
  Addr:  -
  Float: -

Remove top two items on the stack.

Class: class:word | Namespace: global | Interface Layer: all

dump-stack

  Data:  -
  Addr:  -
  Float: -

Display the items on the data stack.

Class: class:word | Namespace: global | Interface Layer: all

dup

  Data:  n-nn
  Addr:  -
  Float: -

Duplicate the top item on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

eq?

  Data:  nn-f
  Addr:  -
  Float: -

Compare two values for equality. Returns `TRUE` if they are equal or `FALSE` otherwise.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 eq?
    $a $b eq?

file:A

  Data:  -n
  Addr:  -
  Float: -

Constant for opening a file in APPEND mode.

Class: class:data | Namespace: file | Interface Layer: rre

file:R

  Data:  -n
  Addr:  -
  Float: -

Constant for opening a file in READ mode.

Class: class:data | Namespace: file | Interface Layer: rre

file:close

  Data:  h-
  Addr:  -
  Float: -

Given a file handle, close the file.

Class: class:word | Namespace: file | Interface Layer: rre

file:delete

  Data:  s-
  Addr:  -
  Float: -

Delete the named file.

Class: class:word | Namespace: file | Interface Layer: rre

file:for-each-line

  Data:  sq-
  Addr:  -
  Float: -

Given a file name, open it and run the quote once for each line in the file.

Class: class:word | Namespace: file | Interface Layer: rre

file:open

  Data:  sm-h
  Addr:  -
  Float: -

Open a named file (s) with the given mode (m). Returns a handle identifying the file.

Class: class:word | Namespace: file | Interface Layer: rre

Example #1:

  '/etc/motd file:R file:open

file:read-line

  Data:  f-s
  Addr:  -
  Float: -

Given a file handle, read a line and return a pointer to it.

Class: class:word | Namespace: file | Interface Layer: rre

file:spew

  Data:  ss-
  Addr:  -
  Float: -

Given a string (s1) and a file name (s2), write the string into the file, replacing any existing content.

Class: class:word | Namespace: file | Interface Layer: rre

file:write

  Data:  ch-
  Addr:  -
  Float: -

Write a character to the file represented by the handle.

Class: class:word | Namespace: file | Interface Layer: rre

here

  Data:  -a
  Addr:  -
  Float: -

Return the next free address in memory.

Class: class:word | Namespace: global | Interface Layer: all

if

  Data:  fq-
  Addr:  -
  Float: -

Execute the quote if the flag is `TRUE`.

Class: class:word | Namespace: global | Interface Layer: all

n:inc

  Data:  n-m
  Addr:  -
  Float: -

Increment n by one.

Class: class:word | Namespace: n | Interface Layer: all

Example #1:

    #100 n:inc

n:random

  Data:  -n
  Addr:  -
  Float: -

Return a random number.

Class: class:word | Namespace: n | Interface Layer: rre

Example #1:

    n:random n:put

nip

  Data:  nm-m
  Addr:  -
  Float: -

Remove the second item from the stack.

Class: class:word | Namespace: global | Interface Layer: all

nl

  Data:  -
  Addr:  -
  Float: -

Display a newline.

Class: class:word | Namespace: global | Interface Layer: all

or

  Data:  mn-o
  Addr:  -
  Float: -

Perform a bitwise OR between the provided values.

Class: class:primitive | Namespace: global | Interface Layer: all

over

  Data:  nm-nmn
  Addr:  -
  Float: -

Put a copy of n over m.

Class: class:word | Namespace: global | Interface Layer: all

repeat

  Data:  -
  Addr:  -
  Float: -

Begin an unconditional loop.

Class: class:macro | Namespace: global | Interface Layer: all

s:append

  Data:  ss-s
  Addr:  -
  Float: -

Return a new string consisting of s1 followed by s2.

Class: class:word | Namespace: s | Interface Layer: all

s:begins-with?

  Data:  ss-f
  Addr:  -
  Float: -

Return `TRUE` if s1 begins with s2 or `FALSE` otherwise.

Class: class:word | Namespace: s | Interface Layer: all

s:chop

  Data:  s-s
  Addr:  -
  Float: -

Remove the last character from a string. Returns a new string.

Class: class:word | Namespace: s | Interface Layer: all

s:const

  Data:  ss-
  Addr:  -
  Float: -

Create a constant named s2, returning a pointer to s1. This will use `s:keep` to preserve the original string.

Class: class:word | Namespace: s | Interface Layer: all

s:contains/string?

  Data:  ss-f
  Addr:  -
  Float: -

Return `TRUE` if the second string is present in the first string or `FALSE` otherwise.

Class: class:word | Namespace: s | Interface Layer: all

s:copy

  Data:  sa-
  Addr:  -
  Float: -

Copy a string (s) to a destination (a). This will include the terminator character when copying.

Class: class:word | Namespace: s | Interface Layer: all

s:empty

  Data:  -s
  Addr:  -
  Float: -

Return an empty string.

Class: class:word | Namespace: s | Interface Layer: all

s:for-each

  Data:  sq-
  Addr:  -
  Float: -

Execute the quote once for each value in the string.

Class: class:word | Namespace: s | Interface Layer: all

s:format

  Data:  ...s-s
  Addr:  -
  Float: -

Construct a new string using the template passed and items from the stack.

Class: class:word | Namespace: s | Interface Layer: all

s:put

  Data:  s-
  Addr:  -
  Float: -

Display a string.

Class: class:word | Namespace: global | Interface Layer: all

s:split/char

  Data:  sc-ss
  Addr:  -
  Float: -

Split a string on the first occurrence of the specified character.

Class: class:word | Namespace: s | Interface Layer: all

s:split/string

  Data:  ss-ss
  Addr:  -
  Float: -

Split a string on the first occurrence of the specified string. After the split, the top stack item will be the part of the string before the specified substring, and the second item will be the rest of the original string.

Class: class:word | Namespace: s | Interface Layer: all

Example #1:

  'Hello_Brave_World! 'Brave s:split-on-string s:put nl s:put nl

set-hook

  Data:  aa-
  Addr:  -
  Float: -

Patch the hook point in a2 to point to a1.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    :foo hook ;
    :bar #1 ;
    &bar &foo set-hook

sip

  Data:  nq(?n-?)-n
  Addr:  -
  Float: -

Run quote. After execution completes, put a copy of n back on top of the stack.

Class: class:word | Namespace: global | Interface Layer: all

socket:close

  Data:  n-
  Addr:  -
  Float: -

Close a socket.

Class: class:word | Namespace: socket | Interface Layer: rre

socket:configure

  Data:  ss-
  Addr:  -
  Float: -

Configure internal settings for a host (s1) and port (s2). Use before `socket:bind`.

Class: class:word | Namespace: socket | Interface Layer: rre

socket:connect

  Data:  n-nn
  Addr:  -
  Float: -

Connect to a server. Provide a socket, this will return a status flag and an error code.

Class: class:word | Namespace: socket | Interface Layer: rre

socket:create

  Data:  -n
  Addr:  -
  Float: -

Obtain a new socket.

Class: class:word | Namespace: socket | Interface Layer: rre

socket:recv

  Data:  ann-nn
  Addr:  -
  Float: -

Receive data from a socket. This will read into memory starting at address *a*, up to *n1* bytes. *n2* is the socket. Returns the number of bytes read and an error code.

Class: class:word | Namespace: socket | Interface Layer: rre

socket:send

  Data:  sn-nn
  Addr:  -
  Float: -

Send a string to a socket. This will return the number of characters sent and an error code.

Class: class:word | Namespace: socket | Interface Layer: rre

swap

  Data:  nm-mn
  Addr:  -
  Float: -

Exchange the position of the top two items on the stack

Class: class:primitive | Namespace: global | Interface Layer: all

unhook

  Data:  a-
  Addr:  -
  Float: -

Reset the hook point in a1 to the default definition.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    :foo hook #1 ;
    :bar #2 ;
    &bar &foo set-hook
    &foo unhook

unix:pclose

  Data:  n-
  Addr:  -
  Float: -

Close a pipe.

Class: class:word | Namespace: unix | Interface Layer: rre

unix:popen

  Data:  sn-n
  Addr:  -
  Float: -

Open a pipe. Takes a command to run, and a file mode (`file:R` or `file:W`; `file:R+` may work on some systems). Returns a file ID usable with words in the `file:` namespace.

Class: class:word | Namespace: unix | Interface Layer: rre

unix:system

  Data:  s-
  Addr:  -
  Float: -

Runs another application using the system shell and returns after execution is completed.

Class: class:word | Namespace: unix | Interface Layer: rre

Example #1:

  'ls_-lh_/etc unix:system

var

  Data:  s-
  Addr:  -
  Float: -

Create a variable. The variable is initialized to 0.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    'Base var

{

  Data:  -
  Addr:  -
  Float: -

Begin an array. This is intended to make creating arrays a bit cleaner than using a quotation and `a:counted-results`.

Class: class:word | Namespace: global | Interface Layer: all

{{

  Data:  -
  Addr:  -
  Float: -

Begin a lexically scoped area.

Class: class:word | Namespace: global | Interface Layer: all

}

  Data:  -a
  Addr:  -
  Float: -

Complete an array begun by `{`. Returns a pointer to the data.

Class: class:word | Namespace: global | Interface Layer: all

}}

  Data:  -
  Addr:  -
  Float: -

End a lexically scoped area. This will hide any headers between `{{` and `---reveal---`, leaving only headers between `---reveal---` and the `}}` visible.

Class: class:word | Namespace: global | Interface Layer: all

sigil:!

  Data:  ns-
  Addr:  -
  Float: -

Store a value into a variable.

Interpret Time:
  Store a value into the named variable.

Compile Time:
  Compile the code to store a value into a named variable.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:#

  Data:  s-n
  Addr:  -
  Float: -

Process token as a number.

Interpret Time:
  Convert the string into a number and leave on the stack.

Compile Time:
  Convert the string into a number and compile into the current definition as a literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:$

  Data:  s-c
  Addr:  -
  Float: -

Process token as an ASCII character.

Interpret Time:
  Fetch first character from string. Leave on stack.

Compile Time:
  Fetch first character from the string. Compile into the current definition as  literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:&

  Data:  s-a
  Addr:  -
  Float: -

Return a pointer to a named item. If name is not found, returns 0.

Interpret Time:
  Lookup name in dictionary, return contents of the xt field on the stack.

Compile Time:
  Lookup name in dictionary, compile code to push the contents of the xt field into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:'

  Data:  s-s
  Addr:  -
  Float: -

Process token as a string.

Interpret Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space.

Compile Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space. Then compile the string into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:(

  Data:  s-
  Addr:  -
  Float: -

Process token as a comment.

Interpret Time:
  Discard the string.

Compile Time:
  Discard the string.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil::

  Data:  s-
  Addr:  -
  Float: -

Hook. Process token as a new definition.

Interpret Time:
  Create a header pointing to `here` with the provided string as the name. Sets class to `class:word`.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:@

  Data:  s-n
  Addr:  -
  Float: -

Fetch from a stored variable.

Interpret Time:
  Fetch a value from a named variable.

Compile Time:
  Compile the code to fetch a value from a named variable into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

