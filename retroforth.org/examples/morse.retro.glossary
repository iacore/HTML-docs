*

  Data:  nn-n
  Addr:  -
  Float: -

Multiply `n1` by `n2` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #2 #6 *
    #-1 #100 *

-

  Data:  nn-n
  Addr:  -
  Float: -

Subtract `n2` from `n1` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #2 #1 - 

;

  Data:  -
  Addr:  -
  Float: -

End the current definition.

Class: class:macro | Namespace: global | Interface Layer: all

[

  Data:  -
  Addr:  -
  Float: -

Begin a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

]

  Data:  -
  Addr:  -
  Float: -

End a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

c:put

  Data:  c-
  Addr:  -
  Float: -

Vectored. Display a single character.

Class: class:word | Namespace: global | Interface Layer: all

drop

  Data:  n-
  Addr:  -
  Float: -

Discard the top value on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

dup

  Data:  n-nn
  Addr:  -
  Float: -

Duplicate the top item on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

eq?

  Data:  nn-f
  Addr:  -
  Float: -

Compare two values for equality. Returns `TRUE` if they are equal or `FALSE` otherwise.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 eq?
    $a $b eq?

file:W

  Data:  -n
  Addr:  -
  Float: -

Constant for opening a file in WRITE mode.

Class: class:data | Namespace: file | Interface Layer: rre

file:close

  Data:  h-
  Addr:  -
  Float: -

Given a file handle, close the file.

Class: class:word | Namespace: file | Interface Layer: rre

file:open

  Data:  sm-h
  Addr:  -
  Float: -

Open a named file (s) with the given mode (m). Returns a handle identifying the file.

Class: class:word | Namespace: file | Interface Layer: rre

Example #1:

  '/etc/motd file:R file:open

file:write

  Data:  ch-
  Addr:  -
  Float: -

Write a character to the file represented by the handle.

Class: class:word | Namespace: file | Interface Layer: rre

if

  Data:  fq-
  Addr:  -
  Float: -

Execute the quote if the flag is `TRUE`.

Class: class:word | Namespace: global | Interface Layer: all

nl

  Data:  -
  Addr:  -
  Float: -

Display a newline.

Class: class:word | Namespace: global | Interface Layer: all

s:for-each

  Data:  sq-
  Addr:  -
  Float: -

Execute the quote once for each value in the string.

Class: class:word | Namespace: s | Interface Layer: all

sp

  Data:  -
  Addr:  -
  Float: -

Display a space (`ASCII:SPACE`)

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    :spaces (n-)  [ sp ] times ;
    #12 spaces

times

  Data:  nq-
  Addr:  -
  Float: -

Run the specified quote the specified number of times.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #12 [ $- c:put ] times

var

  Data:  s-
  Addr:  -
  Float: -

Create a variable. The variable is initialized to 0.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    'Base var

sigil:!

  Data:  ns-
  Addr:  -
  Float: -

Store a value into a variable.

Interpret Time:
  Store a value into the named variable.

Compile Time:
  Compile the code to store a value into a named variable.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:#

  Data:  s-n
  Addr:  -
  Float: -

Process token as a number.

Interpret Time:
  Convert the string into a number and leave on the stack.

Compile Time:
  Convert the string into a number and compile into the current definition as a literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:$

  Data:  s-c
  Addr:  -
  Float: -

Process token as an ASCII character.

Interpret Time:
  Fetch first character from string. Leave on stack.

Compile Time:
  Fetch first character from the string. Compile into the current definition as  literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:'

  Data:  s-s
  Addr:  -
  Float: -

Process token as a string.

Interpret Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space.

Compile Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space. Then compile the string into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:(

  Data:  s-
  Addr:  -
  Float: -

Process token as a comment.

Interpret Time:
  Discard the string.

Compile Time:
  Discard the string.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:.

  Data:  s-
  Addr:  -
  Float: -F

Interpret time: convert string to a floating-point value. Compile time: convert string to a floating-point value and compile code to push this value to the float stack.

Class: class:macro | Namespace: sigil | Interface Layer: rre

sigil::

  Data:  s-
  Addr:  -
  Float: -

Hook. Process token as a new definition.

Interpret Time:
  Create a header pointing to `here` with the provided string as the name. Sets class to `class:word`.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:@

  Data:  s-n
  Addr:  -
  Float: -

Fetch from a stored variable.

Interpret Time:
  Fetch a value from a named variable.

Compile Time:
  Compile the code to fetch a value from a named variable into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

