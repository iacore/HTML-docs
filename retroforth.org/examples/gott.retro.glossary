---reveal---

  Data:  -
  Addr:  -
  Float: -

Switch to the exposed (public) portion of a lexical namespace.

Class: class:word | Namespace: global | Interface Layer: all

;

  Data:  -
  Addr:  -
  Float: -

End the current definition.

Class: class:macro | Namespace: global | Interface Layer: all

[

  Data:  -
  Addr:  -
  Float: -

Begin a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

]

  Data:  -
  Addr:  -
  Float: -

End a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

dump-stack

  Data:  -
  Addr:  -
  Float: -

Display the items on the data stack.

Class: class:word | Namespace: global | Interface Layer: all

f:*

  Data:  -
  Addr:  -
  Float: FF-F

Multiply two floating-point numbers, returning the result.

Class: class:word | Namespace: f | Interface Layer: rre

Example #1:

    .3.1415 .22 f:*

f:+

  Data:  -
  Addr:  -
  Float: FF-F

Add two floating-point numbers, returning the result.

Class: class:word | Namespace: f | Interface Layer: rre

Example #1:

    .3.1 .22 f:+

f:-

  Data:  -
  Addr:  -
  Float: FF-F

Subtract F2 from F1 returning the result.

Class: class:word | Namespace: f | Interface Layer: rre

Example #1:

    .22.3 .0.12 f:-

f:/

  Data:  -
  Addr:  -
  Float: FF-F

Divide floating-point value F1 by F2.

Class: class:word | Namespace: f | Interface Layer: rre

f:adepth

  Data:  -n
  Addr:  -
  Float: -

Return the number of items on the alternate floating-point stack.

Class: class:word | Namespace: f | Interface Layer: rre

f:depth

  Data:  -n
  Addr:  -
  Float: -

Return the number of items on the floating-point stack.

Class: class:word | Namespace: f | Interface Layer: rre

f:drop

  Data:  -
  Addr:  -
  Float: F-

Discard the top item on the floating-point stack.

Class: class:word | Namespace: f | Interface Layer: rre

f:dump-astack

  Data:  -
  Addr:  -
  Float: -

Display the items on the alternate floating-point stack.

Class: class:word | Namespace: f | Interface Layer: rre

f:dump-stack

  Data:  -
  Addr:  -
  Float: -

Display the items on the floating-point stack.

Class: class:word | Namespace: f | Interface Layer: rre

f:dup

  Data:  -
  Addr:  -
  Float: F-FF

Duplicate the top item on the floating-point stack.

Class: class:word | Namespace: f | Interface Layer: rre

f:dup-pair

  Data:  -
  Addr:  -
  Float: nm-nmnm

Duplicate the top two items on the stack.

Class: class:word | Namespace: f | Interface Layer: rre

f:pop

  Data:  -
  Addr:  -
  Float: -F

Pop a floating-point value from the alternate stack.

Class: class:word | Namespace: f | Interface Layer: rre

f:push

  Data:  -
  Addr:  -
  Float: F-

Push a floating-point value to an alternative stack.

Class: class:word | Namespace: f | Interface Layer: rre

f:rot

  Data:  -
  Addr:  -
  Float: abc-bca

Rotate the top three values.

Class: class:word | Namespace: f | Interface Layer: rre

f:swap

  Data:  -
  Addr:  -
  Float: FG-GF

Exchange the top and second items on the floating-point stack.

Class: class:word | Namespace: f | Interface Layer: rre

if

  Data:  fq-
  Addr:  -
  Float: -

Execute the quote if the flag is `TRUE`.

Class: class:word | Namespace: global | Interface Layer: all

lt?

  Data:  nn-f
  Addr:  -
  Float: -

Compare n1 and n2. Return `TRUE` if n1 is less than n2, or `FALSE` otherwise.

Class: class:primitive | Namespace: global | Interface Layer: all

n:to-float

  Data:  n-
  Addr:  -
  Float: -F

Convert a number into a floating-point value.

Class: class:word | Namespace: n | Interface Layer: rre

nl

  Data:  -
  Addr:  -
  Float: -

Display a newline.

Class: class:word | Namespace: global | Interface Layer: all

r

  Data:  s-
  Addr:  -
  Float: -

Lookup a reference by name and inline its pointer to the current assembly segment.

Class: class:word | Namespace: global | Interface Layer: all

reset

  Data:  ...-
  Addr:  -
  Float: -

Remove all items from the stack.

Class: class:word | Namespace: global | Interface Layer: all

s:put

  Data:  s-
  Addr:  -
  Float: -

Display a string.

Class: class:word | Namespace: global | Interface Layer: all

times

  Data:  nq-
  Addr:  -
  Float: -

Run the specified quote the specified number of times.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #12 [ $- c:put ] times

{{

  Data:  -
  Addr:  -
  Float: -

Begin a lexically scoped area.

Class: class:word | Namespace: global | Interface Layer: all

}}

  Data:  -
  Addr:  -
  Float: -

End a lexically scoped area. This will hide any headers between `{{` and `---reveal---`, leaving only headers between `---reveal---` and the `}}` visible.

Class: class:word | Namespace: global | Interface Layer: all

sigil:#

  Data:  s-n
  Addr:  -
  Float: -

Process token as a number.

Interpret Time:
  Convert the string into a number and leave on the stack.

Compile Time:
  Convert the string into a number and compile into the current definition as a literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:&

  Data:  s-a
  Addr:  -
  Float: -

Return a pointer to a named item. If name is not found, returns 0.

Interpret Time:
  Lookup name in dictionary, return contents of the xt field on the stack.

Compile Time:
  Lookup name in dictionary, compile code to push the contents of the xt field into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:'

  Data:  s-s
  Addr:  -
  Float: -

Process token as a string.

Interpret Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space.

Compile Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space. Then compile the string into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:(

  Data:  s-
  Addr:  -
  Float: -

Process token as a comment.

Interpret Time:
  Discard the string.

Compile Time:
  Discard the string.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:.

  Data:  s-
  Addr:  -
  Float: -F

Interpret time: convert string to a floating-point value. Compile time: convert string to a floating-point value and compile code to push this value to the float stack.

Class: class:macro | Namespace: sigil | Interface Layer: rre

sigil::

  Data:  s-
  Addr:  -
  Float: -

Hook. Process token as a new definition.

Interpret Time:
  Create a header pointing to `here` with the provided string as the name. Sets class to `class:word`.

Class: class:macro | Namespace: sigil | Interface Layer: all

