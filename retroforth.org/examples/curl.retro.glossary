+

  Data:  nn-n
  Addr:  -
  Float: -

Add `n1` to `n2` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 + 

;

  Data:  -
  Addr:  -
  Float: -

End the current definition.

Class: class:macro | Namespace: global | Interface Layer: all

ASCII:SPACE

  Data:  -n
  Addr:  -
  Float: -

Constant. Refers to specific ASCII code.

Class: class:data | Namespace: ASCII | Interface Layer: all

[

  Data:  -
  Addr:  -
  Float: -

Begin a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

]

  Data:  -
  Addr:  -
  Float: -

End a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

allot

  Data:  n-
  Addr:  -
  Float: -

Allocate the specified number of cells from the `Heap`.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    'Buffer d:create  #100 allot

bi

  Data:  xqq-?
  Addr:  -
  Float: -

Execute q1 against x, then execute q2 against a copy of x.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #100 [ #10 * ] [ #10 - ] bi

buffer:add

  Data:  n-
  Addr:  -
  Float: -

Append a value to the current buffer.

Class: class:word | Namespace: buffer | Interface Layer: all

buffer:set

  Data:  a-
  Addr:  -
  Float: -

Assign a new buffer as the current one.

Class: class:word | Namespace: buffer | Interface Layer: all

d:create

  Data:  s-
  Addr:  -
  Float: -

Hook. Create a new dictionary header named the specified string. The new header will point to `here` and have a class of `class:data`.

Class: class:word | Namespace: d | Interface Layer: all

drop

  Data:  n-
  Addr:  -
  Float: -

Discard the top value on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

dup

  Data:  n-nn
  Addr:  -
  Float: -

Duplicate the top item on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

file:R

  Data:  -n
  Addr:  -
  Float: -

Constant for opening a file in READ mode.

Class: class:data | Namespace: file | Interface Layer: rre

file:read-line

  Data:  f-s
  Addr:  -
  Float: -

Given a file handle, read a line and return a pointer to it.

Class: class:word | Namespace: file | Interface Layer: rre

nl

  Data:  -
  Addr:  -
  Float: -

Display a newline.

Class: class:word | Namespace: global | Interface Layer: all

s:chop

  Data:  s-s
  Addr:  -
  Float: -

Remove the last character from a string. Returns a new string.

Class: class:word | Namespace: s | Interface Layer: all

s:const

  Data:  ss-
  Addr:  -
  Float: -

Create a constant named s2, returning a pointer to s1. This will use `s:keep` to preserve the original string.

Class: class:word | Namespace: s | Interface Layer: all

s:copy

  Data:  sa-
  Addr:  -
  Float: -

Copy a string (s) to a destination (a). This will include the terminator character when copying.

Class: class:word | Namespace: s | Interface Layer: all

s:for-each

  Data:  sq-
  Addr:  -
  Float: -

Execute the quote once for each value in the string.

Class: class:word | Namespace: s | Interface Layer: all

s:format

  Data:  ...s-s
  Addr:  -
  Float: -

Construct a new string using the template passed and items from the stack.

Class: class:word | Namespace: s | Interface Layer: all

s:keep

  Data:  s-s
  Addr:  -
  Float: -

Store a string into the heap and return a pointer to the start of it.

Class: class:word | Namespace: s | Interface Layer: all

s:put

  Data:  s-
  Addr:  -
  Float: -

Display a string.

Class: class:word | Namespace: global | Interface Layer: all

s:split/string

  Data:  ss-ss
  Addr:  -
  Float: -

Split a string on the first occurrence of the specified string. After the split, the top stack item will be the part of the string before the specified substring, and the second item will be the rest of the original string.

Class: class:word | Namespace: s | Interface Layer: all

Example #1:

  'Hello_Brave_World! 'Brave s:split-on-string s:put nl s:put nl

swap

  Data:  nm-mn
  Addr:  -
  Float: -

Exchange the position of the top two items on the stack

Class: class:primitive | Namespace: global | Interface Layer: all

unix:pclose

  Data:  n-
  Addr:  -
  Float: -

Close a pipe.

Class: class:word | Namespace: unix | Interface Layer: rre

unix:popen

  Data:  sn-n
  Addr:  -
  Float: -

Open a pipe. Takes a command to run, and a file mode (`file:R` or `file:W`; `file:R+` may work on some systems). Returns a file ID usable with words in the `file:` namespace.

Class: class:word | Namespace: unix | Interface Layer: rre

sigil:#

  Data:  s-n
  Addr:  -
  Float: -

Process token as a number.

Interpret Time:
  Convert the string into a number and leave on the stack.

Compile Time:
  Convert the string into a number and compile into the current definition as a literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:&

  Data:  s-a
  Addr:  -
  Float: -

Return a pointer to a named item. If name is not found, returns 0.

Interpret Time:
  Lookup name in dictionary, return contents of the xt field on the stack.

Compile Time:
  Lookup name in dictionary, compile code to push the contents of the xt field into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:'

  Data:  s-s
  Addr:  -
  Float: -

Process token as a string.

Interpret Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space.

Compile Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space. Then compile the string into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:(

  Data:  s-
  Addr:  -
  Float: -

Process token as a comment.

Interpret Time:
  Discard the string.

Compile Time:
  Discard the string.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil::

  Data:  s-
  Addr:  -
  Float: -

Hook. Process token as a new definition.

Interpret Time:
  Create a header pointing to `here` with the provided string as the name. Sets class to `class:word`.

Class: class:macro | Namespace: sigil | Interface Layer: all

