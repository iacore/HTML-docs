+

  Data:  nn-n
  Addr:  -
  Float: -

Add `n1` to `n2` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 + 

,

  Data:  n-
  Addr:  -
  Float: -

Store the specified value into the memory at `here` and increment `Heap` by 1.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    $a , $b , $c , #0 ,

;

  Data:  -
  Addr:  -
  Float: -

End the current definition.

Class: class:macro | Namespace: global | Interface Layer: all

[

  Data:  -
  Addr:  -
  Float: -

Begin a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

]

  Data:  -
  Addr:  -
  Float: -

End a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

buffer:add

  Data:  n-
  Addr:  -
  Float: -

Append a value to the current buffer.

Class: class:word | Namespace: buffer | Interface Layer: all

buffer:preserve

  Data:  q-
  Addr:  -
  Float: -

Save and restore the current buffer before and after executing the specified quote.

Class: class:word | Namespace: buffer | Interface Layer: all

buffer:set

  Data:  a-
  Addr:  -
  Float: -

Assign a new buffer as the current one.

Class: class:word | Namespace: buffer | Interface Layer: all

choose

  Data:  fqq-
  Addr:  -
  Float: -

Execute q1 if the flag is true (-1) or q2 if the flag is false (0). Only these flags are valid when using `choose`; passing other values as flags will result in memory corruption.

Class: class:word | Namespace: global | Interface Layer: all

copy

  Data:  sdl-
  Addr:  -
  Float: -

Copy `l` cells from memory at `s` to the memory at `d`. These should not overlap.

Class: class:word | Namespace: global | Interface Layer: all

d:create

  Data:  s-
  Addr:  -
  Float: -

Hook. Create a new dictionary header named the specified string. The new header will point to `here` and have a class of `class:data`.

Class: class:word | Namespace: d | Interface Layer: all

drop

  Data:  n-
  Addr:  -
  Float: -

Discard the top value on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

dup

  Data:  n-nn
  Addr:  -
  Float: -

Duplicate the top item on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

eq?

  Data:  nn-f
  Addr:  -
  Float: -

Compare two values for equality. Returns `TRUE` if they are equal or `FALSE` otherwise.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 eq?
    $a $b eq?

fetch

  Data:  a-n
  Addr:  -
  Float: -

Fetch the value stored at the specified address.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    &Version fetch

n:inc

  Data:  n-m
  Addr:  -
  Float: -

Increment n by one.

Class: class:word | Namespace: n | Interface Layer: all

Example #1:

    #100 n:inc

nl

  Data:  -
  Addr:  -
  Float: -

Display a newline.

Class: class:word | Namespace: global | Interface Layer: all

over

  Data:  nm-nmn
  Addr:  -
  Float: -

Put a copy of n over m.

Class: class:word | Namespace: global | Interface Layer: all

s:for-each

  Data:  sq-
  Addr:  -
  Float: -

Execute the quote once for each value in the string.

Class: class:word | Namespace: s | Interface Layer: all

s:length

  Data:  s-n
  Addr:  -
  Float: -

Return the number of characters in a string, excluding the NULL terminator.

Class: class:word | Namespace: s | Interface Layer: all

s:put

  Data:  s-
  Addr:  -
  Float: -

Display a string.

Class: class:word | Namespace: global | Interface Layer: all

times

  Data:  nq-
  Addr:  -
  Float: -

Run the specified quote the specified number of times.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #12 [ $- c:put ] times

tri

  Data:  xqqq-?
  Addr:  -
  Float: -

Apply q1 against x, then q2 against a copy of x, and finally q3 against another copy of x.

Class: class:word | Namespace: global | Interface Layer: all

tri@

  Data:  xyzq-?
  Addr:  -
  Float: -

Apply q against x, then against y, and finally against z.

Class: class:word | Namespace: global | Interface Layer: all

sigil:#

  Data:  s-n
  Addr:  -
  Float: -

Process token as a number.

Interpret Time:
  Convert the string into a number and leave on the stack.

Compile Time:
  Convert the string into a number and compile into the current definition as a literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:$

  Data:  s-c
  Addr:  -
  Float: -

Process token as an ASCII character.

Interpret Time:
  Fetch first character from string. Leave on stack.

Compile Time:
  Fetch first character from the string. Compile into the current definition as  literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:&

  Data:  s-a
  Addr:  -
  Float: -

Return a pointer to a named item. If name is not found, returns 0.

Interpret Time:
  Lookup name in dictionary, return contents of the xt field on the stack.

Compile Time:
  Lookup name in dictionary, compile code to push the contents of the xt field into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:'

  Data:  s-s
  Addr:  -
  Float: -

Process token as a string.

Interpret Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space.

Compile Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space. Then compile the string into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:(

  Data:  s-
  Addr:  -
  Float: -

Process token as a comment.

Interpret Time:
  Discard the string.

Compile Time:
  Discard the string.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil::

  Data:  s-
  Addr:  -
  Float: -

Hook. Process token as a new definition.

Interpret Time:
  Create a header pointing to `here` with the provided string as the name. Sets class to `class:word`.

Class: class:macro | Namespace: sigil | Interface Layer: all

