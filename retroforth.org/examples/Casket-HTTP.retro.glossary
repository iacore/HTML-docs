+

  Data:  nn-n
  Addr:  -
  Float: -

Add `n1` to `n2` and return the result.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 + 

,

  Data:  n-
  Addr:  -
  Float: -

Store the specified value into the memory at `here` and increment `Heap` by 1.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    $a , $b , $c , #0 ,

-eq?

  Data:  nn-f
  Addr:  -
  Float: -

Compare two values for inequality. Returns `TRUE` if they are not equal or `FALSE` otherwise.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 -eq?
    $a $b -eq?

;

  Data:  -
  Addr:  -
  Float: -

End the current definition.

Class: class:macro | Namespace: global | Interface Layer: all

ASCII:CR

  Data:  -n
  Addr:  -
  Float: -

Constant. Refers to specific ASCII code.

Class: class:data | Namespace: ASCII | Interface Layer: all

ASCII:LF

  Data:  -n
  Addr:  -
  Float: -

Constant. Refers to specific ASCII code.

Class: class:data | Namespace: ASCII | Interface Layer: all

ASCII:SPACE

  Data:  -n
  Addr:  -
  Float: -

Constant. Refers to specific ASCII code.

Class: class:data | Namespace: ASCII | Interface Layer: all

[

  Data:  -
  Addr:  -
  Float: -

Begin a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

]

  Data:  -
  Addr:  -
  Float: -

End a quotation.

Class: class:macro | Namespace: global | Interface Layer: all

allot

  Data:  n-
  Addr:  -
  Float: -

Allocate the specified number of cells from the `Heap`.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    'Buffer d:create  #100 allot

bi

  Data:  xqq-?
  Addr:  -
  Float: -

Execute q1 against x, then execute q2 against a copy of x.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #100 [ #10 * ] [ #10 - ] bi

buffer:add

  Data:  n-
  Addr:  -
  Float: -

Append a value to the current buffer.

Class: class:word | Namespace: buffer | Interface Layer: all

buffer:get

  Data:  -n
  Addr:  -
  Float: -

Remove the last value from the current buffer.

Class: class:word | Namespace: buffer | Interface Layer: all

buffer:set

  Data:  a-
  Addr:  -
  Float: -

Assign a new buffer as the current one.

Class: class:word | Namespace: buffer | Interface Layer: all

c:get

  Data:  -c
  Addr:  -
  Float: -

Vectored. Read a single keypress.

Class: class:word | Namespace: global | Interface Layer: rre

c:put

  Data:  c-
  Addr:  -
  Float: -

Vectored. Display a single character.

Class: class:word | Namespace: global | Interface Layer: all

choose

  Data:  fqq-
  Addr:  -
  Float: -

Execute q1 if the flag is true (-1) or q2 if the flag is false (0). Only these flags are valid when using `choose`; passing other values as flags will result in memory corruption.

Class: class:word | Namespace: global | Interface Layer: all

d:create

  Data:  s-
  Addr:  -
  Float: -

Hook. Create a new dictionary header named the specified string. The new header will point to `here` and have a class of `class:data`.

Class: class:word | Namespace: d | Interface Layer: all

drop

  Data:  n-
  Addr:  -
  Float: -

Discard the top value on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

dup

  Data:  n-nn
  Addr:  -
  Float: -

Duplicate the top item on the stack.

Class: class:primitive | Namespace: global | Interface Layer: all

eq?

  Data:  nn-f
  Addr:  -
  Float: -

Compare two values for equality. Returns `TRUE` if they are equal or `FALSE` otherwise.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    #1 #2 eq?
    $a $b eq?

fetch-next

  Data:  a-an
  Addr:  -
  Float: -

Fetch the value stored at the specified address. Returns the next address and the value.

Class: class:word | Namespace: global | Interface Layer: all

file:R

  Data:  -n
  Addr:  -
  Float: -

Constant for opening a file in READ mode.

Class: class:data | Namespace: file | Interface Layer: rre

file:close

  Data:  h-
  Addr:  -
  Float: -

Given a file handle, close the file.

Class: class:word | Namespace: file | Interface Layer: rre

file:exists?

  Data:  s-f
  Addr:  -
  Float: -

Given a file name, return `TRUE` if it exists or `FALSE` if it does not.

Class: class:word | Namespace: file | Interface Layer: rre

file:open

  Data:  sm-h
  Addr:  -
  Float: -

Open a named file (s) with the given mode (m). Returns a handle identifying the file.

Class: class:word | Namespace: file | Interface Layer: rre

Example #1:

  '/etc/motd file:R file:open

file:read

  Data:  h-c
  Addr:  -
  Float: -

Given a file handle, read and return the next character in it.

Class: class:word | Namespace: file | Interface Layer: rre

file:size

  Data:  h-n
  Addr:  -
  Float: -

Given a file handle, return the size of the file (in bytes).

Class: class:word | Namespace: file | Interface Layer: rre

here

  Data:  -a
  Addr:  -
  Float: -

Return the next free address in memory.

Class: class:word | Namespace: global | Interface Layer: all

if

  Data:  fq-
  Addr:  -
  Float: -

Execute the quote if the flag is `TRUE`.

Class: class:word | Namespace: global | Interface Layer: all

include

  Data:  s-
  Addr:  -
  Float: -

Run the code in the specified file. 

Class: class:word | Namespace: global | Interface Layer: rre

n:inc

  Data:  n-m
  Addr:  -
  Float: -

Increment n by one.

Class: class:word | Namespace: n | Interface Layer: all

Example #1:

    #100 n:inc

or

  Data:  mn-o
  Addr:  -
  Float: -

Perform a bitwise OR between the provided values.

Class: class:primitive | Namespace: global | Interface Layer: all

s:append

  Data:  ss-s
  Addr:  -
  Float: -

Return a new string consisting of s1 followed by s2.

Class: class:word | Namespace: s | Interface Layer: all

s:case

  Data:  sSq- || sSq-s
  Addr:  -
  Float: -

If the `s` matches `S`, discard `s` and run the quote before exiting the caller. If they do not match, discard the quote and leave `s` on the stack.

Class: class:word | Namespace: s | Interface Layer: all

s:const

  Data:  ss-
  Addr:  -
  Float: -

Create a constant named s2, returning a pointer to s1. This will use `s:keep` to preserve the original string.

Class: class:word | Namespace: s | Interface Layer: all

s:contains/char?

  Data:  sc-f
  Addr:  -
  Float: -

Return `TRUE` if the character is present in the string or `FALSE` otherwise.

Class: class:word | Namespace: s | Interface Layer: all

s:copy

  Data:  sa-
  Addr:  -
  Float: -

Copy a string (s) to a destination (a). This will include the terminator character when copying.

Class: class:word | Namespace: s | Interface Layer: all

s:eq?

  Data:  ss-f
  Addr:  -
  Float: -

Compare two strings for equality. Return `TRUE` if identical or `FALSE` if not.

Class: class:word | Namespace: s | Interface Layer: all

Example #1:

    'hello 'again s:eq?
    'test  'test  s:eq?

s:format

  Data:  ...s-s
  Addr:  -
  Float: -

Construct a new string using the template passed and items from the stack.

Class: class:word | Namespace: s | Interface Layer: all

s:get

  Data:  -s
  Addr:  -
  Float: -

Read input from standard in (via `c:get`) until a CR or LF is encountered. Returns a string.

Class: class:word | Namespace: all | Interface Layer: rre

s:index/char

  Data:  sc-n
  Addr:  -
  Float: -

Return the location of the first instance of the specified character in the string.

Class: class:word | Namespace: s | Interface Layer: all

s:keep

  Data:  s-s
  Addr:  -
  Float: -

Store a string into the heap and return a pointer to the start of it.

Class: class:word | Namespace: s | Interface Layer: all

s:length

  Data:  s-n
  Addr:  -
  Float: -

Return the number of characters in a string, excluding the NULL terminator.

Class: class:word | Namespace: s | Interface Layer: all

s:put

  Data:  s-
  Addr:  -
  Float: -

Display a string.

Class: class:word | Namespace: global | Interface Layer: all

s:split/char

  Data:  sc-ss
  Addr:  -
  Float: -

Split a string on the first occurrence of the specified character.

Class: class:word | Namespace: s | Interface Layer: all

s:to-upper

  Data:  s-s
  Addr:  -
  Float: -

Convert lowercase ASCII characters in a string to uppercase.

Class: class:word | Namespace: s | Interface Layer: all

sip

  Data:  nq(?n-?)-n
  Addr:  -
  Float: -

Run quote. After execution completes, put a copy of n back on top of the stack.

Class: class:word | Namespace: global | Interface Layer: all

store

  Data:  na-
  Addr:  -
  Float: -

Store a value into the specified address.

Class: class:primitive | Namespace: global | Interface Layer: all

Example #1:

    'Base var
    #10 &Base store

swap

  Data:  nm-mn
  Addr:  -
  Float: -

Exchange the position of the top two items on the stack

Class: class:primitive | Namespace: global | Interface Layer: all

times

  Data:  nq-
  Addr:  -
  Float: -

Run the specified quote the specified number of times.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #12 [ $- c:put ] times

tri

  Data:  xqqq-?
  Addr:  -
  Float: -

Apply q1 against x, then q2 against a copy of x, and finally q3 against another copy of x.

Class: class:word | Namespace: global | Interface Layer: all

until

  Data:  q(-f)-
  Addr:  -
  Float: -

Execute quote repeatedly while the quote returns a value of `FALSE`. The quote should return a flag of either `TRUE` or `FALSE`, though `until` will treat any non-zero value as `TRUE`.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    #10 [ dup n:put nl n:dec dup n:zero? ] until

v:inc

  Data:  a-
  Addr:  -
  Float: -

Increment the value stored at the specified address by 1.

Class: class:word | Namespace: v | Interface Layer: all

var

  Data:  s-
  Addr:  -
  Float: -

Create a variable. The variable is initialized to 0.

Class: class:word | Namespace: global | Interface Layer: all

Example #1:

    'Base var

sigil:!

  Data:  ns-
  Addr:  -
  Float: -

Store a value into a variable.

Interpret Time:
  Store a value into the named variable.

Compile Time:
  Compile the code to store a value into a named variable.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:#

  Data:  s-n
  Addr:  -
  Float: -

Process token as a number.

Interpret Time:
  Convert the string into a number and leave on the stack.

Compile Time:
  Convert the string into a number and compile into the current definition as a literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:$

  Data:  s-c
  Addr:  -
  Float: -

Process token as an ASCII character.

Interpret Time:
  Fetch first character from string. Leave on stack.

Compile Time:
  Fetch first character from the string. Compile into the current definition as  literal.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:&

  Data:  s-a
  Addr:  -
  Float: -

Return a pointer to a named item. If name is not found, returns 0.

Interpret Time:
  Lookup name in dictionary, return contents of the xt field on the stack.

Compile Time:
  Lookup name in dictionary, compile code to push the contents of the xt field into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:'

  Data:  s-s
  Addr:  -
  Float: -

Process token as a string.

Interpret Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space.

Compile Time:
  Move string into temporary buffer. If `RewriteUnderscores` is `TRUE`, replace all instances of _ with space. Then compile the string into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:(

  Data:  s-
  Addr:  -
  Float: -

Process token as a comment.

Interpret Time:
  Discard the string.

Compile Time:
  Discard the string.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil::

  Data:  s-
  Addr:  -
  Float: -

Hook. Process token as a new definition.

Interpret Time:
  Create a header pointing to `here` with the provided string as the name. Sets class to `class:word`.

Class: class:macro | Namespace: sigil | Interface Layer: all

sigil:@

  Data:  s-n
  Addr:  -
  Float: -

Fetch from a stored variable.

Interpret Time:
  Fetch a value from a named variable.

Compile Time:
  Compile the code to fetch a value from a named variable into the current definition.

Class: class:macro | Namespace: sigil | Interface Layer: all

