<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Prolog to Mercury transition guide: IO</title>

<meta name="description" content="The Prolog to Mercury transition guide: IO">
<meta name="keywords" content="The Prolog to Mercury transition guide: IO">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html#Top" rel="up" title="Top">
<link href="AssertRetract.html#AssertRetract" rel="next" title="AssertRetract">
<link href="Syntax.html#Syntax" rel="previous" title="Syntax">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="IO"></a>
<div class="header">
<p>
Next: <a href="AssertRetract.html#AssertRetract" accesskey="n" rel="next">AssertRetract</a>, Previous: <a href="Syntax.html#Syntax" accesskey="p" rel="previous">Syntax</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Input-and-output"></a>
<h2 class="chapter">3 Input and output</h2>

<p>Mercury is a purely declarative language.
Therefore it cannot use Prolog&rsquo;s mechanism for doing
input and output with side-effects.
The mechanism that Mercury uses is the threading of an object
that represents the state of the world through the computation.
The type of this object is <code>io.state</code>, or just <code>io</code> for short.
Each operation that affects the state of the world
must have two arguments of this type,
representing respectively the state of the world before the operation,
and the state of the world after the operation.
The modes of the two arguments that are added to calls are
<code>di</code> for &ldquo;destructive input&rdquo; and <code>uo</code> for &ldquo;unique output&rdquo;.
The first means that the input variable
must be the last reference to the original state of the world,
and the latter means that the output variable is guaranteed to be the
only reference to the state of the world produced by this predicate.
</p>
<p>For example, the direct translation of the Prolog predicate
</p>
<div class="example">
<pre class="example">write_total(Total) :-
    write('The total is '),
    write(Total),
    write('.'),
    nl.
</pre></div>

<p>into Mercury yields this Mercury predicate:
</p>
<div class="example">
<pre class="example">:- pred write_total(int::in, io::di, io::uo) is det.

write_total(Total, IO0, IO) :-
    print(&quot;The total is &quot;, IO0, IO1),
    print(Total, IO1, IO2),
    print('.', IO2, IO3),
    nl(IO3, IO).
</pre></div>

<p>The variables <code>IO0</code>, <code>IO1</code> etc each represent
one version of the state of the world.
<code>IO0</code> represents the state before the total is printed,
<code>IO1</code> represents the state after just <code>The total is</code> is printed,
and so on.
However, programmers usually don&rsquo;t want to give specific names
to all these different versions;
they want to name only the entities
that all these variables represent different versions of.
That is why Mercury supports state variable notation.
This is syntactic sugar
designed to make it easier to thread a sequence of variables
holding the successive states of an entity through a clause.
You as the programmer name only the entity,
and let the compiler name the various versions.
With state variables, the above clause would be written as
</p>
<div class="example">
<pre class="example">write_total(Total, !IO) :-
    print(&quot;The total is &quot;, !IO),
    print(Total, !IO),
    print('.', !IO),
    nl(!IO).
</pre></div>

<p>and the compiler will internally convert this clause
into code that looks like the previous clause.
(The usual convention in Mercury programs
is to name the state variable representing the state of the world <code>!IO</code>.)
</p>
<p>In the head of a clause,
what looks like an argument that consists of
a variable name prefixed by an exclamation mark
actually stands for two arguments which are both variables,
holding the initial and final state
of whatever entity the state variable stands for.
In this case, they stand for the state of the world,
respectively before and after the line about the total has been printed.
In calls in the body of a clause,
what looks like an argument that consists of
a variable name prefixed by an exclamation mark
also stands for two arguments which are both variables,
but these hold respectively, the current and the next state.
</p>
<p>In Prolog, it is quite normal to give to <code>print</code>
an argument that is an atom that is not used anywhere else in the program,
or at least not in code related to the code that does the printing.
This is because the term being printed
does not have to belong to a defined type.
Since Mercury is strongly typed, the atom being printed
<em>would</em> have to be a data constructor of a defined type.
A Mercury programmer <em>could</em> define a meaningless type
just to give one of its data constructors to a call to <code>print</code>,
but it is far better to simply call a predicate specifically designed
to print the string, or integer, or character, you want to print:
</p>
<div class="example">
<pre class="example">write_total(Total, !IO) :-
    io.write_string(&quot;The total is &quot;, !IO),
    io.write_int(Total, !IO),
    io.write_char('.', !IO),
    io.nl(!IO).
</pre></div>

<p>The <code>io.</code> prefix on the predicates called in the body indicates that
the callees are in the <code>io</code> module of the Mercury standard library.
This module contains all of Mercury&rsquo;s primitive I/O operations.
These <em>module qualifications</em> are not strictly necessary
(unless two or more modules define predicates
with the same names and argument types,
the Mercury compiler can figure out which modules called predicates are in),
but Mercury convention is to make the module qualifier explicit
in order to make the intent of the code crystal clear to readers.
</p>
<p>The above could also be written more compactly like this:
</p><div class="example">
<pre class="example">write_total(Total, !IO) :-
    io.format(&quot;The total is %d.\n&quot;, [i(Total)], !IO).
</pre></div>

<p>The first argument of <code>io.format</code> is a format string
modelled directly on the format strings supported by <code>printf</code> in C,
while the second is a list of the values to be printed,
which should have one value for each conversion specifier.
In this case, there is one conversion specifier, &lsquo;<samp>%d</samp>&rsquo;,
which calls for the printing of an integer as a decimal number,
and the corresponding value is the integer <code>Total</code>.
Since Mercury is strongly typed,
and different arguments may have different types,
in the argument list
integers must be wrapped inside <code>i()</code>,
floats must be wrapped inside <code>f()</code>,
strings must be wrapped inside <code>s()</code>, and
chars must be wrapped inside <code>c()</code>.
Despite appearances, in the usual case of the format string being constant,
the wrappers and the list of arguments have neither time nor space overhead,
because the compiler optimizes them away,
replacing the call to <code>io.format</code> with
the calls to <code>io.write_string</code>, <code>io.write_int</code> etc above.
</p>
<p>One of the important consequences of our model for input and output
is that predicates that can fail may not do input or output.
This is because the state of the world must be a unique object,
and each I/O operation destructively replaces it with a new state.
Since each I/O operation destroys the current state object
and produces a new one,
it is not possible for I/O to be performed in a context that may fail,
since when failure occurs the old state of the world will have been destroyed,
and since bindings cannot be exported from a failing computation,
the new state of the world is not accessible.
</p>
<p>In some circumstances, Prolog programs that suffer from this problem
can be fixed by moving the I/O out of the failing context.
For example
</p>
<div class="example">
<pre class="example">    &hellip;
    ( solve(Goal) -&gt;
        &hellip;
    ;
        &hellip;
    ),
    ...
</pre></div>

<p>where &lsquo;<samp>solve(Goal)</samp>&rsquo; does some I/O
can be transformed into valid Mercury in at least two ways.
The first is to make &lsquo;<samp>solve</samp>&rsquo; deterministic and return a status:
</p>
<div class="example">
<pre class="example">    &hellip; 
    solve(Goal, Result, !IO),
    (
        Result = success(&hellip;),
        &hellip;        
    ;
        Result = failure,
        &hellip; 
    ),
    &hellip;    
</pre></div>

<p>The other way is to transform &lsquo;<samp>solve</samp>&rsquo; so that
all the input and output takes place outside it:
</p>
<div class="example">
<pre class="example">    ...
    io.write_string(&quot;calling: &quot;, !IO),
    solve.write_goal(Goal, !IO),
    ( solve(Goal) -&gt;
        io.write_string(&quot;succeeded\n&quot;, !IO),
        ...
    ;
        ...
    ),
    ...
</pre></div>

<hr>
<div class="header">
<p>
Next: <a href="AssertRetract.html#AssertRetract" accesskey="n" rel="next">AssertRetract</a>, Previous: <a href="Syntax.html#Syntax" accesskey="p" rel="previous">Syntax</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
