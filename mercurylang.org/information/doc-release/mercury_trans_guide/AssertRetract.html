<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Prolog to Mercury transition guide: AssertRetract</title>

<meta name="description" content="The Prolog to Mercury transition guide: AssertRetract">
<meta name="keywords" content="The Prolog to Mercury transition guide: AssertRetract">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html#Top" rel="up" title="Top">
<link href="FailLoops.html#FailLoops" rel="next" title="FailLoops">
<link href="IO.html#IO" rel="previous" title="IO">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="AssertRetract"></a>
<div class="header">
<p>
Next: <a href="FailLoops.html#FailLoops" accesskey="n" rel="next">FailLoops</a>, Previous: <a href="IO.html#IO" accesskey="p" rel="previous">IO</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Assert-and-retract"></a>
<h2 class="chapter">4 Assert and retract</h2>

<p>In Prolog, calls to the builtin predicates <code>assert</code> and <code>retract</code>
can change the set of clauses of the program currently being executed.
This makes compilation very tricky,
and different Prolog systems react differently
when the program alters the definition of a predicate that has active calls.
It also makes program analysis almost impossible,
since the program that the compiler should analyze
is not actually available at compilation time.
Since Mercury is a compiled language,
it does not allow the compiled program to be altered in any way.
</p>
<p>Most uses of <code>assert</code> and <code>retract</code> in Prolog programs
are not actually intended to alter the program.
Their purpose is just to maintain a set of facts,
with semantically separate sets of facts being stored in separate predicates.
(Most Prolog systems require these predicates
to be marked as <code>dynamic</code> predicates.)
A Mercury programmer who wants to store a set of facts
would simply store those facts as data (not as code) in a data structure.
</p>
<p>The standard library contains
several abstract data types (ADTs) for storing collections of items,
each of which is useful for different classes of problems.
</p>
<p>If the order of the items in the collection is important,
consider the <code>list</code> and <code>cord</code> ADTs.
<code>list</code> has lower constant factors,
but the <code>cord</code> ADTs supports concatenation in constant time.
The <code>stack</code> and <code>queue</code> ADTs implement
lists with specific semantics and operations appropriate to those semantics.
</p>
<p>If the order of items in the collection is not important,
and if the items are key-value pairs,
you can store them in ADTs implementing several different kinds of trees,
including <code>rbtree</code> and <code>tree234</code>.
In the absence of a compelling reason to choose a different implementation,
we recommend the <code>map</code> ADT for generic use.
Maps are implemented using 234 trees,
which are guaranteed to be balanced and thus have good worst-case behavior,
but also have good performance in the average case.
<code>bimap</code>, <code>injection</code>, <code>multi_map</code> and <code>rtree</code> are
specialized version of maps.
</p>
<p>If the items in the collection are not key-value pairs,
then consider the <code>set</code> and <code>bag</code> ADTs.
The <code>set</code> ADT itself has several versions,
some based on trees and some based on bit vectors,
each with its own tradeoffs.
</p>
<p>The Mercury standard library has some modules
for more specialized collections as well, such as graphs.
And of course, if needed, you can always create your own ADT.
</p>
<p>If for some reason you cannot thread variables holding some data
through the parts of your program that need access to that data,
then you can store that data in a &lsquo;<samp>mutable</samp>&rsquo;,
which is as close as Mercury comes to Prolog&rsquo;s dynamic predicates.
Each Mercury mutable stores one value,
though of course this value can be a collection,
and that collection may be (but doesn&rsquo;t have to be) implemented
by one of the Mercury standard library modules listed above.
</p>
<p>Each mutable has a getter and setter predicate.
You can set things up so that the getter and setter predicates
both function as I/O operations,
destroying the current state of the world
and returning a new state of the world.
This effectively considers the mutable to be part of the state of the world
<em>outside</em> the Mercury program.
The <code>io</code> module also provides another way to do this,
by allowing the storage of information in the <code>io.state</code>
using the predicates <code>io.get_globals</code> and <code>io.set_globals</code>.
These predicates take an argument of type <code>univ</code>, the universal type,
so that by using <code>type_to_univ</code> and <code>univ_to_type</code>
it is possible to store data of any type in the <code>io.state</code>.
</p>
<p>Alternatively, you can set things up so that
the getter and setter predicates of a mutable are <em>not</em> I/O operations,
but in that case calls to those predicates are not considered pure Mercury,
and must instead use Mercury&rsquo;s mechanisms for controlled impurity.
These mechanisms require all code that is not pure Mercury
to be explicitly marked as such.
They are intended to allow programmers
to implement pure interfaces using <em>small</em> pieces of impure code,
for use in circumstances where there is no feasible way
to implement that same interface using pure code.
Most Mercury programs do not use impure code at all.
The ones that do make use of it use it very sparingly,
with 99.9+% of their code being pure Mercury.
</p>
<hr>
<div class="header">
<p>
Next: <a href="FailLoops.html#FailLoops" accesskey="n" rel="next">FailLoops</a>, Previous: <a href="IO.html#IO" accesskey="p" rel="previous">IO</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
