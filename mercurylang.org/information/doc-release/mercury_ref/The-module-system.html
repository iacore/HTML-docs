<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: The module system</title>

<meta name="description" content="The Mercury Language Reference Manual: The module system">
<meta name="keywords" content="The Mercury Language Reference Manual: The module system">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Modules.html#Modules" rel="up" title="Modules">
<link href="An-example-module.html#An-example-module" rel="next" title="An example module">
<link href="Modules.html#Modules" rel="previous" title="Modules">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="The-module-system"></a>
<div class="header">
<p>
Next: <a href="An-example-module.html#An-example-module" accesskey="n" rel="next">An example module</a>, Up: <a href="Modules.html#Modules" accesskey="u" rel="up">Modules</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="The-module-system-1"></a>
<h3 class="section">9.1 The module system</h3>

<p>The Mercury module system is relatively simple and straightforward.
</p>
<p>Each module must start with a &lsquo;<samp>:- module <var>ModuleName</var></samp>&rsquo; declaration,
specifying the name of the module.
</p>
<p>An &lsquo;<samp>:- interface.</samp>&rsquo; declaration indicates
the start of the module&rsquo;s interface section:
this section specifies the entities that are exported by this module.
Mercury provides support for abstract data types,
by allowing the definition of a type to be kept hidden,
with the interface only exporting the type name.
The interface section may contain definitions of types,
type classes, data constructors, instantiation states, and modes,
and declarations for abstract data types, abstract type class instances,
functions, predicates, and (sub-)modules.
The interface section may not contain definitions
for functions or predicates (i.e. clauses),
or definitions of (sub-)modules.
</p>
<p>An &lsquo;<samp>:- implementation.</samp>&rsquo; declaration indicates
the start of the module&rsquo;s implementation section.
Any entities declared in this section are local to the module
(and its submodules, if any) and cannot be used by other modules.
The implementation section must contain definitions
for all abstract data types, abstract instance declarations,
functions, predicates, and submodules exported by the module,
as well as for all local types, type class instances, functions,
predicates, and submodules.
The implementation section can be omitted if it is empty.
</p>
<p>The module may optionally end
with a &lsquo;<samp>:- end_module <var>ModuleName</var></samp>&rsquo; declaration;
the name specified in the &lsquo;<samp>end_module</samp>&rsquo; must be the same
as that in the corresponding &lsquo;<samp>module</samp>&rsquo; declaration.
</p>

<p>If a module wishes to make use of entities exported by other modules,
then it must explicitly import those modules
using one or more &lsquo;<samp>:- import_module <var>Modules</var></samp>&rsquo;
or &lsquo;<samp><span class="nolinebreak">:-</span>&nbsp;<span class="nolinebreak">use_module</span>&nbsp;<var>Modules</var></samp>&rsquo;<!-- /@w --> declarations,
in order to make those declarations visible.
In both cases, <var>Modules</var>
is a comma-separated list of fully qualified module names.
These declarations may occur
either in the interface or the implementation section.
If the imported entities are used in the interface section,
then the corresponding <code>import_module</code> or <code>use_module</code>
declaration must also be in the interface section.
If the imported entities are only used in the implementation section,
the <code>import_module</code> or <code>use_module</code> declaration
should be in the implementation section.
</p>
<p>The names of predicates, functions, constructors, constructor fields,
types, modes, insts, type classes, and (sub-)modules
can be explicitly module qualified using the &lsquo;<samp>.</samp>&rsquo; operator,
e.g. &lsquo;<samp>module.name</samp>&rsquo; or &lsquo;<samp>module.submodule.name</samp>&rsquo;.
This is useful both for readability and for resolving name conflicts.
Uses of entities imported using <code>use_module</code> declarations
<em>must</em> be explicitly fully module qualified.
</p>
<p>Currently we also support &lsquo;<samp>__</samp>&rsquo; as an alternative module qualifier,
so you can write <code>module__name</code> instead of <code>module.name</code>.
</p>
<p>Certain optimizations require information or source code
for predicates defined in other modules to be as effective as possible.
At the moment, inlining and higher-order specialization
are the only optimizations
that the Mercury compiler can perform across module boundaries.
</p>
<p>Exactly one module of the program
must export a predicate &lsquo;<samp>main/2</samp>&rsquo;,
which must be declared as either
</p>
<div class="example">
<pre class="example">:- pred main(io.state::di, io.state::uo) is det.
</pre></div>

<p>or
</p>
<div class="example">
<pre class="example">:- pred main(io.state::di, io.state::uo) is cc_multi.
</pre></div>

<p>(or any declaration equivalent to one of the two above).
</p>
<p>Mercury has a standard library which includes over 100 modules,
including modules for
lists, stacks, queues, priority queues, sets, bags (multi-sets),
maps (dictionaries), random number generation, input/output,
and filename and directory handling.
See the Mercury Library Reference Manual for a list of the available modules,
and for the documentation of each module.
</p>
<hr>
<div class="header">
<p>
Next: <a href="An-example-module.html#An-example-module" accesskey="n" rel="next">An example module</a>, Up: <a href="Modules.html#Modules" accesskey="u" rel="up">Modules</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
