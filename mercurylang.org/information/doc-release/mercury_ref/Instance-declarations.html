<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Instance declarations</title>

<meta name="description" content="The Mercury Language Reference Manual: Instance declarations">
<meta name="keywords" content="The Mercury Language Reference Manual: Instance declarations">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Type-classes.html#Type-classes" rel="up" title="Type classes">
<link href="Abstract-typeclass-declarations.html#Abstract-typeclass-declarations" rel="next" title="Abstract typeclass declarations">
<link href="Typeclass-declarations.html#Typeclass-declarations" rel="previous" title="Typeclass declarations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Instance-declarations"></a>
<div class="header">
<p>
Next: <a href="Abstract-typeclass-declarations.html#Abstract-typeclass-declarations" accesskey="n" rel="next">Abstract typeclass declarations</a>, Previous: <a href="Typeclass-declarations.html#Typeclass-declarations" accesskey="p" rel="previous">Typeclass declarations</a>, Up: <a href="Type-classes.html#Type-classes" accesskey="u" rel="up">Type classes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Instance-declarations-1"></a>
<h3 class="section">10.2 Instance declarations</h3>

<p>Once the interface of the type class
has been defined in the <code>typeclass</code> declaration,
we can use an <code>instance</code> declaration
to define how a particular type (or sequence of types)
satisfies the interface declared in the <code>typeclass</code> declaration.
</p>
<p>An instance declaration has the form
</p>
<div class="example">
<pre class="example">:- instance <var>classname</var>(<var>typename</var>(<var>typevar</var>, &hellip;), &hellip;)
        where [<var>method_definition</var>, <var>method_definition</var>, &hellip;].
</pre></div>

<p>An &lsquo;<samp>instance</samp>&rsquo; declaration
gives a type for each parameter of the type class.
Each of these types must be either a type with no arguments,
or a polymorphic type whose arguments are all type variables.
For example <code>int</code>, <code>list(T)</code>,
<code>bintree(K, V)</code> and <code>bintree(T, T)</code> are allowed,
but <code>T</code> and <code>list(int)</code> are not.
The types in an instance declaration must not be abstract types
which are elsewhere defined as equivalence types.
A program may not contain
more than one instance declaration for a particular type
(or sequence of types, in the case of a multi-parameter type class)
and typeclass.
These restrictions ensure that there are no overlapping instance declarations,
i.e. for each typeclass there is at most one instance declaration
that may be applied to any type (or sequence of types).
</p>
<p>There is no special interaction between subtypes and the typeclass system.
A subtype is <em>not</em> automatically an instance of a typeclass if
there is an &lsquo;<samp>instance</samp>&rsquo; declaration for its supertype.
</p>
<p>Each <var>method_definition</var> entry
in the &lsquo;<samp>where [&hellip;]</samp>&rsquo; part of an <code>instance</code> declaration
defines the implementation of one of the class methods for this instance.
There are two ways of defining methods.
</p>
<p>The first way to define a method is by
giving the name of the predicate or function which implements that method.
In this case, the <var>method_definition</var> must have one of the following forms:
</p>
<div class="example">
<pre class="example">pred(<var>method_name</var>/<var>arity</var>) is <var>predname</var>
func(<var>method_name</var>/<var>arity</var>) is <var>funcname</var>
</pre></div>

<p>The <var>predname</var> or <var>funcname</var> must name
a predicate or function of the specified arity
whose type, modes, determinism, and purity are at least as permissive
as the declared type, modes, determinism, and purity
of the class method with the specified <var>method_name</var> and <var>arity</var>,
after the types of the arguments in the instance declaration
have been substituted in place of the parameters in the type class declaration.
</p>
<p>The second way of defining methods is
by listing the clauses for the definition inside the instance declaration.
A <var>method_definition</var> can be a clause.
These clauses are just like the clauses
used to define ordinary predicates or functions (see <a href="Items.html#Items">Items</a>),
and so they can be facts, rules, or DCG rules.
The only difference is that in instance declarations,
clauses are separated by commas rather than being terminated by periods,
and so rules and DCG rules in instance declarations
must normally be enclosed in parentheses.
As with ordinary predicates,
you can have more than one clause for each method.
The clauses must satisfy
the declared type, modes, determinism and purity for the method,
after the types of the arguments in the instance declaration
have been substituted in place of the parameters in the type class declaration.
</p>
<p>These two ways are mutually exclusive:
each method must be defined either by a single naming definition
(using the &lsquo;<samp>pred(&hellip;) is <var>predname</var></samp>&rsquo;
or &lsquo;<samp>func(&hellip;) is <var>funcname</var></samp>&rsquo; form),
or by a set of one or more clauses, but not both.
</p>
<p>Here is an example of an instance declaration
and the different kinds of method definitions that it can contain:
</p>
<div class="example">
<pre class="example">:- typeclass foo(T) where [
    func method1(T, T) = int,
    func method2(T) = int,
    pred method3(T::in, int::out) is det,
    pred method4(T::in, io.state::di, io.state::uo) is det,
    func method5(bool, T) = T
].

:- instance foo(int) where [
    % method defined by naming the implementation
    func(method1/2) is (+),

    % method defined by a fact
    method2(X) = X + 1,

    % method defined by a rule
    (method3(X, Y) :- Y = X + 2),

    % method defined by a DCG rule
    (method4(X) --&gt; io.print(X), io.nl),

    % method defined by multiple clauses
    method5(no, _) = 0,
    (method5(yes, X) = Y :- X + Y = 0)
].
</pre></div>

<p>Each &lsquo;<samp>instance</samp>&rsquo; declaration
must define an implementation for every method
declared in the corresponding &lsquo;<samp>typeclass</samp>&rsquo; declaration.
It is an error to define more than one implementation
for the same method within a single &lsquo;<samp>instance</samp>&rsquo; declaration.
</p>
<p>Any call to a method must have argument types
(and in the case of functions, return type)
which are constrained to be a member of that method&rsquo;s type class,
or which match one of the instance declarations
visible at the point of the call.
A method call will invoke the predicate or function
specified for that method in the instance declaration
that matches the types of the arguments to the call.
</p>
<p>Note that even if a type class has no methods,
an explicit instance declaration is required
for a type to be considered an instance of that type class.
</p>
<p>Here is an example of some code using an instance declaration:
</p>
<div class="example">
<pre class="example">:- type coordinate
    ---&gt;    coordinate(
                float,  % X coordinate
                float   % Y coordinate
            ).

:- instance point(coordinate) where [
    pred(coords/3) is coordinate_coords,
    func(translate/3) is coordinate_translate
].

:- pred coordinate_coords(coordinate, float, float).
:- mode coordinate_coords(in, out, out) is det.

coordinate_coords(coordinate(X, Y), X, Y).

:- func coordinate_translate(coordinate, float, float) = coordinate.

coordinate_translate(coordinate(X, Y), Dx, Dy) = coordinate(X + Dx, Y + Dy).
</pre></div>

<p>We have now made the <code>coordinate</code> type
an instance of the <code>point</code> type class.
If we introduce a new type <code>coloured_coordinate</code>
which represents a point in two dimensional space
with a colour associated with it,
it can also become an instance of the type class:
</p>
<div class="example">
<pre class="example">:- type rgb
    ---&gt;    rgb(
                int,
                int,
                int
            ).

:- type coloured_coordinate
    ---&gt;    coloured_coordinate(
                float,
                float,
                rgb
            ).

:- instance point(coloured_coordinate) where [
    pred(coords/3) is coloured_coordinate_coords,
    func(translate/3) is coloured_coordinate_translate
].

:- pred coloured_coordinate_coords(coloured_coordinate, float, float).
:- mode coloured_coordinate_coords(in, out, out) is det.

coloured_coordinate_coords(coloured_coordinate(X, Y, _), X, Y).

:- func coloured_coordinate_translate(coloured_coordinate, float, float)
    = coloured_coordinate.

coloured_coordinate_translate(coloured_coordinate(X, Y, Colour), Dx, Dy)
    = coloured_coordinate(X + Dx, Y + Dy, Colour).
</pre></div>

<p>If we call &lsquo;<samp>translate/3</samp>&rsquo;
with the first argument having type &lsquo;<samp>coloured_coordinate</samp>&rsquo;,
this will invoke &lsquo;<samp>coloured_coordinate_translate</samp>&rsquo;.
Likewise, if we call &lsquo;<samp>translate/3</samp>&rsquo;
with the first argument having type &lsquo;<samp>coordinate</samp>&rsquo;,
this will invoke &lsquo;<samp>coordinate_translate</samp>&rsquo;.
</p>
<p>Further instances of the type class could be made,
e.g. a type that represents the point using polar coordinates.
</p>
<p>Since methods may be defined using clauses,
and the interface sections of modules may <em>not</em> include clauses,
instance declarations that specify method definitions
may appear only in the implementation section of a module.
If you want to export the knowledge that a type, or a sequence of types,
is a member of a given typeclass,
then put a version of the instance declaration
that omits all method definitions
(see <a href="Abstract-instance-declarations.html#Abstract-instance-declarations">Abstract instance declarations</a>)
into the interface section of the module
that contains the full instance declaration in its implementation section.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Abstract-typeclass-declarations.html#Abstract-typeclass-declarations" accesskey="n" rel="next">Abstract typeclass declarations</a>, Previous: <a href="Typeclass-declarations.html#Typeclass-declarations" accesskey="p" rel="previous">Typeclass declarations</a>, Up: <a href="Type-classes.html#Type-classes" accesskey="u" rel="up">Type classes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
