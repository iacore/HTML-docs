<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: pragma foreign_proc</title>

<meta name="description" content="The Mercury Language Reference Manual: pragma foreign_proc">
<meta name="keywords" content="The Mercury Language Reference Manual: pragma foreign_proc">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Calling-foreign-code-from-Mercury.html#Calling-foreign-code-from-Mercury" rel="up" title="Calling foreign code from Mercury">
<link href="Foreign-code-attributes.html#Foreign-code-attributes" rel="next" title="Foreign code attributes">
<link href="Calling-foreign-code-from-Mercury.html#Calling-foreign-code-from-Mercury" rel="previous" title="Calling foreign code from Mercury">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="pragma-foreign_005fproc"></a>
<div class="header">
<p>
Next: <a href="Foreign-code-attributes.html#Foreign-code-attributes" accesskey="n" rel="next">Foreign code attributes</a>, Up: <a href="Calling-foreign-code-from-Mercury.html#Calling-foreign-code-from-Mercury" accesskey="u" rel="up">Calling foreign code from Mercury</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="pragma-foreign_005fproc-1"></a>
<h4 class="subsection">15.1.1 pragma foreign_proc</h4>

<p>A declaration of the form
</p>
<div class="example">
<pre class="example">:- pragma foreign_proc(&quot;<var>Lang</var>&quot;,
    <var>Pred</var>(<var>Var1</var>::<var>Mode1</var>, <var>Var2</var>::<var>Mode2</var>, &hellip;),
    <var>Attributes</var>, <var>Foreign_Code</var>).
</pre></div>

<p>or
</p>
<div class="example">
<pre class="example">:- pragma foreign_proc(&quot;<var>Lang</var>&quot;,
    <var>Func</var>(<var>Var1</var>::<var>Mode1</var>, <var>Var2</var>::<var>Mode2</var>, &hellip;) = (<var>Var</var>::<var>Mode</var>),
    <var>Attributes</var>, <var>Foreign_Code</var>).
</pre></div>

<p>means that any calls to the specified mode of <var>Pred</var> or <var>Func</var>
will result in execution of the foreign code given in <var>Foreign_Code</var>
written in language <var>Lang</var>,
if <var>Lang</var> is selected as the foreign language code by this implementation.
See the &ldquo;Foreign Language Interface&rdquo; chapter of the Mercury User&rsquo;s Guide,
for more information about how the implementation selects
the appropriate &lsquo;<samp>foreign_proc</samp>&rsquo; to use.
</p>
<p>The foreign code fragment may refer to the specified variables
(<var>Var1</var>, <var>Var2</var>, &hellip;, and <var>Var</var>) directly by name.
It is an error for a variable to occur more than once in the argument list.
These variables will have foreign language types
corresponding to their Mercury types,
as determined by language and implementation specific rules.
</p>
<p>All &lsquo;<samp>foreign_proc</samp>&rsquo; implementations are assumed to be impure.
If they are actually pure or semipure,
they must be explicitly promised as such by the user
(either by using foreign language attributes specified below,
or a &lsquo;<samp>promise_pure</samp>&rsquo; or &lsquo;<samp>promise_semipure</samp>&rsquo; pragma
as specified in <a href="Impurity.html#Impurity">Impurity</a>).
</p>
<p>Additional restrictions on the foreign language interface code
depend on the foreign language and compilation options.
For more information, including the list of supported foreign languages
and the strings used to identify them,
see the language specific information
in the &ldquo;Foreign Language Interface&rdquo; chapter of the Mercury User&rsquo;s Guide.
</p>
<p>If there is a <code>pragma foreign_proc</code> declaration
for any mode of a predicate or function,
then there must be either a clause or a <code>pragma foreign_proc</code> declaration
for every mode of that predicate or function.
</p>
<p>Here is an example of code using &lsquo;<samp>pragma foreign_proc</samp>&rsquo;.
The following code defines a Mercury function &lsquo;<samp>sin/1</samp>&rsquo;
which calls the C function &lsquo;<samp>sin()</samp>&rsquo; of the same name.
</p>
<div class="example">
<pre class="example">:- func sin(float) = float.
:- pragma foreign_proc(&quot;C&quot;,
    sin(X::in) = (Sin::out),
    [promise_pure, may_call_mercury],
&quot;
    Sin = sin(X);
&quot;).
</pre></div>

<p>If the foreign language code does not recursively invoke Mercury code,
as in the above example, then you can use &lsquo;<samp>will_not_call_mercury</samp>&rsquo;
in place of &lsquo;<samp>may_call_mercury</samp>&rsquo; in the declarations above.
This allows the compiler to use a slightly more efficient calling convention.
(If you use this form, and the foreign code <em>does</em> invoke Mercury code,
then the behaviour is undefined &mdash; your program may misbehave or crash.)
</p>
<p>If there are both Mercury definitions and foreign_proc definitions
for a procedure and/or foreign_proc definitions for different languages,
it is implementation-defined which definition is used.
</p>
<p>For pure and semipure procedures,
the declarative semantics of the foreign_proc definitions
must be the same as that of the Mercury code.
The only thing that is allowed to differ is the efficiency
(including the possibility of non-termination)
and the order of solutions.
</p>
<p>It is an error for a procedure with a &lsquo;<samp>pragma foreign_proc</samp>&rsquo; declaration
to have a determinism of <code>multi</code> or <code>nondet</code>.
</p>
<p>Since foreign_procs with the determinism <code>multi</code> or <code>nondet</code>
cannot be defined directly,
procedures with those determinisms
that require foreign code in their implementation
must be defined using a combination
of Mercury clauses and (semi)deterministic foreign_procs.
The following implementation for the standard library predicate
&lsquo;<samp>string.append/3</samp>&rsquo; in the mode &lsquo;<samp>append(out, out, in) is multi</samp>&rsquo;
illustrates this technique:
</p>
<div class="example">
<pre class="example">:- pred append(string, string, string).
:- mode append(out, out, in) is multi.

append(S1, S2, S3) :-
    S3Len = string.length(S3),
    append_2(0, S3Len, S1, S2, S3).

:- pred append_2(int::in, int::in, string::out, string::out, string::in) is multi.

append_2(NextS1Len, S3Len, S1, S2, S3) :-
    ( NextS1Len = S3Len -&gt;
        append_3(NextS1Len, S3Len, S1, S2, S3)
    ;
        (
            append_3(NextS1Len, S3Len, S1, S2, S3)
        ;
            append_2(NextS1Len + 1, S3Len, S1, S2, S3)
        )
    ).

:- pred append_3(int::in, int::in, string::out, string::out, string::in) is det.

:- pragma foreign_proc(&quot;C&quot;,
    append_3(S1Len::in, S3Len::in, S1::out, S2::out, S3::in),
    [will_not_call_mercury, promise_pure],
&quot;
    S1 = allocate_string(S1Len);   /* Allocate a new string of length S1Len */
    memcpy(S1, S3, S1Len);
    S1[S1Len] = '\\0';
    S2 = allocate_string(S2, S3Len - S1Len);
    strcpy(S2, S3Len + S1Len);
&quot;).

</pre></div>

<hr>
<div class="header">
<p>
Next: <a href="Foreign-code-attributes.html#Foreign-code-attributes" accesskey="n" rel="next">Foreign code attributes</a>, Up: <a href="Calling-foreign-code-from-Mercury.html#Calling-foreign-code-from-Mercury" accesskey="u" rel="up">Calling foreign code from Mercury</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
