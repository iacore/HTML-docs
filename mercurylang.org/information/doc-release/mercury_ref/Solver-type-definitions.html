<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Solver type definitions</title>

<meta name="description" content="The Mercury Language Reference Manual: Solver type definitions">
<meta name="keywords" content="The Mercury Language Reference Manual: Solver type definitions">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Solver-types.html#Solver-types" rel="up" title="Solver types">
<link href="Implementing-solver-types.html#Implementing-solver-types" rel="next" title="Implementing solver types">
<link href="Abstract-solver-type-declarations.html#Abstract-solver-type-declarations" rel="previous" title="Abstract solver type declarations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Solver-type-definitions"></a>
<div class="header">
<p>
Next: <a href="Implementing-solver-types.html#Implementing-solver-types" accesskey="n" rel="next">Implementing solver types</a>, Previous: <a href="Abstract-solver-type-declarations.html#Abstract-solver-type-declarations" accesskey="p" rel="previous">Abstract solver type declarations</a>, Up: <a href="Solver-types.html#Solver-types" accesskey="u" rel="up">Solver types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Solver-type-definitions-1"></a>
<h3 class="section">17.3 Solver type definitions</h3>

<p>A solver type definition takes the following form:
</p>
<div class="example">
<pre class="example">:- solver type <var>solver_type</var>
    where   representation   is <var>representation_type</var>,
            ground           is <var>ground_inst</var>,
            any              is <var>any_inst</var>,
            constraint_store is <var>mutable_decls</var>,
            equality         is <var>equality_pred</var>,
            comparison       is <var>comparison_pred</var>.
</pre></div>

<p>The <code>representation</code> attribute is mandatory.
The <var>ground_inst</var> and <var>any_inst</var> attributes are optional
and default to <code>ground</code>.
The <code>constraint_store</code> attribute is mandatory:
<var>mutable_decls</var> must be either a single mutable declaration
(see <a href="Module_002dlocal-mutable-variables.html#Module_002dlocal-mutable-variables">Module-local mutable variables</a>),
or a comma separated list of mutable declarations in brackets.
The <code>equality</code> and <code>comparison</code> attributes are optional,
although a solver type without equality would not be very useful.
The attributes that are not omitted must appear in the order shown above.
</p>
<p>The <var>representation_type</var>
is the type used to implement the <var>solver_type</var>.
A two-tier scheme of this kind is necessary for a number of reasons,
including
</p><ul>
<li> a semantic gap is necessary to accommodate the fact that
non-ground <var>solver_type</var> values
may be represented by ground <var>representation_type</var> values
(in the context of the corresponding constraint solver state);
</li><li> this approach greatly simplifies
the definition of equality and comparison predicates for the <var>solver_type</var>.
</li></ul>


<p>The <var>ground_inst</var> is the inst associated with
<var>representation_type</var> values denoting <code>ground</code>
<var>solver_type</var> values.
</p>
<p>The <var>any_inst</var> is the inst associated with <var>representation_type</var> values
denoting <code>any</code> <code>solver_type</code> values.
</p>
<p>The compiler constructs four impure functions
for converting between <var>solver_type</var> values
and <var>representation_type</var> values
(<var>name</var> is the function symbol used to name <var>solver_type</var>
and <var>arity</var> is the number of type parameters it takes):
</p>
<div class="example">
<pre class="example">:- impure func 'representation of ground <var>name</var>/<var>arity</var>'(<var>solver_type</var>) =
                        <var>representation_type</var>.
:-        mode 'representation of ground <var>name</var>/<var>arity</var>'(in) =
                        out(<var>ground_inst</var>) is det.

:- impure func 'representation of any <var>name</var>/<var>arity</var>'(<var>solver_type</var>) =
                        <var>representation_type</var>.
:-        mode 'representation of any <var>name</var>/<var>arity</var>'(in(any)) =
                        out(<var>any_inst</var>) is det.

:- impure func 'representation to ground <var>name</var>/<var>arity</var>'(<var>representation_type</var>) =
                        <var>solver_type</var>.
:-        mode 'representation to ground <var>name</var>/<var>arity</var>'(in(<var>ground_inst</var>)) =
                        out is det.

:- impure func 'representation to any <var>name</var>/<var>arity</var>'(<var>representation_type</var>) =
                        <var>solver_type</var>.
:-        mode 'representation to any <var>name</var>/<var>arity</var>'(in(<var>any_inst</var>)) =
                        out(any) is det.
</pre></div>

<p>These functions are impure because of the semantic gap issue mentioned above.
</p>
<p>Solver types may be exported from their defining module,
but only in an abstract form.
This requires the full definition
to appear in the implementation section of the module,
and an abstract declaration like the following in its interface:
</p>
<div class="example">
<pre class="example">:- solver type <var>solver_type</var>.
</pre></div>

<p>If a solver type is exported,
then its representation type,
and, if specified, its equality and/or comparison predicates
must also exported from the same module.
</p>
<p>If a solver type has no equality predicate specified,
then the compiler will generate an equality predicate
that throws an exception of type &lsquo;<samp>exception.software_error/0</samp>&rsquo; when called.
</p>
<p>Likewise, if a solver type has no equality comparison specified,
then the compiler will generate a comparison predicate
that throws an exception of type &lsquo;<samp>exception.software_error/0</samp>&rsquo; when called.
</p>
<p>If provided,
any mutable declarations given for the <code>constraint_store</code> attribute
are equivalent to separate mutable declarations;
their association with the solver type is for the purposes of documentation.
That is,
</p>
<div class="example">
<pre class="example">:- solver type t
    where &hellip;,
          constraint_store is [ mutable(a, int, 42, ground, []),
                                mutable(b, string, &quot;Hi&quot;, ground, [])
                               ],
          &hellip;
</pre></div>

<p>is equivalent to
</p>
<div class="example">
<pre class="example">:- solver type t
    where &hellip;
:- mutable(a, int, 42, ground, []).
:- mutable(b, string, &quot;Hi&quot;, ground, []).
</pre></div>

<hr>
<div class="header">
<p>
Next: <a href="Implementing-solver-types.html#Implementing-solver-types" accesskey="n" rel="next">Implementing solver types</a>, Previous: <a href="Abstract-solver-type-declarations.html#Abstract-solver-type-declarations" accesskey="p" rel="previous">Abstract solver type declarations</a>, Up: <a href="Solver-types.html#Solver-types" accesskey="u" rel="up">Solver types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
