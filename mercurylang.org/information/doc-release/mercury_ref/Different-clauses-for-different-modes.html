<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Different clauses for different modes</title>

<meta name="description" content="The Mercury Language Reference Manual: Different clauses for different modes">
<meta name="keywords" content="The Mercury Language Reference Manual: Different clauses for different modes">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Modes.html#Modes" rel="up" title="Modes">
<link href="Unique-modes.html#Unique-modes" rel="next" title="Unique modes">
<link href="Constrained-polymorphic-modes.html#Constrained-polymorphic-modes" rel="previous" title="Constrained polymorphic modes">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Different-clauses-for-different-modes"></a>
<div class="header">
<p>
Previous: <a href="Constrained-polymorphic-modes.html#Constrained-polymorphic-modes" accesskey="p" rel="previous">Constrained polymorphic modes</a>, Up: <a href="Modes.html#Modes" accesskey="u" rel="up">Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Different-clauses-for-different-modes-1"></a>
<h3 class="section">4.4 Different clauses for different modes</h3>

<p>Because the compiler automatically reorders conjunctions to satisfy the modes,
it is often possible for a single clause to satisfy different modes.
However, occasionally reordering of conjunctions is not sufficient;
you may want to write different code for different modes.
</p>
<p>For example, the usual code for list append
</p>
<div class="example">
<pre class="example">append([], Ys, Ys).
append([X|Xs], Ys, [X|Zs]) :-
    append(Xs, Ys, Zs).
</pre></div>

<p>works fine in most modes,
but is not very satisfactory for the &lsquo;<samp>append(out, in, in)</samp>&rsquo; mode of append,
because although every call in this mode only has at most one solution,
the compiler&rsquo;s determinism inference will not be able to infer that.
This means that using the usual code
for append in this mode will be inefficient,
and the overly conservative determinism inference
may cause spurious determinism errors later.
</p>
<p>For this mode, it is better to use a completely different algorithm:
</p>
<div class="example">
<pre class="example">append(Prefix, Suffix, List) :-
    list.length(List, ListLength),
    list.length(Suffix, SuffixLength),
    PrefixLength = ListLength - SuffixLength,
    list.split_list(PrefixLength, List, Prefix, Suffix).
</pre></div>

<p>However, that code doesn&rsquo;t work in the other modes of &lsquo;<samp>append</samp>&rsquo;.
</p>
<p>To handle such cases, you can use mode annotations on clauses,
which indicate that particular clauses
should only be used for particular modes.
To specify that a clause only applies to a given mode,
each argument <var>Arg</var> of the clause head
should be annotated with the corresponding argument mode <var>Mode</var>,
using the &lsquo;<samp>::</samp>&rsquo; mode qualification operator,
i.e. &lsquo;<samp><var>Arg</var> :: <var>Mode</var></samp>&rsquo;.
</p>
<p>For example, if &lsquo;<samp>append</samp>&rsquo; was declared as
</p><div class="example">
<pre class="example">:- pred append(list(T), list(T), list(T)).
:- mode append(in, in, out).
:- mode append(out, out, in).
:- mode append(in, out, in).
:- mode append(out, in, in).
</pre></div>

<p>then you could implement it as
</p>
<div class="example">
<pre class="example">append(L1::in,  L2::in,  L3::out) :- usual_append(L1, L2, L3).
append(L1::out, L2::out, L3::in)  :- usual_append(L1, L2, L3).
append(L1::in,  L2::out, L3::in)  :- usual_append(L1, L2, L3).
append(L1::out, L2::in,  L3::in)  :- other_append(L1, L2, L3).

usual_append([], Ys, Ys).
usual_append([X|Xs], Ys, [X|Zs]) :- usual_append(Xs, Ys, Zs).

other_append(Prefix, Suffix, List) :-
    list.length(List, ListLength),
    list.length(Suffix, SuffixLength),
    PrefixLength = ListLength - SuffixLength,
    list.split_list(PrefixLength, List, Prefix, Suffix).
</pre></div>

<p>This language feature can be used to write &ldquo;impure&rdquo; code
that doesn&rsquo;t have any consistent declarative semantics.
For example, you can easily use it
to write something similar to Prolog&rsquo;s (in)famous &lsquo;<samp>var/1</samp>&rsquo; predicate:
</p>
<div class="example">
<pre class="example">:- mode var(in).
:- mode var(free&gt;&gt;free).
var(_::in) :- fail.
var(_::free&gt;&gt;free) :- true.
</pre></div>

<p>As you can see, in this case the two clauses are <em>not</em> equivalent.
</p>
<p>Because of this possibility,
predicates or functions which are defined
using different code for different modes
are by default assumed to be impure;
the programmer must either
(1) carefully ensure that the logical meaning of the clauses
is the same for all modes,
which can be declared to the compiler
through a &lsquo;<samp>pragma promise_equivalent_clauses</samp>&rsquo; declaration,
or a &lsquo;<samp>pragma promise_pure</samp>&rsquo; declaration,
or (2) declare the predicate or function as impure.
See <a href="Impurity.html#Impurity">Impurity</a>.
</p>
<p>In the example with &lsquo;<samp>append</samp>&rsquo; above,
the two ways of implementing append do have the same declarative semantics,
so we can safely use the first approach:
</p>
<div class="example">
<pre class="example">:- pragma promise_equivalent_clauses(append/3).
</pre></div>

<p>The pragma
</p>
<div class="example">
<pre class="example">:- pragma promise_pure(append/3).
</pre></div>

<p>would also promise that the clauses are equivalent,
but on top of that would also promise that the code of each clause is pure.
Sometimes, if some clauses contain impure code,
that is a promise that the programmer wants to make,
but this extra promise is unnecessary in this case.
</p>
<p>In the example with &lsquo;<samp>var/1</samp>&rsquo; above,
the two clauses have different semantics,
so the predicate must be declared as impure:
</p>
<div class="example">
<pre class="example">	:- impure pred var(T).
</pre></div>

<hr>
<div class="header">
<p>
Previous: <a href="Constrained-polymorphic-modes.html#Constrained-polymorphic-modes" accesskey="p" rel="previous">Constrained polymorphic modes</a>, Up: <a href="Modes.html#Modes" accesskey="u" rel="up">Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
