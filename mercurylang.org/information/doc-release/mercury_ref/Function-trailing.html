<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Function trailing</title>

<meta name="description" content="The Mercury Language Reference Manual: Function trailing">
<meta name="keywords" content="The Mercury Language Reference Manual: Function trailing">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Trailing.html#Trailing" rel="up" title="Trailing">
<link href="Delayed-goals-and-floundering.html#Delayed-goals-and-floundering" rel="next" title="Delayed goals and floundering">
<link href="Value-trailing.html#Value-trailing" rel="previous" title="Value trailing">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Function-trailing"></a>
<div class="header">
<p>
Next: <a href="Delayed-goals-and-floundering.html#Delayed-goals-and-floundering" accesskey="n" rel="next">Delayed goals and floundering</a>, Previous: <a href="Value-trailing.html#Value-trailing" accesskey="p" rel="previous">Value trailing</a>, Up: <a href="Trailing.html#Trailing" accesskey="u" rel="up">Trailing</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Function-trailing-1"></a>
<h4 class="subsection">20.5.3 Function trailing</h4>

<p>For more complicated uses of trailing,
you can store the address of a C function on the trail
and have the Mercury runtime call your function back
whenever future execution backtracks to the current choice point or earlier,
or whenever that choice point is pruned,
because execution commits to never backtracking over that point,
or whenever that choice point is garbage collected.
</p>
<p>Note the garbage collector in the current Mercury implementation
does not garbage-collect the trail;
this case is mentioned
only so that we can cater for possible future extensions.
</p>
<dl compact="compact">
<dt><b>&bull; <code>MR_trail_function()</code></b></dt>
<dd><p>Prototype:
</p><div class="example">
<pre class="example">typedef enum {
        MR_undo,
        MR_exception,
        MR_retry,
        MR_commit,
        MR_solve,
        MR_gc
} MR_untrail_reason;

void MR_trail_function(
        void (*<var>untrail_func</var>)(void *, MR_untrail_reason),
        void *<var>value</var>
);
</pre></div>
<p>A call to &lsquo;<samp>MR_trail_function(<var>untrail_func</var>, <var>value</var>)</samp>&rsquo;
adds an entry to the function trail.
The Mercury implementation ensures that
if future execution ever backtracks to the current choicepoint,
or backtracks past the current choicepoint to some earlier choicepoint,
then <code>(*<var>untrail_func</var>)(<var>value</var>, <var>reason</var>)</code> will be called,
where <var>reason</var> will be &lsquo;<samp>MR_undo</samp>&rsquo;
if the backtracking was due to a goal failing,
&lsquo;<samp>MR_exception</samp>&rsquo; if the backtracking was due
to a goal throwing an exception,
or &lsquo;<samp>MR_retry</samp>&rsquo; if the backtracking was due
to the use of the &ldquo;retry&rdquo; command in &lsquo;<samp>mdb</samp>&rsquo;, the Mercury debugger,
or any similar user request in a debugger.
The Mercury implementation also ensures that
if the current choice point is pruned
because execution commits to never backtracking to it,
then <code>(*<var>untrail_func</var>)(<var>value</var>, MR_commit)</code> will be called.
It also ensures that
if execution requires that the current goal be solvable,
then <code>(*<var>untrail_func</var>)(<var>value</var>, MR_solve)</code> will be called.
This happens in calls to <code>solutions/2</code>, for example.
(<code>MR_commit</code> is used for &ldquo;hard&rdquo; commits,
i.e. when we commit to a solution and prune away the alternative solutions;
<code>MR_solve</code> is used for &ldquo;soft&rdquo; commits,
i.e. when we must commit to a solution
but do not prune away all the alternatives.)
</p>
<p>MR_gc is currently not used &mdash;
it is reserved for future use.
</p>
</dd>
</dl>

<p>Typically if the <var>untrail_func</var> is called
with <var>reason</var> being &lsquo;<samp>MR_undo</samp>&rsquo;, &lsquo;<samp>MR_exception</samp>&rsquo;,
or &lsquo;<samp>MR_retry</samp>&rsquo;,
then it should undo the effects of the update(s) specified by <var>value</var>,
and then free any resources associated with that trail entry.
If it is called with <var>reason</var> being &lsquo;<samp>MR_commit</samp>&rsquo; or &lsquo;<samp>MR_solve</samp>&rsquo;,
then it should not undo the update(s);
instead, it may check for floundering (see the next section).
In the &lsquo;<samp>MR_commit</samp>&rsquo; case it may, in some cases, be possible
to also free resources associated with the trail entry.
If it is called with anything else (such as &lsquo;<samp>MR_gc</samp>&rsquo;),
then it should probably abort execution with an error message.
</p>
<p>Note that the address of the C function passed as the first argument of
<code>MR_trail_function()</code> must be word aligned.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Delayed-goals-and-floundering.html#Delayed-goals-and-floundering" accesskey="n" rel="next">Delayed goals and floundering</a>, Previous: <a href="Value-trailing.html#Value-trailing" accesskey="p" rel="previous">Value trailing</a>, Up: <a href="Trailing.html#Trailing" accesskey="u" rel="up">Trailing</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
