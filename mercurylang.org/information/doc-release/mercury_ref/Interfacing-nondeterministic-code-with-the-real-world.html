<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Interfacing nondeterministic code with the real world</title>

<meta name="description" content="The Mercury Language Reference Manual: Interfacing nondeterministic code with the real world">
<meta name="keywords" content="The Mercury Language Reference Manual: Interfacing nondeterministic code with the real world">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Determinism.html#Determinism" rel="up" title="Determinism">
<link href="Committed-choice-nondeterminism.html#Committed-choice-nondeterminism" rel="next" title="Committed choice nondeterminism">
<link href="Replacing-compile_002dtime-checking-with-run_002dtime-checking.html#Replacing-compile_002dtime-checking-with-run_002dtime-checking" rel="previous" title="Replacing compile-time checking with run-time checking">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Interfacing-nondeterministic-code-with-the-real-world"></a>
<div class="header">
<p>
Next: <a href="Committed-choice-nondeterminism.html#Committed-choice-nondeterminism" accesskey="n" rel="next">Committed choice nondeterminism</a>, Previous: <a href="Replacing-compile_002dtime-checking-with-run_002dtime-checking.html#Replacing-compile_002dtime-checking-with-run_002dtime-checking" accesskey="p" rel="previous">Replacing compile-time checking with run-time checking</a>, Up: <a href="Determinism.html#Determinism" accesskey="u" rel="up">Determinism</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Interfacing-nondeterministic-code-with-the-real-world-1"></a>
<h3 class="section">6.4 Interfacing nondeterministic code with the real world</h3>

<p>Normally, attempting to call
a <code>nondet</code> or <code>multi</code> mode of a predicate
from a predicate declared as <code>semidet</code> or <code>det</code>
will cause a determinism error.
So how can we call nondeterministic code from deterministic code?
There are several alternative possibilities.
</p>
<p>If you just want to see if a nondeterministic goal is satisfiable or not,
without needing to know what variable bindings it produces,
then there is no problem -
determinism analysis considers <code>nondet</code> and <code>multi</code> goals
with no non-local output variables to be
<code>semidet</code> and <code>det</code> respectively.
</p>
<p>If you want to use the values of output variables,
then you need to ask yourself
which one of possibly many solutions to a goal do you want?
If you want all of them, you need to one of the predicates
in the standard library module &lsquo;<samp>solutions</samp>&rsquo;,
such as &lsquo;<samp>solutions/2</samp>&rsquo; itself,
which collects all of the solutions to a goal into a list &mdash;
see <a href="Higher_002dorder.html#Higher_002dorder">Higher-order</a>.
</p>
<p>If you just want one solution from a predicate and don&rsquo;t care which,
you should declare the relevant mode of the predicate
to have determinism <code>cc_nondet</code> or <code>cc_multi</code>
(depending on whether you are guaranteed at least one solution or not).
This tells the compiler that this mode of this predicate
may have more than one solution when viewed as a statement in logic,
but the implementation should stop after generating the first solution.
In other words, the implementation should <em>commit</em> to the first solution.
</p>
<p>The commit to the first solution means that
a piece of <code>cc_nondet</code> or <code>cc_multi</code> code
can never be asked to generate a second solution.
If e.g. a <code>cc_nondet</code> call is in a conjunction,
then no later goal in that conjunction (after mode reordering) may fail,
because that would ask the committed choice goal for a second solution.
The compiler enforces this rule.
</p>
<p>In the declarative semantics,
which solution will be the first is unpredictable,
but in the operational semantics,
you <em>can</em> predict which solution will be the first,
since Mercury does depth-first search
with left-to-right execution of clause bodies,
though that is not on the source code form of each clause body,
but on its form <em>after</em> mode analysis reordering
to put the producer of each variable before all its consumers.
</p>
<p>The &lsquo;<samp>committed choice nondeterminism</samp>&rsquo; of a predicate
has to be propagated up the call tree,
making its callers, its callers&rsquo; callers, and so on,
also <code>cc_nondet</code> or <code>cc_multi</code>,
until either you get to <code>main/2</code> at the top of the call tree,
or you get to a location where you don&rsquo;t have to propagate
the committed choice context upward anymore.
</p>
<p>While <code>main/2</code> is usually declared to have determinism <code>det</code>,
it may also be declared <code>cc_multi</code>.
In the latter case,
while the program&rsquo;s declarative semantics may admit more than one solution,
the implementation will stop after the first,
so alternative solutions to <code>main/2</code>
(and hence also to <code>cc_nondet</code> or <code>cc_multi</code> predicates
called directly or indirectly from &lsquo;<samp>main/2</samp>&rsquo;)
are implicitly pruned away.
This is similar to the &ldquo;don&rsquo;t care&rdquo; nondeterminism
of committed choice logic programming languages such as GHC.
</p>
<p>One way to stop propagating committed choice nondeterminism
is the one mentioned above: if a goal has no non-local output variables
(i.e.&nbsp;none of the goal&rsquo;s outputs are visible from outside the goal),
then the goal&rsquo;s solutions are indistinguishable from the outside,
and the implementation will only attempt to satisfy the goal once,
whether or not the goal is committed choice.
Therefore if a <code>cc_multi</code> goal has all its outputs ignored,
then the compiler considers it to be a <code>det</code> goal,
while if a <code>cc_nondet</code> goal has all its outputs ignored,
then the compiler considers it to be a <code>semidet</code> goal.
</p>
<p>The other way to stop propagating committed choice nondeterminism is applicable
when you know that all the solutions returned will be equivalent
in all the ways that your program cares about.
For example, you might want to find the maximum element in a set
by iterating over the elements in the set.
Iterating over the elements in a set in an unspecified order is a
nondeterministic operation,
but no matter which order you iterate over them,
the maximum value in the set should be the same.
</p>
<p>If this condition is satisfied,
i.e.&nbsp;if you know that there will only ever be at most one distinct solution
under your equality theory of the output variables,
then you can use a &lsquo;<samp>promise_equivalent_solutions</samp>&rsquo; determinism cast.
If goal &lsquo;<samp>G</samp>&rsquo; is a <code>cc_multi</code> goal
whose outputs are <code>X</code> and <code>Y</code>, then
<code>promise_equivalent_solutions [X, Y] ( G )</code>
promises the compiler that all solutions of <code>G</code> are equivalent,
so that regardless of which solution of <code>G</code>
the implementation happens to commit to,
the rest of the program will compute
either identical or (similarly) equivalent results.
This allows the compiler to consider
<code>promise_equivalent_solutions [X, Y] ( G )</code>
to have determinism <code>det</code>.
Likewise, the compiler will consider
<code>promise_equivalent_solutions [X, Y] ( G )</code>
where <code>G</code> is <code>cc_nondet</code> to have determinism <code>semidet</code>.
</p>
<p>Note that specifying a user-defined equivalence relation
as the equality predicate for user-defined types
(see <a href="User_002ddefined-equality-and-comparison.html#User_002ddefined-equality-and-comparison">User-defined equality and comparison</a>)
means that &lsquo;<samp>promise_equivalent_solutions</samp>&rsquo;
can be used to express more general forms of equivalence.
For example, if you define a set type which represents sets as unsorted lists,
you would want to define a user-defined equivalence relation for that type,
which could sort the lists before comparing them.
The &lsquo;<samp>promise_equivalent_solutions</samp>&rsquo; determinism cast
could then be used for sets
even though the lists used to represent the sets
might not be in the same order in every solution.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Committed-choice-nondeterminism.html#Committed-choice-nondeterminism" accesskey="n" rel="next">Committed choice nondeterminism</a>, Previous: <a href="Replacing-compile_002dtime-checking-with-run_002dtime-checking.html#Replacing-compile_002dtime-checking-with-run_002dtime-checking" accesskey="p" rel="previous">Replacing compile-time checking with run-time checking</a>, Up: <a href="Determinism.html#Determinism" accesskey="u" rel="up">Determinism</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
