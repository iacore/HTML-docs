<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Tabled evaluation</title>

<meta name="description" content="The Mercury Language Reference Manual: Tabled evaluation">
<meta name="keywords" content="The Mercury Language Reference Manual: Tabled evaluation">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Implementation_002ddependent-extensions.html#Implementation_002ddependent-extensions" rel="up" title="Implementation-dependent extensions">
<link href="Termination-analysis.html#Termination-analysis" rel="next" title="Termination analysis">
<link href="Fact-tables.html#Fact-tables" rel="previous" title="Fact tables">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Tabled-evaluation"></a>
<div class="header">
<p>
Next: <a href="Termination-analysis.html#Termination-analysis" accesskey="n" rel="next">Termination analysis</a>, Previous: <a href="Fact-tables.html#Fact-tables" accesskey="p" rel="previous">Fact tables</a>, Up: <a href="Implementation_002ddependent-extensions.html#Implementation_002ddependent-extensions" accesskey="u" rel="up">Implementation-dependent extensions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Tabled-evaluation-1"></a>
<h3 class="section">20.2 Tabled evaluation</h3>

<p>(Note: &ldquo;Tabled evaluation&rdquo; has no relation
to the &ldquo;fact tables&rdquo; described above.)
</p>
<p>Ordinarily, the results of each procedure call are not recorded;
if the same procedure is called with the same arguments,
then the answer(s) must be recomputed again.
For some procedures, this recomputation can be very wasteful.
</p>
<p>With tabled evaluation, the implementation keeps a table
containing the previously computed results of the specified procedure;
this table is sometimes called the memo table
(since it &ldquo;remembers&rdquo; previous answers).
At each procedure call, the implementation will search the memo table
to check whether the answer(s) have already been computed,
and if so, the answers will be returned directly from the memo table
rather than being recomputed.
This can result in much faster execution,
at the cost of additional space to record answers in the table.
</p>
<p>The implementation can also check at runtime for the situation
where a procedure calls itself recursively with the same arguments,
which would normally result in an infinite loop;
if this situation is encountered, it can (at the programmer&rsquo;s option)
either throw an exception,
or avoid the infinite loop
by computing solutions using a &ldquo;minimal model&rdquo; semantics.
(Specifically, the minimal model computed by our implementation
is the perfect model.)
</p>
<p>When targeting the generation of C code,
the current Mercury implementation supports
three different pragmas for tabling, to cover these three cases:
&lsquo;<samp>loop_check</samp>&rsquo;, &lsquo;<samp>memo</samp>&rsquo;, and &lsquo;<samp>minimal_model</samp>&rsquo;.
(None of these are supported
when targeting the generation of C# or Java code.)
</p>
<ul>
<li> The &lsquo;<samp>loop_check</samp>&rsquo; pragma asks only for loop checking.
With this pragma, the memo table will map each distinct set of input arguments
only to a single boolean saying whether
a call with those arguments is currently active or not;
the pragma&rsquo;s only effect is to cause the predicate to throw an exception
if this boolean says that the current call has the same arguments
as one of its ancestors, which indicates an infinite recursive loop.

<p>Note that loop checking for nondet and multi predicates assumes that
calls to these predicates generate all their solutions and then fail.
If a caller asks them only for some solutions
and then cuts away all later solutions
(e.g. via a quantification
that only asks whether a solution satisfying a particular test exists),
then the cut-away call never gets a chance
to record the fact that it is not longer active.
The next call to that predicate with the same arguments
will therefore think that the previous call is still active,
and will consider this call to be an infinite loop.
</p></li><li> The &lsquo;<samp>memo</samp>&rsquo; pragma asks for both loop checking and memoization.
With this pragma, the memo table will map each distinct set of input arguments
either to the set of results computed previously for those arguments,
or to an indication that the call is still active
and thus those results are still being computed.
This predicate will thus look for infinite recursive loops
(and throw an exception if and when it finds one)
but it will also record all its solutions in the memo table,
and will avoid recomputing solutions
that are already available in the memo table.
</li><li> The &lsquo;<samp>minimal_model</samp>&rsquo; pragma asks
for the computation of a &ldquo;minimal model&rdquo; semantics.
These differ from the &lsquo;<samp>memo</samp>&rsquo; pragma in that
the detection of what appears to be an infinite recursive loop is not fatal.
The implementation will consider
the apparently infinitely recursive calls to fail
if the call concerned has no way of computing
any solutions it has not already computed and recorded,
and if it does have such a way,
then it switches the execution to explore those ways
before coming back to the apparently infinitely recursive call.

<p>Minimal model evaluation is applicable
only to procedures that can succeed more than once,
and only in grades that explicitly support it.
</p></li></ul>

<p>The syntax for each of these declarations is
</p>
<div class="example">
<pre class="example">:- pragma memo(<var>Name</var>/<var>Arity</var>).
:- pragma memo(<var>Name</var>/<var>Arity</var>, [<var>list of tabling attributes</var>]).
:- pragma loop_check(<var>Name</var>/<var>Arity</var>).
:- pragma loop_check(<var>Name</var>/<var>Arity</var>, [<var>list of tabling attributes</var>]).
:- pragma minimal_model(<var>Name</var>/<var>Arity</var>).
:- pragma minimal_model(<var>Name</var>/<var>Arity</var>, [<var>list of tabling attributes</var>]).
</pre></div>

<p>where <var>Name</var>/<var>Arity</var> specifies
the predicate or function to which the declaration applies.
The declaration applies to all modes of the predicate and/or function named.
At most one of these declarations may be specified
for any given predicate or function.
</p>
<p>Programmers can also request the application of tabling
only to one particular mode of a predicate or function,
via declarations such as these:
</p>
<div class="example">
<pre class="example">:- pragma memo(<var>Name</var>(in, in, out)).
:- pragma memo(<var>Name</var>(in, in, out), [<var>list of tabling attributes</var>]).
:- pragma loop_check(<var>Name</var>(in, out)).
:- pragma loop_check(<var>Name</var>(in, out), [<var>list of tabling attributes</var>]).
:- pragma minimal_model(<var>Name</var>(in, in, out, out)).
:- pragma minimal_model(<var>Name</var>(in, in, out, out), [<var>list of tabling attributes</var>]).
</pre></div>

<p>Because all variants of tabling record the values of input arguments,
and all except &lsquo;<samp>loop_check</samp>&rsquo; also record the values of output arguments,
you cannot apply any of these pragmas to procedures
whose arguments&rsquo; modes include any unique component.
</p>
<p>Tabled evaluation of a predicate or function
that has an argument whose type is a foreign type
will result in a run-time error
unless the foreign type is one for which
the &lsquo;<samp>can_pass_as_mercury_type</samp>&rsquo; and &lsquo;<samp>stable</samp>&rsquo; assertions
have been made (see <a href="Using-foreign-types-from-Mercury.html#Using-foreign-types-from-Mercury">Using foreign types from Mercury</a>).
</p>
<p>The optional list of attributes allows programmers
to control some aspects of the management of the memo table(s)
of the procedure(s) affected by the pragma.
</p>
<p>The &lsquo;<samp>allow_reset</samp>&rsquo; attribute asks the compiler
to define a predicate that, when called, resets the memo table.
The name of this predicate will be &ldquo;table_reset_for&rdquo;,
followed by the name of the tabled predicate, followed by its arity,
and (if the predicate has more than one mode) by the mode number
(the first declared mode is mode 0, the second is mode 1, and so on).
These three or four components are separated by underscores.
The reset predicate takes a di/uo pair of I/O states as arguments.
The presence of these I/O state arguments in the reset predicate,
and the fact that tabled predicates cannot have unique arguments
together imply that a memo table cannot be reset
while a call using that memo table is active.
</p>
<p>The &lsquo;<samp>statistics</samp>&rsquo; attribute asks the compiler
to define a predicate that, when called,
returns statistics about the memo table.
The name of this predicate will be &ldquo;table_statistics_for&rdquo;,
followed by the name of the tabled predicate, followed by its arity,
and (if the predicate has more than one mode) by the mode number
(the first declared mode is mode 0, the second is mode 1, and so on).
These three or four components are separated by underscores.
The statistics predicate takes three arguments.
The second and third are a di/uo pair of I/O states,
while the first is an output argument that contains information
about accesses to and modifications of the procedure&rsquo;s memo table,
both since the creation of the table,
and since the last call to this predicate.
The type of this argument is defined in the file table_builtin.m
in the Mercury standard library.
That module also contains a predicate for printing out this information
in a programmer-friendly format.
</p>
<p>As mentioned above, the Mercury compiler implements tabling
only when targeting the generation of C code.
In other grades, the compiler normally generates a warning
for each tabling pragma that it is forced to ignore.
The &lsquo;<samp>disable_warning_if_ignored</samp>&rsquo; attribute tells the compiler
not to generate such a warning for the pragma it is attached to.
Since the &lsquo;<samp>loopcheck</samp>&rsquo; and &lsquo;<samp>minimal_model</samp>&rsquo; pragmas
affect the semantics of the program,
and such changes should not be made silently,
this attribute may not be specified for them.
But this attribute may be specified for &lsquo;<samp>memo</samp>&rsquo; pragmas,
since these affect only the program&rsquo;s performance, not its semantics.
</p>
<p>The remaining two attributes, &lsquo;<samp>fast_loose</samp>&rsquo; and &lsquo;<samp>specified</samp>&rsquo;,
control how arguments are looked up in the memo table.
The default implementation
looks up the <em>value</em> of each input argument,
and thus requires time proportional to
the number of function symbols in the input arguments.
This is the only implementation allowed for minimal model tabling,
but for predicates tabled with the &lsquo;<samp>loop_check</samp>&rsquo; and &lsquo;<samp>memo</samp>&rsquo; pragmas,
programmers can also choose some other tabling methods.
</p>
<p>The &lsquo;<samp>fast_loose</samp>&rsquo; attribute asks the compiler to generate code
that looks up only the <em>address</em> of each input argument in the memo table,
which means that the time required
is linear only in the <em>number</em> of input arguments, not their <em>size</em>.
The tradeoff is that &lsquo;<samp>fast_loose</samp>&rsquo;
does not recognize calls as duplicates
if they involve input arguments that are logically equal
but are stored at different locations in memory.
The following declaration calls for this variant of tabling.
</p>
<div class="example">
<pre class="example">:- pragma memo(<var>Name</var>(in, in, in, out),
        [allow_reset, statistics, fast_loose]).
</pre></div>

<p>The &lsquo;<samp>specified</samp>&rsquo; attribute allows programmers
to choose individually, for each input argument,
whether that argument should be looked up in the memo table
by value or by address,
or whether it should be looked up at all:
</p>
<div class="example">
<pre class="example">:- pragma memo(<var>Name</var>(in, in, in, out), [allow_reset, statistics,
        specified([value, addr, promise_implied, output])]).
</pre></div>

<p>The &lsquo;<samp>specified</samp>&rsquo; attribute should have an argument which is a list,
and this list should contain one element
for each argument of the predicate or function concerned
(if a function, the last element is for the return value).
For output arguments, the list element should be &lsquo;<samp>output</samp>&rsquo;.
For input arguments, the list element may be
&lsquo;<samp>value</samp>&rsquo;, &lsquo;<samp>addr</samp>&rsquo; or &lsquo;<samp>promise_implied</samp>&rsquo;.
The first calls for tabling the argument by value,
the second calls for tabling the argument by address,
and the third calls for not tabling the argument at all.
This last course of action promises that any two calls
that agree on the values of the value-tabled input arguments
and on the addresses of the address-tabled input arguments
will behave the same regardless of the values of the untabled input arguments.
In most cases, this will mean that the values of the untabled arguments
are implied by the values of the value-tabled arguments
and the addresses of the address-tabled arguments,
though the promise can also be fulfilled
if the table predicate or function does not actually use
the untabled argument for computing any of its output.
(It is ok for it to use the untabled argument
to decide what exception to throw.)
</p>

<p>If the tabled predicate or function has only one mode,
then this declaration can also be specified without giving the argument modes:
</p>
<div class="example">
<pre class="example">:- pragma memo(<var>Name</var>/<var>Arity</var>, [allow_reset, statistics,
        specified([value, addr, promise_implied, output])]).
</pre></div>

<p>Note that a &lsquo;<samp>pragma minimal_model</samp>&rsquo; declaration
changes the declarative semantics of the specified predicate or function:
instead of using the completion of the clauses as the basis for the semantics,
as is normally the case in Mercury,
the declarative semantics that is used is a &ldquo;minimal model&rdquo; semantics,
specifically, the perfect model semantics.
Anything which is true or false in the completion semantics
is also true or false (respectively) in the perfect model semantics,
but there are goals for which the perfect model specifies
that the result is true or false,
whereas the completion semantics leaves the result unspecified.
For these goals, the usual Mercury semantics requires the
implementation to either loop or report an error message,
but the perfect model semantics requires a particular answer to be returned.
In particular, the perfect model semantics says that
any call that is not true in <em>all</em> models is false.
</p>
<p>Programmers should therefore use a &lsquo;<samp>pragma minimal_model</samp>&rsquo; declaration
only in cases where their intended interpretation for a procedure
coincides with the perfect model for that procedure.
Fortunately, however, this is usually what programmers intend.
</p>

<p>For more information on tabling, see K. Sagonas&rsquo;s PhD thesis
<cite>The SLG-WAM: A Search-Efficient Engine for Well-Founded Evaluation
of Normal Logic Programs.</cite> See <a href="_005b4_005d.html#g_t_005b4_005d">[4]</a>.
The operational semantics
of procedures with a &lsquo;<samp>pragma minimal_model</samp>&rsquo; declaration
corresponds to what Sagonas calls &ldquo;SLGd resolution&rdquo;.
</p>
<p>In the general case,
the execution mechanism required by minimal model tabling is quite complicated,
requiring the ability to delay goals and then wake them up again.
The Mercury implementation uses a technique
based on copying relevant parts of the stack to the heap when delaying goals.
It is described in
<cite>Tabling in Mercury: design and implementation</cite>
by Z. Somogyi and K. Sagonas,
Proceedings of the Eight International Symposium
on Practical Aspects of Declarative Languages.
</p>
<table class="cartouche" border="1"><tr><td>
<p><strong>Please note:</strong>
the current implementation of tabling does not support
all the possible compilation grades
(see the &ldquo;Compilation model options&rdquo; section of the Mercury User&rsquo;s Guide)
allowed by the Mercury implementation.
In particular, minimal model tabling is incompatible with
high level code and the use of trailing.
</p></td></tr></table>

<hr>
<div class="header">
<p>
Next: <a href="Termination-analysis.html#Termination-analysis" accesskey="n" rel="next">Termination analysis</a>, Previous: <a href="Fact-tables.html#Fact-tables" accesskey="p" rel="previous">Fact tables</a>, Up: <a href="Implementation_002ddependent-extensions.html#Implementation_002ddependent-extensions" accesskey="u" rel="up">Implementation-dependent extensions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
