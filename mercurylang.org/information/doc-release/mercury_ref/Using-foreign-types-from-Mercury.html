<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Using foreign types from Mercury</title>

<meta name="description" content="The Mercury Language Reference Manual: Using foreign types from Mercury">
<meta name="keywords" content="The Mercury Language Reference Manual: Using foreign types from Mercury">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Foreign-language-interface.html#Foreign-language-interface" rel="up" title="Foreign language interface">
<link href="Using-foreign-enumerations-in-Mercury-code.html#Using-foreign-enumerations-in-Mercury-code" rel="next" title="Using foreign enumerations in Mercury code">
<link href="Java-data-passing-conventions.html#Java-data-passing-conventions" rel="previous" title="Java data passing conventions">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Using-foreign-types-from-Mercury"></a>
<div class="header">
<p>
Next: <a href="Using-foreign-enumerations-in-Mercury-code.html#Using-foreign-enumerations-in-Mercury-code" accesskey="n" rel="next">Using foreign enumerations in Mercury code</a>, Previous: <a href="Data-passing-conventions.html#Data-passing-conventions" accesskey="p" rel="previous">Data passing conventions</a>, Up: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="u" rel="up">Foreign language interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Using-foreign-types-from-Mercury-1"></a>
<h3 class="section">15.4 Using foreign types from Mercury</h3>

<p>Types defined in a foreign language can be accessed in Mercury
using a declaration of the form
</p>
<div class="example">
<pre class="example">:- pragma foreign_type(<var>Lang</var>, <var>MercuryTypeName</var>, <var>ForeignTypeDescriptor</var>).
</pre></div>

<p>This defines <var>MercuryTypeName</var>
as a synonym for type <var>ForeignTypeDescriptor</var>
defined in the foreign language <var>Lang</var>.
<var>MercuryTypeName</var> must be the name of
either an abstract type or a discriminated union type.
In both cases,
<var>MercuryTypeName</var> must be declared with &lsquo;<samp>:- type</samp>&rsquo; as usual.
The &lsquo;<samp>pragma foreign_type</samp>&rsquo; must not have wider visibility
than the type declaration
(if the &lsquo;<samp>pragma foreign_type</samp>&rsquo; declaration is in the interface,
the &lsquo;<samp>:- type</samp>&rsquo; declaration must be also).
</p>
<p>If <var>MercuryTypeName</var> names a discriminated union type,
that type cannot be the base type of any subtypes,
nor can it be a subtype itself (see <a href="Subtypes.html#Subtypes">Subtypes</a>).
</p>
<p><var>ForeignTypeDescriptor</var> defines
how the Mercury type is mapped for a particular foreign language.
Specific syntax is given in the language specific information below.
</p>
<p><var>MercuryTypeName</var> is treated as an abstract type
at all times in Mercury code.
However, if <var>MercuryTypeName</var>
is one of the parameters of a foreign_proc for <var>Lang</var>,
and the &lsquo;<samp>pragma foreign_type</samp>&rsquo; declaration is visible to the foreign_proc,
it will be passed to that foreign_proc
as specified by <var>ForeignTypeDescriptor</var>.
</p>
<p>The same type may have a foreign language definition
for more than one foreign language.
The definition used in the generated code
will be the one for the foreign language
that is most appropriate for the target language of the compilation
(see the language specific information below for details).
All the foreign language definitions must have the same visibility.
</p>
<p>A type which has one or more foreign language definitions
may also have a Mercury definition,
which must define a discriminated union type.
The constructors for this Mercury type will only be visible in Mercury clauses
for predicates or functions with &lsquo;<samp>pragma foreign_proc</samp>&rsquo; clauses
for all of the languages for which there are
&lsquo;<samp>foreign_type</samp>&rsquo; declarations for the type.
</p>
<p>You can also associate assertions about the properties of the foreign type
with the &lsquo;<samp>foreign_type</samp>&rsquo; declaration, using the following syntax:
</p>
<div class="example">
<pre class="example">:- pragma foreign_type(<var>Lang</var>, <var>MercuryTypeName</var>, <var>ForeignTypeDescriptor</var>,
    [<var>ForeignTypeAssertion</var>, &hellip;]).
</pre></div>

<p>Currently, three kinds of assertions are supported.
</p>
<p>The &lsquo;<samp>can_pass_as_mercury_type</samp>&rsquo; assertion
states that on the C backends, values of the given type
can be passed to and from Mercury code without boxing,
via simple casts, which is faster.
This requires the type to be either an integer type or a pointer type,
and requires it to be castable to &lsquo;<samp>MR_Word</samp>&rsquo; and back
without loss of information
(which means that its size may not be greater than the size of &lsquo;<samp>MR_Word</samp>&rsquo;).
</p>
<p>The &lsquo;<samp>word_aligned_pointer</samp>&rsquo; assertion implies
&lsquo;<samp>can_pass_as_mercury_type</samp>&rsquo; and additionally states that values of the
given type are pointer values clear in the tag bits.
It allows the Mercury implementation to avoid boxing values of the given type
when the type appears as the sole argument of a data constructor.
</p>
<p>The &lsquo;<samp>stable</samp>&rsquo; assertion is meaningful
only in the presence of the &lsquo;<samp>can_pass_as_mercury_type</samp>&rsquo;
or &lsquo;<samp>word_aligned_pointer</samp>&rsquo; assertions.
It states that either the C type is an integer type,
or it is a pointer type pointing to memory that will never change.
Together, these assertions are sufficient to allow
tabling (see <a href="Tabled-evaluation.html#Tabled-evaluation">Tabled evaluation</a>)
and the &lsquo;<samp>compare_representation</samp>&rsquo; primitive
to work on values of such types.
</p>
<p>Violations of any of these assertions are very likely to result
in the generated executable silently doing the wrong thing,
giving no clue to where the problem might be.
Since deciding whether a C type satisfies the conditions of these assertions
requires knowledge of the internals of the Mercury implementation,
we do not recommend the use of any of these assertions
unless you are confident of your expertise in those internals.
</p>
<p>As with discriminated union types,
programmers can specify the unification and/or<!-- /@w --> comparison predicates
to use for values of the type using the following syntax
(see <a href="User_002ddefined-equality-and-comparison.html#User_002ddefined-equality-and-comparison">User-defined equality and comparison</a>):
</p>
<div class="example">
<pre class="example">:- pragma foreign_type(<var>Lang</var>, <var>MercuryTypeName</var>, <var>ForeignTypeDescriptor</var>)
        where equality is <var>EqualityPred</var>, comparison is <var>ComparePred</var>.
</pre></div>

<p>You can use Mercury foreign language interfacing declarations
which specify language <var>X</var> to interface to types
that are actually written in a different language <var>Y</var>,
provided that <var>X</var> and <var>Y</var> have compatible interface conventions.
Support for this kind of compatibility
is described in the language specific information below.
</p>

<hr>
<div class="header">
<p>
Next: <a href="Using-foreign-enumerations-in-Mercury-code.html#Using-foreign-enumerations-in-Mercury-code" accesskey="n" rel="next">Using foreign enumerations in Mercury code</a>, Previous: <a href="Data-passing-conventions.html#Data-passing-conventions" accesskey="p" rel="previous">Data passing conventions</a>, Up: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="u" rel="up">Foreign language interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
