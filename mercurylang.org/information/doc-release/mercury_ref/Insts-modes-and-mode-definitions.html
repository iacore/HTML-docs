<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Insts modes and mode definitions</title>

<meta name="description" content="The Mercury Language Reference Manual: Insts modes and mode definitions">
<meta name="keywords" content="The Mercury Language Reference Manual: Insts modes and mode definitions">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Modes.html#Modes" rel="up" title="Modes">
<link href="Predicate-and-function-mode-declarations.html#Predicate-and-function-mode-declarations" rel="next" title="Predicate and function mode declarations">
<link href="Modes.html#Modes" rel="previous" title="Modes">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Insts-modes-and-mode-definitions"></a>
<div class="header">
<p>
Next: <a href="Predicate-and-function-mode-declarations.html#Predicate-and-function-mode-declarations" accesskey="n" rel="next">Predicate and function mode declarations</a>, Up: <a href="Modes.html#Modes" accesskey="u" rel="up">Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Insts_002c-modes_002c-and-mode-definitions"></a>
<h3 class="section">4.1 Insts, modes, and mode definitions</h3>

<p>The <em>mode</em> of a predicate, or function, is a mapping
from the initial state of instantiation of the arguments of the predicate,
or the arguments and result of a function,
to their final state of instantiation.
To describe states of instantiation,
we use information provided by the type system.
Types can be viewed as regular trees with two kinds of nodes:
or-nodes representing types
and and-nodes representing constructors.
The children of an or-node are the constructors
that can be used to construct terms of that type;
the children of an and-node are the types
of the arguments of the constructors.
We attach mode information to the or-nodes of type trees.
</p>
<p>An <em>instantiatedness tree</em>
is an assignment of an <em>instantiatedness</em>
&mdash; either <em>free</em> or <em>bound</em> &mdash;
to each or-node of a type tree,
with the constraint that all descendants of a free node must be free.
</p>
<p>A term is <em>approximated by</em> an instantiatedness tree
if for every node in the instantiatedness tree,
</p>
<ul>
<li> if the node is &ldquo;free&rdquo;,
then the corresponding node in the term (if any)
is a free variable that does not share with any other variable
(we call such variables <em>distinct</em>);

</li><li> if the node is &ldquo;bound&rdquo;,
then the corresponding node in the term (if any)
is a function symbol.

</li></ul>

<p>When an instantiatedness tree tells us that a variable is bound,
there may be several alternative function symbols to which it could be bound.
The instantiatedness tree does not tell us which of these it is bound to;
instead for each possible function symbol it tells us exactly
which arguments of the function symbol will be free and which will be bound.
The same principle applies recursively to these bound arguments.
</p>
<p>Mercury&rsquo;s mode system allows users
to declare names for instantiatedness trees using declarations such as
</p>
<div class="example">
<pre class="example">:- inst listskel == bound([] ; [free | listskel]).
</pre></div>

<p>This instantiatedness tree describes lists
whose skeleton is known but whose elements are distinct variables.
As such, it approximates the term <code>[A,B]</code>
but not the term <code>[H|T]</code> (only part of the skeleton is known),
the term <code>[A,2]</code> (not all elements are variables),
or the term <code>[A,A]</code> (the elements are not distinct variables).
</p>
<p>As a shorthand, the mode system provides <code>free</code> and <code>ground</code>
as names for instantiatedness trees
all of whose nodes are free and bound respectively
(with the exception of solver type values
which may be semantically ground,
but be defined in terms of non-ground solver type values;
see <a href="Solver-types.html#Solver-types">Solver types</a> for more detail).
The shape of these trees is determined
by the type of the variable to which they apply.
</p>
<p>A more concise, alternative syntax exists
for <code>bound</code> instantiatedness trees:
</p>
<div class="example">
<pre class="example">:- inst maybeskel
    ---&gt;    no
    ;       yes(free).
</pre></div>

<p>which is equivalent to writing
</p>
<div class="example">
<pre class="example">:- inst maybeskel == bound(no ; yes(free)).
</pre></div>

<p>You can specify what type (actually what type constructor)
an inst is intended to be used on
by adding <code>for</code>, followed by the name and arity of that type constructor,
after the name of the inst, like this:
</p><div class="example">
<pre class="example">:- inst maybeskel for maybe/1
    ---&gt;    no
    ;       yes(free).
</pre></div>

<p>This can be useful documentation,
and the compiler will generate an error message
when an inst that was declared to be for values of one type constructor
is applied to values of another type constructor.
</p>
<p>As execution proceeds, variables may become more instantiated.
A <em>mode mapping</em> is a mapping
from an initial instantiatedness tree to a final instantiatedness tree,
with the constraint that no node of the type tree
is transformed from bound to free.
Mercury allows the user to specify mode mappings directly
by expressions such as <code>inst1 &gt;&gt; inst2</code>,
or to give them a name using declarations such as
</p>
<div class="example">
<pre class="example">:- mode m == inst1 &gt;&gt; inst2.
</pre></div>

<p>Two standard shorthand modes are provided,
corresponding to the standard notions of inputs and outputs:
</p>
<div class="example">
<pre class="example">:- mode in == ground &gt;&gt; ground.
:- mode out == free &gt;&gt; ground.
</pre></div>

<p>Though we do not recommend this,
Prolog fans who want to use the symbols &lsquo;<samp>+</samp>&rsquo; and &lsquo;<samp>-</samp>&rsquo;
can do so by simply defining them using a mode declaration:
</p>
<div class="example">
<pre class="example">:- mode (+) == in.
:- mode (-) == out.
</pre></div>

<p>These two modes are enough for most functions and predicates.
Nevertheless, Mercury&rsquo;s mode system is sufficiently
expressive to handle more complex data-flow patterns,
including those involving partially instantiated data structures,
i.e. terms that contain both function symbols and free variables,
such as &lsquo;<samp>f(a, b, X)</samp>&rsquo;.
In the current implementation,
partially instantiated data structures are unsupported
due to a lack of alias tracking in the mode system.
For more information,
please see the <samp>LIMITATIONS.md</samp> file distributed with Mercury.
</p>
<p>For example, consider an interface to a database
that associates data with keys,
and provides read and write access to the items it stores.
To represent accesses to the database over a network,
you would need declarations such as
</p>
<div class="example">
<pre class="example">:- type operation
     ---&gt;   lookup(key, data)
     ;      set(key, data).
:- inst request for operation/0
    ---&gt;    lookup(ground, free)
    ;       set(ground, ground).
:- mode create_request == free &gt;&gt; request.
:- mode satisfy_request == request &gt;&gt; ground.
</pre></div>

<p>&lsquo;<samp>inst</samp>&rsquo; and &lsquo;<samp>mode</samp>&rsquo; declarations can be parametric.
For example, the following declaration
</p>
<div class="example">
<pre class="example">:- inst listskel(Inst) for list/1
    ---&gt;    []
    ;       [Inst | listskel(Inst)].
</pre></div>

<p>defines the inst &lsquo;<samp>listskel(Inst)</samp>&rsquo; to be a list skeleton
whose elements have inst <code>Inst</code>;
you can then use insts such as &lsquo;<samp>listskel(listskel(free))</samp>&rsquo;,
which represents the instantiation state of a list of lists of free variables.
The standard library provides the parametric modes
</p>
<div class="example">
<pre class="example">:- mode in(Inst) == Inst &gt;&gt; Inst.
:- mode out(Inst) == free &gt;&gt; Inst.
</pre></div>

<p>so that for example the mode &lsquo;<samp>create_request</samp>&rsquo; defined above
could have be defined as
</p>
<div class="example">
<pre class="example">:- mode create_request == out(request).
</pre></div>

<p>There must not be more than one inst definition
with the same name and arity in the same module.
Similarly, there must not be more than one mode definition
with the same name and arity in the same module.
</p>
<p>Note that user defined insts and modes may not have names
that have meanings in Mercury.
(Most of these are documented in later sections.)
</p>
<p>The list of reserved inst names is
</p><div class="example">
<pre class="example">=&lt;
any
bound
bound_unique
clobbered
clobbered_any
free
ground
is
mostly_clobbered
mostly_unique
mostly_unique_any
not_reached
unique
unique_any
</pre></div>

<p>The list of reserved mode names is
</p><div class="example">
<pre class="example">=
&gt;&gt;
any_func
any_pred
func
is
pred
</pre></div>

<hr>
<div class="header">
<p>
Next: <a href="Predicate-and-function-mode-declarations.html#Predicate-and-function-mode-declarations" accesskey="n" rel="next">Predicate and function mode declarations</a>, Up: <a href="Modes.html#Modes" accesskey="u" rel="up">Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
