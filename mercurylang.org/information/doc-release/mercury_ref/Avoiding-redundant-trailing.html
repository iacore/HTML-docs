<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Avoiding redundant trailing</title>

<meta name="description" content="The Mercury Language Reference Manual: Avoiding redundant trailing">
<meta name="keywords" content="The Mercury Language Reference Manual: Avoiding redundant trailing">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Trailing.html#Trailing" rel="up" title="Trailing">
<link href="Bibliography.html#Bibliography" rel="next" title="Bibliography">
<link href="Delayed-goals-and-floundering.html#Delayed-goals-and-floundering" rel="previous" title="Delayed goals and floundering">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Avoiding-redundant-trailing"></a>
<div class="header">
<p>
Previous: <a href="Delayed-goals-and-floundering.html#Delayed-goals-and-floundering" accesskey="p" rel="previous">Delayed goals and floundering</a>, Up: <a href="Trailing.html#Trailing" accesskey="u" rel="up">Trailing</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Avoiding-redundant-trailing-1"></a>
<h4 class="subsection">20.5.5 Avoiding redundant trailing</h4>

<p>If a mutable data structure is updated multiple times,
and each update is recorded on the trail using the functions described above,
then some of this trailing may be redundant.
It is generally not necessary to record enough information
to recover the original state of the data structure
for <em>every</em> update on the trail;
instead, it is enough to record
the original state of each updated data structure just once
for each choice point occurring after the data structure is allocated,
rather than once for each update.
</p>
<p>The functions described below provide a means to avoid redundant trailing.
</p>
<dl compact="compact">
<dt><b>&bull; <code>MR_ChoicepointId</code></b></dt>
<dd><p>Declaration:
</p><div class="example">
<pre class="example">typedef &hellip; MR_ChoicepointId;
</pre></div>

<p>The type <code>MR_ChoicepointId</code> is an abstract type
used to hold the identity of a choice point.
Values of this type can be compared
using C&rsquo;s &lsquo;<samp>==</samp>&rsquo; operator or using &lsquo;<samp>MR_choicepoint_newer()</samp>&rsquo;.
</p><br>
</dd>
<dt><b>&bull; <code>MR_current_choicepoint_id()</code></b></dt>
<dd><p>Prototype:
</p><div class="example">
<pre class="example">MR_ChoicepointId MR_current_choicepoint_id(void);
</pre></div>

<p><code>MR_current_choicepoint_id()</code> returns a value
indicating the identity of the most recent choice point;
that is, the point to which execution would backtrack
if the current computation failed.
The value remains meaningful if the choicepoint is pruned away by a commit,
but is not meaningful
after backtracking past the point where the choicepoint was created
(since choicepoint ids may be reused after backtracking).
</p><br>
</dd>
<dt><b>&bull; <code>MR_null_choicepoint_id()</code></b></dt>
<dd><p>Prototype:
</p><div class="example">
<pre class="example">MR_ChoicepointId MR_null_choicepoint_id(void);
</pre></div>

<p><code>MR_null_choicepoint_id()</code> returns a &ldquo;null&rdquo; value that is distinct
from any value ever returned by <code>MR_current_choicepoint_id</code>.
(Note that <code>MR_null_choicepoint_id()</code>
is a macro that is guaranteed to be suitable for use as a static initializer,
so that it can for example be used
to provide the initial value of a C global variable.)
</p><br>
</dd>
<dt><b>&bull; <code>MR_choicepoint_newer()</code></b></dt>
<dd><p>Prototype:
</p><div class="example">
<pre class="example">bool MR_choicepoint_newer(MR_ChoicepointId, MR_ChoicepointId);
</pre></div>

<p><code>MR_choicepoint_newer(<var>x</var>, <var>y</var>)</code> true
iff the choicepoint indicated by <var>x</var>
is newer than (i.e. was created more recently than)
the choicepoint indicated by <var>y</var>.
The null ChoicepointId is considered older than any non-null ChoicepointId.
If either of the choice points have been backtracked over,
the behaviour is undefined.
</p>
</dd>
</dl>

<p>The way these functions are generally used is as follows.
When you create a mutable data structure,
you should call <code>MR_current_choicepoint_id()</code>
and save the value it returns
as a &lsquo;<samp>prev_choicepoint</samp>&rsquo; field in your data structure.
When you are about to modify your mutable data structure,
you can then call <code>MR_current_choicepoint_id()</code> again
and compare the result from that call
with the value saved in the &lsquo;<samp>prev_choicepoint</samp>&rsquo; field in the data structure
using <code>MR_choicepoint_newer()</code>.
If the current choicepoint is newer, then you must trail the update,
and update the &lsquo;<samp>prev_choicepoint</samp>&rsquo; field with the new value;
furthermore, you must also take care that on backtracking the
previous value of the &lsquo;<samp>prev_choicepoint</samp>&rsquo; field in your data
structure is restored to its previous value, by trailing that update too.
But if <code>MR_current_choice_id()</code>
is not newer than the <code>prev_choicepoint</code> field,
then you can safely perform the update to your data structure
without trailing it.
</p>
<p>If your mutable data structure is a C global variable,
then you can use <code>MR_null_choicepoint_id()</code>
for the initial value of the &lsquo;<samp>prev_choicepoint</samp>&rsquo; field.
If on the other hand your mutable data structure
is created by a predicate or function that uses tabled evaluation
(see <a href="Tabled-evaluation.html#Tabled-evaluation">Tabled evaluation</a>),
then you <em>should</em> use <code>MR_null_choicepoint_id()</code>
for the initial value of the field.
Doing so will ensure that the data will be reset to its initial value
if execution backtracks to a point
before the mutable data structure was created,
which is important because this copy of the mutable data structure
will be tabled and will therefore be produced again
if later execution attempts to create another instance of it.
</p>
<p>For an example of avoiding redundant trailing, see the sample module below.
</p>
<p>Note that there is a cost to this &mdash;
you have to include an extra field in your data structure
for each part of the data structure which you might update,
you need to perform a test for each update to decide
whether or not to trail it,
and if you do need to trail the update,
then you have an extra field that you need to trail.
Whether or not the benefits from avoiding redundant trailing
outweigh these costs will depend on your application.
</p>
<div class="example">
<pre class="example">:- module trailing_example.
:- interface.

:- type int_ref.

    % Create a new int_ref with the specified value.
    %
:- pred new_int_ref(int_ref::uo, int::in) is det.

    % update_int_ref(Ref0, Ref, OldVal, NewVal).
    % Ref0 has value OldVal and Ref has value NewVal.
    %
:- pred update_int_ref(int_ref::mdi, int_ref::muo, int::out, int::in)
    is det.

:- implementation.

:- pragma foreign_decl(&quot;C&quot;, &quot;

typedef struct {
    MR_ChoicepointId prev_choicepoint;
    MR_Integer data;
} C_IntRef;

&quot;).

:- pragma foreign_type(&quot;C&quot;, int_ref, &quot;C_IntRef *&quot;).

:- pragma foreign_proc(&quot;C&quot;,
    new_int_ref(Ref::uo, Value::in),
    [will_not_call_mercury, promise_pure],
&quot;
    C_Intref *x = malloc(sizeof(C_IntRef));
    x-&gt;prev_choicepoint = MR_current_choicepoint_id();
    x-&gt;data = Value;
    Ref = x;
&quot;).

:- pragma foreign_proc(&quot;C&quot;,
    update_int_ref(Ref0::mdi, Ref::muo, OldValue::out, NewValue::in),
    [will_not_call_mercury, promise_pure],
&quot;
    C_IntRef *x = Ref0;
    OldValue = x-&gt;data;

    /* Check whether we need to trail this update. */
    if (MR_choicepoint_newer(MR_current_choicepoint_id(),
        x-&gt;prev_choicepoint))
    {
        /*
        ** Trail both x-&gt;data and x-&gt;prev_choicepoint,
        ** since we're about to update them both.
        */
        assert(sizeof(x-&gt;data) == sizeof(MR_Word));
        assert(sizeof(x-&gt;prev_choicepoint) == sizeof(MR_Word));
        MR_trail_current_value((MR_Word *)&amp;x-&gt;data);
        MR_trail_current_value((MR_Word *)&amp;x-&gt;prev_choicepoint);

        /*
        ** Update x-&gt;prev_choicepoint to indicate that
        ** x-&gt;data's previous value has been trailed
        ** at this choice point.
        */
        x-&gt;prev_choicepoint = MR_current_choicepoint_id();
    }
    x-&gt;data = NewValue;
    Ref = Ref0;
&quot;).

</pre></div>


<hr>
<div class="header">
<p>
Previous: <a href="Delayed-goals-and-floundering.html#Delayed-goals-and-floundering" accesskey="p" rel="previous">Delayed goals and floundering</a>, Up: <a href="Trailing.html#Trailing" accesskey="u" rel="up">Trailing</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
