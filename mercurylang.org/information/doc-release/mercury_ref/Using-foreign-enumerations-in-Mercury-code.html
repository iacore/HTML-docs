<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Using foreign enumerations in Mercury code</title>

<meta name="description" content="The Mercury Language Reference Manual: Using foreign enumerations in Mercury code">
<meta name="keywords" content="The Mercury Language Reference Manual: Using foreign enumerations in Mercury code">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Foreign-language-interface.html#Foreign-language-interface" rel="up" title="Foreign language interface">
<link href="Using-Mercury-enumerations-in-foreign-code.html#Using-Mercury-enumerations-in-foreign-code" rel="next" title="Using Mercury enumerations in foreign code">
<link href="Using-foreign-types-from-Mercury.html#Using-foreign-types-from-Mercury" rel="previous" title="Using foreign types from Mercury">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Using-foreign-enumerations-in-Mercury-code"></a>
<div class="header">
<p>
Next: <a href="Using-Mercury-enumerations-in-foreign-code.html#Using-Mercury-enumerations-in-foreign-code" accesskey="n" rel="next">Using Mercury enumerations in foreign code</a>, Previous: <a href="Using-foreign-types-from-Mercury.html#Using-foreign-types-from-Mercury" accesskey="p" rel="previous">Using foreign types from Mercury</a>, Up: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="u" rel="up">Foreign language interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Using-foreign-enumerations-in-Mercury-code-1"></a>
<h3 class="section">15.5 Using foreign enumerations in Mercury code</h3>

<p>While a &lsquo;<samp>pragma foreign_type</samp>&rsquo; declaration
imports a foreign <em>type</em> into Mercury,
a &lsquo;<samp>pragma foreign_enum</samp>&rsquo; declaration
imports <em>the values of the constants of an enumeration type</em> into Mercury.
</p>
<p>While languages such as C have special syntax for defining enumeration types,
in Mercury, an enumeration type is simply an ordinary discriminated union type
whose function symbols all have arity zero.
</p>
<p>Given an enumeration type such as
</p><div class="example">
<pre class="example">:- type unix_file_permissions
    ---&gt;    user_read
    ;       user_write
    ;       user_executable
    ;       group_read
    ;       group_write
    ;       group_executable
    ;       other_read
    ;       other_write
    ;       other_executable.
</pre></div>

<p>the values used to represent each constant
are usually decided by the Mercury compiler.
However, the values assigned this way
may not match the values expected by foreign language code
that uses values of the enumeration,
and even if they happen to match,
programmers probably would not want to <em>rely</em> on this coincidence.
</p>
<p>This is why Mercury supports a mechanism that allows programmers
to specify the representation of each constant in an enumeration type
when generating code for a given target language.
This mechanism is the &lsquo;<samp>pragma foreign_enum</samp>&rsquo; declaration,
which looks like this:
</p>
<div class="example">
<pre class="example">:- pragma foreign_enum(&quot;C&quot;, unix_file_permissions/0,
[
    user_read        - &quot;S_IRUSR&quot;,
    user_write       - &quot;S_IWUSR&quot;,
    user_executable  - &quot;S_IXUSR&quot;,
    group_read       - &quot;S_IRGRP&quot;,
    group_write      - &quot;S_IWGRP&quot;,
    group_executable - &quot;S_IXGRP&quot;,
    other_read       - &quot;S_IROTH&quot;,
    other_write      - &quot;S_IWOTH&quot;,
    other_executable - &quot;S_IXOTH&quot;
]).
</pre></div>

<p>(Unix systems have a standard header file
that defines each of &lsquo;<samp>S_IRUSR</samp>&rsquo;, &hellip;, &lsquo;<samp>S_IXOTH</samp>&rsquo;
as macros that each expand to an integer constant;
these constants happen <em>not</em> to be the ones
that the Mercury compiler would assign to those constants.)
</p>
<p>The general form of &lsquo;<samp>pragma foreign_enum</samp>&rsquo; declarations is
</p>
<div class="example">
<pre class="example">:- pragma foreign_enum(&quot;<var>Lang</var>&quot;, <var>MercuryType</var>, <var>CtorValues</var>).
</pre></div>

<p>where <var>CtorValues</var> is a list of pairs of the form:
</p>
<div class="example">
<pre class="example">[
    ctor_0 - &quot;ForeignValue_0&quot;,
    ctor_1 - &quot;ForeignValue_1&quot;,
    &hellip;
    ctor_N - &quot;ForeignValue_N&quot;
]
</pre></div>

<p>The first element of each pair
is a constant (function symbol of arity 0) of the type <var>MercuryType</var>,
and the second is either a numeric or a symbolic name
for the integer value in the language <var>Lang</var>
that the programmer wants to be used to represent that constructor.
</p>
<p>The mapping defined by this list of pairs must form a bijection,
i.e. the list must map distinct constructors to distinct values,
and vice versa.
The Mercury compiler is not required to check this, because it cannot;
even if two symbolic names (such as C macros) are distinct,
they may expand to the same integer in the target language.
</p>
<p>Mercury implementations may impose
further foreign-language-specific restrictions
on the form that values used to represent enumeration constructors may take.
See the language specific information below for details.
</p>
<p>It is an error for any given <var>MercuryType</var>
to be the subject of more than one &lsquo;<samp>pragma foreign_enum</samp>&rsquo; declaration
for any given foreign language,
since that would amount to an attempt
to specify two or more (probably) conflicting representations
for each of the type&rsquo;s function symbols.
</p>

<p>A &lsquo;<samp>pragma foreign_enum</samp>&rsquo; declaration must occur in the implementation
section of the module that defines the type <var>MercuryType</var>.
Because of this, the names of the constants
need not and must not be module qualified.
</p>
<p>Note that the default comparison for types
that are the subject of a &lsquo;<samp>pragma foreign_enum</samp>&rsquo; declaration
will be defined by the foreign values,
rather than the order of the constructors in the type declaration
(as would otherwise be the case).
</p>

<hr>
<div class="header">
<p>
Next: <a href="Using-Mercury-enumerations-in-foreign-code.html#Using-Mercury-enumerations-in-foreign-code" accesskey="n" rel="next">Using Mercury enumerations in foreign code</a>, Previous: <a href="Using-foreign-types-from-Mercury.html#Using-foreign-types-from-Mercury" accesskey="p" rel="previous">Using foreign types from Mercury</a>, Up: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="u" rel="up">Foreign language interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
