<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: User-defined equality and comparison</title>

<meta name="description" content="The Mercury Language Reference Manual: User-defined equality and comparison">
<meta name="keywords" content="The Mercury Language Reference Manual: User-defined equality and comparison">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html#Top" rel="up" title="Top">
<link href="Higher_002dorder.html#Higher_002dorder" rel="next" title="Higher-order">
<link href="Committed-choice-nondeterminism.html#Committed-choice-nondeterminism" rel="previous" title="Committed choice nondeterminism">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="User_002ddefined-equality-and-comparison"></a>
<div class="header">
<p>
Next: <a href="Higher_002dorder.html#Higher_002dorder" accesskey="n" rel="next">Higher-order</a>, Previous: <a href="Determinism.html#Determinism" accesskey="p" rel="previous">Determinism</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="User_002ddefined-equality-and-comparison-1"></a>
<h2 class="chapter">7 User-defined equality and comparison</h2>

<p>When defining abstract data types,
often it is convenient to use a non-canonical representation &mdash;
that is, one for which a single abstract value
may have more than one different possible concrete representation.
For example, you may wish to implement an abstract type &lsquo;<samp>set</samp>&rsquo;
by representing a set as an (unsorted) list.
</p>
<div class="example">
<pre class="example">:- module set_as_unsorted_list.
:- interface.
:- type set(T).

:- implementation.
:- import_module list.
:- type set(T)
    ---&gt;    set(list(T)).
</pre></div>

<p>In this example,
the concrete representations &lsquo;<samp>set([1,2])</samp>&rsquo; and &lsquo;<samp>set([2,1])</samp>&rsquo;
would both represent the same abstract value,
namely the set containing the elements 1 and 2.
</p>
<p>For types such as this, which do not have a canonical representation,
the standard definition of equality is not the desired one;
we want equality on sets to mean equality of the abstract values,
not equality of their representations.
To support such types, Mercury allows programmers to specify
a user-defined equality predicate for user-defined types
(not including subtypes):
</p>
<div class="example">
<pre class="example">:- type set(T)
    ---&gt;    set(list(T))
            where equality is set_equals.
</pre></div>

<p>Here &lsquo;<samp>set_equals</samp>&rsquo; is the name of a user-defined predicate
that is used for equality on the type &lsquo;<samp>set(T)</samp>&rsquo;.
It could for example be defined in terms of a &lsquo;<samp>subset</samp>&rsquo; predicate.
</p>
<div class="example">
<pre class="example">:- pred set_equals(set(T)::in, set(T)::in) is semidet.
set_equals(S1, S2) :-
    subset(S1, S2),
    subset(S2, S1).
</pre></div>

<p>A comparison predicate can also be supplied.
</p>
<div class="example">
<pre class="example">:- type set(T)
    ---&gt;    set(list(T))
            where equality is set_equals, comparison is set_compare.

:- pred set_compare(builtin.comparison_result::uo,
    set(T)::in, set(T)::in) is det.

set_compare(Result, Set1, Set2) :-
    promise_equivalent_solutions [Result] (
        set_compare_2(Set1, Set2, Result)
    ).

:- pred set_compare_2(set(T)::in, set(T)::in,
    builtin.comparison_result::uo) is cc_multi.

set_compare_2(set(List1), set(List2), Result) :-
    builtin.compare(Result, list.sort(List1), list.sort(List2)).
</pre></div>

<p>If a comparison predicate is supplied
and the unification predicate is omitted,
a unification predicate is generated by the compiler
in terms of the comparison predicate.
For the &lsquo;<samp>set</samp>&rsquo; example, the generated predicate would be:
</p>
<div class="example">
<pre class="example">set_equals(S1, S2) :-
    set_compare((=), S1, S2).
</pre></div>

<p>If a unification predicate is supplied without a comparison predicate,
the compiler will generate a comparison predicate
which throws an exception of type &lsquo;<samp>exception.software_error</samp>&rsquo; when called.
</p>
<p>A type declaration for a type &lsquo;<samp>foo(T1, &hellip;, TN)</samp>&rsquo;
may contain a &lsquo;<samp>where equality is <var>equalitypred</var></samp>&rsquo; specification
only if it declares a discriminated union type or a foreign type
(see <a href="Using-foreign-types-from-Mercury.html#Using-foreign-types-from-Mercury">Using foreign types from Mercury</a>)
and the following conditions are satisfied:
</p>
<ul>
<li> <var>equalitypred</var> must be the name of a predicate with signature
<div class="example">
<pre class="example">:- pred <var>equalitypred</var>(foo(T1, &hellip;, TN)::in,
                foo(T1, &hellip;, TN)::in) is semidet.
</pre></div>

<p>It is legal for the type, mode and determinism to be more permissive:
the type or the mode&rsquo;s initial insts may be more general
(e.g. the type of the equality predicate
could be just the polymorphic type &lsquo;<samp>pred(T, T)</samp>&rsquo;)
and the mode&rsquo;s final insts or the determinism may be more specific
(e.g. the determinism of the equality predicate
could be any of <code>det</code>, <code>failure</code> or <code>erroneous</code>).
</p>
</li><li> If the type is a discriminated union
then its definition cannot be a single zero-arity constructor.

</li><li> The equality predicate must be &ldquo;pure&rdquo; (see <a href="Impurity.html#Impurity">Impurity</a>).

</li><li> The equality predicate must be defined in the same module as the type.

</li><li> If the type is exported the equality predicate must also be exported.

</li><li> <var>equalitypred</var> should be an equivalence relation;
that is, it must be symmetric, reflexive, and transitive.
However, the compiler is not required to check this
<a name="DOCF3" href="#FOOT3"><sup>3</sup></a>.

</li></ul>

<p>Types with user-defined equality can only be used in limited ways.
Because there are multiple representations for the same abstract value,
any attempt to examine the representation of such a value
is a conceptually non-deterministic operation.
In Mercury this is modelled using committed choice nondeterminism.
</p>
<p>The semantics of specifying &lsquo;<samp>where equality is <var>equalitypred</var></samp>&rsquo;
on the type declaration for a type <var>T</var> are as follows:
</p>
<ul>
<li> If the program contains any deconstruction unification or switch
on a variable of type <var>T</var> that could fail,
other than unifications with mode &lsquo;<samp>(in, in)</samp>&rsquo;,
then it is a compile-time error.

</li><li> If the program contains any deconstruction unification or switch
on a variable of type <var>T</var> that cannot fail,
then that operation has determinism <code>cc_multi</code>.

</li><li> Any attempts to examine the representation of a variable of type <var>T</var>
using facilities of the standard library
(e.g. &lsquo;<samp>argument</samp>&rsquo;/3 and &lsquo;<samp>functor/3</samp>&rsquo; in &lsquo;<samp>deconstruct</samp>&rsquo;)
that do not have determinism <code>cc_multi</code> or <code>cc_nondet</code>
will result in a run-time error.

</li><li> In addition to the usual equality axioms,
the declarative semantics of the program will contain the axiom
&lsquo;<samp><var>X</var> = <var>Y</var> &lt;=&gt; <var>equalitypred</var>(<var>X</var>, <var>Y</var>)</samp>&rsquo;
for all <var>X</var> and <var>Y</var> of type &lsquo;<samp>T</samp>&rsquo;.

</li><li> Any &lsquo;<samp>(in, in)</samp>&rsquo; unifications for type <var>T</var>
are computed using the specified predicate <var>equalitypred</var>.

</li></ul>

<p>A type declaration for a type &lsquo;<samp>foo(T1, &hellip;, TN)</samp>&rsquo;
may contain a &lsquo;<samp>where comparison is <var>comparepred</var></samp>&rsquo; specification
only if it declares a discriminated union type or a foreign type
(see <a href="Using-foreign-types-from-Mercury.html#Using-foreign-types-from-Mercury">Using foreign types from Mercury</a>) and the
following conditions are satisfied:
</p>
<ul>
<li> <var>comparepred</var> must be the name of a predicate with signature
<div class="example">
<pre class="example">:- pred <var>comparepred</var>(builtin.comparison_result::uo,
                foo(T1, &hellip;, TN)::in, foo(T1, &hellip;, TN)::in) is det.
</pre></div>

<p>As with equality predicates,
it is legal for the type, mode and determinism to be more permissive.
</p>
</li><li> If the type is a discriminated union
then its definition cannot be a single zero-arity constructor.

</li><li> The comparison predicate must also be &ldquo;pure&rdquo; (see <a href="Impurity.html#Impurity">Impurity</a>).

</li><li> The comparison predicate must be defined in the same module as the type.

</li><li> If the type is exported the comparison predicate must also be exported.

</li><li> The relation
<div class="example">
<pre class="example">compare_eq(X, Y) :- <var>comparepred</var>((=), X, Y).
</pre></div>
<p>must be an equivalence relation;
that is, it must be symmetric, reflexive, and transitive.
The compiler is not required to check this.
</p>
</li><li> The relations
<div class="example">
<pre class="example">compare_leq(X, Y) :- <var>comparepred</var>(R, X, Y), (R = (=) ; R = (&lt;)).
compare_geq(X, Y) :- <var>comparepred</var>(R, X, Y), (R = (=) ; R = (&gt;)).
</pre></div>
<p>must be total order relations:
that is they must be antisymmetric, reflexive and transitive.
The compiler is not required to check this.
</p>
</li></ul>

<p>For each type for which the declaration has a
&lsquo;<samp>where comparison is <var>comparepred</var></samp>&rsquo; specification,
any calls to the standard library predicate &lsquo;<samp>builtin.compare/3</samp>&rsquo;
with arguments of that type are evaluated
as if they were calls to <var>comparepred</var>.
</p>
<p>A type declaration may contain a
&lsquo;<samp>where equality is <var>equalitypred</var>, comparison is <var>comparepred</var></samp>&rsquo;
specification only if in addition to the conditions above,
&lsquo;<samp>all [X, Y] (<var>comparepred</var>((=), X, Y) &lt;=&gt; <var>equalitypred</var>(X, Y))</samp>&rsquo;.
The compiler is not required to check this.
</p>
<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h3><a name="FOOT3" href="#DOCF3">(3)</a></h3>
<p>If <var>equalitypred</var> is not an equivalence relation,
then the program is inconsistent:
its declarative semantics contains a contradiction,
because the additional axioms for the user-defined equality
contradict the standard equality axioms.
That implies that the implementation
may compute any answer at all (see <a href="Semantics.html#Semantics">Semantics</a>),
i.e. the behaviour of the program is undefined.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Higher_002dorder.html#Higher_002dorder" accesskey="n" rel="next">Higher-order</a>, Previous: <a href="Determinism.html#Determinism" accesskey="p" rel="previous">Determinism</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
