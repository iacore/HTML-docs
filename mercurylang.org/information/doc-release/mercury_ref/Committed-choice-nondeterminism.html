<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Committed choice nondeterminism</title>

<meta name="description" content="The Mercury Language Reference Manual: Committed choice nondeterminism">
<meta name="keywords" content="The Mercury Language Reference Manual: Committed choice nondeterminism">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Determinism.html#Determinism" rel="up" title="Determinism">
<link href="User_002ddefined-equality-and-comparison.html#User_002ddefined-equality-and-comparison" rel="next" title="User-defined equality and comparison">
<link href="Interfacing-nondeterministic-code-with-the-real-world.html#Interfacing-nondeterministic-code-with-the-real-world" rel="previous" title="Interfacing nondeterministic code with the real world">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Committed-choice-nondeterminism"></a>
<div class="header">
<p>
Previous: <a href="Interfacing-nondeterministic-code-with-the-real-world.html#Interfacing-nondeterministic-code-with-the-real-world" accesskey="p" rel="previous">Interfacing nondeterministic code with the real world</a>, Up: <a href="Determinism.html#Determinism" accesskey="u" rel="up">Determinism</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Committed-choice-nondeterminism-1"></a>
<h3 class="section">6.5 Committed choice nondeterminism</h3>

<p>In addition to the determinism annotations described earlier,
there are &ldquo;committed choice&rdquo; versions of <code>multi</code> and <code>nondet</code>,
called <code>cc_multi</code> and <code>cc_nondet</code>.
These can be used instead of <code>multi</code> or <code>nondet</code>
if all calls to that mode of the predicate (or function)
occur in a context in which only one solution is needed.
</p>
<p>Such single-solution contexts are determined as follows.
</p>
<ul>
<li> The body of any procedure declared <code>cc_multi</code> or <code>cc_nondet</code>
is in a single-solution context.
For example, the program entry point &lsquo;<samp>main/2</samp>&rsquo;
may be declared <code>cc_multi</code>,
and in that case the clauses for <code>main</code> are in a single-solution context.

</li><li> Any goal with no output variables is in a single-solution context.

</li><li> If a conjunction is in a single-solution context,
then the right-most conjunct is in a single-solution context,
and if the right-most conjunct cannot fail,
then the rest of the conjunction is also in a single-solution context.
(&ldquo;Right-most&rdquo; here refers to the order <em>after</em> mode reordering.)

</li><li> If an if-then-else is in a single-solution context,
then the &ldquo;then&rdquo; part and the &ldquo;else&rdquo; part are in single-solution contexts,
and if the &ldquo;then&rdquo; part cannot fail,
then the condition of the if-then-else is also in a single-solution context.

</li><li> For other compound goals, i.e. disjunctions, negations,
and (explicitly) existentially quantified goals,
if the compound goal is in a single-solution context,
then the immediate sub-goals of that compound goal
are also in single-solution contexts.

</li></ul>

<p>The compiler will check that
all calls to a committed-choice mode of a predicate (or function)
do indeed occur in a single-solution context.
</p>
<p>You can declare two different modes of a predicate (or function)
which differ only in &ldquo;cc-ness&rdquo;
(i.e. one being <code>multi</code> and the other <code>cc_multi</code>,
or one being <code>nondet</code> and the other <code>cc_nondet</code>).
In that case,
the compiler will select the appropriate one for each call
depending on whether the call comes from a single-solution context or not.
Calls from single-solution contexts will call the committed choice version,
while calls which are not from single-solution contexts
will call the backtracking version.
</p>
<p>There are several reasons to use committed choice determinism annotations.
One reason is for efficiency:
committed choice annotations allow the compiler
to generate much more efficient code.
Another reason is for doing I/O,
which is allowed only in <code>det</code> or <code>cc_multi</code> predicates,
not in <code>multi</code> predicates.
Another is for dealing with types that use non-canonical representations
(see <a href="User_002ddefined-equality-and-comparison.html#User_002ddefined-equality-and-comparison">User-defined equality and comparison</a>).
And there are a variety of other applications.
</p>


<hr>
<div class="header">
<p>
Previous: <a href="Interfacing-nondeterministic-code-with-the-real-world.html#Interfacing-nondeterministic-code-with-the-real-world" accesskey="p" rel="previous">Interfacing nondeterministic code with the real world</a>, Up: <a href="Determinism.html#Determinism" accesskey="u" rel="up">Determinism</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
