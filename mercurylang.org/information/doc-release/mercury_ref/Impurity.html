<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Impurity</title>

<meta name="description" content="The Mercury Language Reference Manual: Impurity">
<meta name="keywords" content="The Mercury Language Reference Manual: Impurity">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html#Top" rel="up" title="Top">
<link href="Purity-levels.html#Purity-levels" rel="next" title="Purity levels">
<link href="Using-pragma-foreign_005fcode-for-Java.html#Using-pragma-foreign_005fcode-for-Java" rel="previous" title="Using pragma foreign_code for Java">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Impurity"></a>
<div class="header">
<p>
Next: <a href="Solver-types.html#Solver-types" accesskey="n" rel="next">Solver types</a>, Previous: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="p" rel="previous">Foreign language interface</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Impurity-declarations"></a>
<h2 class="chapter">16 Impurity declarations</h2>

<p>In order to efficiently implement certain predicates,
it is occasionally necessary to venture outside pure logic programming.
Other predicates cannot be implemented at all
within the paradigm of logic programming,
for example, all solutions predicates.
Such predicates are often written using the foreign language interface.
Sometimes, however, it would be more convenient, or more efficient,
to write such predicates using the facilities of Mercury.
For example, it is much more convenient to access
arguments of compound Mercury terms in Mercury than in C,
and the ability of the Mercury compiler to specialize code
can make higher-order predicates written in Mercury
significantly more efficient than similar C code.
</p>
<p>One important aim of Mercury&rsquo;s impurity system
is to make the distinction between the pure and impure code very clear.
This is done by requiring every impure predicate or function to be so declared,
and by requiring every call to an impure predicate or function
to be flagged as such.
Predicates or functions that are implemented
in terms of impure predicates or functions
are assumed to be impure themselves
unless they are explicitly promised to be pure.
</p>
<p>Please note that the facilities described here are needed only very rarely.
The main intent is for implementing language primitives
such as the all solutions predicates,
or for implementing interfaces to foreign language libraries
using the foreign language interface.
Any other use of &lsquo;<samp>impure</samp>&rsquo; or &lsquo;<samp>semipure</samp>&rsquo; probably indicates
either a weakness in the Mercury standard library,
or the programmer&rsquo;s lack of familiarity with the standard library.
Newcomers to Mercury are hence encouraged to <strong>skip this section</strong>.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Purity-levels.html#Purity-levels" accesskey="1">Purity levels</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Choosing the right level of purity.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Purity-ordering.html#Purity-ordering" accesskey="2">Purity ordering</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">How purity levels are ordered
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Impurity-semantics.html#Impurity-semantics" accesskey="3">Impurity semantics</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">What impure code means.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Declaring-impurity.html#Declaring-impurity" accesskey="4">Declaring impurity</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Declaring predicates impure.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Impure-goals.html#Impure-goals" accesskey="5">Impure goals</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Marking a goal as impure.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Promising-purity.html#Promising-purity" accesskey="6">Promising purity</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Promising that a predicate is pure.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Impurity-example.html#Impurity-example" accesskey="7">Impurity example</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">A simple example using impurity.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Higher_002dorder-impurity.html#Higher_002dorder-impurity" accesskey="8">Higher-order impurity</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Using impurity with higher-order code.
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="Solver-types.html#Solver-types" accesskey="n" rel="next">Solver types</a>, Previous: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="p" rel="previous">Foreign language interface</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
