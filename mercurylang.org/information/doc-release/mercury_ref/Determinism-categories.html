<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Determinism categories</title>

<meta name="description" content="The Mercury Language Reference Manual: Determinism categories">
<meta name="keywords" content="The Mercury Language Reference Manual: Determinism categories">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Determinism.html#Determinism" rel="up" title="Determinism">
<link href="Determinism-checking-and-inference.html#Determinism-checking-and-inference" rel="next" title="Determinism checking and inference">
<link href="Determinism.html#Determinism" rel="previous" title="Determinism">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Determinism-categories"></a>
<div class="header">
<p>
Next: <a href="Determinism-checking-and-inference.html#Determinism-checking-and-inference" accesskey="n" rel="next">Determinism checking and inference</a>, Up: <a href="Determinism.html#Determinism" accesskey="u" rel="up">Determinism</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Determinism-categories-1"></a>
<h3 class="section">6.1 Determinism categories</h3>

<p>For each mode of a predicate or function,
we categorise that mode according to how many times it can succeed,
and whether or not it can fail before producing its first solution.
</p>
<p>If all possible calls to a particular mode of a predicate or function
which return to the caller
(calls which terminate,
do not throw an exception
and do not cause a fatal runtime error)
</p>
<ul>
<li> have exactly one solution,
then that mode is <em>deterministic</em> (<code>det</code>);

</li><li> either have no solutions or have one solution,
then that mode is <em>semideterministic</em> (<code>semidet</code>);

</li><li> have at least one solution but may have more,
then that mode is <em>multisolution</em> (<code>multi</code>);

</li><li> have zero or more solutions,
then that mode is <em>nondeterministic</em> (<code>nondet</code>);

</li><li> fail without producing a solution,
then that mode has a determinism of <code>failure</code>.
</li></ul>

<p>If no possible calls to a particular mode of a predicate or function
can return to the caller,
then that mode has a determinism of <code>erroneous</code>.
</p>
<p>The determinism annotation <code>erroneous</code> is used on the library predicates
&lsquo;<samp>require.error/1</samp>&rsquo; and &lsquo;<samp>exception.throw/1</samp>&rsquo;,
but apart from that,
determinism annotations <code>erroneous</code> and <code>failure</code>
are generally not needed.
</p>
<p>To summarize:
</p>
<div class="example">
<pre class="example">                Maximum number of solutions
Can fail?       0               1               &gt; 1
no              erroneous       det             multi
yes             failure         semidet         nondet
</pre></div>

<p>(Note: the &ldquo;Can fail?&rdquo; column here indicates only whether the procedure
can fail before producing at least one solution;
attempts to find a <em>second</em> solution to a particular call,
e.g. for a procedure with determinism <code>multi</code>,
are always allowed to fail.)
</p>
<p>The determinism of each mode of a predicate or function
is indicated by an annotation on the mode declaration.
For example:
</p>
<div class="example">
<pre class="example">:- pred append(list(T), list(T), list(T)).
:- mode append(in, in, out) is det.
:- mode append(out, out, in) is multi.
:- mode append(in, in, in) is semidet.

:- func length(list(T)) = int.
:- mode length(in) = out is det.
:- mode length(in(list_skel)) = out is det.
:- mode length(in) = in is semidet.
</pre></div>

<p>An annotation of <code>det</code> or <code>multi</code> is an assertion
that for every value each of the inputs,
there exists at least one value of the outputs
for which the predicate is true,
or (in the case of functions)
for which the function term is equal to the result term.
Conversely, an annotation of <code>det</code> or <code>semidet</code> is an assertion
that for every value each of the inputs,
there exists at most one value of the outputs for which the predicate is true,
or (in the case of functions)
for which the function term is equal to the result term.
These assertions are called the <em>mode-determinism assertions</em>;
they can play a role in the semantics,
because in certain circumstances,
they may allow an implementation to perform
optimizations that would not otherwise be allowed,
such as optimizing away a goal with no outputs
even though it might infinitely loop.
</p>
<p>If the mode of the predicate is given in the <code>:- pred</code> declaration
rather than in a separate <code>:- mode</code> declaration,
then the determinism annotation goes on the <code>:- pred</code> declaration
(and similarly for functions).
In particular, this is necessary
if a predicate does not have any argument variables.
If the determinism declaration is given on a <code>:- func</code> declaration
without the mode, the function is assumed to have the default mode
(see <a href="Modes.html#Modes">Modes</a> for more information on default modes of functions).
</p>
<p>For example:
</p>
<div class="example">
<pre class="example">:- pred loop(int::in) is erroneous.
loop(X) :- loop(X).

:- pred p is det.
p.

:- pred q is failure.
q :- fail.
</pre></div>

<p>If there is no mode declaration for a function,
then the default mode for that function
is considered to have been declared as <code>det</code>.
If you want to write a partial function,
i.e. one whose determinism is <code>semidet</code>,
then you must explicitly declare the mode and determinism.
</p>
<p>In Mercury, a function is supposed to be
a true mathematical function of its arguments;
that is, the value of the function&rsquo;s result
should be determined only by the values of its arguments.
Hence, for any mode of a function
that specifies that all the arguments are fully input
(i.e. for which the initial inst of all the arguments is a ground inst),
the determinism of that mode can only be
<code>det</code>, <code>semidet</code>, <code>erroneous</code>, or <code>failure</code>.
</p>
<p>The determinism categories form this lattice:
</p>
<div class="example">
<pre class="example">             erroneous
              /     \
          failure   det
             \     /   \
             semidet  multi
                 \     /
                  nondet
</pre></div>

<p>The higher up this lattice a determinism category is,
the more the compiler knows about the number of solutions
of procedures of that determinism.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Determinism-checking-and-inference.html#Determinism-checking-and-inference" accesskey="n" rel="next">Determinism checking and inference</a>, Up: <a href="Determinism.html#Determinism" accesskey="u" rel="up">Determinism</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
