<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Some idioms using existentially quantified types</title>

<meta name="description" content="The Mercury Language Reference Manual: Some idioms using existentially quantified types">
<meta name="keywords" content="The Mercury Language Reference Manual: Some idioms using existentially quantified types">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Existential-types.html#Existential-types" rel="up" title="Existential types">
<link href="Type-conversions.html#Type-conversions" rel="next" title="Type conversions">
<link href="Existentially-typed-data-types.html#Existentially-typed-data-types" rel="previous" title="Existentially typed data types">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Some-idioms-using-existentially-quantified-types"></a>
<div class="header">
<p>
Previous: <a href="Existentially-typed-data-types.html#Existentially-typed-data-types" accesskey="p" rel="previous">Existentially typed data types</a>, Up: <a href="Existential-types.html#Existential-types" accesskey="u" rel="up">Existential types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Some-idioms-using-existentially-quantified-types-1"></a>
<h3 class="section">11.4 Some idioms using existentially quantified types</h3>

<p>The standard library module &lsquo;<samp>univ</samp>&rsquo;
provides an abstract type named &lsquo;<samp>univ</samp>&rsquo; which can hold values of any type.
You can form heterogeneous containers
(containers that can hold values of different types at the same time)
by using data structures that contain <code>univ</code>s, e.g. &lsquo;<samp>list(univ)</samp>&rsquo;.
</p>
<p>The interface to &lsquo;<samp>univ</samp>&rsquo; includes the following:
</p>
<div class="example">
<pre class="example">% `univ' is a type which can hold any value.
:- type univ.

% The function univ/1 takes a value of any type and constructs
% a `univ' containing that value (the type will be stored along
% with the value)
:- func univ(T) = univ.

% The function univ_value/1 takes a `univ' argument and extracts
% the value contained in the `univ' (together with its type).
% This is the inverse of the function univ/1.
:- some [T] func univ_value(univ) = T.
</pre></div>

<p>The &lsquo;<samp>univ</samp>&rsquo; type in the standard library
is in fact a simple example of an existentially typed data type.
It could be implemented as follows:
</p>
<div class="example">
<pre class="example">:- implementation.
:- type univ
    ---&gt;    some [T] mkuniv(T).
univ(X) = 'new mkuniv'(X).
univ_value(mkuniv(X)) = X.
</pre></div>

<p>An existentially typed procedure
is not allowed to have different types for its existentially typed arguments
in different clauses or in different subgoals of a single clause.
For instance, both of the following examples are illegal:
</p>
<div class="example">
<pre class="example">:- some [T] pred bad_example(string, T).

bad_example(&quot;foo&quot;, 42).
bad_example(&quot;bar&quot;, &quot;blah&quot;).
    % type error (cannot unify `int' and `string')

:- some [T] pred bad_example2(string, T).

bad_example2(Name, Value) :-
    ( Name = &quot;foo&quot;, Value = 42
    ; Name = &quot;bar&quot;, Value = &quot;blah&quot;
    ).
    % type error (cannot unify `int' and `string')
</pre></div>

<p>However, using &lsquo;<samp>univ</samp>&rsquo;,
it is possible for an existentially typed function
to return values of different types at each invocation.
</p>
<div class="example">
<pre class="example">:- some [T] pred good_example(string, T).

good_example(Name, univ_value(Univ)) :-
    ( Name = &quot;foo&quot;, Univ = univ(42)
    ; Name = &quot;bar&quot;, Univ = univ(&quot;blah&quot;)
    ).
</pre></div>

<p>Using &lsquo;<samp>univ</samp>&rsquo; doesn&rsquo;t work if you also want to use type class constraints.
If you want to use type class constraints,
then you must define your own existentially typed data type,
analogous to &lsquo;<samp>univ</samp>&rsquo;, and use that:
</p>
<div class="example">
<pre class="example">:- type univ_showable
    ---&gt;    some [T] (mkshowable(T) =&gt; showable(T)).

:- some [T] pred harder_example(string, T) =&gt; showable(T).

harder_example(Name, Showable) :-
    ( Name = &quot;bar&quot;, Univ = 'new mkshowable'(42)
    ; Name = &quot;bar&quot;, Univ = 'new mkshowable'(&quot;blah&quot;)
    ),
    Univ = mkshowable(Showable).
</pre></div>

<p>The issue can also arise for mode-specific clauses
(see <a href="Different-clauses-for-different-modes.html#Different-clauses-for-different-modes">Different clauses for different modes</a>).
For instance, the following example is illegal:
</p>
<div class="example">
<pre class="example">:- some [T] pred bad_example3(string, T).
:-         mode bad_example3(in(bound(&quot;foo&quot;)), out) is det.
:-          mode bad_example3(in(bound(&quot;bar&quot;)), out) is det.
:- pragma promise_pure(bad_example3/2).
bad_example3(&quot;foo&quot;::in(bound(&quot;foo&quot;)), 42::out).
bad_example3(&quot;bar&quot;::in(bound(&quot;bar&quot;)), &quot;blah&quot;::out).
    % type error (cannot unify `int' and `string')
</pre></div>

<p>The solution is similar,
although in this case an intermediate predicate is required:
</p>
<div class="example">
<pre class="example">:- some [T] pred good_example3(string, T).
:-          mode good_example3(in(bound(&quot;foo&quot;)), out) is det.
:-          mode good_example3(in(bound(&quot;bar&quot;)), out) is det.
good_example3(Name, univ_value(Univ)) :-
	good_example3_univ(Name, Univ).

:- pred good_example3_univ(string, univ).
:- mode good_example3_univ(in(bound(&quot;foo&quot;)), out) is det.
:- mode good_example3_univ(in(bound(&quot;bar&quot;)), out) is det.
:- pragma promise_pure(good_example3_univ/2).
good_example3_univ(&quot;foo&quot;::in(bound(&quot;foo&quot;)), univ(42)::out).
good_example3_univ(&quot;bar&quot;::in(bound(&quot;bar&quot;)), univ(&quot;blah&quot;)::out).
</pre></div>


<hr>
<div class="header">
<p>
Previous: <a href="Existentially-typed-data-types.html#Existentially-typed-data-types" accesskey="p" rel="previous">Existentially typed data types</a>, Up: <a href="Existential-types.html#Existential-types" accesskey="u" rel="up">Existential types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
