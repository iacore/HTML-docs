<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Creating higher-order terms</title>

<meta name="description" content="The Mercury Language Reference Manual: Creating higher-order terms">
<meta name="keywords" content="The Mercury Language Reference Manual: Creating higher-order terms">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Higher_002dorder.html#Higher_002dorder" rel="up" title="Higher-order">
<link href="Calling-higher_002dorder-terms.html#Calling-higher_002dorder-terms" rel="next" title="Calling higher-order terms">
<link href="Higher_002dorder.html#Higher_002dorder" rel="previous" title="Higher-order">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Creating-higher_002dorder-terms"></a>
<div class="header">
<p>
Next: <a href="Calling-higher_002dorder-terms.html#Calling-higher_002dorder-terms" accesskey="n" rel="next">Calling higher-order terms</a>, Up: <a href="Higher_002dorder.html#Higher_002dorder" accesskey="u" rel="up">Higher-order</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Creating-higher_002dorder-terms-1"></a>
<h3 class="section">8.1 Creating higher-order terms</h3>

<p>To create a higher-order predicate or function term,
you can use a lambda expression,
or, if the predicate or function has only one mode
and it is not a zero-arity function,
you can just use its name.
For example, if you have declared a predicate
</p>
<div class="example">
<pre class="example">:- pred sum(list(int), int).
:- mode sum(in, out) is det.
</pre></div>

<p>the following unifications have the same effect:
</p>
<div class="example">
<pre class="example">X = (pred(List::in, Length::out) is det :- sum(List, Length))
Y = sum
</pre></div>

<p>In the above example,
the type of <code>X</code>, and <code>Y</code> is &lsquo;<samp>pred(list(int), int)</samp>&rsquo;,
which means a predicate of two arguments
of types <code>list(int)</code> and <code>int</code> respectively.
</p>
<p>Similarly, given
</p>
<div class="example">
<pre class="example">:- func scalar_product(int, list(int)) = list(int).
:- mode scalar_product(in, in) = out is det.
</pre></div>

<p>the following three unifications have the same effect:
</p>
<div class="example">
<pre class="example">X = (func(Num, List) = NewList :- NewList = scalar_product(Num, List))
Y = (func(Num::in, List::in) = (NewList::out) is det
        :- NewList = scalar_product(Num, List))
Z = scalar_product
</pre></div>

<p>In the above example,
the type of <code>X</code>, <code>Y</code>, and <code>Z</code> is
&lsquo;<samp>func(int, list(int)) = list(int)</samp>&rsquo;,
which means a function of two arguments,
whose types are <code>int</code> and <code>list(int)</code>,
with a return type of <code>int</code>.
As with &lsquo;<samp>:- func</samp>&rsquo; declarations,
if the modes and determinism of the function
are omitted in a higher-order function term,
then the modes default to <code>in</code> for the arguments,
<code>out</code> for the function result,
and the determinism defaults to <code>det</code>.
</p>
<p>The Melbourne Mercury implementation currently requires
that you use an explicit lambda expression to specify which mode you want,
if the predicate or function has more than one mode
(but see below for an exception to this rule).
</p>
<p>You can also create higher-order function terms
of non-zero arity and higher-order predicate terms by &ldquo;currying&rdquo;,
i.e. specifying the first few arguments to a predicate or function,
but leaving the remaining arguments unspecified.
For example, the unification
</p>
<div class="example">
<pre class="example">Sum123 = sum([1,2,3])
</pre></div>

<p>binds <code>Sum123</code> to a higher-order predicate term of type &lsquo;<samp>pred(int)</samp>&rsquo;.
Similarly, the unification
</p>
<div class="example">
<pre class="example">Double = scalar_product(2)
</pre></div>

<p>binds <code>Double</code> to a higher-order function term of type
&lsquo;<samp>func(list(int)) = list(int)</samp>&rsquo;.
</p>
<p>As a special case, currying of a multi-moded predicate or function is allowed
provided that the mode of the predicate or function
can be determined from the insts of the higher-order curried arguments.
For example, &lsquo;<samp>P = list.foldl(io.write)</samp>&rsquo; is allowed
because the inst of &lsquo;<samp>io.write</samp>&rsquo; matches
exactly one mode of &lsquo;<samp>list.foldl</samp>&rsquo;.
</p>
<p>For higher-order predicate expressions that thread an accumulator pair,
we have syntax that allows you to use DCG notation
in the goal of the expression.
For example,
</p>
<div class="example">
<pre class="example">Pred = (pred(Strings::in, Num::out, di, uo) is det --&gt;
    io.write_string(&quot;The strings are: &quot;),
    { list.length(Strings, Num) },
    io.write_strings(Strings),
    io.nl
)
</pre></div>

<p>is equivalent to
</p>
<div class="example">
<pre class="example">Pred = (pred(Strings::in, Num::out, IO0::di, IO::uo) is det :-
    io.write_string(&quot;The strings are: &quot;, IO0, IO1),
    list.length(Strings, Num),
    io.write_strings(Strings, IO1, IO2),
    io.nl(IO2, IO)
)
</pre></div>

<p>Higher-order function terms of zero arity
can only be created using an explicit lambda expression;
you have to use e.g. &lsquo;<samp>(func) = foo</samp>&rsquo; rather than plain &lsquo;<samp>foo</samp>&rsquo;,
because the latter denotes the result of evaluating the function,
rather than the function itself.
</p>
<p>Note that when constructing a higher-order term,
you cannot just use the name of a builtin language construct
such as &lsquo;<samp>=</samp>&rsquo;, &lsquo;<samp>\=</samp>&rsquo;, &lsquo;<samp>call</samp>&rsquo;, or &lsquo;<samp>apply</samp>&rsquo;,
and nor can such constructs be curried.
Instead, you must either use an explicit lambda expression,
or you must write a forwarding predicate or function.
For example, instead of
</p>
<div class="example">
<pre class="example">list.filter(\=(2), [1, 2, 3], List)
</pre></div>

<p>you must write either
</p>
<div class="example">
<pre class="example">list.filter((pred(X::in) is semidet :- X \= 2), [1, 2, 3], List)
</pre></div>

<p>or
</p>
<div class="example">
<pre class="example">list.filter(not_equal(2), [1, 2, 3], List)
</pre></div>

<p>where you have defined &lsquo;<samp>not_equal</samp>&rsquo; using
</p>
<div class="example">
<pre class="example">:- pred not_equal(T::in, T::in) is semidet.
not_equal(X, Y) :- X \= Y.
</pre></div>

<p>Another case when this arises is when want to curry a higher-order term.
Suppose, for example, that you have
a higher-order predicate term &lsquo;<samp>OldPred</samp>&rsquo;
of type &lsquo;<samp>pred(int, char, float)</samp>&rsquo;,
and you want to construct a new higher-order predicate term
&lsquo;<samp>NewPred</samp>&rsquo; of type &lsquo;<samp>pred(char, float)</samp>&rsquo; from &lsquo;<samp>OldPred</samp>&rsquo;
by supplying a value for just the first argument.
The solution is the same:
use an explicit lambda expression or a forwarding predicate.
In either case, the body of the lambda expression or the forwarding predicate
must contain a higher-order call with all the arguments supplied.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Calling-higher_002dorder-terms.html#Calling-higher_002dorder-terms" accesskey="n" rel="next">Calling higher-order terms</a>, Up: <a href="Higher_002dorder.html#Higher_002dorder" accesskey="u" rel="up">Higher-order</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
