<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Functional dependencies</title>

<meta name="description" content="The Mercury Language Reference Manual: Functional dependencies">
<meta name="keywords" content="The Mercury Language Reference Manual: Functional dependencies">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Type-classes.html#Type-classes" rel="up" title="Type classes">
<link href="Existential-types.html#Existential-types" rel="next" title="Existential types">
<link href="Type-class-constraints-on-instance-declarations.html#Type-class-constraints-on-instance-declarations" rel="previous" title="Type class constraints on instance declarations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Functional-dependencies"></a>
<div class="header">
<p>
Previous: <a href="Type-class-constraints-on-instance-declarations.html#Type-class-constraints-on-instance-declarations" accesskey="p" rel="previous">Type class constraints on instance declarations</a>, Up: <a href="Type-classes.html#Type-classes" accesskey="u" rel="up">Type classes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Functional-dependencies-1"></a>
<h3 class="section">10.8 Functional dependencies</h3>

<p>Type class constraints may include any number of functional dependencies.
A <em>functional dependency</em> constraint
takes the form <code>(<var>Domain</var> -&gt; <var>Range</var>)</code>.
The <var>Domain</var> and <var>Range</var> arguments are either single type variables,
or conjunctions of type variables separated by commas.
</p>
<div class="example">
<pre class="example">	:- typeclass <var>Typeclass</var>(<var>Var</var>, &hellip;)&nbsp;&lt;= ((<var>D</var> -&gt; <var>R</var>), &hellip;) &hellip;

	:- typeclass <var>Typeclass</var>(<var>Var</var>, &hellip;)&nbsp;&lt;= (<var>D1</var>, <var>D2</var>, &hellip; -&gt; <var>R1</var>, <var>R2</var>, &hellip;), &hellip;
</pre></div>

<p>Each type variable must appear in the parameter list of the typeclass.
Abstract typeclass declarations must have
exactly the same functional dependencies as their concrete forms.
</p>
<p>Mutually recursive functional dependencies are allowed,
so the following examples are legal:
</p>
<div class="example">
<pre class="example">	:- typeclass foo(A, B) &lt;= ((A -&gt; B), (B -&gt; A)).
	:- typeclass bar(A, B, C, D)&nbsp;&lt;= ((A, B -&gt; C), (B, C -&gt; D), (D -&gt; A, C)).
</pre></div>

<p>A functional dependency on a typeclass places an additional requirement
on the set of instances which are allowed for that type class.
The requirement is that
all types bound to variables in the range of the functional dependency
must be able to be uniquely determined
by the types bound to variables in the domain of the functional dependency.
If more than one functional dependency is present,
then the requirement for each one must be satisfied.
</p>
<p>For example, given the typeclass declaration
</p>
<div class="example">
<pre class="example">:- typeclass baz(A, B) &lt;= (A -&gt; B) where &hellip;
</pre></div>

<p>it would be illegal to have both of the instances
</p>
<div class="example">
<pre class="example">:- instance baz(int, int) where &hellip;
:- instance baz(int, string) where &hellip;
</pre></div>

<p>although either one would be acceptable on its own.
</p>
<p>The following instance would also be illegal
</p>
<div class="example">
<pre class="example">:- instance baz(string, list(T)) where &hellip;
</pre></div>

<p>since the variable <code>T</code> may not always be bound to the same type.
However, the instance
</p>
<div class="example">
<pre class="example">:- instance baz(list(S), list(T)) &lt;= baz(S, T) where &hellip;
</pre></div>

<p>is legal because
the &lsquo;<samp>baz(S, T)</samp>&rsquo; constraint ensures that
whatever <code>T</code> is bound to,
it is always uniquely determined from the binding of <code>S</code>.
</p>
<p>The extra requirements that result from the use of functional dependencies
allow the bindings of some variables
to be determined from the bindings of others.
This in turn relaxes some of the requirements
of typeclass constraints on predicate and function signatures,
and on existentially typed data constructors.
</p>
<p>Without any functional dependencies, all variables in constraints
must appear in the signature of the predicate or function being declared.
However, variables which are in the range of a functional dependency
need not appear in the signature,
since it is known that their bindings will be determined
from the bindings of the variables in the domain.
</p>
<p>More formally, the constraints on a predicate or function signature
<em>induce</em> a set of functional dependencies
on the variables appearing in those constraints.
A functional dependency &lsquo;<samp>(A1, &hellip; -&gt; B1, &hellip;)</samp>&rsquo;
is induced from a constraint
&lsquo;<samp><var>Typeclass</var>(<var>Type1</var>, &hellip;)</samp>&rsquo;
if and only if the typeclass &lsquo;<samp><var>Typeclass</var></samp>&rsquo;
has a functional dependency &lsquo;<samp>(D1, &hellip; -&gt; R1, &hellip;)</samp>&rsquo;,
and for each typeclass parameter &lsquo;<samp>Di</samp>&rsquo; there exists an &lsquo;<samp>Aj</samp>&rsquo;
every type variable appearing in the &lsquo;<samp><var>Typek</var></samp>&rsquo;
corresponding to &lsquo;<samp>Di</samp>&rsquo;,
and each &lsquo;<samp>Bi</samp>&rsquo; appears in the &lsquo;<samp><var>Typej</var></samp>&rsquo;
bound to the typeclass parameter &lsquo;<samp>Rk</samp>&rsquo; for some <var>k</var>.
</p>
<p>For example, with the definition of <code>baz</code> above,
the constraint <code>baz(map(X, Y), list(Z))</code>
induces the constraint <code>(X, Y -&gt; Z)</code>,
since <var>X</var> and <var>Y</var> appear in the domain argument,
and <var>Z</var> appears in the range argument.
</p>
<p>The set of type variables determined from a signature
is the <em>closure</em> of the set appearing in the signature
under the functional dependencies induced from the constraints.
The closure is defined as the smallest set of variables
which includes all of the variables appearing in the signature,
and is such that, for each induced functional dependency
&lsquo;<samp><var>Domain</var> -&gt; <var>Range</var></samp>&rsquo;,
if the closure includes all of the variables in <var>Domain</var>
then it includes all of the variables in <var>Range</var>.
</p>
<p>For example, the declaration
</p>
<div class="example">
<pre class="example">:- pred p(X, Y) &lt;= baz(map(X, Y), list(Z)).
</pre></div>

<p>is acceptable since the closure of {<var>X</var>,
<var>Y</var>} under the induced functional dependency must include <var>Z</var>.
Moreover, the typeclass <code>baz/2</code> would be allowed
to have a method that only uses the first parameter, <var>A</var>,
since the second parameter, <var>B</var>, would always be determined from the first.
</p>
<p>Note that, since all instances must satisfy the superclass constraints,
the restrictions on instances obviously transfer from superclass to subclass.
Again, this allows the requirements of typeclass constraints to be relaxed.
Thus, the functional dependencies on the ancestors of constraints
also induce functional dependencies on the variables,
and the closure that we calculate takes these into account.
</p>
<p>For example, in this code
</p>
<div class="example">
<pre class="example">:- typeclass quux(P, Q, R) &lt;= baz(R, P) where &hellip;

:- pred q(Q, R) &lt;= quux(P, Q, R).
</pre></div>
<p>the signature of <code>q/2</code> is acceptable
since the superclass constraint on <code>quux/2</code>
induces the dependency &lsquo;<samp>R -&gt; P</samp>&rsquo; on the type variables,
hence <var>P</var> is in the closure of {<var>Q</var>, <var>R</var>}.
</p>
<p>The presence of functional dependencies
also allows &ldquo;improvement&rdquo; to occur during type inference.
This can occur in two ways.
First, if two constraints of a given class match
on all of the domain arguments of a functional dependency on that class,
then it can be inferred that they also match on the range arguments.
For example,
given the constraints <code>baz(A,&nbsp;B1)</code><!-- /@w --> and <code>baz(A,&nbsp;B2)</code><!-- /@w -->,
it will be inferred that <code>B1 = B2</code>.
</p>
<p>Similarly, if a constraint of a given class
is subsumed by a known instance of that class in the domain arguments,
then its range arguments can be unified
with the corresponding instance range arguments.
For example, given the instance:
</p>
<div class="example">
<pre class="example">:- instance baz(list(T), string) where &hellip;
</pre></div>

<p>then the constraint <code>baz(list(int), X)</code>
can be improved with the inference that <code>X&nbsp;=&nbsp;string</code><!-- /@w -->.
</p>
<hr>
<div class="header">
<p>
Previous: <a href="Type-class-constraints-on-instance-declarations.html#Type-class-constraints-on-instance-declarations" accesskey="p" rel="previous">Type class constraints on instance declarations</a>, Up: <a href="Type-classes.html#Type-classes" accesskey="u" rel="up">Type classes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
