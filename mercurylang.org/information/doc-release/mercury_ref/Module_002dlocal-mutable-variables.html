<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Module-local mutable variables</title>

<meta name="description" content="The Mercury Language Reference Manual: Module-local mutable variables">
<meta name="keywords" content="The Mercury Language Reference Manual: Module-local mutable variables">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Modules.html#Modules" rel="up" title="Modules">
<link href="Type-classes.html#Type-classes" rel="next" title="Type classes">
<link href="Module-finalisation.html#Module-finalisation" rel="previous" title="Module finalisation">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Module_002dlocal-mutable-variables"></a>
<div class="header">
<p>
Previous: <a href="Module-finalisation.html#Module-finalisation" accesskey="p" rel="previous">Module finalisation</a>, Up: <a href="Modules.html#Modules" accesskey="u" rel="up">Modules</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Module_002dlocal-mutable-variables-1"></a>
<h3 class="section">9.6 Module-local mutable variables</h3>

<p>Certain special cases require a module to have
one or more mutable (i.e. destructively updateable) variables,
for example to hold the constraint store for a solver type.
</p>
<p>A mutable variable is declared using the &lsquo;<samp>mutable</samp>&rsquo; directive:
</p>
<div class="example">
<pre class="example">:- mutable(<var>varname</var>, <var>vartype</var>, <var>initial_value</var>, <var>varinst</var>, [<var>attribute</var>, &hellip;]).
</pre></div>

<p>This constructs a new mutable variable with access predicates
that have the following signatures:
</p>
<div class="example">
<pre class="example">:- semipure pred get_<var>varname</var>(<var>vartype</var>::out(<var>varinst</var>)) is det.
:- impure   pred set_<var>varname</var>(<var>vartype</var>::in(<var>varinst</var>)) is det.
</pre></div>

<p>The initial value of <var>varname</var> is <var>initial_value</var>,
which is set before the program&rsquo;s &lsquo;<samp>main/2</samp>&rsquo; predicate is executed.
</p>
<p>The type <var>vartype</var> is not allowed
to contain any type variables or have any type class constraints.
</p>
<p>The inst <var>varinst</var> is not allowed to contain any inst variables.
It is also not allowed to be equivalent to,
or contain components that are equivalent to,
the builtin insts <code>free</code>, <code>unique</code>, <code>mostly_unique</code>,
<code>dead</code> (<code>clobbered</code>)
or <code>mostly_dead</code> (<code>mostly_clobbered</code>).
</p>
<p>The initial value of a mutable, <var>initial_value</var>,
may be any Mercury expression with type <var>vartype</var> and inst <var>varinst</var>
subject to the above restrictions.
It may be impure or semipure.
</p>
<p>The following <var>attributes</var> must be supported:
</p>
<dl compact="compact">
<dt>&lsquo;<samp>trailed</samp>&rsquo;/&lsquo;<samp>untrailed</samp>&rsquo;</dt>
<dd><p>This attribute specifies whether
the implementation should generate code
to undo the effects of &lsquo;<samp>set_<var>varname</var>/1</samp>&rsquo; on backtracking
(&lsquo;<samp>trailed</samp>&rsquo;) or not (&lsquo;<samp>untrailed</samp>&rsquo;).
The default, in case none is specified, is &lsquo;<samp>trailed</samp>&rsquo;.
</p>
</dd>
<dt>&lsquo;<samp>attach_to_io_state</samp>&rsquo;</dt>
<dd><p>This attribute causes the compiler
to also construct access predicates that have the following signatures:
</p>
<div class="example">
<pre class="example">:- pred get_<var>varname</var>(<var>vartype</var>::out(<var>varinst</var>), io::di, io::uo) is det.
:- pred set_<var>varname</var>(<var>vartype</var>::in(<var>varinst</var>),  io::di, io::uo) is det.
</pre></div>

</dd>
<dt>&lsquo;<samp>constant</samp>&rsquo;</dt>
<dd><p>This attribute causes the compiler to construct
only a &lsquo;<samp>get</samp>&rsquo; access predicate, but not a &lsquo;<samp>set</samp>&rsquo; access predicate.
Since <var>varname</var> will always have the initial value given to it,
the &lsquo;<samp>get</samp>&rsquo; access predicate is pure; its signature will be:
</p>
<div class="example">
<pre class="example">:- pred get_<var>varname</var>(<var>vartype</var>::out(<var>varinst</var>)) is det.
</pre></div>

<p>The &lsquo;<samp>constant</samp>&rsquo; attribute cannot be specified together with
the &lsquo;<samp>attach_to_io_state</samp>&rsquo; attribute
(since they disagree on this signature).
It also cannot be specified together with an explicit &lsquo;<samp>trailed</samp>&rsquo; attribute.
</p>
</dd>
</dl>

<p>The Melbourne Mercury compiler also supports the following attributes:
</p>
<dl compact="compact">
<dt>&lsquo;<samp>foreign_name(<var>Lang</var>, <var>Name</var>)</samp>&rsquo;</dt>
<dd><p>Allow foreign code to access the mutable variable
in some implementation dependent manner.
<var>Lang</var> must be a valid target language for this Mercury implementation.
<var>Name</var> must be a valid identifier in that language.
It is an error to specify
more than one foreign name attribute for each language.
</p>
<p>For the C backends,
this attribute allows foreign code to access the mutable variable
as an external variable called <var>Name</var>.
For the low-level C backend, e.g. the asm_fast grades,
the type of this variable will be <code>MR_Word</code>.
For the high-level C backend, e.g. the hlc grades,
the type of this variable depends upon the Mercury type of the mutable.
For mutables of a Mercury primitive type,
the corresponding C type is given
by the mapping in <a href="C-data-passing-conventions.html#C-data-passing-conventions">C data passing conventions</a>.
For mutables of any other type,
the corresponding C type will be <code>MR_Word</code>.
</p>
<p>This attribute is not currently implemented for the non-C backends.
</p>
</dd>
<dt>&lsquo;<samp>thread_local</samp>&rsquo;</dt>
<dd><p>This attribute allows a mutable to take on different values in each thread.
When a child thread is spawned,
it inherits all the values of thread-local mutables of the parent thread.
Changing the value of a thread-local mutable
does not affect its value in any other threads.
</p>
<p>The &lsquo;<samp>thread_local</samp>&rsquo; attribute cannot be specified
together with either of the &lsquo;<samp>trailed</samp>&rsquo; or &lsquo;<samp>constant</samp>&rsquo; attributes.
</p>
</dd>
</dl>

<p>It is an error for a &lsquo;<samp>mutable</samp>&rsquo; directive
to appear in the interface section of a module.
The usual visibility rules for submodules
apply to the mutable variable access predicates.
</p>
<p>For the purposes of determining
when mutables are assigned their initial values,
the expression <var>initial_value</var> behaves
as though it were a predicate specified in an &lsquo;<samp>initialise</samp>&rsquo; directive.
</p>
<div class="example">
<pre class="example">:- initialise foo/2.
:- mutable(bar, int, 561, ground, [untrailed]).
:- initialise baz/2.
</pre></div>

<p>In the above example,
</p>
<ul>
<li> &lsquo;<samp>foo/2</samp>&rsquo; will be invoked first,
</li><li> then &lsquo;<samp>bar</samp>&rsquo; will be set to its initial value of 561,
</li><li> and then &lsquo;<samp>baz/2</samp>&rsquo; will be invoked.
</li></ul>

<p>The effect of a mutable initial value expression
terminating with an uncaught exception
is also the same as though it were
a predicate specified in an &lsquo;<samp>initialise</samp>&rsquo; directive.
</p>
<hr>
<div class="header">
<p>
Previous: <a href="Module-finalisation.html#Module-finalisation" accesskey="p" rel="previous">Module finalisation</a>, Up: <a href="Modules.html#Modules" accesskey="u" rel="up">Modules</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
