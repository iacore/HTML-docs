<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Existentially typed data types</title>

<meta name="description" content="The Mercury Language Reference Manual: Existentially typed data types">
<meta name="keywords" content="The Mercury Language Reference Manual: Existentially typed data types">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Existential-types.html#Existential-types" rel="up" title="Existential types">
<link href="Some-idioms-using-existentially-quantified-types.html#Some-idioms-using-existentially-quantified-types" rel="next" title="Some idioms using existentially quantified types">
<link href="Existential-class-constraints.html#Existential-class-constraints" rel="previous" title="Existential class constraints">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Existentially-typed-data-types"></a>
<div class="header">
<p>
Next: <a href="Some-idioms-using-existentially-quantified-types.html#Some-idioms-using-existentially-quantified-types" accesskey="n" rel="next">Some idioms using existentially quantified types</a>, Previous: <a href="Existential-class-constraints.html#Existential-class-constraints" accesskey="p" rel="previous">Existential class constraints</a>, Up: <a href="Existential-types.html#Existential-types" accesskey="u" rel="up">Existential types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Existentially-typed-data-types-1"></a>
<h3 class="section">11.3 Existentially typed data types</h3>

<p>Type variables occurring in the body of a discriminated union type
definition may be existentially quantified.
Constructor definitions within discriminated union type definitions
may be preceded by an existential type quantifier
and followed by one or more existential type class constraints.
</p>
<p>For example:
</p>
<div class="example">
<pre class="example">% A simple heterogeneous list type.
:- type list_of_any
    ---&gt;    nil_any
    ;       some [T] cons_any(T, list_of_any).

% A heterogeneous list type with a type class constraint.
:- typeclass showable(T) where [ func show(T) = string ].
:- type showable_list
    ---&gt;    nil
    ;       some [T] (cons(T, showable_list) =&gt; showable(T)).

% A different way of doing the same kind of thing, this
% time using the standard type list(T).
:- type showable
    ---&gt;    some [T] (s(T) =&gt; showable(T)).
:- type list_of_showable == list(showable).

% Here is an arbitrary example involving multiple type variables
% and multiple constraints.
:- typeclass foo(T1, T2) where [ /* &hellip; */ ].
:- type bar(T)
    ---&gt;    f1
    ;       f2(T)
    ;       some [T1] f3(T1)
    ;       some [T1, T2] f4(T1, T2, T) =&gt; (showable(T1), showable(T2))
    ;       some [T1, T2] f5(list(T1), T2) =&gt; fooable(T1, T2).
</pre></div>

<p>Construction and deconstruction of existentially quantified data types
are inverses:
when constructing a value of an existentially quantified data type,
the &ldquo;existentially quantified&rdquo; functor acts
for purposes of type checking like a universally quantified function:
the caller will determine the values of the type variables.
Conversely, for deconstruction the functor acts
like an existentially quantified function:
the caller must be defined so as to work
for all possible values of the existentially quantified type variables
which satisfy the declared type class constraints.
</p>
<p>In order to make this distinction clear to the compiler,
whenever you want to construct a value
using an existentially quantified functor,
you must prepend &lsquo;<samp>new </samp>&rsquo; onto the functor name.
This tells the compiler to treat it as though it were universally quantified:
the caller can bind that functor&rsquo;s existentially quantified type variables
to any type which satisfies the declared type class constraints.
Conversely, any occurrence without the &lsquo;<samp>new </samp>&rsquo; prefix
must be a deconstruction, and is therefore existentially quantified:
the caller must not bind the existentially quantified type variables,
but the caller is allowed to depend on those type variables
satisfying the declared type class constraints, if any.
</p>
<p>For example, the function &lsquo;<samp>make_list</samp>&rsquo; constructs
a value of type &lsquo;<samp>showable_list</samp>&rsquo;
containing a sequence of values of different types,
all of which are instances of the &lsquo;<samp>showable</samp>&rsquo; class
</p>
<div class="example">
<pre class="example">:- instance showable(int).
:- instance showable(float).
:- instance showable(string).

:- func make_list = showable_list.
make_list = List :-
	Int = 42,
	Float = 1.0,
	String = &quot;blah&quot;,
	List =  'new cons'(Int,
		'new cons'(Float,
		'new cons'(String, nil))).
</pre></div>

<p>while the function &lsquo;<samp>process_list</samp>&rsquo; below
applies the &lsquo;<samp>show</samp>&rsquo; method of the &lsquo;<samp>showable</samp>&rsquo; class
to the values in such a list.
</p>
<div class="example">
<pre class="example">:- func process_list(showable_list) = list(string).
process_list(nil) = &quot;&quot;.
process_list(cons(Head, Tail)) = [show(Head) | process_list(Tail)].
</pre></div>

<p>There are some restrictions on the forms that
existentially typed data constructors can take.
</p>
<p>One restriction is that no type variable may be quantified
both universally, by being listed as an argument of the type constructor,
and existentially, by being listed in the existential type quantifier
before the data constructor.
The type &lsquo;<samp>t12</samp>&rsquo; violates this restriction:
</p>
<div class="example">
<pre class="example">:- type t12(T)
    ---&gt;    f1(T)
    ;       some [T] f2(T).
</pre></div>

<p>The reason for the restriction is simple:
without it, how can one decide whether
the argument of &lsquo;<samp>f2</samp>&rsquo; is universally or existentially quantified?
</p>
<p>The other restriction is that every existentially quantified type variable
must occur
</p><ul>
<li> either in one of the argument types of the data constructor,
</li><li> or in one of the type class constraints on the data constructor,
in the range of a functional dependency.
</li></ul>

<p>This means that the type &lsquo;<samp>t3</samp>&rsquo; in
</p><div class="example">
<pre class="example">:- type t3
    ---&gt;    some [T1, T2] f5(T1) =&gt; xable(T1, T2).
</pre></div>
<p>violates this restriction
<em>unless</em> the type class &lsquo;<samp>xable</samp>&rsquo; has a functional dependency
that determines the type bound to its second argument
from the type bound to its first.
</p>
<p>The reason for this restriction is that
the identity of the type bound to the existential type variable
must somehow be decided at runtime.
It can either be given by the type of an argument,
or determined through a functional dependency
from the types bound to one or more other existential type variables.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Some-idioms-using-existentially-quantified-types.html#Some-idioms-using-existentially-quantified-types" accesskey="n" rel="next">Some idioms using existentially quantified types</a>, Previous: <a href="Existential-class-constraints.html#Existential-class-constraints" accesskey="p" rel="previous">Existential class constraints</a>, Up: <a href="Existential-types.html#Existential-types" accesskey="u" rel="up">Existential types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
