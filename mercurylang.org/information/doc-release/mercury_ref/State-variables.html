<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: State variables</title>

<meta name="description" content="The Mercury Language Reference Manual: State variables">
<meta name="keywords" content="The Mercury Language Reference Manual: State variables">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Syntax.html#Syntax" rel="up" title="Syntax">
<link href="DCG_002drules.html#DCG_002drules" rel="next" title="DCG-rules">
<link href="Goals.html#Goals" rel="previous" title="Goals">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="State-variables"></a>
<div class="header">
<p>
Next: <a href="DCG_002drules.html#DCG_002drules" accesskey="n" rel="next">DCG-rules</a>, Previous: <a href="Goals.html#Goals" accesskey="p" rel="previous">Goals</a>, Up: <a href="Syntax.html#Syntax" accesskey="u" rel="up">Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="State-variables-1"></a>
<h3 class="section">2.12 State variables</h3>

<p>Clauses may use &lsquo;<samp>state variables</samp>&rsquo;
as a shorthand for naming intermediate values in a sequence.
That is, where in the plain syntax one might write
</p><div class="example">
<pre class="example">    main(IO0, IO) :-
        io.write_string(&quot;The answer is &quot;, IO0, IO1),
        io.write_int(calculate_answer(&hellip;), IO1, IO2),
        io.nl(IO3, IO).
</pre></div>
<p>using state variable syntax one could write
</p><div class="example">
<pre class="example">    main(!IO) :-
        io.write_string(&quot;The answer is &quot;, !IO),
        io.write_int(calculate_answer(&hellip;), !IO),
        io.nl(!IO).
</pre></div>

<p>A state variable is written &lsquo;<samp>!.<var>X</var></samp>&rsquo; or &lsquo;<samp>!:<var>X</var></samp>&rsquo;,
denoting the &ldquo;current&rdquo; or &ldquo;next&rdquo; value of the sequence labelled <var>X</var>.
An argument &lsquo;<samp>!<var>X</var></samp>&rsquo; is shorthand
for two state variable arguments &lsquo;<samp>!.<var>X</var>, !:<var>X</var></samp>&rsquo;;
that is,
&lsquo;<samp>p(&hellip;, !<var>X</var>, &hellip;)</samp>&rsquo;
is parsed as
&lsquo;<samp>p(&hellip;, !.<var>X</var>, !:<var>X</var>, &hellip;)</samp>&rsquo;.
</p>
<p>State variables obey special scope rules.
A state variable <var>X</var> must be explicitly introduced,
and this can happen in one of four ways:
</p><ul>
<li> As &lsquo;<samp>!<var>X</var></samp>&rsquo; in the head of a clause.
In this case,
references to state variable &lsquo;<samp>!<var>X</var></samp>&rsquo; or to its components
may appear anywhere in the clause.
</li><li> As either &lsquo;<samp>!.<var>X</var></samp>&rsquo; or &lsquo;<samp>!:<var>X</var></samp>&rsquo; or both
in the head of a clause.
Again, in this case,
references to state variable &lsquo;<samp>!<var>X</var></samp>&rsquo; or to its components
may appear anywhere in the clause.
</li><li> As either &lsquo;<samp>!.<var>X</var></samp>&rsquo; or &lsquo;<samp>!:<var>X</var></samp>&rsquo; or both
in the head of a lambda expression.
In this case,
references to state variable &lsquo;<samp>!<var>X</var></samp>&rsquo; or to its components
may appear anywhere in that lambda expression.
(The reason that &lsquo;<samp>!<var>X</var></samp>&rsquo; may not appear as a parameter term
in the head of a lambda is that
there is no syntax for specifying the modes of the two implied parameters.)
</li><li> In an explicit quantification such as &lsquo;<samp>some [!<var>X</var>] <var>Goal</var></samp>&rsquo;.
In this case,
references to state variable &lsquo;<samp>!<var>X</var></samp>&rsquo; or to its components
may appear anywhere in &lsquo;<samp><var>Goal</var></samp>&rsquo;.
</li></ul>

<p>A state variable <var>X</var> in the enclosing scope
of a lambda or if-then-else expression
may only be referred to as &lsquo;<samp>!.<var>X</var></samp>&rsquo;
(unless the enclosing <var>X</var> is shadowed
by a more local state variable of the same name.)
</p>
<p>For instance, the following clause employing a lambda expression
</p><div class="example">
<pre class="example">p(<var>A</var>, <var>B</var>, !<var>S</var>) :-
    F =
        ( pred(<var>C</var>::in, <var>D</var>::out) is det :-
            q(<var>C</var>, <var>D</var>, !<var>S</var>)
        ),
    ( if F(<var>A</var>, <var>E</var>) then
        <var>B</var> = <var>E</var>
    else
        <var>B</var> = <var>A</var>
    ).
</pre></div>
<p>is illegal because
it implicitly refers to &lsquo;<samp>!:<var>S</var></samp>&rsquo; inside the lambda expression.
However
</p><div class="example">
<pre class="example">p(<var>A</var>, <var>B</var>, !<var>S</var>) :-
    F =
        ( pred(<var>C</var>::in, <var>D</var>::out, !.<var>S</var>::in, !:<var>S</var>::out) is det :-
            q(<var>C</var>, <var>D</var>, !<var>S</var>)
        ),
    ( if F(<var>A</var>, <var>E</var>, !<var>S</var>) then
        <var>B</var> = <var>E</var>
    else
        <var>B</var> = <var>A</var>
    ).
</pre></div>
<p>is acceptable because
the state variable <var>S</var> accessed inside the lambda expression
is locally scoped to the lambda expression
(shadowing the state variable of the same name outside the lambda expression),
and the lambda expression may refer to
the next version of a local state variable.
</p>
<p>There are two restrictions concerning state variables in functions,
whether they are defined by clauses or lambda expressions.
</p><ul>
<li> &lsquo;<samp>!<var>X</var></samp>&rsquo; is not a legal function result,
because it stands for two arguments, rather than one.
</li><li> &lsquo;<samp>!<var>X</var></samp>&rsquo; may not appear as an argument in a function application,
because this would not make sense
given the usual interpretation of state variables and functions.
(The default mode of functions is that all arguments are input,
while in the overwhelming majority of cases, &lsquo;<samp>!:<var>X</var></samp>&rsquo; is output.)
</li></ul>

<p>Within each clause, the compiler
replaces each occurrence of !<var>X</var> in an argument list
with two arguments: !.<var>X</var>, !:<var>X</var>,
where !.<var>X</var> represents the current version of the state of !<var>X</var>,
and !:<var>X</var> represents its next state.
It then replaces all occurrences of !.<var>X</var> and !:<var>X</var>
with ordinary variables in way that (in the general case)
represents a sequence of updates to the state of <var>X</var>
from an initial state to a final state.
</p>
<p>This replacement is done by code that is equivalent to
the &lsquo;<samp>transform_goal</samp>&rsquo; and &lsquo;<samp>transform_clause</samp>&rsquo; functions below.
The basic operation used by these functions is substitution:
&lsquo;<samp>substitute(<var>Goal</var>,
[!.<var>X</var> -&gt; <var>CurX</var>, !:<var>X</var> -&gt; <var>NextX</var>])</samp>&rsquo;
stands for a copy of <var>Goal</var> in which
every free occurrence of &lsquo;<samp>!.<var>X</var></samp>&rsquo; is replaced with <var>CurX</var>, and
every free occurrence of &lsquo;<samp>!:<var>X</var></samp>&rsquo; is replaced with <var>NextX</var>.
(A free occurrence is one not bound
by the head of a clause or lambda, or by an explicit quantification.)
</p>
<p>The &lsquo;<samp>transform_goal(<var>Goal</var>, <var>X</var>, <var>CurX</var>, <var>NextX</var>)</samp>&rsquo;
function&rsquo;s inputs are
</p><ul>
<li> the goal to transform <var>Goal</var>,
</li><li> the name of the state variable <var>X</var>,
</li><li> and the ordinary variables <var>CurX</var> and <var>NextX</var>
representing the current and next versions of that state variable.
</li></ul>
<p>It returns a transformed version of <var>Goal</var>.
</p>
<p>&lsquo;<samp>transform_goal</samp>&rsquo; has a case for each kind of Mercury goal.
These cases are as follows.
</p>
<dl compact="compact">
<dt><code>Calls</code></dt>
<dd><p>Given a first order call such as
&lsquo;<samp><var>predname</var>(<var>ArgTerm1</var>, ..., <var>ArgTermN</var>)</samp>&rsquo;
or a higher-order call such as
&lsquo;<samp><var>PredVar</var>(<var>ArgTerm1</var>, ..., <var>ArgTermN</var>)</samp>&rsquo;,
if any of the <var>ArgTermI</var> is !<var>X</var>,
&lsquo;<samp>transform_goal</samp>&rsquo; replaces that argument with two arguments:
!.<var>X</var> and !:<var>X</var>.
It then checks whether
&lsquo;<samp>!:<var>X</var></samp>&rsquo; appears in the updated <var>Call</var>.
</p><ul>
<li> If it does, then it replaces <var>Call</var> with
<div class="example">
<pre class="example">substitute(<var>Call</var>, [!.<var>X</var> -&gt; <var>CurX</var>, !:<var>X</var> -&gt; <var>NextX</var>])
</pre></div>
</li><li> If it does not, then it replaces <var>AtomicGoal</var> with
<div class="example">
<pre class="example">substitute(<var>Call</var>, [!.<var>X</var> -&gt; <var>CurX</var>]),
<var>NextX</var> = <var>CurX</var>
</pre></div>
</li></ul>
<p>Note that !.<var>X</var> can occur in <var>Call</var> on its own
(i.e. without !:<var>X</var>).
Likewise, !:<var>X</var> can occur in <var>Call</var> without !.<var>X</var>,
but this does not need separate handling.
</p>
<p>Note that <var>PredVar</var> in a higher-order call
may not be a reference to a state var,
i.e it may not be !<var>X</var>, !.<var>X</var> or !:<var>X</var>.
</p>
</dd>
<dt><code>Unifications</code></dt>
<dd><p>In a unification &lsquo;<samp><var>TermA</var> = <var>TermB</var></samp>&rsquo;,
each of <var>TermA</var> and <var>TermB</var>
may have one of the following four forms:
</p><ul>
<li> The term may be !.<var>S</var> for some state variable <var>S</var>.
If <var>S</var> is <var>X</var>,
then &lsquo;<samp>transform_goal</samp>&rsquo; replaces the term with <var>CurX</var>.
</li><li> The term may be !:<var>S</var> for some state variable <var>S</var>.
If <var>S</var> is <var>X</var>,
then &lsquo;<samp>transform_goal</samp>&rsquo; replaces the term with <var>NextX</var>.
</li><li> The term may be a plain variable
(i.e. variable that does not refer to any state variable).
&lsquo;<samp>transform_goal</samp>&rsquo; leaves such terms unchanged.
</li><li> The term may be a non-variable term, which means that
it must have the form
&lsquo;<samp><var>f</var>(<var>ArgTerm1</var>, ..., <var>ArgTermN</var>)</samp>&rsquo;.
&lsquo;<samp>transform_goal</samp>&rsquo; handles these the same way it handles first order calls.
</li></ul>
<p>Note that <var>TermA</var> and <var>TermB</var> may not have the form !<var>S</var>.
</p>
</dd>
<dt><code>State variable field updates</code></dt>
<dd><p>A state variable field update goal has the form
</p><div class="example">
<pre class="example">!<var>S</var> ^ <var>field_list</var> := <var>Term</var>
</pre></div>
<p>where <var>field_list</var> is a valid field list See <a href="Record-syntax.html#Record-syntax">Record syntax</a>.
This means that
</p><div class="example">
<pre class="example">!<var>S</var> ^ <var>field1</var> := <var>Term</var>
!<var>S</var> ^ <var>field1</var> ^ <var>field2</var> := <var>Term</var>
!<var>S</var> ^ <var>field1</var> ^ <var>field2</var> ^ <var>field3</var> := <var>Term</var>
</pre></div>
<p>are all valid field update goals.
If <var>S</var> is <var>X</var>,
&lsquo;<samp>transform_goal</samp>&rsquo; replaces such goals with
</p><div class="example">
<pre class="example"><var>NextX</var> = <var>CurX</var> ^ <var>field_list</var> := <var>Term</var>
</pre></div>
<p>Otherwise, it leaves the goal unchanged.
</p>
</dd>
<dt><code>Conjunctions</code></dt>
<dd><p>Given a nonempty conjunction,
whether a sequential conjunction such as
<var>Goal1</var>, <var>Goal2</var>
or a parallel conjunction such as <var>Goal1</var> &amp; <var>Goal2</var>,
&lsquo;<samp>transform_goal</samp>&rsquo;
</p><ul>
<li> creates a fresh variable <var>MidX</var>,
</li><li> replaces <var>Goal1</var> with
<div class="example">
<pre class="example">substitute(<var>Goal1</var>, [!.<var>X</var> -&gt; <var>CurX</var>, !:<var>X</var> -&gt; <var>MidX</var>])
</pre></div>
</li><li> replaces <var>Goal2</var> with
<div class="example">
<pre class="example">substitute(<var>Goal2</var>, [!.<var>X</var> -&gt; <var>MidX</var>, !:<var>X</var> -&gt; <var>NextX</var>])
</pre></div>
</li></ul>
<p>This implies that first <var>Goal1</var>
updates the state of <var>X</var> from <var>CurX</var> to <var>MidX</var>,
and then <var>Goal2</var>
updates the state of <var>X</var> from <var>MidX</var> to <var>NextX</var>.
</p>
<p>Given the empty conjunction, i.e. the goal &lsquo;<samp>true</samp>&rsquo;,
&lsquo;<samp>transform_goal</samp>&rsquo; will replace it with
</p><div class="example">
<pre class="example"><var>NextX</var> = <var>CurX</var>
</pre></div>

</dd>
<dt><code>Disjunctions</code></dt>
<dd><p>Given a disjunction such as <var>Goal1</var> ; <var>Goal2</var>,
&lsquo;<samp>transform_goal</samp>&rsquo;
</p><ul>
<li> replaces <var>Goal1</var> with
<div class="example">
<pre class="example">substitute(<var>Goal1</var>, [!.<var>X</var> -&gt; <var>CurX</var>, !:<var>X</var> -&gt; <var>NextX</var>])
</pre></div>
</li><li> replaces <var>Goal2</var> with
<div class="example">
<pre class="example">substitute(<var>Goal2</var>, [!.<var>X</var> -&gt; <var>CurX</var>, !:<var>X</var> -&gt; <var>NextX</var>])
</pre></div>
</li></ul>
<p>This shows that both disjuncts start with the <var>CurX</var>,
and both end with <var>NextX</var>.
If a disjunct has no update of !<var>X</var>,
then the value of <var>NextX</var> in that disjunct will be set to <var>CurX</var>.
</p>
<p>The empty disjunction, i.e. the goal &lsquo;<samp>fail</samp>&rsquo;, cannot succeed,
so what the value of !:<var>X</var> would be if it <em>did</em> succeed is moot.
Therefore &lsquo;<samp>transform_goal</samp>&rsquo; returns empty disjunctions unchanged.
</p>
</dd>
<dt><code>Negations</code></dt>
<dd><p>Given a negated goal of the form &lsquo;<samp>not <var>NegatedGoal</var></samp>&rsquo;
&lsquo;<samp>transform_goal</samp>&rsquo;
</p><ul>
<li> creates a fresh variable <var>DummyX</var>, and then
</li><li> replaces &lsquo;<samp>not <var>NegatedGoal</var></samp>&rsquo; with
<div class="example">
<pre class="example">&lsquo;<samp>not</samp>&rsquo; substitute(&lsquo;<samp>NegatedGoal</samp>&rsquo;, [!.<var>X</var> -&gt; <var>CurX</var>, !:<var>X</var> -&gt; <var>DummyX</var>]),
<var>NextX</var> = <var>CurX</var>
</pre></div>
</li></ul>
<p>It does this because negated goals
may not generate any outputs visible from the rest of the code,
which means that any output they <em>do</em> generate
must be local to the negated goal.
</p>
<p>Negations that use &lsquo;<samp>\+ <var>NegatedGoal</var></samp>&rsquo; notation
are handled exactly the same way,
</p>
</dd>
<dt><code>If-then-elses</code></dt>
<dd><p>Given an if-then-else, whether it uses
( if <var>Cond</var> then <var>Then</var> else <var>Else</var> ) syntax or
( <var>Cond</var> -&gt; <var>Then</var> ; <var>Else</var> ) syntax,
&lsquo;<samp>transform_goal</samp>&rsquo;
</p><ul>
<li> creates a fresh variable <var>MidX</var>,
</li><li> replaces <var>Cond</var> with
<div class="example">
<pre class="example">substitute(<var>Cond</var>, [!.<var>X</var> -&gt; <var>CurX</var>, !:<var>X</var> -&gt; <var>MidX</var>])
</pre></div>
</li><li> replaces <var>Then</var> with
<div class="example">
<pre class="example">substitute(<var>Then</var>, [!.<var>X</var> -&gt; <var>MidX</var>, !:<var>X</var> -&gt; <var>NextX</var>])
</pre></div>
</li><li> replaces <var>Else</var> with
<div class="example">
<pre class="example">substitute(<var>Else</var>, [!.<var>X</var> -&gt; <var>CurX</var>, !:<var>X</var> -&gt; <var>NextX</var>])
</pre></div>
</li></ul>
<p>This effectively treats an if-then-else as being a disjunction,
with the first disjunct being
the conjunction of the <var>Cond</var> and <var>Then</var> goals,
and the second disjunct being the <var>Else</var> goal.
(The <var>Else</var> goal is implicitly conjoined inside the second disjunct
with the negation of the existential closure of <var>Cond</var>,
since the else case is executed only if the condition has no solution.)
</p>
</dd>
<dt><code>Bidirectional implications</code></dt>
<dd><p>&lsquo;<samp>transform_goal</samp>&rsquo; treats a bidirectional implication goal,
which has the form <var>GoalA</var> &lt;=&gt; <var>GoalB</var>,
as it were the conjunction of its two constituent unidirectional implications:
<var>GoalA</var> =&gt; <var>GoalB</var>, <var>GoalA</var> &lt;= <var>GoalB</var>.
</p>
</dd>
<dt><code>Unidirectional implications</code></dt>
<dd><p>&lsquo;<samp>transform_goal</samp>&rsquo; treats a unidirectional implication,
which has one of the two forms
&lsquo;<samp><var>GoalA</var> =&gt; <var>GoalB</var></samp>&rsquo; and &lsquo;<samp><var>GoalB</var> &lt;= <var>GoalA</var></samp>&rsquo;,
as if they were written as
&lsquo;<samp>not (<var>GoalA</var>, not <var>GoalB</var>)</samp>&rsquo;.
</p>
</dd>
<dt><code>Universal quantifications</code></dt>
<dd><p>&lsquo;<samp>transform_goal</samp>&rsquo; treats universal quantifications,
which have the form &lsquo;<samp>all <var>Vars</var> <var>SubGoal</var></samp>&rsquo;
as if they were written as
&lsquo;<samp>not (some <var>Vars</var> (not <var>SubGoal</var>))</samp>&rsquo;.
Note that in universal quantifications,
<var>Vars</var> must a list of ordinary variables.
</p>
</dd>
<dt><code>Existential quantifications</code></dt>
<dd><p>In existential quantifications,
which have the form &lsquo;<samp>some <var>Vars</var> <var>SubGoal</var></samp>&rsquo;,
<var>Vars</var> must be a list, in which every element
must be either ordinary variable (such as <var>A</var>),
or a state variable (such as !<var>B</var>).
(Note that <var>Vars</var> may not contain
any element whose form is !.<var>B</var> or !:<var>B</var>.)
</p><ul>
<li> If <var>Vars</var> does not contain !<var>X</var>,
then &lsquo;<samp>transform_goal</samp>&rsquo; will replace <var>SubGoal</var> with
<div class="example">
<pre class="example">substitute(<var>SubGoal</var>, [!.<var>X</var> -&gt; <var>CurX</var>, !:<var>X</var> -&gt; <var>NextX</var>])
</pre></div>
</li><li> If <var>Vars</var> does contain !<var>X</var>,
then &lsquo;<samp>transform_goal</samp>&rsquo; will leave <var>SubGoal</var> unchanged, because
any references to !.<var>X</var>, !:<var>X</var> and !<var>X</var> inside <var>SubGoal</var>
refer to the state variable <var>X</var> introduced by this scope,
not the one visible outside.
Effectively, this state variable <var>X</var>
<em>shadows</em> the one visible outside.
</li></ul>
<p>Note that state variables in <var>Vars</var>
are handled by &lsquo;<samp>transform_clause</samp>&rsquo; below.
</p>
</dd>
</dl>

<p>The &lsquo;<samp>transform_clause</samp>&rsquo; function&rsquo;s input is a clause,
which may be a non-DCG clause or a DCG clause, which have the forms
</p><div class="example">
<pre class="example"><var>predname</var>(<var>ArgTerm1</var>, ..., <var>ArgTermN</var>) :- <var>BodyGoal</var>.
</pre></div>
<p>and
</p><div class="example">
<pre class="example"><var>predname</var>(<var>ArgTerm1</var>, ..., <var>ArgTermN</var>) --&gt; <var>BodyGoal</var>.
</pre></div>
<p>respectively.
&lsquo;<samp>transform_clause</samp>&rsquo; handles both the same way.
</p>
<ul>
<li> While any of the <var>ArgTerms</var>
has one of the forms !.<var>X</var>, !:<var>X</var> and !<var>X</var>,
<ul>
<li> &lsquo;<samp>transform_clause</samp>&rsquo; will create two fresh variables,
<var>InitX</var> and <var>FinalX</var>,
</li><li> it will replace
any one of the <var>ArgTerms</var> that is !.<var>X</var> with <var>InitX</var>,
any one of the <var>ArgTerms</var> that is !:<var>X</var> with <var>FinalX</var>, and
any one of the <var>ArgTerms</var> that is !<var>X</var> with
the argument pair <var>InitX</var>, <var>FinalX</var>, and
</li><li> it will replace <var>BodyGoal</var> with the result of
&lsquo;<samp>transform_goal(<var>BodyGoal</var>, <var>X</var>, <var>InitX</var>, <var>FinalX</var>)</samp>&rsquo;.
</li></ul>
</li><li> While <var>BodyGoal</var> contains a lambda expression
whose argument list contains either !.<var>X</var> or !:<var>X</var> or both:
<ul>
<li> &lsquo;<samp>transform_clause</samp>&rsquo; will create two fresh variables,
<var>InitX</var> and <var>FinalX</var>,
</li><li> it will replace
any one of the arguments that is !.<var>X</var> with <var>InitX</var>, and
any one of the arguments that is !:<var>X</var> with <var>FinalX</var>
(there may not be any argument that is !<var>X</var>), and
</li><li> it will replace the lambda goal <var>BodyGoal</var> with the result of
&lsquo;<samp>transform_goal(<var>BodyGoal</var>, <var>X</var>, <var>InitX</var>, <var>FinalX</var>)</samp>&rsquo;.
</li></ul>
</li><li> While <var>BodyGoal</var> contains an existential quantification goal
&lsquo;<samp>some <var>Vars</var> <var>SubGoal</var></samp>&rsquo;
where <var>Vars</var> contains a state variable such as !<var>B</var>,
<ul>
<li> &lsquo;<samp>transform_clause</samp>&rsquo; will create two fresh variables,
<var>InitB</var> and <var>FinalB</var>,
</li><li> it will replace <var>SubGoal</var> with the result of
&lsquo;<samp>transform_goal(<var>SubGoal</var>, <var>B</var>, <var>InitB</var>, <var>FinalB</var>)</samp>&rsquo;,
and then
</li><li> it will delete !<var>B</var> from <var>Vars</var>.
</li></ul>
</li></ul>

<p>Actual application of this transformation would, in the general case,
result in the generation of many different versions of each state variable,
for which we need more names than just
&lsquo;<samp><var>CurX</var></samp>&rsquo;, &lsquo;<samp><var>TmpX</var></samp>&rsquo; and &lsquo;<samp><var>NextX</var></samp>&rsquo;.
The Mercury compiler therefore uses
</p><ul>
<li> &lsquo;<samp><var>STATE_VARIABLE_X_0</var></samp>&rsquo; as the initial value of a state variable,
</li><li> &lsquo;<samp><var>STATE_VARIABLE_X_N</var></samp>&rsquo;,
where &lsquo;<samp><var>N</var></samp>&rsquo; is a nonzero positive integer,
as its intermediate values, and
</li><li> &lsquo;<samp><var>STATE_VARIABLE_X</var></samp>&rsquo; as its final value.
</li></ul>

<p>This transformation can lead
to the introduction of chains of unifications for variables
that do not otherwise play a role in the definition,
such as
&lsquo;<samp><var>STATE_VARIABLE_X_5</var> = <var>STATE_VARIABLE_X_6</var>,
<var>STATE_VARIABLE_X_6</var> = <var>STATE_VARIABLE_X_7</var>,
<var>STATE_VARIABLE_X_7</var> = <var>STATE_VARIABLE_X_8</var></samp>&rsquo;.
Where possible, the compiler automatically shortcircuits such sequences
by removing any unneeded intermediate variables.
In the above case, this would yield
&lsquo;<samp><var>STATE_VARIABLE_X_5</var> = <var>STATE_VARIABLE_X_8</var></samp>&rsquo;.
</p>
<p>The following code fragments illustrate
some appropriate uses of state variable syntax.
</p>
<dl compact="compact">
<dt><b>Threading the I/O state</b></dt>
<dd><div class="example">
<pre class="example">main(!IO) :-
    io.write_string(&quot;The 100th prime is &quot;, !IO),
    X = prime(100),
    io.write_int(X, !IO),
    io.nl(!IO).
</pre></div>

</dd>
<dt><b>Handling accumulators (1)</b></dt>
<dd><div class="example">
<pre class="example">foldl2(_, [], !A, !B).
foldl2(P, [X | Xs], !A, !B) :-
    P(X, !A, !B),
    foldl2(P, Xs, !A, !B).
</pre></div>

</dd>
<dt><b>Handling accumulators (2)</b></dt>
<dd><div class="example">
<pre class="example">iterate_while2(Test, Update, !A, !B) :-
    ( if Test(!.A, !.B) then
        Update(!A, !B),
        iterate_while2(Test, Update, !A, !B)
    else
        true
    ).
</pre></div>

</dd>
<dt><b>Introducing state</b></dt>
<dd><div class="example">
<pre class="example">compute_out(InA, InB, InsC, Out) :-
    some [!State]
    (
        init_state(!:State),
        update_state_a(InA, !State),
        update_state_b(InB, !State),
        list.foldl(update_state_c, InC, !State),
        compute_output(!.State, Out)
    ).
</pre></div>
</dd>
</dl>

<hr>
<div class="header">
<p>
Next: <a href="DCG_002drules.html#DCG_002drules" accesskey="n" rel="next">DCG-rules</a>, Previous: <a href="Goals.html#Goals" accesskey="p" rel="previous">Goals</a>, Up: <a href="Syntax.html#Syntax" accesskey="u" rel="up">Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
