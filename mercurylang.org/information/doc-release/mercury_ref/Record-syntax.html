<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Record syntax</title>

<meta name="description" content="The Mercury Language Reference Manual: Record syntax">
<meta name="keywords" content="The Mercury Language Reference Manual: Record syntax">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Data_002dterms.html#Data_002dterms" rel="up" title="Data-terms">
<link href="Unification-expressions.html#Unification-expressions" rel="next" title="Unification expressions">
<link href="Data_002dfunctors.html#Data_002dfunctors" rel="previous" title="Data-functors">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Record-syntax"></a>
<div class="header">
<p>
Next: <a href="Unification-expressions.html#Unification-expressions" accesskey="n" rel="next">Unification expressions</a>, Previous: <a href="Data_002dfunctors.html#Data_002dfunctors" accesskey="p" rel="previous">Data-functors</a>, Up: <a href="Data_002dterms.html#Data_002dterms" accesskey="u" rel="up">Data-terms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Record-syntax-1"></a>
<h4 class="subsection">2.15.2 Record syntax</h4>

<p>Record syntax provides a convenient way
to select or update fields of data constructors,
independent of the definition of the constructor.
Record syntax expressions are transformed into
sequences of calls to field selection or update functions
(see <a href="Field-access-functions.html#Field-access-functions">Field access functions</a>).
</p>
<p>A field specifier is a name or a compound data-term.
A field list is a list of field specifiers separated by <code>^</code>.
<code>field</code>, <code>field1 ^ field2</code> and <code>field1(A) ^ field2(B, C)</code>
are all valid field lists.
</p>
<p>If the top-level functor of a field specifier is &lsquo;<samp><var>field</var>/N</samp>&rsquo;,
there must be a visible selection function &lsquo;<samp><var>field</var>/(N + 1)</samp>&rsquo;.
If the field specifier occurs in a field update expression,
there must also be a visible update function
named &lsquo;<samp>'<var>field</var> :='/(N + 2)</samp>&rsquo;.
</p>
<p>Record syntax expressions have one of the following forms.
There are also record syntax DCG goals (see <a href="DCG_002dgoals.html#DCG_002dgoals">DCG-goals</a>),
which provide similar functionality to record syntax expressions,
except that they act on the DCG arguments of a DCG clause.
</p>
<dl compact="compact">
<dt><code><var>Term</var> ^ <var>field_list</var></code></dt>
<dd>
<p>A field selection.
For each field specifier in <var>field_list</var>,
apply the corresponding selection function in turn.
</p>
<p><var>Term</var> must be a valid data-term.
<var>field_list</var> must be a valid field list.
</p>
<p>A field selection is transformed using the following rules:
</p><div class="example">
<pre class="example">transform(Term ^ Field(Arg1, &hellip;)) = Field(Arg1, &hellip;, Term).
transform(Term ^ Field(Arg1, &hellip;) ^ Rest) =
                transform(Field(Arg1, &hellip;, Term) ^ Rest).
</pre></div>

<p>Examples:
</p>
<p><code>Term ^ field</code> is equivalent to <code>field(Term)</code>.
</p>
<p><code>Term ^ field(Arg)</code> is equivalent to <code>field(Arg, Term)</code>.
</p>
<p><code>Term&nbsp;^&nbsp;field1(Arg1)&nbsp;^&nbsp;field2(Arg2,&nbsp;Arg3)</code><!-- /@w --> is equivalent
to <code>field2(Arg2,&nbsp;Arg3,&nbsp;field1(Arg1,&nbsp;Term))</code><!-- /@w -->.
</p>
</dd>
<dt><code><var>Term</var> ^ <var>field_list</var> := <var>FieldValue</var></code></dt>
<dd>
<p>A field update, returning a copy of <var>Term</var>
with the value of the field specified by <var>field_list</var>
replaced with <var>FieldValue</var>.
</p>
<p><var>Term</var> must be a valid data-term.
<var>field_list</var> must be a valid field list.
</p>
<p>A field update is transformed using the following rules:
</p><div class="example">
<pre class="example">transform(Term ^ Field(Arg1, &hellip;) := FieldValue) =
                'Field :='(Arg1, &hellip;, Term, FieldValue)).

transform(Term0 ^ Field(Arg1, &hellip;) ^ Rest := FieldValue) = Term :-
        OldFieldValue = Field(Arg1, &hellip;, Term0),
        NewFieldValue = transform(OldFieldValue ^ Rest := FieldValue),
        Term = 'Field :='(Arg1, &hellip;, Term0, NewFieldValue).
</pre></div>

<p>Examples:
</p>
<p><code>Term&nbsp;^&nbsp;field&nbsp;:=&nbsp;FieldValue</code><!-- /@w -->
is equivalent to <code>'field&nbsp;:='(Term,&nbsp;FieldValue)</code><!-- /@w -->.
</p>
<p><code>Term&nbsp;^&nbsp;field(Arg)&nbsp;:=&nbsp;FieldValue</code><!-- /@w -->
is equivalent to <code>'field&nbsp;:='(Arg,&nbsp;Term,&nbsp;FieldValue)</code><!-- /@w -->.
</p>
<p><code>Term&nbsp;^&nbsp;field1(Arg1)&nbsp;^&nbsp;field2(Arg2)&nbsp;:=&nbsp;FieldValue</code><!-- /@w -->
is equivalent to the code
</p><div class="example">
<pre class="example">OldField1 = field1(Arg1, Term),
NewField1 = 'field2 :='(Arg2, OldField1, FieldValue),
Result = 'field1 :='(Arg1, Term, NewField1)
</pre></div>

</dd>
</dl>

<hr>
<div class="header">
<p>
Next: <a href="Unification-expressions.html#Unification-expressions" accesskey="n" rel="next">Unification expressions</a>, Previous: <a href="Data_002dfunctors.html#Data_002dfunctors" accesskey="p" rel="previous">Data-functors</a>, Up: <a href="Data_002dterms.html#Data_002dterms" accesskey="u" rel="up">Data-terms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
