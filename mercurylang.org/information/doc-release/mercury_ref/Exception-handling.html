<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Exception handling</title>

<meta name="description" content="The Mercury Language Reference Manual: Exception handling">
<meta name="keywords" content="The Mercury Language Reference Manual: Exception handling">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html#Top" rel="up" title="Top">
<link href="Semantics.html#Semantics" rel="next" title="Semantics">
<link href="Type-conversions.html#Type-conversions" rel="previous" title="Type conversions">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Exception-handling"></a>
<div class="header">
<p>
Next: <a href="Semantics.html#Semantics" accesskey="n" rel="next">Semantics</a>, Previous: <a href="Type-conversions.html#Type-conversions" accesskey="p" rel="previous">Type conversions</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Exception-handling-1"></a>
<h2 class="chapter">13 Exception handling</h2>

<p>Mercury procedures may throw exceptions.
Exceptions may be caught
using the predicates defined in the &lsquo;<samp>exception</samp>&rsquo; library module,
or using try goals.
</p>
<p>A &lsquo;<samp>try</samp>&rsquo; goal has the following form:
</p>
<div class="example">
<pre class="example">    try <var>Params</var> <var>Goal</var>
    then <var>ThenGoal</var>
    else <var>ElseGoal</var>
    catch <var>Term</var> -&gt; <var>CatchGoal</var>
    &hellip;
    catch_any <var>CatchAnyVar</var> -&gt; <var>CatchAnyGoal</var>
</pre></div>

<p><var>Goal</var>, <var>ThenGoal</var>, <var>ElseGoal</var>, <var>CatchGoal</var>,
<var>CatchAnyGoal</var> must be valid goals.
</p>
<p><var>Goal</var> must have one of the following determinisms:
<code>det</code>, <code>semidet</code>, <code>cc_multi</code>, or <code><span class="nolinebreak">cc_nondet</span></code><!-- /@w -->.
</p>
<p>The non-local variables of <var>Goal</var>
must not have an inst equivalent to
<code>unique</code>, <code><span class="nolinebreak">mostly_unique</span></code><!-- /@w --> or <code>any</code>,
unless they have the type &lsquo;<samp>io.state</samp>&rsquo;.
</p>
<p><var>Params</var> must be a valid list of zero or more try parameters.
</p>
<p>The &ldquo;then&rdquo; part is mandatory.
The &ldquo;else&rdquo; part is mandatory if <var>Goal</var> may fail;
otherwise it must be omitted.
There may be zero or more &ldquo;catch&rdquo; branches.
The &ldquo;catch_any&rdquo; part is optional.
<var>CatchAnyVar</var> must be a single variable.
</p>
<p>The try parameter &lsquo;<samp>io</samp>&rsquo; takes a single argument,
which must be the name of a state variable prefixed by &lsquo;<samp>!</samp>&rsquo;;
for example, &lsquo;<samp>io(!IO)</samp>&rsquo;.
The state variable must have the type &lsquo;<samp>io.state</samp>&rsquo;,
and be in scope of the try goal.
The state variable is threaded through <var>Goal</var>,
so it may perform I/O but cannot fail.
If no &lsquo;<samp>io</samp>&rsquo; parameter exists, <var>Goal</var> may not perform I/O and may fail.
</p>
<p>A try goal has determinism <code>cc_multi</code>.
</p>
<p>On entering a try goal, <var>Goal</var> is executed.
If it succeeds without throwing an exception, <var>ThenGoal</var> is executed.
Any variables bound by <var>Goal</var> are visible in <var>ThenGoal</var> only.
If <var>Goal</var> fails, then <var>ElseGoal</var> is executed.
</p>
<p>If <var>Goal</var> throws an exception,
the exception value is unified with
each of the <var>Term</var>s in the &ldquo;catch&rdquo; branches in turn.
On the first successful unification,
the corresponding <var>CatchGoal</var> is executed
(and other &ldquo;catch&rdquo; and &ldquo;catch_any&rdquo; branches ignored).
Variables bound during the unification of the <var>Term</var>
are in scope of the corresponding <var>CatchGoal</var>.
</p>
<p>If the exception value does not unify
with any of the terms in &ldquo;catch&rdquo; branches,
and a &ldquo;catch_any&rdquo; branch is present,
the exception is bound to <var>CatchAnyVar</var>
and the <var>CatchAnyGoal</var> executed.
<var>CatchAnyVar</var> is visible in the <var>CatchAnyGoal</var> only,
and is existentially typed, i.e. it has type &lsquo;<samp>some [T] T</samp>&rsquo;.
</p>
<p>Finally, if the thrown value did not unify with any &ldquo;catch&rdquo; term,
and there is no &ldquo;catch_any&rdquo; branch, the exception is rethrown.
</p>
<p>The declarative semantics of a try goal is:
</p>
<div class="example">
<pre class="example">    (
        try [] Goal
        then Then
        else Else
        catch CP1 -&gt; CG1
        catch CP2 -&gt; CG2
        &hellip;
        catch_any CAV -&gt; CAG
    )
    &lt;=&gt;
    (
        Goal, Then
    ;
        not Goal, Else
    ;
        some [Excp]
        ( if Excp = CP1 then
            CG1
        else if Excp = CP2 then
            CG2
        else if &hellip;
            &hellip;
        else
            Excp = CAV,
            CAG
        )
    ).
</pre></div>

<p>If no &lsquo;<samp>else</samp>&rsquo; branch is present, then &lsquo;<samp>Else = fail</samp>&rsquo;.
If no &lsquo;<samp>catch_any</samp>&rsquo; branch is present, then &lsquo;<samp>CAG = fail</samp>&rsquo;.
</p>
<p>An example of a try goal that performs I/O is:
</p>
<div class="example">
<pre class="example">:- pred p_carefully(io::di, io::uo) is cc_multi.

p_carefully(!IO) :-
    (try [io(!IO)] (
        io.write_string(&quot;Calling p\n&quot;, !IO),
        p(Output, !IO)
    )
    then
        io.write_string(&quot;p returned: &quot;, !IO),
        io.write(Output, !IO),
        io.nl(!IO)
    catch S -&gt;
        io.write_string(&quot;p threw a string: &quot;, !IO),
        io.write_string(S, !IO),
        io.nl(!IO)
    catch 42 -&gt;
        io.write_string(&quot;p threw 42\n&quot;, !IO)
    catch_any Other -&gt;
        io.write_string(&quot;p threw something: &quot;, !IO),
        io.write(Other, !IO),
        % Rethrow the value.
        throw(Other)
    ).
</pre></div>

<hr>
<div class="header">
<p>
Next: <a href="Semantics.html#Semantics" accesskey="n" rel="next">Semantics</a>, Previous: <a href="Type-conversions.html#Type-conversions" accesskey="p" rel="previous">Type conversions</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
