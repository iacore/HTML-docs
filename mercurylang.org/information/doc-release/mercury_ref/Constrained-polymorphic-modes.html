<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Constrained polymorphic modes</title>

<meta name="description" content="The Mercury Language Reference Manual: Constrained polymorphic modes">
<meta name="keywords" content="The Mercury Language Reference Manual: Constrained polymorphic modes">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Modes.html#Modes" rel="up" title="Modes">
<link href="Different-clauses-for-different-modes.html#Different-clauses-for-different-modes" rel="next" title="Different clauses for different modes">
<link href="Predicate-and-function-mode-declarations.html#Predicate-and-function-mode-declarations" rel="previous" title="Predicate and function mode declarations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Constrained-polymorphic-modes"></a>
<div class="header">
<p>
Next: <a href="Different-clauses-for-different-modes.html#Different-clauses-for-different-modes" accesskey="n" rel="next">Different clauses for different modes</a>, Previous: <a href="Predicate-and-function-mode-declarations.html#Predicate-and-function-mode-declarations" accesskey="p" rel="previous">Predicate and function mode declarations</a>, Up: <a href="Modes.html#Modes" accesskey="u" rel="up">Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Constrained-polymorphic-modes-1"></a>
<h3 class="section">4.3 Constrained polymorphic modes</h3>

<p>Mode declarations for predicates and functions may also have inst parameters.
However, such parameters must be constrained
to be <em>compatible</em> with some other inst.
In a predicate or function mode declaration,
an inst of the form &lsquo;<samp><var>InstParam</var> =&lt; <var>Inst</var></samp>&rsquo;,
where <var>InstParam</var> is a variable and <var>Inst</var> is an inst,
states that
<var>InstParam</var> is constrained to be <em>compatible</em> with <var>Inst</var>,
that is,
<var>InstParam</var> represents some inst
that can be used anywhere where <var>Inst</var> is required.
If an inst parameter occurs more than once in a declaration,
it must have the same constraint on each occurrence.
</p>
<p>For example, in the mode declaration
</p><div class="example">
<pre class="example">:- mode append(in(list_skel(I =&lt; ground)), in(list_skel(I =&lt; ground)),
    out(list_skel(I =&lt; ground))) is det.
</pre></div>
<p><code>I</code> is an inst parameter which is constrained to be ground.
If &lsquo;<samp>append</samp>&rsquo; is called with the first two arguments
having an inst of, say, &lsquo;<samp>list_skel(bound(f))</samp>&rsquo;,
then after &lsquo;<samp>append</samp>&rsquo; returns,
all three arguments will have inst &lsquo;<samp>list_skel(bound(f))</samp>&rsquo;.
If the mode of append had been simply
</p><div class="example">
<pre class="example">:- mode append(in(list_skel(ground)), in(list_skel(ground)),
    out(list_skel(ground))) is det.
</pre></div>
<p>then we would only have been able to infer an inst of &lsquo;<samp>list_skel(ground)</samp>&rsquo;
for the third argument, not the more specific inst.
</p>
<p>Note that attempting to call &lsquo;<samp>append</samp>&rsquo;
when the first two arguments do not have ground insts
(e.g. &lsquo;<samp>list_skel(bound(g(free)))</samp>&rsquo;)
is a mode error because it violates the constraint on the inst parameter.
</p>
<p>To avoid having to repeat a constraint everywhere that an inst parameter occurs,
it is possible to list the constraints after the rest of the mode declaration,
following a &lsquo;<samp>&lt;=</samp>&rsquo;.
E.g. the above example could have been written as
</p><div class="example">
<pre class="example">:- (mode append(in(list_skel(I)), in(list_skel(I)),
    out(list_skel(I))) is det) &lt;= I =&lt; ground.
</pre></div>

<p>Note that in the current Mercury implementation
this syntax requires parentheses
around the &lsquo;<samp>mode(&hellip;) is <var>Det</var></samp>&rsquo; part of the declaration.
</p>
<p>Also, if the constraint on an inst parameter is &lsquo;<samp>ground</samp>&rsquo;
then it is not necessary to give the constraint in the declaration.
The example can be further shortened to
</p><div class="example">
<pre class="example">:- mode append(in(list_skel(I)), in(list_skel(I)), out(list_skel(I)))
    is det.
</pre></div>

<p>Constrained polymorphic modes are particularly useful
when passing objects with higher-order types to polymorphic predicates,
since they allow the higher-order mode information to be retained
(see <a href="Higher_002dorder.html#Higher_002dorder">Higher-order</a>).
</p>
<hr>
<div class="header">
<p>
Next: <a href="Different-clauses-for-different-modes.html#Different-clauses-for-different-modes" accesskey="n" rel="next">Different clauses for different modes</a>, Previous: <a href="Predicate-and-function-mode-declarations.html#Predicate-and-function-mode-declarations" accesskey="p" rel="previous">Predicate and function mode declarations</a>, Up: <a href="Modes.html#Modes" accesskey="u" rel="up">Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
