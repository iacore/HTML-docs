<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Determinism checking and inference</title>

<meta name="description" content="The Mercury Language Reference Manual: Determinism checking and inference">
<meta name="keywords" content="The Mercury Language Reference Manual: Determinism checking and inference">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Determinism.html#Determinism" rel="up" title="Determinism">
<link href="Replacing-compile_002dtime-checking-with-run_002dtime-checking.html#Replacing-compile_002dtime-checking-with-run_002dtime-checking" rel="next" title="Replacing compile-time checking with run-time checking">
<link href="Determinism-categories.html#Determinism-categories" rel="previous" title="Determinism categories">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Determinism-checking-and-inference"></a>
<div class="header">
<p>
Next: <a href="Replacing-compile_002dtime-checking-with-run_002dtime-checking.html#Replacing-compile_002dtime-checking-with-run_002dtime-checking" accesskey="n" rel="next">Replacing compile-time checking with run-time checking</a>, Previous: <a href="Determinism-categories.html#Determinism-categories" accesskey="p" rel="previous">Determinism categories</a>, Up: <a href="Determinism.html#Determinism" accesskey="u" rel="up">Determinism</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Determinism-checking-and-inference-1"></a>
<h3 class="section">6.2 Determinism checking and inference</h3>

<p>The determinism of goals
is inferred from the determinism of their component parts,
according to the rules below.
The inferred determinism of a procedure is just the inferred
determinism of the procedure&rsquo;s body.
</p>
<p>For procedures that are local to a module,
the determinism annotations may be omitted;
in that case, their determinism will be inferred.
(To be precise, the determinism of procedures without a determinism annotation
is defined as the least fixpoint of the transformation which,
given an initial assignment
of the determinism <code>det</code> to all such procedures,
applies those rules to infer
a new determinism assignment for those procedures.)
</p>
<p>It is an error to omit the determinism annotation
for procedures that are exported from their containing module.
</p>
<p>If a determinism annotation is supplied for a procedure,
the declared determinism is compared against the inferred determinism.
If the declared determinism is greater than or not comparable to the
inferred determinism (in the partial ordering above), it is an error.
If the declared determinism is less than the inferred determinism,
it is not an error, but the implementation may issue a warning.
</p>
<p>The determinism category of each goal
is inferred according to the following rules.
These rules work with the two components of determinism categories:
whether the goal can fail without producing a solution,
and the maximum number of solutions of the goal (0, 1, or more).
If the inference process below reports that a goal can succeed more than once,
but the goal generates no outputs that are visible from outside the goal,
and the goal is not impure (see <a href="Impurity.html#Impurity">Impurity</a>),
then the final determinism of the goal
will be based on the goal succeeding at most once,
since the compiler will implicitly prune away any duplicate solutions.
</p>
<dl compact="compact">
<dt>Calls</dt>
<dd><p>The determinism category of a call
is the determinism declared or inferred
for the called mode of the called procedure.
</p>
</dd>
<dt>Unifications</dt>
<dd><p>The determinism of a unification
is either <code>det</code>, <code>semidet</code>, or <code>failure</code>,
depending on its mode.
</p>
<p>A unification that assigns the value of one variable to another
is deterministic.
A unification that constructs a structure and assigns it to a variable
is also deterministic.
A unification that tests whether a variable has a given top function symbol
is semideterministic,
unless the compiler knows the top function symbol of that variable,
in which case its determinism is either det or failure
depending on whether the two function symbols are the same or not.
A unification that tests two variables for equality
is semideterministic,
unless the compiler knows that the two variables are aliases for one another,
in which case the unification is deterministic,
or unless the compiler knows that the two variables
have different function symbols in the same position,
in which case the unification has a determinism of failure.
</p>
<p>The compiler knows the top function symbol of a variable
if the previous part of the procedure definition
contains a unification of the variable with a function symbol,
or if the variable&rsquo;s type has only one function symbol.
</p>
</dd>
<dt>Conjunctions</dt>
<dd><p>The determinism of the empty conjunction (the goal &lsquo;<samp>true</samp>&rsquo;)
is <code>det</code>.
The conjunction &lsquo;<samp>(<var>A</var>, <var>B</var>)</samp>&rsquo; can fail
if either <var>A</var> can fail, or if <var>A</var> can succeed at least once,
and <var>B</var> can fail.
The conjunction can succeed at most zero times
if either <var>A</var> or <var>B</var> can succeed at most zero times.
The conjunction can succeed more than once
if either <var>A</var> or <var>B</var> can succeed more than once
and both <var>A</var> and <var>B</var> can succeed at least once.
(If e.g. <var>A</var> can succeed at most zero times,
then even if <var>B</var> can succeed many times
the maximum number of solutions of the conjunction is still zero.)
Otherwise, i.e. if both <var>A</var> and <var>B</var> succeed at most once,
the conjunction can succeed at most once.
</p>
</dd>
<dt>Switches</dt>
<dd><p>A disjunction is a <em>switch</em>
if each disjunct has near its start
a unification that tests the same bound variable
against a different function symbol.
For example, consider the common pattern
</p><div class="example">
<pre class="example">(
    L = [], empty(Out)
;
    L = [H|T], nonempty(H, T, Out)
)
</pre></div>

<p>If <code>L</code> is input to the disjunction,
then the disjunction is a switch on <code>L</code>.
</p>
<p>If two variables are unified with each other,
then whatever function symbol one variable is unified with,
the other variable is considered to be unified with the same function symbol.
In the following example,
since <code>K</code> is unified with <code>L</code>,
the second disjunct unifies <code>L</code> as well as <code>K</code> with cons,
and thus the disjunction is recognized as a switch.
</p><div class="example">
<pre class="example">(
    L = [], empty(Out)
;
    K = L, K = [H|T], nonempty(H, T, Out)
)
</pre></div>

<p>A switch can fail
if the various arms of the switch do not cover
all the function symbols in the type of the switched-on variable,
or if the code in some arms of the switch can fail,
bearing in mind that in each arm of the switch,
the unification that tests the switched-on variable
against the function symbol of that arm
is considered to be deterministic.
A switch can succeed several times
if some arms of the switch can succeed several times,
possibly because there are multiple disjuncts
that test the switched-on variable against the same function symbol.
A switch can succeed at most zero times
only if all the reachable arms of the switch can succeed at most zero times.
(A switch arm is not reachable
if it unifies the switched-on variable with a function symbol
that is ruled out by that variable&rsquo;s initial instantiation state.)
</p>
<p>Only unifications may occur
before the test of the switched-on variable in each disjunct.
Tests of the switched-on variable
may occur within existential quantification goals.
</p>
<p>The following example is a switch.
</p>
<div class="example">
<pre class="example">(
    Out = 1, L = []
;
    some [H, T] (
        L = [H|T],
        nonempty(H, T, Out)
    )
)
</pre></div>

<p>The following example is not a switch
because the call in the first disjunct occurs
before the test of the switched-on variable.
</p>
<div class="example">
<pre class="example">(
    empty(Out), L = []
;
    L = [H|T], nonempty(H, T, Out)
)
</pre></div>

<p>The unification of the switched-on variable with a function symbol
may occur inside a nested disjunction in a given disjunct,
provided that unification is preceded only by other unifications,
both inside the nested disjunction and before the nested disjunction.
The following example is a switch on <code>X</code>,
provided <code>X</code> is bound beforehand.
</p>
<div class="example">
<pre class="example">(
    X = f
    p(Out)
;
    Y = X,
    (
        Y = g,
        Intermediate = 42
    ;
        Z = Y,
        Z = h(Arg),
        q(Arg, Intermediate)
    ),
    r(Intermediate, Out)
)
</pre></div>

</dd>
<dt>Disjunctions</dt>
<dd><p>The determinism of the empty disjunction (the goal &lsquo;<samp>fail</samp>&rsquo;)
is <code>failure</code>.
A disjunction &lsquo;<samp>(<var>A</var> ; <var>B</var>)</samp>&rsquo; that is not a switch
can fail if both <var>A</var> and <var>B</var> can fail.
It can succeed at most zero times
if both <var>A</var> and <var>B</var> can succeed at most zero times.
It can succeed at most once
if one of <var>A</var> and <var>B</var> can succeed at most once
and the other can succeed at most zero times.
Otherwise, i.e. if either <var>A</var> or <var>B</var> can succeed more than once,
or if both <var>A</var> and <var>B</var> can succeed at least once,
it can succeed more than once.
</p>

</dd>
<dt>If-then-else</dt>
<dd>
<p>If the condition of an if-then-else cannot fail,
the if-then-else is equivalent
to the conjunction of the condition and the &ldquo;then&rdquo; part,
and its determinism is computed accordingly.
Otherwise, an if-then-else can fail
if either the &ldquo;then&rdquo; part or the &ldquo;else&rdquo; part can fail.
It can succeed at most zero times
if the &ldquo;else&rdquo; part can succeed at most zero times
and if at least one of the condition and the &ldquo;then&rdquo; part
can succeed at most zero times.
It can succeed more than once
if any one of the condition, the &ldquo;then&rdquo; part and the &ldquo;else&rdquo; part
can succeed more than once.
</p>
</dd>
<dt>Negations</dt>
<dd>
<p>If the determinism of the negated goal is <code>erroneous</code>,
then the determinism of the negation is <code>erroneous</code>.
If the determinism of the negated goal is <code>failure</code>,
the determinism of the negation is <code>det</code>.
If the determinism of the negated goal is <code>det</code> or <code>multi</code>,
the determinism of the negation is <code>failure</code>.
Otherwise, the determinism of the negation is <code>semidet</code>.
</p>
</dd>
</dl>

<hr>
<div class="header">
<p>
Next: <a href="Replacing-compile_002dtime-checking-with-run_002dtime-checking.html#Replacing-compile_002dtime-checking-with-run_002dtime-checking" accesskey="n" rel="next">Replacing compile-time checking with run-time checking</a>, Previous: <a href="Determinism-categories.html#Determinism-categories" accesskey="p" rel="previous">Determinism categories</a>, Up: <a href="Determinism.html#Determinism" accesskey="u" rel="up">Determinism</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
