<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Semantics</title>

<meta name="description" content="The Mercury Language Reference Manual: Semantics">
<meta name="keywords" content="The Mercury Language Reference Manual: Semantics">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html#Top" rel="up" title="Top">
<link href="Foreign-language-interface.html#Foreign-language-interface" rel="next" title="Foreign language interface">
<link href="Exception-handling.html#Exception-handling" rel="previous" title="Exception handling">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Semantics"></a>
<div class="header">
<p>
Next: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="n" rel="next">Foreign language interface</a>, Previous: <a href="Exception-handling.html#Exception-handling" accesskey="p" rel="previous">Exception handling</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Semantics-1"></a>
<h2 class="chapter">14 Semantics</h2>

<p>A legal Mercury program is one that complies with the syntax,
type, mode, determinism, and module system rules specified in earlier chapters.
If a program does not comply with those rules,
the compiler must report an error.
</p>
<p>For each legal Mercury program,
there is an associated predicate calculus theory
whose language is specified by the type declarations in the program
and whose axioms are the completion of the clauses for all predicates
in the program,
plus the usual equality axioms extended with the completion of the
equations for all functions in the program,
plus axioms corresponding to the mode-determinism assertions
(see <a href="Determinism.html#Determinism">Determinism</a>),
plus axioms specifying the semantics of library predicates and functions.
The declarative semantics of a legal Mercury program
is specified by this theory.
</p>
<p>Mercury implementations must be sound:
the answers they compute must be true in every model of the theory.
Mercury implementations are not required to be complete:
they may fail to compute an answer in finite time,
or they may exhaust the resource limitations of the execution
environment, even though an answer is provable in the theory.
However, there are certain minimum requirements that they
must satisfy with respect to completeness.
</p>
<p>There is an operational semantics of Mercury programs called the
<em>strict sequential</em> operational semantics.  In this semantics,
the program is executed top-down, starting from &lsquo;<samp>main/2</samp>&rsquo;
preceded by any module initialisation goals
(as per <a href="Module-initialisation.html#Module-initialisation">Module initialisation</a>), followed by any module finalisation
goals (as per <a href="Module-finalisation.html#Module-finalisation">Module finalisation</a>),
and function calls within a goal, conjunctions and disjunctions are all
executed in depth-first left-to-right order.
Conjunctions and function calls are
&ldquo;minimally&rdquo; reordered as required by the modes:
the order is determined by selecting the first mode-correct sub-goal
(conjunct or function call),
executing that, then selecting the first of the remaining sub-goals
which is now mode-correct, executing that, and so on.
(There is no interleaving of different individual conjuncts or function calls,
however; the sub-goals are reordered, not split and interleaved.)
Function application is strict, not lazy.
</p>
<p>Mercury implementations are required to provide a method of processing
Mercury programs which is equivalent to the strict sequential
operational semantics.
</p>
<p>There is another operational semantics of Mercury programs
called the <em>strict commutative</em> operational semantics.
This semantics is equivalent to the strict sequential operational semantics
except that there is no requirement that
function calls, conjunctions and disjunctions be executed left-to-right;
they may be executed in any order, and may even be interleaved.
Furthermore, the order may even be different
each time a particular goal is entered.
</p>
<p>As well as providing the strict sequential operational semantics,
Mercury implementations may optionally provide
additional implementation-defined operational semantics,
provided that any such implementation-defined operational semantics
are at least as complete as the strict commutative operational semantics.
An implementation-defined semantics
is &ldquo;at least as complete&rdquo; as the strict commutative semantics
if and only if the implementation-defined semantics
guarantees to compute an answer in finite time
for any program for which an answer would be computed in finite time
for all possible executions under the strict commutative semantics
(i.e. for all possible orderings of conjunctions and disjunctions).
</p>
<p>Thus, to summarize,
there are in fact a variety of different operational semantics for Mercury.
In one of them, the strict sequential semantics,
there is no nondeterminism &mdash; the behaviour is always specified exactly.
Programs are executed top-down using SLDNF (or something equivalent),
mode analysis does &ldquo;minimal&rdquo; reordering (in a precisely defined sense),
function calls, conjunctions and disjunctions
are executed depth-first left-to-right,
and function evaluation is strict.
All implementations are required to support the strict sequential semantics,
so that a program which works on one implementation using this semantics
will be guaranteed to work on any other implementation.
However, implementations are also allowed to support
other operational semantics,
which may have non-determinism,
so long as they are sound with respect to the declarative semantics,
and so long as they meet a minimum level of completeness
(they must be at least as complete as the strict commutative semantics,
in the sense that every program which terminates for all possible orderings
must also terminate in any implementation-defined operational semantics).
</p>
<p>This compromise allows Mercury to be used in several different ways.
Programmers who care more about ease of programming and portability
than about efficiency can use the strict sequential semantics,
and can then be guaranteed that
if their program works on one correct implementation,
it will work on all correct implementations.
Compiler implementors who want to write optimizing implementations
that do lots of clever code reorderings and other high-level transformations
or that want to offer parallelizing implementations
which take maximum advantage of parallelism
can define different semantic models.
Programmers who care about efficiency more than portability
can write code for these implementation-defined semantic models.
Programmers who care about efficiency <em>and</em> portability
can achieve this by writing code for the strict commutative semantics.
Of course, this is not quite as easy as using the strict sequential semantics,
since it is in general not sufficient
to test your programs on just one implementation
if you are to be sure that it will be able to use
the maximally efficient operational semantics on any implementation.
However, if you do write code which works for all possible executions
under the strict commutative semantics
(i.e. for all possible orderings of conjunctions and disjunctions),
then you can be guaranteed that it will work correctly
on every implementation, under every possible implementation-defined semantics.
</p>
<p>The Melbourne Mercury implementation offers
eight different semantics,
which can be selected with different combinations
of the &lsquo;<samp>--no-reorder-conj</samp>&rsquo;, &lsquo;<samp>--no-reorder-disj</samp>&rsquo;,
and &lsquo;<samp>--no-fully-strict</samp>&rsquo; options.
(The &lsquo;<samp>--no-fully-strict</samp>&rsquo; option allows the compiler to improve
completeness by optimizing away infinite loops and goals with determinism
<code>erroneous</code>.)
The default semantics are the strict commutative semantics.
Enabling &lsquo;<samp>--no-reorder-conj</samp>&rsquo; and &lsquo;<samp>--no-reorder-disj</samp>&rsquo;
gives the strict sequential semantics.
</p>
<p>Future implementations of Mercury
may wish to offer other operational semantics.
For example, they may wish to provide semantics
in which function evaluation is lazy, rather than strict;
semantics with a guaranteed fair search rule; and so forth.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="n" rel="next">Foreign language interface</a>, Previous: <a href="Exception-handling.html#Exception-handling" accesskey="p" rel="previous">Exception handling</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
