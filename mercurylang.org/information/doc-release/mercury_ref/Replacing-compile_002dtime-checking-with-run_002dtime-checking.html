<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Replacing compile-time checking with run-time checking</title>

<meta name="description" content="The Mercury Language Reference Manual: Replacing compile-time checking with run-time checking">
<meta name="keywords" content="The Mercury Language Reference Manual: Replacing compile-time checking with run-time checking">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Determinism.html#Determinism" rel="up" title="Determinism">
<link href="Interfacing-nondeterministic-code-with-the-real-world.html#Interfacing-nondeterministic-code-with-the-real-world" rel="next" title="Interfacing nondeterministic code with the real world">
<link href="Determinism-checking-and-inference.html#Determinism-checking-and-inference" rel="previous" title="Determinism checking and inference">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Replacing-compile_002dtime-checking-with-run_002dtime-checking"></a>
<div class="header">
<p>
Next: <a href="Interfacing-nondeterministic-code-with-the-real-world.html#Interfacing-nondeterministic-code-with-the-real-world" accesskey="n" rel="next">Interfacing nondeterministic code with the real world</a>, Previous: <a href="Determinism-checking-and-inference.html#Determinism-checking-and-inference" accesskey="p" rel="previous">Determinism checking and inference</a>, Up: <a href="Determinism.html#Determinism" accesskey="u" rel="up">Determinism</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Replacing-compile_002dtime-checking-with-run_002dtime-checking-1"></a>
<h3 class="section">6.3 Replacing compile-time checking with run-time checking</h3>

<p>Note that &ldquo;perfect&rdquo; determinism inference is an undecidable problem,
because it requires solving the halting problem.
(For instance, in the following example
</p>
<div class="example">
<pre class="example">:- pred p(T, T).
:- mode p(in, out) is det.

p(A, B) :-
    (
        something_complicated(A, B)
    ;
        B = A
    ).
</pre></div>

<p>&lsquo;<samp>p/2</samp>&rsquo; can have more than one solution
only if &lsquo;<samp>something_complicated/2</samp>&rsquo; can succeed.)
Sometimes,
the rules specified by the Mercury language for determinism inference
will infer a determinism that is not as precise as you would like.
However, it is generally easy to overcome such problems.
The way to do this is to replace the compiler&rsquo;s static checking
with some manual run-time checking.
For example, if you know that a particular goal should never fail,
but the compiler infers that goal to be <code>semidet</code>,
you can check at runtime that the goal does succeed,
and if it fails, call the library predicate &lsquo;<samp>error/1</samp>&rsquo;.
</p>
<div class="example">
<pre class="example">:- pred q(T, T).
:- mode q(in, out) is det.

q(A, B) :-
    ( goal_that_should_never_fail(A, B0) -&gt;
        B = B0
    ;
        error(&quot;goal_that_should_never_fail failed!&quot;)
    ).
</pre></div>

<p>The predicate <code>error/1</code> has determinism <code>erroneous</code>,
which means the compiler knows that it will never succeed or fail,
so the inferred determinism for the body of <code>q/2</code> is <code>det</code>.
(Checking assumptions like this is good coding style anyway.
The small amount of up-front work that Mercury requires
is paid back in reduced debugging time.)
Mercury&rsquo;s mode analysis knows that
computations with determinism <code>erroneous</code> can never succeed,
which is why it does not require the &ldquo;else&rdquo; part
to generate a value for <code>B</code>.
The introduction of the new variable <code>B0</code> is necessary
because the condition of an if-then-else is a negated context,
and can export the values it generates
only to the &ldquo;then&rdquo; part of the if-then-else,
not directly to the surrounding computation.
(If the surrounding computations had direct access
to values generated in conditions,
they might access them even if the condition failed.)
</p>
<hr>
<div class="header">
<p>
Next: <a href="Interfacing-nondeterministic-code-with-the-real-world.html#Interfacing-nondeterministic-code-with-the-real-world" accesskey="n" rel="next">Interfacing nondeterministic code with the real world</a>, Previous: <a href="Determinism-checking-and-inference.html#Determinism-checking-and-inference" accesskey="p" rel="previous">Determinism checking and inference</a>, Up: <a href="Determinism.html#Determinism" accesskey="u" rel="up">Determinism</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
