<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Using Mercury enumerations in foreign code</title>

<meta name="description" content="The Mercury Language Reference Manual: Using Mercury enumerations in foreign code">
<meta name="keywords" content="The Mercury Language Reference Manual: Using Mercury enumerations in foreign code">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Foreign-language-interface.html#Foreign-language-interface" rel="up" title="Foreign language interface">
<link href="Adding-foreign-declarations.html#Adding-foreign-declarations" rel="next" title="Adding foreign declarations">
<link href="Using-foreign-enumerations-in-Mercury-code.html#Using-foreign-enumerations-in-Mercury-code" rel="previous" title="Using foreign enumerations in Mercury code">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Using-Mercury-enumerations-in-foreign-code"></a>
<div class="header">
<p>
Next: <a href="Adding-foreign-declarations.html#Adding-foreign-declarations" accesskey="n" rel="next">Adding foreign declarations</a>, Previous: <a href="Using-foreign-enumerations-in-Mercury-code.html#Using-foreign-enumerations-in-Mercury-code" accesskey="p" rel="previous">Using foreign enumerations in Mercury code</a>, Up: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="u" rel="up">Foreign language interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Using-Mercury-enumerations-in-foreign-code-1"></a>
<h3 class="section">15.6 Using Mercury enumerations in foreign code</h3>

<p>A &lsquo;<samp>pragma foreign_enum</samp>&rsquo; declaration
imports the values of the constants of an enumeration type into Mercury.
However, sometimes one needs the reverse:
the ability to <em>export</em>
the values of the constants of an enumeration type
(whether those values were assigned by &lsquo;<samp>foreign_enum</samp>&rsquo; pragmas or not)
from Mercury to foreign language code in
&lsquo;<samp>foreign_proc</samp>&rsquo; and &lsquo;<samp>foreign_code</samp>&rsquo; pragmas.
This is what &lsquo;<samp>pragma foreign_export_enum</samp>&rsquo; declarations are for.
</p>
<p>These pragmas have the following general form:
</p>
<div class="example">
<pre class="example">:- pragma foreign_export_enum(&quot;<var>Lang</var>&quot;, <var>MercuryType</var>,
    <var>Attributes</var>, <var>Overrides</var>).
</pre></div>

<p>When given such a pragma,
the compiler will define a symbolic name in language <var>Lang</var>
for each of the constructors of <var>MercuryType</var>
(which must be an enumeration type).
Each symbolic name allows code in that foreign language
to create a value corresponding to that of the constructor it represents.
(The exact mechanism used depends upon the foreign language;
see the language specific information below for further details.)
</p>
<p>For each foreign language,
there is a default mapping between the name of a Mercury constructor
and its symbolic name in the language <var>Lang</var>.
This default mapping is not required to map
every valid constructor name to a valid name in language <var>Lang</var>;
where it does not, the programmer must specify a valid symbolic name.
The programmer may also choose to map a constructor to a symbolic name
that differs from the one supplied
by the default mapping for language <var>Lang</var>.
<var>Overrides</var> is a list
whose elements are pairs of constructor names and strings.
The latter specify the name that the implementation should use
as the symbolic name in the foreign language.
<var>Overrides</var> has the following form:
</p>
<div class="example">
<pre class="example">[cons_I - &quot;symbol_I&quot;, &hellip;, cons_J - &quot;symbol_J&quot;]
</pre></div>

<p>This can be used to provide
either a valid symbolic name where the default mapping does not,
or to override a valid symbolic name generated by the default mapping.
This argument may be omitted if <var>Overrides</var> is empty.
</p>
<p>The argument <var>Attributes</var> is a list of optional attributes.
If empty,
it may be omitted from the &lsquo;<samp>pragma foreign_export_enum</samp>&rsquo; declaration
if the <var>Overrides</var> argument is also omitted.
The following attributes must be supported by all Mercury implementations.
</p>
<dl compact="compact">
<dt>&lsquo;<samp>prefix(<var>Prefix</var>)</samp>&rsquo;</dt>
<dd><p>Prefix each symbolic name, regardless of how it was generated,
with the string <var>Prefix</var>.
A &lsquo;<samp>pragma foreign_export_enum</samp>&rsquo; declaration
may contain at most one &lsquo;<samp>prefix</samp>&rsquo; attribute.
</p>
</dd>
<dt>&lsquo;<samp>uppercase</samp>&rsquo;</dt>
<dd><p>Convert any alphabetic characters in a Mercury constructor name to uppercase
when generating the symbolic name using the default mapping.
Symbolic names specified by the programmer using <var>Overrides</var>
are not affected by this attribute.
If the &lsquo;<samp>prefix</samp>&rsquo; attribute is also specified,
then the prefix is added to the symbolic name
<em>after</em> the conversion to uppercase has been performed,
i.e. the characters in the prefix
are not affected by the &lsquo;<samp>uppercase</samp>&rsquo; attribute.
</p>
</dd>
</dl>

<p>The implementation does not check
the validity of a symbolic name in the foreign language
until after the effects of any attributes have been applied.
This means that attributes may cause
an otherwise valid symbolic name to become invalid, or vice versa.
</p>
<p>A Mercury module may contain &lsquo;<samp>pragma foreign_export_enum</samp>&rsquo; declarations
that refer to imported types, subject to the usual visibility restrictions.
</p>
<p>A Mercury module, or program, may contain
more than one &lsquo;<samp>pragma foreign_export_enum</samp>&rsquo; declaration
for a given Mercury type for a given language.
This can be useful when a project is transitioning
from using one naming scheme for Mercury constants in foreign code
to another naming scheme.
</p>
<p>It is an error if the mapping between constructors and symbolic names
in a &lsquo;<samp>pragma foreign_export_enum</samp>&rsquo; declaration
does not form a bijection.
It is also an error
if two separate &lsquo;<samp>pragma foreign_export_enum</samp>&rsquo; declarations
for a given foreign language, <em>whether or not for the same type</em>,
specify the same symbolic name,
since in that case, the Mercury compiler would generate
two conflicting definitions for that symbolic name.
However, the Mercury implementation is not required to check either condition.
</p>
<p>A &lsquo;<samp>pragma foreign_export_enum</samp>&rsquo; declaration
may occur only in the implementation section of a module.
</p>

<hr>
<div class="header">
<p>
Next: <a href="Adding-foreign-declarations.html#Adding-foreign-declarations" accesskey="n" rel="next">Adding foreign declarations</a>, Previous: <a href="Using-foreign-enumerations-in-Mercury-code.html#Using-foreign-enumerations-in-Mercury-code" accesskey="p" rel="previous">Using foreign enumerations in Mercury code</a>, Up: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="u" rel="up">Foreign language interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
