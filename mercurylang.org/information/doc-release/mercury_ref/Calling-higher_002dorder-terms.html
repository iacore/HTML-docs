<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Calling higher-order terms</title>

<meta name="description" content="The Mercury Language Reference Manual: Calling higher-order terms">
<meta name="keywords" content="The Mercury Language Reference Manual: Calling higher-order terms">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Higher_002dorder.html#Higher_002dorder" rel="up" title="Higher-order">
<link href="Higher_002dorder-insts-and-modes.html#Higher_002dorder-insts-and-modes" rel="next" title="Higher-order insts and modes">
<link href="Creating-higher_002dorder-terms.html#Creating-higher_002dorder-terms" rel="previous" title="Creating higher-order terms">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Calling-higher_002dorder-terms"></a>
<div class="header">
<p>
Next: <a href="Higher_002dorder-insts-and-modes.html#Higher_002dorder-insts-and-modes" accesskey="n" rel="next">Higher-order insts and modes</a>, Previous: <a href="Creating-higher_002dorder-terms.html#Creating-higher_002dorder-terms" accesskey="p" rel="previous">Creating higher-order terms</a>, Up: <a href="Higher_002dorder.html#Higher_002dorder" accesskey="u" rel="up">Higher-order</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Calling-higher_002dorder-terms-1"></a>
<h3 class="section">8.2 Calling higher-order terms</h3>

<p>Once you have created a higher-order predicate term
(sometimes known as a closure),
the next thing you want to do is to call it.
For predicates, you use the builtin goal call/N:
</p>
<dl compact="compact">
<dt><code>call(Closure)</code></dt>
<dt><code>call(Closure1, Arg1)</code></dt>
<dt><code>call(Closure2, Arg1, Arg2)</code></dt>
<dt>&hellip;</dt>
<dd><p>A higher-order predicate call.  &lsquo;<samp>call(Closure)</samp>&rsquo; just calls the
specified higher-order predicate term.  The other forms append the
specified arguments onto the argument list of the closure before
calling it.
</p></dd>
</dl>

<p>For example, the goal
</p>
<div class="example">
<pre class="example">call(Sum123, Result)
</pre></div>

<p>would bind <code>Result</code> to the sum of &lsquo;<samp>[1, 2, 3]</samp>&rsquo;, i.e. to 6.
</p>
<p>For functions, you use the builtin expression apply/N:
</p>
<dl compact="compact">
<dt><code>apply(Closure)</code></dt>
<dt><code>apply(Closure1, Arg1)</code></dt>
<dt><code>apply(Closure2, Arg1, Arg2)</code></dt>
<dt>&hellip;</dt>
<dd><p>A higher-order function application.
Such a term denotes the result of
invoking the specified higher-order function term with the specified arguments.
</p></dd>
</dl>

<p>For example, given the definition of &lsquo;<samp>Double</samp>&rsquo; above, the goal
</p>
<div class="example">
<pre class="example">List = apply(Double, [1, 2, 3])
</pre></div>

<p>would be equivalent to
</p>
<div class="example">
<pre class="example">List = scalar_product(2, [1, 2, 3])
</pre></div>

<p>and so for a suitable implementation of the function &lsquo;<samp>scalar_product/2</samp>&rsquo;
this would bind <code>List</code> to &lsquo;<samp>[2, 4, 6]</samp>&rsquo;.
</p>
<p>One extremely useful higher-order predicate in the Mercury standard library
is &lsquo;<samp>solutions/2</samp>&rsquo;, which has the following declaration:
</p>
<div class="example">
<pre class="example">:- pred solutions(pred(T), list(T)).
:- mode solutions(pred(out) is nondet, out) is det.
</pre></div>

<p>The term which you pass to &lsquo;<samp>solutions/2</samp>&rsquo; is a higher-order predicate term.
You can pass the name of a one-argument predicate,
or you can pass a several-argument predicate
with all but one of the arguments supplied (a closure).
The declarative semantics of &lsquo;<samp>solutions/2</samp>&rsquo; can be defined as follows:
</p>
<div class="example">
<pre class="example">solutions(Pred, List) is true iff
    all [X] (call(Pred, X) &lt;=&gt; list.member(X, List))
    and List is sorted.
</pre></div>

<p>where &lsquo;<samp>call(Pred, X)</samp>&rsquo;
invokes the higher-order predicate term <code>Pred</code> with argument <code>X</code>,
and where &lsquo;<samp>list.member/2</samp>&rsquo;
is the standard library predicate for list membership.
In other words, &lsquo;<samp>solutions(Pred, List)</samp>&rsquo; finds all the values of <code>X</code>
for which &lsquo;<samp>call(Pred, X)</samp>&rsquo; is true,
collects these solutions in a list,
sorts the list, and returns that list as its result.
Here is an example: the standard library defines a predicate
&lsquo;<samp>list.perm(List0, List)</samp>&rsquo;
</p>
<div class="example">
<pre class="example">:- pred list.perm(list(T), list(T)).
:- mode list.perm(in, out) is nondet.
</pre></div>

<p>which succeeds iff List is a permutation of List0.
Hence the following call to solutions
</p>
<div class="example">
<pre class="example">solutions(list.perm([3,1,2]), L)
</pre></div>

<p>should return
all the possible permutations of the list &lsquo;<samp>[3,1,2]</samp>&rsquo; in sorted order:
</p>
<div class="example">
<pre class="example">L = [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]].
</pre></div>

<p>See also &lsquo;<samp>unsorted_solutions/2</samp>&rsquo; and &lsquo;<samp>solutions_set/2</samp>&rsquo;,
which are defined in the standard library module &lsquo;<samp>solutions</samp>&rsquo;
and documented in the Mercury Library Reference Manual.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Higher_002dorder-insts-and-modes.html#Higher_002dorder-insts-and-modes" accesskey="n" rel="next">Higher-order insts and modes</a>, Previous: <a href="Creating-higher_002dorder-terms.html#Creating-higher_002dorder-terms" accesskey="p" rel="previous">Creating higher-order terms</a>, Up: <a href="Higher_002dorder.html#Higher_002dorder" accesskey="u" rel="up">Higher-order</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
