<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Predicate and function type declarations</title>

<meta name="description" content="The Mercury Language Reference Manual: Predicate and function type declarations">
<meta name="keywords" content="The Mercury Language Reference Manual: Predicate and function type declarations">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Types.html#Types" rel="up" title="Types">
<link href="Field-access-functions.html#Field-access-functions" rel="next" title="Field access functions">
<link href="Subtypes.html#Subtypes" rel="previous" title="Subtypes">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Predicate-and-function-type-declarations"></a>
<div class="header">
<p>
Next: <a href="Field-access-functions.html#Field-access-functions" accesskey="n" rel="next">Field access functions</a>, Previous: <a href="User_002ddefined-types.html#User_002ddefined-types" accesskey="p" rel="previous">User-defined types</a>, Up: <a href="Types.html#Types" accesskey="u" rel="up">Types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Predicate-and-function-type-declarations-1"></a>
<h3 class="section">3.3 Predicate and function type declarations</h3>

<p>The argument types of each predicate
must be explicitly declared with a &lsquo;<samp>:- pred</samp>&rsquo; declaration.
The argument types and return type of each function must be
explicitly declared with a &lsquo;<samp>:- func</samp>&rsquo; declaration.
For example:
</p>
<div class="example">
<pre class="example">:- pred is_all_uppercase(string).

:- func strlen(string) = int.
</pre></div>

<p>Predicates and functions can be polymorphic;
that is, their declarations can include type variables.
For example:
</p>
<div class="example">
<pre class="example">:- pred member(T, list(T)).

:- func length(list(T)) = int.
</pre></div>

<p>A predicate or function can be declared
to have a given higher-order type (see <a href="Higher_002dorder.html#Higher_002dorder">Higher-order</a>)
by using an explicit type qualification in the type declaration.
This is useful where several predicates or functions
need to have the same type signature,
which often occurs for type class method implementations
(see <a href="Type-classes.html#Type-classes">Type classes</a>),
and for predicates to be passed as higher-order terms.
</p>
<p>For example,
</p>
<div class="example">
<pre class="example">:- type foldl_pred(T, U) == pred(T, U, U).
:- type foldl_func(T, U) == (func(T, U) = U).

:- pred p(int) : foldl_pred(T, U).
:- func f(int) : foldl_func(T, U).
</pre></div>

<p>is equivalent to
</p>
<div class="example">
<pre class="example">:- pred p(int, T, U, U).
:- pred f(int, T, U) = U.
</pre></div>

<p>Type variables in predicate and function declarations
are implicitly universally quantified by default;
that is, the predicate or function may be called with arguments
and (in the case of functions) return value
whose actual types are any instance of the types specified in the declaration.
For example, the function &lsquo;<samp>length/1</samp>&rsquo; declared above
could be called with the argument having type
&lsquo;<samp>list(int)</samp>&rsquo;, or &lsquo;<samp>list(float)</samp>&rsquo;, or &lsquo;<samp>list(list(int))</samp>&rsquo;, etc.
</p>
<p>Type variables in predicate and function declarations can
also be existentially quantified; this is discussed in
<a href="Existential-types.html#Existential-types">Existential types</a>.
</p>
<p>There must only be one predicate with a given name and arity in each module,
and only one function with a given name and arity in each module.
It is an error to declare the same predicate or function twice.
</p>
<p>There must be at least one clause defined
for each declared predicate or function,
except for those defined using the foreign language interface
(see <a href="Foreign-language-interface.html#Foreign-language-interface">Foreign language interface</a>).
However, Mercury implementations are permitted to provide a method
of processing Mercury programs in which such errors are not reported
until and unless the predicate or function is actually called.
(The Melbourne Mercury implementation provides this
with its &lsquo;<samp>--allow-stubs</samp>&rsquo; option.
This can be useful during program development,
since it allows you to execute parts of a program
while the program&rsquo;s implementation is still incomplete.)
</p>
<p>Note that a predicate defined using DCG notation (see <a href="DCG_002drules.html#DCG_002drules">DCG-rules</a>)
will appear to be defined with two fewer arguments than it is declared with.
It will also appear to be called with two fewer arguments
when called from predicates defined using DCG notation.
However, when called from an ordinary predicate or function,
it must have all the arguments it was declared with.
</p>
<p>The compiler infers the types of data-terms,
and in particular the types of variables
and overloaded constructors, functions, and predicates.
A <em>type assignment</em> is an assignment of a type to every variable,
and of a particular constructor, function, or predicate
to every name in a clause.
A type assignment is <em>valid</em> if it satisfies the following conditions.
</p>
<p>Each constructor in a clause
must have been declared in at least one visible type declaration.
The type assigned to each constructor term
must match one of the type declarations for that constructor,
and the types assigned to the arguments of that constructor
must match the argument types specified in that type declaration.
</p>
<p>The type assigned to each function call term
must match the return type of one of the &lsquo;<samp>:- func</samp>&rsquo; declarations
for that function, and the types assigned to the arguments of that function
must match the argument types specified in that type declaration.
</p>
<p>The type assigned to each predicate argument must match
the type specified in one of the &lsquo;<samp>:- pred</samp>&rsquo; declarations
for that predicate.
The type assigned to each head argument in a predicate clause
must exactly match the argument type specified
in the corresponding &lsquo;<samp>:- pred</samp>&rsquo; declaration.
</p>
<p>The type assigned to each head argument in a function clause
must exactly match the argument type
specified in the corresponding &lsquo;<samp>:- func</samp>&rsquo; declaration,
and the type assigned to the result term in a function clause
must exactly match the result type specified
in the corresponding &lsquo;<samp>:- func</samp>&rsquo; declaration.
</p>
<p>The type assigned to each data-term with an explicit type qualification
(see <a href="Explicit-type-qualification.html#Explicit-type-qualification">Explicit type qualification</a>)
must match the type specified by the type qualification
expression<a name="DOCF2" href="#FOOT2"><sup>2</sup></a>.
</p>
<p>(Here &ldquo;match&rdquo; means to be an instance of,
i.e. to be identical to for some substitution of the type parameters,
and &ldquo;exactly match&rdquo; means to be identical up to renaming of type parameters.)
</p>
<p>One type assignment <var>A</var> is said to be
<em>more general</em> than another type assignment <var>B</var>
if there is a binding of the type parameters in <var>A</var>
that makes it identical (up to renaming of parameters) to <var>B</var>.
If there is more than one valid type assignment,
the compiler must choose the most general one.
If there are two valid type assignments which are not identical up to renaming
and neither of which is more general than the other,
then there is a type ambiguity, and compiler must report an error.
A clause is <em>type-correct</em>
if there is a unique (up to renaming) most general valid type assignment.
Every clause in a Mercury program must be type-correct.
</p>
<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h3><a name="FOOT2" href="#DOCF2">(2)</a></h3>
<p>The type of an explicitly
type qualified term may be an instance of the type specified by the
qualifier. This allows explicit type qualifications to constrain the
types of two data-terms to be identical, without knowing the exact types
of the data-terms. It also allows type qualifications to refer to the
types of the results of existentially typed predicates or functions.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Field-access-functions.html#Field-access-functions" accesskey="n" rel="next">Field access functions</a>, Previous: <a href="User_002ddefined-types.html#User_002ddefined-types" accesskey="p" rel="previous">User-defined types</a>, Up: <a href="Types.html#Types" accesskey="u" rel="up">Types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
