<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Predicate and function mode declarations</title>

<meta name="description" content="The Mercury Language Reference Manual: Predicate and function mode declarations">
<meta name="keywords" content="The Mercury Language Reference Manual: Predicate and function mode declarations">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Modes.html#Modes" rel="up" title="Modes">
<link href="Constrained-polymorphic-modes.html#Constrained-polymorphic-modes" rel="next" title="Constrained polymorphic modes">
<link href="Insts-modes-and-mode-definitions.html#Insts-modes-and-mode-definitions" rel="previous" title="Insts modes and mode definitions">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Predicate-and-function-mode-declarations"></a>
<div class="header">
<p>
Next: <a href="Constrained-polymorphic-modes.html#Constrained-polymorphic-modes" accesskey="n" rel="next">Constrained polymorphic modes</a>, Previous: <a href="Insts-modes-and-mode-definitions.html#Insts-modes-and-mode-definitions" accesskey="p" rel="previous">Insts modes and mode definitions</a>, Up: <a href="Modes.html#Modes" accesskey="u" rel="up">Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Predicate-and-function-mode-declarations-1"></a>
<h3 class="section">4.2 Predicate and function mode declarations</h3>

<p>A <em>predicate mode declaration</em>
assigns a mode mapping to each argument of a predicate.
A <em>function mode declaration</em>
assigns a mode mapping to each argument of a function,
and a mode mapping to the function result.
Each mode of a predicate or function is called a <em>procedure</em>.
For example, given the mode names defined by
</p>
<div class="example">
<pre class="example">:- mode out_listskel == free &gt;&gt; listskel.
:- mode in_listskel == listskel &gt;&gt; listskel.
</pre></div>

<p>the (type and) mode declarations
of the function &lsquo;<samp>length</samp>&rsquo; and predicate &lsquo;<samp>append</samp>&rsquo; are as follows:
</p>
<div class="example">
<pre class="example">:- func length(list(T)) = int.
:- mode length(in_listskel) = out.
:- mode length(out_listskel) = in.

:- pred append(list(T), list(T), list(T)).
:- mode append(in, in, out).
:- mode append(out, out, in).
</pre></div>

<p>Note that functions may have more than one mode, just like predicates;
functions can be reversible.
</p>
<p>Alternately, the mode declarations for &lsquo;<samp>length</samp>&rsquo;
could use the standard library modes &lsquo;<samp>in/1</samp>&rsquo; and &lsquo;<samp>out/1</samp>&rsquo;:
</p>
<div class="example">
<pre class="example">:- func length(list(T)) = int.
:- mode length(in(listskel)) = out.
:- mode length(out(listskel)) = in.
</pre></div>

<p>As for type declarations,
a predicate or function can be defined
to have a given higher-order inst (see <a href="Higher_002dorder-insts-and-modes.html#Higher_002dorder-insts-and-modes">Higher-order insts and modes</a>)
by using <code>`with_inst`</code> in the mode declaration.
</p>
<p>For example,
</p>
<div class="example">
<pre class="example">:- inst foldl_pred == (pred(in, in, out) is det).
:- inst foldl_func == (func(in, in) = out is det).

:- mode p(in) `with_inst` foldl_pred.
:- mode f(in) `with_inst` foldl_func.
</pre></div>

<p>is equivalent to
</p>
<div class="example">
<pre class="example">:- mode p(in, in, in, out) is det.
:- mode f(in, in, in) = out is det.
</pre></div>

<p>(&lsquo;<samp>is det</samp>&rsquo; is explained in <a href="Determinism.html#Determinism">Determinism</a>.)
</p>
<p>If a predicate or function has only one mode,
the &lsquo;<samp>pred</samp>&rsquo; and &lsquo;<samp>mode</samp>&rsquo; declaration can be combined:
</p>
<div class="example">
<pre class="example">:- func length(list(T)::in) = (int::out).
:- pred append(list(T)::in, list(T)::in, list(T)::out).

:- pred p `with_type` foldl_pred(T, U) `with_inst` foldl_pred.
</pre></div>

<p>It is an error for a predicate or function
whose &lsquo;<samp>pred</samp>&rsquo; and &lsquo;<samp>mode</samp>&rsquo; declarations are so combined
to have any other separate &lsquo;<samp>mode</samp>&rsquo; declarations.
</p>
<p>If there is no mode declaration for a function,
the compiler assumes a default mode for the function
in which all the arguments have mode <code>in</code>
and the result of the function has mode <code>out</code>.
(However, there is no requirement that a function have such a mode;
if there is any explicit mode declaration, it overrides the default.)
</p>
<p>If a predicate or function type declaration
occurs in the interface section of a module,
then all mode declarations for that predicate or function
must occur in the interface section of the <em>same</em> module.
Likewise, if a predicate or function type declaration
occurs in the implementation section of a module,
then all mode declarations for that predicate or function
must occur in the implementation section of the <em>same</em> module.
Therefore, is an error
for a predicate or function to have mode declarations
in both the interface and implementation sections of a module.
</p>
<p>A function or predicate mode declaration is an assertion by the programmer
that for all possible argument terms and (if applicable) result term
for the function or predicate
that are approximated (in our technical sense)
by the initial instantiatedness trees of the mode declaration
and all of whose free variables are distinct,
if the function or predicate succeeds, then
the resulting binding of those argument terms and (if applicable)
result term will in turn be approximated
by the final instantiatedness trees of the mode declaration,
with all free variables again being distinct.
We refer to such assertions as <em>mode declaration constraints</em>.
These assertions are checked by the compiler, which rejects programs
if it cannot prove that their mode declaration constraints are satisfied.
</p>
<p>Note that with the usual definition of &lsquo;<samp>append</samp>&rsquo;, the mode
</p>
<div class="example">
<pre class="example">:- mode append(in_listskel, in_listskel, out_listskel).
</pre></div>

<p>would not be allowed,
since it would create aliasing between the different arguments &mdash;
on success of the predicate, the list elements would be free variables,
but they would not be distinct.
</p>
<p>In Mercury it is always possible to call a procedure with an argument
that is more bound than the initial inst specified for that argument
in the procedure&rsquo;s mode declaration.
In such cases, the compiler will insert additional unifications
to ensure that the argument actually passed to the procedure
will have the inst specified.
For example, if the predicate <code>p/1</code> has mode &lsquo;<samp>p(out)</samp>&rsquo;,
you can still call &lsquo;<samp>p(X)</samp>&rsquo; if <code>X</code> is ground.
The compiler will transform this code to &lsquo;<samp>p(Y), X = Y</samp>&rsquo;
where <code>Y</code> is a fresh variable.
It is almost as if the predicate <code>p/1</code> has another mode &lsquo;<samp>p(in)</samp>&rsquo;;
we call such modes &ldquo;implied modes&rdquo;.
</p>
<p>To make this concept precise, we introduce the following definition.
A term <em>satisfies</em> an instantiatedness tree
if for every node in the instantiatedness tree,
</p>
<ul>
<li> if the node is &ldquo;free&rdquo;,
then the corresponding node in the term (if any)
is either a distinct free variable,
or a function symbol.

</li><li> if the node is &ldquo;bound&rdquo;,
then the corresponding node in the term (if any)
is a function symbol.

</li></ul>

<p>The <em>mode set</em> for a predicate or function
is the set of mode declarations for the predicate or function.
A mode set is an assertion by the programmer
that the predicate should only be called with argument terms
that satisfy the initial instantiatedness trees
of one of the mode declarations in the set
(i.e. the specified modes and the modes they imply
are the only allowed modes for this predicate or function).
We refer to the assertion associated with a mode set
as the <em>mode set constraint</em>;
these are also checked by the compiler.
</p>
<p>A predicate or function <var>p</var> is
<em>well-moded with respect to a given mode declaration</em>
if given that the predicates and functions called by <var>p</var>
all satisfy their mode declaration constraints,
there exists an ordering of the conjuncts in each conjunction
in the clauses of <var>p</var> such that
</p>
<ul>
<li> <var>p</var> satisfies its mode declaration constraint, and
</li><li> <var>p</var> satisfies the mode set constraint of all of the predicates and
functions it calls
</li></ul>

<p>We say that a predicate or function is well-moded
if it is well-moded with respect to
all the mode declarations in its mode set,
and we say that a program is well-moded
if all its predicates and functions are well-moded.
</p>
<p>The mode analysis algorithm checks one procedure at a time.
It abstractly interprets the definition of the predicate or function,
keeping track of the instantiatedness of each variable,
and selecting a mode for each call and unification in the definition.
To ensure that
the mode set constraints of called predicates and functions are satisfied,
the compiler may reorder the elements of conjunctions;
it reports an error if no satisfactory order exists.
Finally it checks that
the resulting instantiatedness of the procedure&rsquo;s arguments
is the same as the one given by the procedure&rsquo;s declaration.
</p>
<p>The mode analysis algorithm annotates each call with the mode used.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Constrained-polymorphic-modes.html#Constrained-polymorphic-modes" accesskey="n" rel="next">Constrained polymorphic modes</a>, Previous: <a href="Insts-modes-and-mode-definitions.html#Insts-modes-and-mode-definitions" accesskey="p" rel="previous">Insts modes and mode definitions</a>, Up: <a href="Modes.html#Modes" accesskey="u" rel="up">Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
