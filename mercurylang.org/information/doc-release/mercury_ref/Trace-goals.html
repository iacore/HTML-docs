<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Trace goals</title>

<meta name="description" content="The Mercury Language Reference Manual: Trace goals">
<meta name="keywords" content="The Mercury Language Reference Manual: Trace goals">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html#Top" rel="up" title="Top">
<link href="Pragmas.html#Pragmas" rel="next" title="Pragmas">
<link href="Solver-types-and-negated-contexts.html#Solver-types-and-negated-contexts" rel="previous" title="Solver types and negated contexts">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Trace-goals"></a>
<div class="header">
<p>
Next: <a href="Pragmas.html#Pragmas" accesskey="n" rel="next">Pragmas</a>, Previous: <a href="Solver-types.html#Solver-types" accesskey="p" rel="previous">Solver types</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Trace-goals-1"></a>
<h2 class="chapter">18 Trace goals</h2>

<p>Sometimes, programmers find themselves
needing to perform some side-effects in the middle of declarative code.
One example is an operation that takes so long that
users may think the program has gone into an infinite loop:
periodically printing a progress message can give them reassurance.
Another example is a program that is
too long-running for its behaviour to be analyzed via debuggers
and too complex for analysis via profilers;
a programmable logging facility generating data
for analysis by a specially-written program may be the best option.
However, inserting arbitrary side effects into declarative code
is against the spirit of Mercury.
Trace goals exist to provide a mechanism
to code these side effects in a disciplined fashion.
</p>
<p>The format of trace goals is <code>trace <var>Params</var> <var>Goal</var></code>.
<var>Goal</var> must be a valid goal;
<var>Params</var> must be a valid list of one or more trace parameters.
The following example shows all four of the available kinds of parameters:
&lsquo;<samp>compile_time</samp>&rsquo;, &lsquo;<samp>run_time</samp>&rsquo;, &lsquo;<samp>io</samp>&rsquo; and &lsquo;<samp>state</samp>&rsquo;.
(In practice, it is far more typical to have just one parameter, &lsquo;<samp>io</samp>&rsquo;.)
</p>
<div class="example">
<pre class="example">:- mutable(logging_level, int, 0, ground, []).

:- pred time_consuming_task(data::in, result::out) is det.

time_consuming_task(In, Out) :-
    trace [
        compile_time(flag(&quot;do_logging&quot;) or grade(debug)),
        run_time(env(&quot;VERBOSE&quot;)),
        io(!IO),
        state(logging_level, !LoggingLevel)
    ] (
        io.write_string(&quot;Time_consuming_task start\n&quot;, !IO),
        ( !.LoggingLevel &gt; 1 -&gt;
            io.write_string(&quot;Input is &quot;, !IO),
            io.write(In, !IO),
            io.nl(!IO)
        ;
            true
        )
    ),
    &hellip;
    % perform the actual task
</pre></div>

<p>The &lsquo;<samp>compile_time</samp>&rsquo; parameter says under what circumstances
the trace goal should be included in the executable program.
In the example, at least one of two conditions has to be true:
either this module has to be compiled
with the option &lsquo;<samp>--trace-flag=do_logging</samp>&rsquo;,
or it has to be compiled in a debugging grade.
</p>
<p>In general, the single argument of the &lsquo;<samp>compile_time</samp>&rsquo; function symbol
is a boolean expression of primitive compile-time conditions.
Valid boolean operators in these expressions
are &lsquo;<samp>and</samp>&rsquo;, &lsquo;<samp>or</samp>&rsquo; and &lsquo;<samp>not</samp>&rsquo;.
There are three kinds of primitive compile-time conditions.
The first has the form &lsquo;<samp>flag(<var>FlagName</var>)</samp>&rsquo;,
where <var>FlagName</var> is an arbitrary name picked by the programmer;
this condition is true
if the module is compiled with the option &lsquo;<samp>--trace-flag=<var>FlagName</var></samp>&rsquo;.
The second has the form &lsquo;<samp>tracelevel(shallow)</samp>&rsquo;, or &lsquo;<samp>tracelevel(deep)</samp>&rsquo;;
this condition is true (irrespective of grade)
if the module is compiled with at least the specified trace level.
The third has the form &lsquo;<samp>grade(GradeTest)</samp>&rsquo;.
The supported &lsquo;<samp>GradeTests</samp>&rsquo;s and their meanings are as follows.
</p>
<dl compact="compact">
<dt>&lsquo;<samp>debug</samp>&rsquo;</dt>
<dd><p>True if the module is compiled with execution tracing enabled.
</p></dd>
<dt>&lsquo;<samp>ssdebug</samp>&rsquo;</dt>
<dd><p>True if the module is compiled with source-to-source debugging enabled.
</p></dd>
<dt>&lsquo;<samp>prof</samp>&rsquo;</dt>
<dd><p>True if the module is compiled with non-deep profiling enabled.
</p></dd>
<dt>&lsquo;<samp>profdeep</samp>&rsquo;</dt>
<dd><p>True if the module is compiled with deep profiling enabled.
</p></dd>
<dt>&lsquo;<samp>par</samp>&rsquo;</dt>
<dd><p>True if the module is compiled with parallel execution enabled.
</p></dd>
<dt>&lsquo;<samp>trail</samp>&rsquo;</dt>
<dd><p>True if the module is compiled with trailing enabled.
</p></dd>
<dt>&lsquo;<samp>llds</samp>&rsquo;</dt>
<dd><p>True if the module is compiled with &lsquo;<samp>--highlevel-code</samp>&rsquo; disabled.
</p></dd>
<dt>&lsquo;<samp>mlds</samp>&rsquo;</dt>
<dd><p>True if the module is compiled with &lsquo;<samp>--highlevel-code</samp>&rsquo; enabled.
</p></dd>
<dt>&lsquo;<samp>c</samp>&rsquo;</dt>
<dd><p>True if the target language of the compilation is C.
</p></dd>
<dt>&lsquo;<samp>csharp</samp>&rsquo;</dt>
<dd><p>True if the target language of the compilation is C#.
</p></dd>
<dt>&lsquo;<samp>java</samp>&rsquo;</dt>
<dd><p>True if the target language of the compilation is Java.
</p></dd>
</dl>

<p>The &lsquo;<samp>run_time</samp>&rsquo; parameter says under what circumstances the trace goal,
if included in the executable program, should actually be executed.
In this case, the environment variable &lsquo;<samp>VERBOSE</samp>&rsquo; has be to set
when the program starts execution.
(It doesn&rsquo;t matter what value it is set to.)
</p>
<p>In general, the single argument of the &lsquo;<samp>run_time</samp>&rsquo; function symbol
is a boolean expression of primitive run-time conditions.
Valid boolean operators in these expressions
are &lsquo;<samp>and</samp>&rsquo;, &lsquo;<samp>or</samp>&rsquo; and &lsquo;<samp>not</samp>&rsquo;.
There is just one primitive run-time condition.
It has the form &lsquo;<samp>env(<var>EnvVarName</var>)</samp>&rsquo;,
this condition is true
if the environment variable <var>EnvVarName</var> exists
when the program starts execution.
</p>
<p>The &lsquo;<samp>compile_time</samp>&rsquo; and &lsquo;<samp>run_time</samp>&rsquo; parameters
may not appear in the parameter list more than once;
programmers who want more than one condition
have to specify how (with what boolean operators)
their values should be combined.
However, it is ok for them not to appear in the parameter list at all.
If there is no &lsquo;<samp>compile_time</samp>&rsquo; parameter,
the trace goal is always compiled into the executable;
if there is no &lsquo;<samp>run_time</samp>&rsquo; parameter,
the trace goal is always executed (if it is compiled into the executable).
</p>
<p>Since the trace goal may end up
either not compiled into the executable or just not executed,
it cannot bind any variables that occur in the surrounding code.
(If it were allowed to bind such variables,
then those variables would stay unbound
if either the compile time or the run time condition were false.)
This greatly restricts what trace goals can do.
</p>
<p>The usual reason for including a trace goal
in a procedure body is to perform some I/O,
which requires access to the I/O state.
The &lsquo;<samp>io</samp>&rsquo; parameter supplies this access.
Its argument must be the name of a state variable prefixed by &lsquo;<samp>!</samp>&rsquo;;
by convention, it is usually &lsquo;<samp>!IO</samp>&rsquo;.
The language implementation supplies
the initial unique value of the I/O state
as the value of &lsquo;<samp>!.IO</samp>&rsquo; at the start of the trace goal;
it requires the trace goal to give back
the final unique value of the I/O state
as the value of &lsquo;<samp>!.IO</samp>&rsquo; current at the end of the trace goal.
</p>
<p>Note that trace goals that wish to do I/O
must include this parameter in their parameter list
<em>even if</em> the surrounding code already has access to an I/O state.
This is because otherwise,
doing any I/O inside the trace goal
would destroy the value of the current I/O state,
changing the instantiation state of the variable holding it,
and trace goals are not allowed to do that.
</p>
<p>The &lsquo;<samp>io</samp>&rsquo; parameter may appear in the parameter list at most once,
since it doesn&rsquo;t make sense to have
two copies of the I/O state available to the trace goal.
</p>
<p>Besides doing I/O, trace goals may read and possibly write
the values of mutable variables.
Each mutable the trace goal wants access to should be listed
in its own &lsquo;<samp>state</samp>&rsquo; parameter
(which may therefore appear in the parameter list more than once).
Each &lsquo;<samp>state</samp>&rsquo; parameter has two arguments:
the first gives the name of the mutable,
while the second must be the name of a state variable prefixed by &lsquo;<samp>!</samp>&rsquo;,
e.g. &lsquo;<samp>!LoggingLevel</samp>&rsquo;.
The language implementation supplies
the initial value of the mutable
as the value of (in this case) &lsquo;<samp>!.LoggingLevel</samp>&rsquo;
at the start of the trace goal;
at the end of the trace goal,
it puts the value of &lsquo;<samp>!.LoggingLevel</samp>&rsquo; current then
back into the mutable.
</p>
<p>The intention here is that trace goals
should be able to access mutables that give them information
about the parameters within which they should operate.
The ability of trace goals to actually <em>update</em> the values of mutables
is intended to allow the implementation of trace goals
whose actions depend on the actions executed by previous trace goals.
For example, a trace goal could test
whether the current input value is the same as the previous input value,
and if it is, then it can say so instead of printing the value out again.
Another possibility is a progress message
which is printed not for every item processed, but for every 1000th item,
reassuring users without overwhelming them with a deluge of output.
</p>
<p>This kind of code is the <em>only</em> intended use of this ability.
Any program in which the value of a mutable set by a trace goal
is inspected by code that is not itself within a trace goal
is explicitly violating the intended uses of trace goals.
Only the difficulty of implementing the required global program analysis
prevents the language design from outlawing such programs in the first place.
</p>
<p>The compiler will not delete trace goals
from the bodies of the procedures containing them.
However, trace goals inside a procedure don&rsquo;t prevent
calls to that procedure from being optimized away,
if such optimization is otherwise possible.
(There is no point in debugging or logging
operations that don&rsquo;t actually occur.)
In their effect on program optimizations,
trace goals function as a kind of impure code,
but one with an implicit promise_pure around the clause in which they occur.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Pragmas.html#Pragmas" accesskey="n" rel="next">Pragmas</a>, Previous: <a href="Solver-types.html#Solver-types" accesskey="p" rel="previous">Solver types</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
