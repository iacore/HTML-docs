<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury Language Reference Manual: Discriminated unions</title>

<meta name="description" content="The Mercury Language Reference Manual: Discriminated unions">
<meta name="keywords" content="The Mercury Language Reference Manual: Discriminated unions">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="User_002ddefined-types.html#User_002ddefined-types" rel="up" title="User-defined types">
<link href="Equivalence-types.html#Equivalence-types" rel="next" title="Equivalence types">
<link href="User_002ddefined-types.html#User_002ddefined-types" rel="previous" title="User-defined types">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Discriminated-unions"></a>
<div class="header">
<p>
Next: <a href="Equivalence-types.html#Equivalence-types" accesskey="n" rel="next">Equivalence types</a>, Up: <a href="User_002ddefined-types.html#User_002ddefined-types" accesskey="u" rel="up">User-defined types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<a name="Discriminated-unions-1"></a>
<h4 class="subsection">3.2.1 Discriminated unions</h4>

<p>These encompass both enumeration and record types in other languages.
A derived type is defined using &lsquo;<samp>:- type <var>type</var> ---&gt; <var>body</var></samp>&rsquo;.
(Note there are <em>three</em> dashes in that arrow.
It should not be confused with the two-dash arrow used for DCGs
or the one-dash arrow used for if-then-else.)
If the <var>type</var> term is a functor of arity zero
(i.e. one having zero arguments),
it names a monomorphic type.
Otherwise, it names a polymorphic type;
the arguments of the functor must be distinct type variables.
The <var>body</var> term is defined as
a sequence of constructor definitions separated by semicolons.
</p>
<p>Ordinarily, each constructor definition
must be a functor whose arguments (if any) are types.
Ordinary discriminated union definitions must be <em>transparent</em>:
all type variables occurring in the <var>body</var>
must also occur in the <var>type</var>.
(The reverse is not the case:
a variable occurring in the <var>type</var>
need not also occur in the <var>body</var>.
Such variables are called &lsquo;<samp>phantom type parameters</samp>&rsquo;,
and their use is explained below.)
</p>
<p>However, constructor definitions can optionally be existentially typed.
In that case, the functor will be preceded by an existential type quantifier
and can optionally be followed by an existential type class constraint.
For details, see <a href="Existential-types.html#Existential-types">Existential types</a>.
Existentially typed discriminated union definitions need not be transparent.
</p>
<p>The arguments of constructor definitions may be labelled.
These labels cause the compiler to generate functions
which can be used to conveniently select and update fields of a term
in a manner independent of the definition of the type
(see <a href="Field-access-functions.html#Field-access-functions">Field access functions</a>).
A labelled argument has the form <code><var>fieldname</var>&nbsp;::&nbsp;<var>Type</var></code><!-- /@w -->.
It is an error for two fields in the same type to have the same label.
</p>
<p>Here are some examples of discriminated union definitions:
</p>
<div class="example">
<pre class="example">:- type fruit
        ---&gt;    apple
        ;       orange
        ;       banana
        ;       pear.

:- type strange
        ---&gt;    foo(int)
        ;       bar(string).

:- type employee
        ---&gt;    employee(
                       name        :: string,
                       age         :: int,
                       department  :: string
                ).

:- type tree
        ---&gt;    empty
        ;       leaf(int)
        ;       branch(tree, tree).

:- type list(T)
        ---&gt;    []
        ;       [T | list(T)].

:- type pair(T1, T2)
        ---&gt;    T1 - T2.
</pre></div>

<p>If the body of a discriminated union type definition
contains a term whose top-level functor is <code>';'/2</code>,
the semicolon is normally assumed to be a separator.
This makes it difficult to define a type
whose constructors include <code>';'/2</code>.
To allow this, curly braces can be used to quote the semicolon.
It is then also necessary to quote curly braces.
The following example illustrates this:
</p>
<div class="example">
<pre class="example">:- type tricky
        ---&gt;    { int ; int }
        ;       { { int } }.
</pre></div>

<p>This defines a type with two constructors,
<code>';'/2</code> and <code>'{}'/1</code>, whose argument types are all <code>int</code>.
We recommend against using constructors named <code>'{}'</code>
because of the possibility of confusion with the builtin tuple types.
</p>
<p>Each discriminated union type definition introduces a distinct type.
Mercury considers two discriminated union types that have the same bodies
to be distinct types (name equivalence).
Having two different definitions of a type
with the same name and arity in the same module is an error.
</p>
<p>Constructors may be overloaded among different types:
there may be any number of constructors with a given name and arity,
so long as they all have different types.
However, there must not be more than one constructor
with the same name, arity, and result type in the same module.
(There is no particularly good reason for this restriction;
in the future we may allow several such functors
as long as they have different argument types.)
Note that excessive overloading of constructors
can slow down type checking
and can make the program confusing for human readers,
so overloading should not be over-used.
</p>


<p>Note that user defined types may not have names that have meanings in Mercury.
(Most of these are documented in later sections.)
</p>
<p>The list of reserved type names is
</p><div class="example">
<pre class="example">int
int8
int16
int32
int64
uint
uint8
uint16
uint32
uint64
float
character
string
{}
=
=&lt;
pred
func
pure
semipure
impure
''
</pre></div>

<p>Phantom type parameters are useful when you have
two distinct concepts that you want to keep separate,
but for which nevertheless you want to use the representation.
This is an example of their use, taken from the Mercury compiler:
</p>
<div class="example">
<pre class="example">:- type var(T)
        ---&gt;    ...

:- type prog_var == var(prog_var_type).
:- type type_var == var(type_var_type).

:- type prog_var_type
    ---&gt;    prog_var_type.
:- type type_var_type
    ---&gt;    type_var_type.
</pre></div>

<p>The <code>var</code> type represents the generic notion of a variable.
It has a phantom type parameter, <code>T</code>,
which does not occur in the body of its type definition.
The <code>prog_var</code> and <code>type_var</code> types represent
two different specific kinds of variables:
program variables, which occur in code (clauses),
and type variables, which occur in types, respectively.
They each bind a different type to the type parameter <code>T</code>.
These types, <code>prog_var_type</code> and <code>type_var_type</code>,
each have a single function symbol of arity zero.
This means that each type has only one value,
which in turn means that values of these types contain no information at all.
But containing information is not the purpose of these types.
Their purpose is to ensure that
if ever a computation that expects program variables
is ever accidentally given type variables, or vice versa,
this mismatch is detected and reported by the compiler.
Two variables can be unified only if they have the same type.
While two <code>prog_var</code>s have the same type,
and two <code>type_var</code>s have the same type,
a <code>prog_var</code> and <code>type_var</code> have different types,
due to having different types
(<code>prog_var_type</code> and <code>type_var_type</code>)
being bound to the phantom type parameter <code>T</code>.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Equivalence-types.html#Equivalence-types" accesskey="n" rel="next">Equivalence types</a>, Up: <a href="User_002ddefined-types.html#User_002ddefined-types" accesskey="u" rel="up">User-defined types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
