<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury User&rsquo;s Guide: Preparing a program for debugging</title>

<meta name="description" content="The Mercury User&rsquo;s Guide: Preparing a program for debugging">
<meta name="keywords" content="The Mercury User&rsquo;s Guide: Preparing a program for debugging">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Debugging.html#Debugging" rel="up" title="Debugging">
<link href="Tracing-optimized-code.html#Tracing-optimized-code" rel="next" title="Tracing optimized code">
<link href="Tracing-of-Mercury-programs.html#Tracing-of-Mercury-programs" rel="previous" title="Tracing of Mercury programs">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Preparing-a-program-for-debugging"></a>
<div class="header">
<p>
Next: <a href="Tracing-optimized-code.html#Tracing-optimized-code" accesskey="n" rel="next">Tracing optimized code</a>, Previous: <a href="Tracing-of-Mercury-programs.html#Tracing-of-Mercury-programs" accesskey="p" rel="previous">Tracing of Mercury programs</a>, Up: <a href="Debugging.html#Debugging" accesskey="u" rel="up">Debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Preparing-a-program-for-debugging-1"></a>
<h3 class="section">7.4 Preparing a program for debugging</h3>

<p>When you compile a Mercury program, you can specify
whether you want to be able to run the Mercury debugger on the program or not.
If you do, the compiler embeds calls to the Mercury debugging system
into the executable code of the program,
at the execution points that represent trace events.
At each event, the debugging system decides
whether to give control back to the executable immediately,
or whether to first give control to you,
allowing you to examine the state of the computation and issue commands.
</p>
<p>Mercury supports two broad ways of preparing a program for debugging.
The simpler way is to compile a program in a debugging grade,
which you can do directly by specifying a grade
that includes the word &ldquo;debug&rdquo; or &ldquo;decldebug&rdquo;
(e.g. &lsquo;<samp>asm_fast.gc.debug</samp>&rsquo;, or &lsquo;<samp>asm_fast.gc.decldebug</samp>&rsquo;),
or indirectly by specifying one of the &lsquo;<samp>--debug</samp>&rsquo; or &lsquo;<samp>--decl-debug</samp>&rsquo;
grade options to the compiler, linker, and other tools
(in particular <code>mmc</code>, <code>mgnuc</code>, <code>ml</code>, and <code>c2init</code>).
If you follow this way,
and accept the default settings of the various compiler options
that control the selection of trace events (which are described below),
you will be assured of being able to get control
at every execution point that represents a potential trace event,
which is very convenient.
</p>
<p>The &ldquo;decldebug&rdquo; grades improve declarative debugging by allowing the user
to track the source of subterms (see <a href="Improving-the-search.html#Improving-the-search">Improving the search</a>).
Doing this increases the size of executables,
so these grades should only be used when you need
the subterm dependency tracking feature of the declarative debugger.
Note that declarative debugging,
with the exception of the subterm dependency tracking features,
also works in the .debug grades.
</p>

<p>The two drawbacks of using a debugging grade
are the large size of the resulting executables,
and the fact that often you discover that you need to debug a big program
only after having built it in a non-debugging grade.
This is why Mercury also supports another way
to prepare a program for debugging,
one that does not require the use of a debugging grade.
With this way, you can decide, individually for each module,
which of four trace levels,
&lsquo;<samp>none</samp>&rsquo;, &lsquo;<samp>shallow</samp>&rsquo;, &lsquo;<samp>deep</samp>&rsquo;, and &lsquo;<samp>rep</samp>&rsquo;
you want to compile them with:
</p>
<a name="index-debugger-trace-level"></a>
<a name="index-trace-level"></a>
<a name="index-shallow-tracing"></a>
<a name="index-deep-tracing"></a>
<dl compact="compact">
<dt>&lsquo;<samp>none</samp>&rsquo;</dt>
<dd><p>A procedure compiled with trace level &lsquo;<samp>none</samp>&rsquo;
will never generate any events.
</p></dd>
<dt>&lsquo;<samp>deep</samp>&rsquo;</dt>
<dd><p>A procedure compiled with trace level &lsquo;<samp>deep</samp>&rsquo;
will always generate all the events requested by the user.
By default, this is all possible events,
but you can tell the compiler that
you are not interested in some kinds of events
via compiler options (see below).
However, declarative debugging requires all events to be generated
if it is to operate properly,
so do not disable the generation of any event types
if you want to use declarative debugging.
For more details see <a href="Declarative-debugging.html#Declarative-debugging">Declarative debugging</a>.
</p></dd>
<dt>&lsquo;<samp>rep</samp>&rsquo;</dt>
<dd><p>This trace level is the same as trace level &lsquo;<samp>deep</samp>&rsquo;,
except that a representation of the module is stored in the executable
along with the usual debugging information.
The declarative debugger can use this extra information
to help it avoid asking unnecessary questions,
so this trace level has the effect of better declarative debugging
at the cost of increased executable size.
For more details see <a href="Declarative-debugging.html#Declarative-debugging">Declarative debugging</a>.
</p></dd>
<dt>&lsquo;<samp>shallow</samp>&rsquo;</dt>
<dd><p>A procedure compiled with trace level &lsquo;<samp>shallow</samp>&rsquo;
will generate interface events
if it is called from a procedure compiled with trace level &lsquo;<samp>deep</samp>&rsquo;,
but it will never generate any internal events,
and it will not generate any interface events either
if it is called from a procedure compiled with trace level &lsquo;<samp>shallow</samp>&rsquo;.
If it is called from a procedure compiled with trace level &lsquo;<samp>none</samp>&rsquo;,
the way it will behave is dictated by whether
its nearest ancestor whose trace level is not &lsquo;<samp>none</samp>&rsquo;
has trace level &lsquo;<samp>deep</samp>&rsquo; or &lsquo;<samp>shallow</samp>&rsquo;.
</p></dd>
</dl>

<p>The intended uses of these trace levels are as follows.
</p>
<dl compact="compact">
<dt>&lsquo;<samp>deep</samp>&rsquo;</dt>
<dd><p>You should compile a module with trace level &lsquo;<samp>deep</samp>&rsquo;
if you suspect there may be a bug in the module,
or if you think that being able to examine what happens inside that module
can help you locate a bug.
</p></dd>
<dt>&lsquo;<samp>rep</samp>&rsquo;</dt>
<dd><p>You should compile a module with trace level &lsquo;<samp>rep</samp>&rsquo;
if you suspect there may be a bug in the module,
you wish to use the full power of the declarative debugger,
and you are not concerned about the size of the executable.
</p></dd>
<dt>&lsquo;<samp>shallow</samp>&rsquo;</dt>
<dd><p>You should compile a module with trace level &lsquo;<samp>shallow</samp>&rsquo;
if you believe the code of the module is reliable and unlikely to have bugs,
but you still want to be able to get control at calls to and returns from
any predicates and functions defined in the module,
and if you want to be able to see the arguments of those calls.
</p></dd>
<dt>&lsquo;<samp>none</samp>&rsquo;</dt>
<dd><p>You should compile a module with trace level &lsquo;<samp>none</samp>&rsquo;
only if you are reasonably confident that the module is reliable,
and if you believe that knowing what calls other modules make to this module
would not significantly benefit you in your debugging.
</p></dd>
</dl>

<p>In general, it is a good idea for most or all modules
that can be called from modules compiled with trace level
&lsquo;<samp>deep</samp>&rsquo; or &lsquo;<samp>rep</samp>&rsquo;
to be compiled with at least trace level &lsquo;<samp>shallow</samp>&rsquo;.
</p>
<p>You can control what trace level a module is compiled with
by giving one of the following compiler options:
</p>
<dl compact="compact">
<dt>&lsquo;<samp>--trace shallow</samp>&rsquo;</dt>
<dd><p>This always sets the trace level to &lsquo;<samp>shallow</samp>&rsquo;.
</p></dd>
<dt>&lsquo;<samp>--trace deep</samp>&rsquo;</dt>
<dd><p>This always sets the trace level to &lsquo;<samp>deep</samp>&rsquo;.
</p></dd>
<dt>&lsquo;<samp>--trace rep</samp>&rsquo;</dt>
<dd><p>This always sets the trace level to &lsquo;<samp>rep</samp>&rsquo;.
</p></dd>
<dt>&lsquo;<samp>--trace minimum</samp>&rsquo;</dt>
<dd><p>In debugging grades, this sets the trace level to &lsquo;<samp>shallow</samp>&rsquo;;
in non-debugging grades, it sets the trace level to &lsquo;<samp>none</samp>&rsquo;.
</p></dd>
<dt>&lsquo;<samp>--trace default</samp>&rsquo;</dt>
<dd><p>In debugging grades, this sets the trace level to &lsquo;<samp>deep</samp>&rsquo;;
in non-debugging grades, it sets the trace level to &lsquo;<samp>none</samp>&rsquo;.
</p></dd>
</dl>

<p>As the name implies, the last alternative is the default,
which is why by default you get
no debugging capability in non-debugging grades
and full debugging capability in debugging grades.
The table also shows that in a debugging grade,
no module can be compiled with trace level &lsquo;<samp>none</samp>&rsquo;.
</p>
<p><strong>Important note</strong>:
If you are not using a debugging grade, but you compile some modules with
a trace level other than none,
then you must also pass the &lsquo;<samp>--trace</samp>&rsquo; (or &lsquo;<samp>-t</samp>&rsquo;) option
to c2init and to the Mercury linker.
If you are using Mmake, then you can do this by including &lsquo;<samp>--trace</samp>&rsquo;
in the &lsquo;<samp>MLFLAGS</samp>&rsquo; variable.
</p>
<p>If you are using Mmake, then you can also set the compilation options
for a single module named <var>Module</var> by setting the Mmake variable
&lsquo;<samp>MCFLAGS-<var>Module</var></samp>&rsquo;.  For example, to compile the file
<samp>foo.m</samp> with deep tracing, <samp>bar.m</samp> with shallow tracing,
and everything else with no tracing, you could use the following:
</p>
<div class="example">
<pre class="example">MLFLAGS     = --trace
MCFLAGS-foo = --trace deep
MCFLAGS-bar = --trace shallow
</pre></div>

<hr>
<div class="header">
<p>
Next: <a href="Tracing-optimized-code.html#Tracing-optimized-code" accesskey="n" rel="next">Tracing optimized code</a>, Previous: <a href="Tracing-of-Mercury-programs.html#Tracing-of-Mercury-programs" accesskey="p" rel="previous">Tracing of Mercury programs</a>, Up: <a href="Debugging.html#Debugging" accesskey="u" rel="up">Debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
