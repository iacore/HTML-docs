<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury User&rsquo;s Guide: Stand-alone interfaces</title>

<meta name="description" content="The Mercury User&rsquo;s Guide: Stand-alone interfaces">
<meta name="keywords" content="The Mercury User&rsquo;s Guide: Stand-alone interfaces">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html#Top" rel="up" title="Top">
<link href="Index.html#Index" rel="next" title="Index">
<link href="Foreign-language-interface.html#Foreign-language-interface" rel="previous" title="Foreign language interface">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Stand_002dalone-interfaces"></a>
<div class="header">
<p>
Next: <a href="Index.html#Index" accesskey="n" rel="next">Index</a>, Previous: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="p" rel="previous">Foreign language interface</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Stand_002dalone-interfaces-1"></a>
<h2 class="chapter">13 Stand-alone interfaces</h2>

<p>Programs written in a language other than Mercury
should not make calls to foreign exported Mercury procedures
unless the Mercury runtime has been initialised.
(In the case where the Mercury runtime has not been initialised,
the behaviour of these calls is undefined.)
Such programs must also ensure that
any module specific initialisation is performed
before calling foreign exported procedures in Mercury modules.
Likewise, module specific finalisation may need to be performed
after all calls to Mercury procedures have been made.
</p>
<p>A stand-alone interface provides a mechanism by which
non-Mercury programs may initialise (and shut down)
the Mercury runtime plus a specified set of Mercury libraries.
</p>
<p>A stand-alone interface is created by invoking the compiler
with the &lsquo;<samp>--generate-standalone-interface</samp>&rsquo; option.
The set of Mercury libraries to be included in the stand-alone interface
is given via one of the usual mechanisms
for specifying what libraries to link against,
e.g. the &lsquo;<samp>--ml</samp>&rsquo; and &lsquo;<samp>--mld</samp>&rsquo; options. (see <a href="Libraries.html#Libraries">Libraries</a>).
The Mercury standard library is always included in this set.
</p>
<p>In C grades,
the &lsquo;<samp>--generate-standalone-interface</samp>&rsquo; option
causes the compiler to generate
an object file that should be linked into the executable.
This object file contains two functions:
<code>mercury_init()</code> and <code>mercury_terminate()</code>.
The compiler also generates a C header file
that contains the prototypes of these functions.
(This header file may be included in C++ programs.)
The roles of the two functions are described below.
</p>
<dl compact="compact">
<dt><b>&bull; <code>mercury_init()</code></b></dt>
<dd><p>Prototype:
</p><div class="example">
<pre class="example">void mercury_init(int <var>argc</var>, char **<var>argv</var>, void *<var>stackbottom</var>);
</pre></div>

<p>Initialise the Mercury runtime, standard library and any other Mercury
libraries that were specified when the stand-alone interface was generated.
<var>argc</var> and <var>argv</var> are the argument count and argument vector,
as would be passed to the function <code>main()</code> in a C program.
<var>stackbottom</var> is the address of the base of the stack.
In grades that use conservative garbage collection this is used to
tell the collector where to begin tracing.
This function must be called before any Mercury procedures
and must only be called once.
It is recommended that the value of <var>stackbottom</var> be set by passing
the address of a local variable in the <code>main()</code> function of a program,
for example:
</p><div class="example">
<pre class="example">    int main(int argc, char **argv) {
        void *dummy;
        mercury_init(argc, argv, &amp;dummy);
        &hellip;
    }
</pre></div>
<p>Note that the address of the stack base should be word aligned as
some garbage collectors rely upon this.
(This is why the type of the dummy variable in the above example is
<code>void *</code>.)
If the value of <var>stackbottom</var> is <code>NULL</code> then the collector will attempt
to determine the address of the base of the stack itself.
Note that modifying the argument vector, <var>argv</var>, after the Mercury runtime
has been initialised will result in undefined behaviour since the runtime
maintains a reference into <var>argv</var>.
</p>
<br>
</dd>
<dt><b>&bull; <code>mercury_terminate()</code></b></dt>
<dd><p>Prototype:
</p><div class="example">
<pre class="example">int mercury_terminate(void);
</pre></div>

<p>Shut down the Mercury runtime.
The value returned by this function is Mercury&rsquo;s exit status
(as set by the predicate &lsquo;<samp>io.set_exit_status/3</samp>&rsquo;).
This function will also invoke any finalisers contained in the set
of libraries for which the stand-alone interface was generated.
</p></dd>
</dl>

<p>The basename of the object and header file are provided as
the argument of &lsquo;<samp>--generate-standalone-interface</samp>&rsquo; option.
</p>

<p>Stand-alone interfaces are not required if the target language is Java
or C#.
For those target languages the Mercury runtime will be automatically
initialised when the classes or library assemblies containing code generated
by the Mercury compiler are loaded.
</p>
<p>For an example of using a stand-alone interface see the
&lsquo;<samp>samples/c_interface/standalone_c</samp>&rsquo; directory in the Mercury distribution.
</p>


<hr>
<div class="header">
<p>
Next: <a href="Index.html#Index" accesskey="n" rel="next">Index</a>, Previous: <a href="Foreign-language-interface.html#Foreign-language-interface" accesskey="p" rel="previous">Foreign language interface</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
