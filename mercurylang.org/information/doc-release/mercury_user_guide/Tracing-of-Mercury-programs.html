<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury User&rsquo;s Guide: Tracing of Mercury programs</title>

<meta name="description" content="The Mercury User&rsquo;s Guide: Tracing of Mercury programs">
<meta name="keywords" content="The Mercury User&rsquo;s Guide: Tracing of Mercury programs">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Debugging.html#Debugging" rel="up" title="Debugging">
<link href="Preparing-a-program-for-debugging.html#Preparing-a-program-for-debugging" rel="next" title="Preparing a program for debugging">
<link href="GNU-Emacs-interface.html#GNU-Emacs-interface" rel="previous" title="GNU Emacs interface">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Tracing-of-Mercury-programs"></a>
<div class="header">
<p>
Next: <a href="Preparing-a-program-for-debugging.html#Preparing-a-program-for-debugging" accesskey="n" rel="next">Preparing a program for debugging</a>, Previous: <a href="GNU-Emacs-interface.html#GNU-Emacs-interface" accesskey="p" rel="previous">GNU Emacs interface</a>, Up: <a href="Debugging.html#Debugging" accesskey="u" rel="up">Debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Tracing-of-Mercury-programs-1"></a>
<h3 class="section">7.3 Tracing of Mercury programs</h3>
<a name="index-Tracing-1"></a>

<p>The Mercury debugger is based on a modified version of the box model
on which the four-port debuggers of most Prolog systems are based.
Such debuggers abstract the execution of a program into a sequence,
also called a <em>trace</em>, of execution events of various kinds.
The four kinds of events supported by most Prolog systems (their <em>ports</em>)
are
</p>
<a name="index-debugger-trace-events"></a>
<a name="index-trace-events"></a>
<a name="index-call-_0028trace-event_0029"></a>
<a name="index-exit-_0028trace-event_0029"></a>
<a name="index-redo-_0028trace-event_0029"></a>
<a name="index-fail-_0028trace-event_0029"></a>
<dl compact="compact">
<dt><em>call</em></dt>
<dd><p>A call event occurs just after a procedure has been called,
and control has just reached the start of the body of the procedure.
</p></dd>
<dt><em>exit</em></dt>
<dd><p>An exit event occurs when a procedure call has succeeded,
and control is about to return to its caller.
</p></dd>
<dt><em>redo</em></dt>
<dd><p>A redo event occurs when all computations
to the right of a procedure call have failed,
and control is about to return to this call
to try to find alternative solutions.
</p></dd>
<dt><em>fail</em></dt>
<dd><p>A fail event occurs when a procedure call has run out of alternatives,
and control is about to return to the rightmost computation to its left
that still has possibly successful alternatives left.
</p></dd>
</dl>

<p>Mercury also supports these four kinds of events,
but not all events can occur for every procedure call.
Which events can occur for a procedure call, and in what order,
depend on the determinism of the procedure.
The possible event sequences for procedures of the various determinisms
are as follows.
</p>
<dl compact="compact">
<dt><em>nondet procedures</em></dt>
<dd><p>a call event, zero or more repeats of (exit event, redo event), and a fail event
</p></dd>
<dt><em>multi procedures</em></dt>
<dd><p>a call event, one or more repeats of (exit event, redo event), and a fail event
</p></dd>
<dt><em>semidet and cc_nondet procedures</em></dt>
<dd><p>a call event, and either an exit event or a fail event
</p></dd>
<dt><em>det and cc_multi procedures</em></dt>
<dd><p>a call event and an exit event
</p></dd>
<dt><em>failure procedures</em></dt>
<dd><p>a call event and a fail event
</p></dd>
<dt><em>erroneous procedures</em></dt>
<dd><p>a call event
</p></dd>
</dl>

<p>In addition to these four event types,
Mercury supports <em>exception</em> events.
An exception event occurs
when an exception has been thrown inside a procedure,
and control is about to propagate this exception to the caller.
An exception event can replace the final exit or fail event
in the event sequences above
or, in the case of erroneous procedures,
can come after the call event.
</p>
<p>Besides the event types call, exit, redo, fail and exception,
which describe the <em>interface</em> of a call,
Mercury also supports several types of events
that report on what is happening <em>internal</em> to a call.
Each of these internal event types has an associated parameter called a path.
The internal event types are:
</p>
<a name="index-cond-_0028trace-event_0029"></a>
<a name="index-then-_0028trace-event_0029"></a>
<a name="index-else-_0028trace-event_0029"></a>
<a name="index-disj-_0028trace-event_0029"></a>
<a name="index-switch-_0028trace-event_0029"></a>
<a name="index-neg_005fenter-_0028trace-event_0029"></a>
<a name="index-neg_005ffail-_0028trace-event_0029"></a>
<a name="index-neg_005fsuccess-_0028trace-event_0029"></a>
<dl compact="compact">
<dt><em>cond</em></dt>
<dd><p>A cond event occurs when execution reaches
the start of the condition of an if-then-else.
The path associated with the event specifies which if-then-else this is.
</p></dd>
<dt><em>then</em></dt>
<dd><p>A then event occurs when execution reaches
the start of the then part of an if-then-else.
The path associated with the event specifies which if-then-else this is.
</p></dd>
<dt><em>else</em></dt>
<dd><p>An else event occurs when execution reaches
the start of the else part of an if-then-else.
The path associated with the event specifies which if-then-else this is.
</p></dd>
<dt><em>disj</em></dt>
<dd><p>A disj event occurs when execution reaches
the start of a disjunct in a disjunction.
The path associated with the event specifies
which disjunct of which disjunction this is.
</p></dd>
<dt><em>switch</em></dt>
<dd><p>A switch event occurs when execution reaches
the start of one arm of a switch
(a disjunction in which each disjunct unifies a bound variable
with different function symbol).
The path associated with the event specifies
which arm of which switch this is.
</p></dd>
<dt><em>neg_enter</em></dt>
<dd><p>A neg_enter event occurs when execution reaches
the start of a negated goal.
The path associated with the event specifies which negation goal this is.
</p></dd>
<dt><em>neg_fail</em></dt>
<dd><p>A neg_fail event occurs when
a goal inside a negation succeeds,
which means that its negation fails.
The path associated with the event specifies which negation goal this is.
</p></dd>
<dt><em>neg_success</em></dt>
<dd><p>A neg_success event occurs when
a goal inside a negation fails,
which means that its negation succeeds.
The path associated with the event specifies which negation goal this is.
</p></dd>
</dl>

<a name="index-path"></a>
<a name="index-goal-path"></a>
<p>A goal path is a sequence of path components separated by semicolons.
Each path component is one of the following:
</p>
<dl compact="compact">
<dt><code>c<var>num</var></code></dt>
<dd><p>The <var>num</var>&rsquo;th conjunct of a conjunction.
</p></dd>
<dt><code>d<var>num</var></code></dt>
<dd><p>The <var>num</var>&rsquo;th disjunct of a disjunction.
</p></dd>
<dt><code>s<var>num</var></code></dt>
<dd><p>The <var>num</var>&rsquo;th arm of a switch.
</p></dd>
<dt><code>?</code></dt>
<dd><p>The condition of an if-then-else.
</p></dd>
<dt><code>t</code></dt>
<dd><p>The then part of an if-then-else.
</p></dd>
<dt><code>e</code></dt>
<dd><p>The else part of an if-then-else.
</p></dd>
<dt><code>~</code></dt>
<dd><p>The goal inside a negation.
</p></dd>
<dt><code>q!</code></dt>
<dd><p>The goal inside an existential quantification or other scope
that changes the determinism of the goal.
</p></dd>
<dt><code>q</code></dt>
<dd><p>The goal inside an existential quantification or other scope
that doesn&rsquo;t change the determinism of the goal.
</p></dd>
</dl>

<p>A goal path describes the position of a goal
inside the body of a procedure definition.
For example, if the procedure body is a disjunction
in which each disjunct is a conjunction,
then the goal path &lsquo;<samp>d2;c3;</samp>&rsquo; denotes
the third conjunct within the second disjunct.
If the third conjunct within the second disjunct is an atomic goal
such as a call or a unification,
then this will be the only goal with whose path has &lsquo;<samp>d2;c3;</samp>&rsquo; as a prefix.
If it is a compound goal,
then its components will all have paths that have &lsquo;<samp>d2;c3;</samp>&rsquo; as a prefix,
e.g. if it is an if-then-else,
then its three components will have the paths
&lsquo;<samp>d2;c3;?;</samp>&rsquo;, &lsquo;<samp>d2;c3;t;</samp>&rsquo; and &lsquo;<samp>d2;c3;e;</samp>&rsquo;.
</p>
<p>Goal paths refer to the internal form of the procedure definition.
When debugging is enabled
(and the option &lsquo;<samp>--trace-optimized</samp>&rsquo; is not given),
the compiler will try to keep this form
as close as possible to the source form of the procedure,
in order to make event paths as useful as possible to the programmer.
Due to the compiler&rsquo;s flattening of terms,
and its introduction of extra unifications to implement calls in implied modes,
the number of conjuncts in a conjunction will frequently differ
between the source and internal form of a procedure.
This is rarely a problem, however, as long as you know about it.
Mode reordering can be a bit more of a problem,
but it can be avoided by writing single-mode predicates and functions
so that producers come before consumers.
The compiler transformation that
potentially causes the most trouble in the interpretation of goal paths
is the conversion of disjunctions into switches.
In most cases, a disjunction is transformed into a single switch,
and it is usually easy to guess, just from the events within a switch arm,
just which disjunct the switch arm corresponds to.
Some cases are more complex;
for example, it is possible for a single disjunction
to be transformed into several switches,
possibly with other, smaller disjunctions inside them.
In such cases, making sense of goal paths
may require a look at the internal form of the procedure.
You can ask the compiler to generate a file
with the internal forms of the procedures in a given module
by including the options &lsquo;<samp>-dfinal -Dpaths</samp>&rsquo; on the command line
when compiling that module.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Preparing-a-program-for-debugging.html#Preparing-a-program-for-debugging" accesskey="n" rel="next">Preparing a program for debugging</a>, Previous: <a href="GNU-Emacs-interface.html#GNU-Emacs-interface" accesskey="p" rel="previous">GNU Emacs interface</a>, Up: <a href="Debugging.html#Debugging" accesskey="u" rel="up">Debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
