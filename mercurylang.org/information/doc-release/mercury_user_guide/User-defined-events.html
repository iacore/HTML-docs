<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury User&rsquo;s Guide: User defined events</title>

<meta name="description" content="The Mercury User&rsquo;s Guide: User defined events">
<meta name="keywords" content="The Mercury User&rsquo;s Guide: User defined events">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Debugging.html#Debugging" rel="up" title="Debugging">
<link href="I_002fO-tabling.html#I_002fO-tabling" rel="next" title="I/O tabling">
<link href="Mercury-debugger-concepts.html#Mercury-debugger-concepts" rel="previous" title="Mercury debugger concepts">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="User-defined-events"></a>
<div class="header">
<p>
Next: <a href="I_002fO-tabling.html#I_002fO-tabling" accesskey="n" rel="next">I/O tabling</a>, Previous: <a href="Mercury-debugger-concepts.html#Mercury-debugger-concepts" accesskey="p" rel="previous">Mercury debugger concepts</a>, Up: <a href="Debugging.html#Debugging" accesskey="u" rel="up">Debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="User-defined-events-1"></a>
<h3 class="section">7.8 User defined events</h3>

<p>Besides the builtin set of events,
the Mercury debugger also supports events defined by the user.
The intention is that users can define one kind of event
for each semantically important event in the program
that is not captured by the standard builtin events,
and can then generate those events at the appropriate point in the source code.
Each event appears in the source code
as a call prefixed by the keyword &lsquo;<samp>event</samp>&rsquo;,
with each argument of the call giving the value of an event <em>attribute</em>.
</p>
<p>Users can specify the set of user defined events that can appear in a program,
and the names, types and order of the attributes
of each kind of user defined event,
by giving the name of an event set specification file to the compiler
when compiling that program
as the argument of the &lsquo;<samp>event-set-file-name</samp>&rsquo; option.
This file should contain a header giving the event set&rsquo;s name,
followed by a sequence of one or more event specifications,
like this:
</p>
<div class="example">
<pre class="example">    event set queens

    event nodiag_fail(
        test_failed:    string,
        arg_b:          int,
        arg_d:          int,
        arg_list_len:   int synthesized by list_len_func(sorted_list),
        sorted_list:    list(int) synthesized by list_sort_func(arg_list),
        list_len_func:  function,
        list_sort_func: function,
        arg_list:       list(int)
    )

    event safe_test(
        test_list:      listint
    )

    event noargs
</pre></div>

<p>The header consists of the keywords &lsquo;<samp>event set</samp>&rsquo;
and an identifier giving the event set name.
Each event specification consists of the keyword &lsquo;<samp>event</samp>&rsquo;,
the name of the event, and,
if the event has any attributes, a parenthesized list of those attributes.
Each attribute&rsquo;s specification consists of
a name, a colon and information about the attribute.
</p>
<p>There are three kinds of attributes.
</p><ul>
<li> For ordinary attributes, like &lsquo;<samp>arg_b</samp>&rsquo;,
the information about the attribute is the Mercury type of that attribute.
</li><li> For function attributes, like &lsquo;<samp>list_sort_func</samp>&rsquo;,
the information about the attribute is just the keyword &lsquo;<samp>function</samp>&rsquo;.
</li><li> For synthesized attributes, like &lsquo;<samp>sorted_list</samp>&rsquo;,
the information about the attribute is the type of the attribute,
the keywords &lsquo;<samp>synthesized by</samp>&rsquo;,
and a description of the Mercury function call
required to synthesize the value of the attribute.
The synthesis call consists of the name of a function attribute
and a list of the names of one or more argument attributes.
Argument attributes cannot be function attributes;
they may be either ordinary attributes, or previously synthesized attributes.
A synthesized attribute is not allowed
to depend on itself directly or indirectly,
but there are no restrictions on the positions of synthesized attributes
compared to the positions of the function attributes computing them
or of the argument attributes of the synthesis functions.
</li></ul>

<p>The result types of function attributes
are given by the types of the synthesized attributes they compute.
The argument types of function attributes (and the number of those arguments)
are given by the types of the arguments they are applied to.
Each function attribute must be used
to compute at least one synthesized attribute,
otherwise there would be no way to compute its type.
If it is used to compute more than one synthesized attribute,
the result and argument types must be consistent.
</p>
<p>Each event goal in the program must use
the name of one of the events defined here as the predicate name of the call,
and the call&rsquo;s arguments must match
the types of that event&rsquo;s non-synthesized attributes.
Given that B and N are integers and L is a list of integers,
these event goals are fine,
</p><div class="example">
<pre class="example">	event nodiag_fail(&quot;N - B&quot;, B, N, list.length, list.sort, [N | L]),
	event safe_test([1, 2, 3])
</pre></div>
<p>but these goals
</p><div class="example">
<pre class="example">	event nodiag_fail(&quot;N - B&quot;, B, N, list.sort, list.length, [N | L]),
	event nodiag_fail(&quot;N - B&quot;, B, list.length, N, list.sort, [N | L]),
	event safe_test([1], [2])
	event safe_test(42)
	event nonexistent_event(42)
</pre></div>
<p>will all generate errors.
</p>
<p>The attributes of event calls are always input,
and the event goal is always &lsquo;<samp>det</samp>&rsquo;.
</p>
<hr>
<div class="header">
<p>
Next: <a href="I_002fO-tabling.html#I_002fO-tabling" accesskey="n" rel="next">I/O tabling</a>, Previous: <a href="Mercury-debugger-concepts.html#Mercury-debugger-concepts" accesskey="p" rel="previous">Mercury debugger concepts</a>, Up: <a href="Debugging.html#Debugging" accesskey="u" rel="up">Debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
