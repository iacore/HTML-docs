<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury User&rsquo;s Guide: I/O tabling</title>

<meta name="description" content="The Mercury User&rsquo;s Guide: I/O tabling">
<meta name="keywords" content="The Mercury User&rsquo;s Guide: I/O tabling">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Debugging.html#Debugging" rel="up" title="Debugging">
<link href="Debugger-commands.html#Debugger-commands" rel="next" title="Debugger commands">
<link href="User-defined-events.html#User-defined-events" rel="previous" title="User defined events">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="I_002fO-tabling"></a>
<div class="header">
<p>
Next: <a href="Debugger-commands.html#Debugger-commands" accesskey="n" rel="next">Debugger commands</a>, Previous: <a href="User-defined-events.html#User-defined-events" accesskey="p" rel="previous">User defined events</a>, Up: <a href="Debugging.html#Debugging" accesskey="u" rel="up">Debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="I_002fO-tabling-1"></a>
<h3 class="section">7.9 I/O tabling</h3>

<p>In Mercury, predicates that want to do I/O
must take a di/uo pair of I/O state arguments.
Some of these predicates call other predicates to do I/O for them,
but some are <em>I/O primitives</em>, i.e. they perform the I/O themselves.
The Mercury standard library provides a large set of these primitives,
and programmers can write their own through the foreign language interface.
An I/O action is the execution of one call to an I/O primitive.
</p>
<p>In debugging grades, the Mercury implementation has the ability
to automatically record, for every I/O action,
the identity of the I/O primitive involved in the action
and the values of all its arguments.
The size of the table storing this information
is proportional to the number of <em>tabled</em> I/O actions,
which are the I/O actions whose details are entered into the table.
Therefore the tabling of I/O actions is never turned on automatically;
instead, users must ask for I/O tabling to start
with the &lsquo;<samp>table_io start</samp>&rsquo; command in mdb.
</p>
<p>The purpose of I/O tabling is to enable transparent retries across I/O actions.
(The mdb &lsquo;<samp>retry</samp>&rsquo; command
restores the computation to a state it had earlier,
allowing the programmer to explore code that the program has already executed;
see its documentation in the <a href="Debugger-commands.html#Debugger-commands">Debugger commands</a> section below.)
In the absence of I/O tabling,
retries across I/O actions can have bad consequences.
Retry of a goal that reads some input requires that input to be provided twice;
retry of a goal that writes some output generates duplicate output.
Retry of a goal that opens a file leads to a file descriptor leak;
retry of a goal that closes a file can lead to errors
(duplicate closes, reads from and writes to closed files).
</p>
<p>I/O tabling avoids these problems by making I/O primitives <em>idempotent</em>.
This means that they will generate their desired effect
when they are first executed,
but reexecuting them after a retry won&rsquo;t have any further effect.
The Mercury implementation achieves this
by looking up the action (which is identified by a I/O action number)
in the table and returning the output arguments stored in the table
for the given action <em>without</em> executing the code of the primitive.
</p>
<p>Starting I/O tabling when the program starts execution
and leaving it enabled for the entire program run
will work well for program runs that don&rsquo;t do lots of I/O.
For program runs that <em>do</em> lots of I/O,
the table can fill up all available memory.
In such cases, the programmer may enable I/O tabling with &lsquo;<samp>table_io start</samp>&rsquo;
just before the program enters the part they wish to debug
and in which they wish to be able to perform
transparent retries across I/O actions,
and turn it off with &lsquo;<samp>table_io stop</samp>&rsquo; after execution leaves that part.
</p>
<p>The commands &lsquo;<samp>table_io start</samp>&rsquo; and &lsquo;<samp>table_io stop</samp>&rsquo;
can each be given only once during an mdb session.
They divide the execution of the program into three phases:
before &lsquo;<samp>table_io start</samp>&rsquo;,
between &lsquo;<samp>table_io start</samp>&rsquo; and &lsquo;<samp>table_io stop</samp>&rsquo;,
and after &lsquo;<samp>table_io stop</samp>&rsquo;.
Retries across I/O will be transparent only in the middle phase.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Debugger-commands.html#Debugger-commands" accesskey="n" rel="next">Debugger commands</a>, Previous: <a href="User-defined-events.html#User-defined-events" accesskey="p" rel="previous">User defined events</a>, Up: <a href="Debugging.html#Debugging" accesskey="u" rel="up">Debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
