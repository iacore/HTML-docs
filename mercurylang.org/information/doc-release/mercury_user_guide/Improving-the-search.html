<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 5.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>The Mercury User&rsquo;s Guide: Improving the search</title>

<meta name="description" content="The Mercury User&rsquo;s Guide: Improving the search">
<meta name="keywords" content="The Mercury User&rsquo;s Guide: Improving the search">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Declarative-debugging.html#Declarative-debugging" rel="up" title="Declarative debugging">
<link href="Trace-counts.html#Trace-counts" rel="next" title="Trace counts">
<link href="Search-modes.html#Search-modes" rel="previous" title="Search modes">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.indentedblock {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smallindentedblock {margin-left: 3.2em; font-size: smaller}
div.smalllisp {margin-left: 3.2em}
kbd {font-style:oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space:nowrap}
span.nolinebreak {white-space:nowrap}
span.roman {font-family:serif; font-weight:normal}
span.sansserif {font-family:sans-serif; font-weight:normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en" bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#800080" alink="#FF0000">
<a name="Improving-the-search"></a>
<div class="header">
<p>
Previous: <a href="Search-modes.html#Search-modes" accesskey="p" rel="previous">Search modes</a>, Up: <a href="Declarative-debugging.html#Declarative-debugging" accesskey="u" rel="up">Declarative debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Improving-the-search-1"></a>
<h4 class="subsection">7.11.7 Improving the search</h4>

<p>The number of questions asked by the declarative debugger before it pinpoints
the location of a bug can be reduced by giving it extra information.  The kind
of extra information that can be given and how to convey this information are
explained in this section.
</p>
<a name="Tracking-suspicious-subterms"></a>
<h4 class="subsubsection">7.11.7.1 Tracking suspicious subterms</h4>

<p>An incorrect subterm can be tracked to the call that bound the subterm
from within the interactive term browser
(see <a href="Declarative-debugging-commands.html#Declarative-debugging-commands">Declarative debugging commands</a>).
</p>
<p>After issuing a &lsquo;<samp>track</samp>&rsquo; command,
the next question asked by the declarative debugger will
be about the call that bound the incorrect subterm,
unless that call was
eliminated as a possible bug because of an answer to a previous
question or the call that bound the subterm was not traced.
</p>
<p>For example consider the following fragment of a program that calculates
payments for a loan:
</p>
<div class="example">
<pre class="example">:- type payment
        ---&gt;   payment(
                       date    :: date,
                       amount  :: float
               ).

:- type date ---&gt; date(int, int, int).  % date(day, month, year).

:- pred get_payment(loan::in, int::in, payment::out) is det.

get_payment(Loan, PaymentNo, Payment) :-
    get_payment_amount(Loan, PaymentNo, Amount),
    get_payment_date(Loan, PaymentNo, Date),
    Payment = payment(Date, Amount).
</pre></div>

<p>Suppose that <code>get_payment</code> produces an incorrect result and the
declarative debugger asks:
</p>
<div class="example">
<pre class="example">get_payment(loan(...), 10, payment(date(9, 10, 1977), 10.000000000000)).
Valid?
</pre></div>

<p>Then if we know that this is the right payment amount for the given loan,
but the date is incorrect, we can track the <code>date(...)</code> subterm and the
debugger will then ask us about <code>get_payment_date</code>:
</p>
<div class="example">
<pre class="example">get_payment(loan(...), 10, payment(date(9, 10, 1977), 10.000000000000)).
Valid? browse
browser&gt; cd 3/1
browser&gt; ls
date(9, 10, 1977)
browser&gt; track
get_payment_date(loan(...), 10, date(9, 10, 1977)).
Valid?
</pre></div>

<p>Thus irrelevant questions about <code>get_payment_amount</code> are avoided.
</p>
<p>If, say, the date was only wrong in the year part, then we could also have
tracked the year subterm in which case the next question would have been about
the call that constructed the year part of the date.
</p>
<p>This feature is also useful when using the procedural debugger.  For example,
suppose that you come across a &lsquo;<samp>CALL</samp>&rsquo; event and you would like to know the
source of a particular input to the call.  To find out you could first go to
the final event by issuing a &lsquo;<samp>finish</samp>&rsquo; command.  Invoke the declarative
debugger with a &lsquo;<samp>dd</samp>&rsquo; command and then track the input term you are
interested in.  The next question will be about the call that bound the term.
Issue a &lsquo;<samp>pd</samp>&rsquo; command at this point to return to the procedural debugger.
It will now show the final event of the call that bound the term.
</p>
<p>Note that this feature is only available if the executable is compiled
in a .decldebug grade or with the &lsquo;<samp>--trace rep</samp>&rsquo; option.  If a module
is compiled with the &lsquo;<samp>--trace rep</samp>&rsquo; option but other modules in the
program are not then you will not be able to track subterms through those
other modules.
</p>
<a name="Trusting-predicates_002c-functions-and-modules"></a>
<h4 class="subsubsection">7.11.7.2 Trusting predicates, functions and modules</h4>

<p>The declarative debugger can also be told to assume that certain predicates,
functions or entire modules do not contain any bugs.  The declarative
debugger will never ask questions about trusted predicates or functions.  It
is a good idea to trust standard library modules imported by a program being
debugged.
</p>
<p>The declarative debugger can be told which predicates/functions it can trust
before the &lsquo;<samp>dd</samp>&rsquo; command is given.  This is done using the &lsquo;<samp>trust</samp>&rsquo;,
&lsquo;<samp>trusted</samp>&rsquo; and &lsquo;<samp>untrust</samp>&rsquo; commands at the mdb prompt (see
<a href="Declarative-debugging-mdb-commands.html#Declarative-debugging-mdb-commands">Declarative debugging mdb commands</a> for details on how to use these
commands).
</p>
<p>Trust commands may be placed in the &lsquo;<samp>.mdbrc</samp>&rsquo; file which contains default
settings for mdb (see <a href="Mercury-debugger-invocation.html#Mercury-debugger-invocation">Mercury debugger invocation</a>).  Trusted
predicates will also be exported with a &lsquo;<samp>save</samp>&rsquo; command (see
<a href="Miscellaneous-commands.html#Miscellaneous-commands">Miscellaneous commands</a>).
</p>
<p>During the declarative debugging session the user may tell the declarative
debugger to trust the predicate or function in the current question.
Alternatively the user may tell the declarative debugger to trust all the
predicates and functions in the same module as the predicate or function in the
current question.  See the &lsquo;<samp>trust</samp>&rsquo; command in
<a href="Declarative-debugging-commands.html#Declarative-debugging-commands">Declarative debugging commands</a>.
</p>
<a name="When-different-search-modes-are-used"></a>
<h4 class="subsubsection">7.11.7.3 When different search modes are used</h4>

<p>If a search mode is given when invoking the declarative debugger then that
search mode will be used, unless (a) a subterm is tracked during the session,
or (b) the user has not answered &lsquo;<samp>no</samp>&rsquo; to any questions yet,
in which case top-down search is used until &lsquo;<samp>no</samp>&rsquo; is answered to at least
one question.
</p>
<p>If no search mode is specified with the &lsquo;<samp>dd</samp>&rsquo; command then
the search mode depends on if the &lsquo;<samp>--resume</samp>&rsquo; option is
given.
If it is then the previous search mode will be used,
otherwise top-down search will be used.
</p>
<p>You can check the search mode used to find a particular question by issuing
an &lsquo;<samp>info</samp>&rsquo; command at the question prompt in the declarative debugger.
You can also change the search mode from within the declarative debugger
with the &lsquo;<samp>mode</samp>&rsquo; command.
</p>
<hr>
<div class="header">
<p>
Previous: <a href="Search-modes.html#Search-modes" accesskey="p" rel="previous">Search modes</a>, Up: <a href="Declarative-debugging.html#Declarative-debugging" accesskey="u" rel="up">Declarative debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
