

<!doctype html>
<html>

<head>
<meta charset="utf-8">
<title>WarmPlace.ru. SunVox - модульный синтезатор и трекер</title>
<link rel="stylesheet" type="text/css" href="/styles.css?v=1" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="keywords" content="sunvox, synth, synthesizer, tracker, sequencer, beatmachine, daw, modular studio, generator, effects, music creation, analog, mobile, demoscene, midi, chiptune, chipmusic">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body bgcolor="#FFFFFF" text="#000000" link="#383838" vlink="#383838" alink="#383838">

    <div id="outer_menu_div">
    <div id="outer2_menu_div">
    <div id="menu_div">
	<table class="menu_table" width=100%>
	    <tr>
		<td valign=top width=20%>
		    <font class=menu_title>WarmPlace.ru</font><br>
		    <a class=menu_item href="/">News</a><br>
		    <a class=menu_item href="/forum">Forum</a><br>
		    <a class=menu_item href="/about.php">About</a><br>
		    <!--<a class=menu_item href="/subscribe/index.php">Subscribe</a><br>-->
		    <a class=menu_item href="/donate">Donate</a><br>
		    <br>
		    <a class=menu_lang_switch href="?setlang=rus">Русский</a><br>
		</td>
		<td valign=top width=20%>
		    <font class=menu_title>Software</font><br>
		    <a class=menu_item href="/soft/sunvox">SunVox</a><br>
		    <a class=menu_item href="/soft/pixitracker">PixiTracker</a><br>
		    <a class=menu_item href="/soft/ans">Virtual ANS</a><br>
		    <a class=menu_item href="/soft/phonopaper">PhonoPaper</a><br>
		    <a class=menu_item href="/soft/fbits">Fractal Bits</a><br>
		    <a class=menu_item href="/soft/pixilang">Pixilang</a><br>
		    <a class=menu_item href="/soft/pixivisor">PixiVisor</a> | <a class=menu_item href="/soft/pixiscope">PixiScope</a><br>
		    <a class=menu_item href="/soft/qvjhd">Quantum VJ HD</a><br>
		    <a class=menu_item href="/soft/archive">More...</a><br>
		</td>
		<td valign=top width=20%>
		    <font class=menu_title>Hardware</font><br>
		    <a class=menu_item href="/docs/mobile_audio_input">Mobile audio input</a><br>
		    <a class=menu_item href="/hard/qdj">Quantum DJ</a><br>
		    <a class=menu_item href="/hard/qvj">Quantum VJ</a><br>
		    <a class=menu_item href="https://www.facebook.com/groups/1651567995059350">More...</a><br>
		</td>
		<td valign=top width=20%>
		    <font class=menu_title>Music & Video</font><br>
		    <a class=menu_item href="/music/archive">Music</a><br>
		    <a class=menu_item href="/soft/sunvox/jsplay">Generative music</a><br>
		    <a class=menu_item href="https://www.youtube.com/channel/UC7flcTQsJbVD_d0wcnBX18Q">Video</a><br>
		</td>
    
	    </tr>
	</table>
    </div>
    </div>
    </div>

    <div id="outer_content_div">
    <div id="outer2_content_div">
    <div id="content_div">


<center>
<div class="title">SunVox</div>
<div class="title_version">Последняя версия - v2.1c (15 марта 2023)</div>
</center><br>

<table border=0>
<tr>
<td valign=top width=136><img src="images/icon.png">
<td valign=top>
<b>Скачать:</b><br>
<a class="download_link" href="sunvox-2.1c.zip">SunVox для Windows, macOS, Linux и Windows CE</a><br>
<a class="download_link" href="https://itunes.apple.com/app/sunvox/id324462544?mt=8">SunVox для iOS</a><br>
<a class="download_link" href="https://play.google.com/store/apps/details?id=nightradio.sunvox">SunVox для Android</a><br>
<br>
<a href="changelog_ru.txt">Список изменений</a><br>
<a href="sunvox_lib.php" title="Библиотека SunVox для разработчиков (Linux, Windows, macOS, Android, iOS, JS)">Библиотека SunVox для разработчиков</a>
 | <a href="jsplay/">JS SunVox Player</a><br>
<br>
<!--<a href="../../forum/viewtopic.php?f=16&t=1361"><img src="progress.png?v=1"></a><br>-->
Пожалуйста, <a href="https://warmplace.ru/donate/index_ru.php">поддержите</a> дальнейшее развитие SunVox<br>По вопросам покупки/обновления для Android и iOS пишите на почту <a href="mailto:nightradio@gmail.com">nightradio@gmail.com</a> или в <a href="https://t.me/NightRadio1">Телеграм</a><br>Если вы ранее покупали SunVox для Android, то вы можете скачать <a href="https://warmplace.ru/temp/SunVox.apk">APK файл</a> 
(при первом старте приложение свяжется с Google и проверит факт покупки).<br><br>


</table>

<a class="menu_item2" href="#about">Описание</a><br>
<a class="menu_item2" href="manual_ru.php">Инструкция</a><br>
<a class="menu_item2" href="#vid">Видео</a> | 
<a class="menu_item2" href="../../forum/viewtopic.php?f=3&t=1223">Примеры/уроки</a> | 
<a class="menu_item2" href="#scr">Скриншоты</a> | 
<a class="menu_item2" href="art_ru.php">Логотип</a><br>
<a class="menu_item2" href="#music">Музыка</a> | 
<a class="menu_item2" href="../../forum/viewtopic.php?f=16&t=1445">Сэмплы</a> | 
<a class="menu_item2" href="../../forum/viewforum.php?f=11">Модули</a><br>
<a class="menu_item2" href="../../forum/viewforum.php?f=16">Форум</a> | 
<a class="menu_item2" href="https://vk.com/sunvoxtracker">VK</a> | 
<a class="menu_item2" href="https://www.facebook.com/groups/sunvox/">FB</a> | 
<a class="menu_item2" href="https://t.me/joinchat/Q1x7oKvYnwjzuEL4">Telegram чат</a>/<a class="menu_item2" href="https://t.me/sunvox">новости</a><br>
<a class="menu_item2" href="#old">Старые версии</a><br>

<br>

<div id="about" class="title2">Что такое SunVox</div>
<br>

SunVox - это программа для создания музыки, совмещающая в себе мощный модульный синтезатор и <a href="../../wiki/sunvox:manual_ru#что_такое_трекер_немного_истории">трекер</a>. 
Идеально подходит для разного рода экспериментов в области электронной музыки, поиска нового звучания, новых стилей.
SunVox поддерживает большое количество систем и устройств (даже очень старых и медленных), что дает возможность писать музыку где угодно и на чем угодно.
Распространяется бесплатно для большинства систем, за исключением Android и iOS.
<br>
<center><br><img alt="SunVox - модульная студия в кармане" src="images/sunvox.jpg"></center>
<br>

Ключевые особенности:
<ul>
<li>небольшой размер программы;
<li>интерфейс полностью на русском языке;
<li>масштабируемость для удобной работы на экранах любого размера;
<li>оптимизированные алгоритмы синтеза: SunVox почти без изменений работает на компьютерах любой мощности;
<li>поддержка систем: <ul>
<li>Windows (2000+);
<li>macOS (10.13+);
<li>Linux (x86, x86_64, ARM (Raspberry Pi, PocketCHIP, N900 и пр.), ARM64 (PINE64 и пр.));
<li>iOS (11.0+);
<li>Android (4.1+);
<li>Windows CE (в т.ч. Pocket PC и Windows Mobile; только ARM);
</ul>
<li>поддержка звуковых систем: ASIO, DirectSound, MME, ALSA, OSS, JACK, Audiobus, IAA;
<li>SunVox как плагин (подключаемый модуль): AU-инструмент/эффект для iOS;
<li>поддержка 16 и 32 битных WAV, AIFF и XI сэмплов;
<li>импорт форматов XM (FastTracker) и MOD (ProTracker, OctaMED);
<li>многодорожечный экспорт в WAV;
<li>поддержка MIDI файлов (загрузка и сохранение) и MIDI устройств (in/out);
<li>функции для генеративной музыки: случайный выбор нот, случайные значения контроллеров, эффекты вероятности; <a href="jsplay/">см. примеры</a>;
<li>для разработчиков доступна бесплатная кроссплатформенная <a href="sunvox_lib.php">библиотека</a> - движок SunVox можно свободно использовать в своих программах;
<li>большое количество встроенных модулей синтезаторов и эффектов + возможность делать сложные связи между ними.
</ul>

<a href="manual_ru.php">Подробная инструкция</a><br>
(установка, системные требования, ответы на часто задаваемые вопросы и многое другое)</a><br>
<br>

<div id="scr" class="title2">Скриншоты</div>
<br>

<center>
<a href="images/sunvox36.png"><img alt="SunVox screenshot 1" border=0 src="images/01.png"></a> 
<a href="images/sunvox37.png"><img alt="SunVox screenshot 2" border=0 src="images/02.png"></a> 
<a href="images/sunvox38.png"><img alt="SunVox screenshot 3" border=0 src="images/03.png"></a> 
<a href="images/sunvox39.png"><img alt="SunVox screenshot 4" border=0 src="images/04.png"></a> 
<a href="images/sunvox40.png"><img alt="SunVox screenshot 5" border=0 src="images/05.png"></a> 
<a href="images/sunvox41.png"><img alt="SunVox screenshot 6" border=0 src="images/06.png"></a> 
<a href="images/sunvox42.png"><img alt="SunVox screenshot 7" border=0 src="images/07.png"></a> 
<a href="images/sunvox43.png"><img alt="SunVox screenshot 8" border=0 src="images/08.png"></a><br><br>
<a href="device_screens/Compaq iPAQ 3850 (WinMobile).jpg"><img alt="Compaq iPAQ 3850" border=0 src="device_screens/P. Compaq iPAQ 3850 (WinMobile).jpg"></a> 
<a href="device_screens/Dell Axim (WinMobile).jpg"><img alt="Dell Axim" border=0 src="device_screens/P. Dell Axim (WinMobile).jpg"></a> 
<a href="device_screens/Jornada 720 (WinMobile) (2).jpg"><img alt="Jornada 720" border=0 src="device_screens/P. Jornada 720 (WinMobile) (2).jpg"></a> 
<a href="device_screens/Jornada 720 (WinMobile).jpg"><img alt="Jornada 720" border=0 src="device_screens/P. Jornada 720 (WinMobile).jpg"></a> 
<a href="device_screens/Nec MobilePro 900 (WinMobile).jpg"><img alt="Nec MobilePro 900" border=0 src="device_screens/P. Nec MobilePro 900 (WinMobile).jpg"></a> 
<a href="device_screens/Nokia N900 (Maemo).jpg"><img alt="Nokia N900" border=0 src="device_screens/P. Nokia N900 (Maemo).jpg"></a> 
<a href="device_screens/TungstenT (PalmOS).jpg"><img alt="Tungsten|T" border=0 src="device_screens/P. TungstenT (PalmOS).jpg"></a> 
<a href="device_screens/iPad, Alesis IO Dock, Monotron.jpg"><img alt="iPad, Alesis IO Dock, Monotron" border=0 src="device_screens/P. iPad, Alesis IO Dock, Monotron.jpg"></a> 
<a href="device_screens/iPad2, MIDI.jpg"><img alt="iPad2" border=0 src="device_screens/P. iPad2, MIDI.jpg"></a> 
<a href="device_screens/iPhone plus Monotron.jpg"><img alt="iPhone and Monotron" border=0 src="device_screens/P. iPhone plus Monotron.jpg"></a> 
<a href="device_screens/Samsung Galaxy Note (Android).jpg"><img alt="Galaxy Note (Android)" border=0 src="device_screens/P. Samsung Galaxy Note (Android).jpg"></a> 
<a href="device_screens/OpenPandora (Linux).jpg"><img alt="Galaxy Note (Android)" border=0 src="device_screens/P. OpenPandora (Linux).jpg"></a> 
<a href="device_screens/Sony CLIE PEG-UX50 (PalmOS).jpg"><img alt="Sony CLIE PEG-UX50 (PalmOS)" border=0 src="device_screens/P. Sony CLIE PEG-UX50 (PalmOS).jpg"></a> 
<a href="device_screens/various.jpg"><img alt="Samsung galaxy Fit (Android); Dell Axim X30 (Windows Mobile); Prestigio multyPAD pmp5080b (Android)" border=0 src="device_screens/P. various.jpg"></a> 
<a href="device_screens/LG Optimus Hub (Android).jpg"><img alt="Monotron Duo, LG Optimus Hub (Android), Zoom H4n" border=0 src="device_screens/P. LG Optimus Hub (Android).jpg"></a> 
<a href="device_screens/Palm TX.jpg"><img alt="Palm TX" border=0 src="device_screens/P. Palm TX.jpg"></a> 
<a href="device_screens/iPad mini 2, MIDI.jpg"><img alt="iPad mini 2 + bluetooth MIDI" border=0 src="device_screens/P. iPad mini 2, MIDI.jpg"></a> 
<a href="device_screens/various2.jpg"><img alt="Various Android and iOS devices" border=0 src="device_screens/P. various2.jpg"></a><br><br>
</center>

<div id="vid" class="title2">Видео</div>
<br>

<center>
<iframe class="vid" width="640" height="360" src="https://www.youtube.com/embed/wmfHPnXhmZI" frameborder="0" allowfullscreen></iframe><br><br>
<iframe class="vid" width="640" height="360" src="https://www.youtube.com/embed/xK1VMKaxGoc" frameborder="0" allowfullscreen></iframe><br><br>
<iframe class="vid" width="640" height="360" src="https://www.youtube.com/embed/BG5TZ2V-RQs" frameborder="0" allowfullscreen></iframe><br><br>
<iframe class="vid" width="640" height="360" src="https://www.youtube.com/embed/E54uAhMBA1A" frameborder="0" allowfullscreen></iframe><br><br>
</center>
<a href="../../forum/viewtopic.php?f=3&t=1223">Видео-примеры/уроки</a><br>
<a href="https://www.youtube.com/user/NightRadio2007">Еще видео на моем YouTube канале...</a><br><br>

<div id="music" class="title2">Музыка, написанная в SunVox</div>
<br>

Лучшие треки 
<a href="https://soundcloud.com/nightradio/sets/sunvox-music-best-of-2022">2022</a>, 
<a href="https://soundcloud.com/nightradio/sets/sunvox-music-best-of-2021">2021</a>, 
<a href="https://soundcloud.com/nightradio/sets/sunvox-music-best-of-2020">2020</a>, 
<a href="https://soundcloud.com/nightradio/sets/sunvox-music-best-of-2019">2019</a>, 
<a href="https://soundcloud.com/nightradio/sets/sunvox-music-best-of-2018">2018</a>, 
<a href="https://soundcloud.com/nightradio/sets/sunvox-music-best-of-2017">2017</a>, 
<a href="https://soundcloud.com/nightradio/sets/sunvox-music-best-of-2016">2016</a>, 
<a href="https://soundcloud.com/nightradio/sets/sunvox-music-best-of-2015">2015</a>.
<br>
Пожалуйста, помечайте свои треки тегом #SunVox - так их легче будет найти. 
А лучшие композиции будут включены в ежемесячную подборку на SoundCloud. 
Спасибо!<br>
<br>
Альбомы от NightRadio:<br>
<ul>
<li><a href="../../music/btts/index_ru.php">Back to the Sources</a>
<li><a href="../../music/soul_resonance/index_ru.php">Soul Resonance</a>
<li><a href="https://nightradio.bandcamp.com/album/ether-winds">Ether Winds</a>
</ul>
Конкурсы:<br>
<ul>
<li><a href="../../forum/viewtopic.php?f=10&t=6328">SunVox Compo 2023.02</a>
<li><a href="../../forum/viewtopic.php?f=10&t=6027">SunVox Compo 2022.02</a>
<li><a href="../../forum/viewtopic.php?f=10&t=5646">SunVox Compo 2021.02</a>
<li><a href="../../forum/viewtopic.php?f=10&t=5195">SunVox Compo 2020.02</a>
<li><a href="../../forum/viewtopic.php?f=10&t=4859">SunVox Compo 2019.03</a>
<li><a href="../../compo/sunvox/2015.10">SunVox Compo 2015.10</a>
<li><a href="../../compo/sunvox/2015.02">SunVox Compo 2015.02</a>
<li><a href="../../forum/viewtopic.php?f=10&t=3442">SunVox Compo 2014.10</a>
</ul>

<div id="old" class="title2">Старые версии</div>
<br>

<a href="sunvox-1.8.1-palmos-meego.zip">SunVox 1.8.1 для PalmOS и MeeGo</a><br>
<a href="https://www.dropbox.com/sh/5tjr1m8ri7i455z/AAAQktsEmqH90IGrRGfooqaNa?dl=0">Архив</a><br>


    </div>
    </div>
    </div>
    
    <div id="outer_footer_div">
    <div id="footer_div">
	<font color="#404040">© Alexander Zolotov <a href="mailto:nightradio@gmail.com">nightradio@gmail.com</a></font>
    </div>
    </div>

</body>

</html>


