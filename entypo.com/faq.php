
<!DOCTYPE html>
<!-- This site was created in Webflow. http://www.webflow.com-->
<!-- Last Published: Fri Jan 30 2015 09:39:59 GMT+0000 (UTC) -->
<html data-wf-site="532ab60ff3a4b2cc590003b4" data-wf-page="54b3b84d3fcaea9b3634f029">
<head>
  <meta charset="utf-8">
  <title>Entypo - FAQ</title>
  <meta name="description" content="Frequently asked questions about Entypo — the suite of 411 carefully crafted premium pictograms by Daniel Bruce.">
  <meta name="keywords" content="Entypo Entypo+ Pictograms Icons Social Daniel Bruce">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="generator" content="Webflow">
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/webflow.css">
  <link rel="stylesheet" type="text/css" href="css/entypo.webflow.css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script>
    WebFont.load({
      google: {
        families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Montserrat:400,700","Droid Sans Mono:regular"]
      }
    });
  </script>
  <script type="text/javascript" src="js/modernizr.js"></script>
  <link rel="shortcut icon" type="image/x-icon" href="images/entypo-favicon.png">
  <link rel="apple-touch-icon" href="images/entypo-webclip.png">
</head>
<body>
  <div class="section head">
    <div class="w-container container-head">
      <h1><a href="index.php" class="links">Entypo+</a></h1>
      <h2>411 carefully crafted premium pictograms by <a href="http://www.danielbruce.se" class="links" target="_blank">Daniel Bruce</a> — CC BY-SA 4.0.</h2>
    </div>
    <div class="container-buttons"><a class="navigation-button" href="index.php">Home</a>
      <div class="w-hidden-small w-hidden-tiny navigation-text">•</div><a class="w-hidden-small w-hidden-tiny navigation-button modal-link" href="https://dl-web.dropbox.com/get/Downloads/entypo.zip?_download_id=40492994151784829990402130053505141254134367597523538957872252&_notify_domain=www.dropbox.com&_subject_uid=4339492&dl=1&w=AACbXDcAOxTTExWWUL53fg5tVliR9Dn9TGbVlkEQrCYziA" id="download" onClick="_gaq.push(['_trackEvent', 'Download (Other)', 'V2']);">Download</a>
      <div class="navigation-text">•</div>
      <div class="w-hidden-tiny navigation-text">FAQ</div>
      <div class="w-hidden-tiny navigation-text">•</div><a class="navigation-button" href="mailto:info@entypo.com">Contact</a>
    </div>
  </div>
  <div class="section faq">
    <h3>What is Entypo+?</h3>
    <p class="left">Entypo+ is the third version of a free suite of premium quality pictograms. It’s released under the license <a href="http://creativecommons.org/licenses/by-sa/4.0/" class="links" target="_blank">CC BY-SA 4.0</a>. Each pictogram has been drawn for pixel perfection at a size of 20 x 20 pixels and with a very consistent style. The difference between this version of Entypo and previous ones is that the suite now only&nbsp;consist&nbsp;of SVG images. There is no fonts or PSD.&nbsp;</p>
    <h3>Where is the font?</h3>
    <p class="left">Entypo+ doesn’t come as a font anymore but you can create your own using the SVG images. It’s really easy. Go to <a href="http://fontello.com" class="links" target="_blank">Fontello</a>,&nbsp;<a href="http://www.fontastic.me" class="links" target="_blank">Fontastic.me</a>&nbsp;or the <a href="https://icomoon.io/app" class="links" target="_blank">IcoMoon app</a> to do so.</p>
    <h3>Where can I find the old character map?</h3>
    <p class="left">I don't do support for Entypo 2 anymore, just like Microsoft doesn't do support for Windows XP. But you can always find old versions of the site on&nbsp;<a href="http://t.co/6iNaiM7EZ9" class="links" target="_blank">the Wayback Machine</a>.</p>
    <h3>Why is there no font?</h3>
    <p class="left">Building a font takes a lot of time, time spent better on designing new pictograms. Just deciding on which unicode position to use for each glyph is a huge task and also a source of great debate. Further more, most projects make use of maybe ten, twenty pictograms and a font containing all pictograms is then unnecessary large. In kilobytes that is.</p>
    <h3>Why aren’t the pictograms grouped and/or organized in some smart way?</h3>
    <p class="left">With Entypo+ I have tried to make everything as simple as possible so I can spend the small amount of time I got on drawing new pictograms. So everything apart from drawing, naming and exporting a SVG image had to go. That is why all pictograms now are in a simple alphabetical order.</p>
    <h3>Will there be a new version of Entypo soon?</h3>
    <p class="left">This will most likely be the final version of Entypo but new pictograms will be added to it over time. Follow me on Twitter and/or Dribbble to keep up.</p>
    <h3>How do I give attribution?</h3>
    <p class="left">A mention like “Entypo pictograms by Daniel Bruce — www.entypo.com” is considered acceptable attribution. You can place this in the footer of your website, on an about page or in the code if you don’t wanna give it to much attention.</p>
    <h3>How do I use Entypo+?</h3>
    <p class="left">You can use Entypo in anyway you want. Use it with InDesign for your fancy print magazine or in OmniGraph, wire framing the next app sensation. Note that I’m not a developer so don’t ask me about how to use web fonts or SVG images. Ask Google instead.</p>
    <h3>Is Entypo free even for commercial use?</h3>
    <p class="left">Yes, absolutely.</p>
    <h3>How can I make a web font using the SVG images?</h3>
    <p class="left">Pick the SVG images you want to use and then go to <a href="http://www.fontello.com" class="links" target="_blank">Fontello</a>,&nbsp;<a href="http://www.fontastic.me" class="links" target="_blank">Fontastic.me</a> or the <a href="https://icomoon.io/app" class="links" target="_blank">IcoMoon app</a> and follow their instructions on how to do this. Trust me, it really easy to do.</p>
    <h3>I’m missing a certain pictogram, could you add it?</h3>
    <p class="left">Most of the time yes. Drop me an <a href="mailto:info@entypo.com" class="links">email</a> or tweet and I’ll see what I can do about it.</p>
    <h3>Can I&nbsp;donate to the development of Entypo?</h3>
    <p class="left">Yes, please. Use Paypal and donate to info@entypo.com.</p>
  </div>
  <div class="section foot"><img class="image" src="images/cc.svg" alt="Creative Commons logotype" width="80">
    <div class="made">Site created in</div>
    <a class="w-inline-block" href="http://www.webflow.com" target="_blank"><img class="image" src="images/webflow-logo.svg" width="80" alt=”Webflow logo">
    </a>
    <div>All pictograms has been tested and approved by Leah (6 y/o) and Zoe (4 y/o). Crafted in Stockholm, Sweden 2015 — CC BY-SA 4.0. Thank you&nbsp;<a href="http://www.space2u.com" class="links" target="_blank">Space2u</a> for support. Contact Entypo at <a href="mailto:info@entypo.com" class="links">info@entypo.com</a>.</div>
  </div>
  <div class="w-hidden-small w-hidden-tiny modal-background">
    <div class="modal-window">
      <div class="section social">
        <h1>And that’s your free lunch!</h1>
        <h2>Take that Mr. Friedman. Now why don’t you:</h2>
        <div class="container-buttons social"><a class="social-button twitter" href="http://twitter.com/danielbruce_" target="_blank" onClick="_gaq.push(['_trackEvent', 'Follow (Other)', 'Twitter']);">Follow me on Twitter</a><a class="social-button dribbble" href="http://dribbble.com/danielbruce" target="_blank" onClick="_gaq.push(['_trackEvent', 'Follow (Other)', 'Dribbble']);">and/or on Dribbble</a>
        </div>
      </div>
      <a class="w-inline-block close-modal" href="#"><img src="images/circle-with-cross.svg" alt="Close modal icon" width="30">
      </a>
    </div>
  </div>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script type="text/javascript" src="js/webflow.js"></script>
  <!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.modal-link').click(function() {
        $('.modal-background').fadeIn();
      });
      $('.close-modal').click(function() {
        $('.modal-background').fadeOut();
      });
    });
  </script>
  <script type="text/javascript">
    var _gaq = _gaq || [];
    	_gaq.push(['_setAccount', 'UA-28617882-1']);
    	_gaq.push(['_trackPageview']);
    	(function() {
    	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	})();
  </script>
</body>
</html>