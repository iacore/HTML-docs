
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1.0" />

<style type="text/css">.koka .plaincode, .koka a.pp .pc { display: none; } .koka a.pp { color: inherit; text-decoration: none; }</style>
<link rel="stylesheet" type="text/css" href="styles/koka.css" />

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,400italic,700,700italic" />
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,500,700,400italic" />

<title>std/time documentation</title>
</head>

<body class="koka doc body madoko">
<h1 class="mo">std<span class="fslash">/</span>time</h1>
<div class="doc koka comment">

<p class="p noindent"> Date and time functionality.
</p><h2 id="sec-dates-and-time" class="h1" data-heading-depth="1" style="display:block"><span class="heading-before"><span class="heading-label">1</span>.&#8194;</span>Dates and time</h2>
<p class="p noindent">This is an extensive library for managing time.
By using careful defaults the library offers an easy to use interface for the
common use cases, but uses accurate and precise time representations and algorithms.
In particular, it handles accurate leap seconds, time zones, multiple calendars,
and astronomical time scales.
Internally time is represented by a 128-bit <code class="koka"><a class="pp" href="std_num_ddouble.html#type_space_ddouble"><span class="tp">ddouble</span><span class="pc"><span class="mo">std<span class="fslash">/</span>num<span class="fslash">/</span>ddouble<span class="fslash last">/</span></span>ddouble: <span class="co">V</span></span></a></code> giving it a very high range
and precision (up to 31 decimal digits, see the <code class="koka"><a class="doc link" href="std_time_instant.html"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>instant</span></a></code> module for more
information). As such, we believe this library is one of the most accurate time
libraries around.
</p>
<p class="p indent">Core concepts are:
</p>
<ul class="ul list-star loose">
<li class="li ul-li list-star-li loose-li">
<p>A <code class="koka"><a class="pp" href="std_time_duration.html#type_space_duration"><span class="tp">duration</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>duration<span class="fslash last">/</span></span>duration: <span class="co">V</span></span></a></code> is a span of time in SI seconds (as measured on the earth&#39;s geoid).
</p></li>
<li class="li ul-li list-star-li loose-li">
<p>An <code class="koka"><a class="pp" href="std_time_instant.html#type_space_instant"><span class="tp">instant</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>instant<span class="fslash last">/</span></span>instant: <span class="co">V</span></span></a></code> represents an instant in time. Instants can be substracted to
get the <code class="koka"><a class="pp" href="std_time_duration.html#type_space_duration"><span class="tp">duration</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>duration<span class="fslash last">/</span></span>duration: <span class="co">V</span></span></a></code> between them. Instants are usually represented as the number
of fractional SI seconds since the <code class="koka"><a class="pp" href="std_time_instant.html#epoch">epoch<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>instant<span class="fslash last">/</span></span>epoch: <span class="tp">instant</span></span></a></code> (2000-01-01Z), but can be represented
in different time scales for efficiency. For example, there is the TAI time scale (<code class="koka"><a class="pp" href="std_time_instant.html#ts_tai">ts<span class="dash">-</span>tai<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>instant<span class="fslash last">/</span></span>ts<span class="dash">-</span>tai: <span class="tp">timescale</span></span></a></code>),
standard UTC (<code class="koka"><a class="pp" href="std_time_utc.html#ts_ti">ts<span class="dash">-</span>ti<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>utc<span class="fslash last">/</span></span>ts<span class="dash">-</span>ti: <span class="tp">timescale</span></span></a></code>), or astronomical time scales like terrestrial time (<code class="koka"><a class="doc link" href="std_time_astro.html"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>astro</span></a></code>).
</p></li>
<li class="li ul-li list-star-li loose-li">
<p>A <code class="koka"><a class="pp" href="std_time_time.html#type_space_time"><span class="tp">time</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>time<span class="fslash last">/</span></span>time: <span class="co">V</span></span></a></code> is a human readable <code class="koka"><a class="pp" href="std_time_date.html#type_space_date"><span class="tp">date</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>date<span class="fslash last">/</span></span>date: <span class="co">V</span></span></a></code> (year, month, day), and <code class="koka"><a class="pp" href="std_time_date.html#type_space_clock"><span class="tp">clock</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>date<span class="fslash last">/</span></span>clock: <span class="co">V</span></span></a></code> (hour, minutes, seconds)
in a particular <code class="koka"><a class="pp" href="std_time_calendar.html#type_space_calendar"><span class="tp">calendar</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>calendar<span class="fslash last">/</span></span>calendar: <span class="co">V</span></span></a></code> (usually the standard ISO calendar) and time zone.
Instants can be converted to time values and back. We can add logical years, months, or
days to a <code class="koka"><a class="pp" href="std_time_time.html#type_space_time"><span class="tp">time</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>time<span class="fslash last">/</span></span>time: <span class="co">V</span></span></a></code>, or get the difference between times in days etc. See the
<code class="koka"><a class="doc link" href="std_time_time.html"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>time</span></a></code> module for more information.
</p></li></ul>

<p class="p noindent">The current time, in the UTC timezone (<code class="koka"><a class="pp" href="std_time_calendar.html#tz_utc">tz<span class="dash">-</span>utc<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>calendar<span class="fslash last">/</span></span>tz<span class="dash">-</span>utc: <span class="tp">timezone</span></span></a></code>) using the ISO calendar (<code class="koka"><a class="pp" href="std_time_calendar.html#cal_iso">cal<span class="dash">-</span>iso<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>calendar<span class="fslash last">/</span></span>cal<span class="dash">-</span>iso: <span class="tp">calendar</span></span></a></code>):
</p><pre class="koka source"><span class="plaincode">&gt; time-now().show
"2017-01-30T15:40:25.367000103Z"
</span><span class="nicecode"><span class="op">&gt;</span> time<span class="dash">-</span>now()<span class="kw op">.</span>show
<span class="st">"2017-01-30T15:40:25.367000103Z"</span>
</span></pre>



<p class="p noindent">Or the current time in the local timezone:
</p><pre class="koka source"><span class="plaincode">&gt; local-time-now().show
"2017-01-30T07:59:01.693000078-08:00 (PST)"
</span><span class="nicecode"><span class="op">&gt;</span> local<span class="dash">-</span>time<span class="dash">-</span>now()<span class="kw op">.</span>show
<span class="st">"2017-01-30T07:59:01.693000078-08:00 (PST)"</span>
</span></pre>



<p class="p noindent">We can also get the current instant in time as the number of SI seconds since the <code class="koka"><a class="pp" href="std_time_instant.html#epoch">epoch<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>instant<span class="fslash last">/</span></span>epoch: <span class="tp">instant</span></span></a></code>:
</p><pre class="koka source"><span class="plaincode">&gt; now()
539106063.980999947s
</span><span class="nicecode"><span class="op">&gt;</span> <a class="pp" href="std_time_chrono.html#now">now<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>chrono<span class="fslash last">/</span></span>now: <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp sp">&lt;</span><span class="tp">ndet</span><span class="tp sp">,</span><span class="tp">utc</span><span class="tp sp">&gt;</span> <span class="tp">instant</span></span></a>()
<span class="number">539106063.980999947</span>s
</span></pre>


<p class="p noindent">See <code class="koka"><a class="doc link" href="std_time_chrono.html"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>chrono</span></a></code> for more information on system time.
</p>
<p class="p indent">Calendar times can be constructed by giving a year, month (1-based), and day of the month (1-based)
and optional hour, minutes, seconds, fraction of the second, timezone (<code class="koka"><a class="pp" href="std_time_calendar.html#tz_utc">tz<span class="dash">-</span>utc<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>calendar<span class="fslash last">/</span></span>tz<span class="dash">-</span>utc: <span class="tp">timezone</span></span></a></code>) and
calendar (<code class="koka"><a class="pp" href="std_time_calendar.html#cal_iso">cal<span class="dash">-</span>iso<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>calendar<span class="fslash last">/</span></span>cal<span class="dash">-</span>iso: <span class="tp">calendar</span></span></a></code>):
</p><pre class="koka source"><span class="plaincode">&gt; time(2000,1,1).show
"2000-01-01T00:00:00Z"
</span><span class="nicecode"><span class="op">&gt;</span> time(<span class="number">2000</span>,<span class="number">1</span>,<span class="number">1</span>)<span class="kw op">.</span>show
<span class="st">"2000-01-01T00:00:00Z"</span>
</span></pre>



<p class="p noindent">Instants can be constructed similarly, and can be converted to and from
<code class="koka"><a class="pp" href="std_time_time.html#type_space_time"><span class="tp">time</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>time<span class="fslash last">/</span></span>time: <span class="co">V</span></span></a></code> values. Here we construct a time value interpreted in
the TAI time scale (which was 32 seconds ahead of UTC on 2000-01-01Z):
</p><pre class="koka source"><span class="plaincode">&gt; instant(2000,1,1).time(cal=cal-tai).show
"2000-01-01T00:00:32Z TAI"
</span><span class="nicecode"><span class="op">&gt;</span> instant(<span class="number">2000</span>,<span class="number">1</span>,<span class="number">1</span>)<span class="kw op">.</span>time(cal<span class="kw">=</span>cal<span class="dash">-</span>tai)<span class="kw op">.</span>show
<span class="st">"2000-01-01T00:00:32Z TAI"</span>
</span></pre>



<p class="p noindent">The <code class="koka">cal<span class="dash">-</span>tai</code> calendar is the standard ISO calendar but uses the TAI time scale (<code class="koka"><a class="pp" href="std_time_instant.html#ts_tai">ts<span class="dash">-</span>tai<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>instant<span class="fslash last">/</span></span>ts<span class="dash">-</span>tai: <span class="tp">timescale</span></span></a></code>)
instead of UTC time.
</p><h3 id="sec-leap-seconds" class="h2" data-heading-depth="2" style="display:block"><span class="heading-before"><span class="heading-label">1.1</span>.&#8194;</span>Leap seconds</h3>
<p class="p noindent">The library takes much care to correctly handle leap seconds and time zone transitions.
For example, here are some durations between different times:
</p><pre class="koka source"><span class="plaincode">&gt; (time(2016,1,1,0,0,0,0.5) - time(2015,12,31,23,59,59)).show
"1.5s"

&gt; (time(2017,1,1,0,0,0,0.5) - time(2016,12,31,23,59,59)).show
"2.5s" // over a leap second

&gt; (time(2017,1,1,0,0,0,0.5) - time(2017,1,1,2,59,59,tz=tz-fixed(3)).show
"2.5s" // UTC+3, and over a leap second
</span><span class="nicecode"><span class="op">&gt;</span> (time(<span class="number">2016</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">0</span>,<span class="number">0</span>,<span class="number">0</span>,<span class="number">0.5</span>) <span class="op"><span class="minus">-</span></span> time(<span class="number">2015</span>,<span class="number">12</span>,<span class="number">31</span>,<span class="number">23</span>,<span class="number">59</span>,<span class="number">59</span>))<span class="kw op">.</span>show
<span class="st">"1.5s"</span>

<span class="op">&gt;</span> (time(<span class="number">2017</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">0</span>,<span class="number">0</span>,<span class="number">0</span>,<span class="number">0.5</span>) <span class="op"><span class="minus">-</span></span> time(<span class="number">2016</span>,<span class="number">12</span>,<span class="number">31</span>,<span class="number">23</span>,<span class="number">59</span>,<span class="number">59</span>))<span class="kw op">.</span>show
<span class="st">"2.5s"</span> <span class="comment">// over a leap second
</span>
<span class="op">&gt;</span> (time(<span class="number">2017</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">0</span>,<span class="number">0</span>,<span class="number">0</span>,<span class="number">0.5</span>) <span class="op"><span class="minus">-</span></span> time(<span class="number">2017</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">2</span>,<span class="number">59</span>,<span class="number">59</span>,tz<span class="kw">=</span><a class="pp" href="std_time_calendar.html#tz_fixed">tz<span class="dash">-</span>fixed<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>calendar<span class="fslash last">/</span></span>tz<span class="dash">-</span>fixed: <span class="tp sp">(</span><span class="tp tpp">hours</span> <span class="tp kw op">:</span> <span class="tp">int</span><span class="tp sp">,</span> <span class="tp tpp">mins</span> <span class="tp kw op">:</span> <span class="tp op">?</span><span class="tp">int</span><span class="tp sp">,</span> <span class="tp tpp">name</span> <span class="tp kw op">:</span> <span class="tp op">?</span><span class="tp">string</span><span class="tp sp">,</span> <span class="tp tpp">abbrv</span> <span class="tp kw op">:</span> <span class="tp op">?</span><span class="tp">string</span><span class="tp sp">,</span> <span class="tp tpp">hourwidth</span> <span class="tp kw op">:</span> <span class="tp op">?</span><span class="tp">int</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">timezone</span></span></a>(<span class="number">3</span>))<span class="kw op">.</span>show
<span class="st">"2.5s"</span> <span class="comment">// UTC+3, and over a leap second
</span></span></pre>



<p class="p noindent">Similarly, adding logical <em class="em-low1">days</em> (or weeks, months, or years) works over leap seconds and
other time irregularties:
</p><pre class="koka source"><span class="plaincode">&gt; time(2016,12,31,12).add-days(1).show
"2017-01-01T12:00:00Z"   // over a leap second

&gt; time(1582,10,4,cal=cal-jg).add-days(1).show
"1582-10-15T00:00:00Z JG"  // transition from Julian (&#96;cal-julian&#96;) to Gregorian (&#96;cal-gregorian&#96;) calendar)
</span><span class="nicecode"><span class="op">&gt;</span> time(<span class="number">2016</span>,<span class="number">12</span>,<span class="number">31</span>,<span class="number">12</span>)<span class="kw op">.</span>add<span class="dash">-</span>days(<span class="number">1</span>)<span class="kw op">.</span>show
<span class="st">"2017-01-01T12:00:00Z"</span>   <span class="comment">// over a leap second
</span>
<span class="op">&gt;</span> time(<span class="number">1582</span>,<span class="number">10</span>,<span class="number">4</span>,cal<span class="kw">=</span><a class="pp" href="std_time_calendars.html#cal_jg">cal<span class="dash">-</span>jg<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>calendars<span class="fslash last">/</span></span>cal<span class="dash">-</span>jg: <span class="tp">calendar</span></span></a>)<span class="kw op">.</span>add<span class="dash">-</span>days(<span class="number">1</span>)<span class="kw op">.</span>show
<span class="st">"1582-10-15T00:00:00Z JG"</span>  <span class="comment">// transition from Julian (&#96;cal-julian&#96;) to Gregorian (&#96;cal-gregorian&#96;) calendar)
</span></span></pre>



<p class="p noindent">See the <code class="koka"><a class="doc link" href="#utc">std<span class="fslash">/</span>time<span class="fslash">/</span>utc</a></code> module for more information on leap seconds.
</p><h3 id="sec-time-zones" class="h2" data-heading-depth="2" style="display:block"><span class="heading-before"><span class="heading-label">1.2</span>.&#8194;</span>Time zones</h3>
<p class="p noindent">If calculating with timezones besides the UTC (<code class="koka"><a class="pp" href="std_time_calendar.html#tz_utc">tz<span class="dash">-</span>utc<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>calendar<span class="fslash last">/</span></span>tz<span class="dash">-</span>utc: <span class="tp">timezone</span></span></a></code>)
and local timezone (<code class="koka"><a class="pp" href="std_time_calendar.html#tz_local">tz<span class="dash">-</span>local<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>calendar<span class="fslash last">/</span></span>tz<span class="dash">-</span>local: <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-&gt;</span> <span class="tp">ndet</span> <span class="tp">timezone</span></span></a></code>), one needs to use the <code class="koka"><a class="doc link" href="std_time_timezone.html"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>timezone</span></a></code>
module to load the latest time zone information (<code class="koka"><a class="doc link" href="std_time_timezone.html#load_timezones">std<span class="fslash">/</span>time<span class="fslash">/</span>timezone<span class="fslash">/</span>load<span class="dash">-</span>timezones</a></code>).
</p><pre class="koka source"><span class="plaincode">import std/time
import std/time/timezone

fun test-timezones() {
  val tzs = load-timezones()
  val tz1 = tzs.timezone("America/New&#95;York")
  val tz2 = tzs.timezone("Pacific/Kosrae")

  /&#42; A flight of 12.5 hours from New York to Kosrae &#42;/
  val t1  = instant(1997,12,31,12,1,0,tz=tz1) + (12.hours + 30.minutes)
  t1.time(tz=tz2).show.println   // "1998-01-01T17:31:00+12:00"

  /&#42; Again, but across DST &amp; leap second &#42;/
  val t2  = instant(1998,12,31,12,1,0,tz=tz1) + (12.hours + 30.minutes)
  t2.time(tz=tz2).show.println   // "1999-01-01T16:30:59+11:00"
}
</span><span class="nicecode"><span class="kw">import</span> <a class="doc link" href="std.html#time">std<span class="fslash">/</span>time</a>
<span class="kw">import</span> <a class="doc link" href="#timezone">std<span class="fslash">/</span>time<span class="fslash">/</span>timezone</a>

<span class="kw">fun</span> test<span class="dash">-</span>timezones() {
  <span class="kw">val</span> tzs <span class="kw">=</span> load<span class="dash">-</span>timezones()
  <span class="kw">val</span> tz1 <span class="kw">=</span> tzs<span class="kw op">.</span><a class="pp" href="std_time_time.html#timezone">timezone<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>time<span class="fslash last">/</span></span>timezone: <span class="tp sp">(</span><span class="tp tpp">time</span> <span class="tp kw op">:</span> <span class="tp">time</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">timezone</span></span></a>(<span class="st">"America/New&#95;York"</span>)
  <span class="kw">val</span> tz2 <span class="kw">=</span> tzs<span class="kw op">.</span><a class="pp" href="std_time_time.html#timezone">timezone<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>time<span class="fslash last">/</span></span>timezone: <span class="tp sp">(</span><span class="tp tpp">time</span> <span class="tp kw op">:</span> <span class="tp">time</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">timezone</span></span></a>(<span class="st">"Pacific/Kosrae"</span>)

  <span class="comment">/&#42; A flight of 12.5 hours from New York to Kosrae &#42;/</span>
  <span class="kw">val</span> t1  <span class="kw">=</span> instant(<span class="number">1997</span>,<span class="number">12</span>,<span class="number">31</span>,<span class="number">12</span>,<span class="number">1</span>,<span class="number">0</span>,tz<span class="kw">=</span>tz1) <span class="op">+</span> (<span class="number">12</span><span class="kw op">.</span>hours <span class="op">+</span> <span class="number">30</span><span class="kw op">.</span>minutes)
  t1<span class="kw op">.</span>time(tz<span class="kw">=</span>tz2)<span class="kw op">.</span>show<span class="kw op">.</span>println   <span class="comment">// "1998-01-01T17:31:00+12:00"
</span>
  <span class="comment">/&#42; Again, but across DST &amp; leap second &#42;/</span>
  <span class="kw">val</span> t2  <span class="kw">=</span> instant(<span class="number">1998</span>,<span class="number">12</span>,<span class="number">31</span>,<span class="number">12</span>,<span class="number">1</span>,<span class="number">0</span>,tz<span class="kw">=</span>tz1) <span class="op">+</span> (<span class="number">12</span><span class="kw op">.</span>hours <span class="op">+</span> <span class="number">30</span><span class="kw op">.</span>minutes)
  t2<span class="kw op">.</span>time(tz<span class="kw">=</span>tz2)<span class="kw op">.</span>show<span class="kw op">.</span>println   <span class="comment">// "1999-01-01T16:30:59+11:00"
</span>}
</span></pre>


<h3 id="sec-intervals" class="h2" data-heading-depth="2" style="display:block"><span class="heading-before"><span class="heading-label">1.3</span>.&#8194;</span>Intervals</h3>
<p class="p noindent">Using the <code class="koka"><a class="pp" href="std_time_chrono.html#now">now<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>chrono<span class="fslash last">/</span></span>now: <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-&gt;</span> <span class="tp sp">&#60;</span><span class="tp">ndet</span><span class="tp sp">,</span><span class="tp">utc</span><span class="tp sp">&#62;</span> <span class="tp">instant</span></span></a></code> or <code class="koka">time<span class="dash">-</span>now</code> for measuring time intervals is not appropriate since these functions
are not guaranteed to be monotonic or of sufficiently high precision. For timing, the <code class="koka"><a class="doc link" href="std_time_timer.html"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>timer</span></a></code> module
should be used with functions like <code class="koka"><a class="pp" href="std_time_timer.html#ticks">ticks<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>timer<span class="fslash last">/</span></span>ticks: <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-&gt;</span> <span class="tp">ndet</span> <span class="tp">duration</span></span></a></code> or <code class="koka"><a class="pp" href="std_time_timer.html#elapsed">elapsed<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>timer<span class="fslash last">/</span></span>elapsed: <span class="tp kw">forall</span><span class="tp sp">&#60;</span><span class="tp tv">a</span><span class="tp sp">,</span><span class="tp tv">e</span><span class="tp sp">&#62;</span> <span class="tp sp">(</span><span class="tp tpp">action</span> <span class="tp kw op">:</span> <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-&gt;</span> <span class="tp sp">&#60;</span><span class="tp">ndet</span><span class="tp kw op">|</span><span class="tp tv">e</span><span class="tp sp">&#62;</span> <span class="tp tv">a</span><span class="tp sp">)</span> <span class="tp kw op">-&gt;</span> <span class="tp sp">&#60;</span><span class="tp">ndet</span><span class="tp kw op">|</span><span class="tp tv">e</span><span class="tp sp">&#62;</span> <span class="tp sp">(</span><span class="tp">duration</span><span class="tp sp">,</span> <span class="tp tv">a</span><span class="tp sp">)</span></span></a></code>:
</p><pre class="koka source"><span class="plaincode">import std/time/timer

fun nfib(n : int) : int {
  if (n &lt;= 1) then 1 else 1 + nfib(n - 1) + nfib(n - 2)
}

fun test-nfib() {
  print-elapsed{ nfib(30) }   // "elapsed 0.111s"
}
</span><span class="nicecode"><span class="kw">import</span> <a class="doc link" href="#timer">std<span class="fslash">/</span>time<span class="fslash">/</span>timer</a>

<span class="kw">fun</span> nfib(n <span class="tp kw op">:</span> <a class="pp" href="std_core_types.html#type_space_int"><span class="tp">int</span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>int: <span class="co">V</span></span></a>) <span class="tp kw op">:</span> <a class="pp" href="std_core_types.html#type_space_int"><span class="tp">int</span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>int: <span class="co">V</span></span></a> {
  <span class="kw">if</span> (n <span class="op">&lt;=</span> <span class="number">1</span>) <span class="kw">then</span> <span class="number">1</span> <span class="kw">else</span> <span class="number">1</span> <span class="op">+</span> nfib(n <span class="op"><span class="minus">-</span></span> <span class="number">1</span>) <span class="op">+</span> nfib(n <span class="op"><span class="minus">-</span></span> <span class="number">2</span>)
}

<span class="kw">fun</span> test<span class="dash">-</span>nfib() {
  <a class="pp" href="std_time_timer.html#print_elapsed">print<span class="dash">-</span>elapsed<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>timer<span class="fslash last">/</span></span>print<span class="dash">-</span>elapsed: <span class="tp kw">forall</span><span class="tp sp">&lt;</span><span class="tp tv">a</span><span class="tp sp">,</span><span class="tp tv">e</span><span class="tp sp">&gt;</span> <span class="tp sp">(</span><span class="tp tpp">action</span> <span class="tp kw op">:</span> <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp sp">&lt;</span><span class="tp">ndet</span><span class="tp sp">,</span><span class="tp">console</span><span class="tp kw op">|</span><span class="tp tv">e</span><span class="tp sp">&gt;</span> <span class="tp tv">a</span><span class="tp sp">,</span> <span class="tp tpp">msg</span> <span class="tp kw op">:</span> <span class="tp op">?</span><span class="tp">string</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp sp">&lt;</span><span class="tp">ndet</span><span class="tp sp">,</span><span class="tp">console</span><span class="tp kw op">|</span><span class="tp tv">e</span><span class="tp sp">&gt;</span> <span class="tp tv">a</span></span></a>{ nfib(<span class="number">30</span>) }   <span class="comment">// "elapsed 0.111s"
</span>}
</span></pre>


<h3 id="sec-utc" class="h2" data-heading-depth="2" style="display:block"><span class="heading-before"><span class="heading-label">1.4</span>.&#8194;</span>UTC</h3>
<p class="p noindent">The library uses by default the International Time (TI) time scale (<code class="koka"><a class="pp" href="std_time_utc.html#ts_ti">ts<span class="dash">-</span>ti<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>utc<span class="fslash last">/</span></span>ts<span class="dash">-</span>ti: <span class="tp">timescale</span></span></a></code>) which
uses UTC time with leap seconds up to 2019, and ignores leap seconds after
this. This is a nice default as it tracks historical leap seconds precisely
while having deterministic future time calculations. Moreover, even if there
are future leap seconds, only durations calculations between times will be
off by the inserted leap seconds; <code class="koka">time<span class="dash">-</span>now</code>, <code class="koka"><a class="pp" href="std_time_time.html#type_space_time"><span class="tp">time</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>time<span class="fslash last">/</span></span>time: <span class="co">V</span></span></a></code>, and <code class="koka"><span class="tp">instants</span></code>
stay valid and designate correct times (as they use a <em class="em-low1">day + seconds</em>
internal representation).
</p>
<p class="p indent">There are two other good UTC time scales though:
</p>
<ul class="ul list-dash compact">
<li class="li ul-li list-dash-li compact-li"><code class="koka"><a class="pp" href="std_time_utc.html#ts_ti_sls">ts<span class="dash">-</span>ti<span class="dash">-</span>sls<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>utc<span class="fslash last">/</span></span>ts<span class="dash">-</span>ti<span class="dash">-</span>sls: <span class="tp">timescale</span></span></a></code> smooths leap seconds over the last 1000s of a day with a leap
seccond so they never appear in any displayed time or timestamp.
</li>
<li class="li ul-li list-dash-li compact-li"><code class="koka"><a class="pp" href="std_time_utc.html#ts_utc_create">ts<span class="dash">-</span>utc<span class="dash">-</span>create<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>utc<span class="fslash last">/</span></span>ts<span class="dash">-</span>utc<span class="dash">-</span>create: <span class="tp sp">(</span><span class="tp tpp">leaps</span> <span class="tp kw op">:</span> <span class="tp">leaps<span class="dash">-</span>table</span><span class="tp sp">)</span> <span class="tp kw op">-&gt;</span> <span class="tp">timescale</span></span></a></code>/<code class="koka">cal<span class="dash">-</span>utc<span class="dash">-</span>load</code> creates a proper UTC timescale from a leap
second table. This ensures future leap seconds are fully accounted for and
that duration calculations work properly up to 6 months into the future.
</li></ul>

<p class="p noindent">.
</p></div>
<table class="index"><tr><td class="code"><span class="nested0">std</span></td></tr>
<tr><td class="code"><span class="nested1"><a class="link" href="std_core.html"><span class="mo">core</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">Core functions.
</p></div></td></tr>
<tr><td class="code"><span class="nested1">time</span></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_calendar.html"><span class="mo">calendar</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">Calendars.  
</p></div></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_calendars.html"><span class="mo">calendars</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">Various calendars: TAI, GPS, ISO week, Julian, Coptic, etc.
</p></div></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_chrono.html"><span class="mo">chrono</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">Get the system time.
</p></div></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_date.html"><span class="mo">date</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">Date, clock, and week days support.
</p></div></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_duration.html"><span class="mo">duration</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">Time durations in SI seconds.
</p></div></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_format.html"><span class="mo">format</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">Formatting of time into a string.
</p></div></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_instant.html"><span class="mo">instant</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">Instants in time.
</p></div></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_locale.html"><span class="mo">locale</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">Localization of time formats.
</p></div></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_parse.html"><span class="mo">parse</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">Parsing of time strings.
</p></div></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_time.html"><span class="mo">time</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">Time represents instants as human readable calendar time.
</p></div></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_timer.html"><span class="mo">timer</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">High resolution timer.
</p></div></td></tr>
<tr><td class="code"><span class="nested2"><a class="link" href="std_time_utc.html"><span class="mo">utc</span></a></span></td><td><div class="doc koka comment">

<p class="p noindent">UTC time scales and leap second support.
</p></div></td></tr>
</table>
</body>
</html>

<span data-line=""></span>