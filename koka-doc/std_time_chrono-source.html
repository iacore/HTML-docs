<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1.0" />

<style type="text/css">.koka .plaincode, .koka a.pp .pc { display: none; } .koka a.pp { color: inherit; text-decoration: none; }</style>
<link rel="stylesheet" type="text/css" href="styles/koka.css" />

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,400italic,700,700italic" />
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,500,700,400italic" />

<title>chrono.kk documentation</title>
</head>

<body class="koka doc body madoko">
<pre class="koka source"><span class="comment">/&#42;----------------------------------------------------------------------------
   Copyright 2012-2021, Microsoft Research, Daan Leijen.

   Licensed under the Apache License, Version 2.0 ("The Licence"). You may not
   use this file except in compliance with the License. A copy of the License
   can be found in the LICENSE file at the root of this distribution.
----------------------------------------------------------------------------&#42;/</span>

<span class="comment">/&#42; Get the system time.

&#42;/</span>
<span class="kw">module</span> chrono

<span class="kw">import</span> timestamp
<span class="kw">import</span> duration
<span class="kw">import</span> instant
<span class="kw">import</span> utc

<span class="kw">extern</span> <span class="kw">import</span>
  c  file <span class="st">"chrono-inline.c"</span>
  cs file <span class="st">"chrono-inline.cs"</span>
  js file <span class="st">"chrono-inline.js"</span>

<span class="comment">// The current &#96;:instant&#96; in time as returned by the system clock
// in the UTC timescale. Equivalent to &#96;now-in(utc())&#96;, see
// &#96;now-in&#96; for further information about the system clock.
</span><span class="decl-fun" id="now"><span class="kw">pub</span> <span class="kw">fun</span> <a class="pp" href="std_time_chrono.html#now">now<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>chrono<span class="fslash last">/</span></span>now: <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp sp">&lt;</span><span class="tp">ndet</span><span class="tp sp">,</span><span class="tp">utc</span><span class="tp sp">&gt;</span> <span class="tp">instant</span></span></a>() <span class="tp kw op">:</span> <a class="pp" href="std_core_types-source.html#type_space__lt__gt_"><span class="tp"><span class="effect">&lt;</span></span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>(&lt;&gt;): <span class="co">E</span></span></a><a class="pp" href="std_core_types-source.html#type_space_ndet"><span class="tp"><span class="effect">ndet</span></span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>ndet: <span class="co">X</span></span></a><span class="tp sp">,</span><a class="pp" href="std_time_utc-source.html#type_space_utc"><span class="tp">utc</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>utc<span class="fslash last">/</span></span>utc: <span class="co">HX</span></span></a><span class="tp sp">&gt;</span> <a class="pp" href="std_time_instant-source.html#type_space_instant"><span class="tp">instant</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>instant<span class="fslash last">/</span></span>instant: <span class="co">V</span></span></a>
  <a class="pp" href="#now_in">now-in<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>chrono<span class="fslash last">/</span></span>now<span class="dash">-</span>in: <span class="tp sp">(</span><span class="tp tpp">ts</span> <span class="tp kw op">:</span> <span class="tp op">?</span><span class="tp">timescale</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">ndet</span> <span class="tp">instant</span></span></a>(<a class="pp" href="std_time_utc-source.html#utc">utc<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>utc<span class="fslash last">/</span></span>utc: <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">utc</span> <span class="tp">timescale</span></span></a>()</span>)


<span class="comment">// The current &#96;:instant&#96; in time as returned by the system clock
// in an optional time scale &#96;ts&#96; (&#96;= ts-ti&#96;).
//
// This uses the best available system clock for the requested
// timescale. For example
// it uses [&#96;&#96;CLOCK&#95;UTC&#96;&#96;](http://www.madore.org/~david/computers/unix-leap-seconds.html)
// when available to get proper UTC time, or &#96;&#96;CLOCK&#95;TAI&#96;&#96; for TAI time.
//
// Otherwise, it usually uses Unix (POSIX) time (&#96;&#96;CLOCK&#95;REALTIME&#96;&#96;).
// Unfortunately, most operating systems cannot not report time in leap
// seconds accurately. The &#96;now&#96; function is limited by the OS in this case.
//
// To guard against inaccurate clocks and increase monotonicity,
// the &#96;now&#96; function guarantees that if the current measurement is
// upto 1 second in the past with regard to the previous call to &#96;now&#96;,
// that the returned instant is monotonic by adding nano seconds to the
// previous measurement until the system clock catches up again.
//
// This is effective in particular on older OS's where the time sometimes jumps
// back one second after a leap second. By limiting the adjustment to at most
// one second it ensures the clock catches up soon and does not affect the user
// setting a new time in the past.
</span><span class="decl-fun" id="now_in"><span class="kw">pub</span> <span class="kw">fun</span> <a class="pp" href="std_time_chrono.html#now_in">now-in<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>chrono<span class="fslash last">/</span></span>now<span class="dash">-</span>in: <span class="tp sp">(</span><span class="tp tpp">ts</span> <span class="tp kw op">:</span> <span class="tp op">?</span><span class="tp">timescale</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">ndet</span> <span class="tp">instant</span></span></a>( <a class="pp">ts<span class="pc">ts: <span class="tp op">?</span><span class="tp">timescale</span></span></a> <span class="tp kw op">:</span> <a class="pp" href="std_core_types-source.html#type_space_optional"><span class="tp">timescale</span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>optional: <span class="co">V</span> <span class="kw op">-></span> <span class="co">V</span></span></a> <span class="kw">=</span> <a class="pp" href="std_time_utc-source.html#ts_ti">ts-ti<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>utc<span class="fslash last">/</span></span>ts<span class="dash">-</span>ti: <span class="tp">timescale</span></span></a>) <span class="tp kw op">:</span> <a class="pp" href="std_core_types-source.html#type_space_ndet"><span class="tp"><span class="effect">ndet</span></span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>ndet: <span class="co">X</span></span></a> <a class="pp" href="std_time_instant-source.html#type_space_instant"><span class="tp">instant</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>instant<span class="fslash last">/</span></span>instant: <span class="co">V</span></span></a>
  <span class="comment">// on current backends (C#, JavaScript) we can only use &#96;unix-now&#96; :-(
</span>  <span class="kw">val</span> <a class="pp" href="std_core_types-source.html#con_space__lp__comma__rp_"><span class="co">(</span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>(,): <span class="tp kw">forall</span><span class="tp sp">&lt;</span><span class="tp tv">a</span><span class="tp sp">,</span><span class="tp tv">b</span><span class="tp sp">&gt;</span> <span class="tp sp">(</span><span class="tp tpp">fst</span> <span class="tp kw op">:</span> <span class="tp tv">a</span><span class="tp sp">,</span> <span class="tp tpp">snd</span> <span class="tp kw op">:</span> <span class="tp tv">b</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp sp">(</span><span class="tp tv">a</span><span class="tp sp">,</span> <span class="tp tv">b</span><span class="tp sp">)</span></span></a><a class="pp">secs<span class="pc">secs: <span class="tp">float64</span></span></a>,<a class="pp">frac<span class="pc">frac: <span class="tp">float64</span></span></a><a class="pp" href="std_core_types-source.html#con_space__lp__comma__rp_"><span class="co">)</span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>(,): <span class="tp kw">forall</span><span class="tp sp">&lt;</span><span class="tp tv">a</span><span class="tp sp">,</span><span class="tp tv">b</span><span class="tp sp">&gt;</span> <span class="tp sp">(</span><span class="tp tpp">fst</span> <span class="tp kw op">:</span> <span class="tp tv">a</span><span class="tp sp">,</span> <span class="tp tpp">snd</span> <span class="tp kw op">:</span> <span class="tp tv">b</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp sp">(</span><span class="tp tv">a</span><span class="tp sp">,</span> <span class="tp tv">b</span><span class="tp sp">)</span></span></a> <span class="kw">=</span> <a class="pp" href="#unix_now">unix-now<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>chrono<span class="fslash last">/</span></span>unix<span class="dash">-</span>now: <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">ndet</span> <span class="tp sp">(</span><span class="tp">float64</span><span class="tp sp">,</span> <span class="tp">float64</span><span class="tp sp">)</span></span></a>()
  <span class="kw">val</span> <a class="pp">leap<span class="pc">leap: <span class="tp">int</span></span></a> <span class="kw">=</span> <span class="number">0</span>
  <a class="pp" href="std_time_utc-source.html#unix_instant_2">unix-instant<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>utc<span class="fslash last">/</span></span>unix<span class="dash">-</span>instant<span class="postfix">.2</span>: <span class="tp sp">(</span><span class="tp tpp">t</span> <span class="tp kw op">:</span> <span class="tp">timespan</span><span class="tp sp">,</span> <span class="tp tpp">leap</span> <span class="tp kw op">:</span> <span class="tp op">?</span><span class="tp">int</span><span class="tp sp">,</span> <span class="tp tpp">ts</span> <span class="tp kw op">:</span> <span class="tp op">?</span><span class="tp">timescale</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">instant</span></span></a>(<a class="pp" href="std_time_timestamp-source.html#timespan_2">timespan<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>timestamp<span class="fslash last">/</span></span>timespan<span class="postfix">.2</span>: <span class="tp sp">(</span><span class="tp tpp">secs</span> <span class="tp kw op">:</span> <span class="tp">float64</span><span class="tp sp">,</span> <span class="tp tpp">frac</span> <span class="tp kw op">:</span> <span class="tp">float64</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">timespan</span></span></a>(<a class="pp">secs<span class="pc">secs: <span class="tp">float64</span></span></a>,<a class="pp">frac<span class="pc">frac: <span class="tp">float64</span></span></a>),<a class="pp">leap<span class="pc">leap: <span class="tp">int</span></span></a>,<a class="pp">ts<span class="pc">ts: <span class="tp">timescale</span></span></a></span>)

<span class="comment">// Returns a unix time stamp as seconds and fraction of seconds;
// The fraction of seconds is for added precision if necessary,
// and can be larger than one to indicate leap seconds.
// This still needs to be adjusted to our epoch and taking account of leap seconds.
</span><span class="decl-fun" id="unix_now"><span class="kw">extern</span> <a class="pp" href="std_time_chrono.html#unix_now">unix-now<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>chrono<span class="fslash last">/</span></span>unix<span class="dash">-</span>now: <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">ndet</span> <span class="tp sp">(</span><span class="tp">float64</span><span class="tp sp">,</span> <span class="tp">float64</span><span class="tp sp">)</span></span></a>() <span class="tp kw op">:</span> <a class="pp" href="std_core_types-source.html#type_space_ndet"><span class="tp"><span class="effect">ndet</span></span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>ndet: <span class="co">X</span></span></a> <a class="pp" href="std_core_types-source.html#type_space__lp__comma__rp_"><span class="tp">(</span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>(,): <span class="tp sp">(</span><span class="co">V</span>, <span class="co">V</span>) <span class="kw op">-></span> <span class="co">V</span></span></a><a class="pp" href="std_core_types-source.html#type_space_float64"><span class="tp">float64</span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>float64: <span class="co">V</span></span></a><span class="tp sp">,</span><a class="pp" href="std_core_types-source.html#type_space_float64"><span class="tp">float64</span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>float64: <span class="co">V</span></span></a><a class="pp" href="std_core_types-source.html#type_space__lp__comma__rp_"><span class="tp">)</span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>(,): <span class="tp sp">(</span><span class="co">V</span>, <span class="co">V</span>) <span class="kw op">-></span> <span class="co">V</span></span></a>
  c  <span class="st">"kk&#95;time&#95;unix&#95;now&#95;tuple"</span>
  cs <span class="st">"&#95;Chrono.UnixNow"</span>
  js <span class="st">"&#95;unix&#95;now"</span></span>

<span class="comment">// Return the smallest time difference that the system clock can measure.
</span><span class="decl-fun" id="now_resolution"><span class="kw">pub</span> <span class="kw">fun</span> <a class="pp" href="std_time_chrono.html#now_resolution">now-resolution<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>chrono<span class="fslash last">/</span></span>now<span class="dash">-</span>resolution: <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">ndet</span> <span class="tp">duration</span></span></a>() <span class="tp kw op">:</span> <a class="pp" href="std_core_types-source.html#type_space_ndet"><span class="tp"><span class="effect">ndet</span></span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>ndet: <span class="co">X</span></span></a> <a class="pp" href="std_time_duration-source.html#type_space_duration"><span class="tp">duration</span><span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>duration<span class="fslash last">/</span></span>duration: <span class="co">V</span></span></a>
  <a class="pp" href="std_time_duration-source.html#duration_2">duration<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>duration<span class="fslash last">/</span></span>duration<span class="postfix">.2</span>: <span class="tp sp">(</span><span class="tp tpp">secs</span> <span class="tp kw op">:</span> <span class="tp">float64</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">duration</span></span></a>(<a class="pp" href="#xnow_resolution">xnow-resolution<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>chrono<span class="fslash last">/</span></span>xnow<span class="dash">-</span>resolution: <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">ndet</span> <span class="tp">float64</span></span></a>()</span>)

<span class="decl-fun" id="xnow_resolution"><span class="kw">extern</span> <a class="pp" href="std_time_chrono.html#xnow_resolution">xnow-resolution<span class="pc"><span class="mo">std<span class="fslash">/</span>time<span class="fslash">/</span>chrono<span class="fslash last">/</span></span>xnow<span class="dash">-</span>resolution: <span class="tp sp">(</span><span class="tp sp">)</span> <span class="tp kw op">-></span> <span class="tp">ndet</span> <span class="tp">float64</span></span></a>() <span class="tp kw op">:</span> <a class="pp" href="std_core_types-source.html#type_space_ndet"><span class="tp"><span class="effect">ndet</span></span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>ndet: <span class="co">X</span></span></a> <a class="pp" href="std_core_types-source.html#type_space_float64"><span class="tp">float64</span><span class="pc"><span class="mo">std<span class="fslash">/</span>core<span class="fslash">/</span>types<span class="fslash last">/</span></span>float64: <span class="co">V</span></span></a>
  c <span class="st">"kk&#95;time&#95;dresolution"</span>
  cs <span class="st">"&#95;Chrono.NowResolution"</span>
  js <span class="st">"&#95;now&#95;resolution"</span></span>

</pre>

</body>
</html>
