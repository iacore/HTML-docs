package.path = "./?.lua"
local js = require("js")
do
  local noop
  local function _1_()
    return nil
  end
  noop = _1_
  package.loaded.ffi = {typeof = noop}
  _G.os = {getenv = noop}
  _G.io = {open = noop}
  local function _2_(_241, _242)
    return (_241 & _242)
  end
  local function _3_(_241, _242)
    return (_241 >> _242)
  end
  _G.bit = {band = _2_, rshift = _3_}
  _G.unpack = table.unpack
end
_G.print = function(...)
  return (js.global.console):log(...)
end
local document = js.global.document
local compile_fennel = document:getElementById("compile-fennel")
local compile_lua = document:getElementById("compile-lua")
local out = document:getElementById("out")
local fennel_source = document:getElementById("fennel-source")
local fennel_highlighted = document:getElementById("fennel-highlighted")
local lua_source = document:getElementById("lua-source")
local lua_highlighted = document:getElementById("lua-highlighted")
local function status(msg, success)
  out.innerHTML = msg
  out.style.color = ((success and "black") or "#dd1111")
  return nil
end
fennel_source.onkeydown = function(_, e)
  if not e then
    e = js.global.event
  else
  end
  if (((e.key or e.which) == "Enter") and e.ctrlKey) then
    compile_fennel.onclick()
    return false
  else
    return nil
  end
end
lua_source.onkeydown = function(_, e)
  if not e then
    e = js.global.event
  else
  end
  if (((e.key or e.which) == "Enter") and e.ctrlKey) then
    compile_lua.onclick()
    return false
  else
    return nil
  end
end
local fennel_syntax
package.preload["syntax-table"] = package.preload["syntax-table"] or function(...)
  return {["#"] = {["special?"] = true}, ["%"] = {["special?"] = true}, ["*"] = {["special?"] = true}, ["+"] = {["special?"] = true}, ["-"] = {["special?"] = true}, ["->"] = {["macro?"] = true}, ["->>"] = {["macro?"] = true}, ["-?>"] = {["macro?"] = true}, ["-?>>"] = {["macro?"] = true}, ["."] = {["special?"] = true}, [".."] = {["special?"] = true}, ["/"] = {["special?"] = true}, ["//"] = {["special?"] = true}, [":"] = {["special?"] = true}, ["<"] = {["special?"] = true}, ["<="] = {["special?"] = true}, ["="] = {["special?"] = true}, [">"] = {["special?"] = true}, [">="] = {["special?"] = true}, ["?."] = {["macro?"] = true}, ["^"] = {["special?"] = true}, _G = {["global?"] = true}, accumulate = {["binding-form?"] = true, ["body-form?"] = true, ["macro?"] = true}, ["and"] = {["special?"] = true}, arg = {["global?"] = true}, assert = {["function?"] = true, ["global?"] = true}, band = {["special?"] = true}, bit32 = {["global?"] = true}, ["bit32.arshift"] = {["function?"] = true, ["global?"] = true}, ["bit32.band"] = {["function?"] = true, ["global?"] = true}, ["bit32.bnot"] = {["function?"] = true, ["global?"] = true}, ["bit32.bor"] = {["function?"] = true, ["global?"] = true}, ["bit32.btest"] = {["function?"] = true, ["global?"] = true}, ["bit32.bxor"] = {["function?"] = true, ["global?"] = true}, ["bit32.extract"] = {["function?"] = true, ["global?"] = true}, ["bit32.lrotate"] = {["function?"] = true, ["global?"] = true}, ["bit32.lshift"] = {["function?"] = true, ["global?"] = true}, ["bit32.replace"] = {["function?"] = true, ["global?"] = true}, ["bit32.rrotate"] = {["function?"] = true, ["global?"] = true}, ["bit32.rshift"] = {["function?"] = true, ["global?"] = true}, bnot = {["special?"] = true}, bor = {["special?"] = true}, bxor = {["special?"] = true}, case = {["body-form?"] = true, ["macro?"] = true}, ["case-try"] = {["body-form?"] = true, ["macro?"] = true}, collect = {["binding-form?"] = true, ["body-form?"] = true, ["macro?"] = true}, collectgarbage = {["function?"] = true, ["global?"] = true}, comment = {["body-form?"] = true, ["special?"] = true}, coroutine = {["global?"] = true}, ["coroutine.create"] = {["function?"] = true, ["global?"] = true}, ["coroutine.isyieldable"] = {["function?"] = true, ["global?"] = true}, ["coroutine.resume"] = {["function?"] = true, ["global?"] = true}, ["coroutine.running"] = {["function?"] = true, ["global?"] = true}, ["coroutine.status"] = {["function?"] = true, ["global?"] = true}, ["coroutine.wrap"] = {["function?"] = true, ["global?"] = true}, ["coroutine.yield"] = {["function?"] = true, ["global?"] = true}, debug = {["global?"] = true}, ["debug.debug"] = {["function?"] = true, ["global?"] = true}, ["debug.gethook"] = {["function?"] = true, ["global?"] = true}, ["debug.getinfo"] = {["function?"] = true, ["global?"] = true}, ["debug.getlocal"] = {["function?"] = true, ["global?"] = true}, ["debug.getmetatable"] = {["function?"] = true, ["global?"] = true}, ["debug.getregistry"] = {["function?"] = true, ["global?"] = true}, ["debug.getupvalue"] = {["function?"] = true, ["global?"] = true}, ["debug.getuservalue"] = {["function?"] = true, ["global?"] = true}, ["debug.sethook"] = {["function?"] = true, ["global?"] = true}, ["debug.setlocal"] = {["function?"] = true, ["global?"] = true}, ["debug.setmetatable"] = {["function?"] = true, ["global?"] = true}, ["debug.setupvalue"] = {["function?"] = true, ["global?"] = true}, ["debug.setuservalue"] = {["function?"] = true, ["global?"] = true}, ["debug.traceback"] = {["function?"] = true, ["global?"] = true}, ["debug.upvalueid"] = {["function?"] = true, ["global?"] = true}, ["debug.upvaluejoin"] = {["function?"] = true, ["global?"] = true}, ["do"] = {["body-form?"] = true, ["special?"] = true}, dofile = {["function?"] = true, ["global?"] = true}, doto = {["body-form?"] = true, ["macro?"] = true}, each = {["binding-form?"] = true, ["body-form?"] = true, ["special?"] = true}, error = {["function?"] = true, ["global?"] = true}, ["eval-compiler"] = {["body-form?"] = true, ["special?"] = true}, faccumulate = {["binding-form?"] = true, ["body-form?"] = true, ["macro?"] = true}, fcollect = {["binding-form?"] = true, ["body-form?"] = true, ["macro?"] = true}, fn = {["body-form?"] = true, ["define?"] = true, ["special?"] = true}, ["for"] = {["binding-form?"] = true, ["body-form?"] = true, ["special?"] = true}, getmetatable = {["function?"] = true, ["global?"] = true}, global = {["define?"] = true, ["special?"] = true}, hashfn = {["special?"] = true}, icollect = {["binding-form?"] = true, ["body-form?"] = true, ["macro?"] = true}, ["if"] = {["special?"] = true}, ["import-macros"] = {["macro?"] = true}, include = {["special?"] = true}, io = {["global?"] = true}, ["io.close"] = {["function?"] = true, ["global?"] = true}, ["io.flush"] = {["function?"] = true, ["global?"] = true}, ["io.input"] = {["function?"] = true, ["global?"] = true}, ["io.lines"] = {["function?"] = true, ["global?"] = true}, ["io.open"] = {["function?"] = true, ["global?"] = true}, ["io.output"] = {["function?"] = true, ["global?"] = true}, ["io.popen"] = {["function?"] = true, ["global?"] = true}, ["io.read"] = {["function?"] = true, ["global?"] = true}, ["io.tmpfile"] = {["function?"] = true, ["global?"] = true}, ["io.type"] = {["function?"] = true, ["global?"] = true}, ["io.write"] = {["function?"] = true, ["global?"] = true}, ipairs = {["function?"] = true, ["global?"] = true}, lambda = {["body-form?"] = true, ["define?"] = true, ["macro?"] = true}, length = {["special?"] = true}, let = {["binding-form?"] = true, ["body-form?"] = true, ["special?"] = true}, load = {["function?"] = true, ["global?"] = true}, loadfile = {["function?"] = true, ["global?"] = true}, ["local"] = {["define?"] = true, ["special?"] = true}, lshift = {["special?"] = true}, lua = {["special?"] = true}, macro = {["body-form?"] = true, ["define?"] = true, ["macro?"] = true}, macrodebug = {["macro?"] = true}, macros = {["define?"] = true, ["special?"] = true}, match = {["body-form?"] = true, ["macro?"] = true}, ["match-try"] = {["body-form?"] = true, ["macro?"] = true}, math = {["global?"] = true}, ["math.abs"] = {["function?"] = true, ["global?"] = true}, ["math.acos"] = {["function?"] = true, ["global?"] = true}, ["math.asin"] = {["function?"] = true, ["global?"] = true}, ["math.atan"] = {["function?"] = true, ["global?"] = true}, ["math.atan2"] = {["function?"] = true, ["global?"] = true}, ["math.ceil"] = {["function?"] = true, ["global?"] = true}, ["math.cos"] = {["function?"] = true, ["global?"] = true}, ["math.cosh"] = {["function?"] = true, ["global?"] = true}, ["math.deg"] = {["function?"] = true, ["global?"] = true}, ["math.exp"] = {["function?"] = true, ["global?"] = true}, ["math.floor"] = {["function?"] = true, ["global?"] = true}, ["math.fmod"] = {["function?"] = true, ["global?"] = true}, ["math.frexp"] = {["function?"] = true, ["global?"] = true}, ["math.ldexp"] = {["function?"] = true, ["global?"] = true}, ["math.log"] = {["function?"] = true, ["global?"] = true}, ["math.log10"] = {["function?"] = true, ["global?"] = true}, ["math.max"] = {["function?"] = true, ["global?"] = true}, ["math.min"] = {["function?"] = true, ["global?"] = true}, ["math.modf"] = {["function?"] = true, ["global?"] = true}, ["math.pow"] = {["function?"] = true, ["global?"] = true}, ["math.rad"] = {["function?"] = true, ["global?"] = true}, ["math.random"] = {["function?"] = true, ["global?"] = true}, ["math.randomseed"] = {["function?"] = true, ["global?"] = true}, ["math.sin"] = {["function?"] = true, ["global?"] = true}, ["math.sinh"] = {["function?"] = true, ["global?"] = true}, ["math.sqrt"] = {["function?"] = true, ["global?"] = true}, ["math.tan"] = {["function?"] = true, ["global?"] = true}, ["math.tanh"] = {["function?"] = true, ["global?"] = true}, ["math.tointeger"] = {["function?"] = true, ["global?"] = true}, ["math.type"] = {["function?"] = true, ["global?"] = true}, ["math.ult"] = {["function?"] = true, ["global?"] = true}, next = {["function?"] = true, ["global?"] = true}, ["not"] = {["special?"] = true}, ["not="] = {["special?"] = true}, ["or"] = {["special?"] = true}, os = {["global?"] = true}, ["os.clock"] = {["function?"] = true, ["global?"] = true}, ["os.date"] = {["function?"] = true, ["global?"] = true}, ["os.difftime"] = {["function?"] = true, ["global?"] = true}, ["os.execute"] = {["function?"] = true, ["global?"] = true}, ["os.exit"] = {["function?"] = true, ["global?"] = true}, ["os.getenv"] = {["function?"] = true, ["global?"] = true}, ["os.remove"] = {["function?"] = true, ["global?"] = true}, ["os.rename"] = {["function?"] = true, ["global?"] = true}, ["os.setlocale"] = {["function?"] = true, ["global?"] = true}, ["os.time"] = {["function?"] = true, ["global?"] = true}, ["os.tmpname"] = {["function?"] = true, ["global?"] = true}, package = {["global?"] = true}, ["package.loadlib"] = {["function?"] = true, ["global?"] = true}, ["package.searchpath"] = {["function?"] = true, ["global?"] = true}, pairs = {["function?"] = true, ["global?"] = true}, partial = {["macro?"] = true}, pcall = {["function?"] = true, ["global?"] = true}, ["pick-args"] = {["macro?"] = true}, ["pick-values"] = {["macro?"] = true}, print = {["function?"] = true, ["global?"] = true}, quote = {["special?"] = true}, rawequal = {["function?"] = true, ["global?"] = true}, rawget = {["function?"] = true, ["global?"] = true}, rawlen = {["function?"] = true, ["global?"] = true}, rawset = {["function?"] = true, ["global?"] = true}, require = {["function?"] = true, ["global?"] = true}, ["require-macros"] = {["special?"] = true}, rshift = {["special?"] = true}, select = {["function?"] = true, ["global?"] = true}, set = {["special?"] = true}, ["set-forcibly!"] = {["special?"] = true}, setmetatable = {["function?"] = true, ["global?"] = true}, string = {["global?"] = true}, ["string.byte"] = {["function?"] = true, ["global?"] = true}, ["string.char"] = {["function?"] = true, ["global?"] = true}, ["string.dump"] = {["function?"] = true, ["global?"] = true}, ["string.find"] = {["function?"] = true, ["global?"] = true}, ["string.format"] = {["function?"] = true, ["global?"] = true}, ["string.gmatch"] = {["function?"] = true, ["global?"] = true}, ["string.gsub"] = {["function?"] = true, ["global?"] = true}, ["string.len"] = {["function?"] = true, ["global?"] = true}, ["string.lower"] = {["function?"] = true, ["global?"] = true}, ["string.match"] = {["function?"] = true, ["global?"] = true}, ["string.pack"] = {["function?"] = true, ["global?"] = true}, ["string.packsize"] = {["function?"] = true, ["global?"] = true}, ["string.rep"] = {["function?"] = true, ["global?"] = true}, ["string.reverse"] = {["function?"] = true, ["global?"] = true}, ["string.sub"] = {["function?"] = true, ["global?"] = true}, ["string.unpack"] = {["function?"] = true, ["global?"] = true}, ["string.upper"] = {["function?"] = true, ["global?"] = true}, table = {["global?"] = true}, ["table.concat"] = {["function?"] = true, ["global?"] = true}, ["table.insert"] = {["function?"] = true, ["global?"] = true}, ["table.move"] = {["function?"] = true, ["global?"] = true}, ["table.pack"] = {["function?"] = true, ["global?"] = true}, ["table.remove"] = {["function?"] = true, ["global?"] = true}, ["table.sort"] = {["function?"] = true, ["global?"] = true}, ["table.unpack"] = {["function?"] = true, ["global?"] = true}, tonumber = {["function?"] = true, ["global?"] = true}, tostring = {["function?"] = true, ["global?"] = true}, tset = {["special?"] = true}, type = {["function?"] = true, ["global?"] = true}, utf8 = {["global?"] = true}, ["utf8.char"] = {["function?"] = true, ["global?"] = true}, ["utf8.codepoint"] = {["function?"] = true, ["global?"] = true}, ["utf8.codes"] = {["function?"] = true, ["global?"] = true}, ["utf8.len"] = {["function?"] = true, ["global?"] = true}, ["utf8.offset"] = {["function?"] = true, ["global?"] = true}, values = {["special?"] = true}, var = {["define?"] = true, ["special?"] = true}, when = {["body-form?"] = true, ["macro?"] = true}, ["while"] = {["body-form?"] = true, ["special?"] = true}, ["with-open"] = {["binding-form?"] = true, ["body-form?"] = true, ["macro?"] = true}, xpcall = {["function?"] = true, ["global?"] = true}, ["~="] = {["special?"] = true}, ["\206\187"] = {["body-form?"] = true, ["define?"] = true, ["macro?"] = true}}
end
fennel_syntax = require("syntax-table")
package.preload["fennel-highlight"] = package.preload["fennel-highlight"] or function(...)
  local function scan(source, rules)
    local current_index = 1
    local last_matching_at = 0
    local function at_eof_3f()
      return (current_index > string.len(source))
    end
    local function advance(n)
      current_index = (current_index + n)
      return nil
    end
    local function yield_nonmatching()
      if (current_index > (1 + last_matching_at)) then
        local nonmatching = string.sub(source, (last_matching_at + 1), (current_index - 1))
        return coroutine.yield("nonmatching", nonmatching)
      else
        return nil
      end
    end
    local function yield_and_advance(class, matched)
      yield_nonmatching()
      coroutine.yield(class, matched)
      advance(string.len(matched))
      last_matching_at = (current_index - 1)
      return nil
    end
    local function test_rules(rules0)
      local matching_rule = nil
      for _, _9_ in ipairs(rules0) do
        local _each_10_ = _9_
        local class = _each_10_[1]
        local pattern = _each_10_[2]
        if matching_rule then break end
        local _11_ = string.match(source, pattern, current_index)
        if (nil ~= _11_) then
          local str = _11_
          matching_rule = {class, str}
        else
          matching_rule = nil
        end
      end
      return matching_rule
    end
    while not at_eof_3f() do
      local _13_ = test_rules(rules)
      if ((_G.type(_13_) == "table") and (nil ~= (_13_)[1]) and (nil ~= (_13_)[2])) then
        local class = (_13_)[1]
        local str = (_13_)[2]
        yield_and_advance(class, str)
      elseif true then
        local _ = _13_
        advance(1)
      else
      end
    end
    return yield_nonmatching()
  end
  local function __3ehtml(syntax, rules, source)
    local tbl_17_auto = {}
    local i_18_auto = #tbl_17_auto
    local function _15_()
      return scan(source, rules)
    end
    for class, value in coroutine.wrap(_15_) do
      local val_19_auto
      if ("nonmatching" == class) then
        val_19_auto = value
      else
        local extra
        if ("symbol" == class) then
          local _16_ = syntax[value]
          if ((_G.type(_16_) == "table") and ((_16_)["special?"] == true)) then
            extra = "special"
          elseif ((_G.type(_16_) == "table") and ((_16_)["macro?"] == true)) then
            extra = "macro"
          elseif ((_G.type(_16_) == "table") and ((_16_)["global?"] == true)) then
            extra = "global"
          else
            extra = nil
          end
        else
          extra = nil
        end
        local extended_class
        if extra then
          extended_class = (class .. " " .. extra)
        else
          extended_class = class
        end
        local escaped_value = string.gsub(string.gsub(value, "&", "&amp;"), "<", "&lt;")
        val_19_auto = string.format("<span class=\"%s\">%s</span>", extended_class, escaped_value)
      end
      if (nil ~= val_19_auto) then
        i_18_auto = (i_18_auto + 1)
        do end (tbl_17_auto)[i_18_auto] = val_19_auto
      else
      end
    end
    return tbl_17_auto
  end
  local fennel_rules
  do
    local symbol_char = "!%$&#%*%+%-%./:<=>%?%^_a-zA-Z0-9"
    fennel_rules = {{"comment", "^(;[^\n]*[\n])"}, {"string", "^(\"\")"}, {"string", "^(\".-[^\\]\")"}, {"keyword", ("^(:[" .. symbol_char .. "]+)")}, {"number", "^([%+%-]?%d+[xX]?%d*%.?%d?)"}, {"nil", ("^(nil)[^" .. symbol_char .. "]")}, {"boolean", ("^(true)[^" .. symbol_char .. "]")}, {"boolean", ("^(false)[^" .. symbol_char .. "]")}, {"symbol", ("^([" .. symbol_char .. "]+)")}}
  end
  local function fennel__3ehtml(syntax, source)
    return __3ehtml(syntax, fennel_rules, source)
  end
  local lua_keywords = {"and", "break", "do", "else", "elseif", "end", "for", "function", "goto", "if", "in", "local", "not", "or", "repeat", "return", "then", "until", "while"}
  local function fennel_syntax__3elua_syntax(fennel_syntax)
    local lua_syntax = {}
    do
      local tbl_14_auto = lua_syntax
      for k, v in pairs(fennel_syntax) do
        local k_15_auto, v_16_auto = nil, nil
        if (v["global?"] or v["special?"]) then
          k_15_auto, v_16_auto = k, v
        else
          k_15_auto, v_16_auto = nil
        end
        if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
          tbl_14_auto[k_15_auto] = v_16_auto
        else
        end
      end
    end
    do
      local tbl_14_auto = lua_syntax
      for _, k in ipairs(lua_keywords) do
        local k_15_auto, v_16_auto = k, {["special?"] = true}
        if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
          tbl_14_auto[k_15_auto] = v_16_auto
        else
        end
      end
    end
    return lua_syntax
  end
  local lua_rules
  do
    local symbol_char_first = "a-zA-Z_"
    local symbol_char_rest = (symbol_char_first .. "0-9%.")
    lua_rules = {{"comment", "^(%-%-%[===%[.-]===])"}, {"comment", "^(%-%-%[==%[.-]==])"}, {"comment", "^(%-%-%[=%[.-]=])"}, {"comment", "^(%-%-%[%[.-]])"}, {"comment", "^(%-%-[^\n]*[\n])"}, {"string", "^(\"\")"}, {"string", "^(%[%[.-]])"}, {"string", "^(\".-[^\\]\")"}, {"string", "^('.-[^\\]')"}, {"number", "^(0[xX][0-9a-fA-F]?%.[0-9a-fA-F]+[eE][+-]?[0-9]?)"}, {"number", "^(0[xX][0-9a-fA-F]+[eE][+-]?[0-9]?)"}, {"number", "^(0[xX][0-9a-fA-F]?%.[0-9a-fA-F]+)"}, {"number", "^(0[xX][0-9a-fA-F]+)"}, {"number", "^([0-9]?%.[0-9]+[eE][+-]?[0-9]?)"}, {"number", "^([0-9]+[eE][+-]?[0-9]?)"}, {"number", "^([0-9]?%.[0-9]+)"}, {"number", "^([0-9]+)"}, {"nil", ("^(nil)[^" .. symbol_char_rest .. "]")}, {"boolean", ("^(true)[^" .. symbol_char_rest .. "]")}, {"boolean", ("^(false)[^" .. symbol_char_rest .. "]")}, {"symbol", ("^([" .. symbol_char_first .. "][" .. symbol_char_rest .. "]*)")}, {"symbol", "^(<=)"}, {"symbol", "^(>=)"}, {"symbol", "^(==)"}, {"symbol", "^(~=)"}, {"symbol", "^(%.%.)"}, {"symbol", "^([%+%-%*/%^<>=#])"}}
  end
  local function lua__3ehtml(syntax, source)
    return __3ehtml(fennel_syntax__3elua_syntax(syntax), lua_rules, source)
  end
  return {["fennel->html"] = fennel__3ehtml, fennel_to_html = fennel__3ehtml, ["lua->html"] = lua__3ehtml, lua_to_html = lua__3ehtml}
end
local function highlight_fennel()
  local highlight = require("fennel-highlight")
  local highlighted_source_tags = highlight["fennel->html"](fennel_syntax, fennel_source.value)
  local highlighted_source = table.concat(highlighted_source_tags)
  fennel_highlighted.innerHTML = highlighted_source
  return nil
end
local lua_syntax
do
  local tbl_14_auto = {}
  for k, v in pairs(fennel_syntax) do
    local k_15_auto, v_16_auto = nil, nil
    if v["global?"] then
      k_15_auto, v_16_auto = k, v
    else
      k_15_auto, v_16_auto = nil
    end
    if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
      tbl_14_auto[k_15_auto] = v_16_auto
    else
    end
  end
  lua_syntax = tbl_14_auto
end
for _, k in ipairs({"and", "break", "do", "else", "elseif", "end", "false", "for", "function", "if", "in", "local", "nil", "not", "or", "repeat", "return", "then", "true", "until", "while", "goto"}) do
  lua_syntax[k] = {["special?"] = true}
end
local function highlight_lua()
  local highlight = require("fennel-highlight")
  local highlighted_source_tags = highlight["lua->html"](fennel_syntax, lua_source.value)
  local highlighted_source = table.concat(highlighted_source_tags)
  lua_highlighted.innerHTML = highlighted_source
  return nil
end
highlight_fennel()
highlight_lua()
fennel_source.onscroll = function()
  fennel_highlighted.scrollTop = fennel_source.scrollTop
  fennel_highlighted.scrollLeft = fennel_source.scrollLeft
  return nil
end
lua_source.onscroll = function()
  lua_highlighted.scrollTop = lua_source.scrollTop
  lua_highlighted.scrollLeft = lua_source.scrollLeft
  return nil
end
fennel_source.oninput = function()
  highlight_fennel()
  return fennel_source.onscroll()
end
lua_source.oninput = function()
  highlight_lua()
  return lua_source.onscroll()
end
local function update_fennel_source(new_value)
  fennel_source.value = new_value
  return highlight_fennel()
end
local function update_lua_source(new_value)
  lua_source.value = new_value
  return highlight_lua()
end
local anti_msg = ("Compiled Lua to Fennel.\n\n" .. "Note that compiling Lua to Fennel can result in some" .. " strange-looking code when\nusing constructs that Fennel" .. " does not support natively, like early returns.")
local function init_worker(auto_click)
  local worker = js.new(js.global.Worker, "/see-worker.js")
  local function send(fennel_3f, code)
    local prefix = ((fennel_3f and " ") or "\9")
    return worker:postMessage((prefix .. code))
  end
  worker.onmessage = function(_, e)
    out.innerHTML = e.data
    compile_fennel.onclick = function()
      return send(true, fennel_source.value)
    end
    compile_lua.onclick = function()
      return send(false, lua_source.value)
    end
    worker.onmessage = function(_0, event)
      if (event.data):match(" $") then
        update_lua_source(event.data)
        return status("Compiled Fennel to Lua.", true)
      elseif (event.data):match("\9$") then
        update_fennel_source(event.data)
        return status(anti_msg, true)
      else
        return status(event.data, false)
      end
    end
    if (auto_click and auto_click.onclick) then
      return auto_click.onclick()
    else
      return nil
    end
  end
  return worker.onmessage
end
local function load_direct(auto_click)
  local antifennel = dofile("antifennel.lua")
  local fennel = require("fennel")
  compile_fennel.onclick = function()
    local ok, code = pcall(fennel.compileString, fennel_source.value)
    if ok then
      update_lua_source(code)
      return status("Compiled Fennel to Lua.", true)
    else
      return status(tostring(code), false)
    end
  end
  compile_lua.onclick = function()
    local _30_, _31_ = load(lua_source.value)
    if ((_30_ == nil) and (nil ~= _31_)) then
      local msg = _31_
      return status(("Lua: " .. msg), false)
    elseif true then
      local _ = _30_
      local _32_, _33_ = pcall(antifennel, lua_source.value)
      if ((_32_ == true) and (nil ~= _33_)) then
        local code = _33_
        update_fennel_source(code)
        return status(anti_msg, true)
      elseif (true and (nil ~= _33_)) then
        local _0 = _32_
        local msg = _33_
        return status(tostring(msg), false)
      else
        return nil
      end
    else
      return nil
    end
  end
  out.innerHTML = ("Loaded Fennel " .. fennel.version .. " in " .. _VERSION)
  if (auto_click and auto_click.onclick) then
    return auto_click.onclick()
  else
    return nil
  end
end
local started = false
local function init(auto_click)
  if not started then
    started = true
    out.innerHTML = "Loading..."
    if js.global.Worker then
      return init_worker(auto_click)
    elseif js.global.setTimeout then
      local function _37_()
        return load_direct(auto_click)
      end
      return (js.global):setTimeout(_37_)
    else
      return load_direct(auto_click)
    end
  else
    return nil
  end
end
compile_fennel.onclick = init
compile_lua.onclick = init
fennel_source.onfocus = init
lua_source.onfocus = init
local fennel_samples = {fibonacci = "(fn fibonacci [n]\n (if (< n 2)\n  n\n  (+ (fibonacci (- n 1)) (fibonacci (- n 2)))))\n\n(print (fibonacci 10))", ["pong movement"] = ";; Read the keyboard, move player accordingly\n(local dirs {:up [0 -1] :down [0 1]\n            :left [-1 0] :right [1 0]})\n\n(each [key delta (pairs dirs)]\n  (when (love.keyboard.isDown key)\n    (let [[dx dy] delta\n          [px py] player\n          x (+ px (* dx player.speed dt))\n          y (+ py (* dy player.speed dt))]\n      (world:move player x y))))", walk = "(fn walk-tree [root f custom-iterator]\n  (fn walk [iterfn parent idx node]\n    (when (f idx node parent)\n      (each [k v (iterfn node)]\n        (walk iterfn node k v))))\n  (walk (or custom-iterator pairs) nil nil root)\n  root)"}
local lua_samples = {antifennel = "local function uncamelize(name)\n   local function splicedash(pre, cap)\n     return pre .. \"-\" .. cap:lower()\n   end\n   return name:gsub(\"([a-z0-9])([A-Z])\", splicedash)\nend\n\nlocal function mangle(name, field)\n   if not field and reservedFennel[name] then\n     name = \"___\" .. name .. \"___\"\n   end\n   return field and name or\n      uncamelize(name):gsub(\"([a-z0-9])_\", \"%1-\")\nend\n\nlocal function compile(rdr, filename)\n   local ls = lex_setup(rdr, filename)\n   local ast_builder = lua_ast.New(mangle)\n   local ast_tree = parse(ast_builder, ls)\n   return letter(compiler(nil, ast_tree))\nend", ["love.run"] = "function love.run()\n   love.load()\n   while true do\n      love.event.pump()\n      local needs_refresh = false\n      for name, a,b,c,d,e,f in love.event.poll() do\n         if(type(love[name]) == \"function\") then\n            love[name](a,b,c,d,e,f)\n            needs_refresh = true\n         elseif(name == \"quit\") then\n            os.exit()\n         end\n      end\n      for _,c in pairs(internal.coroutines) do\n         local ok, val = coroutine.resume(c)\n         if(ok and val) then needs_refresh = true\n         elseif(not ok) then print(val) end\n      end\n      for i,c in lume.ripairs(internal.coroutines) do\n         if(coroutine.status(c) == \"dead\") then\n            table.remove(internal.coroutines, i)\n         end\n      end\n      if(needs_refresh) then refresh() end\n      love.timer.sleep(0.05)\n   end\nend", ["sample select"] = "local sample_lua = document:getElementById(\"samples\")\nlocal lua_samples = {}\n\nfor name, sample in pairs(lua_samples) do\n   local option = document:createElement(\"option\")\n   option.innerHTML = name\n   sample_lua:appendChild(option)\nend\n\nfunction sample_lua.onchange(self, e)\n   init()\n   local code = lua_samples[self.value]\n   if code then lua_source.value = code end\nend"}
local function init_samples(id, samples, update_source)
  local select = document:getElementById(id)
  for name, sample in pairs(samples) do
    local option = document:createElement("option")
    option.innerHTML = name
    select:appendChild(option)
  end
  select.onchange = function(self, e)
    init()
    local _40_ = samples[self.value]
    if (nil ~= _40_) then
      local code = _40_
      return update_source(code)
    else
      return nil
    end
  end
  return select.onchange
end
init_samples("sample-fennel", fennel_samples, update_fennel_source)
init_samples("sample-lua", lua_samples, update_lua_source)
if js.global.URLSearchParams then
  local params = js.new(js.global.URLSearchParams, document.location.search)
  local fennel_param = params:get("fennel")
  local lua_param = params:get("lua")
  if (tostring(fennel_param) ~= "null") then
    update_fennel_source(fennel_param)
    return init(compile_fennel)
  elseif (tostring(lua_param) ~= "null") then
    update_lua_source(lua_param)
    return init(compile_lua)
  else
    return nil
  end
else
  return nil
end
