<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>Fennel – Macro Guide</title>
  <style type="text/css">
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
  </style>
  <style type="text/css">
a.sourceLine { display: inline-block; line-height: 1.25; }
a.sourceLine { pointer-events: none; color: inherit; text-decoration: inherit; }
a.sourceLine:empty { height: 1.2em; }
.sourceCode { overflow: visible; }
code.sourceCode { white-space: pre; position: relative; }
div.sourceCode { margin: 1em 0; }
pre.sourceCode { margin: 0; }
@media screen {
div.sourceCode { overflow: auto; }
}
@media print {
code.sourceCode { white-space: pre-wrap; }
a.sourceLine { text-indent: -1em; padding-left: 1em; }
}
pre.numberSource a.sourceLine
  { position: relative; left: -4em; }
pre.numberSource a.sourceLine::before
  { content: attr(title);
    position: relative; left: -1em; text-align: right; vertical-align: baseline;
    border: none; pointer-events: all; display: inline-block;
    -webkit-touch-callout: none; -webkit-user-select: none;
    -khtml-user-select: none; -moz-user-select: none;
    -ms-user-select: none; user-select: none;
    padding: 0 4px; width: 4em;
    color: #aaaaaa;
  }
pre.numberSource { margin-left: 3em; border-left: 1px solid #aaaaaa;  padding-left: 4px; }
div.sourceCode
  {  }
@media screen {
a.sourceLine::before { text-decoration: underline; }
}
code span.al { color: #ff0000; font-weight: bold; } /* Alert */
code span.an { color: #60a0b0; font-weight: bold; font-style: italic; } /* Annotation */
code span.at { color: #7d9029; } /* Attribute */
code span.bn { color: #40a070; } /* BaseN */
code span.bu { } /* BuiltIn */
code span.cf { color: #007020; font-weight: bold; } /* ControlFlow */
code span.ch { color: #4070a0; } /* Char */
code span.cn { color: #880000; } /* Constant */
code span.co { color: #60a0b0; font-style: italic; } /* Comment */
code span.cv { color: #60a0b0; font-weight: bold; font-style: italic; } /* CommentVar */
code span.do { color: #ba2121; font-style: italic; } /* Documentation */
code span.dt { color: #902000; } /* DataType */
code span.dv { color: #40a070; } /* DecVal */
code span.er { color: #ff0000; font-weight: bold; } /* Error */
code span.ex { } /* Extension */
code span.fl { color: #40a070; } /* Float */
code span.fu { color: #06287e; } /* Function */
code span.im { } /* Import */
code span.in { color: #60a0b0; font-weight: bold; font-style: italic; } /* Information */
code span.kw { color: #007020; font-weight: bold; } /* Keyword */
code span.op { color: #666666; } /* Operator */
code span.ot { color: #007020; } /* Other */
code span.pp { color: #bc7a00; } /* Preprocessor */
code span.sc { color: #4070a0; } /* SpecialChar */
code span.ss { color: #bb6688; } /* SpecialString */
code span.st { color: #4070a0; } /* String */
code span.va { color: #19177c; } /* Variable */
code span.vs { color: #4070a0; } /* VerbatimString */
code span.wa { color: #60a0b0; font-weight: bold; font-style: italic; } /* Warning */
  </style>
      <link rel="stylesheet" href="fennel.css"></link>
      <link rel="stylesheet" href="https://code.cdn.mozilla.net/fonts/fira.css"/>
      <script type="text/javascript" src="fengari-web.js"></script>
      <script type="application/lua" src="init.lua" async></script>
      <title>the Fennel programming language</title>
      <style type="text/css">
        pre {
        border-radius: 3px;
        border: 1px solid #ddd;
        padding: 0.7em;
        }
      </style>
</head>
<body>
<header>
<h1 class="title">Macro Guide</h1>
</header>
<nav id="TOC">
<ul>
<li><a href="#code-is-data">Code is data</a></li>
<li><a href="#how-to-manipulate-data">How to Manipulate Data</a></li>
<li><a href="#details">Details</a><ul>
<li><a href="#quoting">Quoting</a></li>
<li><a href="#macrodebug">macrodebug</a></li>
<li><a href="#identifiers-and-gensym">Identifiers and Gensym</a></li>
<li><a href="#identifier-arguments">Identifier arguments</a></li>
<li><a href="#ast-quirks">AST quirks</a></li>
<li><a href="#using-functions-from-a-module">Using functions from a module</a></li>
<li><a href="#macro-modules">Macro Modules</a></li>
<li><a href="#assert-compile">assert-compile</a></li>
</ul></li>
<li><a href="#thats-all">That's all!</a></li>
</ul>
</nav>
<p>Macros are how lisps accomplish metaprogramming. You'll see a lot of people treat lisp macros with a kind of mystical reverence. &quot;Macros allow you to do things that aren't possible in other languages&quot; is a common refrain, but that's nonsense. (<a href="https://en.wikipedia.org/wiki/Turing_tarpit">Turing tarpits</a> exist; other languages have macros.) The difference is that lisps allow you to write programs using the same notation you use for data structures. It's not that these things are impossible in other languages; it's just that the lisp way flows seamlessly instead of feeling arcane.</p>
<p>There are really only three things you need to understand in order to write effective macros:</p>
<ul>
<li>Code is Data</li>
<li>How to Manipulate Data</li>
<li>Details</li>
</ul>
<h2 id="code-is-data">Code is data</h2>
<p>In some sense &quot;code is data&quot; seems like a tautology. Code lives in files on disk; files contain data. If you want to use that data, you load up a parser and you can operate on it like you would any other file format. Almost every language works this way; that's just the basics of compilers. In many languages, the compiler feels distant and hallowed. You submit your code, and you are granted back an executable etched in a stone tablet (or perhaps the wrath of the compiler if you made a mistake). The data it used to produce that executable is not accessible to the lowly program.</p>
<p>But it doesn't have to be this way. Using a compiler can feel more like having a conversation where you go back and forth, and macros can blur the line between the compiler and the program. In the case of Fennel, that means you can get the program's own data as a table.</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb1-1" title="1">(<span class="kw">when</span> message.to</a>
<a class="sourceLine" id="cb1-2" title="2">  (deliver message :immediately))</a></code></pre></div>
<p>According to the compiler, this is a table of three elements: the symbol <code>when</code>, the symbol <code>message.to</code>, and the table <code>(deliver message :immediately)</code> which itself consists of two symbols and a string. A symbol is a bare token which represents an identifier. Let's look at a more complex example:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb2-1" title="1">(<span class="kw">let</span> [[a b c &amp; rest] (get-elements)</a>
<a class="sourceLine" id="cb2-2" title="2">      {:input input :output output} (pipe-for a <span class="dv">22</span>)]</a>
<a class="sourceLine" id="cb2-3" title="3">  (route input b c output (<span class="kw">table</span>.unpack rest)))</a></code></pre></div>
<p>There's a lot more going on here, but it's still just a table of three elements. Again we start with a symbol (<code>let</code>) but in this case the second argument is a sequential table. Square brackets in Fennel code tend to indicate that new locals are being introduced, but they are just normal sequential tables to the parser. When destructuring you can see that both sequential tables and key/value tables can be used, for the return value of <code>get-elements</code> and <code>pipe-for</code> respectively.</p>
<h2 id="how-to-manipulate-data">How to Manipulate Data</h2>
<p>Manipulating data is just regular programming! If you know Fennel, you know how to write a function which takes a table and returns a table that looks a little different. Cool!</p>
<p>Once you understand that code is data and data is (mostly) tables, that means all the skills you have from writing regular Fennel programs can apply to macros. You want to use <code>table.remove</code> on a list that represents code? Go for it. You can use <code>each</code>/<code>ipairs</code> to step thru the contents of the lists. Destructuring and even pattern matching work as you'd expect. A list is just a special kind of table (with a metatable) which prints with parens instead of square brackets.</p>
<h2 id="details">Details</h2>
<p>That's all! Everything else is details.</p>
<p>OK, so ... that's a slight exaggeration. There are a few things you still need to understand. Let's start with the tiniest of examples just to kick things off:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb3-1" title="1">(<span class="ex">macro</span><span class="fu"> postfix3 </span>[[x1 x2 x3]]</a>
<a class="sourceLine" id="cb3-2" title="2">  (list x3 x2 x1))</a>
<a class="sourceLine" id="cb3-3" title="3"></a>
<a class="sourceLine" id="cb3-4" title="4">(postfix3 (<span class="st">&quot;world&quot;</span> <span class="st">&quot;hello&quot;</span> <span class="kw">print</span>)) <span class="co">; -&gt; becomes (print &quot;hello&quot; &quot;world&quot;)</span></a></code></pre></div>
<p>This is one of the simplest macros possible. It takes a list of three elements and returns a list with the elements in the opposite order. From this example you can see that macros look like functions which take arguments. But you can't write a function that takes <code>(&quot;world&quot; &quot;hello&quot; print)</code> as an argument! What's going on here?</p>
<p>Argument to macros get passed <em>before</em> they get evaluated. If you tried to evaluate <code>(&quot;world&quot; &quot;hello&quot; print)</code> it would fail trying to call <code>&quot;world&quot;</code> as a function. But the macro operates at compile time before that.</p>
<p>Let's walk thru a bigger example.</p>
<p>The first step when you want to write a macro is to identify the transformation you want to perform on the code. Let's take a look at what it might look like to write a <code>thrice-if</code> macro which takes a condition and a form, and runs the form three times, each time first checking that the condition is still true.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb4-1" title="1">(thrice-if (ready-to-go?)</a>
<a class="sourceLine" id="cb4-2" title="2">           (make-it-so!))</a></code></pre></div>
<p>We want this to result in the following code:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb5-1" title="1">(<span class="kw">if</span> (ready-to-go?)</a>
<a class="sourceLine" id="cb5-2" title="2">    (<span class="kw">do</span> (make-it-so!)</a>
<a class="sourceLine" id="cb5-3" title="3">        (<span class="kw">if</span> (ready-to-go?)</a>
<a class="sourceLine" id="cb5-4" title="4">            (<span class="kw">do</span> (make-it-so!)</a>
<a class="sourceLine" id="cb5-5" title="5">                (<span class="kw">if</span> (ready-to-go?)</a>
<a class="sourceLine" id="cb5-6" title="6">                    (<span class="kw">do</span> (make-it-so!)))))))</a></code></pre></div>
<p>So maybe we don't yet know how to write this macro. But stop for a minute and imagine if this were not a macro but a function which takes a normal square-bracket table and uses strings instead of symbols. It would look like this when run:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb6-1" title="1">(thrice-if* [:ready-to-go?]</a>
<a class="sourceLine" id="cb6-2" title="2">            [:make-it-so!])</a>
<a class="sourceLine" id="cb6-3" title="3"></a>
<a class="sourceLine" id="cb6-4" title="4"><span class="co">;; [:if [:ready-to-go?]</span></a>
<a class="sourceLine" id="cb6-5" title="5"><span class="co">;;       [:do [:make-it-so!]</span></a>
<a class="sourceLine" id="cb6-6" title="6"><span class="co">;;            [:if [:ready-to-go?]</span></a>
<a class="sourceLine" id="cb6-7" title="7"><span class="co">;;                 [:do [:make-it-so!] </span></a>
<a class="sourceLine" id="cb6-8" title="8"><span class="co">;;                      [:if [:ready-to-go?]</span></a>
<a class="sourceLine" id="cb6-9" title="9"><span class="co">;;                           [:do [:make-it-so!]]]]]]]</span></a></code></pre></div>
<p>When the problem is framed this way, it's easy to imagine how such a function might work. This one uses recursion, but you could implement it with a loop if you prefer; that's not important. The important thing is: tables go in, and a table comes out.</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb7-1" title="1">(<span class="ex">fn</span><span class="fu"> thrice-if* </span>[condition result]</a>
<a class="sourceLine" id="cb7-2" title="2">  (<span class="ex">fn</span><span class="fu"> step </span>[i]</a>
<a class="sourceLine" id="cb7-3" title="3">    (<span class="kw">if</span> (<span class="op">&lt;</span> <span class="dv">0</span> i)</a>
<a class="sourceLine" id="cb7-4" title="4">        [:if condition [:do result (step (<span class="op">-</span> i <span class="dv">1</span>))]]))</a>
<a class="sourceLine" id="cb7-5" title="5">  (step <span class="dv">3</span>))</a></code></pre></div>
<p>Now that we have this function, what does it take to turn it into a macro?</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb8-1" title="1">(<span class="ex">macro</span><span class="fu"> thrice-if </span>[condition result]</a>
<a class="sourceLine" id="cb8-2" title="2">  (<span class="ex">fn</span><span class="fu"> step </span>[i]</a>
<a class="sourceLine" id="cb8-3" title="3">    (<span class="kw">if</span> (<span class="op">&lt;</span> <span class="dv">0</span> i)</a>
<a class="sourceLine" id="cb8-4" title="4">        (list (sym :if) condition (list (sym :do) result (step (<span class="op">-</span> i <span class="dv">1</span>))))))</a>
<a class="sourceLine" id="cb8-5" title="5">  (step <span class="dv">3</span>))</a></code></pre></div>
<p>Instead of using <code>[]</code> tables we call the <code>list</code> function, and instead of strings we call the <code>sym</code> function. Easy!</p>
<p>Both these functions are only available inside macros because of the compiler environment. We'll get back to that later.</p>
<p><strong>Note</strong>: It is very common to get to step one, write out the expansion you want your macro to return, and then realize you could probably do it with a function. If you can, then great! You'll save yourself some headache. Macros can tidy up repetitive code, but they do introduce conceptual overhead, so be sure to weigh the pros and cons before diving in. Of course, it takes time and experience to learn how to judge this.</p>
<h3 id="quoting">Quoting</h3>
<p>The notation above is easy to understand, but it's not as concise as it could be. We have a trick that lets us tidy up those verbose calls to <code>list</code> and <code>sym</code>: quoting.</p>
<p>The backtick character can be thought of as creating a template of a list which you can then selectively interpolate values using a comma character to unquote. This is similar to how string interpolation works in languages like Ruby.</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb9-1" title="1">(<span class="ex">macro</span><span class="fu"> thrice-if </span>[condition result]</a>
<a class="sourceLine" id="cb9-2" title="2">  (<span class="ex">fn</span><span class="fu"> step </span>[i]</a>
<a class="sourceLine" id="cb9-3" title="3">    (<span class="kw">if</span> (<span class="op">&lt;</span> <span class="dv">0</span> i)</a>
<a class="sourceLine" id="cb9-4" title="4">        `(<span class="kw">if</span> ,condition (<span class="kw">do</span> ,result ,(step (<span class="op">-</span> i <span class="dv">1</span>))))))</a>
<a class="sourceLine" id="cb9-5" title="5">  (step <span class="dv">3</span>))</a></code></pre></div>
<p>Symbols inside a quoted form remain as symbols. Symbols in an unquoted form (like <code>,condition</code> and <code>,result</code> above) are <strong>evaluated</strong> meaning they are replaced with whatever value they have in the code at that point.</p>
<p>Unquoting doesn't just apply to symbols; you can unquote lists too: <code>,(step (- i 1))</code> above does that. A quoted list remains a list, but an unquoted list behaves like it does in normal code: it calls a function. The return value of the function is placed into the quoted list.</p>
<p>Quote and unquote are merely tools of notation. There is no difference in the meaning between this version and the first version which calls <code>list</code> and <code>sym</code> explicitly.</p>
<h3 id="macrodebug">macrodebug</h3>
<p>Quoting is notoriously subtle and often trips macro authors up. If you run into trouble, <code>macrodebug</code> can save the day by showing you precisely what your macro is expanding to. It's a tool you can run in the repl to inspect the results of the macro expansion:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb10-1" title="1">&gt;&gt; (<span class="kw">macrodebug</span> (thrice-if (<span class="kw">and</span> (transporters-online?) </a>
<a class="sourceLine" id="cb10-2" title="2">                               (<span class="op">&lt;</span> <span class="dv">8</span> (torpedo-count))) </a>
<a class="sourceLine" id="cb10-3" title="3">                          (make-it-so!)))</a>
<a class="sourceLine" id="cb10-4" title="4">(<span class="kw">if</span> (<span class="kw">and</span> (transporters-online?) (<span class="op">&lt;</span> <span class="dv">8</span> (torpedo-count))) (<span class="kw">do</span> (make-it-so!) (<span class="kw">if</span> (<span class="kw">and</span> (transporters-online?) (<span class="op">&lt;</span> <span class="dv">8</span> (torpedo-count))) (<span class="kw">do</span> (make-it-so!) (<span class="kw">if</span> (<span class="kw">and</span> (transporters-online?) (<span class="op">&lt;</span> <span class="dv">8</span> (torpedo-count))) (<span class="kw">do</span> (make-it-so!)))))))</a></code></pre></div>
<p>Unfortunately as you can see, the downside of <code>macrodebug</code> is that its output is not the most readable. You will want to copy it into your text editor and add in newlines and indentation before you go any further, or run it thru <a href="https://git.sr.ht/~technomancy/fnlfmt">fnlfmt</a>.</p>
<h3 id="identifiers-and-gensym">Identifiers and Gensym</h3>
<p>Macros which introduce identifiers are slightly more complicated. If you write a macro which accepts code as an argument, you can't make any assumptions about the code. For example:</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb11-1" title="1">(<span class="ex">local</span><span class="fu"> engines </span>(<span class="kw">require</span> :engines))</a>
<a class="sourceLine" id="cb11-2" title="2"></a>
<a class="sourceLine" id="cb11-3" title="3">(<span class="ex">macro</span><span class="fu"> with-weapons-check </span>[...]</a>
<a class="sourceLine" id="cb11-4" title="4">  `(<span class="kw">let</span> [phasers (<span class="kw">require</span> :phasers)</a>
<a class="sourceLine" id="cb11-5" title="5">         overloaded? (phasers.overloaded?)]</a>
<a class="sourceLine" id="cb11-6" title="6">     (<span class="kw">when</span> (<span class="kw">not</span> overloaded?)</a>
<a class="sourceLine" id="cb11-7" title="7">       ,...)))</a>
<a class="sourceLine" id="cb11-8" title="8"></a>
<a class="sourceLine" id="cb11-9" title="9">(<span class="kw">let</span> [overloaded? (engines.overloaded?)]</a>
<a class="sourceLine" id="cb11-10" title="10">  (with-weapons-check</a>
<a class="sourceLine" id="cb11-11" title="11">    (<span class="kw">if</span> overloaded?</a>
<a class="sourceLine" id="cb11-12" title="12">        (<span class="kw">print</span> <span class="st">&quot;Engines overloaded&quot;</span>)</a>
<a class="sourceLine" id="cb11-13" title="13">        (go-to-warp!))))</a></code></pre></div>
<p>This program will not behave as expected, because the outer <code>overloaded?</code> value is shadowed by the one introduced by the macro. In this case, the bug is very subtle and might not get noticed until there is a dangerous situation!</p>
<p>But in fact, this program will not even compile. Fennel will detect that the macro is introducing a new identifier in an unsafe way:</p>
<pre><code>Compile error in enterprise.fnl:4
  macro tried to bind phasers without gensym

  `(let [phasers (require :phasers)
         ^^^^^^^
* Try changing to phasers# when introducing identifiers inside macros.
</code></pre>
<p>The compiler's helpful hint on the last line there points us to a solution. Adding <code>#</code> to the end of a symbol inside a quoted form activates &quot;gensym&quot;, that is, the symbol will be expanded to a different symbol which is guaranteed to be unique and can never conflict with an existing local value.</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb13-1" title="1">(<span class="ex">macro</span><span class="fu"> with-weapons-check </span>[...]</a>
<a class="sourceLine" id="cb13-2" title="2">  `(<span class="kw">let</span> [phasers# (<span class="kw">require</span> :phasers)</a>
<a class="sourceLine" id="cb13-3" title="3">         overloaded?# (phasers#.overloaded?)]</a>
<a class="sourceLine" id="cb13-4" title="4">     (<span class="kw">when</span> (<span class="kw">not</span> overloaded?#)</a>
<a class="sourceLine" id="cb13-5" title="5">       ,...)))</a></code></pre></div>
<p>Above we said that there is no difference between using <code>list</code>/<code>sym</code> and using backtick to construct lists. While the resulting code is the same, this safety check will only work when you use backtick, so you should prefer that style. In very rare cases, you could wish to bypass this safety check; when you are in a situation like that, you can use <code>sym</code> to create a symbol which the compiler will not flag. But this is almost always a mistake.</p>
<h3 id="identifier-arguments">Identifier arguments</h3>
<p>Sometimes you want to introduce an identifier that does not use gensym so that code inside the body of the macro can refer to it. For instance, here's a simplified version of the <code>with-open</code> macro that introduces a local which is bound to a file that gets closed automatically at the end of the body.</p>
<div class="sourceCode" id="cb14"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb14-1" title="1">(<span class="ex">macro</span><span class="fu"> with-open2 </span>[[name to-open] ...]</a>
<a class="sourceLine" id="cb14-2" title="2">  `(<span class="kw">let</span> [,name ,to-open</a>
<a class="sourceLine" id="cb14-3" title="3">         value# (<span class="kw">do</span> ,...)]</a>
<a class="sourceLine" id="cb14-4" title="4">     (<span class="op">:</span> ,name :close)</a>
<a class="sourceLine" id="cb14-5" title="5">     value#))</a>
<a class="sourceLine" id="cb14-6" title="6"></a>
<a class="sourceLine" id="cb14-7" title="7">(with-open2 [f (<span class="kw">io</span>.open <span class="st">&quot;/tmp/hey&quot;</span> :w)]</a>
<a class="sourceLine" id="cb14-8" title="8">  (f:write (get-contents)))</a></code></pre></div>
<p>In a case like this, the macro should accept symbols for the locals as arguments. It is convention to take these arguments in square brackets, but the macro system does not enforce this.</p>
<p>Note that the <code>with-weapons-check</code> and <code>with-open2</code> examples above take an arbitrary number of arguments using <code>...</code> which get treated as the &quot;body&quot; of the macro. Picking a name that begins with <code>with-</code> or <code>def</code> for macros like this will make it clearer that their bodies should be indented with two spaces instead of like normal function calls.</p>
<h3 id="ast-quirks">AST quirks</h3>
<p>Above we said that tables in the code are just regular tables that you can manipulate like you do in normal Fennel code. This is mostly true, but there are a few quirks to it.</p>
<p>You can construct tables yourself inside the macro, but they will lack file/line metadata unless you manually copy it over from one of the input tables. This can lead to less effective error messages.</p>
<div class="sourceCode" id="cb15"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb15-1" title="1">(<span class="ex">macro</span><span class="fu"> f </span>[a]</a>
<a class="sourceLine" id="cb15-2" title="2">  (<span class="kw">print</span> (. a <span class="dv">1</span>) :/ (<span class="kw">type</span> (. a <span class="dv">1</span>)) :/ (<span class="kw">length</span> a)))</a>
<a class="sourceLine" id="cb15-3" title="3"></a>
<a class="sourceLine" id="cb15-4" title="4">(f [nil <span class="dv">1</span> <span class="dv">8</span>]) <span class="co">; -&gt; nil / table / 3</span></a></code></pre></div>
<p>When a macro gets any form with a <code>nil</code> in it, the table representing that form does not actually have a nil value in that place; it's a symbol named <code>nil</code> which will evaluate to nil once the code is compiled. This is an important distinction, because otherwise it will not have a single clearly-defined <code>length</code>; Lua's semantics state that both 2 and 4 are valid lengths for <code>[1 2 nil 4]</code> because the table has a gap in it. But using a <code>nil</code> symbol instead in the AST means the length will always be 4, which helps avoids some nasty bugs.</p>
<h3 id="using-functions-from-a-module">Using functions from a module</h3>
<p>If you want your macroexpanded code to call a function your library provides in a module, you may at first accidentally write a sloppy version of your macro which only works if the module is already required in a local in scope where the macro is called:</p>
<div class="sourceCode" id="cb16"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb16-1" title="1">(<span class="ex">macro</span><span class="fu"> mymacro </span>[a b c]</a>
<a class="sourceLine" id="cb16-2" title="2">  `(mymodule.process (<span class="op">+</span> b c) a))</a></code></pre></div>
<p>However, this is error-prone; you shouldn't make any assumptions about the scope of the caller. While it will fail to compile in contexts where <code>mymodule</code> is not in scope at all, there is no guarantee that <code>mymodule</code> will be bound to the module you intend. It's much better to expand to a form which requires whatever module is needed inside the macroexpansion:</p>
<div class="sourceCode" id="cb17"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb17-1" title="1">(<span class="ex">macro</span><span class="fu"> mymacro </span>[a b c]</a>
<a class="sourceLine" id="cb17-2" title="2">  `(<span class="kw">let</span> [mymodule# (<span class="kw">require</span> :mymodule)]</a>
<a class="sourceLine" id="cb17-3" title="3">     (mymodule#.process (<span class="op">+</span> b c) a)))</a></code></pre></div>
<p>Remember that <code>require</code> caches all modules, so this will be a cheap table lookup.</p>
<h3 id="macro-modules">Macro Modules</h3>
<p>In the example above we used <code>macro</code> to write an inline macro. This is great when you only need it used in one file. These macros cannot be exported as part of the module, but the <code>import-macros</code> form lets you write a macro module containing macros which can be re-used anywhere.</p>
<p>A macro module is just like any other module: it contains function definitions and ends with a table containing just the functions which are exported. The only difference is that the entire macro module is loaded in the <strong>compiler environment</strong>. This is how it has access to functions like <code>list</code>, <code>sym</code>, etc. For a full list of functions available, see the &quot;Compiler Environment&quot; section of <a href="reference.html#compiler-environment">the reference</a>.</p>
<div class="sourceCode" id="cb18"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb18-1" title="1"><span class="co">;; thrice.fnl</span></a>
<a class="sourceLine" id="cb18-2" title="2">(<span class="ex">fn</span><span class="fu"> thrice-if </span>[condition result]</a>
<a class="sourceLine" id="cb18-3" title="3">  (<span class="ex">fn</span><span class="fu"> step </span>[i]</a>
<a class="sourceLine" id="cb18-4" title="4">    (<span class="kw">if</span> (<span class="op">&lt;</span> <span class="dv">0</span> i)</a>
<a class="sourceLine" id="cb18-5" title="5">        `(<span class="kw">if</span> ,condition (<span class="kw">do</span> ,result ,(step (<span class="op">-</span> i <span class="dv">1</span>))))))</a>
<a class="sourceLine" id="cb18-6" title="6">  (step <span class="dv">3</span>))</a>
<a class="sourceLine" id="cb18-7" title="7"></a>
<a class="sourceLine" id="cb18-8" title="8">{<span class="op">:</span> thrice-if}</a></code></pre></div>
<p>The <code>import-macros</code> form allows you to use macros from a macro module. The first argument is a destructuring form which lets you pull individual macros out, but you can bind the entire module to a single table if you prefer. The second argument is the name of the macro module. Again, see <a href="reference.html#import-macros-load-macros-from-a-separate-module">the reference</a> for details.</p>
<div class="sourceCode" id="cb19"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb19-1" title="1">(<span class="kw">import-macros</span> {<span class="op">:</span> thrice-if} :thrice)</a>
<a class="sourceLine" id="cb19-2" title="2"></a>
<a class="sourceLine" id="cb19-3" title="3">(thrice-if (main-power-online?)</a>
<a class="sourceLine" id="cb19-4" title="4">           (enable-replicators))</a></code></pre></div>
<h3 id="assert-compile">assert-compile</h3>
<p>You can use <code>assert</code> in your macros to defensively ensure the inputs passed in make sense. However, it's preferable to use the <code>assert-compile</code> form instead. It works exactly the same as <code>assert</code> except it takes an optional third argument, which should be a table or symbol passed in as an argument to the macro. Lists, other table types, and symbols have file and line number metadata attached to them, which means the compiler can pinpoint the source of the problem in the error message.</p>
<div class="sourceCode" id="cb20"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb20-1" title="1">(<span class="ex">macro</span><span class="fu"> thrice-if </span>[condition result]</a>
<a class="sourceLine" id="cb20-2" title="2">  (assert-compile (list? result) <span class="st">&quot;expected list for result&quot;</span> result)</a>
<a class="sourceLine" id="cb20-3" title="3">  (<span class="ex">fn</span><span class="fu"> step </span>[i]</a>
<a class="sourceLine" id="cb20-4" title="4">    (<span class="kw">if</span> (<span class="op">&lt;</span> <span class="dv">0</span> i)</a>
<a class="sourceLine" id="cb20-5" title="5">        `(<span class="kw">if</span> ,condition (<span class="kw">do</span> ,result ,(step (<span class="op">-</span> i <span class="dv">1</span>))))))</a>
<a class="sourceLine" id="cb20-6" title="6">  (step <span class="dv">3</span>))</a>
<a class="sourceLine" id="cb20-7" title="7"></a>
<a class="sourceLine" id="cb20-8" title="8">(thrice-if true abc)</a></code></pre></div>
<pre class="shell"><code>$ fennel scratch.fnl
Compile error in scratch.fnl:8
  expected list for result

(thrice-if true abc)
                ^^^
stack traceback: ...
</code></pre>
<p>It's not required, but it's a nice courtesy to your users.</p>
<h2 id="thats-all">That's all!</h2>
<p>Now you're all set: go write a macro or two.</p>
<p>But ... don't go overboard.</p>
<hr />
<p><a href="index.html">Home</a></p>
<p><a href="https://git.sr.ht/~technomancy/fennel-lang.org">source for this site</a></p>
</body>
</html>
