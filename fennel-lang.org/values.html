<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>Fennel – Values of Fennel</title>
  <style type="text/css">
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
  </style>
  <style type="text/css">
a.sourceLine { display: inline-block; line-height: 1.25; }
a.sourceLine { pointer-events: none; color: inherit; text-decoration: inherit; }
a.sourceLine:empty { height: 1.2em; }
.sourceCode { overflow: visible; }
code.sourceCode { white-space: pre; position: relative; }
div.sourceCode { margin: 1em 0; }
pre.sourceCode { margin: 0; }
@media screen {
div.sourceCode { overflow: auto; }
}
@media print {
code.sourceCode { white-space: pre-wrap; }
a.sourceLine { text-indent: -1em; padding-left: 1em; }
}
pre.numberSource a.sourceLine
  { position: relative; left: -4em; }
pre.numberSource a.sourceLine::before
  { content: attr(title);
    position: relative; left: -1em; text-align: right; vertical-align: baseline;
    border: none; pointer-events: all; display: inline-block;
    -webkit-touch-callout: none; -webkit-user-select: none;
    -khtml-user-select: none; -moz-user-select: none;
    -ms-user-select: none; user-select: none;
    padding: 0 4px; width: 4em;
    color: #aaaaaa;
  }
pre.numberSource { margin-left: 3em; border-left: 1px solid #aaaaaa;  padding-left: 4px; }
div.sourceCode
  {  }
@media screen {
a.sourceLine::before { text-decoration: underline; }
}
code span.al { color: #ff0000; font-weight: bold; } /* Alert */
code span.an { color: #60a0b0; font-weight: bold; font-style: italic; } /* Annotation */
code span.at { color: #7d9029; } /* Attribute */
code span.bn { color: #40a070; } /* BaseN */
code span.bu { } /* BuiltIn */
code span.cf { color: #007020; font-weight: bold; } /* ControlFlow */
code span.ch { color: #4070a0; } /* Char */
code span.cn { color: #880000; } /* Constant */
code span.co { color: #60a0b0; font-style: italic; } /* Comment */
code span.cv { color: #60a0b0; font-weight: bold; font-style: italic; } /* CommentVar */
code span.do { color: #ba2121; font-style: italic; } /* Documentation */
code span.dt { color: #902000; } /* DataType */
code span.dv { color: #40a070; } /* DecVal */
code span.er { color: #ff0000; font-weight: bold; } /* Error */
code span.ex { } /* Extension */
code span.fl { color: #40a070; } /* Float */
code span.fu { color: #06287e; } /* Function */
code span.im { } /* Import */
code span.in { color: #60a0b0; font-weight: bold; font-style: italic; } /* Information */
code span.kw { color: #007020; font-weight: bold; } /* Keyword */
code span.op { color: #666666; } /* Operator */
code span.ot { color: #007020; } /* Other */
code span.pp { color: #bc7a00; } /* Preprocessor */
code span.sc { color: #4070a0; } /* SpecialChar */
code span.ss { color: #bb6688; } /* SpecialString */
code span.st { color: #4070a0; } /* String */
code span.va { color: #19177c; } /* Variable */
code span.vs { color: #4070a0; } /* VerbatimString */
code span.wa { color: #60a0b0; font-weight: bold; font-style: italic; } /* Warning */
  </style>
      <link rel="stylesheet" href="fennel.css"></link>
      <link rel="stylesheet" href="https://code.cdn.mozilla.net/fonts/fira.css"/>
      <script type="text/javascript" src="fengari-web.js"></script>
      <script type="application/lua" src="init.lua" async></script>
      <title>the Fennel programming language</title>
      <style type="text/css">
        pre {
        border-radius: 3px;
        border: 1px solid #ddd;
        padding: 0.7em;
        }
      </style>
</head>
<body>
<header>
<h1 class="title">Values of Fennel</h1>
</header>
<nav id="TOC">
<ul>
<li><a href="#compile-time">Compile-time</a></li>
<li><a href="#transparency">Transparency</a></li>
<li><a href="#making-mistakes-obvious">Making mistakes obvious</a></li>
<li><a href="#consistency-and-distinction">Consistency and Distinction</a></li>
</ul>
</nav>
<p>This document is an outline of the guiding design principles of Fennel. Fennel's community values are covered in the <a href="coc.html">code of conduct</a>.</p>
<h2 id="compile-time">Compile-time</h2>
<p>First and foremost is the notion that Fennel is a compiler with no runtime. This places somewhat severe limits on what we can accomplish, but it also creates a valuable sense of focus. We are of course very fortunate to be building on a language like Lua where the runtime semantics are for the most part excellent, and the areas upon which we improve can be identified at compile time.</p>
<p>This means Fennel (the language) consists entirely of macros and special forms, and no functions. Fennel (the compiler) of course has plenty of functions in it, but they are for the most part not intended for use outside the context of embedding the compiler in another Lua program.</p>
<p>The exception to this rule is <code>fennel.view</code> which can be used independently; it addresses a severe problem in Lua's runtime semantics where <code>tostring</code> on a table produces nearly-useless results. But this can be thought of as simply another library which happens to be included in the compiler. The <code>fennel.view</code> function is a prerequisite to having a useful repl. The repl of course is also a function you can call at runtime if you embed the compiler, but this is a special case that blurs the lines between runtime and compile time.</p>
<h2 id="transparency">Transparency</h2>
<p>Well-written Lua programs exhibit an excellent sense of transparency largely due to how Lua leans on lexical scoping so predominantly. When you look at a good Lua program, you can tell exactly where any given identifier comes from just by following the basic rules of lexical scope. Badly-written Lua programs often use globals and do not have this property.</p>
<p>With Fennel we try to take this even further by making globals an error by default. It's still possible to write programs that use globals using <code>_G</code> (indeed for Lua interop this sometimes cannot be avoided) but it should be very clear when this happens; it's not something that you would do by accident or due to laziness.</p>
<p>One counter-example here is the deprecated <code>require-macros</code> form; it introduced new identifiers into the scope without making it clear what the names were. That is why it was replaced by the much clearer <code>import-macros</code>. The two below are equivalent, but one has hidden implicit scope changes and the other exhibits transparency:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb1-1" title="1">(<span class="kw">require-macros</span> :my.<span class="ex">macros</span><span class="fu">) </span><span class="co">; what did we introduce here? who knows!</span></a>
<a class="sourceLine" id="cb1-2" title="2"></a>
<a class="sourceLine" id="cb1-3" title="3">(<span class="kw">import-macros</span> {<span class="op">:</span> transform-bar <span class="op">:</span> skip-element} :my-macros)</a></code></pre></div>
<p>Of course this comes at the cost of a little extra verbosity, but it is well worth it. In Fennel programs, you should never have a hard time answering the question &quot;where did this come from?&quot;</p>
<h2 id="making-mistakes-obvious">Making mistakes obvious</h2>
<p>The most obvious legitimate criticism of Lua is that it makes it easy to set or read globals by accident simply by making a typo in the name of an identifier. This is easily fixed by requiring global access to be explicit; it's perhaps the most obvious way that Fennel tries to catch common mistakes. But there are others; for instance Fennel does not allow you to shadow the name of a special form with a local. It also doesn't allow you to omit the body from a <code>let</code> form like many other lisps do:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode fennel"><code class="sourceCode fennelsyntax"><a class="sourceLine" id="cb2-1" title="1">(<span class="ex">fn</span><span class="fu"> abc </span>[]</a>
<a class="sourceLine" id="cb2-2" title="2">  (<span class="kw">let</span> [a <span class="dv">1</span></a>
<a class="sourceLine" id="cb2-3" title="3">        b <span class="dv">2</span></a>
<a class="sourceLine" id="cb2-4" title="4">        c (calculate-c)]) <span class="co">; &lt;- missing body!</span></a>
<a class="sourceLine" id="cb2-5" title="5">    (<span class="op">+</span> a b c))</a></code></pre></div>
<p>This will be flagged as an error because the entire <code>let</code> form is closed after the call to <code>calculate-c</code> when the intent was clearly only to close the binding form.</p>
<p>Another example would be that you can't call <code>set</code> on a local unless it is introduced using <code>var</code>. This means that if you have code which assumes the locals will remain the same and then go and mess with that assumption it is an error; you have to explicitly declare that assumption void first before you are permitted to violate it.</p>
<p>This touches on a broader theme: it's easier to understand code when you can look at it and immediately know certain things will never happen. By excluding certain capabilities from the language, certain mistakes become impossible.</p>
<p>For example, Fennel code will never use a block of memory after it has been freed, because <code>malloc</code> and <code>free</code> are not even part of its vocabulary. In languages with immutable data structures, it's impossible to have bugs which come from one piece of code making a change to data in a way that another function did not expect. Fennel does not have immutable data structures, but still we recognize that removing the ability to do things (or making them opt-in instead of opt-out) can significantly improve the resulting code.</p>
<p>Other examples include the lack of <code>goto</code> and the lack of early returns. Or how if a loop terminates early, it will make this obvious by using an <code>&amp;until</code> clause at the top of the loop; you don't have to read the entire loop body to search for a <code>break</code> as you would in Lua.</p>
<h2 id="consistency-and-distinction">Consistency and Distinction</h2>
<p>Older lisps overload parentheses to mean lots of different things; they are used for data lists, but they are also used to signify function and macro calls or to group key/value pairs together and around an entire group of key/value pairs in <code>let</code>. There are many other uses.</p>
<p>Fennel overloads delimiters in a few ways, but the distinction should be visually clearer and much more limited by context. Parentheses almost always mean a function or macro call; the main exception is inside a binding form where it can be used to bind multiple values. The other exception is the now-deprecated <code>?</code> notation for pattern matching guards; it has been replaced by calling <code>where</code>. Square brackets usually indicate a sequential table, but in a macro they can indicate a binding form. Perhaps were Fennel rooted in a language richer in typographical delimiters than English, this overloading would not be necessary and every delimiter pair could have exactly one meaning.</p>
<p>This is something Lua drops the ball on in a few places; it overloads one notation to mean different things. For instance, <code>for</code> in Lua can be used to numerically step from one number to another in a loop, or it can be used to step thru an iterator. Fennel separates this out into <code>for</code> to be used with numeric stepping and <code>each</code> which uses iterators. Another example is the table literal notation: Lua uses <code>{}</code> for sequential tables as well as key/value tables, while Fennel uses <code>[]</code> for sequential tables following more recent programming convention.</p>
<p>Fennel uses notation in other ways to avoid ambiguity; for instance when <code>&amp;as</code> was introduced in destructuring forms for giving access to the entire table, the <code>&amp;</code> character was reserved so that it could not be used in identifiers. This also makes it easier to write macros which do similar things; now we have a way to indicate that a given symbol must have some meaning assigned to it other than being an identifier.</p>
<hr />
<p><a href="index.html">Home</a></p>
<p><a href="https://git.sr.ht/~technomancy/fennel-lang.org">source for this site</a></p>
</body>
</html>
